/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;

import java.awt.*;
import java.util.*;
import java.text.NumberFormat;
import javax.help.CSH;

import com.bhpBilliton.viewer2d.data.GeneralDataSource;
import com.gwsys.gw2d.ruler.AdjustableStepTick;
import com.gwsys.gw2d.ruler.MajorMinorTick;
import com.gwsys.gw2d.ruler.RangeRulerModel;
import com.gwsys.gw2d.ruler.RulerModel;
import com.gwsys.gw2d.ruler.RulerView;
import com.gwsys.gw2d.ruler.TickDefinition;
import com.gwsys.gw2d.shape.GridLine;
import com.gwsys.gw2d.util.*;

import com.gwsys.gw2d.view.*;
import com.gwsys.gw2d.gui.OrientedLabel;
import com.gwsys.gw2d.model.CommonShapeLayer;
import com.gwsys.gw2d.model.RenderingAttribute;
import com.gwsys.seismic.indexing.SegyBinaryEntry;
import com.gwsys.seismic.indexing.HeaderFieldEntry;
import com.gwsys.seismic.indexing.SegyHeaderFormat;
import com.gwsys.seismic.indexing.SeismicConstants;
import com.gwsys.seismic.reader.SeismicReader;
import com.gwsys.seismic.core.*;
import com.gwsys.seismic.io.TraceHeader;
import com.gwsys.seismic.util.PlotConstants;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This is the main plot for the cross-section viewer.
 *               It can display seismic, model, event data,
 *               and annotate them correctly. <br>
 *               The annotation of a cross-section display may include
 *               a vertical axis, multiple horizontal axes, one for each
 *               selected trace header fields, a title, a color bar, and
 *               four labels, one on each side. <br>
 *               Because cgSeismicPlot has the limitation of handling only one
 *               pipeline for annotaion, this class is created borrowing  some
 *               of its code from cgSeismicPlot. The major part of the class is
 *               for drawing axes correctly for the display.
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpPlotXV extends AbstractPlot {
    private ArrayList _available = new ArrayList();
    private ArrayList _selected  = new ArrayList();

    private boolean _sampleStepAuto;
    private boolean _sampleGridLine;

    private SimpleViewContainer _gridView;
    private GridLine _gridShape;
    private CommonShapeLayer _gridLayer;
    private RenderingAttribute _gridAttribute;

    //This may be redundant with DND code in BhpPlotMV but left in place until
    //determining whether all AbstractPlot subclasses should implement DND.
    private DTListener dtListener;
    private DropTarget dropTarget;
    public static final int acceptableActions = DnDConstants.ACTION_COPY;
    /**
     * Creates a new cross-section plot.
     * @param hs horizontal scale of the plot
     * @param vs vertical scale of the plot
     */
    public BhpPlotXV(double hs, double vs) {
        super(hs, vs);
        CSH.setHelpIDString(this,"xsection_viewer");
        _sampleStepAuto = true;
        _gridShape = null;
        _gridLayer = null;
        _gridAttribute = new RenderingAttribute();
        _gridAttribute.setLineColor(Color.lightGray);
        this.dtListener = new DTListener(this);
        this.dropTarget = new DropTarget(this, this.acceptableActions, this.dtListener, true);
    }

    /**
     * Retrieves the available fields for horizontal annotation.
     */
    public ArrayList getAvailable()  { return _available; }
    /**
     * Retrieves the fields that is currently used for horizontal annotation.
     */
    public ArrayList getSelected()   { return _selected; }
    /**
     * Finds out if the vertical axis step value is auto calculated or user defined.
     */
    public boolean getSampleStepAuto() { return _sampleStepAuto; }
    /**
     * Finds out if the sample grid line will be drawn. <br>
     * Currently, no grid line is implemented.
     */
    public boolean getSampleGridLine() { return _sampleGridLine; }


    /**
     * Sets the fields for horizontal annotation and remaining available fields. <br>
     * For all the fields in the trace header, it is either selected for annotation
     * or remaining in the available list. That is <br>
     * selected + available = all trace header fields
     * @param v the list of selected fields for horizontal annotation.
     */
    public void setSelectedAndAvailable(ArrayList v)   {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) { System.out.println("No layer in setSelectedAndAvai"); return; }
        SeismicWorkflow pipeline = layer.getPipeline();
        if (pipeline == null) { System.out.println("No pipeline"); return; }
        _available.clear();
        _available.addAll(pipeline.getDataLoader().getDataReader().getKeys());
        if (layer.getDataSource().equals(GeneralDataSource.DATA_SOURCE_SEGY)) {
            // this is only for plain segy data.
            SegyBinaryEntry traceIdDesc = new SegyBinaryEntry(
                    0, SeismicConstants.DATA_FORMAT_UNSIGNEDINT,
                    DataChooser.TRACE_ID_AS_KEY, "TraceNumber",
                    HeaderFieldEntry.FIELD_TYPE_PRIMARY, true);
            _available.add(traceIdDesc);
        }

        _selected.clear();
        _selected.addAll(v);
        for (int i=0; i<v.size(); i++)
            _available.remove(((BhpTraceAxisObject)v.get(i))._field);
    }
    /**
     * Sets the vertical sample axis step state.
     * @param b true if the step needs to be auto calculated,
     *        false to use the user defined step value.
     */
    public void setSampleStepAuto(boolean b) { _sampleStepAuto = b; }
    /**
     * Sets if the grid line will be drawn. <br>
     * Currently, no grid line is implemented.
     */
    public void setSampleGridLine(boolean b) { _sampleGridLine = b; }

    /**
     * Adds one more horizontal trace axis.
     * @param name the name of the trace header field.
     * @param step the axis step value.
     * @param line boolean flag indicates if the axis base line needs to be drawn.
     * @param gap minimum gap between labels, in pixels.
     * @param angle angle of the axis labels.
     * @param limit limit of the lable values.
     * @param flag flag of the label. Not Use.
     */
    public void addTraceAxis(String name, double step, boolean line,
                             double gap, double angle, double limit, int flag) {
        _selected.add(new BhpTraceAxisObject(name, step, line, gap, angle, limit, flag));
    }

    /**
     * Retrieves the annotation readings for the given position.
     * @param virtTraceId the horizontal position, the trace ID of the interested trace.
     * @param virtualY the vertical position, in model space.
     * @return the annotation readings.
     *         The fields name and value pairs are put in a Hashtable.
     */
    public Hashtable buildSelectedAnnotationParameter(int virtTraceId, int virtualY) {
        BhpLayer theLayer = this.getBhpLayer(_axisAssociateId);
        Hashtable selectParameter = new Hashtable();
        if (virtTraceId == -999) return selectParameter;

        SeismicWorkflow pipeline = theLayer.getPipeline();
        TraceHeader tmeta = pipeline.getDataLoader().getDataSelector().
        getTraceHeader(virtTraceId);
        if (tmeta == null) return selectParameter;

        BhpSeismicTableModel layerParameter = theLayer.getParameter();
        String vname = layerParameter.getVerticalName();
        double verticalValue = theLayer.findVerticalReadingFromModel(virtualY);
        selectParameter.put(vname, new Double(verticalValue));

        Hashtable headerValues = tmeta.getDataValues();
        Vector formats = pipeline.getDataLoader().getDataReader().getDataFormat().getTraceHeaderFormats();
        SegyHeaderFormat headerFormat = (SegyHeaderFormat) (formats.firstElement());
//        Vector headerFields = headerFormat.getHeaderFields();

        Integer headerId;
        String headerName;
        Object headerValue;
//        HeaderFieldEntry headerField;


        for (int kkk=0; kkk<layerParameter.getRowCount(); kkk++) {
            headerName = ((String)(layerParameter.getValueAt(kkk, 0))).trim();
            if (headerName.equals(vname)) continue;
            if (headerName.equals("TraceNumber")) {
                int realid = pipeline.getDataLoader().getDataSelector().virtualToReal(virtTraceId);
                double[] idSettings = layerParameter.getKeySettingsOfName("TraceNumber");
                realid = (int) (idSettings[0] + realid * idSettings[2]);
                selectParameter.put(headerName, new Integer(realid));
                continue;
            }
            headerId = BhpSegyFormat.getFieldForName(headerName, headerFormat);

            if (headerId!=null && headerValues.containsKey(headerId)) {
                headerValue = headerValues.get(headerId);
                selectParameter.put(headerName, headerValue);
            }
        }

        // adding the data value at the point.
        // for horizon data, it does not make much sense.
        if (theLayer.getDataType() != BhpViewer.BHP_DATA_TYPE_EVENT)
            selectParameter.put("value", ""+theLayer.findSpecificDataValue(virtTraceId, verticalValue));

        return selectParameter;
   }

   /* This method will only be called when we loading a configuration file */
    /**
     * Updates the annotation of the plot.
     */
    public void updateAnnotation() {
        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) {
			System.out.println("No layer in updateAnnotation"); return; }
        SeismicWorkflow pipeline = layer.getPipeline();
        if (pipeline == null) {
			System.out.println("No pipeline"); return; }

        updateVAxes();
        updateMiscAnnotation();

        _available.clear();
        _available.addAll(pipeline.getDataLoader().getDataReader().getKeys());
        if (layer.getDataSource().equals(GeneralDataSource.DATA_SOURCE_SEGY)) {
            // this is only for plain segy data.
            SegyBinaryEntry traceIdDesc = new SegyBinaryEntry(
                    0, SeismicConstants.DATA_FORMAT_UNSIGNEDINT,
                    DataChooser.TRACE_ID_AS_KEY, "TraceNumber",
                    HeaderFieldEntry.FIELD_TYPE_PRIMARY, true);
            _available.add(traceIdDesc);
        }

        String keyName;
        HeaderFieldEntry field;
        BhpTraceAxisObject newObj = null;
        BhpTraceAxisObject oldObj = null;
        for (int i=0; i<_selected.size(); i++) {
            oldObj = (BhpTraceAxisObject) _selected.get(i);
            keyName = oldObj._name;
            if (keyName.equals("TraceNumber")) {
                SegyBinaryEntry traceIdDesc = new SegyBinaryEntry(
                                0, SeismicConstants.DATA_FORMAT_UNSIGNEDINT,
                                DataChooser.TRACE_ID_AS_KEY, "TraceNumber",
                                HeaderFieldEntry.FIELD_TYPE_PRIMARY, true);
                newObj = new BhpTraceAxisObject(traceIdDesc);
                newObj._step = oldObj._step;
                newObj._drawLine = oldObj._drawLine;
                _selected.set(i, newObj);
            }
            else {
                for (int j=0; j<_available.size(); j++) {
                    field = (HeaderFieldEntry) _available.get(j);
                    if (field.getName().equals(keyName)) {
                        newObj = new BhpTraceAxisObject(field);
                        newObj._step = oldObj._step;
                        newObj._drawLine = oldObj._drawLine;
                        newObj._labelAngle = oldObj._labelAngle;
                        newObj._tickSpace = oldObj._tickSpace;
                        newObj._labelLimit = oldObj._labelLimit;
                        newObj._labelFlag = oldObj._labelFlag;
                        _selected.set(i, newObj);
                        _available.remove(j);
                        break;
                    }
                }
            }
        }

        updateHAxes();

        this.invalidate();
        this.validate();
    }

    /**
     * Sets the layer that based on which the annotation will be drawn.
     * @param id the identification number of the layer.
     * @param doUpdate a boolean flag indicates if the display shouble be
     *        updated right away. This can be used to avoid multiple repaint.
     */
    public void setAxisAssociateLayer(int id, boolean doUpdate) {
        if (id == _axisAssociateId) return;

        if (id < 0)  id = -1;

        if (id < 0) {
            _axisAssociateId = id;
            cleanAxesAndAnnotations();
            _available.clear();
            _selected.clear();

            _hAxesSyncName = "";

            return;
        }

        _axisAssociateId = id;

        BhpLayer layer = this.getBhpLayer(id);
        if (layer == null) {
            return;
        }
        //BhpSeismicLayer slayer = (BhpSeismicLayer) layer;
        SeismicWorkflow pipeline = layer.getPipeline();
        if (pipeline == null) return;
        // top & bottom
        // clean it before adding new items
        _available.clear();
        _selected.clear();

        //_hAxesLoc = cgSeismicPlot.TOP;
        _available.addAll(pipeline.getDataLoader().getDataReader().getKeys());
        if (layer.getDataSource().equals(GeneralDataSource.DATA_SOURCE_SEGY)) {
            // this is only for plain segy data.
            SegyBinaryEntry traceIdDesc = new SegyBinaryEntry(
                    0, SeismicConstants.DATA_FORMAT_UNSIGNEDINT,
                    DataChooser.TRACE_ID_AS_KEY, "TraceNumber",
                    HeaderFieldEntry.FIELD_TYPE_PRIMARY, true);
            _available.add(traceIdDesc);
        }
        String hname = layer.getParameter().getXSectionHorizontalKeyName();
        HeaderFieldEntry fldDesc = null;
        for (int i=0; i<_available.size(); i++) {
            fldDesc = (HeaderFieldEntry) _available.get(i);
            if (fldDesc.getName().equals(hname)) {
                _selected.add(new BhpTraceAxisObject(fldDesc));
                _available.remove(fldDesc);
                break;
            }
        }


        // draw annotation
        if (doUpdate) {
            updateHAxes();
            updateVAxes();
            updateMiscAnnotation();
        }

        //_view.setTransformation(_view.getTransformation());
    }

    /**
     * Updates the horizontal axes.
     */
    public void updateHAxes() {
        clearAnnotation(_annT, _annTL, true);

        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) return;
        if (_hAxesLoc != PlotConstants.NONE) {
            boolean findHAxesSyncName = false;
            TickDefinition tg1;
            BhpTraceAxisObject taObject = null;
            HeaderFieldEntry hfd = null;
            double step,gap,angle,limit;
            boolean drawLine;
            int flag;
            //int idx;
            int topAxesCount = 0;
            int bottomAxesCount = 0;

            for (int k=_selected.size()-1; k>=0; k--) {

                taObject = (BhpTraceAxisObject) _selected.get(k);
                hfd = taObject._field;

                if (hfd.getName().equals(_hAxesSyncName)) findHAxesSyncName = true;
                step = taObject._step;
                gap = taObject._tickSpace;
                angle = taObject._labelAngle;
                drawLine = taObject._drawLine;
                flag = taObject._labelFlag;
                limit = taObject._labelLimit;


                NumberFormat nf = NumberFormat.getInstance();
                switch (hfd.getValueType()) {
                    case SeismicConstants.DATA_FORMAT_IBM_FLOAT:
                    case SeismicConstants.DATA_FORMAT_DOUBLE:
                    case SeismicConstants.DATA_FORMAT_FLOAT:
                        nf.setMinimumFractionDigits(3);
                        break;
                }

                SeismicWorkflow pipeline = layer.getPipeline();
                int traceAxisHeight = 27;
                if (angle != 0.0)
                    traceAxisHeight = calculateTraceAxisHeight(pipeline, hfd, angle);

                Bound2D vl = layer.getModel().getBoundingBox();
                double mlt = pipeline.getDataLoader().getDataSelector().getStartSampleValue();
                double mlb = pipeline.getDataLoader().getDataSelector().getEndSampleValue();
                Bound2D ml = new Bound2D(vl.getMinX()/layer.getLayerHorizontalScale(), mlt,
                                       vl.getMaxX()/layer.getLayerHorizontalScale(), mlb);
                Transform2D trans1 = layer.getTransformation();
                double scalex = trans1.getScaleX() * layer.getLayerHorizontalScale();
                double scaley = trans1.getScaleY() * layer.getLayerVerticalScale();
                if (layer instanceof BhpEventLayer) {
                    ml = new Bound2D(vl.getMinX(), mlt, vl.getMaxX(), mlb);
                    scalex = trans1.getScaleX();
                    scaley = trans1.getScaleY();
                }
                _haxisTrans = new Transform2D(
                                            scalex, scaley,
                                            trans1.getTranslateX(),
                                            trans1.getTranslateY());
                //tg1 = new BhpTraceTickGenerator(0, layer, hfd.getIdentifier(), step, gap);

                if ((_hAxesLoc & PlotConstants.TOP) != 0) {

                    NumberFormat nfMajor = NumberFormat.getInstance();
                    NumberFormat nfMinor = NumberFormat.getInstance();
                    /*
                    BhpLinearLabelGenerator ar = new BhpLinearLabelGenerator(
                                10, 15, 5, 5, 15, 5, angle, true,
                                _attribute, _attribute, _attribute,
                                nfMajor, nfMinor, flag, limit);
                    cgAxisView axisT = new cgAxisView(
                                cgAxisShape.NORTH, ml.getMinX(), ml.getMaxX(),
                                tg1, ar);*/

					SeismicReader reader = pipeline.getDataLoader().getDataReader();
		            int tnumber = reader.getMetaData().getNumberOfTraces();
		            tg1 = new BhpTraceTickGenerator(0, layer, hfd.getFieldId(), step, gap);

		            tg1.setModelRange(0, tnumber);

					RulerModel axisModel=new RangeRulerModel(RangeRulerModel.HORIZONTAL,tg1);
					RulerView axisT = new RulerView(axisModel);
                    axisT.setBackground(Color.white);
                    //axisT.setTransformation(_haxisTrans);
                    //axisT.getAxisShape().setBaseLineVisible(drawLine, true);
                    axisT.setPreferredSize(new Dimension((int)(tnumber*scalex), traceAxisHeight));
                    //axisT.setViewPosition(new Point(layer.getViewPosition().x, 0));
                    _annT.add((axisT), _annT.getComponentCount()-topAxesCount);
					OrientedLabel cornerLabel = new OrientedLabel(hfd.getName());
					cornerLabel.setScrollable(false);
					cornerLabel.setLabelAttribute(_attribute);
                    int nameLen = hfd.getName().length();
                    cornerLabel.setPreferredSize(new Dimension(
                            nameLen*10,
                            traceAxisHeight));
                    _annTL.add(cornerLabel, _annTL.getComponentCount()-topAxesCount);
                    topAxesCount++;
                }

            }
            if (_hAxesSyncName == null || _hAxesSyncName.length() == 0 || !findHAxesSyncName)
                if (hfd != null) _hAxesSyncName = hfd.getName();
        }
        //validate();
        _annT.validate();
    }

    /**
     * Updates the vertical axes.
     */
    public void updateVAxes() {
        clearAnnotation(_annL, true);
        clearAnnotation(_annR, true);

        BhpLayer layer = this.getBhpLayer(_axisAssociateId);
        if (layer == null) return;
        if (_vAxesLoc != PlotConstants.NONE) {
            SeismicWorkflow pipeline = layer.getPipeline();
            //double startValue = pipeline.getSeismicReader().getMetaData().getStartValue();
            Bound2D vl = layer.getModel().getBoundingBox();
            double mlt = pipeline.getDataLoader().getDataSelector().getStartSampleValue();
            double mlb = pipeline.getDataLoader().getDataSelector().getEndSampleValue();
            if (pipeline.getTraceRasterizer() instanceof BhpModelTraceRasterizer) {
                BhpModelTraceRasterizer mrast = (BhpModelTraceRasterizer) pipeline.getTraceRasterizer();
                double mrange = mrast.getEndDepth() - mrast.getStartDepth();
                mlb = mlt + mrange;
            }
            Bound2D modelBound = new Bound2D(vl.getMinX(), mlt, vl.getMaxX(), mlb);

            Transform2D trans1 = layer.getTransformation();
            double scalex = trans1.getScaleX() * layer.getLayerHorizontalScale();
            double scaley = trans1.getScaleY() * layer.getLayerVerticalScale();
            if (layer instanceof BhpEventLayer) {
                scalex = trans1.getScaleX();
                scaley = trans1.getScaleY();
            }
            double sampleScale = pipeline.getDataLoader().getDataReader().getMetaData().getSampleRate();
	        if (pipeline instanceof BhpEventPipeline)
				sampleScale = 1.0;
	        double realStart = modelBound.getMinY();
	        if (pipeline.getTraceRasterizer() instanceof BhpModelTraceRasterizer) {
	                BhpModelTraceRasterizer mrast = (BhpModelTraceRasterizer) pipeline.getTraceRasterizer();
	                realStart = mrast.getStartDepth();
	        }
	        else if (layer instanceof BhpSeismicLayer) {
	                double[] traclSettings = layer.getParameter().getVerticalSettings();
	                if (traclSettings[0] != -1)  {
	                    realStart = traclSettings[0] +
	                    pipeline.getDataLoader().getDataSelector().getStartSampleValue();
	                }
	        }
            _vaxisTrans = new Transform2D(
                                        scalex, scaley / sampleScale,
                                        trans1.getTranslateX(),
                                        //trans1.getTranslateY()
                                        -realStart*scaley/sampleScale);
			AdjustableStepTick tg1 = new AdjustableStepTick(realStart,
					realStart+modelBound.getHeight(),50);
			RulerModel axisModel=new RangeRulerModel(RangeRulerModel.VERTICAL,tg1);
			RulerView axisL = new RulerView(axisModel);


            if ((_vAxesLoc & PlotConstants.LEFT) != 0) {
                NumberFormat nfMajor = NumberFormat.getInstance();
                NumberFormat nfMinor = NumberFormat.getInstance();
                if (pipeline.getDataLoader().getDataReader().getMetaData().getSampleUnits() == SeismicConstants.UNIT_TIME) {
                    nfMajor.setMinimumFractionDigits(2);
                    nfMajor.setMaximumFractionDigits(2);
                    nfMinor.setMinimumFractionDigits(2);
                    nfMinor.setMaximumFractionDigits(2);
                }
                else {
                    nfMajor.setMaximumFractionDigits(0);
                    nfMinor.setMaximumFractionDigits(0);
                }
		    if (!_sampleStepAuto) {
                	  MajorMinorTick mmt = new MajorMinorTick(realStart, 
        					realStart+modelBound.getHeight(),_vMajorStep, _vMinorStep);
        		  axisL = new RulerView(new RangeRulerModel(RangeRulerModel.VERTICAL,mmt));
                }                axisL.setBackground(Color.white);
                axisL.setBackground(Color.white);

                axisL.setPreferredSize(new Dimension(80, (int)(Math.abs(mlt-mlb)*scaley / sampleScale)));

                _annL.add((axisL), _annL.getComponentCount());
            }
            // handle grid according to _sampleGridLine and gtg
            if (_gridLayer == null) {
                _gridLayer = new CommonShapeLayer();
                _model.insertLayer(_gridLayer, 0); // this put grid behind the curves
                _gridView = new SimpleViewContainer(_model);
                _view.addGridView(_gridView);
            }
            _gridLayer.removeAllShapes();

            if (_sampleGridLine && _vaxisTrans != null) {
		double xfactor = 1;
                if (this.getTransformation()!=null)
                    xfactor = this.getTransformation().getScaleX();
                Bound2D gridBbox = new Bound2D(0, realStart, 100000/xfactor,
                        realStart+modelBound.getHeight());
				TickDefinition tick = new AdjustableStepTick(
						realStart,realStart+modelBound.getHeight(), 50);
                _gridShape = new GridLine(gridBbox, tick, tick);
				_gridShape.setAttribute(_gridAttribute);
                _gridShape.setVisible(true);
                _gridLayer.addShape(_gridShape);
                _gridView.setTransformation(_vaxisTrans);
            }
        }
        else {
            // set grid to be invisible
            if (_gridLayer != null) _gridLayer.removeAllShapes();
        }

        validate();
        _annL.validate();
        _annR.validate();
    }

    private int calculateTraceAxisHeight(SeismicWorkflow pipeline, HeaderFieldEntry fld, double angle) {
        int height = 40;
        try {
            int fldid = fld.getFieldId();
            int fldtype = fld.getValueType();
            SeismicReader reader = pipeline.getDataLoader().getDataReader();
            int tnumber = reader.getMetaData().getNumberOfTraces();
            if (tnumber != 0) {
                double svalue = reader.getTraceMetaData(0).getFieldValue(fldid).doubleValue();
                double evalue = reader.getTraceMetaData(tnumber-1).getFieldValue(fldid).doubleValue();
                double mvalue = reader.getTraceMetaData((int)((tnumber-1)/2)).getFieldValue(fldid).doubleValue();
                double maxValue = Math.max(svalue, Math.max(evalue, mvalue));
                int numberOfDigit = 1;
                if (maxValue != 0)
                    numberOfDigit = (int)(Math.log(Math.abs(maxValue))/Math.log(10)) + 1;
                int extraDigit = (int)(numberOfDigit / 3) + 0;      // for ,
                if (fldtype == SeismicConstants.DATA_FORMAT_FLOAT ||
                    fldtype == SeismicConstants.DATA_FORMAT_IBM_FLOAT ||
                    fldtype == SeismicConstants.DATA_FORMAT_DOUBLE)
                    extraDigit = extraDigit + 3;
                double sinAngle = Math.abs(Math.sin(angle));
                double angleHeight = (numberOfDigit + extraDigit) * 8 * sinAngle;
                return (int) (Math.max(angleHeight, height));
            }
        }
        catch (Exception ex) {
            System.out.println("BhpPlotXV.calculateTraceAxisHeight exception");
            ex.printStackTrace();
        }
        return height;
    }
}
