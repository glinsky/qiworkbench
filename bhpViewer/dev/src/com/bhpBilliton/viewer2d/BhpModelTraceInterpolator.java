/*
bhpViewer - a 2D seismic viewer
This program module Copyright (C) Interactive Network Technologies 2006

The following is important information about the license which accompanies this copy of
the bhpViewer in either source code or executable versions (hereinafter the "Software").
The Software, which is owned by Interactive Network Technologies, Inc. ("INT")
is distributed pursuant to the terms of the GNU GPL v 2.0 ("GPL"), which can be found at
http://www.gnu.org/licenses/gpl.txt.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
For a license to use the Software under conditions other than in a way compliant with the
GPL or the additional restrictions set forth in this license agreement or to purchase
support for the Software, please contact: Interactive Network Technologies, Inc.,
2901 Wilcrest, Suite 100, Houston, Texas 77042 <sales@int.com>

All licensees should note the following:

*        In order to compile and/or modify the source code versions of the Software,
a user may require one or more of INT's proprietary toolkits or libraries.
Although all modifications or derivative works based on the source code are governed by the
GPL, such toolkits are proprietary to INT, and a library license is required to make use of
such toolkits when making modifications or derivatives of the Software.
More information about obtaining such a license can be obtained by contacting sales@int.com.

*        Under agreement with INT, BHP Billiton Petroleum (Americas) Inc. or its designee
serves as the custodian ("Custodian") of the Software.  Licensees are encouraged to
submit modified versions of the Software to the Custodian at www.qiWorkbench.org.
This will allow the Custodian to consider integrating such revised versions into the
version of the Software it distributes. Doing so will foster future innovation
and overall development of the bhpViewer platform.  Notwithstanding the foregoing,
the Custodian is under no obligation to include modifications or revisions in future
distributions.

This program module may have been modified by BHP Billiton Petroleum,
G&W Systems Consulting Corp or other third parties, and such portions are licensed
under the GPL.
To contact BHP Billiton about this software you can e-mail info@qiworkbench.org or
visit http://qiworkbench.org to learn more.
*/


package com.bhpBilliton.viewer2d;


import com.bhpBilliton.viewer2d.data.BhpModelReaderInterface;
import com.gwsys.seismic.io.TraceData;
import com.gwsys.seismic.util.*;
import com.gwsys.seismic.core.*;


/**
 * Title:        BHP Viewer <br><br>
 * Description:  This class interpolates the model data according to the
 *               corresponding depth array.
 *               Please refer to the documentation of TraceInterpolator
 *               in JSeismic for more detail. <br><br>
 *
 * @author Synthia Kong
 * @version 1.0
 */

public class BhpModelTraceInterpolator extends TraceInterpolator {
    private BhpModelReaderInterface _reader;
    private float _startDepth;
    private float _endDepth;

    private int _outputStart, _outputEnd;

    /**
     * Constructs a trace interpolator with specified interpolation type. <br>
     * Please refer to the JSeismic documentation for more detail.
     * @param reader the model reader of the pipeline.
     */
    public BhpModelTraceInterpolator(BhpModelReaderInterface reader) {
        _reader = reader;
        _startDepth = _reader.getMinDepth();
        _endDepth = _reader.getMaxDepth();
    }

    /**
     * Method not supported by this class. <br>
     * Please refer to the JSeismic documentation for more detail.
     */
    public void setDecimationThreshold(double v) {
        System.out.println("BhpModelTraceInterpolator does not implement setDecimationThreshold");
    }
    /**
     * Method not supported by this class. <br>
     * Please refer to the JSeismic documentation for more detail.
     */
    public double getDecimationThreshold() {
        System.out.println("BhpModelTraceInterpolator does not implement getDecimationThreshold");
        return 0;
    }

    /**
     * Sets the model reader of the pipeline.
     */
    public void setBhpModelReader(BhpModelReaderInterface reader) {
        _reader = reader;
        _startDepth = _reader.getMinDepth();
        _endDepth = _reader.getMaxDepth();
    }

    /**
     * Sets the value of the start depth.
     */
    public void setStartDepth(float d) { _startDepth = d; }
    /**
     * Sets the value of the end depth.
     */
    public void setEndDepth(float d)   { _endDepth = d; }

    /**
     * Interpolates the trace according to its depth value.
     * @param traceId the trace ID of the trace that will be interpolated.
     * @param sampleRange the range of required samples from this process.
     * @param traceDataOut the destination trace data for the trace process.
     * @return a flag indicates if the process is successful.
     */
    public boolean process(int traceId, NumberRange sampleRange, TraceData traceDataOut) {

        if (!(getParentProcessor().process(traceId, sampleRange, getTraceData())))
            return false;

        // draw the whole thing
        float depthMin = _reader.getMinDepth();
        float depthMax = _reader.getMaxDepth();
        depthMin = Math.min(depthMin, _startDepth);
        depthMax = Math.max(depthMax, _endDepth);

        _outputStart = 0;
        //_outputEnd   = (int) Math.floor(((_inputEnd-_inputStart+1) * _factor));
        _outputEnd = (int) Math.floor((depthMax - depthMin) * getResamplingFactor());
        //int inputLength = (_inputEnd - _inputStart + 1);
        int outputLength = (_outputEnd - _outputStart + 1);
        float[] inputData = getTraceData().getSamples();
        float[] depthArray = _reader.getDepthArray(traceId);
        traceDataOut.setNumAppliedSamples(outputLength);
        float[] outputData = traceDataOut.getSamples();

        int index;
		int prevIndex = 0;
		double currentDepth;
		double depthRange = depthMax - depthMin;
		double depthStep = depthRange / outputLength;
		double previousDepth = depthMin + (_outputStart - 1) * depthStep;
		int depthLoopLimit = depthArray.length - 1;
		for (int i = _outputStart; i < _outputEnd; i++) {

			currentDepth = previousDepth + depthStep;
			previousDepth = currentDepth;

			index = -1;
			if (currentDepth > depthArray[0]) {
				for (int j = prevIndex; j < depthLoopLimit; j++) {
					if (currentDepth >= depthArray[j]
							&& currentDepth < depthArray[j + 1]) {
						index = j;
						prevIndex = j;
						break;
					}
				}
			}
			if (index < 0)
				outputData[i] = 0;
			else
				outputData[i] = inputData[index];
		}

        traceDataOut.setUniqueKey(getTraceData().getUniqueKey());
        return true;
    }

}
