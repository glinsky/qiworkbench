/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bhpb.geoio.filesystems.properties;

import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

public class ModelPropertiesTest extends TestCase {

    private static final String PROPERTY1 = "property1";
    private static final String PROPERTY2 = "property2";
    private static final String PROPERTY3 = "property3";

    @Override
    public void setUp() {
    }

    @Override
    public void tearDown() {
    }

    public void testSetSelectedPropertyWithString() {
        ModelProperties modelProps = new ModelProperties(new ModelFileMetadata());
        assertNotNull(modelProps);
        modelProps.setSelectedProperty(PROPERTY1);
        assertNotNull(modelProps.getSelectedProperties());
        assertEquals(modelProps.getSelectedProperties(), PROPERTY1);
        assertEquals(1,modelProps.getNumSelectedProperties());
    }

    public void testSetSelectedPropertiesWithList() {
        ModelProperties modelProps = new ModelProperties(new ModelFileMetadata());
        assertNotNull(modelProps);
        List<String> testProperties = new ArrayList<String>();
        testProperties.add(PROPERTY1);
        testProperties.add(PROPERTY2);
        testProperties.add(PROPERTY3);
        modelProps.setSelectedProperties(testProperties);
        assertNotNull(modelProps.getSelectedProperties());
        assertEquals("modelProps.getSelectedProperties() was not equal to the concatenated, comma-delimited test property names",
                modelProps.getSelectedProperties(), PROPERTY1 + "," + PROPERTY2 + "," + PROPERTY3);
        assertEquals(3,modelProps.getNumSelectedProperties());
    }
}
