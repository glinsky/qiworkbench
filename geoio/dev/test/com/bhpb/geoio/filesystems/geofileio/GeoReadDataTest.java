/*
 * GeoIOResultTest.java
 * JUnit based test
 *
 * Created on February 13, 2008, 1:41 PM
 */

package com.bhpb.geoio.filesystems.geofileio;

import junit.framework.*;

/**
 *
 * @author folsw9
 */
public class GeoReadDataTest extends TestCase {
    
    public GeoReadDataTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
    }

    @Override
    protected void tearDown() throws Exception {
    }

    public void testSetAbnormResult() {
        GeoIOData result = new GeoReadData();
        String abnormResultString = "This is an abnormal result";
        result.setErrorMsg(abnormResultString);
        assertTrue("AbnormalSeedResult string was not equal to the expected value", abnormResultString.equals(result.getErrorMsg()));
    }    
}