/*
 * Test the XML parsers that process the self-descriptors for the various seismic
 * data formats and records and create metadata that encapsulates the information
 * found in the XML self-descriptors.
 */

package com.bhpb.geoio.datasystems;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.IOException;
import java.util.HashMap;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;
import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata;
import com.bhpb.geoio.datasystems.metadata.DataRecordMetadata;
import com.bhpb.geoio.datasystems.xmlparser.DataFormatParser;
import com.bhpb.geoio.datasystems.xmlparser.DataRecordParser;
import com.bhpb.geoio.datasystems.xmlparser.DataRecordNameSpaceParser;
import com.bhpb.geoio.filesystems.GeoIOException;

import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Gil Hansen (JUnit test)
 */
public class XmlParsersTest {
	//SEGY REV0:
    /** Results of parsing SEGYrev0_Format.xml */
    static public final String SEGY_REV0_FORMAT_METADATA = "SegyRev0FormatMetadata.txt";

    /** Results of parsing SEGYrev0_TraceBlock_Record.xml */
    static public final String SEGY_REV0_TRACE_BLOCK_METADATA = "SegyRev0TraceRecordMetadata.txt";

    /** Results of parsing SEGYrev0_BinaryHeader_StdNameSpace.xml */
    static public final String SEGY_REV0_BINARY_HEADER_NAME_SPACE = "SegyRev0BinaryHeaderNameSpace.txt";

    static public final String SEGY_REV0_TRACE_BLOCK_URI = SEGY_REV0_URI_BASE+"SEGYrev0_TraceBlock_Record.xml";

    static public final String SEGY_REV0_BINARY_HEADER_NAME_SPACE_URI = SEGY_REV0_URI_BASE+"SEGYrev0_BinaryHeader_StdNameSpace.xml";
	
	//BHP_SU
    /** Results of parsing BHP_SU_Format.xml */
    static public final String BHP_SU_FORMAT_METADATA = "BhpSUFormatMetadata.txt";
	
    static public final String BHP_SU_TRACE_BLOCK_URI = BHP_SU_URI_BASE+"BhpSU_TraceBlock_Record.xml";

    static final String baseDir = System.getProperty("basedir");
    static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;

    DataFormatParser dataFormatParser = DataFormatParser.getInstance();
    DataRecordParser dataRecordParser = DataRecordParser.getInstance();
    String actualMetadata = "";

    @Test
    public void testParseSegyRev0Format() {
        try {
            //parse the definition of the geophysical format
            dataFormatParser.parseFormatDefn(SEGY_REV0_FORMAT_URI);
            DataFormatMetadata formatMetadata = dataFormatParser.getMetadata();
            actualMetadata = formatMetadata.toString();
        } catch (GeoIOException gioe) {
            fail("IO Error parsing "+SEGY_REV0_FORMAT+"'s format XML definitions: "+ gioe.getMessage());
        }

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+SEGY_REV0_FORMAT_METADATA));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMetadata);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                assertEquals("Failed to process "+SEGY_REV0_FORMAT+"'s format's self-descriptor", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);

        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected results file: "+SEGY_REV0_FORMAT_METADATA+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing format self-descriptor: "+SEGY_REV0_FORMAT+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing format self-descriptor: "+SEGY_REV0_FORMAT+"; error message: "+e.getMessage());
        }
    }

    @Test
    @Ignore
    public void testParseSegyRev0TraceRecord() {
        try {
            dataRecordParser.parseRecordDefn(SEGY_REV0_TRACE_BLOCK_URI);
            DataRecordMetadata recordMetadata = dataRecordParser.getMetadata();
            actualMetadata = recordMetadata.toString();
        } catch (GeoIOException gioe) {
            fail("IO Error parsing "+SEGY_REV0_FORMAT+"'s trace record XML definitions: "+ gioe.getMessage());
        }

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+SEGY_REV0_TRACE_BLOCK_METADATA));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMetadata);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                assertEquals("Failed to process "+SEGY_REV0_FORMAT+"'s trace record's self-descriptor", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);

        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected results file: "+SEGY_REV0_TRACE_BLOCK_METADATA+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing format self-descriptor: "+SEGY_REV0_FORMAT+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing format self-descriptor: "+SEGY_REV0_FORMAT+"; error message: "+e.getMessage());
        }
    }

    DataRecordNameSpaceParser nameSpaceParser = DataRecordNameSpaceParser.getInstance();
    HashMap<String, String> actual2genericMap;
    String actualMap = "";

    @Test
    public void testParseSegyRev0BinaryHeaderNameSpace() {
        try {
            //parse the definition of the BinaryHeader's name space
            nameSpaceParser.parseRecordNameSpaceDefn(SEGY_REV0_BINARY_HEADER_NAME_SPACE_URI);
            HashMap<String, HashMap<String, String>> maps = nameSpaceParser.getNameSpaceMaps();
            actual2genericMap = maps.get("default");
            actualMap = nameSpaceParser.mapToString();
        } catch (GeoIOException gioe) {
            fail("IO Error parsing "+SEGY_REV0_BINARY_HEADER_ID+"'s name space XML definitions: "+ gioe.getMessage());
        }

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+SEGY_REV0_BINARY_HEADER_NAME_SPACE));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMap);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                assertEquals("Failed to process "+SEGY_REV0_BINARY_HEADER_ID+"'s name spaces's self-descriptor", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);

        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected results file: "+SEGY_REV0_BINARY_HEADER_NAME_SPACE+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing format self-descriptor: "+SEGY_REV0_BINARY_HEADER_ID+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing format self-descriptor: "+SEGY_REV0_BINARY_HEADER_ID+"; error message: "+e.getMessage());
        }
    }

    @Test
    public void testParseBhpSuFormat() {
        try {
            //parse the definition of the geophysical format
            dataFormatParser.parseFormatDefn(BHP_SU_FORMAT_URI);
            DataFormatMetadata formatMetadata = dataFormatParser.getMetadata();
            actualMetadata = formatMetadata.toString();
        } catch (GeoIOException gioe) {
            fail("IO Error parsing "+BHP_SU_FORMAT+"'s format XML definitions: "+ gioe.getMessage());
        }

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHP_SU_FORMAT_METADATA));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMetadata);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                assertEquals("Failed to process "+BHP_SU_FORMAT+"'s format's self-descriptor", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);

        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected results file: "+BHP_SU_FORMAT_METADATA+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing format self-descriptor: "+BHP_SU_FORMAT+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing format self-descriptor: "+BHP_SU_FORMAT+"; error message: "+e.getMessage());
        }
    }
}