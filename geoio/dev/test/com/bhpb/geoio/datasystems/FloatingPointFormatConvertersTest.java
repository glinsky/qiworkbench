/*
*   THE FOLLOWING ARE EXAMPLES OF IEEE AND IBM 32 BIT FLOATING POINT
*                    IEEE (HEX)      IBM (HEX)
*            0        00000000       00 000000
*           -0        80000000          N/A
*            1        3F800000       41 100000
*            2        40000000       41 200000
*            3        40400000       41 300000
*            4        40800000       41 400000
*            8        41000000       41 800000
*           15        41700000       41 F00000
*           16        41800000       42 100000
*           32        42000000       42 200000
*          -32        C2000000       C2 200000
*         -NAN        FF8XXXXX          N/A     FRACTION > 0
*         +NAN        7F8XXXXX          N/A     FRACTION > 0
*      -INFINITY      FF800000          N/A     FRACTION = 0
*      +INFINITY      7F800000          N/A     FRACTION = 0
*      -DENORMALIZE   804XXXXX          N/A     FRACTION > 0
*      +DENORMALIZE   004XXXXX          N/A     FRACTION > 0
*/

/* For algorithms, see
 * http://www.thescripts.com/forum/thread221981.html
 * http://support.microsoft.com/kb/q235856/
 * http://gcc.gnu.org/ml/gcc-patches/2002-05/msg02444.html
 * http://gcc.gnu.org/ml/gcc-patches/2002-05/msg02017.html
 * http://www.math.utah.edu/~beebe/software/ieee/
 * http://www.geo.mtu.edu/~ctyoung/cpgms/svbu.c
 * http://en.wikipedia.org/wiki/IBM_Floating_Point_Architecture
 * http://en.wikipedia.org/wiki/IEEE_754
 */

package com.bhpb.geoio.datasystems;

import junit.framework.TestCase;

/**
 * @author folsw9 (JUnit test)
 * @author hansg (test methods)
 */
public class FloatingPointFormatConvertersTest extends TestCase {
    /** bytes for a float. Note: Used as a temporary. */
    byte[] fltBytes = new byte[Float.SIZE];
    
    public void setup() {
        
    }
    
    @Override
    public void tearDown() {
        
    }
    
    public void testIBMtoIEEEconversion() {
        //IBM 0xC276A000 = -118.625
        fltBytes[0] = (byte)0xc2;
        fltBytes[1] = (byte)0x76;
        fltBytes[2] = (byte)0xa0;
        fltBytes[3] = (byte)0x00;
        
        assertTrue("Failed IBM to IEEE floating point conversion", -118.625f == FloatingPointFormatConverters._IBM2IEEEfloat(fltBytes));
        
        fltBytes[0] = (byte)0x42;
        fltBytes[1] = (byte)0x76;
        fltBytes[2] = (byte)0xa0;
        fltBytes[3] = (byte)0x00;
        
        assertTrue("Failed IBM to IEEE floating point conversion.", 118.625f == FloatingPointFormatConverters._IBM2IEEEfloat(fltBytes));
    }
    
    public void testIEEEtoIBMconversion() {
        fltBytes = FloatingPointFormatConverters._IEEEfloat2IBM(-118.625f);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 0 - expected 0xC2 but got " + Integer.toHexString(fltBytes[0]), fltBytes[0] == (byte)0xC2);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 1 - expected 0x76 but got " + Integer.toHexString(fltBytes[1]), fltBytes[1] == (byte)0x76);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 2 - expected 0xA0 but got " + Integer.toHexString(fltBytes[2]), fltBytes[2] == (byte)0xA0);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 3 - expected 0x00 but got " + Integer.toHexString(fltBytes[3]), fltBytes[3] == (byte)0x00);
        
        fltBytes = FloatingPointFormatConverters._IEEEfloat2IBM(118.625f);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 0 - expected 0x42 but got " + Integer.toHexString(fltBytes[0]), fltBytes[0] == (byte)0x42);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 1 - expected 0x76 but got " + Integer.toHexString(fltBytes[1]), fltBytes[1] == (byte)0x76);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 2 - expected 0xA0 but got " + Integer.toHexString(fltBytes[2]), fltBytes[2] == (byte)0xA0);
        assertTrue("Failed IEEE to IBM floating point conversion at byte 3 - expected 0x00 but got " + Integer.toHexString(fltBytes[3]), fltBytes[3] == (byte)0x00);
    }
    
    public void testIEEEtoIBMandBackConversion() {
        fltBytes = FloatingPointFormatConverters._IEEEfloat2IBM(0.15625f);
        float ieeeFloat = FloatingPointFormatConverters._IBM2IEEEfloat(fltBytes);
        assertTrue("Failed IEEE to IBM to IEEE round-trip conversion, expected 0.15625f but got" + ieeeFloat, 0.15625 == ieeeFloat);
    }
}