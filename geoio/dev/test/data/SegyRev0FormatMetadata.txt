
data format METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata,
 format name=SEGY,
 version=rev0,
 URL=http://www.seg.org/publications/tech-stand/seg_y_rev0.pdf,
 char encoding=EBCDIC,
 float format=IBM,
 endianess=Big Endian

 units METADATA:
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=milliseconds,
 measurement=time
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=microseconds,
 measurement=time
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=hertz,
 measurement=frequency
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=dB/octave,
 measurement=frequencySlope
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=meters,
 measurement=distance
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=feet,
 measurement=distance
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=meters,
 measurement=length
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=feet,
 measurement=length
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=meters,
 measurement=coordinate
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=feet,
 measurement=coordinate
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=secondsOfArc,
 measurement=coordinate
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=feet/sec,
 measurement=velocity
<unit> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FieldUnit,
 unit name=meters/sec,
 measurement=velocity

 data records METADATA:

<dataRecord> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FormatDataRecord,
 record name=SEGYrev0_ASCIIHeader,
 instances=1,
 data record def'n=SEGYrev0_ASCIIHeader_Record.xml,
 data record name space def'n=SEGYrev0_ASCIIHeader_StdNameSpace.xml
<data> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$RecordData,
 type=string[],
 item type=java.lang.String,
 capacity=40,
 length=80

<dataRecord> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FormatDataRecord,
 record name=SEGYrev0_BinaryHeader,
 instances=1,
 data record def'n=SEGYrev0_BinaryHeader_Record.xml,
 data record name space def'n=SEGYrev0_BinaryHeader_StdNameSpace.xml
<data> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$RecordData,
 type=byte[],
 item type=java.lang.Byte,
 capacity=400,
 length=0

<dataRecord> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$FormatDataRecord,
 record name=SEGYrev0_TraceBlock,
 instances=-1,
 data record def'n=SEGYrev0_TraceBlock_Record.xml,
 data record name space def'n=SEGYrev0_TraceBlock_StdNameSpace.xml
<data> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$RecordData,
 type=byte[],
 item type=java.lang.Byte,
 capacity=240,
 length=0
<data> METADATA:class com.bhpb.geoio.datasystems.metadata.DataFormatMetadata$RecordData,
 type=array[],
 item type=java.lang.Array,
 capacity=-1,
 length=0
