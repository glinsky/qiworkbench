/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.geoio;

import java.util.ArrayList;

import com.bhpb.qiworkbench.api.IMessagingManager;

import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.qiworkbench.api.IComponentUtils;
import com.bhpb.qiworkbench.api.IServletDispatcher;

/**
 * Interface of methods for read and writing geoData from/to a geoFile.
 * Each geophysical format implements the methods differently. For example,
 * reading a trace in BHP-SU format uses a bhpreadcube command while reading
 * a trace in SEGY rev0 format extracts the trace from a binary .segy file.
 */
public interface IGeoFileIO {
    /**
     * Get the metadata for a geoFile.
     * @param args List of input arguments:
     * <ul>
     * <li>location preference: local or remote</li>
     * <li>full path of the geoFile</li>
     * </ul>
     * @param messagingMgr MessagingManager of the geoIO Service requesting the metadata.
     * @return The geoFile's metadata.
     */
    GeoFileMetadata getFileMetadata(ArrayList<String> args, IMessagingManager messagingMgr);

    /**
     * Read a set of traces from a geoFile using socket IO.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile
     * <li>[2] full pathname of the geoFile
     * <li>[3] type of geoData to be read
     * <li>[4] Specification of data records to be read, i.e., record IDs.
     * </ul>
     * @return The empty string if all the traces were successfully read; otherwise, the
     * reason the read failed.
     */
    String readTraces(IServletDispatcher servletDispatcher, IComponentUtils componentUtils, 
            ArrayList<String> args);
}
