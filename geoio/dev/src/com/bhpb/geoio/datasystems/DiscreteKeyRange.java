/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.datasystems;

import org.w3c.dom.Node;

import static com.bhpb.geoio.datasystems.DataSystemConstants.*;

import com.bhpb.geoio.util.ElementAttributeReader;

/**
 * Range for an index key; either a full range or a chosen range.
 */
public class DiscreteKeyRange extends AbstractKeyRange {
    /**
     * Constructs a DiscreteKeyRange.
     * 
     * @param range the full key range with which this chosen DiscreteKeyRange 
     * is associated or null if this _is_ a full key range
     */
    public DiscreteKeyRange(DiscreteKeyRange range) {
        super(range.getFullKeyRange());
        this.keyType = range.getKeyType();
        this.min = range.getMin();
        this.max = range.getMax();
        this.incr = range.getIncr();
    }
    
    /**
     * Constructs a DiscreteKeyRange.
     * 
     * @param node w3c.dom.node to deserialize
     * @param fullKeyRange the full key range with which this chosen DiscreteKeyRange 
     * is associated or null if this _is_ a full key range.  This should NOT be
     * null as metadata full key ranges are created by geoIO, NOT deserialization
     */
    public DiscreteKeyRange(Node node, DiscreteKeyRange fullKeyRange) {
        super(fullKeyRange);
        this.min = ElementAttributeReader.getDouble(node, "min");
        this.max = ElementAttributeReader.getDouble(node, "max");
        this.incr = ElementAttributeReader.getDouble(node, "incr");
        this.keyType = GeoKeyType.valueOf(ElementAttributeReader.getString(node, "keyType"));
    }

     /**
     * Constructs a full (not chosen) DiscreteKeyRange.
     * 
     * @param min minimum header key value
     * @param max max header key value
     * @param incr header key increment
     * @param keyType GeoKeyType
     */
    public DiscreteKeyRange(double min, double max, double incr, GeoKeyType keyType) {
        super(null);
        this.min = min;
        this.max = max;
        this.incr = incr;
        this.keyType = keyType;
    }
    
    /**
     * Constructs a DiscreteKeyRange.
     * 
     * @param min minimum header key value
     * @param max max header key value
     * @param incr header key increment
     * @param keyType GeoKeyType
     * @param fullKeyRange fullKeyRaneg with which this DiscreteKeyRange will
     * be associated or null if this _is_ a full key range
     */
    public DiscreteKeyRange(double min, double max, double incr, GeoKeyType keyType, DiscreteKeyRange fullKeyRange) {
        super(fullKeyRange);
        this.min = min;
        this.max = max;
        this.incr = incr;
        this.keyType = keyType;
    }

    double min = 0;
    double max = 0;
    double incr = 1;
    GeoKeyType keyType = GeoKeyType.NO_TYPE;
    
    @Override
    /**
     * Returns true if and only if the Object is a non-null DiscreteKeyRange
     * with min, max, incr and KeyType equal to the values of those fields for
     * this DiscreteKeyRange.  Note that chosenRanges which reference different fullKeyRanges
     * may therefore be considered equal.
     * 
     * @param obj the Object to test for equivalency to this DiscreteKeyRange
     */
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj instanceof DiscreteKeyRange) {
            DiscreteKeyRange discreteKeyRange = (DiscreteKeyRange) obj;
            return (min == discreteKeyRange.getMin() &&
                    max == discreteKeyRange.getMax() &&
                    incr == discreteKeyRange.getIncr() &&
                    keyType == discreteKeyRange.getKeyType()
                    );
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.min) ^ (Double.doubleToLongBits(this.min) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.max) ^ (Double.doubleToLongBits(this.max) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.incr) ^ (Double.doubleToLongBits(this.incr) >>> 32));
        hash = 17 * hash + (this.keyType != null ? this.keyType.hashCode() : 0);
        return hash;
    }
        
    /**
     * Check if a discrete key range
     * @return true if discrete; otherwise, false.
     */
    @Override
    public boolean isDiscrete() {
        return true;
    }

    //GETTERS
    public double getMin() {
        return min;
    }
    public double getMax() {
        return max;
    }
    public double getIncr() {
        return incr;
    }
    public GeoKeyType getKeyType() {
        return keyType;
    }

    //SETTERS
    public void setKeyType(GeoKeyType keyType) {
        this.keyType = keyType;
    }

    public void setRange(double min, double max, double incr) {
        this.min = min;
        this.max = max;
        this.incr = incr;
        this.keyType = GeoKeyType.REGULAR_TYPE;
    }
    
    public void setRange(double min, double max, double incr, GeoKeyType keyType) {
        this.min = min;
        this.max = max;
        this.incr = incr;
        this.keyType = keyType;
    }

    /** Display an discrete key range's information */
    @Override
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append("\n Discrete Range:");
        buf.append(" min="+min);
        buf.append(", max="+max);
        buf.append(", incr="+incr);
        buf.append(", key type="+keyType);
        return buf.toString();
    }
    
    /**
     * Convert the internal representation of the key range to the representation
     * required by a generated script.
     */
    @Override
    public String toScriptString() {
        StringBuffer buf  = new StringBuffer();
        //bug fix - for compatibility with bhp-su, a range's field with a bhpread argument list
        //should contain only a single value
        if (min == max) {
            buf.append(min);
        } else {
            buf.append(min);
            buf.append("-");
            buf.append(max);
            buf.append("[");
            buf.append(incr);
            buf.append("]");
        }
        return buf.toString();
    }
    
    /**
     * Convert the internal representation of the key range to well-formed XML
     * 
     * @return XML representation of key range
     */
    @Override
    public String toXmlString() {
        StringBuffer buf  = new StringBuffer();

        buf.append("<DiscreteKeyRange ");
        buf.append("min=\""+min+"\" ");
        buf.append("max=\""+max+"\" ");
        buf.append("incr=\""+incr+"\" ");
        buf.append("keyType=\""+keyType+"\" ");
        buf.append("/>\n");

        return buf.toString();
    }
}
