/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems;
import java.io.Serializable;
import static java.lang.System.out;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import static com.bhpb.geoio.datasystems.FloatingPointFormatConverters.*;

import com.bhpb.geoio.datasystems.metadata.FieldMetadata;
import com.bhpb.qiworkbench.api.DataSystemConstants.FieldKind;
import com.bhpb.qiworkbench.api.DataSystemConstants.FloatFormat;
import com.bhpb.qiworkbench.api.DataSystemConstants.ImplType;
import com.bhpb.qiworkbench.api.DataSystemConstants.RecordType;
import com.bhpb.qiworkbench.api.GeoDataRecordAccessException;
import com.bhpb.qiworkbench.api.GeoIncompatibleTypeException;
import com.bhpb.qiworkbench.api.GeoIndexOutOfBoundsException;
import com.bhpb.qiworkbench.api.IDataObject;
import com.bhpb.qiworkbench.api.ITrace;
import com.bhpb.qiworkbench.api.ITraceHeader;

/**
 * Container and accessors for geophycial data record.
 */
public class DataObject extends Object implements Serializable, IDataObject, ITrace {
    public DataObject() {};

    public DataObject(ByteOrder byteOrder, FloatFormat floatFormat, RecordType recordType, FieldKind fieldKind, String pojoID) {
        this.byteOrder = byteOrder;
        this.floatFormat = floatFormat;
        this.recordType = recordType;
        this.fieldKind = fieldKind;
        this.pojoID = pojoID;
    }

    static final long serialVersionUID = -7053822526687357402L;
    /** Unique ID*/
    String pojoID = "";

    /** Container for data record. The default byte ordering is big indian. */
    transient ByteBuffer bbuf;

    /** Byte ordering of all multi-btye data types. 
        This is always the native byte order of the server which executed the bhpsu script
        which generated the data.  There is no meaningful client-side default value.*/
    ByteOrder byteOrder;

    /** Floating-point format of decimal numbers */
    FloatFormat floatFormat = FloatFormat.IEEE_FORMAT;

    /** Type of data record */
    RecordType recordType = RecordType.TEXT;

    /** Size of data record in bytes */
    int recordSize = -1;
    
    /** Size of data record header (in bytes) if a binary block */
    int headerSize = 0;

    /** Type of data record, i.e., binary (fields accessible by name) or
    vector (elements all of the same type, e.g., trace samples) */
    FieldKind fieldKind = FieldKind.SCALAR;

    /** bytes for a float. Note: Used as a temporary. */
    byte[] floatBytes = new byte[Float.SIZE];

    /** bytes for a double. Note: Used as a temporary. */
    byte[] doubleBytes = new byte[Double.SIZE];

    /** Byte buffer for a float. Note: Used as a temporary. */
    transient ByteBuffer floatBuf = ByteBuffer.allocate(Float.SIZE);

    /** Byte buffer for a double . Note: Used as a temporary. */
    transient ByteBuffer doubleBuf = ByteBuffer.allocate(Double.SIZE);

    /**
     * Map of a field to its metadata. Key: generic name of field; Value:
     * field's metadata.
     */
    HashMap<String, FieldMetadata> fieldMap;

	public HashMap<String, FieldMetadata> getFieldMap() {
		return fieldMap;
	}
    protected void setFieldMap(HashMap<String, FieldMetadata> fieldMap) {
        this.fieldMap = fieldMap;
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }
    
    /*byteOrder is now immutable */
    /*
    public void setByteOrder(Endian byteOrder) {
        this.byteOrder = byteOrder;
    }
    */
    
    public FloatFormat getFloatFormat() {
        return floatFormat;
    }
    public void setFloatFormat(FloatFormat floatFormat) {
        this.floatFormat = floatFormat;
    }

    public RecordType getRecordType() {
        return recordType;
    }
    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public int getRecordSize() {
        return recordSize;
    }
    public void setRecordSize(int recordSize) {
        this.recordSize = recordSize;
    }
    
    public int getHeaderSize() {
        return headerSize;
    }
    public void setHeaderSize(int headerSize) {
        this.headerSize = headerSize;
    }

    public FieldKind getFieldKind() {
        return fieldKind;
    }
    public void setFieldKind(FieldKind fieldKind) {
        this.fieldKind = fieldKind;
    }

    /** Extract the data record from its containing byte buffer. */
    public byte[] getDataRecord() {
        return bbuf.array();
    }
    /** Create the containing byte buffer for a data record */
    public void setDataRecord(byte[] drec) {
        bbuf = ByteBuffer.wrap(drec);
        recordSize = drec.length;
        //set the byte ordering of the byte buffer to that of the data
        if (byteOrder == ByteOrder.LITTLE_ENDIAN) {
            bbuf.order(ByteOrder.LITTLE_ENDIAN);
        }
        
    }
    /**
     * Create an empty data record with a given capacity.
     * @param capacity Capacity of the data record created.
     */
    public void createDataRecord(int capacity) {
        bbuf = ByteBuffer.allocate(capacity);
        //set the byte ordering of the byte buffer to that of the data
        if (byteOrder == ByteOrder.LITTLE_ENDIAN) {
            bbuf.order(ByteOrder.LITTLE_ENDIAN);
        }
    }

    /**
     * Calculate the size of the fields in the data record.
     * @return Size of the fields in the data record.
     */
    public int calcRecordSize() {
        int size = 0;
        Set<String> keys = fieldMap.keySet();
        Iterator<String> iter = keys.iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            FieldMetadata meta = fieldMap.get(key);
            size += meta.getLength();
        }
        return size;
    }

    /**
     * Get this DO's unique ID
     * @return DO's unique ID
     */
    public String getDoID() {
        return pojoID;
    }

    /**
     * Set the DO's unique ID
     * @param DO's unique ID
     */
    public void setDoID(String pojoID) {
        this.pojoID = pojoID;
    }

    /**
     * Check if thie DO is empty, i.e., contains no data
     * @reutrn true if empty; otherwise, false.
     */
    public boolean isEmpty() {
        return pojoID.equals("") ? true : false;
    }

    //GET VALUE OF A SCALAR FIELD IN THE DATA RECORD BY GENERIC NAME
//TODO: Getters throw a FieldAcessException we define.
    /**
     * Get boolean value of a scalar field by generic name
     * @param field Generic name of field
     */
    public boolean getBoolean(String fieldName) {
//TODO: Is boolean required?
        return false;
    }

    /**
     * Get char value of a scalar field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @return Field value as a single byte char
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not char.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public char getChar(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
//TODO: Handle multi-byte encodings?.
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.CHAR) {
                throw new GeoIncompatibleTypeException("type (char) mismatch: "+implType);
            }
            return (char)bbuf.get(meta.getOffset());
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get byte value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a byte.
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not byte.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public byte getByte(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.BYTE) {
                throw new GeoIncompatibleTypeException("type (byte) mismatch: "+implType);
            }
            return bbuf.get(meta.getOffset());
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get short value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a short
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not short.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public short getShort(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.SHORT) {
                throw new GeoIncompatibleTypeException("type (short) mismatch: "+implType);
            }
            return bbuf.getShort(meta.getOffset());
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get int value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as an int.
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not int.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public int getInt(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.INT) {
                throw new GeoIncompatibleTypeException("type (int) mismatch: "+implType);
            }
            return bbuf.getInt(meta.getOffset());
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get long value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a long.
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not long.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public long getLong(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.LONG) {
                throw new GeoIncompatibleTypeException("type (long) mismatch: "+implType);
            }
            return bbuf.getLong(meta.getOffset());
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get float value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a float in IEEE format
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not float.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public float getFloat(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.FLOAT) {
                throw new GeoIncompatibleTypeException("type (float) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                bbuf.get(floatBytes, meta.getOffset(), meta.getLength());
                return IBM2IEEEfloat(floatBytes);
            }
            else return bbuf.getFloat(meta.getOffset());
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get double value of a scalar field by generic name
     * @param field Generic name of field
     * @return Field value as a double in IEEE format
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not float.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public double getDouble(String fieldName) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.DOUBLE) {
                throw new GeoIncompatibleTypeException("type (double) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                bbuf.get(doubleBytes, meta.getOffset(), meta.getLength());
                return IBM2IEEEdouble(floatBytes);
            }
            else return bbuf.getDouble(meta.getOffset());
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get String value of a scalar field by generic name
     * @param field Generic name of field
     */
    public String getString(String fieldName) {
//TODO: Is String required?
        return "";
    }

    //GET VALUE OF A VECTOR FIELD IN THE DATA RECORD BY GENERIC NAME
    //THE VECTOR FIELD OCCURS AFTER THE HEADER, IF THERE IS ONE
//TODO: Getters throw a FieldAcessException we define.
    /**
     * Get boolean value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     */
    public boolean getBoolean(String fieldName, int idx) {
//TODO: Is boolean required?
        return false;
    }

    /**
     * Get char value of a vector field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a single byte char
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not char.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public char getChar(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
//TODO: Handle multi-byte encodings?.
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.CHAR) {
                throw new GeoIncompatibleTypeException("type (char) mismatch: "+implType);
            }
            return (char)bbuf.get(headerSize+meta.getOffset(idx));
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get byte value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a byte.
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not byte.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public byte getByte(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.BYTE) {
                throw new GeoIncompatibleTypeException("type (byte) mismatch: "+implType);
            }
            return bbuf.get(headerSize+meta.getOffset(idx));
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get short value of a vector field by generic name
     * @param field Generic name of record
     * @param idx Index of vector element accessing.
     * @return Field value as a short
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not short.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public short getShort(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.SHORT) {
                throw new GeoIncompatibleTypeException("type (short) mismatch: "+implType);
            }
            return bbuf.getShort(headerSize+meta.getOffset(idx));
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get int value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as an int.
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not int.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public int getInt(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.INT) {
                throw new GeoIncompatibleTypeException("type (int) mismatch: "+implType);
            }
            return bbuf.getInt(headerSize+meta.getOffset(idx));
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get long value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a long.
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not long.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public long getLong(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.LONG) {
                throw new GeoIncompatibleTypeException("type (long) mismatch: "+implType);
            }
            return bbuf.getLong(headerSize+meta.getOffset(idx));
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get float value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a float in IEEE format
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not float.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public float getFloat(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.FLOAT) {
                throw new GeoIncompatibleTypeException("type (float) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                bbuf.get(floatBytes, headerSize+meta.getOffset(idx), meta.getLength());
                return IBM2IEEEfloat(floatBytes);
            }
            else return bbuf.getFloat(headerSize+meta.getOffset(idx));
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }
	
	/**
	 * Get vector as floats
	 * @return Vector of values as floats in IEEE format; null if DO isn't
	 * a binary vector field with or without a header.
	 * @throws GeoIndexOutOfBoundsException The vector being accessed in not 
	 * within the data record.
	 */
	public float[] getFloatVector() throws GeoIndexOutOfBoundsException {
		if ((recordType != RecordType.BINARY || recordType != RecordType.BINARY_BLOCK) && fieldKind != FieldKind.VECTOR)
			return null;
		
		int i=0, j=0;
		int num = (recordSize-headerSize)/4;
		float[] vfloats = new float[num];
		try {
			for (i=0, j=headerSize; i<num; i++, j+=4) {
				vfloats[i] = bbuf.getFloat(j);
			}
			
			return vfloats;
		} catch (IndexOutOfBoundsException ioob) {
			throw new GeoDataRecordAccessException("Element "+i+" outside data record: "+ioob.getMessage());
		}
	}

    /**
     * Get double value a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @return Field value as a double in IEEE format
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not double.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public double getDouble(String fieldName, int idx) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.DOUBLE) {
                throw new GeoIncompatibleTypeException("type (double) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                bbuf.get(doubleBytes, headerSize+meta.getOffset(idx), meta.getLength());
                return IBM2IEEEdouble(floatBytes);
            }
            else return bbuf.getDouble(headerSize+meta.getOffset(idx));
        } catch (IndexOutOfBoundsException ioob) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+ioob.getMessage());
        }
    }

    /**
     * Get String value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     */
    public String getString(String fieldName, int dix) {
//TODO: Is String required?
        return "";
    }

    //SET VALUE OF A SCALAR FIELD BY GENERIC NAME

    /**
     * Set boolean value of a scalar field by generic name
     * @param field Generic name of field
     * @param val boolean value
     */
    public void setBoolean(String fieldName, boolean val) {
//TODO: Is boolean required?
    }

    /**
     * Set char value of a scalar field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @param val char value
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not char.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public void setChar(String fieldName, byte val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
//TODO: Handle multi-byte encodings?.
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.CHAR) {
                throw new GeoIncompatibleTypeException("type (char) mismatch: "+implType);
            }
            bbuf.put(meta.getOffset(), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set byte value of a scalar field by generic name
     * @param field Generic name of field
     * @param val byte value
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not byte.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public void setByte(String fieldName, byte val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.BYTE) {
                throw new GeoIncompatibleTypeException("type (byte) mismatch: "+implType);
            }
            bbuf.put(meta.getOffset(), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set short value of a scalar field by generic name
     * @param field Generic name of field
     * @param val short value
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not short.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public void setShort(String fieldName, short val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.SHORT) {
                throw new GeoIncompatibleTypeException("type (short) mismatch: "+implType);
            }
            bbuf.putShort(meta.getOffset(), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set int value of a scalar field by generic name
     * @param field Generic name of field
     * @param val Boolean value
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not int.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public void setInt(String fieldName, int val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.INT) {
                throw new GeoIncompatibleTypeException("type (int) mismatch: "+implType);
            }
            bbuf.putInt(meta.getOffset(), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set long value of a scalar field by generic name
     * @param field Generic name of field
     * @param val long value
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not long.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public void setLong(String fieldName, long val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.LONG) {
                throw new GeoIncompatibleTypeException("type (long) mismatch: "+implType);
            }
            bbuf.putLong(meta.getOffset(), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set float value of a scalar field by generic name
     * @param field Generic name of field
     * @param val float value
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not float.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public void setFloat(String fieldName, float val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.FLOAT) {
                throw new GeoIncompatibleTypeException("type (float) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                floatBytes = IEEEfloat2IBM(val);
                bbuf.put(floatBytes, meta.getOffset(), meta.getLength());
            }
            else bbuf.putFloat(meta.getOffset(), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set double value of a scalar field by generic name
     * @param field Generic name of field
     * @param val double value
	 * @throws GeoIncompatibleTypeException The type of the scalar field being 
	 * accessed is not double.
	 * @throws GeoDataRecordAccessException The scalar field being accessed is 
	 * not within the data record.
     */
    public void setDouble(String fieldName, double val) throws GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            ImplType implType = meta.getImplType();
            if (implType != ImplType.DOUBLE) {
                throw new GeoIncompatibleTypeException("type (double) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                doubleBytes = IEEEdouble2IBM(val);
                bbuf.put(doubleBytes, meta.getOffset(), meta.getLength());
            }
            else bbuf.putDouble(meta.getOffset(), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set String value of a scalar field by generic name
     * @param field Generic name of field
     * @param val String value
     */
    public void setString(String fieldName, String val) {
//TODO: Is String required?
    }

    //SET VALUE OF A VECTOR FIELD BY GENERIC NAME

    /**
     * Set boolean value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val boolean value
     */
    public void setBoolean(String fieldName, int idx, boolean val) throws GeoIndexOutOfBoundsException {
//TODO: Is boolean required?
    }

    /**
     * Set char value of a vector field by generic name.
     * Assumes encoding is 1 byte.
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val char value
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not char.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public void setChar(String fieldName, int idx, byte val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
//TODO: Handle multi-byte encodings?.
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.CHAR) {
                throw new GeoIncompatibleTypeException("type (char) mismatch: "+implType);
            }
            bbuf.put(meta.getOffset(idx), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set byte value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val byte value
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not byte.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public void setByte(String fieldName, int idx, byte val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.BYTE) {
                throw new GeoIncompatibleTypeException("type (byte) mismatch: "+implType);
            }
            bbuf.put(meta.getOffset(idx), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set short value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val short value
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not short.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public void setShort(String fieldName, int idx, short val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.SHORT) {
                throw new GeoIncompatibleTypeException("type (short) mismatch: "+implType);
            }
            bbuf.putShort(meta.getOffset(idx), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set int value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val int value
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not int.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public void setInt(String fieldName, int idx, int val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.INT) {
                throw new GeoIncompatibleTypeException("type (int) mismatch: "+implType);
            }
            bbuf.putInt(meta.getOffset(idx), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set long value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val long value
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not long.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public void setLong(String fieldName, int idx, long val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.LONG) {
                throw new GeoIncompatibleTypeException("type (long) mismatch: "+implType);
            }
            bbuf.putLong(meta.getOffset(idx), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set float value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val float value
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not float.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public void setFloat(String fieldName, int idx, float val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.FLOAT) {
                throw new GeoIncompatibleTypeException("type (float) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                floatBytes = IEEEfloat2IBM(val);
                bbuf.put(floatBytes, meta.getOffset(idx), meta.getLength());
            }
            else bbuf.putFloat(meta.getOffset(idx), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set double value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val double value
	 * @throws GeoIncompatibleTypeException The type of the vector field being 
	 * accessed is not double.
	 * @throws GeoDataRecordAccessException The vector field being accessed is 
	 * not within the data record.
	 * @throws GeoIndexOutOfBoundsException The index of the vector field being
	 * accessed is out of range.
     */
    public void setDouble(String fieldName, int idx, double val) throws GeoIndexOutOfBoundsException, GeoIncompatibleTypeException, GeoDataRecordAccessException {
        try {
            FieldMetadata meta = fieldMap.get(fieldName);
            if (meta.isScalarField())
                out.println("ACCESS ERROR: Not a vector field: "+fieldName);
            if (idx > meta.getCapacity()-1 || idx < 0)
                throw new GeoIndexOutOfBoundsException("Cannot access "+fieldName+"["+idx+"]");
            ImplType implType = meta.getImplType();
            if (implType != ImplType.DOUBLE) {
                throw new GeoIncompatibleTypeException("type (double) mismatch: "+implType);
            }
            if (floatFormat == FloatFormat.IBM_FORMAT) {
                doubleBytes = IEEEdouble2IBM(val);
                bbuf.put(doubleBytes, meta.getOffset(idx), meta.getLength());
            }
            else bbuf.putDouble(meta.getOffset(idx), val);
        } catch (BufferOverflowException boe) {
            throw new GeoDataRecordAccessException(fieldName+" field outside data record: "+boe.getMessage());
        } catch (ReadOnlyBufferException rob) {
            //should never happen
            throw new GeoDataRecordAccessException("Cannot write "+fieldName+" field to read only data record: "+rob.getMessage());
        }
    }

    /**
     * Set String value of a vector field by generic name
     * @param field Generic name of field
     * @param idx Index of vector element accessing.
     * @param val String value
     */
    public void setString(String fieldName, int idx, String val) throws GeoIndexOutOfBoundsException {
//TODO: Is String required?
    }

    /**
     * Reverse the order of the bytes.
     * @param bytes Array of bytes
     * @return Array of bytes in the reverse order
     */
    private void reverseBytes(byte[] bytes) {
        byte b;
        int k = bytes.length-1;
        for (int i=0; i<bytes.length/2; i++) {
            b = bytes[i];
            bytes[i] = bytes[k];
            bytes[k] = b;
            k--;
        }
    }

    //CHARACTER ENCODING CONVERTERS
//TODO: EBCDIC <-> ASCII required?

    //FLOATING-POINT CONVERTERS

    /**
     * Convert IBM float to IEEE float with big endian byte ordering (Java defaults).
     * @param bytes Byte array of single precision number in IBM format.
     * @return Float number in IEEE format, big endian format.
     */
    private float IBM2IEEEfloat(byte[] bytes) {
        if (byteOrder == ByteOrder.LITTLE_ENDIAN) {
            reverseBytes(bytes);
        }
        return _IBM2IEEEfloat(bytes);
    }

    /**
     * Convert IBM double to IEEE double with big endian byte ordering (Java defaults).
     * @param bytes Byte array of a double precision number in IBM format.
     * @return Double number in IEEE format, big endian format.
     */
    private double IBM2IEEEdouble(byte[] bytes) {
        if (byteOrder == ByteOrder.LITTLE_ENDIAN) {
            reverseBytes(bytes);
        }
        return _IBM2IEEEdouble(bytes);
    }

    /**
     * Convert IEEE float to IBM float with byte order of data record.
     * @param val Single precision number in IEEE format, big endian order.
     * @return Byte array of single precision number in IBM format, byte order of data record.
     */
    private byte[] IEEEfloat2IBM(float val) {
        floatBytes = _IEEEfloat2IBM(val);

        if (byteOrder == ByteOrder.LITTLE_ENDIAN) {
            reverseBytes(floatBytes);
        }

        return floatBytes;
    }

    /**
     * Convert IEEE double to IBM double with byte order of data record
     * @param val Double precision number in IEEE format, big endian order.
     * @return Byte array of a double precision number in IBM format, byte order of data record.
     */
    private byte[] IEEEdouble2IBM(double val) {
        doubleBytes = _IEEEdouble2IBM(val);

        if (byteOrder == ByteOrder.LITTLE_ENDIAN) {
            reverseBytes(doubleBytes);
        }

        return doubleBytes;
    }
    
    public ITrace getTrace() {
        return (ITrace) this;
    }
    
    public ITraceHeader getTraceHeader() {
        throw new UnsupportedOperationException("Ordinary DataObject has no Trace Header");
    }
    
    public boolean hasTrace() {
        return this instanceof ITrace;
    }
    
    public boolean hasTraceHeader() {
        return false;
    }
}