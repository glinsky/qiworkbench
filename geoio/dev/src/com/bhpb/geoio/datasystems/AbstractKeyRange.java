/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.datasystems;

/**
 * Base class for a discrete, unlimited or arbitray traverse key range.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public abstract class AbstractKeyRange {
    /* if null, this AbstractKeyRange is a full range,
       otherise, this is a reference to the full range associated with this chosen range */
    private DiscreteKeyRange fullKeyRange;
    
    /**
     * Constructs an AbstractKeyRange.
     * 
     * @param fullKeyRange null if this is a full AbstractKeyRange, or else the full DiscreteKeyRange
     * from which this AbstractKeyRange has been chosen
     */
    public AbstractKeyRange(DiscreteKeyRange fullKeyRange) {
        this.fullKeyRange = fullKeyRange;
    }
    
    /**
     * Gets the full keyRange with which this key range is associated.  If non-null,
     * this is a chosen key range.  If null, this is a full key range.
     * 
     * @return if this is a full key range, null, otherwise, the full key range
     * associated with this chosen key range
     */
    public DiscreteKeyRange getFullKeyRange() {
        return fullKeyRange;
    }
    
    /**
     * The key associated with this range
     */
    String key = "";

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Check if a discrete key range
     * @return true if discrete; otherwise, false.
     */
    public boolean isDiscrete() {
        return false;
    }

    /**
     * Check if an unlimited key range
     * @return true if unlimited; otherwise, false.
     */
    public boolean isUnlimited() {
        return false;
    }

    /**
     * Check if an arbitrary traverse key range
     * @return true if an arbitrary traverse; otherwise, false.
     */
    public boolean isArbTrav() {
        return false;
    }

    /**
     * Check if an list key range
     * @return true if a list; otherwise, false.
     */
    public boolean isList() {
        return false;
    }

    /**
     * Convert the internal representation of the key range to the representation
     * required by a generated script.
     * @return Script representation of key range.
     */
    abstract public String toScriptString();

    /**
     * Convert the internal represetation of the key range to XML. Used when
     * saving state.
     * @return XML representation of key range
     */
    abstract public String toXmlString();
}
