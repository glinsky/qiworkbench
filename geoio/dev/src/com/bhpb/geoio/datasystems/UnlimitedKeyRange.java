/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.datasystems;

/**
 * An unlimited key range.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class UnlimitedKeyRange extends AbstractKeyRange {
    
    /**
     * Constructs an UnlimitedKeyRange.
     * 
     * @param fullKeyRange full key range referenced by this unlimited key range (*) -
     * should never be null.
     */
    public UnlimitedKeyRange(DiscreteKeyRange fullKeyRange) {
        super(fullKeyRange);
    }
    
    /**
     * Check if an unlimited key range
     * @return true if unlimited; otherwise, false.
     */
    @Override
    public boolean isUnlimited() {
        return true;
    }
    
    /**
     * Convert the internal representation of a key range to the representation
     * required by a generated script;
     */
    @Override
    public String toScriptString() {
        return "*";
    }

    /**
     * Convert the internal represetation of the key range to XML. Used when
     * saving state.
     * @return XML representation of key range
     */
    @Override
    public String toXmlString() {
        StringBuffer buf  = new StringBuffer();
        buf.append("<UnlimitedKeyRange ");
        buf.append("range=\""+"*"+"\" ");
        buf.append("/>\n");

        return buf.toString();
    }
}