/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems;

import java.util.HashMap;
import java.util.logging.Logger;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;
import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata;
import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata.FormatDataRecord;
import com.bhpb.geoio.datasystems.metadata.DataRecordMetadata;

import com.bhpb.geoio.datasystems.xmlparser.DataFormatParser;
import com.bhpb.geoio.datasystems.xmlparser.DataRecordParser;
import com.bhpb.geoio.datasystems.xmlparser.DataRecordNameSpaceParser;
import com.bhpb.geoio.filesystems.GeoIOException;

/**
 * Initialize the geoIO library. Maintain the IO format descriptors.
 */
public class GeoIO {
    private static Logger logger = Logger.getLogger(GeoIO.class.getName());

    /**
     * Format descriptors for each defined geophysical IO format. Key is
     * the name of the IO format; value is the format descriptor.
     */
    HashMap<String, GeoFormatDesc> formatDescs = new HashMap<String, GeoFormatDesc>();

    /**
     * Get the descriptor for the IO format.
     * @param ioFormatName Name of the IO format.
     * @return The IO format's descriptor.
     */
    public GeoFormatDesc getFormatDesc(String ioFormatName) {
        return formatDescs.get(ioFormatName);
    }

    private static GeoIO singleton = null;

    private GeoIO() {};

    /**
     * Get the singleton instance of this class. If the class doesn't
     * exist, create it.
     */
    public static GeoIO getInstance() {
        if (singleton == null) {
            singleton = new GeoIO();
        }

        return singleton;
    }

    /**
     * Initialize the IO for a geophysical format. If the geophysical format has
     * already been initialized for IO, return. Also, wrap the metadata for the
     * format in a descriptor. Non-valid formats are ignored.
     * @param ioFormatName The name of the geophysical format
     * @return true if geophysical format successfully initialized for IO; otherwise,
     * false.
     */
    public boolean initFormatIO(String ioFormatName) {
        GeoFormatDesc formatDesc = formatDescs.get(ioFormatName);
        //Exit if format has already been initialized.
        if (formatDesc != null) return true;

        formatDesc = new GeoFormatDesc(ioFormatName);
        DataFormatParser dataFormatParser = DataFormatParser.getInstance();
        DataRecordParser dataRecordParser = DataRecordParser.getInstance();
        DataRecordNameSpaceParser nameSpaceParser = DataRecordNameSpaceParser.getInstance();
        if (ioFormatName.equals(SEGY_REV0_FORMAT)) {
            try {
//System.out.println("PARSING: "+SEGY_REV0_FORMAT_URI);
                //parse the definition of the geophysical format
                dataFormatParser.parseFormatDefn(SEGY_REV0_FORMAT_URI);
                DataFormatMetadata formatMetadata = dataFormatParser.getMetadata();
                formatDesc.setFormatMetadata(formatMetadata);
//                logger.info(formatMetadata.toString());

                //parse the definition(s) of the format's data records
                FormatDataRecord fdr = formatMetadata.getFormatDataRecord(SEGY_REV0_ASCII_HEADER_ID);
                String dataRecordURI = SEGY_REV0_URI_BASE+fdr.getRecordDefn();
//System.out.println("PARSING: "+dataRecordURI);
                dataRecordParser.parseRecordDefn(dataRecordURI);
                DataRecordMetadata recordMetadata = dataRecordParser.getMetadata();
                //parse the data record's name space
                String dataRecordNameSpaceURI = SEGY_REV0_URI_BASE+fdr.getNameSpaceDefn();
//System.out.println("PARSING: "+dataRecordNameSpaceURI);
                nameSpaceParser.parseRecordNameSpaceDefn(dataRecordNameSpaceURI);
                recordMetadata.setActual2GenericMap(nameSpaceParser);
                formatDesc.putRecordMetadata(SEGY_REV0_ASCII_HEADER_ID, recordMetadata);
                //generate the field maps for each data section in the record
                recordMetadata.genFieldMaps();

                fdr = formatMetadata.getFormatDataRecord(SEGY_REV0_BINARY_HEADER_ID);
                dataRecordURI = SEGY_REV0_URI_BASE+fdr.getRecordDefn();
//System.out.println("PARSING: "+dataRecordURI);
                dataRecordParser.parseRecordDefn(dataRecordURI);
                recordMetadata = dataRecordParser.getMetadata();
                //parse the data record's name space
                dataRecordNameSpaceURI = SEGY_REV0_URI_BASE+fdr.getNameSpaceDefn();
//System.out.println("PARSING: "+dataRecordNameSpaceURI);
                nameSpaceParser.parseRecordNameSpaceDefn(dataRecordNameSpaceURI);
                recordMetadata.setActual2GenericMap(nameSpaceParser);
                formatDesc.putRecordMetadata(SEGY_REV0_BINARY_HEADER_ID, recordMetadata);
                //generate the field maps for each data section in the record
                recordMetadata.genFieldMaps();

                fdr = formatMetadata.getFormatDataRecord(SEGY_REV0_TRACE_BLOCK_ID);
                dataRecordURI = SEGY_REV0_URI_BASE+fdr.getRecordDefn();
//System.out.println("PARSING: "+dataRecordURI);
                dataRecordParser.parseRecordDefn(dataRecordURI);
                recordMetadata = dataRecordParser.getMetadata();
                //parse the data record's name space
                dataRecordNameSpaceURI = SEGY_REV0_URI_BASE+fdr.getNameSpaceDefn();
//System.out.println("PARSING: "+dataRecordNameSpaceURI);
                nameSpaceParser.parseRecordNameSpaceDefn(dataRecordNameSpaceURI);
                recordMetadata.setActual2GenericMap(nameSpaceParser);
                formatDesc.putRecordMetadata(SEGY_REV0_TRACE_BLOCK_ID, recordMetadata);
                //generate the field maps for each data section in the record
                recordMetadata.genFieldMaps();
//                logger.info(recordMetadata.toString());

                formatDescs.put(ioFormatName, formatDesc);
                return true;
            } catch (GeoIOException gioe) {
                logger.severe("IO Error parsing "+SEGY_REV0_FORMAT+"'s XML definitions: "+ gioe.getMessage());
                return false;
            }
        }
        else if (ioFormatName.equals(SEGY_REV1_FORMAT)) {
            formatDescs.put(ioFormatName, formatDesc);
            return true;
        }
        else if (ioFormatName.equals(BHP_SU_FORMAT)) {
            try {
                //parse the definition of the geophysical format
                dataFormatParser.parseFormatDefn(BHP_SU_FORMAT_URI);
                DataFormatMetadata formatMetadata = dataFormatParser.getMetadata();
                formatDesc.setFormatMetadata(formatMetadata);
//                logger.info(formatMetadata.toString());

                //parse the definition(s) of the format's data records
                FormatDataRecord fdr = formatMetadata.getFormatDataRecord(BHP_SU_TRACE_BLOCK_ID);
                String dataRecordURI = BHP_SU_URI_BASE+fdr.getRecordDefn();
//System.out.println("PARSING: "+dataRecordURI);
                dataRecordParser.parseRecordDefn(dataRecordURI);
                DataRecordMetadata recordMetadata = dataRecordParser.getMetadata();
                //parse the data record's name space
                String dataRecordNameSpaceURI = BHP_SU_URI_BASE+fdr.getNameSpaceDefn();
//System.out.println("PARSING: "+dataRecordNameSpaceURI);
                nameSpaceParser.parseRecordNameSpaceDefn(dataRecordNameSpaceURI);
                recordMetadata.setActual2GenericMap(nameSpaceParser);
                formatDesc.putRecordMetadata(BHP_SU_TRACE_BLOCK_ID, recordMetadata);
                //generate the field maps for each data section in the record
                recordMetadata.genFieldMaps();
//               logger.info(recordMetadata.toString());

                formatDescs.put(ioFormatName, formatDesc);
                return true;
            } catch (GeoIOException gioe) {
                logger.severe("IO Error parsing "+BHP_SU_FORMAT+"'s XML definitions: "+ gioe.getMessage());
                return false;
            }
        }
        if (ioFormatName.equals(SU_FORMAT)) {
            formatDescs.put(ioFormatName, formatDesc);
            return true;
        }

        //ignore invalid formats
        return false;
    }

    public static void main(String[] args) {
        GeoIO geoIO = new GeoIO();
        boolean initOK = geoIO.initFormatIO(SEGY_REV0_FORMAT);
        if (!initOK) System.out.println("Failed to init Segy rev0 geoIO. Check console output.");
        initOK = geoIO.initFormatIO(BHP_SU_FORMAT);
        if (!initOK) System.out.println("Failed to init BHP-SU geoIO. Check console output.");
    }
}
