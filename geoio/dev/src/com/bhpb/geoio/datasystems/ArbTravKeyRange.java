/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.datasystems;

import com.bhpb.geoio.util.ElementAttributeReader;
import org.w3c.dom.Node;

/**
 * An arbitray traverse key range.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class ArbTravKeyRange extends AbstractKeyRange {
    /**
     * The arbitrary traverse values
     */
    int[] arbTravs;
    
    /**
     * Constructs an ArbTravKeyRange.
     * 
     * @param arbTravs the integer header key values of the arbitrary traverse
     * @param fullKeyRange the full key range with which this chosen ArbTravKeyRange is associated
     */
    public ArbTravKeyRange(int[] arbTravs, DiscreteKeyRange fullKeyRange) {
        super(fullKeyRange);
        setArbTravs(arbTravs);
    }

    public ArbTravKeyRange(Node node, DiscreteKeyRange fullKeyRange) {
        super(fullKeyRange);
        setArbTravs(ElementAttributeReader.getString(node, "arbTrav"));
    }

    /**
     * Check if an arbitrary traverse key range
     * @return true if an arbitrary traverse; otherwise, false.
     */
    @Override
    public boolean isArbTrav() {
        return true;
    }
    
    public int getArbTravVal(int i) {
        return arbTravs[i];
    }
     
    public int[] getArbTravs() {
        return this.arbTravs;
    }
    
    public void setArbTravs(int[] arbTravs) {
        this.arbTravs = new int[arbTravs.length];
        for (int i=0; i<arbTravs.length; i++) {
            this.arbTravs[i] = arbTravs[i];
        }
    }

    private void setArbTravs(String arbTravStrings) {
        String[] arbTravSubstrings = arbTravStrings.split("[.^]");
        this.arbTravs = new int[arbTravSubstrings.length];

        for (int i = 0; i < arbTravSubstrings.length; i++) {
            arbTravs[i] = Integer.valueOf(arbTravSubstrings[i]).intValue();
        }
    }

    public int getArbTravSize() {
        return this.arbTravs.length;
    }
    
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("\n ArbTrav Range:");
        int n = arbTravs.length;
        for (int i=0; i<n; i++) {
            buf.append(arbTravs[i]);
            if (i != n-1) buf.append(",");
        }
         
        return buf.toString();
    }
    
    /**
     * Convert the internal representation of the key range to the representation
     * used to specify one key's component of an arbitrary traverse in the
     * dataset properties GUI.
     *
     * @return the String representation of a single key's arbitrary traverse component
     * of the form p1^p2^...^pN.  If this ArbTravKeyRange contains no points, then
     * the empty String is returned.
     */
    @Override
    public String toScriptString() {
        int nArbTravPoints = arbTravs.length - 1;
        if (nArbTravPoints == 0) {
            return "";
        }
        StringBuffer sbuf = new StringBuffer();
        for (int i = 0; i < nArbTravPoints - 1; i++) {
            sbuf.append(arbTravs[i]);
            sbuf.append("^");
        }
        sbuf.append(arbTravs[nArbTravPoints]);
        return sbuf.toString();
    }

    /**
     * Convert the internal represetation of the key range to XML. Used when
     * saving state.
     * @return XML representation of key range
     */
    @Override
    public String toXmlString() {
        StringBuffer buf  = new StringBuffer();
        
        String arbTrav = "";
        int n = arbTravs.length;
        for (int i=0; i<n; i++) {
            arbTrav += arbTravs[i];
            if (i != n-1) arbTrav += "^";
        }

        buf.append("<ArbTravKeyRange ");
        buf.append("arbTrav=\""+arbTrav+"\" ");
        buf.append("/>\n");

        return buf.toString();
    }
}