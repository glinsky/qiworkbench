/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems;

import java.util.HashMap;

import com.bhpb.geoio.datasystems.metadata.FieldMetadata;

/**
 * Accessor for SEGY and SU trace data which consists of a
 * trace header followed by the traces.
 */
public class SegyTraceDataObject extends DataObject {
    HashMap<String, FieldMetadata> fieldMap;

    public SegyTraceDataObject() {
        //Create the field map for a SEGY trace from its XML self-description
        //NOTE: Pass fieldMap into constructor, for only want to create it once.
        fieldMap = new HashMap<String, FieldMetadata>();

        setFieldMap(fieldMap);
    }
}
