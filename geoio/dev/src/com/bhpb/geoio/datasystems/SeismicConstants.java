/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems;

/**
 * Seismic constants for SEGY, SeismicUnix (SU) and BHP-SU
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class SeismicConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private SeismicConstants() {}

    /** Trace sample units. */
    public enum SampleUnit {TIME_UNIT, DEPTH_FEET_UNIT, DEPTH_METERS_UNIT};

    /** Trace sample formats */
    public enum SampleFormat {IBM_FLOAT4_FORMAT, IEEE_FLOAT4_FORMAT, FIXED4_FORMAT, FIXED2_FORMAT, FIXED1_FORMAT, FIXED4GAIN_FORMAT};
}
