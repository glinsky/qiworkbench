/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems;

import java.nio.ByteBuffer;

/**
 * Floating point format converters. For a definition of the IBM format,
 * see http://en.wikipedia.org/wiki/IBM_Floating_Point_Architecture.
 * For a definition of the IEEE format, see http://en.wikipedia.org/wiki/IEEE_754
 */
public final class FloatingPointFormatConverters {
    /** Byte buffer for a float. Note: Used as a temporary. */
    static ByteBuffer floatBuf = ByteBuffer.allocate(Float.SIZE/Byte.SIZE);

    /** Byte buffer for a double . Note: Used as a temporary. */
    static ByteBuffer doubleBuf = ByteBuffer.allocate(Double.SIZE/Byte.SIZE);
	
    /** bytes for a float. Note: Used as a temporary. */
    static byte[] floatBytes = new byte[Float.SIZE/Byte.SIZE];

    /** bytes for a double. Note: Used as a temporary. */
    static byte[] doubleBytes = new byte[Double.SIZE/Byte.SIZE];

    /**
     * Convert IBM float to IEEE float both with big endian byte ordering.
     * If the IBM exponent is too large, the value returned is NEGATIVE_INFINITY or
     * POSITIVE_INFINITY depending on the sign. Supports denormal numbers.
	 * The IEEE number is also returned in the input array, big endian byte order.
     *
     * @param bytes Byte array of float number in IBM format, big endian byte order.
     * @return IEEE float number, big endian byte order
     */
    static public final float _IBM2IEEEfloat(byte[] bytes) {
        //TEST CASEs
        //IEEE 0x3E200000 = 0.15625
        //IEEE 0xA2ED4000 = -118.625
        //IBM 0xC276A000 = -118.625

        //IBM floating-point number, single precision.
        int ibmSign;   //1 bit
        int ibmExp;    //7 bits
        int ibmFrac;   //24 bits

        //IEEE floating-point number, single precision.
        int ieeeSign;   //1 bit
        int ieeeExp;    //8 bits
        int ieeeFrac;   //23 bits

        //zero is the same in both formats
        if (bytes[0]==0 && bytes[1]==0 && bytes[2]==0 && bytes[3]==0) {
            return 0.0f;
		}

        ibmSign = bytes[0] & 0x80;
        ibmExp = bytes[0] & 0x7F;
		bytes[0] = 0x0;
		floatBuf = ByteBuffer.wrap(bytes);
		ibmFrac = floatBuf.getInt(0);

        float ieeeVal = (float)ibmFrac;
        floatBuf = floatBuf.putFloat(0, ieeeVal);
        ieeeFrac = floatBuf.getInt(0);
        ieeeExp = (ieeeFrac & 0x7f800000) >> 23;
        ieeeFrac = ieeeFrac & 0x7fffff;
        ieeeExp = ieeeExp + 4 * ibmExp - 280;

        if (ieeeExp > 254) {
            return ibmSign != 0 ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
        }

        //Use IEEE denormal number if exponent too small
        if (ieeeExp < 0) {
            ieeeFrac = (int)(ieeeFrac + 0x800000L) >> (-ieeeExp);
            ieeeExp = 0;
        }

        //piece together, then break it apart
		int num = (ibmSign<<1 | ieeeExp)<<23 | ieeeFrac;
		floatBuf = floatBuf.putInt(0, num);
        bytes[0] = floatBuf.get(0);
        bytes[1] = floatBuf.get(1);
        bytes[2] = floatBuf.get(2);
        bytes[3] = floatBuf.get(3);
        floatBuf = ByteBuffer.wrap(bytes);
        return floatBuf.getFloat(0);
    }

    /**
     * Convert IBM double to IEEE double both with big endian byte ordering.
     * If the IBM exponent is too large, the value returned is NEGATIVE_INFINITY or
     * POSITIVE_INFINITY depending on the sign. Supports denormal numbers.
	 * The IEEE number is also returned in the input array, big endian byte order.
	 *
     * @param bytes Byte array of a double number in IBM format, big endian byte order.
     * @return IEEE double number, big endian byte order
     */
    static public final double _IBM2IEEEdouble(byte[] bytes) {
        return 0.0;
    }

    /**
     * Convert IEEE float to IBM float both with big endian byte ordering.
     * @param ieeeVal Single precision floating-point IEEE number, big endian order.
     * @return Byte array of float number in IBM format, big endian byte order
     */
    static public final byte[] _IEEEfloat2IBM(float ieeeVal) {
        //IEEE floating-point number, single precision.
        int ieeeSign;   //1 bit
        int ieeeExp;    //8 bits
        int ieeeFrac;   //23 bits

        floatBuf = floatBuf.putFloat(0, ieeeVal);
        ieeeFrac = floatBuf.getInt(0);
		ieeeSign = floatBuf.get(0) >> 7;
        ieeeExp = ((int)ieeeFrac & 0x7f800000) >> 23;
        ieeeFrac = ieeeFrac & 0x7fffff;
		
		//Check for INFINITY and NaN
		if (ieeeExp == 255) {
			//replace with the largest number
			ieeeFrac = 0xffffff00;
			ieeeExp = 0x7f;
		} else {
			if (ieeeExp > 0)
				//add assumed digit
				//ieeeFrac = (ieeeFrac >> 1) | 0x800000;
				ieeeFrac = (ieeeFrac) | 0x800000;

			if (ieeeFrac != 0) {
				ieeeExp += 130;
				ieeeFrac = ieeeFrac >> (-ieeeExp & 3);
				ieeeExp = (ieeeExp + 3) >> 2;
				
				// (re)normalize. Only executed for denormal numbers
				while (ieeeFrac < 0x100000) {
					--ieeeExp;
					ieeeFrac <<= 4;
				}
			}
		}
		
		int num = ieeeFrac | (ieeeExp << 24) | (ieeeSign << 31);
		floatBuf = floatBuf.putInt(0, num);
        floatBytes[0] = floatBuf.get(0);
        floatBytes[1] = floatBuf.get(1);
        floatBytes[2] = floatBuf.get(2);
        floatBytes[3] = floatBuf.get(3);
		
		return floatBytes;		
    }

    /**
     * Convert IEEE double to IBM double with big endian byte ordering.
     * @param ieeeVal Double precision floating-point IEEE number, big endian order.
     * @return Byte array of double number in IBM format, big endian byte order
     */
    static public final byte[] _IEEEdouble2IBM(double ieeeVal) {
        return new byte[8];
    }
}
