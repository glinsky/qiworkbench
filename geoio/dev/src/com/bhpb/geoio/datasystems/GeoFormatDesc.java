/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems;

import java.io.Serializable;
import java.util.HashMap;

import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata;
import com.bhpb.geoio.datasystems.metadata.DataRecordMetadata;

/**
 * Descriptor for a geophysical format. Contains all of the metadata
 * associated with the format and its data records.
 */
public class GeoFormatDesc implements Serializable {
    /** Name of the geophysical format */
    String formatName = "";

    /** Metadata for the geophysical format. */
    DataFormatMetadata formatMetadata;

    /** Metadata for the format's data records */
    HashMap <String, DataRecordMetadata> recordMetadata = new HashMap <String, DataRecordMetadata>();

    public GeoFormatDesc(String ioFormatName) {
        formatName = ioFormatName;
    }

    //GETTERS
    /**
     * Get the name of the IO format
     * @return The IO format's name.
     */
    public String getFormatName() {
        return formatName;
    }

    /**
     * Get the metadata for the IO format.
     * @return The IO format's metadata.
     */
    public DataFormatMetadata getFormatMetadata() {
        return formatMetadata;
    }

    /**
     * Get the metadata for a data record in the IO format.
     * @param recordName The name of the data record.
     * @return The data record's metadata
     */
    public DataRecordMetadata getRecordMetadata(String recordName) {
        return recordMetadata.get(recordName);
    }

    //SETTERS
    public void setFormatMetadata(DataFormatMetadata formatMetadata) {
        this.formatMetadata = formatMetadata;
    }

    /**
     * Save the metadata for a data record in the IO format
     * @param recordName The name of the data record
     * @param metadata The metadata for the data reocrd
     */
    public void putRecordMetadata(String recordName, DataRecordMetadata metadata) {
        recordMetadata.put(recordName, metadata);
    }
}
