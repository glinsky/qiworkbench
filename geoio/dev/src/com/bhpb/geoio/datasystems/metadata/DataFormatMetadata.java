/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems.metadata;

import java.io.Serializable;
import java.nio.ByteOrder;
import java.util.ArrayList;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;

/**
 * Definition of a geophysical data format. Populated from
 * information in an XML self-descriptor.
 */
public class DataFormatMetadata implements Serializable {
    public DataFormatMetadata() {
    }

    /** Name of data format. */
    String name;

    /** Version of data format */
    String version;

    /** URL of data format (standard) specification */
    String specURL;

    /** Description of the data format */
    String description;

    /** Data records that can occur in geophysical data in this format. */
    ArrayList<FormatDataRecord> records;

    /** Field units */
    ArrayList<FieldUnit> units;

    //DATA ATTRIBUTES
    /** Character Encoding */
    CharEncoding charEncoding;

    /** Floating point format */
    FloatFormat floatFormat;

    /** byte order */
    ByteOrder byteOrder;

    //GETTERS
    public String getName() {
        return this.name;
    }

    public String getVersion() {
        return this.version;
    }

    public String getSpecURL() {
        return this.specURL;
    }

    public String getDesc() {
        return this.description;
    }

    public ArrayList<FormatDataRecord> getRecords() {
        return records;
    }

    public FormatDataRecord getFormatDataRecord(String recordName) {
        for (int i=0; i<records.size(); i++) {
            FormatDataRecord formatDataRecord = records.get(i);
            if (formatDataRecord.getRecordName().equals(recordName))
                return formatDataRecord;
        }
        return null;
    }

    public String getCharEncoding() {
        if (charEncoding == CharEncoding.EBCDIC) return EBCDIC_ENCODING;
        else if (charEncoding == CharEncoding.ASCII) return ASCII_ENCODING;
        if (charEncoding == CharEncoding.UNICODE) return UNICODE_ENCODING;
        return UNDEF_ENCODING;
    }

    public String getEndianess() {
        if (byteOrder == ByteOrder.BIG_ENDIAN) return BIG_ENDIAN;
        else if (byteOrder == ByteOrder.LITTLE_ENDIAN) return LITTLE_ENDIAN;
        else return UNDEF_ENDIAN;
    }

    public String getFloatFormat() {
        if (floatFormat == FloatFormat.IBM_FORMAT) return IBM_FLOAT_FORMAT;
        else if (floatFormat == FloatFormat.IEEE_FORMAT) return IEEE_FLOAT_FORMAT;
        return UNDEF_FLOAT_FORMAT;
    }

    //SETTERS
    public void setName(String name) {
        this.name = name;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setSpecURL(String specURL) {
        this.specURL = specURL;
    }

    public void setDesc(String desc) {
        this.description = desc;
    }

    public void addRecord(FormatDataRecord record) {
        this.records.add(record);
    }

    public void setCharEncoding(String charEncoding) {
        if (charEncoding.equals(EBCDIC_ENCODING))
            this.charEncoding = CharEncoding.EBCDIC;
        else if (charEncoding.equals(ASCII_ENCODING))
            this.charEncoding = CharEncoding.ASCII;
        else if (charEncoding.equals(UNICODE_ENCODING))
            this.charEncoding = CharEncoding.UNICODE;
    }

    public void setEndianess(String endianess) {
        if (endianess.equals(BIG_ENDIAN))
            this.byteOrder = ByteOrder.BIG_ENDIAN;
        if (endianess.equals(LITTLE_ENDIAN))
            this.byteOrder = ByteOrder.LITTLE_ENDIAN;
    }

    public void setFloatFormat(String floatFormat) {
        if (floatFormat.equals(IBM_FLOAT_FORMAT))
            this.floatFormat = FloatFormat.IBM_FORMAT;
        if (floatFormat.equals(IEEE_FLOAT_FORMAT))
            this.floatFormat = FloatFormat.IEEE_FORMAT;
    }

    public void setFieldUnits(ArrayList<FieldUnit> units) {
        this.units = units;
    }

    public void setDataRecords(ArrayList<FormatDataRecord> records) {
        this.records = records;
    }

    /** Display a data format's metadata */
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("\ndata format METADATA:");
        buf.append(this.getClass().toString());
        buf.append(",\n format name=");
        buf.append(this.name);
        buf.append(",\n version=");
        buf.append(this.version);
        buf.append(",\n URL=");
        buf.append(this.specURL);
        buf.append(",\n char encoding=");
        buf.append(getCharEncoding());
        buf.append(",\n float format=");
        buf.append(getFloatFormat());
        buf.append(",\n endianess=");
        buf.append(getEndianess());
        buf.append("\n\n units METADATA:");
        for (int i=0; i<units.size(); i++) {
            buf.append(units.get(i).toString());
        }
        buf.append("\n\n data records METADATA:");
        for (int i=0; i<records.size(); i++) {
            buf.append(records.get(i).toString());
        }
        return buf.toString();
    }

    public class FieldUnit {
        //A unit of measurement may be used for more than 1 measurements.
        //For example, feet is used to measure distance, length and coordinate.
        //Likewise a measurement may have many different units. For example,
        //distance can be in feet or meters, as can length and coordinate.
        //TODO: handle many-to-many mappings
        /** Name of the unit */
        String unitName;

        /** Unit measurement */
        String unitMeasurement;

        //GETTERS
        public String getUnitName() {
            return unitName;
        }

        public String getUnitMeasurement() {
            return unitMeasurement;
        }

        //SETTERS
        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public void setUnitMeasurement(String unitMeasurement) {
            this.unitMeasurement = unitMeasurement;
        }

        /** Display field <unit> metadata */
        public String toString() {
            StringBuffer buf  = new StringBuffer();
            buf.append("\n<unit> METADATA:");
            buf.append(this.getClass().toString());
            buf.append(",\n unit name=");
            buf.append(this.unitName);
            buf.append(",\n measurement=");
            buf.append(this.unitMeasurement);
            return buf.toString();
        }
    }

    /** Description of data in a record. There may be more than 1 data section. */
    public class RecordData {
        /** Name of data section. If there is no 'name' attribute, the name is 'default' */
        String name = "default";

        /**
         * Type of data section:
         * <ul>
         * <li>string[] Vector of strings
         * <li>byte[] Vector of bytes broken down by fields
         * <li>array[] Vector of items of multiple possible types.
         * </ul>
         */
        String type;

        /** Type of an item */
        String itemType = "";

        /**
         * Capacity of the vector, i.e., the max number of elements.
         * Note: For this implementation, the size is the same as the capacity.
         */
        int capacity;

        /** Length of an item in the data type. */
        int length;

        //GETTERS
        public String getName() {
            return this.name;
        }

        public String getType() {
            return this.type;
        }

        public String getItemType() {
            return this.itemType;
        }

        public int getCapacity() {
            return this.capacity;
        }

        public int getLength() {
            return this.length;
        }

        //SETTERS
        public void setName(String name) {
            this.name = name;
        }

        public void setType(String type) {
            this.type = type;
            int idx = type.indexOf("[");
            if (idx != -1) {
                String baseType = type.substring(0, idx);
                if (baseType.equals("string")) itemType = STRING_JAVA;
                else if (baseType.equals("byte")) itemType = BYTE_JAVA;
                else if (baseType.equals("array")) itemType = ARRAY_JAVA;
            }
        }

        public void setCapacity(int capacity) {
            this.capacity = capacity;
        }

        public void setLength(int length) {
            this.length = length;
        }

        /** Display <data> metadata */
        public String toString() {
            StringBuffer buf  = new StringBuffer();
            buf.append("\n<data> METADATA:");
            buf.append(this.getClass().toString());
            buf.append(",\n type=");
            buf.append(this.type);
            buf.append(",\n item type=");
            buf.append(this.itemType);
            buf.append(",\n capacity=");
            buf.append(this.capacity);
            buf.append(",\n length=");
            buf.append(this.length);
            return buf.toString();
        }
    }

    public class FormatDataRecord {
        /** Name of the data record */
        String recordName;

        /**
         * The number of times the data record can occur sequentially in
         * in a data format. -1 indicates an arbitrary number of times.
         */
        int instances;

        /** The XML file name of the self-descriptor for the data record.*/
        String dataRecordDefn;

        /** The XML file name of the self-descriptor for the data record's name space.*/
        String dataRecordNameSpaceDefn;

        /** Data sections within the data record */
        ArrayList<RecordData> dataSections = new ArrayList<RecordData>();

        //GETTERS
        public String getRecordName() {
            return recordName;
        }

        public int getInstances() {
            return this.instances;
        }

        public String getRecordDefn() {
            return dataRecordDefn;
        }

        public String getNameSpaceDefn() {
            return dataRecordNameSpaceDefn;
        }

        public RecordData getRecordData(String dataSectionName) {
            for (int i=0; i<dataSections.size(); i++) {
                RecordData dataSection = dataSections.get(i);
                if (dataSection.getName().equals(dataSectionName)) return dataSection;
            }
            return null;
        }

        //SETTERS
        public void setRecordName(String recordName) {
            this.recordName = recordName;
        }

        public void setInstances(int instances) {
            this.instances = instances;
        }

        public void setRecordDefn(String dataRecordDefn) {
            this.dataRecordDefn = dataRecordDefn;
        }

        public void setNameSpaceDefn(String dataRecordNameSpaceDefn) {
            this.dataRecordNameSpaceDefn = dataRecordNameSpaceDefn;
        }

        public void addRecordData(RecordData dataSection) {
            dataSections.add(dataSection);
        }

        /** Display a data record's metadata */
        public String toString() {
            StringBuffer buf  = new StringBuffer();
            buf.append("\n\n<dataRecord> METADATA:");
            buf.append(this.getClass().toString());
            buf.append(",\n record name=");
            buf.append(this.recordName);
            buf.append(",\n instances=");
            buf.append(this.instances);
            buf.append(",\n data record def'n=");
            buf.append(this.dataRecordDefn);
            buf.append(",\n data record name space def'n=");
            buf.append(this.dataRecordNameSpaceDefn);
            for (int i=0; i<dataSections.size(); i++) {
                buf.append(dataSections.get(i).toString());
            }
            return buf.toString();
        }
    }
}
