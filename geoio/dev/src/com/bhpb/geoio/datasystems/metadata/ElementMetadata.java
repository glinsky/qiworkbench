/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems.metadata;

import java.io.Serializable;
import java.util.ArrayList;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;

/**
 * Meta information about a vector element in a data section of a data record..
 * Note: A scalar field is represented by a <field> element in the
 * self-describing XML for the data record. For a vector field, it
 * is represented by <element>.
 */
public class ElementMetadata implements Serializable {
    /** Generic name, i.e., name independent of any standard. */
    String genericName;

    /** Acutal name, i.e., name specified in some standard. */
    String actualName;

    /** Possible types of the element */
    ArrayList<ElementType> elementTypes = new ArrayList<ElementType>();

    //Getters
    public String getGenericName() {
        return genericName;
    }

    public String getActualName() {
        return actualName;
    }

    //Setters
    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public void setActualName(String actualName) {
        this.actualName = actualName;
    }

    public void addElementType(ElementType type) {
        elementTypes.add(type);
    }

    /** Display an element's metadata */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append("<element> METADATA:");
        buf.append(this.getClass().toString());
        buf.append(",\n generic name=");
        buf.append(this.genericName);
        buf.append(",\n actual name=");
        buf.append(this.actualName);
        for (int i=0; i<elementTypes.size(); i++) {
            elementTypes.get(i).toString();
        }
        return buf.toString();
    }

    public class ElementType {
        /** Generic Data type, i.e., type specified in some standard. */
        String genericType;

        /**
         * Implementation data type, i.e., full qualified type of implementation
         * language. For example,for Java, a generic 'integer' type may be
         * implemented as a 'java.lang.Integer'. The Java type in the XML
         * self-description of a data record is mapped to an enumeration type.
         */
        ImplType implType;

        /**
         * Length in bytes of the type of an element of a vector field.
         */
        int length;

        //GETTERS
        public String getGenericType() {
            return genericType;
        }

        public ImplType getImplType() {
            return implType;
        }

        public int getLength() {
            return length;
        }

        //SETTERS
        public void setGenericType(String genericType) {
            this.genericType = genericType;
        }

        public void setImplType(String implType) {
            this.implType = mapToEnumType(implType);
        }

        public void setLength(int length) {
            this.length = length;
        }

        private ImplType mapToEnumType(String implType) {
            if (implType.equals(ARRAY_JAVA)) return ImplType.ARRAY;
            if (implType.equals(BOOLEAN_JAVA)) return ImplType.BOOLEAN;
            if (implType.equals(BYTE_JAVA)) return ImplType.BYTE;
            if (implType.equals(CHAR_JAVA)) return ImplType.CHAR;
            if (implType.equals(SHORT_JAVA)) return ImplType.SHORT;
            if (implType.equals(INT_JAVA)) return ImplType.INT;
            if (implType.equals(LONG_JAVA)) return ImplType.LONG;
            if (implType.equals(FLOAT_JAVA)) return ImplType.FLOAT;
            if (implType.equals(DOUBLE_JAVA)) return ImplType.DOUBLE;
            if (implType.equals(STRING_JAVA)) return ImplType.STRING;
            System.out.println("SYSTEM ERROR: Unknown implType: "+implType);

            return ImplType.NOTYPE;
        }

        private String mapToJavaType(ImplType implType) {
            switch (implType) {
                case ARRAY:
                    return ARRAY_JAVA;
                case BOOLEAN:
                    return BOOLEAN_JAVA;
                case BYTE:
                    return BYTE_JAVA;
                case CHAR:
                    return CHAR_JAVA;
                case SHORT:
                    return SHORT_JAVA;
                case INT:
                    return INT_JAVA;
                case LONG:
                    return LONG_JAVA;
                case FLOAT:
                    return FLOAT_JAVA;
                case DOUBLE:
                    return DOUBLE_JAVA;
                case STRING:
                    return STRING_JAVA;
                default:
                    return "Undefined type";
            }
        }

        /** Display attributes of the type */
        public String toString() {
            StringBuffer buf  = new StringBuffer();
            buf.append("\n<element> METADATA:");
            buf.append(this.getClass().toString());
            buf.append(",\n generic type=");
            buf.append(this.genericType);
            buf.append(",\n impl type=");
            buf.append(this.mapToJavaType(implType));
            buf.append(",\n length=");
            buf.append(this.length);
            return buf.toString();
        }
    }
}
