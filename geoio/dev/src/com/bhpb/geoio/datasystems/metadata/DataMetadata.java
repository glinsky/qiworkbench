/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems.metadata;

import com.bhpb.geoio.constants.BhpSuIOConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;

/**
 * Definition of geophysical data in a data record. Populated from
 * information in an XML self-descriptor.
 */
public class DataMetadata implements Serializable {
    /** Name of data section. If there is no 'name' attribute, the name is 'default' */
    String name = "default";

    /** Description stating the purpose of the data record. */
    String description;

    /**
     * Type of data section:
     * <ul>
     * <li>string[] Vector of strings
     * <li>byte[] Vector of bytes broken down by fields
     * <li>array[] Vector of items of multiple possible types.
     * </ul>
     */
    String type;

    /** Type of an item */
    String itemType = "";

    /**
     * Capacity of the vector, i.e., the max number of elements.
     * Note: For this implementation, the size is the same as the capacity.
     */
    int capacity;

    /** Byte offset of this data section in the data record. */
    int offset;

    /** Fields in this data record */
    ArrayList<FieldMetadata> fields = new ArrayList<FieldMetadata>();

    /** Map of generic field name to the field's metadata */
    HashMap<String, FieldMetadata> genericFieldMap = new HashMap<String, FieldMetadata>();
	
	/** Map of actual field name to its generic field name */
	HashMap<String, String> actual2generic = new HashMap<String, String>();

    /** Element in a vector data section of this data record */
    ElementMetadata element;

    //GETTERS
    public String getName() {
        return this.name;
    }

    public String getDesc() {
        return this.description;
    }

    public String getType() {
        return this.type;
    }

    public String getItemType() {
        return this.itemType;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public int getOffset() {
        return this.offset;
    }

    public ArrayList<FieldMetadata> getFields() {
        return fields;
    }

    /**
     * Get the field map for the fields in this data section.
     * @return The data section's field map.
     */
    public HashMap<String, FieldMetadata> getFieldMap() {
        return genericFieldMap;
    }

    public ElementMetadata element() {
        return element;
    }

    //SETTERS
    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.description = desc;
    }

    public void setType(String type) {
        this.type = type;
        int idx = type.indexOf("[");
        if (idx != -1) {
            String baseType = type.substring(0, idx);
            if (baseType.equals("string")) itemType = STRING_JAVA;
            else if (baseType.equals("byte")) itemType = BYTE_JAVA;
            else if (baseType.equals("array")) itemType = ARRAY_JAVA;
        }
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void addField(FieldMetadata field) {
       this.fields.add(field);
    }
	
	public void setActual2GenericMap(HashMap<String, String> actual2generic) {
		this.actual2generic = actual2generic;
	}

    /**
     * Generate the field map of a used generic field name to its metadata. Called
     * after a data record's self-descriptor is parsed and the data metadata
     * is created, i.e., all the fields are known.
     */
    public void genFieldMap() {
        for (int i=0; i<fields.size(); i++) {
            FieldMetadata field = fields.get(i);
            String actualName = field.getActualName();
            String genericName = field.getGenericName();
            if (genericName.equals(""))
                genericName = actual2generic.get(actualName);
            if (!genericName.equals(BhpSuIOConstants.BHP_SU_UNUSED_FIELD_NAME))
                genericFieldMap.put(genericName, field);
        }
    }

    public void setElement(ElementMetadata element) {
        this.element = element;
    }

    /** Display a data section's metadata */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append("<data> METADATA:");
        buf.append(",\n type=");
        buf.append(this.type);
        buf.append(",\n item type=");
        buf.append(this.itemType);
        buf.append(",\n capacity=");
        buf.append(this.capacity);
        buf.append(",\n offset=");
        buf.append(this.offset);

        if (fields.size() != 0) {
            buf.append("\n\n <data><fields> METADATA:");
            for (int i=0; i<fields.size(); i++) {
                buf.append(fields.get(i).toString());
            }
        }

        if (element != null) {
            buf.append("\n\n <data><element> METADATA:");
            buf.append(element.toString());
        }
        return buf.toString();
    }
}
