/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems.metadata;

import java.io.Serializable;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;

/**
 * Meta information about a scalar or vector field in a data record.
 * Note: A scalar field is represented by a <field> element in the
 * self-describing XML for the data record. For a vector field, it
 * is represented by <element>.
 */
public class FieldMetadata implements Serializable {
    /** Kind of field (scalar or vector) */
    FieldKind fieldKind;

    /** Acutal name, i.e., name specified in some standard. */
    String actualName = "";
    
    /** Generic name, i.e., name common across all standards */
    String genericName = "";

    /** Generic Data type, i.e., type specified in some standard. */
    String genericType;

    /**
     * Implementation data type, i.e., full qualified type of implementation
     * language. For example,for Java, a generic 'integer' type may be
     * implemented as a 'java.lang.Integer'. The Java type in the XML
     * self-description of a data record is mapped to an enumeration type.
     */
    ImplType implType;

    /**
     * Byte offset of field in a data record (starting at zero).
     * For a scalar field, an integer. For a vector field, the
     * base offset of the vector in the data record. Zero if the
     * entire data record is a vector..
     */
    int offset = 0;

    /**
     * Length in bytes of the type of a scalar field.
     * Or, length in bytes of the type of an element of a vector field.
     */
    int length;

    /**
     * Max capacity of a vector field, number of elements it can hold.
     * For this implementation, the size of the vector, i.e., the number
     * of actual elements it contains, is the same as the capacity.
     */
    int capacity;

    /** Text description of what field represents */
    String description;

    /** Whether or not the field is strongly recommended. true if required, false if optional. */
    boolean recommended;

    public FieldMetadata() {}

    /**
     * Constructor. Note: 'capacity' is only for a vector field. To set it, use
     * its setter.
     */
    public FieldMetadata(String desc, String recom, String actualName, String genericType,
        String implType, int offset, int length, FieldKind fieldKind) {
        this.description = desc;
        setRecommendation(recom);
        this.actualName = actualName;
        this.genericType = genericType;
        setImplType(implType);
        this.offset = offset;
        this.length = length;
        this.fieldKind = fieldKind;
    }

    //Getters
    public FieldKind getFieldKind() {
        return fieldKind;
    }

    public String getActualName() {
        return actualName;
    }
    
    public String getGenericName() {
        return genericName;
    }

    public String getGenericType() {
        return genericType;
    }

    public ImplType getImplType() {
        return implType;
    }

    public int getOffset() {
        return offset;
    }

    public int getOffset(int idx) {
        if (fieldKind == FieldKind.VECTOR) {
            return  offset + idx * length;
        }

        //ignore the idx parameter for a scalar field.
        //Note: Should never pass in an index for a scalar
        return offset;
    }

    public int getLength() {
        return length;
    }

    public int getCapacity() {
        if (fieldKind == FieldKind.VECTOR) return capacity;

        //Note: Should never ask for the capacity of a scalar.
        return length;
    }

    public String getDesc() {
        return description;
    }

    public boolean isRequired() {
        return recommended ? true : false;
    }

    public boolean isOptional() {
        return !recommended ? true : false;
    }

    //Setters
    public void setFieldKind(FieldKind fieldKind) {
        this.fieldKind = fieldKind;
    }

    public void setActualName(String actualName) {
        this.actualName = actualName;
    }
    
    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public void setGenericType(String genericType) {
        this.genericType = genericType;
    }

    public void setImplType(String implType) {
        this.implType = mapToEnumType(implType);
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setDesc(String desc) {
        this.description = desc;
    }

    /**
     * Set recommendation on providing field.
     * @parame recom 'yes' or 'no'
     */
    public void setRecommendation(String recom) {
        this.recommended = recom.toLowerCase().equals("yes") ? true : false;
    }

    public boolean isScalarField() {
        return fieldKind == FieldKind.SCALAR;
    }

    public boolean isVectorField() {
        return fieldKind == FieldKind.VECTOR;
    }

    private ImplType mapToEnumType(String implType) {
        if (implType.equals(ARRAY_JAVA)) return ImplType.ARRAY;
        if (implType.equals(BOOLEAN_JAVA)) return ImplType.BOOLEAN;
        if (implType.equals(BYTE_JAVA)) return ImplType.BYTE;
        if (implType.equals(CHAR_JAVA)) return ImplType.CHAR;
        if (implType.equals(SHORT_JAVA)) return ImplType.SHORT;
        if (implType.equals(INT_JAVA)) return ImplType.INT;
        if (implType.equals(LONG_JAVA)) return ImplType.LONG;
        if (implType.equals(FLOAT_JAVA)) return ImplType.FLOAT;
        if (implType.equals(DOUBLE_JAVA)) return ImplType.DOUBLE;
        if (implType.equals(STRING_JAVA)) return ImplType.STRING;
        System.out.println("SYSTEM ERROR: Unknown implType: "+implType);

        return ImplType.NOTYPE;
    }

    private String mapToJavaType(ImplType implType) {
        switch (implType) {
            case ARRAY:
                return ARRAY_JAVA;
            case BOOLEAN:
                return BOOLEAN_JAVA;
            case BYTE:
                return BYTE_JAVA;
            case CHAR:
                return CHAR_JAVA;
            case SHORT:
                return SHORT_JAVA;
            case INT:
                return INT_JAVA;
            case LONG:
                return LONG_JAVA;
            case FLOAT:
                return FLOAT_JAVA;
            case DOUBLE:
                return DOUBLE_JAVA;
            case STRING:
                return STRING_JAVA;
            default:
                return "Undefined type";
        }
    }

    /** Display a field's metadata */
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append("\n<field> METADATA:");
        buf.append(this.getClass().toString());
        buf.append(":\n kind of field=");
        buf.append(this.fieldKind == FieldKind.SCALAR ? "Scalar" : "Vector");
        buf.append(":\n description=");
        buf.append(this.description);
        buf.append(",\n recommended=" );
        buf.append(this.recommended ? "yes" : "no");
        buf.append(",\n actual name=");
        buf.append(this.actualName);
        buf.append(",\n generic name=");
        buf.append(this.genericName);
        buf.append(",\n generic type=");
        buf.append(this.genericType);
        buf.append(",\n impl type=");
        buf.append(this.mapToJavaType(implType));
        buf.append(",\n offset=");
        buf.append(this.offset);
        if (this.fieldKind == FieldKind.VECTOR) {
            buf.append(",\n capacity=");
            buf.append(this.capacity);
        }
        buf.append(",\n length=");
        buf.append(this.length);
        return buf.toString();
    }
}
