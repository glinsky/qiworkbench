/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.datasystems;

/**
 * A list key range.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class ListKeyRange extends AbstractKeyRange {
    /**
     * The list values
     */
    int[] listVals;

    /**
     * Constructs a ListKeyRange.
     * 
     * @param listVals integral header key values
     * @param fullKeyRange the full key range with which this chosen ListKeyRange
     * is associated - should not be null
     */
    public ListKeyRange(int[] listVals, DiscreteKeyRange fullKeyRange) {
        super(fullKeyRange);
        setListVals(listVals);
    }

    /**
     * Check if an arbitrary traverse key range
     * @return true if an arbitrary traverse; otherwise, false.
     */
    @Override
    public boolean isList() {
        return true;
    }

    public int getListVal(int i) {
        return listVals[i];
    }

    public int[] getListVals() {
        return this.listVals;
    }

    public void setListVals(int[] listVals) {
        this.listVals = new int[listVals.length];
        for (int i=0; i<listVals.length; i++) {
            this.listVals[i] = listVals[i];
        }
    }

    public int getListSize() {
        return this.listVals.length;
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("\n List Range:");
        int n = listVals.length;
        for (int i=0; i<n; i++) {
            buf.append(listVals[i]);
            if (i != n-1) buf.append(",");
        }

        return buf.toString();
    }

    /**
     * Convert the internal representation of the key range to the representation
     * required by a generated script;
     * @return Script representation of key range
     */
    @Override
    public String toScriptString() {
        StringBuffer buf = new StringBuffer();
        int n = listVals.length;
        for (int i=0; i<n; i++) {
            buf.append(listVals[i]);
            if (i != n-1) buf.append(",");
        }

        return buf.toString();
    }

    /**
     * Convert the internal represetation of the key range to XML. Used when
     * saving state.
     * @return XML representation of key range
     */
    @Override
    public String toXmlString() {
        StringBuffer buf  = new StringBuffer();

        String list = "";
        int n = listVals.length;
        for (int i=0; i<n; i++) {
            list += listVals[i];
            if (i != n-1) list += ",";
        }

        buf.append("<ListKeyRange ");
        buf.append("list=\""+list+"\" ");
        buf.append("/>\n");

        return buf.toString();
    }
}
