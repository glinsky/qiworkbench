/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.datasystems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.bhpb.geoio.constants.BhpSuIOConstants;
import com.bhpb.geoio.datasystems.metadata.DataRecordMetadata;
import com.bhpb.geoio.datasystems.metadata.FieldMetadata;
import static com.bhpb.qiworkbench.api.DataSystemConstants.*;
import com.bhpb.qiworkbench.api.GeoIncompatibleTypeException;
import com.bhpb.qiworkbench.api.GeoIndexOutOfBoundsException;
import com.bhpb.qiworkbench.api.ITraceHeader;
import java.nio.ByteOrder;

/**
 * Accessor for BHP-SU data. Note: BHP-SU is indexed SU. A SU
 * trace is the same as a SEGY trace.
 */
public class BhpSuDataObject extends DataObject implements ITraceHeader {
    public BhpSuDataObject() {
		//Get the field map for the data record contained in this data object
		//ASSUMES THE DATA RECORD IS A TRACE BLOCK
        GeoFormatDesc geoFormatDesc = GeoIO.getInstance().getFormatDesc(BHP_SU_FORMAT);
        DataRecordMetadata recordMetadata = geoFormatDesc.getRecordMetadata(BHP_SU_TRACE_BLOCK_ID);
        HashMap<String, FieldMetadata> fieldMap = recordMetadata.getFieldMap(BhpSuIOConstants.BHP_SU_TRACE_HEADER_ID);

        setFieldMap(fieldMap);
    }

    public BhpSuDataObject(ByteOrder byteOrder, FloatFormat floatFormat, RecordType recordType, FieldKind fieldKind, String pojoID) {
        super(byteOrder, floatFormat, recordType, fieldKind, pojoID);
		
		//Get the field map for the data record contained in this data object
		//ASSUMES THE DATA RECORD IS A TRACE BLOCK
        GeoFormatDesc geoFormatDesc = GeoIO.getInstance().getFormatDesc(BHP_SU_FORMAT);
        DataRecordMetadata recordMetadata = geoFormatDesc.getRecordMetadata(BHP_SU_TRACE_BLOCK_ID);
        HashMap<String, FieldMetadata> fieldMap = recordMetadata.getFieldMap(BhpSuIOConstants.BHP_SU_TRACE_HEADER_ID);

        setFieldMap(fieldMap);
    }

    @Override
    public ITraceHeader getTraceHeader() {
        return (ITraceHeader) this;
    }

    @Override
    public boolean hasTraceHeader() {
        return this instanceof ITraceHeader;
    }

    /**
     * Get a list of BHP-SU trace header field names.
     * @return List of BHP-SU trace header field names if they exist; otherwise,
     * an empty list.
     */
   public ArrayList<String> getTraceHeaderFieldNames() {
        ArrayList<String> fieldNames = new ArrayList<String>();
         
        HashMap<String, FieldMetadata> fieldMap = getFieldMap();
        Set<String> keys = fieldMap.keySet();
        Iterator<String> iter = keys.iterator();
        while (iter.hasNext()) {
            String name = iter.next();
            fieldNames.add(name);
        }
         
        return fieldNames;
    }
    
	/**
	 * Get the value of a numerical BHP-SU trace header field.
	 * @param fieldName Generic name of the trace header field
	 * @return Numerical value of the trace header field if the field is a
	 * valid scalar numerical field within the trace header; otherwise null.
	 */
    public Number getTraceHeaderFieldValue(String fieldName) {
        HashMap<String, FieldMetadata> fieldMap = getFieldMap();
		FieldMetadata fieldMetadata = fieldMap.get(fieldName);
		//Check if field name a valid field
		if (fieldMetadata == null) return null;
		
		try {
			switch (fieldMetadata.getImplType()) {
				case SHORT:
					return Short.valueOf(getShort(fieldName));
					
				case INT:
					return Integer.valueOf(getInt(fieldName));
					
				case LONG:
					return Long.valueOf(getLong(fieldName));
					
				case FLOAT:
					return new Float(getFloat(fieldName));
					
				case DOUBLE:
					return new Double(getDouble(fieldName));
				
				default:
					return null;
			}
		} catch (GeoIncompatibleTypeException gite) {
			//will never happen because calling correct accessor based on
			//implementation type
		} catch (GeoIndexOutOfBoundsException giobe) {
			//field is outside the trace header
			return null;
		}
		
		return null;
    }
}