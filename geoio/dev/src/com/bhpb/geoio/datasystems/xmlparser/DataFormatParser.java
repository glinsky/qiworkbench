/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems.xmlparser;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.xml.sax.InputSource;
import org.apache.xerces.parsers.DOMParser;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata;
import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata.FieldUnit;
import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata.FormatDataRecord;
import com.bhpb.geoio.datasystems.metadata.DataFormatMetadata.RecordData;
import com.bhpb.geoio.filesystems.GeoIOException;

/**
 * Parse the XML self-descriptor for a Geo data format and populate the information
 * in the metadata container.
 */
public class DataFormatParser {
    private static Logger logger = Logger.getLogger(DataFormatParser.class.getName());

    DataFormatMetadata formatMetadata;

    public DataFormatMetadata getMetadata() {
        return formatMetadata;
    }

    ArrayList<FieldUnit> fieldUnits;

    ArrayList<FormatDataRecord> dataRecords;

    private static DataFormatParser singleton = null;

    private DataFormatParser() {};

    private static final String ROOT_NODE = "geoDataFormat";

    private static final String DATA_FORMAT_NODE = "dataFormat";
    private static final String FORMAT_NAME_ATTR = "name";
    private static final String FORMAT_VERSION_ATTR = "version";
    private static final String FORMAT_URL_ATTR = "url";

    private static final String DESCRIPTION_NODE = "description";

    private static final String DATA_RECORDS_NODE = "dataRecords";

    private static final String DATA_ATTRIBUTES_NODE = "dataAttributes";
    private static final String DATA_CHAR_ENCODING_NODE = "charEncoding";
    private static final String DATA_FLOATING_FORMAT_NODE = "floatingFormat";
    private static final String DATA_ENDIAN_NODE = "endian";

    private static final String UNITS_NODE = "units";
    private static final String UNIT_NODE = "unit";
    private static final String UNIT_MEASUREMENT_ATTR = "measurement";

    private static final String DATA_RECORD_NODE = "dataRecord";
    private static final String RECORD_NAME_ATTR = "name";
    private static final String RECORD_INSTANCES_ATTR = "instances";

    private static final String DATA_RECORD_DEF_NODE = "dataRecordDef";
	private static final String DATA_RECORD_NAME_SPACE_DEF_NODE = "dataRecordNameSpaceDef";
    private static final String RECORD_DATA_NODE = "data";
    private static final String DATA_TYPE_NODE = "type";
    private static final String DATA_CAPACITY_NODE = "capacity";
    private static final String DATA_LENGTH_NODE = "length";

    /**
     * Get the singleton instance of this class. If the class doesn't
     * exist, create it.
     */
    public static DataFormatParser getInstance() {
        if (singleton == null) {
            singleton = new DataFormatParser();
        }

        return singleton;
    }

    /**
     * Initialize the parser. Mainly involves clearing out the results of the
     * previous parse.
     */
    private void init() {
        //create the container for the geophysical format's metadata
        formatMetadata = new DataFormatMetadata();

        //create the container for the field unit of measurements
        fieldUnits = new ArrayList<FieldUnit>();

        //create the contained for the data records defined by the geophysical format
        dataRecords = new ArrayList<FormatDataRecord>();
    }

    /**
     * Parse the geophysical format's self-describing XML file.
     * @param path URI of the XML file to parse.
     */
    public void parseFormatDefn(String path) throws GeoIOException {
        //clear out the results of the previous parse
        init();

        DOMParser parser = new DOMParser();

        try {
            InputStream in = this.getClass().getResourceAsStream(path);
            parser.parse(new InputSource(in));
            Document doc = parser.getDocument();

            walkTree(doc);
        } catch (FileNotFoundException fnfe) {
            System.out.println("parseFormatDefn: "+fnfe.getMessage());
            System.out.println("   file: "+path);
            throw new GeoIOException("DataFormatParser: Cannot find file: "+fnfe.getMessage());
        } catch (IOException ioe) {
            throw new GeoIOException("DataFormatParser: IO error: "+ioe.getMessage());
        } catch (Exception e) {
            throw new GeoIOException("DataFormatParser: Parsing error: "+e.getMessage());
        }
    }

    FieldUnit fieldUnit = null;
    FormatDataRecord dataRecord = null;
    RecordData dataSection = null;
    ArrayList<RecordData> dataSections;

    /**
     * Recursively walk the XML tree processing each node.
     * @param node Start node to begin walk.
     */
    private void walkTree(Node node) {
        NodeList children;
        NamedNodeMap attributes;
        ArrayList<String>attrNames = new ArrayList<String>();
        ArrayList<String>attrValues = new ArrayList<String>();
        String attrName, attrValue;
        String textValue;
        int intValue = 0;

        switch (node.getNodeType()) {
            case Node.DOCUMENT_NODE:
                // Document object
                walkTree(((Document)node).getDocumentElement());
                break;

            case Node.ELEMENT_NODE:
                // element + attributes
                String name = node.getNodeName();
//System.out.println("Element node: "+name);

                if (name.equals(DATA_FORMAT_NODE)) {
                    // gather information for gephysical format

                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
//System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                        if (attrName.equals(FORMAT_NAME_ATTR)) formatMetadata.setName(attrValue);
                        else if (attrName.equals(FORMAT_VERSION_ATTR)) formatMetadata.setVersion(attrValue);
                        else if (attrName.equals(FORMAT_URL_ATTR)) formatMetadata.setSpecURL(attrValue);
                    }

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(DESCRIPTION_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    formatMetadata.setDesc(textValue);
//System.out.println("    text value: "+textValue);
                }
                else if (name.equals(DATA_RECORDS_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    formatMetadata.setDataRecords(dataRecords);
                }
                else if (name.equals(DATA_ATTRIBUTES_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(DATA_CHAR_ENCODING_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    formatMetadata.setCharEncoding(textValue);
                }
                else if (name.equals(DATA_FLOATING_FORMAT_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    formatMetadata.setFloatFormat(textValue);
                }
                else if (name.equals(DATA_ENDIAN_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    formatMetadata.setEndianess(textValue);
                }
                else if (name.equals(UNITS_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    formatMetadata.setFieldUnits(fieldUnits);
                }
                else if (name.equals(UNIT_NODE)) {
                    fieldUnit = formatMetadata.new FieldUnit();

                    attributes = node.getAttributes();
                    Node attr = attributes.item(0);
                    attrName = attr.getNodeName();
                    attrValue = attr.getNodeValue();
//System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                    if (attrName.equals(UNIT_MEASUREMENT_ATTR))
                        fieldUnit.setUnitMeasurement(attrValue);
                    //get unit of measurement
                    textValue = node.getFirstChild().getNodeValue();
                    fieldUnit.setUnitName(textValue);
                    fieldUnits.add(fieldUnit);
                }
                else if (name.equals(DATA_RECORD_NODE)) {
                    dataRecord = formatMetadata.new FormatDataRecord();

                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
//System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                        if (attrName.equals(RECORD_NAME_ATTR)) dataRecord.setRecordName(attrValue);
                        else if (attrName.equals(RECORD_INSTANCES_ATTR)) {
                            try {
                                intValue = Integer.parseInt(attrValue);
                            } catch (NumberFormatException nfe) {
                                System.out.println("DATA FORMAT XML PARSER ERROR: invalid instances: "+attrValue);
                            }
                            dataRecord.setInstances(intValue);
                        }
                    }
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    dataRecords.add(dataRecord);
                }
                else if (name.equals(DATA_RECORD_DEF_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
//System.out.println("data record def="+textValue);
                    dataRecord.setRecordDefn(textValue);
                }
                else if (name.equals(DATA_RECORD_NAME_SPACE_DEF_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
//System.out.println("data record name space def="+textValue);
                    dataRecord.setNameSpaceDefn(textValue);
                }
                else if (name.equals(RECORD_DATA_NODE)) {
                    dataSection = formatMetadata.new RecordData();

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    dataRecord.addRecordData(dataSection);
                }
                else if (name.equals(DATA_TYPE_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    dataSection.setType(textValue);
                }
                else if (name.equals(DATA_CAPACITY_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA FORMAT XML PARSER ERROR: invalid capacity: "+textValue);
                    }
//System.out.println("format data record capacity="+intValue);
                    dataSection.setCapacity(intValue);
                }
                else if (name.equals(DATA_LENGTH_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA FORMAT XML PARSER ERROR: invalid lengt: "+textValue);
                    }
                    dataSection.setLength(intValue);
                }
                else if (name.equals(ROOT_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                break;

            case Node.COMMENT_NODE:
            case Node.TEXT_NODE:
            case Node.CDATA_SECTION_NODE:
                // textual data
                String text = node.getNodeValue();
                break;

            case Node.PROCESSING_INSTRUCTION_NODE:
                // processing instruction
                break;

            case Node.ENTITY_REFERENCE_NODE:
                // entity reference
                break;

            case Node.DOCUMENT_TYPE_NODE:
                // DTD declaration
                break;
        }
    }
}
