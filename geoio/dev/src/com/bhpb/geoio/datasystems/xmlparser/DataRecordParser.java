/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems.xmlparser;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.lang.NumberFormatException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;

import org.xml.sax.InputSource;
import org.apache.xerces.parsers.DOMParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.geoio.datasystems.metadata.DataMetadata;
import com.bhpb.geoio.datasystems.metadata.DataRecordMetadata;
import com.bhpb.geoio.datasystems.metadata.FieldMetadata;
import com.bhpb.geoio.datasystems.metadata.ElementMetadata;
import com.bhpb.geoio.datasystems.metadata.ElementMetadata.ElementType;
import com.bhpb.geoio.filesystems.GeoIOException;

/**
 * Parse the XML self-descriptor for a Geo data record and populate the information
 * in the metadata container.
 */
public class DataRecordParser {
    private static Logger logger = Logger.getLogger(DataRecordParser.class.getName());

    DataRecordMetadata recordMetadata;

    public DataRecordMetadata getMetadata() {
        return recordMetadata;
    }

    private static DataRecordParser singleton = null;

    private DataRecordParser() {};

    private static final String ROOT_NODE = "geoDataRecord";

    private static final String DATA_RECORD_NODE = "dataRecord";
    private static final String RECORD_NAME_ATTR = "name";
    private static final String RECORD_INSTANCES_ATTR = "instances";

    private static final String RECORD_DESCRIPTION_NODE = "description";
    private static final String DATA_FORMAT_DEF_NODE = "dataFormatDef";

    private static final String RECORD_DATA_NODE = "data";
    private static final String DATA_NAME_ATTR = "name";

    private static final String DATA_DESCRIPTION_NODE = "description";
    private static final String DATA_TYPE_NODE = "type";
    private static final String DATA_CAPACITY_NODE = "capacaity";
    private static final String DATA_OFFSET_NODE = "offset";

    private static final String DATA_FIELDS_NODE = "fields";
    private static final String FIELD_NODE = "field";
    private static final String FIELD_ACTUAL_NAME_ATTR = "actualName";
    private static final String FIELD_GENERIC_NAME_ATTR = "genericName";
    private static final String FIELD_DESCRIPTION_NODE = "description";
    private static final String FIELD_RECOMMEND_NODE = "recommed";
    private static final String FIELD_GENERIC_TYPE_NODE = "genericType";
    private static final String FIELD_IMPL_TYPE_NODE = "implType";
    private static final String FIELD_CAPACITY_NODE = "capacaity";
    private static final String FIELD_OFFSET_NODE = "offset";
    private static final String FIELD_LENGTH_NODE = "length";

    private static final String DATA_ELEMENT_NODE = "element";
    private static final String ELEMENT_GENERIC_NAME_ATTR = "genericName";
    private static final String ELEMENT_ACTUAL_NAME_ATTR = "actualName";

    private static final String ELEMENT_TYPES_NODE = "types";
    private static final String TYPE_NODE = "type";
    private static final String TYPE_GENERIC_TYPE_NODE = "genericType";
    private static final String TYPE_IMPL_TYPE_NODE = "implType";
    private static final String TYPE_LENGTH_NODE = "length";

    Stack<String> currentNode = new Stack<String>();

    /**
     * Get the singleton instance of this class. If the class doesn't
     * exist, create it.
     */
    public static DataRecordParser getInstance() {
        if (singleton == null) {
            singleton = new DataRecordParser();
        }

        return singleton;
    }

    /**
     * Initialize the parser. Mainly involves clearing out the results of the
     * previous parse.
     */
    private void init() {
        //create the container for the data record's metadata
        recordMetadata = new DataRecordMetadata();
    }

    /**
     * Parse the geophysical format's self-describing XML file.
     * @param path URI of the XML file to parse.
     */
    public void parseRecordDefn(String path) throws GeoIOException {
        //clear out the results of the previous parse
        init();

        DOMParser parser = new DOMParser();

        try {
            InputStream in = this.getClass().getResourceAsStream(path);
            parser.parse(new InputSource(in));
            Document doc = parser.getDocument();

            walkTree(doc);
        } catch (FileNotFoundException fnfe) {
            throw new GeoIOException("DataRecordParser: Cannot find file: "+fnfe.getMessage());
        } catch (IOException ioe) {
            throw new GeoIOException("DataRecordParser: IO error: "+ioe.getMessage());
        } catch (Exception e) {
            throw new GeoIOException("DataRecordParser: Parsing error: "+e.getMessage());
        }
    }

    DataMetadata dataSection = null;
    FieldMetadata fieldMetadata = null;
    ElementMetadata elementMetadata = null;
    ElementType elementType = null;

    /**
     * Recursively walk the XML tree processing each node.
     * @param node Start node to begin walk.
     */
    private void walkTree(Node node) {
        NodeList children;
        NamedNodeMap attributes;
        ArrayList<String>attrNames = new ArrayList<String>();
        ArrayList<String>attrValues = new ArrayList<String>();
        String attrName, attrValue;
        String textValue;
        int intValue = 0;

        switch (node.getNodeType()) {
            case Node.DOCUMENT_NODE:
                // Document object
                walkTree(((Document)node).getDocumentElement());
                break;

            case Node.ELEMENT_NODE:
                // element + attributes
                String name = node.getNodeName();
//System.out.println("Element node: "+name);

                if (name.equals(DATA_RECORD_NODE)) {
                    currentNode.push(name);
                    // gather information for data record

                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
//System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                        if (attrName.equals(RECORD_NAME_ATTR))
                            recordMetadata.setName(attrValue);
                        else if (attrName.equals(RECORD_INSTANCES_ATTR)) {
                            try {
                                intValue = Integer.parseInt(attrValue);
                            } catch (NumberFormatException nfe) {
                                System.out.println("DATA RECORD XML PARSER ERROR: invalid instances: "+attrValue);
                            }
                            recordMetadata.setInstances(intValue);
                        }
                    }

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(RECORD_DESCRIPTION_NODE) && currentNode.peek().equals(DATA_RECORD_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
//System.out.println("    Record description: "+textValue);
                    recordMetadata.setDesc(textValue);
                }
                else if (name.equals(DATA_FORMAT_DEF_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    recordMetadata.setFormatDefn(textValue);
                }
                else if (name.equals(RECORD_DATA_NODE)) {
                    dataSection = new DataMetadata();

                    currentNode.push(name);
                    attributes = node.getAttributes();
                    Node attr = attributes.item(0);
                    if (attr != null) {
                        attrName = attr.getNodeName();
                        attrValue = attr.getNodeValue();
//System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                        if (attrName.equals(DATA_NAME_ATTR))
                            dataSection.setName(attrValue);
                    }

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    recordMetadata.addDataSection(dataSection);
                }
                else if (name.equals(DATA_DESCRIPTION_NODE) && currentNode.peek().equals(RECORD_DATA_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
//System.out.println("    Data description: "+textValue);
                    dataSection.setDesc(textValue);
                }
                else if (name.equals(DATA_TYPE_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    dataSection.setType(textValue);
                }
                else if (name.equals(DATA_CAPACITY_NODE) && currentNode.peek().equals(RECORD_DATA_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA RECORD XML PARSER ERROR: invalid data capacity: "+textValue);
                    }
                    dataSection.setCapacity(intValue);
                }
                else if (name.equals(DATA_OFFSET_NODE) && currentNode.peek().equals(RECORD_DATA_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA RECORD XML PARSER ERROR: invalid data offset: "+textValue);
                    }
                    dataSection.setOffset(intValue);
                }
                else if (name.equals(DATA_FIELDS_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(FIELD_NODE)) {
                    fieldMetadata = new FieldMetadata();

                    if (!currentNode.peek().equals(name))
                        currentNode.push(name);
                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
//System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                        if (attrName.equals(FIELD_ACTUAL_NAME_ATTR))
                            fieldMetadata.setActualName(attrValue);
                        else if (attrName.equals(FIELD_GENERIC_NAME_ATTR))
                            fieldMetadata.setGenericName(attrValue);
                    }

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    dataSection.addField(fieldMetadata);
                }
                else if (name.equals(FIELD_DESCRIPTION_NODE) && currentNode.peek().equals(FIELD_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    fieldMetadata.setDesc(textValue);
//System.out.println("    Field description: "+textValue);
                }
                else if (name.equals(FIELD_RECOMMEND_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    fieldMetadata.setRecommendation(textValue);
                }
                else if (name.equals(FIELD_GENERIC_TYPE_NODE) && currentNode.peek().equals(FIELD_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    fieldMetadata.setGenericType(textValue);
                }
                else if (name.equals(FIELD_IMPL_TYPE_NODE) && currentNode.peek().equals(FIELD_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    fieldMetadata.setImplType(textValue);
                }
                else if (name.equals(FIELD_CAPACITY_NODE) && currentNode.peek().equals(FIELD_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA RECORD XML PARSER ERROR: invalid field capacity: "+textValue);
                    }
                    fieldMetadata.setCapacity(intValue);
                }
                else if (name.equals(FIELD_OFFSET_NODE) && currentNode.peek().equals(FIELD_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA RECORD XML PARSER ERROR: invalid field offset: "+textValue);
                    }
                    fieldMetadata.setOffset(intValue);
                }
                else if (name.equals(FIELD_LENGTH_NODE) && currentNode.peek().equals(FIELD_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA RECORD XML PARSER ERROR: invalid field length: "+textValue);
                    }
                    fieldMetadata.setLength(intValue);
                }
                else if (name.equals(DATA_ELEMENT_NODE)) {
                    elementMetadata = new ElementMetadata();

                    if (!currentNode.peek().equals(name))
                        currentNode.push(name);
                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
//System.out.println("    Attribute: "+attrName+", Value: "+attrValue);
                        if (attrName.equals(ELEMENT_GENERIC_NAME_ATTR))
                            elementMetadata.setGenericName(attrValue);
                        else if (attrName.equals(ELEMENT_ACTUAL_NAME_ATTR))
                            elementMetadata.setActualName(attrValue);
                    }

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(ELEMENT_TYPES_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                else if (name.equals(TYPE_NODE)) {
                    elementType = elementMetadata.new ElementType();

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    elementMetadata.addElementType(elementType);
                }
                else if (name.equals(TYPE_GENERIC_TYPE_NODE) && currentNode.peek().equals(DATA_ELEMENT_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    elementType.setGenericType(textValue);
                }
                else if (name.equals(TYPE_IMPL_TYPE_NODE) && currentNode.peek().equals(DATA_ELEMENT_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    elementType.setImplType(textValue);
                }
                else if (name.equals(TYPE_LENGTH_NODE) && currentNode.peek().equals(DATA_ELEMENT_NODE)) {
                    textValue = node.getFirstChild().getNodeValue();
                    try {
                        intValue = Integer.parseInt(textValue);
                    } catch (NumberFormatException nfe) {
                        System.out.println("DATA RECORD XML PARSER ERROR: invalid field length: "+textValue);
                    }
                    elementType.setLength(intValue);
                }
                else if (name.equals(ROOT_NODE)) {
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                break;

            case Node.COMMENT_NODE:
            case Node.TEXT_NODE:
            case Node.CDATA_SECTION_NODE:
                // textual data
                String text = node.getNodeValue();
                break;

            case Node.PROCESSING_INSTRUCTION_NODE:
                // processing instruction
                break;

            case Node.ENTITY_REFERENCE_NODE:
                // entity reference
                break;

            case Node.DOCUMENT_TYPE_NODE:
                // DTD declaration
                break;
        }
    }
}
