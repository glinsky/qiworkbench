/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems.xmlparser;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.lang.NumberFormatException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;
import java.util.logging.Logger;

import org.xml.sax.InputSource;
import org.apache.xerces.parsers.DOMParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.geoio.filesystems.GeoIOException;

/**
 * Parse the XML self-descriptor for the name space of a Geo data record and
 * populate the information in a hash map.
 */
public class DataRecordNameSpaceParser {
    private static Logger logger = Logger.getLogger(DataRecordNameSpaceParser.class.getName());

    HashMap<String, String> actual2generic;

    /** Map of data section name to its actual2generic map */
    HashMap<String, HashMap<String, String>> nameSpaceMaps;

    String nameSpaceName = "";
    String nameSpaceVersion = "";

    /** Name of data section. If there is no 'name' attribute, the name is 'default' */
    String sectionName = "default";

    private static DataRecordNameSpaceParser singleton = null;

    private DataRecordNameSpaceParser() {};

    private static final String ROOT_NODE = "geoNameSpace";
    private static final String NAME_SPACE_NAME_ATTR = "name";
    private static final String NAME_SPACE_VERSION_ATTR = "version";

    private static final String FIELDS_NODE = "fields";
    private static final String DATA_SECTION_NAME_ATTR = "name";

    private static final String FIELD_NODE = "field";
    private static final String GENERIC_NAME_ATTR = "genericName";
    private static final String ACTUAL_NAME_ATTR = "actualName";

    //GETTERS
    public String getNameSpaceName() {
        return nameSpaceName;
    }

    public String getNameSpaceVersion() {
        return nameSpaceVersion;
    }

    public HashMap<String, HashMap<String, String>> getNameSpaceMaps() {
        return nameSpaceMaps;
    }

    /**
     * Get the singleton instance of this class. If the class doesn't
     * exist, create it.
     */
    public static DataRecordNameSpaceParser getInstance() {
        if (singleton == null) {
            singleton = new DataRecordNameSpaceParser();
        }

        return singleton;
    }

    /**
     * Initialize the parser. Mainly involves clearing out the results of the
     * previous parse.
     */
    private void init() {
        nameSpaceName = "";
        nameSpaceVersion = "";
        nameSpaceMaps = new HashMap<String, HashMap<String, String>>();
    }

    /**
     * Parse the geophysical record's name space self-describing XML file.
     * @param path URI of the XML file to parse.
     */
    public void parseRecordNameSpaceDefn(String path) throws GeoIOException {
        //clear out the results of the previous parse
        init();

        DOMParser parser = new DOMParser();

        try {
            InputStream in = this.getClass().getResourceAsStream(path);
            parser.parse(new InputSource(in));
            Document doc = parser.getDocument();

            walkTree(doc);
        } catch (FileNotFoundException fnfe) {
            System.out.println("parseRecordNameSpaceDefn: "+fnfe.getMessage());
            System.out.println("   file: "+path);
            throw new GeoIOException("DataRecordNameSpaceParser: Cannot find file: "+fnfe.getMessage());
        } catch (IOException ioe) {
            throw new GeoIOException("DataRecordNameSpaceParser: IO error: "+ioe.getMessage());
        } catch (Exception e) {
            throw new GeoIOException("DataRecordNameSpaceParser: Parsing error: "+e.getMessage());
        }
    }

    String genericName = "";
    String actualName = "";

    /**
     * Recursively walk the XML tree processing each node.
     * @param node Start node to begin walk.
     */
    private void walkTree(Node node) {
        NodeList children;
        NamedNodeMap attributes;
        ArrayList<String>attrNames = new ArrayList<String>();
        ArrayList<String>attrValues = new ArrayList<String>();
        String attrName, attrValue;
        String textValue;
        int intValue = 0;

        switch (node.getNodeType()) {
            case Node.DOCUMENT_NODE:
                // Document object
                walkTree(((Document)node).getDocumentElement());
                break;

            case Node.ELEMENT_NODE:
                // element + attributes
                String name = node.getNodeName();
//System.out.println("Element node: "+name);

                if (name.equals(FIELDS_NODE)) {
                    //create the map from the actual name of a field to its generic name
                    actual2generic = new HashMap<String, String>();
                    sectionName = "default";

                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
                        if (attrName.equals(DATA_SECTION_NAME_ATTR))
                            sectionName = attrValue;
                    }

                    // gather information for each field in the data section
                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }

                    nameSpaceMaps.put(sectionName, actual2generic);
                }
                else if (name.equals(FIELD_NODE)) {
                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
                        if (attrName.equals(GENERIC_NAME_ATTR))
                            genericName = attrValue;
                        else if (attrName.equals(ACTUAL_NAME_ATTR))
                            actualName = attrValue;
                    }

                    actual2generic.put(actualName, genericName);
                }
                else if (name.equals(ROOT_NODE)) {
                    attributes = node.getAttributes();
                    attrNames.clear(); attrValues.clear();
                    // gather <group> attributes. NOTE: they can be in any order
                    for (int i=0; i<attributes.getLength(); i++) {
                        Node attr = attributes.item(i);
                        attrName = attr.getNodeName();
                        attrNames.add(attrName);
                        attrValue = attr.getNodeValue();
                        attrValues.add(attrValue);
                        if (attrName.equals(NAME_SPACE_NAME_ATTR))
                            nameSpaceName = attrValue;
                        else if (attrName.equals(NAME_SPACE_VERSION_ATTR))
                            nameSpaceVersion = attrValue;
                    }

                    children = node.getChildNodes();
                    if (children != null) {
                        for (int i=0; i<children.getLength(); i++) {
                            Node child = children.item(i);
//System.out.println("    Process child "+child.getNodeName());
                            walkTree(child);
                        }
                    }
                }
                break;

            case Node.COMMENT_NODE:
            case Node.TEXT_NODE:
            case Node.CDATA_SECTION_NODE:
                // textual data
                String text = node.getNodeValue();
                break;

            case Node.PROCESSING_INSTRUCTION_NODE:
                // processing instruction
                break;

            case Node.ENTITY_REFERENCE_NODE:
                // entity reference
                break;

            case Node.DOCUMENT_TYPE_NODE:
                // DTD declaration
                break;
        }
    }

    /** Display a data record's name space */
    public String mapToString() {
        StringBuffer buf  = new StringBuffer();
        buf.append("\n"+nameSpaceName+":");

        Set<String> keys = actual2generic.keySet();
        Iterator<String> iter = keys.iterator();
        String[] results = new String[keys.size()];
        int i=0;
        while (iter.hasNext()) {
            String key = iter.next();
            String val = actual2generic.get(key);
            results[i] = "actual name="+key+"; generic name="+val;
            i++;
        }
        java.util.Arrays.sort(results);
        for (int j=0; j<results.length; j++) {
            buf.append(",\n "+results[j]);
        }
        return buf.toString();
    }
}
