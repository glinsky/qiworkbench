/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.datasystems;


import java.nio.ByteOrder;
import static com.bhpb.qiworkbench.api.DataSystemConstants.*;

/**
 * Accessor for BHP-SU horizon data. For cross-section horizons, only the vector 
 * of time values is carried. The ep and cdp values, in the first two columens 
 * of a .xyz file, are discarded, for they are not required by the qiViewer. For
 * map view horizons, the trace block is carried. The trace header is needed for
 * annotations and the traces for the horizon.
 */
public class BhpSuHorizonDataObject extends BhpSuDataObject {
    String horizonName = "";

    public BhpSuHorizonDataObject() {
        super();
    }

    public BhpSuHorizonDataObject(ByteOrder byteOrder, FloatFormat floatFormat, RecordType recordType, FieldKind fieldKind, String pojoID) {
        super(byteOrder, floatFormat, recordType, fieldKind, pojoID);
    }

    public void setHorizonName(String horizonName) {
        this.horizonName = horizonName;
    }

    public String getHorizonName() {
        return this.horizonName;
    }
}
