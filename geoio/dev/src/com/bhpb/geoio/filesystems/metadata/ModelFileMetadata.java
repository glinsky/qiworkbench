/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.filesystems.metadata;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Metadata about a model geoFile.
 */
public class ModelFileMetadata extends GeoFileMetadata implements Serializable {
    /** number of layers */
    int numLayers = 0;

    /** model properties */
    ArrayList<String> properties = new ArrayList<String>();

    //GETTERS
    public int getNumLayers() {
        return numLayers;
    }

    public int getNumProps() {
        return properties.size();
    }

    public ArrayList<String> getProperties() {
        return properties;
    }

    //SETTERS
    public void setNumLayers(int numLayers) {
        this.numLayers = numLayers;
    }

    public void setProperties(ArrayList<String> properties) {
        this.properties = properties;
    }
    public void addProperty(String property) {
        properties.add(property);
    }

    /** Display a model geoFile's metadata */
    @Override
    public String toString() {
        String baseMetadata = super.toString();

        StringBuffer buf  = new StringBuffer();
        buf.append("\n <model file> METADATA:");
        buf.append("\n  number of layers=");
        buf.append(this.numLayers);
        buf.append(",\n  number of properties=");
        buf.append(getNumProps());
        buf.append(",\n  Properties:");
        for (int i=0; i<properties.size(); i++) {
            buf.append(" "+properties.get(i));
        }

        return baseMetadata + buf.toString();
    }
}
