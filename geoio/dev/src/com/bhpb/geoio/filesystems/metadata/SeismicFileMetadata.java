/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.filesystems.metadata;

import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Metadata about a seismic geoFile.
 */
public class SeismicFileMetadata extends GeoFileMetadata implements Serializable {
    /** seismic horizons */
    ArrayList<String> horizons = new ArrayList<String>();

    public SeismicFileMetadata() {
    }

    public SeismicFileMetadata(String geoFilePath,
            String geoFileName,
            GeoFileType geoFileType,
            GeoDataOrder geoDataOrder,
            ArrayList<String> headerKeys,
            HashMap<String, DiscreteKeyRange> keyRangeMap,
            String geoFileSuffix,
            ArrayList<String> horizons) {
        super(geoFilePath,
                geoFileName,
                geoFileType,
                geoDataOrder,
                headerKeys,
                keyRangeMap,
                geoFileSuffix);
        this.horizons = horizons;
    }

    //GETTERS
    public int getNumHorizons() {
        return horizons.size();
    }

    public ArrayList<String> getHorizons() {
        return horizons;
    }

    //SETTERS

    public void setHorizons(ArrayList<String> horizons) {
        this.horizons = horizons;
    }

    public void addHorizon(String horizon) {
        horizons.add(horizon);
    }

    /** Display a seismic geoFile's metadata */
    public String toString() {
        String baseMetadata = super.toString();

        StringBuffer buf  = new StringBuffer();
        buf.append("\n <seismic file> METADATA:");
        buf.append(",\n  number of horizons=");
        buf.append(getNumHorizons());
        buf.append(",\n  Horizons:");
        for (int i=0; i<horizons.size(); i++) {
            buf.append(" "+horizons.get(i));
        }

        return baseMetadata + buf.toString();
    }
}
