/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#                                                                       
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.filesystems.metadata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import java.nio.ByteOrder;
import static com.bhpb.geoio.datasystems.DataSystemConstants.*;
import static com.bhpb.geoio.filesystems.FileSystemConstants.*;

/**
 * Metadata about a geoFile.
 */
public abstract class GeoFileMetadata extends Object implements Serializable {
    /** Type of geoFile */
    GeoFileType geoFileType = GeoFileType.UNKNOWN_GEOFILE;

    /** Name of geoFile (without file suffix) */
    String geoFileName = "";

    /** Full path of the geoFile. */
    String geoFilePath = "";

    /** Extension of the geoFile */
    String geoFileSuffix = ".dat";

    /** Index keys. The last key is considered the available vertical range. */
    ArrayList<String> keys = new ArrayList<String>();

    /** Full key ranges */
    HashMap<String, DiscreteKeyRange> fullKeyRanges = new HashMap<String, DiscreteKeyRange>();

    /** Order geoData stored in */
    GeoDataOrder geoDataOrder = GeoDataOrder.UNKNOWN_ORDER;

    /** Endianess of data */
    ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
    
    /** If failed to obtain the metadata, why */
    String errMsg = "";

    public GeoFileMetadata() {
    }

    public GeoFileMetadata(String geoFilePath,
            String geoFileName,
            GeoFileType geoFileType,
            GeoDataOrder geoDataOrder,
            ArrayList<String> headerKeys,
            HashMap<String, DiscreteKeyRange> keyRangeMap,
            String geoFileSuffix) {
        this.geoFilePath = geoFilePath;
        this.geoFileName = geoFileName;
        this.geoFileType = geoFileType;
        this.geoDataOrder = geoDataOrder;
        keys = headerKeys;
        fullKeyRanges = keyRangeMap;
        this.geoFileSuffix = geoFileSuffix;
    }

    //GETTERS
    public GeoFileType getGeoFileType() {
        return geoFileType;
    }

    public boolean isSeismicFile() {
        return geoFileType == GeoFileType.SEISMIC_GEOFILE;
    }

    public boolean isModelFile() {
        return geoFileType == GeoFileType.MODEL_GEOFILE;
    }

    public boolean isHorizonFile() {
        return geoFileType == GeoFileType.EVENT_GEOFILE;
    }

    public String getGeoFileName() {
        return geoFileName;
    }

    public String getGeoFilePath() {
        return geoFilePath;
    }

    public String getGeoFileSuffix() {
        return geoFileSuffix;
    }

    /** Get list of index keys */
    public ArrayList<String> getKeys() {
        return keys;
    }
    public int getNumKeys() {
        return keys.size();
    }

    /** Get the range for an index key */
    public DiscreteKeyRange getKeyRange(String key) {
        return fullKeyRanges.get(key);
    }

    public GeoDataOrder getGeoDataOrder() {
        return geoDataOrder;
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }
    
    public String getErrMsg() {
        return errMsg;
    }
    
    public boolean isValidMetadata() {
        return errMsg.equals("") ? true : false;
    }

    /**
     * Similar to toString(), but only serializes the GeoFileName, which
     * is sufficient to recreate the metadata using geoIO.
     * 
     * @return String representation of the name of the GeoFile.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();
        String className = this.getClass().getName();
        
        //open the element with <, the classname and a space
        sbuf.append("<" + className + " ");
        sbuf.append("geoFilePath=\"" + getGeoFilePath() + "\" ");
        sbuf.append(" />\n");
        
        return sbuf.toString();
    }
    //SETTERS

    public void setGeoFileType(GeoFileType geoFileType) {
        this.geoFileType = geoFileType;
    }

    public void setGeoFilePath(String filePath) {
        this.geoFilePath = filePath;
    }

    public void setGeoFileName(String fileName) {
        this.geoFileName = fileName;
    }

    public void setGeoFileSuffix(String suffix) {
        this.geoFileSuffix = suffix;
    }
    /**
     * Note - this method should be named setKeys because it replaces
     * any current ArrayList<String> of keys with the parameter value, whereas
     * the method addKey(String) adds one value to the existing ArrayList.
     *
     * @param keys ArrayList of keys to set for this metadata
     */
    public void addKeys(ArrayList<String> keys) {
        this.keys = keys;
    }

    public void addKey(String key) {
        keys.add(key);
    }

    public void setKeyRanges(HashMap<String, DiscreteKeyRange> ranges) {
        fullKeyRanges = ranges;
    }

    public void addKeyRange(String key, DiscreteKeyRange range) {
        fullKeyRanges.put(key, range);
    }

    public void addKeyRange(String key, double min, double max, double incr, GeoKeyType keyType) {
        //null here indicates that this DiscreteKeyRange _is_ a full key range
        DiscreteKeyRange range = new DiscreteKeyRange(min, max, incr, keyType, null);
        addKeyRange(key, range);
    }

    public void setGeoDataOrder(GeoDataOrder geoDataOrder) {
        this.geoDataOrder = geoDataOrder;
    }

    public void setEndianFormat(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }
    
    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    /** Display a geoFile's metadata */
    @Override
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append("\n\n<BHP_SU File> METADATA:");
        buf.append(this.getClass().toString());
        buf.append(",\n file type=");
        buf.append(this.geoFileType);
        buf.append(",\n file name=");
        buf.append(this.geoFileName);
        buf.append(",\n file path=");
        buf.append(this.geoFilePath);
        buf.append(",\n file suffix=");
        buf.append(this.geoFileSuffix);
        buf.append(",\n data order=");
        buf.append(this.geoDataOrder);
        buf.append(",\n endianess=");
        buf.append(this.byteOrder);
        for (int i=0; i<keys.size(); i++) {
            String key = keys.get(i);
            buf.append(",\n key=");
            buf.append(key);
            AbstractKeyRange range = fullKeyRanges.get(key);
            buf.append(range.toString());
        }
        return buf.toString();
    }
}
