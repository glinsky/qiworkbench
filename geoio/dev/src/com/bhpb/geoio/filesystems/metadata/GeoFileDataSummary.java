/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.filesystems.metadata;

import com.bhpb.geoio.datasystems.SeismicConstants.SampleFormat;
import com.bhpb.geoio.datasystems.SeismicConstants.SampleUnit;
import java.io.Serializable;

/**
 * Summary information about the trace samples in a geoFile. Values are from
 * the binary header or calculated.
 */
public class GeoFileDataSummary extends Object implements Serializable {

    public GeoFileDataSummary() {
        minAmplitude = Float.MAX_VALUE;
        maxAmplitude = Float.MIN_VALUE;
        average = -1;
        rootMeanSquared = -1;
        fixedSamplesPerTrace = true;
        sampleUnits = SampleUnit.TIME_UNIT;
        sampleRate = 0.004; //milliseconds
        sampleStart = 0;
        sampleFormat = SampleFormat.IBM_FLOAT4_FORMAT;
    }
    /** true if the number of samples per trace is fixed; otherwise, false */
    boolean fixedSamplesPerTrace = true;
    /** Number of traces in the dataset */
    int numberOfTraces = 0;
    /** Sample rate. The default is 0.004ms */
    double sampleRate = 0.004;
    /** The number of samples per trace */
    int samplesPerTrace = 0;
    /** The start sample value in sample units */
    double sampleStart;
    /** The sample units for time or depth */
    SampleUnit sampleUnits;
    /** Calculated values for samples in the dataset */
    double minAmplitude, maxAmplitude, average, rootMeanSquared;
    /** Format of a trace sample */
    SampleFormat sampleFormat = SampleFormat.IBM_FLOAT4_FORMAT;

    //GETTERS
    public SampleFormat getSampleFormat() {
        return sampleFormat;
    }

    public String getSampleFormatAsString() {
        String format = "";
        switch (sampleFormat) {
            case IBM_FLOAT4_FORMAT:
                format = "32-bit IBM float";
                break;
            case IEEE_FLOAT4_FORMAT:
                format = "32-bit IEEE float";
                break;
            case FIXED4_FORMAT:
                format = "32-bit integer";
                break;
            case FIXED2_FORMAT:
                format = "16-bit integer";
                break;
            case FIXED1_FORMAT:
                format = "8-bit integer";
                break;
            case FIXED4GAIN_FORMAT:
                format = "32-bit integer with gain";
                break;
            default:
                format = "Unknown sample format";
        }

        return format;
    }

    /** Get the length, in bytes, of a trace sample's format */
    public int getSampleFormatLength() {
        int len = 0;
        switch (sampleFormat) {
            case IBM_FLOAT4_FORMAT:
                len = 4;
                break;
            case IEEE_FLOAT4_FORMAT:
                len = 4;
                break;
            case FIXED4_FORMAT:
                len = 4;
                break;
            case FIXED2_FORMAT:
                len = 2;
                break;
            case FIXED1_FORMAT:
                len = 1;
                break;
            case FIXED4GAIN_FORMAT:
                len = 4;
                break;
            default:
                len = 0;
        }

        return len;
    }

    public double getMaxAmplitude() {
        return maxAmplitude;
    }

    public double getMinAmplitude() {
        return minAmplitude;
    }

    /**
     * Get the average for the absolute sample values in the dataset
     * @return The avaerage for the absolute sample values; default = -1
     */
    public double getAverage() {
        return average;
    }

    public double getRMS() {
        return rootMeanSquared;
    }

    public int getNumberOfTraces() {
        return numberOfTraces;
    }

    public double getSampleRate() {
        return sampleRate;
    }

    public int getSamplesPerTrace() {
        return samplesPerTrace;
    }

    public double getSampleStart() {
        return sampleStart;
    }

    public SampleUnit getSampleUnits() {
        return sampleUnits;
    }

    public boolean isFixedSamplesPerTrace() {
        return fixedSamplesPerTrace;
    }

    public boolean isStatsCollected() {
        return maxAmplitude == Float.MIN_VALUE ? false : true;
    }

    //SETTERS
    public void setFixedSamplesPerTrace(boolean fixedSamplesPerTrace) {
        this.fixedSamplesPerTrace = fixedSamplesPerTrace;
    }

    public void setSampleFormat(SampleFormat sampleFormat) {
        this.sampleFormat = sampleFormat;
    }

    public void setDataStats(double min, double max, double avg, double rms) {
        minAmplitude = min;
        maxAmplitude = max;
        average = avg;
        rootMeanSquared = rms;
    }

    public void setNumberOfTraces(int numberOfTraces) {
        this.numberOfTraces = numberOfTraces;
    }

    public void setSampleRate(double sampleRate) {
        this.sampleRate = sampleRate;
    }

    public void setSamplesPerTrace(int samplesPerTrace) {
        this.samplesPerTrace = samplesPerTrace;
    }

    public void setSampleStart(double sampleStart) {
        this.sampleStart = sampleStart;
    }

    public void setSampleUnits(SampleUnit sampleUnits) {
        this.sampleUnits = sampleUnits;
    }

    /** Display a geoFile's trace summary */
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("\n\n<BHP_SU File> SUMMARY: ");
        buf.append(this.getClass().toString());
        buf.append(",\n  Fixed size trace: ");
        buf.append(fixedSamplesPerTrace);
        buf.append(",\n  Sample format: ");
        buf.append(sampleFormat);
        buf.append(",\n  Trace stats:");
        buf.append("min=" + minAmplitude + ", max=" + maxAmplitude + ", avg=" + average + ", rms=" + rootMeanSquared);
        buf.append(",\n  Number of traces: ");
        buf.append(numberOfTraces);
        buf.append(",\n  Sample rate: ");
        buf.append(sampleRate);
        buf.append(",\n  Samples per trace: ");
        buf.append(samplesPerTrace);
        buf.append(",\n  Sample start: ");
        buf.append(sampleStart);
        //buf.append(",\n  Start value: ");
        //buf.append(startValue);
        buf.append(",\n  Sample units: ");
        buf.append(sampleUnits);

        return buf.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GeoFileDataSummary other = (GeoFileDataSummary) obj;
        if (this.fixedSamplesPerTrace != other.fixedSamplesPerTrace) {
            return false;
        }
        if (this.numberOfTraces != other.numberOfTraces) {
            return false;
        }
        if (this.sampleRate != other.sampleRate) {
            return false;
        }
        if (this.samplesPerTrace != other.samplesPerTrace) {
            return false;
        }
        if (this.sampleStart != other.sampleStart) {
            return false;
        }
        if (this.sampleUnits != other.sampleUnits) {
            return false;
        }
        if (this.minAmplitude != other.minAmplitude) {
            return false;
        }
        if (this.maxAmplitude != other.maxAmplitude) {
            return false;
        }
        if (this.average != other.average) {
            return false;
        }
        if (this.rootMeanSquared != other.rootMeanSquared) {
            return false;
        }
        if (this.sampleFormat != other.sampleFormat) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.fixedSamplesPerTrace ? 1 : 0);
        hash = 79 * hash + this.numberOfTraces;
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.sampleRate) ^ (Double.doubleToLongBits(this.sampleRate) >>> 32));
        hash = 79 * hash + this.samplesPerTrace;
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.sampleStart) ^ (Double.doubleToLongBits(this.sampleStart) >>> 32));
        hash = 79 * hash + (this.sampleUnits != null ? this.sampleUnits.hashCode() : 0);
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.minAmplitude) ^ (Double.doubleToLongBits(this.minAmplitude) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.maxAmplitude) ^ (Double.doubleToLongBits(this.maxAmplitude) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.average) ^ (Double.doubleToLongBits(this.average) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.rootMeanSquared) ^ (Double.doubleToLongBits(this.rootMeanSquared) >>> 32));
        hash = 79 * hash + (this.sampleFormat != null ? this.sampleFormat.hashCode() : 0);
        return hash;
    }
}