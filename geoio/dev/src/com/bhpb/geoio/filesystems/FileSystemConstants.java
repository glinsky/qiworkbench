/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007-2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.filesystems;

/**
 * FileSystem constants used within the geoIO library
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class FileSystemConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private FileSystemConstants(){}

    /** geoFile IO operation */
    public enum GeoIoOp {GEO_NOP, GEO_READ, GEO_WRITE};

    /** Status of a geoIO operation */
    public enum GeoIoOpStatus {SC_UNKNOWN, SC_OK, SC_ERROR};

    /** Type of geoFile */
    public enum GeoFileType {UNKNOWN_GEOFILE, SEISMIC_GEOFILE, EVENT_GEOFILE, MODEL_GEOFILE};

    /** Order geoData is stored in */
    public enum GeoDataOrder {UNKNOWN_ORDER, CROSS_SECTION_ORDER, MAP_VIEW_ORDER};

    /** Missing Trace Options */
    public enum MissingTraceOption {IGNORE_OPTION, FILL_OPTION};

    /** geoFile format types */
    public static final String HORIZON_FORMAT = "horizFileFormat";
    public static final String SEGY_FORMAT = "segyFileFormat";
    public static final String BHP_SU_FORMAT = "bhpsuFileFormat";

    /** Type of data requested from a geoFile */
    public static final String FILE_METADATA_DATA = "fileMetadataData";
    public static final String TRACE_DATA = "traceData";
    public static final String TRACE_SUMMARY_DATA = "traceSummaryData";
    public static final String SEGY_EBCDIC_HEADER_DATA = "segyEbcdicHeaderData";
    public static final String SEGY_BINARY_HEADER_DATA = "segyBinaryHeaderData";
    public static final String HORIZON_DATA = "horizonData";
    public static final String MODEL_DATA = "modelData";
}
