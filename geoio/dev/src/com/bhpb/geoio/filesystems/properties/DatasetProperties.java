/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.filesystems.properties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import static com.bhpb.geoio.datasystems.DataSystemConstants.*;
import static com.bhpb.geoio.filesystems.FileSystemConstants.*;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Properties about a geoFile.
 */
public abstract class DatasetProperties implements Serializable {

    /** geoFile's metadata */
    GeoFileMetadata metadata;
    /** geoFile's trace samples summary info which is tied to the chosen ranges.
    Must be recalculated if any chosen range or arbitrary traverse is changed.*/
    GeoFileDataSummary summary;
    /** Chosen Ranges */
    HashMap<String, AbstractKeyRange> chosenKeyRanges;
    /** Chosen arbitrary traverses */
    HashMap<String, ArbTravKeyRange> chosenKeyArbTravs;
    /** Flag indicating if a key has an arbitrary traverse or a range */
    HashMap<String, GeoArbTravFlag> keyIndicators;
    /** Orderings of keys. */
    HashMap<String, Integer> orders;
    /** Increments of keys. */
    HashMap<String, Double> increments;
    /** Offsets of keys.*/
    HashMap<String, Integer> offsets;
    /** Nearest (aka Interpolation) of keys. */
    HashMap<String, Boolean> nearests;
    /** Synchronization of keys */
    HashMap<String, Boolean> syncs;
    /** Binsizes of keys. */
    HashMap<String, Integer> binsizes;

    public DatasetProperties(GeoFileMetadata metadata) {

        this.metadata = metadata;

        int num = metadata.getNumKeys();
        chosenKeyRanges = new HashMap<String, AbstractKeyRange>(num);
        chosenKeyArbTravs = new HashMap<String, ArbTravKeyRange>(num);
        orders = new HashMap<String, Integer>(num);
        increments = new HashMap<String, Double>(num);
        offsets = new HashMap<String, Integer>(num);
        keyIndicators = new HashMap<String, GeoArbTravFlag>(num);
        nearests = new HashMap<String, Boolean>(num);
        syncs = new HashMap<String, Boolean>(num);
        binsizes = new HashMap<String, Integer>(num);

        ArrayList<String> keys = metadata.getKeys();
        for (int i = 0; i < num; i++) {
            String key = keys.get(i);
            orders.put(key, Integer.valueOf(i + 1));
            DiscreteKeyRange keyRange = (DiscreteKeyRange) metadata.getKeyRange(key);
            increments.put(key, new Double(keyRange.getIncr()));
            offsets.put(key, Integer.valueOf(0));
            keyIndicators.put(key, GeoArbTravFlag.RANGE_FLAG);

            if (i == 0 && metadata.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
                //for maps, the initial chosen range for tracl is a single value
                chosenKeyRanges.put(key, new DiscreteKeyRange(keyRange.getMin(),
                        keyRange.getMin(),
                        keyRange.getIncr(),
                        keyRange.getKeyType(),
                        keyRange));
            } else {
                chosenKeyRanges.put(key, new UnlimitedKeyRange(keyRange));
            }
            

            nearests.put(key, Boolean.FALSE);
            syncs.put(key, Boolean.FALSE);
            binsizes.put(key, Integer.valueOf(0));
        }
    }

    /**
     * Copy constructor.  Creates new Maps but does not create copies of map keys or values.
     *
     * @param aDatasetProperties
     */
    public DatasetProperties(DatasetProperties aDatasetProperties) {
        metadata = aDatasetProperties.metadata;
        summary = aDatasetProperties.summary;

        chosenKeyRanges = new HashMap(aDatasetProperties.chosenKeyRanges);
        chosenKeyArbTravs = new HashMap(aDatasetProperties.chosenKeyArbTravs);
        orders = new HashMap(aDatasetProperties.orders);
        increments = new HashMap(aDatasetProperties.increments);
        offsets = new HashMap(aDatasetProperties.offsets);
        keyIndicators = new HashMap(aDatasetProperties.keyIndicators);
        nearests = new HashMap(aDatasetProperties.nearests);
        syncs = new HashMap(aDatasetProperties.syncs);
        binsizes = new HashMap(aDatasetProperties.binsizes);
    }

    //GETTERS
    public GeoFileMetadata getMetadata() {
        return metadata;
    }

    public GeoFileDataSummary getSummary() {
        return summary;
    }

    /**
     * Get the chosen key ranget for an indexed key.
     * @return chosen range if there is one for the key; otherwise, null
     */
    public AbstractKeyRange getKeyChosenRange(String key) {
        return chosenKeyRanges.get(key);
    }

    /**
     * Get the chosen arbitrary traverse for an indexed key.
     * @return arbitray traverse if there is one for the key; otherwise, null
     */
    public ArbTravKeyRange getKeyChosenArbTrav(String key) {
        return chosenKeyArbTravs.get(key);
    }

    /** Get the order of a key */
    public int getKeyOrder(String key) {
        return orders.get(key).intValue();
    }

    public HashMap<String, Integer> getKeyOrders() {
        return orders;
    }

    /**
     * Get key with the specified order
     * @param order Key's order
     * @return Key with the specified order; empty string if no key has the specified order.
     */
    public String getOrderedKey(int order) {
        ArrayList<String> keys = metadata.getKeys();
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            if (getKeyOrder(key) == order) {
                return key;
            }
        }
        return "";
    }

    public double getKeyIncrement(String key) {
        return increments.get(key).doubleValue();
    }

    public int getKeyOffset(String key) {
        return offsets.get(key).intValue();
    }

    public boolean getKeyNearest(String key) {
        return nearests.get(key).booleanValue();
    }

    public boolean getKeySync(String key) {
        return syncs.get(key).booleanValue();
    }

    public int getKeyBinsize(String key) {
        return binsizes.get(key).intValue();
    }

    private AbstractKeyRange getVerticalKeyRange() {
        ArrayList<String> hKeys = metadata.getKeys();
        String vKeyName = hKeys.get(hKeys.size() - 1);
        return chosenKeyRanges.get(vKeyName);
    }

    /**
     * Gets the maximum time or depth value associated with these DatasetProperties.
     * 
     * If these DatasetProperties are associated with model data, then this value
     * corresponds to the summary 'sampleRate', otherwise, the maximum value of the
     * vertical header key's chosen range.
     *
     * For map files, the value returned is a header key value.  For horizon cross-sections,
     * the value is an amplitude.  For seismic or model cross sections, the units are time or depth.
     *
     * @return the maximum time or depth
     */
    public double getVerticalCoordMax() {
        GeoDataOrder dataOrder = getMetadata().getGeoDataOrder();

        switch (dataOrder) {
            case UNKNOWN_ORDER:
                throw new IllegalArgumentException("Cannot construct ViewerWindow using datasetProperties with UNKNOWN_ORDER.");
            case CROSS_SECTION_ORDER:
                if (isModel()) {
                    if (summary == null) {
                        throw new UnsupportedOperationException("Cannot get cross-section model max. time/depth: summary is null");
                    }
                    return summary.getSampleRate();
                } else if (isHorizon()) {
                    return summary.getMaxAmplitude();
                } else {
                    return getVerticalKeyRange().getFullKeyRange().getMax();
                }
            case MAP_VIEW_ORDER:
                if (isModel()) {
                    if (summary == null) {
                        throw new UnsupportedOperationException("Cannot get cross-section model max. time/depth: summary is null");
                    }
                    //bugfix: for a model, the vertical coord. max is not SampleRate, but sampleStart + sampleRate*numSamplesPerTrace
                    //return summary.getSampleRate();
                    return summary.getSampleStart() + summary.getSampleRate() * summary.getSamplesPerTrace();
                } else {
                    return getVerticalKeyRange().getFullKeyRange().getMax();
                }
            default:
                throw new IllegalArgumentException("Unknown dataOrder: " + dataOrder);
        }
    }

    /**
     * Gets the minimum time or depth value associated with these DatasetProperties.
     * 
     * If these DatasetProperties are associated with model data, then this value
     * corresponds to the summary 'sampleStart', otherwise, the minimum value of the
     * vertical header key's chosen range.
     *
     * For map files, the value returned is a header key value.  For horizon cross-sections,
     * the value is an amplitude.  For seismic or model cross sections, the units are time or depth.
     *
     * @return the maximum time or depth
     */
    public double getVerticalCoordMin() {
        if (metadata.getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER &&
                isHorizon()) {
            return summary.getMinAmplitude();
        } else {
            return summary.getSampleStart();
        }
    }

    public boolean isRange(String key) {
        return keyIndicators.get(key) == GeoArbTravFlag.RANGE_FLAG ? true : false;
    }

    public boolean isArbTrav(String key) {
        return keyIndicators.get(key) == GeoArbTravFlag.ARB_TRAV_FLAG ? true : false;
    }

    /**
     * Check if there are any keys with an arbitrary traverse
     * @reutrn true if there is a key witn an arbitrary tranverse; otherwise false
     */
    public boolean hasArbTrav() {
        return chosenKeyArbTravs.size() > 0 ? true : false;
    }

    /**
     * Get the size of an arbitrary traverse. All have the same size.
     * @return size of an arbitrary traverse; 0 if no key has an arbitrary traverse
     */
    public int getArbTravSize() {
        if (!hasArbTrav()) {
            return 0;

        //find a key with an arbitrary traverse
        }
        ArrayList<String> keys = metadata.getKeys();
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            if (isArbTrav(key)) {
                return chosenKeyArbTravs.get(key).getArbTravSize();
            }
        }

        return 0;
    }

    /**
     * Subclasses implement this method to define any subclass-specific properties
     * which should be serialized to XML as attributes.
     * 
     * This method
     * may also be the empty String, which is semantically meaningless but changes
     * the value returned by saveState() from <datasetProperties ... />
     * to <datasetProperties ... > </datasetProperties>.
     *
     * Note that no validation is performed on the results of this method and returned
     * malformed XML will most likely cause an Exception.
     *
     * @return String of XML attributes representing subclass-specific properties.
     */
    public abstract String getAttributeXML();

    /**
     * Subclasses implement this method to define any subclass-specific properties
     * which should be serialized to XML as content element of <...DatasetProperties>.
     * 
     * This method
     * may also be the empty String, which is semantically meaningless but changes
     * the value returned by saveState() from <...datasetProperties ... />
     * to <...datasetProperties ... > </...datasetProperties>.
     *
     * Note that no validation is performed on the results of this method and returned
     * malformed XML will most likely cause an Exception.
     * 
     * @return String of XML content representing subclass-specific properties.
     */
    public abstract String getContentXML();

    //SETTERS
    public void setMetadata(GeoFileMetadata metadata) {
        this.metadata = metadata;
    }

    public void setSummary(GeoFileDataSummary summary) {
        this.summary = summary;
    }

    /**
     * For all String keys in the map chosenRangeMap, sets the associated chosen
     * AbstractKeyRanges.  Exactly one of the following mutually exclusive conditions must be
     * true:
     * <ul>
     * <li>either all of the AbstractKeyRanges are intances of ArbTravKeyRange or else none are instances of ArbTravKeyRange</li>
     * <li>if all are instances of AbstractKeyRange, then there must be exactly 2 entries in the chosenRangeMap</li>
     * </ul>
     * 
     * If this contract is violated, an IllegalArgumentException will be thrown
     * 
     * @param chosenRangeMap the map of header key names => AbstractKeyRanges
     */
    public <T extends AbstractKeyRange> void setKeyChosenRanges(Map<String, T> chosenRangeMap) {
        boolean hasArbTravKeyRange = false;
        Collection<T> chosenRanges = chosenRangeMap.values();
        int nArbTravKeys = 0;
        for (AbstractKeyRange abstractKeyRange : chosenRanges) {
            if (abstractKeyRange instanceof ArbTravKeyRange) {
                hasArbTravKeyRange = true;
                nArbTravKeys++;
            }
        }

        //This check may not be sufficient; strictly speaking, exactly the first two key ranges must be arbitrary
        //traverses if any are, but there is no way to determine the order of header keys from the chosenRangeMap.
        //In any case, the GUI prevents entering an arbitrary traverse as the vertical range, so this should not matter.
        if (hasArbTravKeyRange && (nArbTravKeys != 2 || chosenKeyRanges.size() != 3)) {
            throw new IllegalArgumentException("chosenRangeMap must contain exactly 2 or 0 arbitrary traverses of 3 header keys (1 vertical)");
        }

        Set<Entry<String, T>> entrySet = chosenRangeMap.entrySet();

        for (Entry<String, T> entry : entrySet) {
            String key = entry.getKey();
            AbstractKeyRange value = entry.getValue();
            if (value.isArbTrav()) {
                setKeyChosenArbTrav(key, (ArbTravKeyRange) value);
            } else {
                setKeyChosenRange(key, value);
            }
        }
    }

    /**
     * (Re)set the chosen range for the given key in the internal maps.
     * @param key The name of the associated header key
     * @param range The chosen Key Range for 'key'
     */
    public void setKeyChosenRange(String key, AbstractKeyRange range) {
        keyIndicators.put(key, GeoArbTravFlag.RANGE_FLAG);
        chosenKeyRanges.put(key, range);
    }

    /**
     * @deprecated this method will be made private in a future release; please
     * use setKeyChosenRanges(Map<String,AbstractKeyRange>) instead
     * @param key the name of the associated header key
     * @param arbTrav the ArbTravKeyRange to choose for 'key'
     */
    public void setKeyChosenArbTrav(String key, ArbTravKeyRange arbTrav) {
        keyIndicators.put(key, GeoArbTravFlag.ARB_TRAV_FLAG);
        chosenKeyArbTravs.put(key, arbTrav);
    }

    public void setKeyOrder(String key, int order) {
        orders.put(key, Integer.valueOf(order));
    }

    public void setKeyIncrement(String key, double incr) {
        increments.put(key, new Double(incr));
    }

    public void setKeyOffset(String key, int offset) {
        offsets.put(key, Integer.valueOf(offset));
    }

    public void setKeyNearest(String key, boolean nearest) {
        nearests.put(key, Boolean.valueOf(nearest));
    }

    public void setKeySync(String key, boolean sync) {
        syncs.put(key, Boolean.valueOf(sync));
    }

    public void setKeyBinsize(String key, int binsize) {
        binsizes.put(key, Integer.valueOf(binsize));
    }

    /**
     * Similar to toString(), but serializes DatasetProperties' state as well-formed
     * XML.
     * 
     * @return String representation of DatasetProperties' state as XML.
     */
    public String saveState() {
        StringBuffer sbuf = new StringBuffer();
        String className = this.getClass().getName();

        //open the element with <, the classname and a space
        sbuf.append("<" + className + " ");
        sbuf.append(getXMLattributes());
        String attribXML = getAttributeXML();
        if (attribXML != null) {
            sbuf.append(attribXML);
        }
        sbuf.append(">\n"); //make cursory effort to create readable XML with newlines but do not indent

        sbuf.append(metadata.saveState());
        ArrayList<String> keys = metadata.getKeys();

        for (String key : keys) {
            sbuf.append("<key " + getKeyAttributes(key) + ">\n");
            if (isArbTrav(key)) {
                sbuf.append(getKeyChosenArbTrav(key).toXmlString());
            } else {
                sbuf.append(getKeyChosenRange(key).toXmlString());
            }
            sbuf.append("</key>\n");
        }

        String contentXML = getContentXML();
        if (contentXML != null) {
            sbuf.append(contentXML);
        }
        //close the element with /, the className and >
        sbuf.append("</" + className + ">\n");

        return sbuf.toString();
    }

    private String getKeyAttributes(String keyName) {
        StringBuilder sbuilder = new StringBuilder();

        sbuilder.append("name=\"" + keyName + "\" ");
        sbuilder.append("binSize=\"" + getKeyBinsize(keyName) + "\" ");
        sbuilder.append("nearest=\"" + getKeyNearest(keyName) + "\" ");
        sbuilder.append("offset=\"" + getKeyOffset(keyName) + "\" ");
        sbuilder.append("order=\"" + getKeyOrder(keyName) + "\" ");
        sbuilder.append("sync=\"" + getKeySync(keyName) + "\" ");

        return sbuilder.toString();
    }

    private String getXMLattributes() {
        StringBuffer sbuf = new StringBuffer();

        sbuf.append("isHorizon=\"" + Boolean.toString(isHorizon()) + "\" ");
        sbuf.append("isModel=\"" + Boolean.toString(isModel()) + "\" ");
        sbuf.append("isSeismic=\"" + Boolean.toString(isSeismic()) + "\" ");

        return sbuf.toString();
    }

    /** Display a geoFile's properties */
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("\n\n<BHP_SU File> PROPERTIES:");
        buf.append(this.getClass().toString());
        ArrayList<String> keys = metadata.getKeys();
        buf.append(",\n  Chosen key ranges:");
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            buf.append(",\n key=");
            buf.append(key);
            AbstractKeyRange range = chosenKeyRanges.get(key);
            ArbTravKeyRange arbTrav = chosenKeyArbTravs.get(key);
            if (isRange(key)) {
                buf.append(range.toString());
            } else if (arbTrav != null && isArbTrav(key)) {
                buf.append(arbTrav.toString());
            }
        }
        buf.append(",\n  Orders:");
        for (int i = 0; i < keys.size(); i++) {
            buf.append(" " + orders.get(keys.get(i)).intValue());
        }
        buf.append(",\n  Increments:");
        for (int i = 0; i < keys.size(); i++) {
            buf.append(" " + increments.get(keys.get(i)).doubleValue());
        }
        buf.append(",\n  Offsets:");
        for (int i = 0; i < keys.size(); i++) {
            buf.append(" " + offsets.get(keys.get(i)).intValue());
        }
        buf.append(",\n  Nearests:");
        for (int i = 0; i < keys.size(); i++) {
            buf.append(" " + nearests.get(keys.get(i)).booleanValue());
        }
        buf.append(",\n  Synchronization:");
        for (int i = 0; i < keys.size(); i++) {
            buf.append(" " + syncs.get(keys.get(i)).booleanValue());
        }
        buf.append(",\n  Binsizes:");
        for (int i = 0; i < keys.size(); i++) {
            buf.append(" " + binsizes.get(keys.get(i)));
        }

        return buf.toString();
    }

    public boolean isHorizon() {
        return this instanceof HorizonProperties;
    }

    public boolean isModel() {
        return this instanceof ModelProperties;
    }

    public boolean isSeismic() {
        return this instanceof SeismicProperties && !(this instanceof HorizonProperties);
    }

    @Override
    /**
     * DatasetProperties are equal if and only if all fields have identical references
     * or are equal primitve values.
     *
     * @param obj DatasetProperties to test for equality with this SeismicProperties
     * @return true if and only if obj is equal to this
     */
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DatasetProperties other = (DatasetProperties) obj;

        if (this.metadata != other.metadata && (this.metadata == null || !this.metadata.equals(other.metadata))) {
            return false;
        }

        if (this.summary != other.summary && (this.summary == null || !this.summary.equals(other.summary))) {
            return false;
        }

        if (this.chosenKeyRanges != other.chosenKeyRanges && (this.chosenKeyRanges == null || !this.chosenKeyRanges.equals(other.chosenKeyRanges))) {
            return false;
        }

        if (this.chosenKeyArbTravs != other.chosenKeyArbTravs && (this.chosenKeyArbTravs == null || !this.chosenKeyArbTravs.equals(other.chosenKeyArbTravs))) {
            return false;
        }

        if (this.keyIndicators != other.keyIndicators && (this.keyIndicators == null || !this.keyIndicators.equals(other.keyIndicators))) {
            return false;
        }

        if (this.orders != other.orders && (this.orders == null || !this.orders.equals(other.orders))) {
            return false;
        }

        if (this.increments != other.increments && (this.increments == null || !this.increments.equals(other.increments))) {
            return false;
        }

        if (this.offsets != other.offsets && (this.offsets == null || !this.offsets.equals(other.offsets))) {
            return false;
        }

        if (this.nearests != other.nearests && (this.nearests == null || !this.nearests.equals(other.nearests))) {
            return false;
        }

        if (this.syncs != other.syncs && (this.syncs == null || !this.syncs.equals(other.syncs))) {
            return false;
        }

        if (this.binsizes != other.binsizes && (this.binsizes == null || !this.binsizes.equals(other.binsizes))) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.metadata != null ? this.metadata.hashCode() : 0);
        hash = 23 * hash + (this.summary != null ? this.summary.hashCode() : 0);
        hash = 23 * hash + (this.chosenKeyRanges != null ? this.chosenKeyRanges.hashCode() : 0);
        hash = 23 * hash + (this.chosenKeyArbTravs != null ? this.chosenKeyArbTravs.hashCode() : 0);
        hash = 23 * hash + (this.keyIndicators != null ? this.keyIndicators.hashCode() : 0);
        hash = 23 * hash + (this.orders != null ? this.orders.hashCode() : 0);
        hash = 23 * hash + (this.increments != null ? this.increments.hashCode() : 0);
        hash = 23 * hash + (this.offsets != null ? this.offsets.hashCode() : 0);
        hash = 23 * hash + (this.nearests != null ? this.nearests.hashCode() : 0);
        hash = 23 * hash + (this.syncs != null ? this.syncs.hashCode() : 0);
        hash = 23 * hash + (this.binsizes != null ? this.binsizes.hashCode() : 0);
        return hash;
    }
}