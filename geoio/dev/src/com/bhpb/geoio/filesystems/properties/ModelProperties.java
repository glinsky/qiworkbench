/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.filesystems.properties;

import java.io.Serializable;
import java.util.ArrayList;

import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import java.util.List;

/**
 * Dataset properties of a model geoFile.
 */
public class ModelProperties extends DatasetProperties implements Serializable {

    public ModelProperties(ModelFileMetadata metadata) {
        super(metadata);

        ArrayList<String> properties = metadata.getProperties();
        if (properties.size() > 0) {
            selectedProperties.add(properties.get(0));
        }
    }

    /**
     * Copy constructor
     * @param original the SeismicProperties to be (deep) copied
     */
    public ModelProperties(ModelProperties aModelProperties) {
        super(aModelProperties);

        setSelectedProperties(new ArrayList<String>(aModelProperties.selectedProperties));
    }

    /** Selected property */
    List<String> selectedProperties = new ArrayList(1);
	
	/** Synchronize property. true => synchronize set; false => synchronize unset */
	boolean synchronizeProperty = false;

    //GETTERS
    public synchronized String getSelectedProperties() {
        StringBuffer sb = new StringBuffer("");
        if (selectedProperties.size() > 0) {
            sb.append(selectedProperties.get(0));
        }
        for (int i = 1; i < selectedProperties.size(); i++) {
            sb.append(",");
            sb.append(selectedProperties.get(i));
        }
        return sb.toString();
    }
	
	public synchronized boolean getSynchronizeProperty() {
		return this.synchronizeProperty;
	}

    //SETTERS
    /**
     * Set the selected property.
     * @param property A valid property
     */
    public synchronized void setSelectedProperty(String property) {
        selectedProperties = new ArrayList<String>(1);
        selectedProperties.add(property);
    }

    /**
     * Sets this ModelProperties' list of SelectedProperties to the names
     * contained in the properties paramter.  No validation is performed.
     * 
     * @param properties List<String> of selected properties.
     */
    public synchronized void setSelectedProperties(List<String> properties) {
        selectedProperties = new ArrayList<String>(properties.size());
        for (String property : properties) {
            selectedProperties.add(property);
        }
    }
	
	public synchronized void setSynchronizeProperty(boolean synchronizeProperty) {
		this.synchronizeProperty = synchronizeProperty;
	}

    /** Display a model geoFile's properties */
    @Override
    public String toString() {
        String baseMetadata = super.toString();

        StringBuffer buf = new StringBuffer();
        buf.append("\n <model file> PROPERTIES:");
        buf.append("\n selected properties=");
        buf.append(getSelectedProperties());
		buf.append("\n property synchronize=");
		buf.append(this.synchronizeProperty);

        return baseMetadata + buf.toString();
    }

    /**
     * Get the number of selected properties.
     * @return
     */
    int getNumSelectedProperties() {
        return selectedProperties.size();
    }

    @Override
    public String getAttributeXML() {
        return null;
    }

    public String getContentXML() {
        StringBuffer buf = new StringBuffer();

        buf.append("<selectedProperties>");
        for (String propertyName : selectedProperties) {
            buf.append("<propertyName>");
            buf.append(propertyName);
            buf.append("</propertyName>\n");
        }
        buf.append("</selectedProperties>");

        return buf.toString();
    }

    /**
     * ModelProperties are equal if and only if both have equivalent DatasetProperties
     * fields and both have null selectedProperties
     * Lists or the selectedProperties Lists are the same size and contain
     * equivalent Strings at each index of the List.
     *
     * @param obj Object to test for equality with this ModelProperties
     * @return true if and only if the objects are equivalent
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModelProperties other = (ModelProperties) obj;
        if (this.selectedProperties != other.selectedProperties && (this.selectedProperties == null || !this.selectedProperties.equals(other.selectedProperties))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + (this.selectedProperties != null ? this.selectedProperties.hashCode() : 0);
        return hash;
    }
}