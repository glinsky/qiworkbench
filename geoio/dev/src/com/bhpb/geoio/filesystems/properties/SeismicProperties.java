/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.geoio.filesystems.properties;

import com.bhpb.geoio.filesystems.FileSystemConstants.MissingTraceOption;
import java.io.Serializable;

import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;

/**
 * Properties about a seismic geoFile.
 */
public class SeismicProperties extends DatasetProperties implements Serializable {
    public SeismicProperties(SeismicFileMetadata metadata) {
        super(metadata);
    }

    /**
     * Copy constructor
     * @param original the SeismicProperties to be (deep) copied
     */
    public SeismicProperties(SeismicProperties aSeismicProperties) {
        super(aSeismicProperties);

        setMissingTraceOption(aSeismicProperties.getMissingTraceOption());
    }

    /** Missing Trace option */
    MissingTraceOption missingTraceOption = MissingTraceOption.IGNORE_OPTION;

    //GETTERS
    public MissingTraceOption getMissingTraceOption() {
        return missingTraceOption;
    }

    //SETTERS
    public void setMissingTraceOption(MissingTraceOption missingTraceOption) {
        this.missingTraceOption = missingTraceOption;
    }

    /** Display a seismic geoFile's properties */
    @Override
    public String toString() {
        String baseProperties = super.toString();

        StringBuffer buf  = new StringBuffer();

        buf.append(",\n missing trace option=");
        buf.append(this.missingTraceOption);

        return baseProperties + buf.toString();
    }

    @Override
    public String saveState() {
        return super.saveState();
    }
    
    public String getAttributeXML() {
        return "missingTraceOption=\"" + missingTraceOption.toString() + "\"";
    }
    
    public String getContentXML() {
        return "";
    }

    @Override
    /**
     * SeismicProperties objects are equal if and only if their DatasetProperties
     * fields are equivalent and the missingTraceOption field has the same value
     *
     * @param obj Objcet to test for equality with this SeismicProperties
     * @return true if and only if obj is equal to this
     */
    public boolean equals(Object obj) {
        if (!(super.equals(obj))) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SeismicProperties other = (SeismicProperties) obj;
        if (this.missingTraceOption != other.missingTraceOption) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.missingTraceOption != null ? this.missingTraceOption.hashCode() : 0);
        return hash;
    }
}