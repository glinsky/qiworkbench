/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.filesystems.properties;

import java.io.Serializable;
import java.util.ArrayList;

import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;

/**
 * Properties about a horizon geoFile.
 */
public class HorizonProperties extends SeismicProperties implements Serializable {

    /** Selected horizon; one of the selected horizons */
    private String selectedHorizon = "";
    /** Horizon synchronization switch */
    private boolean synced = false;

    public HorizonProperties(SeismicFileMetadata metadata) {
        super(metadata);

        ArrayList<String> allHorizons = metadata.getHorizons();
        if (allHorizons != null && allHorizons.size() > 0) {
            selectedHorizon = allHorizons.get(0);
        }
    }
    
    /**
     * Copy constructor.
     * 
     * @param aHorizonProperties HorizonProperties to be (deep) copied
     */
    public HorizonProperties(HorizonProperties aHorizonProperties) {
        super(aHorizonProperties);

        setSelectedHorizon(aHorizonProperties.getSelectedHorizon());
    }

    @Override
    public String getAttributeXML() {
        StringBuffer sbuf = new StringBuffer();
        
        sbuf.append(super.getAttributeXML());
        sbuf.append(" selectedHorizon=\"" + selectedHorizon + "\" ");
        
        return sbuf.toString(); 
    }

    public String getSelectedHorizon() {
        return selectedHorizon;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSync(boolean synced) {
        this.synced = synced;
    }

    /**
     * Set a selected horizon.
     * @param horizon One of the selected horizons.
     */
    public void setSelectedHorizon(String horizon) {
        this.selectedHorizon = horizon;
    }

    /** Display a horizon geoFile's properties */
    @Override
    public String toString() {
        String baseMetadata = super.toString();

        StringBuffer buf = new StringBuffer();
        buf.append("\n <horizon file> HORIZONS:");
        buf.append("\n Selected horizon=");
        buf.append(this.selectedHorizon);
        buf.append("\n Synchronization=" + this.synced);

        return baseMetadata + buf.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HorizonProperties other = (HorizonProperties) obj;
        if ((this.selectedHorizon == null) ? (other.selectedHorizon != null) : !this.selectedHorizon.equals(other.selectedHorizon)) {
            return false;
        }
        if (this.synced != other.synced) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.selectedHorizon != null ? this.selectedHorizon.hashCode() : 0);
        hash = 47 * hash + (this.synced ? 1 : 0);
        return hash;
    }

    
}