/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.geoio.filesystems.geofileio;

import java.io.Serializable;

import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.*;
import com.bhpb.qiworkbench.api.IDataObject;

/**
 * Container of the geoData for a geoIO read operation.
 * <p>If the geoIO read operation failed, contains the reason, i.e., the
 * error message.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class GeoReadData extends GeoIOData implements Serializable {
    private static final long serialVersionUID = 1L;

    public GeoReadData() {
        setOpType(GeoIoOp.GEO_READ);
    }

    /**
     * Get the next data record to be read.
     * @return The next data record to be read. If there are none, an empty record. null
     * if the record cannot be obtained.
     */
    synchronized public IDataObject nextRecord() {
        //if there is no result to return, return an empty DO
        if (geoRecords.size() == 0) return new DataObject();

        try {
            DataObject result = geoRecords.remove(0);
            processedRecords++;

            return result;
        } catch (IndexOutOfBoundsException iobe) {
            //should never happen.
        }

        return null;
    }

    /**
     * Add a data record to the accumulated results being read.
     * @param record Data record read.
     */
    synchronized public void addRecord(DataObject record) {
        geoRecords.add(record);
        //count the number of reads add to the result set
        totalRecords++;
    }

    /**
     * Check if all the geoData has been read, i.e., the geoIO read
     * operation has finished.
     * @return true if all geoData has been read; otherwise, false.
     */
    public boolean isAllDataRead() {
        return geoIoOpFinished;
    }

    //Setters
}
