/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.geoio.filesystems.geofileio;

import java.io.Serializable;
import java.lang.IndexOutOfBoundsException;
import java.util.ArrayList;
import java.util.HashMap;

import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.*;
import com.bhpb.geoio.filesystems.geofileio.GeoIOData;

/**
 * Container of the geoData for a geoIO write operation.
 * <p>If the geoIO write operation failed, contains the reason, i.e., the
 * error message.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class GeoWriteData extends GeoIOData implements Serializable {
    private static final long serialVersionUID = 1L;

    public GeoWriteData() {
        setOpType(GeoIoOp.GEO_WRITE);
    }

    //Setters

    /**
     * Set the total number of records to be written.
     * @param records All of the data objects to be written.
     */
    public void setTotalRecords(ArrayList<DataObject> records) {
        this.geoRecords = records;
        this.totalRecords = records.size();
        statusCode = GeoIoOpStatus.SC_OK;
    }
}
