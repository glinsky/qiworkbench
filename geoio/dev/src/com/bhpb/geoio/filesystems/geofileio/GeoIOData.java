/*
###########################################################################
# geoIO - Read and write geophysical data (seismic, horizon, well log) in various formats.
# This program module Copyright (C) 2007, BHP Billiton Petroleum and is
# licensed under the following BSD License.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of BHP Billiton Petroleum  nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################
*/

package com.bhpb.geoio.filesystems.geofileio;

import java.io.Serializable;
import java.lang.IndexOutOfBoundsException;
import java.util.ArrayList;
import java.util.HashMap;

import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.*;

/**
 * Base class for the container of the geoData of a geoIO operation.
 * <p>If the geoIO operation failed, contains the reason, i.e., the
 * error message.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public abstract class GeoIOData implements Serializable {
    protected static final long serialVersionUID = 1L;

    /** Status code of processing the geoIO data */
    protected GeoIoOpStatus statusCode = GeoIoOpStatus.SC_UNKNOWN;

    /** Whether the geoIO is a read or a write */
    protected GeoIoOp geoIoOp = GeoIoOp.GEO_NOP;

    /** The total number of data records to be read/written */
    protected int totalRecords = 0;

    /** The number of data records actually read/written */
    protected int processedRecords = 0;

    /** geoData of a geoIO operation */
    protected ArrayList<DataObject> geoRecords = new ArrayList<DataObject>();

    /** Error message for an abnormal result */
    protected String errorMsg = "";

    /** State of the geoIO operation */
    protected boolean geoIoOpFinished = false;
    
    /** Type for vector of data objects */
    protected DataObject[] doType = new DataObject[0];

    //Getters
     /**
      * Convenient method to create a vector of data objects from the array
      * list representation.
      * @return Vector of data objects
      */
     public DataObject[] toArray() {
         return geoRecords.toArray(doType);
     }
    
     public boolean isReadOp() {
         return geoIoOp.equals(GeoIoOp.GEO_READ) ? true : false;
     }

     public boolean isWriteOp() {
         return geoIoOp.equals(GeoIoOp.GEO_WRITE) ? true : false;
     }

     /**
      * Get the number of data records still to be read/written.
      * @return The number of data records still to be read/written.
      */
     public int getRemainingRecords() {
         return geoRecords.size();
     }

     /**
      * Get the number of data records already read/written.
      * @return The number of data records already read/written.
      */
     public int getProcessedRecords() {
         return processedRecords;
     }

    /**
     * Check if there are more data records to be processed, i.e.,
     * read but not consummed or yet to be written out.
     * @return "true" if more data records; otherwise, "false".
     */
    public boolean hasMoreRecords() {
        return geoRecords.size() != 0 ? true : false;
    }

    /**
     * Check if there are more data records yet to be read/written
     * @param true if all records not yet read/written.
     */
    public boolean hasAllRecords() {
        return processedRecords == totalRecords ? false : true;
    }

    /**
     * Check if the result's status code indicates an abnormal condition occured
     * during the processing of a geoIO operation that prevented it from being
     * successfully completed. An UNKNOWN status code is not considered
     * abnormal.
     *
     * @return true if a processing error occurred; otherwise, false.
     */
    public boolean isAbnormalStatus() {
        if (statusCode == GeoIoOpStatus.SC_UNKNOWN || statusCode == GeoIoOpStatus.SC_OK) return false;

        return true;
    }

    /**
     * Get error message for an abnormal result.
     * @return Error message
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    //Setters

    /**
     * Set the status of a geoIO operation. The status can change
     * as the operation progresses. For example, fail part way
     * through the operation.
     * @param statusCode Status of a geoIO operation.
     */
    public void setStatus(GeoIoOpStatus statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return statusCode The geoIO op's status code.
     */
    public GeoIoOpStatus getStatus() {
        return statusCode;
    }
    /**
     * Set the type of geoIO operation.
     * @param geoIoOp Type of geoIO operation
     */
    public void setOpType(GeoIoOp geoIoOp) {
        this.geoIoOp = geoIoOp;
    }

    /**
     * Set the total number of records to be written.
     * @param records All of the data objects to be written.
     */
    public void setTotalRecords(ArrayList<DataObject> records) {
        this.geoRecords = records;
        this.totalRecords = records.size();
        statusCode = GeoIoOpStatus.SC_OK;
    }

    /**
     * Set the error message of a geoIO operation that failed
     * @param errorMsg Cause of a failed geoIO operation
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        statusCode = GeoIoOpStatus.SC_ERROR;
    }

    /**
     * Set whether or not the geoIO operation is finished.
     * @param geoIoOpFinished true if finished; otherwise, false.
     */
    public void setOpFinished(boolean geoIoOpFinished) {
        this.geoIoOpFinished = geoIoOpFinished;
    }
    
    /**
     * Determine whether the geoIoOp has finished as set by the geoIOsvc.
     * @return true if and only if the geoIoOp is finished.
     */
    public boolean isOpFinished() {
        return this.geoIoOpFinished;
    }
}
