/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.util;

import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoIoOpStatus;
import com.bhpb.geoio.filesystems.geofileio.GeoReadData;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.qiworkbench.api.IDataObject;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.compAPI.ProgressMonitor2;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JOptionPane;

/**
 * Adapter class which provides methods for sending QIWorkbenchMsg commands
 * to the GeoIO service and for receiving and interpreting the results.  This
 * simplifies the GeoIODirector class and provides methods that can be used to
 * programatically 'drive' the loading of a layer in a non-interactive fashion.
 * <p>
 * The sequence of calls begins with an explicit filePath (aka pathlist) from
 * the content of a message received by the qiViewer.
 * <p>
 * The following should be performed in the order shown (alternatively, see
 * the implementation of the GeoIODirector).  Unless an abnormal
 * reponse or error condition leads to one of the return statements shown below,
 * the results of these method calls are a (1) datasetProperties containing a metaData
 * for the file indicated by fileName, (2) layerProperties containing default settings for
 * a new seismic layer and (3) a set of DataObject[] suitable for rasterization.
 * <p>
 * These are the three parameters of qiviewer.ViewerDesktop.openNewViewerWindow().
 * <p>
 *  ... documentation describing the necessary sequence of calls will go here,
 *  see GeoIODirector for actual implementation.  Notably this refactoring has
 *  combined checking for null response with asserting the content type.
 * <p>
 * Again, see the paragraphs above for an explanation of the three output values
 * indicated in bold.
 */
public class GeoIOAdapter {

    private final IMessagingManager msgMgr;
    private static final int TIMEOUT = 30 * 60 * 1000;   //1/2 hour
    private static final Logger logger =
            Logger.getLogger(GeoIOAdapter.class.getName());
    private static final int GEOIO_WAIT_INCR = 100; //100ms

    public GeoIOAdapter(IMessagingManager msgMgr) {
        this.msgMgr = msgMgr;
        if (msgMgr == null) {
            throw new IllegalArgumentException("Cannot instantiate GeoIOAdapter because agent's IMessagingManager is null.");
        }
    }

    public String sendGetMetadataRequest(String filePath) {
        ArrayList<String> params = new ArrayList<String>();
        params.add(msgMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(filePath);
        params.add(FileSystemConstants.FILE_METADATA_DATA);

        return msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
    }

    public boolean isRequestSent(String msgID) {
        return msgID != null;
    }

    public IQiWorkbenchMsg getResponse(String geoIORequestID) {
        return msgMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
    }

    /**
     * A stripped-down wrapper of GeoIO metadata-related message handling.
     * See qiviewer.util.GeoIODirector for more information.
     * 
     * @param filePath The BHPSU file for which metadata will be read.
     * 
     * @return the metadata read from the requested file
     */
    public GeoFileMetadata readMetadata(String filePath) {
        String geoIORequestID = sendGetMetadataRequest(filePath);

        if (!isRequestSent(geoIORequestID)) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            throw new RuntimeException(errorMsg);
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = getResponse(geoIORequestID);
            if (!isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
                String errorMsg = "Abnormal response to READ_GEOFILE_CMD: " + response.getContent().toString();
                logger.warning(errorMsg);
                throw new RuntimeException(errorMsg);
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get metadata for geoFile " + filePath;
                    logger.warning(errorMsg);
                    throw new RuntimeException(errorMsg);
                }

                if (isResponseContentBhpSuMetadata(response)) {
                    return getMetadata(response);
                } else {
                    logger.warning("Response received from server but content type is not BhpSuMetadata.");
                    throw new RuntimeException("Unable to read BhpSuMetadata from server");
                }
            }
        }
    }

    /**
     * Invoke geoIO to get the summary for the requested DatasetProperties.
     * 
     * 
     * @return the empty String if succesful, or an error messaeg otherwise.
     */
    public String readSummary(DatasetProperties dsProps) {

        String geoIORequestID = sendSummaryRequest(dsProps);

        if (!isRequestSent(geoIORequestID)) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            return errorMsg;
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = getResponse(geoIORequestID);
            if (!isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
                String errorMsg;
                if (response == null) {
                    errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                } else {
                    if (response.isAbnormalStatus()) {
                        errorMsg = "Abnormal response of type '" + response.getContentType() + "': " + response.getContent().toString();
                    } else {
                        errorMsg = "non-Abnormal but invalid response of type '" + response.getContentType() + "': " + response.getContent().toString();
                    }
                }
                logger.warning(errorMsg);
                return errorMsg;
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_SUMMARY_DATA for datasetProperties " + dsProps.toString();
                    logger.warning(errorMsg);
                    return errorMsg;
                }

                if (!isResponsContentTraceSummaryData(response)) {
                    String errorMsg = "Not a response containing geoFile TRACE_SUMMARY_DATA" + response;
                    logger.warning(errorMsg);
                    return errorMsg;
                }

                GeoFileDataSummary summaryData = getTraceSummaryData(response);

                logger.fine("TRACE_SUMMARY_DATA for datasetProperties" + dsProps);
                logger.fine(summaryData.toString());

                dsProps.setSummary(summaryData);
                logger.fine("Summary data set for dataSetProperties.");

                return "";
            }
        }
    }

    public boolean isValidResponse(IQiWorkbenchMsg response, String expectedType) {
        if (response == null || response.isAbnormalStatus()) {
            return false;
        }

        if (expectedType == null) {
            throw new IllegalArgumentException("expectedType must be non-null to check for response validity");
        }

        //TODO improve logging to indicate why response type is invalid
        return (response.getContent() != null) && (expectedType.equals(response.getContentType()));
    }

    public boolean isResponseContentBhpSuMetadata(IQiWorkbenchMsg response) {
        if (isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
            Object content = response.getContent();
            ArrayList contentList = (ArrayList) content;

            //validate this is a response with the geoFile's metadata
            String fileFormat = (String) contentList.get(0);
            String dataType = (String) contentList.get(1);
            if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                    !dataType.equals(FileSystemConstants.FILE_METADATA_DATA)) {
                return false;
            }

            return true;
        } else {
            throw new IllegalArgumentException("Cannot determine whether content type is GeoFile Metadata because response is not valid.");
        }
    }

    public boolean isResponsContentTraceSummaryData(IQiWorkbenchMsg response) {
        if (isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
            ArrayList contentList = (ArrayList) response.getContent();

            //validate this is a response with the geoFile's metadata
            String fileFormat = (String) contentList.get(0);
            String dataType = (String) contentList.get(1);
            if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                    !dataType.equals(FileSystemConstants.TRACE_SUMMARY_DATA)) {
                return false;
            } else {
                return true;
            }
        } else {
            throw new IllegalArgumentException("Cannot determine whether content type is trace summary data because response is not valid.");
        }
    }

    public GeoFileMetadata getMetadata(IQiWorkbenchMsg response) {
        if (isResponseContentBhpSuMetadata(response)) {
            Object content = response.getContent();
            ArrayList contentList = (ArrayList) content;

            return (GeoFileMetadata) contentList.get(2);
        } else {
            throw new IllegalArgumentException("Cannot get GeoFile Metadata from response because the message content type is not correct.");
        }
    }

    public String sendSummaryRequest(DatasetProperties dsProps) {

        ArrayList<Object> params = new ArrayList<Object>();
        params.add(msgMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        params.add(FileSystemConstants.TRACE_SUMMARY_DATA);

        return msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
    }

    public GeoFileDataSummary getTraceSummaryData(IQiWorkbenchMsg response) {
        if (isValidResponse(response, QIWConstants.ARRAYLIST_TYPE) && isResponsContentTraceSummaryData(response)) {
            return (GeoFileDataSummary) ((ArrayList) response.getContent()).get(2);
        } else {
            throw new IllegalArgumentException("Cannot get trace summary data - response is invalid or content is not a trace summary.");
        }
    }

    /**
     * Invoke geoIO, getting all traces associated with the given DatasetProperties.
     * 
     * @param dsProps The DatasetProperties specifying a geoIO read operation.
     * 
     * @return Array of DataObjects associated with the DatasetProperties and
     * issued request, possibly 0 in length.
     */
    public DataObject[] getTraces(DatasetProperties dsProps) {
        String geoIORequestID = sendTraceRequest(dsProps);

        DataObject[] result = new DataObject[0];

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            result = new DataObject[0];
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = getResponse(geoIORequestID);
            if (!isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
                String errorMsg;
                if (response == null) {
                    errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                } else {
                    errorMsg = "Invalid response: non-null response received but it was of the wrong type: " + response.getContentType();
                }
                logger.warning(errorMsg);
                result = new DataObject[0];
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_DATA for datasetProperties " + dsProps.toString();
                    logger.warning(errorMsg);
                    result = new DataObject[0];
                } else {
                    //GeoFileDataSummary summaryData = (GeoFileDataSummary )contentList.get(2);
                    logger.fine("TRACE_DATA for datasetProperties" + dsProps);
                    //logger.fine(summaryData.toString());

                    logger.fine("Trace data for datasetProperties successfully read.");
                    GeoReadData readData = getGeoReadData(response);

                    int MAX_DATA_READ_TIME = 60000;
                    int maxTraces = dsProps.getSummary().getNumberOfTraces();
                    int tracesRead = 0;

                    boolean userSelectedContinue;

                    do {
                        userSelectedContinue = false;
                        logger.fine("Waiting up to " + (MAX_DATA_READ_TIME / 1000) + " seconds for read to complete...");
                        long startTime = System.currentTimeMillis();
                        long elapsedTime = 0;

                        String title = "Reading trace data: " + dsProps.getMetadata().getGeoFileName();
                        String text = "Waiting (up to " + MAX_DATA_READ_TIME / 1000 + " seconds) for traces to be read: ";
                        Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);

                        String status = " traces read out of " + maxTraces;

                        ProgressMonitor2 monitor = new ProgressMonitor2(maxTraces, false, 0,
                                title, text, icon, null);

                        monitor.start(tracesRead + status);
                        boolean userAborted = false;

                        while (!readData.isOpFinished() && !userAborted &&
                                elapsedTime < MAX_DATA_READ_TIME) {
                            try {
                                elapsedTime = System.currentTimeMillis() - startTime;

                                tracesRead = readData.getRemainingRecords();
                                monitor.setCurrent(tracesRead + status, tracesRead);
                                monitor.setText(text + elapsedTime / 1000);

                                if (monitor.isCanceled()) {
                                    userAborted = true;
                                } else {
                                    Thread.sleep(500);
                                }
                            } catch (InterruptedException ie) {
                                logger.warning("Interrupted while waiting for READ_GEOFILE_CMD to finish reading.");
                            }
                        }
                        if (readData.isOpFinished()) { // most prevalent condition - either the op finished or an error occured

                            GeoIoOpStatus statusCode = readData.getStatus();
                            if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_OK)) {
                                logger.info("GeoIOop with status code: " + statusCode);
                            } else if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_ERROR)) {
                                String errorMsg = readData.getErrorMsg();
                                logger.info("GeoIoOp encountered error: " + errorMsg);
                                JOptionPane.showMessageDialog(null, errorMsg, "Abnormal condition reading trace data",
                                        JOptionPane.ERROR_MESSAGE);
                            } else if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_UNKNOWN)) {
                                logger.warning("GeoIoOp finished with status code: " + statusCode + ", the qiViewer may be unstable.");
                            } else {
                                throw new UnsupportedOperationException("Unable to handle unknown GeoIoOpStatus: " + statusCode);
                            }
                        } else if (userAborted) {
                            int selection = JOptionPane.showConfirmDialog(null,
                                    "Stop reading trace data?",
                                    "Data loading canceled",
                                    JOptionPane.YES_NO_OPTION);

                            //continue waiting if the user selects Yes, but not if
                            //the user selections No or closes the ConfirmDialog without making
                            //a selection
                            if (selection == JOptionPane.NO_OPTION) {
                                userSelectedContinue = true;
                            }
                        } else if (elapsedTime >= MAX_DATA_READ_TIME) {
                            tracesRead = readData.getRemainingRecords();
                            int selection = JOptionPane.showConfirmDialog(null,
                                    tracesRead + " traces have been read but the operation is not finished." +
                                    "\n" +
                                    "Continue waiting for traces to be read?",
                                    "Timeout Exceeded",
                                    JOptionPane.YES_NO_OPTION);

                            //continue waiting if the user selects Yes, but not if
                            //the user selections No or closes the ConfirmDialog without making
                            //a selection
                            if (selection == JOptionPane.YES_OPTION) {
                                userSelectedContinue = true;
                            }

                        } else {
                            logger.info("User did not abort the progress monitor, nor was the MAX_DATA_READ_TIME exceeded.  Most likely all the data has been read.");
                        }
                        monitor.setCanceled(true);
                    } while (!readData.isOpFinished() && userSelectedContinue);

                    if (readData.isAbnormalStatus()) {
                        logger.info("READ_GEOFILE_CMD result was abnormal.");
                    } else {
                        if (logger.isLoggable(Level.INFO)) {
                            logger.info("READ_GEOFILE_CMD result was normal.");
                        }
                        result = readData.toArray();
                    }
                }
            }
        }
        return result;
    }

    public String sendTraceRequest(DatasetProperties dsProps) {
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(msgMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        if (dsProps.isModel()) {
            params.add(FileSystemConstants.MODEL_DATA);
        } else {
            params.add(FileSystemConstants.TRACE_DATA);
        }
        params.add("0");
        params.add(Integer.valueOf(dsProps.getSummary().getNumberOfTraces()).toString());
        //geoIO does not use the 'selectedProperty' field of dsProperties, so it must be set here
        if (dsProps.isModel()) {
            ArrayList<String> alist = new ArrayList<String>(1);
            alist.add(((ModelProperties) dsProps).getSelectedProperties());
            params.add(alist);
        }
        return msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
    }

    public boolean isResponseContentTraceData(IQiWorkbenchMsg response) {
        if (isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
            ArrayList contentList = (ArrayList) response.getContent();
            String fileFormat = (String) contentList.get(0);
            String dataType = (String) contentList.get(1);
            if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                    !dataType.equals(FileSystemConstants.TRACE_DATA)) {
                //TODO log this
                return false;
            } else {
                return true;
            }
        } else {
            //TODO log this
            return false;
        }
    }

    public GeoReadData getGeoReadData(IQiWorkbenchMsg response) {
        if (isValidResponse(response, QIWConstants.ARRAYLIST_TYPE) &&
                (isResponseContentTraceData(response) || isResponseContentHorizonData(response) ||
                isResponseContentModelData(response))) {
            return (GeoReadData) ((ArrayList) response.getContent()).get(2);
        } else {
            throw new IllegalArgumentException("Cannot get GeoReadData - response is invalid or the content is not trace/horizon/model data");
        }
    }

    /**
     * Read a horizon contained in a BHP-SU dataset.
     * @param dsProps Properties of the BHP-SU dataset
     * @param horizon Name of a selected horizon.
     * @return ID of the request message
     */
    public String sendHorizonRequest(DatasetProperties dsProps, String horizon) {
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(msgMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        params.add(FileSystemConstants.HORIZON_DATA);
        params.add(horizon);
        params.add(Integer.valueOf(0).toString());
        /*params.add(Integer.valueOf(dsProps.getSummary().getNumberOfTraces()).toString());*/

        return msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
    }

    public boolean isResponseContentHorizonData(IQiWorkbenchMsg response) {
        if (isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
            ArrayList contentList = (ArrayList) response.getContent();
            String fileFormat = (String) contentList.get(0);
            String dataType = (String) contentList.get(1);
            if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                    !dataType.equals(FileSystemConstants.HORIZON_DATA)) {
                //TODO log this
                return false;
            } else {
                return true;
            }
        } else {
            //TODO log this
            return false;
        }
    }

    /**
     * Get a set of model traces.
     * @param dsProps Dataset properties of the model file
     * @param List of model properties to include in each model trace
     * @return ID of request message.
     */
    public String sendModelRequest(DatasetProperties dsProps, ArrayList<String> properties) {
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(msgMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        params.add(FileSystemConstants.MODEL_DATA);
        params.add("0");
        params.add(Integer.valueOf(dsProps.getSummary().getNumberOfTraces()).toString());
        params.add(properties);

        return msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
    }

    /**
     * Check if the response is for a "get model data" request
     * @return true if a response to a model request; otherwise, false
     */
    public boolean isResponseContentModelData(IQiWorkbenchMsg response) {
        if (isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
            ArrayList contentList = (ArrayList) response.getContent();
            String fileFormat = (String) contentList.get(0);
            String dataType = (String) contentList.get(1);
            if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                    !dataType.equals(FileSystemConstants.MODEL_DATA)) {
                //TODO log this
                return false;
            } else {
                return true;
            }
        } else {
            //TODO log this
            return false;
        }
    }

    /**
     * Invoke geoIO, getting all horizon traces associated with the given DatasetProperties.
     *
     * @param dsProps The DatasetProperties specifying a geoIO read operation.
     *
     * @return Array of BhpsuHorizonDataObjects associated with the DatasetProperties and
     * issued request, possibly 0 in length.
     */
    public BhpSuHorizonDataObject[] getHorizons(HorizonProperties dsProps) {
        String horizonName = dsProps.getSelectedHorizon();

        if (horizonName == null || "".equals(horizonName)) {
            logger.warning("Requested read of null or empty horizon name, returning empty array of BhpSuHorizonDataObjects.");
            return new BhpSuHorizonDataObject[0];
        }

        String geoIORequestID = sendHorizonRequest(dsProps, horizonName);

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            return new BhpSuHorizonDataObject[0];
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = getResponse(geoIORequestID);
            if (!isValidResponse(response, QIWConstants.ARRAYLIST_TYPE)) {
                String errorMsg;
                if (response == null) {
                    errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                } else {
                    errorMsg = "Invalid response: non-null response received but it was of the wrong type: " + response.getContentType();
                }
                logger.warning(errorMsg);
                return new BhpSuHorizonDataObject[0];
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get HORIZON_DATA for datasetProperties " + dsProps.toString();
                    logger.warning(errorMsg);
                    return new BhpSuHorizonDataObject[0];
                }

                if (!isResponseContentHorizonData(response)) {
                    String errorMsg = "Not a response containing geoFile HORIZON_DATA" + response;
                    logger.warning(errorMsg);
                    return new BhpSuHorizonDataObject[0];
                } else {
                    logger.fine("HORIZON_DATA for datasetProperties" + dsProps);
                    logger.fine("Horizon data for datasetProperties successfully read.");

                    //TODO if the time used to grow the horizonList is a problem,
                    //determine whether the file is transpose and calculate the size of a horizon list
                    List<BhpSuHorizonDataObject> horizonList = new ArrayList<BhpSuHorizonDataObject>();
                    GeoReadData readData = getGeoReadData(response);

                    int MAX_DATA_READ_TIME = 30000; // maximum of 30 seconds to read one horizon before prompting the user

                    boolean userSelectedContinue;

                    do {
                        userSelectedContinue = false;
                        logger.info("Waiting up to " + (MAX_DATA_READ_TIME / 1000) + " seconds for read to complete...");
                        long startTime = System.currentTimeMillis();
                        long elapsedTime = 0;

                        boolean userAborted = false;
                        while ((readData.isOpFinished() == false) && (elapsedTime < MAX_DATA_READ_TIME) && (!userAborted)) {
                            try {
                                elapsedTime = System.currentTimeMillis() - startTime;
                                Thread.sleep(GEOIO_WAIT_INCR);
                            } catch (InterruptedException ie) {
                                logger.warning("Interrupted while waiting for READ_GEOFILE_CMD to finish reading.");
                            }
                        }
                        if (readData.isOpFinished()) { // most prevalent condition - either the op finished or an error occured
                            GeoIoOpStatus statusCode = readData.getStatus();
                            if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_OK)) {
                                logger.info("GeoIOop with status code: " + statusCode);
                            } else if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_ERROR)) {
                                String errorMsg = readData.getErrorMsg();
                                logger.info("GeoIoOp encountered error: " + errorMsg);
                                JOptionPane.showMessageDialog(null, errorMsg, "Abnormal condition reading trace data",
                                        JOptionPane.ERROR_MESSAGE);
                            } else if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_UNKNOWN)) {
                                logger.warning("GeoIoOp finished with status code: " + statusCode + ", the qiViewer may be unstable.");
                            } else {
                                throw new UnsupportedOperationException("Unable to handle unknown GeoIoOpStatus: " + statusCode);
                            }
                        } else if (userAborted) {
                            int selection = JOptionPane.showConfirmDialog(null,
                                    "Stop reading horizon data?",
                                    "Data loading canceled",
                                    JOptionPane.YES_NO_OPTION);

                            //continue waiting if the user selects Yes, but not if
                            //the user selections No or closes the ConfirmDialog without making
                            //a selection
                            if (selection == JOptionPane.NO_OPTION) // if the user selects YES or closes the Dialog, data loading will be aborted
                            {
                                userSelectedContinue = true;
                            }
                        } else if (elapsedTime >= MAX_DATA_READ_TIME) {
                            int tracesRead = readData.getRemainingRecords();
                            int selection = JOptionPane.showConfirmDialog(null,
                                    tracesRead + " horizons have been read but the operation is not finished." +
                                    "\n" +
                                    "Continue waiting for horizons to be read?",
                                    "Timeout Exceeded",
                                    JOptionPane.YES_NO_OPTION);

                            //continue waiting if the user selects Yes, but not if
                            //the user selections No or closes the ConfirmDialog without making
                            //a selection
                            if (selection == JOptionPane.YES_OPTION) {
                                userSelectedContinue = true;
                            }

                        } else {
                            logger.info("User did not abort the progress monitor, nor was the MAX_DATA_READ_TIME exceeded.  Most likely all the data has been read.");
                        }
                    } while (!readData.isOpFinished() && userSelectedContinue);

                    if (readData.isAbnormalStatus()) {
                        logger.info("READ_GEOFILE_CMD result was abnormal.");
                    } else {
                        logger.info("READ_GEOFILE_CMD result was normal.");
                    }

                    while (readData.hasMoreRecords()) {
                        IDataObject nextRecord = readData.nextRecord();
                        if (nextRecord instanceof BhpSuHorizonDataObject) {
                            horizonList.add((BhpSuHorizonDataObject) nextRecord);
                        } else {
                            throw new RuntimeException("Expected BhpSuHorizonDataObject but received DataObject.");
                        }
                    }
                    return horizonList.toArray(new BhpSuHorizonDataObject[0]);
                }
            }
        }
    }

    /**
     * Write out a BHP-SU horizon in cross-section view. The IO is performed
     * by the geoIOService on the server-side.
     * @param hname Name of the horizon
     * @param dsProps Dataset properties of the horizon
     * @param hvals Horizon values.
     * @return Empty string if operation completed successfully; otherwise, an
     * error message stating why operation was unsuccessful.
     */
    public String writeXsecHorizon(String hname, HorizonProperties dsProps, float[] hvals) {
        ArrayList params = new ArrayList();
        //Note: Writing out a horizon is performed by the server-side GeoIOService
        params.add(QIWConstants.REMOTE_SERVICE_PREF);
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        params.add(FileSystemConstants.HORIZON_DATA);
        params.add(hname);
        params.add(hvals);

        String geoIORequestID = msgMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.WRITE_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);

        String errorMsg = "";
        if (geoIORequestID == null) {
            errorMsg = "Unable to send " + QIWConstants.WRITE_GEOFILE_CMD + " request";
            logger.warning(errorMsg);
            return errorMsg;
        } else {
            // Wait for the response, which will come.
            IQiWorkbenchMsg response = getResponse(geoIORequestID);
            if (!isValidResponse(response, QIWConstants.INTEGER_TYPE)) {
                if (response == null) {
                    errorMsg = "Unable to get response of " + QIWConstants.WRITE_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                } else {
                    errorMsg = "Invalid response: non-null response received, but it was of the wrong type: " + response.getContentType();
                }
                logger.warning(errorMsg);
                return errorMsg;
            } else {
                if (response.isAbnormalStatus()) {
                    errorMsg = "Unable to update horizon with datasetProperties " + dsProps.toString();
                    logger.warning(errorMsg);
                    return errorMsg;
                }
            }
        }

        return "";
    }
}
