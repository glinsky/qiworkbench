/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.util;

/**
 * A circular buffer for hold blocks of binary data. A producer fills the buffer
 * while a consumer empties the buffer. Typically usage: a producer is reading
 * trace blocks from a socket connected to the server and adding them to the 
 * buffer, while a consumer is extracting trace blocks from the buffer and
 * creating data object containers to hold them.It is expected that the server
 * is sending multiple trace blocks across the socket.
 */
public class GeoBinaryBuffer {
	/** Circular buffer holding blocks of binary data */
	byte[] blockBuffer;
	
	/** The size in bytes of the binary data block.*/
	int blockSize;
	/**The size in bytes of the circular buffer for hold binary data blocks. */
	int bufferSize;
	
	//Buffer indices
	/** Index of next available full buffer block. */
	int top;
	/** Index of next available empty block */
	int next;
	/** Index of last available block */
	int last;
	
	/**
	 * Create a buffer to hold n blocks of date of a fixed size.
	 * @param blockSize The size in bytes of a data block
	 * @param bufferSize The capacity of the buffer holding data blocks
	 */
	public GeoBinaryBuffer(int blockSize, int bufferSize) {
		this.blockSize = blockSize;
		this.bufferSize = bufferSize;
		
		//make buffer empty
		top = -1; next = 0;
		
		last = bufferSize - 1;
		
		blockBuffer = new byte[blockSize*bufferSize];
	}
	
	/**
	 * Check if the buffer is full.
	 * @return true if buffer is full; otherwise, false
	 */
	synchronized public boolean isFull() {
		return (top == next) ? true : false;
	}
	 
	/**
	 * Check if the buffer is empty.
	 * @return true if the buffer is empty; otherwise, false.
	 */
	synchronized public boolean isEmpty() {
		return (top < 0) ? true : false;
	}
	 
	 /**
	  * Remove the next available full buffer block if the buffer is not empty.
	  * NOTE: The callee should check if the buffer is not empty before removing
	  * a data block.
	  * @return The next available full buffer block or an empty block if the
	  * buffer is empty.
	  */
	 synchronized public byte[] removeBlock() {
		 //check if the buffer is empty
		 if (top < 0) return new byte[0];
		 
		  top++;
		 //if top catches up to next, the buffer is empty
		 if (top == next) {
			 top = -1; next = 0;
		 }
		 //check if wrapping around the buffer
		 if (top > last) top = 0;
		  
		 byte[] blockData = new byte[blockSize];
		 System.arraycopy(blockBuffer, top*blockSize, blockData, 0, blockSize);
		  
		 return blockData;
	 }
	  
	 /**
	  * Add a block of binary data to the buffer if the buffer is not full.
	  * NOTE: The callee should check if the buffer is not full before adding
	  * a data block.
	  * @param blockData Block of binary data to be added.
	  * @return true if the block was added; otherwise, false (buffer full)
	  */
	  synchronized public boolean addBlock(byte[] blockData) {
		  //check if the buffer if full
		  if (top == next) return false;
		  
		  System.arraycopy(blockData, 0, blockBuffer, next*blockSize, blockSize);
		  //check if buffer is empty
		  if (top < 0) top = 0;
		  
		  //advance to the next available empty block
		  next++;
		  if (next > last) next = 0;
		  //NOTE: If next catches up to top, the buffer is full
		  
		  return true;
	  }
}
