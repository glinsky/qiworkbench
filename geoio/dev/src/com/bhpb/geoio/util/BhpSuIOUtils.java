/*
###########################################################################
# bhpsuIOlib - Read and write geophysical data (seismic, horizon) in BHP-SU
# format.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.util;

import com.bhpb.geoio.constants.BhpSuIOConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.GeoFormatDesc;
import com.bhpb.geoio.datasystems.GeoIO;
import com.bhpb.geoio.datasystems.metadata.FieldMetadata;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;

import java.util.logging.Logger;

/**
 * Utilities for BHP-SU IO.
 */
public class BhpSuIOUtils {

    private static final Logger logger =
            Logger.getLogger(BhpSuIOUtils.class.toString());
            
    /**
     * Get the format descriptor for BHP-SU.
     */
    static public GeoFormatDesc getBhuSuFormatDesc() {
        return GeoIO.getInstance().getFormatDesc(BHP_SU_FORMAT);
    }

    /**
     * Generate the normalized set of seismic trace IDs based on the chosen ranges.
     * They are in the order bhpsuread returns the traces. The trace ID
     * is of the form key1ID;key2ID;key3ID... where the keys are in the order
     * specified in the dataset's metadata; this order is called the normalized
     * order. Each key ID is the key name concatenated with a range value. For
     * example, if the key is 'ep' and the range value is 1350, the key ID is
     * ep1350.
     * <p>
     * To make the trace IDs unigue, the filepath followed by a semicolon needs
     * to be prepended.
     *
     * @param properties Dataset's properties; includes its metadata and summary info.
     * @return List of normalized trace IDs if they can be formed; otherwise, null.
     * @deprecated A normalized traceID will be generated on demand by using
     * information in the trace header.
     */
    static public String[] genNormalizedTraceIDs(DatasetProperties properties) {
        if (properties.hasArbTrav()) {
            return null;
        }
        GeoFileDataSummary summary = properties.getSummary();
        GeoFileMetadata metadata = properties.getMetadata();
        ArrayList<String> mkeys = metadata.getKeys();
        int numTraces = summary.getNumberOfTraces();
        logger.info("Generating traceIDs for " + numTraces + " traces.");
        String[] traceIDs = new String[numTraces];

        //NOTE: The last key determines the number of samples-per-trace.
        // The other key ranges determines the number of traces.
        int numKeys = mkeys.size() - 1;
        String[] keys = new String[numKeys];
        double[] min = new double[numKeys];
        double[] max = new double[numKeys];
        double[] incr = new double[numKeys];
        double[] rangeVals = new double[numKeys];
        String[] keyIDs = new String[numKeys];
        int[] orders = new int[numKeys];
        for (int i = 0; i < numKeys; i++) {
            String key = properties.getOrderedKey(i + 1);
            //find the normalized order of the chosen ordered key
            //and remember it
            int order = 1;
            for (int j = 0; j < numKeys; j++) {
                if (key.equals(mkeys.get(j))) {
                    break;
                }
                order++;
            }
            orders[i] = order;
            keys[i] = key;
            DiscreteKeyRange keyRange = (DiscreteKeyRange)properties.getKeyChosenRange(key);
            incr[i] = keyRange.getIncr();
            //If data is a model in transpose order, do not iterate over tracl
            //exactly 1 layer must be set in the property name e.g. "property:layer"
            //even though the tracl range is '*'. This is a special case of
            //bhpread syntax used in order for the data summary to be correct.            
            if (i == 0 && //TODO should this match vs. 'tracl' or 'key #0'?
                    properties.getMetadata().isModelFile() &&
                    properties.getMetadata().getGeoDataOrder().equals(GeoDataOrder.MAP_VIEW_ORDER)) {
                String selectedProperty = ((ModelProperties) properties).getSelectedProperties();
                String layerNumber = selectedProperty.split(":")[1];
                double layer = Double.parseDouble(layerNumber);
                min[i] = rangeVals[i] = max[i] = layer;
            } else {
                min[i] = rangeVals[i] = keyRange.getMin();
                max[i] = keyRange.getMax();
            }
        //System.out.println("norm order "+i+":"+keys[i]+","+orders[i]+","+min[i]+","+max[i]+","+incr[i]);
        }

        //generate trace IDs in read order
        int tids = 0;
        String tid = "";
        boolean finished = false;
        while (!finished) {
            //gather keyID in normalized order
            for (int j = 0; j < numKeys; j++) {
                keyIDs[orders[j] - 1] = keys[j] + rangeVals[j];
            }
            //create the normalized trace ID
            tid = "";
            for (int j = 0; j < numKeys; j++) {
                tid += keyIDs[j];
                if (j != numKeys - 1) {
                    tid += ";";
                }
            }
            traceIDs[tids++] = tid;

            int k = numKeys - 1;
            while (k != -1) {
                rangeVals[k] += incr[k];
                if (rangeVals[k] > max[k]) {
                    rangeVals[k] = min[k];
                    k--;
                    if (k == -1) {
                        finished = true;
                    }
                } else {
                    break;
                }
            }
        }

        return traceIDs;
    }

    /**
     * Generate the normalized set of trace IDs for the selected horizon based 
     * on the chosen ranges. They are in the order bhpsuread returns the traces.
     * A trace ID
     * is of the form key1ID;key2ID;key3ID... where the keys are in the order
     * specified in the dataset's metadata; this order is called the normalized
     * order. Each key ID is the key name concatenated with a range value. For
     * example, if the key is 'ep' and the range value is 1350, the key ID is
     * ep1350.
     * <p>
     * To make the trace IDs unigue, the filepath followed by a semicolon needs
     * to be prepended.
     *
     * @param properties Horizon's dataset properties; includes its metadata, 
     * summary info and selected horizon.
     * @return List of normalized trace IDs for the selected horizon if they can 
     * be formed; otherwise, null.
     * @deprecated A normalized traceID will be generated on demand by using
     * information in the trace header.
     */
    static public String[] genNormalizedHorizonTraceIDs(HorizonProperties properties) {
        if (properties.hasArbTrav()) {
            return null;
        }
        GeoFileDataSummary summary = properties.getSummary();
        SeismicFileMetadata metadata = (SeismicFileMetadata) properties.getMetadata();
        ArrayList<String> mkeys = metadata.getKeys();
        int numTraces = summary.getNumberOfTraces();
        String[] traceIDs = new String[numTraces];

        String selectedHorizon = properties.getSelectedHorizon();
        ArrayList<String> horizons = metadata.getHorizons();
        int horizIdx = 0;
        for (int i = 0; i < horizons.size(); i++) {
            horizIdx = i;
            if (horizons.get(i).equals(selectedHorizon)) {
                break;
            }
        }
        //System.out.println("selected horiz="+selectedHorizon+", horizIdx="+horizIdx);
        //NOTE: The last key determines the number of samples-per-trace.
        // The other key ranges determines the number of traces.
        int numKeys = mkeys.size() - 1;
        String[] keys = new String[numKeys];
        double[] min = new double[numKeys];
        double[] max = new double[numKeys];
        double[] incr = new double[numKeys];
        double[] rangeVals = new double[numKeys];
        String[] keyIDs = new String[numKeys];
        int[] orders = new int[numKeys];
        for (int i = 0; i < numKeys; i++) {
            String key = properties.getOrderedKey(i + 1);
            //find the normalized order of the chosen ordered key
            //and remember it
            int order = 1;
            for (int j = 0; j < numKeys; j++) {
                if (key.equals(mkeys.get(j))) {
                    break;
                }
                order++;
            }
            orders[i] = order;
            keys[i] = key;
            DiscreteKeyRange keyRange = (DiscreteKeyRange)properties.getKeyChosenRange(key);
            min[i] = rangeVals[i] = keyRange.getMin();
            max[i] = keyRange.getMax();
            incr[i] = keyRange.getIncr();
        //System.out.println("norm order "+i+":"+keys[i]+","+orders[i]+","+min[i]+","+max[i]+","+incr[i]);
        }

        //The first key is tracl. There is one set of traces for each value of
        //tracl which is in 1-1 correspondence with the horizons. Restrict the
        //value of tracl with the selected horizon.
        min[0] = horizIdx;
        max[0] = horizIdx;
        rangeVals[0] = horizIdx;

        //generate trace IDs in read order
        int tids = 0;
        String tid = "";
        boolean finished = false;
        while (!finished) {
            //gather keyID in normalized order
            for (int j = 0; j < numKeys; j++) {
                keyIDs[orders[j] - 1] = keys[j] + rangeVals[j];
            }
            //create the normalized trace ID
            tid = "";
            for (int j = 0; j < numKeys; j++) {
                tid += keyIDs[j];
                if (j != numKeys - 1) {
                    tid += ";";
                }
            }
//System.out.println("traceID "+tids+": "+tid);
            traceIDs[tids++] = tid;

            int k = numKeys - 1;
            while (k != -1) {
                rangeVals[k] += incr[k];
                if (rangeVals[k] > max[k]) {
                    rangeVals[k] = min[k];
                    k--;
                    if (k == -1) {
                        finished = true;
                    }
                } else {
                    break;
                }
            }
        }

        return traceIDs;
    }
}