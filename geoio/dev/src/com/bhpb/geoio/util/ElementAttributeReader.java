/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.geoio.util;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.util.logging.Logger;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ElementAttributeReader {
    private static final Logger logger = Logger.getLogger(ElementAttributeReader.class.toString());
    
    /**
     * Iterates over the NodeList of children, returning the first Node
     * whose name is an exact match for nodeName.
     * 
     * @param node
     * @param nodeName
     * 
     * @return the first matching Node or null if no such node is found
     */
    public static Node getChildNode(Node node, String nodeName) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeName().equals(nodeName)) {
                return children.item(i);
            }
        }
        return null;
    }
    
    public static boolean getBoolean(Node node, String attributeName) {
        String valueString = ((Element)node).getAttribute(attributeName);
        return (valueString.length() == 0) ? false : Boolean.parseBoolean(valueString);
    }

    public static boolean getBoolean(Node node, String attributeName, boolean noValueDefault) {
        String valueString = ((Element)node).getAttribute(attributeName);
        return (valueString.length() == 0) ? noValueDefault : Boolean.parseBoolean(valueString);
    }
    
    public static Color getColor(Node node, String attributeName) {
        return toColor(getString(node, attributeName));
    }
    
    public static double getDouble(Node node, String attributeName) {
        String valueString = ((Element)node).getAttribute(attributeName);
        try {
            return (valueString.length() == 0) ? 0.0 : Double.parseDouble(valueString);
        } catch (NumberFormatException nfe) {
            return 0.0;
        }
    }
    public static double getDouble(Node node, String attributeName, double noValueDefault) {
        String valueString = ((Element)node).getAttribute(attributeName);
        try {
            return (valueString.length() == 0) ? noValueDefault : Double.parseDouble(valueString);
        } catch (NumberFormatException nfe) {
            return noValueDefault;
        }
    }
    
    public static float getFloat(Node node, String attributeName) {
        String valueString = ((Element)node).getAttribute(attributeName);
        try {
            return (valueString.length() == 0) ? 0.0f : Float.parseFloat(valueString);
        } catch (NumberFormatException nfe) {
            return 0.0f;
        }
    }
    public static float getFloat(Node node, String attributeName, float noValueDefault) {
        String valueString = ((Element)node).getAttribute(attributeName);
        try {
            return (valueString.length() == 0) ? noValueDefault : Float.parseFloat(valueString);
        } catch (NumberFormatException nfe) {
            return noValueDefault;
        }
    }
    
    public static int getInt(Node node, String attributeName) {
        String valueString = ((Element)node).getAttribute(attributeName);
        try {
            return (valueString.length() == 0) ? 0 : Integer.parseInt(valueString);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }
    public static int getInt(Node node, String attributeName, int noValueDefault) {
        String valueString = ((Element)node).getAttribute(attributeName);
        try {
            return (valueString.length() == 0) ? noValueDefault : Integer.parseInt(valueString);
        } catch (NumberFormatException nfe) {
            return noValueDefault;
        }
    }

    public static String getString(Node node, String attributeName) {
        return ((Element)node).getAttribute(attributeName);
    }
    
    /**
     * Construct a Color from the string "red green blue alpha"
     * @param color String representation of a color
     * @return Java Color
     */
    public static Color toColor(String color) {
        Color kolor = Color.orange;
        if (color.length() != 0) {
            try {
                String[] tokens = color.trim().split(" ");
                int red = Integer.parseInt(tokens[0]);
                int green = Integer.parseInt(tokens[1]);
                int blue = Integer.parseInt(tokens[2]);
                int alpha = Integer.parseInt(tokens[3]);
                kolor = new Color(red, green, blue, alpha);
            } catch (Exception e) {
                logger.warning("Unable to convert the string color, "+color+", to a Color. Using the default color, orange.");
            }
        }

        return kolor;
    }

    /**
     * Method taken from DeliveryLite.util.Util.java.
     * 
     * @param node w3c dom node
     * 
     * @return XML String representation of DOM node
     */
    public static String nodeToXMLString(Node node) {
        String xml = "";
        if (node == null) {
            return xml;
        }
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer tranformer = tFactory.newTransformer();
            DOMSource source = new DOMSource(node);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(baos);
            tranformer.transform(source, result);
            xml = baos.toString();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }
        return xml;
    }
}