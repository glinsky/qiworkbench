/*
###########################################################################
# bhpsuIOlib - Read and write geophysical data (seismic, horizon) in BHP-SU
# format.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.bhpsuio;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.geoio.datasystems.DataSystemConstants.GeoKeyType;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import java.nio.ByteOrder;

/**
 * Read the metadata for a BHP-SU dataset.
 */
public class ReadFileMetadata {
    private static Logger logger = Logger.getLogger(ReadFileMetadata.class.getName());

    /**
     * Generate the BHP-SU script for getting a dataset's metadata.
     * @param args List of input arguments:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] full path of the geoFile</li>
     * </ul>
     * @param messagingMgr MessagingManager of the geoIO Service requesting the metadata.
     * @return List:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] BHP-SU script as a list of commands; null if cannot generate the script</li>
     * </ul>
     */
    static private ArrayList<String> genMetadataScript(ArrayList<String> args, IMessagingManager messagingMgr) {
        String filePath = args.get(1);

        ArrayList<String> script = new ArrayList<String>();
        // make sure script is executable
        script.add(args.get(0));
        script.add("bash");
        script.add("-c");
        String filesep = messagingMgr.getServerOSFileSeparator();
        int idx = filePath.lastIndexOf(filesep);
        if (idx == -1) return null;
        String fileDir = filePath.substring(0,idx);
        String fileName = filePath.substring(idx+1);
        fileName = fileName.substring(0,fileName.lastIndexOf(".dat"));
        script.add(" source ~/.bashrc; cd " + fileDir + "; bhpio filename=" +fileName);

        return script;
    }

    /**
     * Get the metadata for a geoFile by calling BHP-SU bhpio given the file's
     * full path, then retrieve the output, parse the output and form the metadata
     * object.
     * @param args List of input arguments:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] full path of the geoFile</li>
     * </ul>
     * @param messagingMgr MessagingManager of the geoIO Service requesting the metadata.
     * @return The geoFile's metadata if it is successfully obtained; otherwise, null.
     */
    static public GeoFileMetadata _getFileMetadata(ArrayList<String> args, IMessagingManager messagingMgr) {
        GeoFileMetadata metadata = null;
        String locpref = args.get(0);
        String filePath = args.get(1);

        if (filePath == null || filePath.trim().length() == 0)
            return null;

        ArrayList<String> params = genMetadataScript(args, messagingMgr);
        if (params == null) return null;

        // submit job for execution
        int status = 0;
        int errorStatus = 0;
        String errorContent = "";

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        //QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID);
        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
            }
            return null;
        }

        String jobID = (String)MsgUtils.getMsgContent(response);
        status = MsgUtils.getMsgStatusCode(response);
        logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
        logger.info("status of SUBMIT_JOB_CMD normal response " + status);

        //get stdout
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID);

        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
            }
            return null;
        } else {
            ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
            status = MsgUtils.getMsgStatusCode(response);
            logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
            metadata = parseBhpioOutput(stdOut, filePath);
        }

        //get stderr
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID);

        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
                return null;
            }
            return null;
        } else {
            ArrayList<String> stdErr = (ArrayList)MsgUtils.getMsgContent(response);

            for (int i=0; i<stdErr.size(); i++)
                logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));

            status = MsgUtils.getMsgStatusCode(response);
            logger.info("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
            if (stdErr.size() > 0) {
                if (stdErr.size() != 1 || !stdErr.get(0).startsWith("Opening")) {
                    return null;
                }

            }
        }

        //Wait until the command has finished
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID);
        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if(response != null) {
                status = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
            }
            return null;
        } else{
            status = (Integer)MsgUtils.getMsgContent(response);
            logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
        }

        // get exitValue
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_STATUS_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID);
        if (response == null) {
            return null;
        } else {
            //return status = (Integer)MsgUtils.getMsgContent(response);
            status = (Integer)MsgUtils.getMsgContent(response);
            logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
        }

        return metadata;
    }

    /** bhpio output strings */
    public static final String BIG_ENDIAN = "BIG_ENDIAN";
    public static final String COMMA = ",";
    public static final String CROSS_SECTION = "CROSS-SECTION";
    public static final String DATA = "data,";
    public static final String FILE = "FILE:";
    public static final String FORMAT = "format";
    public static final String HORIZONS = "HORIZONS:";
    public static final String HORIZON_TYPE = "HORIZON";
    public static final String IS = "is ";
    public static final String LITTLE_ENDIAN = "LITTLE_ENDIAN";
    public static final String NAME = "Name:";
    public static final String ORDER = "order";
    public static final String MAP_VIEW = "MAP-VIEW";
    public static final String NUM_LAYERS = "Number of Layers:";
    public static final String PATH = "path=";
    public static final String PROPERTIES = "PROPERTIES:";
    public static final String PROPERTY_TYPE = "PROPERTY";
    public static final String SEISMIC_TYPE = "SEISMIC";
    public static final String STORED_IN = "stored in";
    public static final String WRITTEN_IN = "is written in";
    public static final String TYPE = "Type:";
    public static final String R_TYPE = "R";
    public static final String I_TYPE = "I";
    public static final String DOES_NOT_EXIST = "does not exist";
    public static final String FILENAME = "filename=";
    
    /**
     * Parse the output of the bhpio command, i.e., the metadata for a BHP-SU file.
     * Note: Method public so it can be directly tested.
     * @param list Lines of bhpio stdout
     * @param filePath Full path of the dataset. Note: This is NOT the path in the bhpio output.
     * @return geoFile's metadata
     */
    static public GeoFileMetadata parseBhpioOutput(List<String> list, String filePath) {
        if (list == null || list.size() == 0)
            return null;

        GeoFileMetadata metadata = new SeismicFileMetadata();
        metadata.setGeoFilePath(filePath);
        
        boolean isSeismicFile = false, isModelFile = false;
        String datasetName = "";
        int idx = 0, idx2 = 0;
        String dataType = "";

        for (String line : list) {
            if (line.contains(DOES_NOT_EXIST)) {
                idx = line.indexOf(FILENAME);
                //extract the reason the metadata cannot be obtained
                metadata.setErrMsg(line.substring(idx+9));
                return metadata;
            }
            
            if (line.contains(STORED_IN)) {
                idx = line.indexOf(IS);
                datasetName = line.substring(0, idx-1);
                metadata.setGeoFileName(datasetName);
                idx2 = line.indexOf(DATA);
                dataType = line.substring(idx+3, idx2-1);
                if (dataType.equals(SEISMIC_TYPE) || dataType.equals(HORIZON_TYPE)) {
                    isSeismicFile = true;
                    metadata.setGeoFileType(GeoFileType.SEISMIC_GEOFILE);
                } else if (dataType.equals(PROPERTY_TYPE)) {
                    metadata = new ModelFileMetadata();
                    isModelFile = true;
                    metadata.setGeoFilePath(filePath);
                    metadata.setGeoFileName(datasetName);
                    metadata.setGeoFileType(GeoFileType.MODEL_GEOFILE);
                } else {
                    //error: unknown data type
                }

                idx = line.indexOf(STORED_IN);
                idx2 = line.indexOf(ORDER);
                String order = line.substring(idx+STORED_IN.length()+1, idx2-1);
                if (order.equals(CROSS_SECTION))
                    metadata.setGeoDataOrder(GeoDataOrder.CROSS_SECTION_ORDER);
                else if (order.equals(MAP_VIEW))
                    metadata.setGeoDataOrder(GeoDataOrder.MAP_VIEW_ORDER);
                    else {
                        //error: unknown data order
                    }

                continue;
            }

            if (line.contains(WRITTEN_IN)) {
                idx = line.indexOf(WRITTEN_IN);
                if (!line.startsWith(datasetName)) {
                    //validation error
                }
                idx2 = line.indexOf(FORMAT);
                String endian = line.substring(idx+WRITTEN_IN.length()+1, idx2-1);
                if (endian.equals(BIG_ENDIAN))
                    metadata.setEndianFormat(ByteOrder.BIG_ENDIAN);
                else if (endian.equals(LITTLE_ENDIAN))
                    metadata.setEndianFormat(ByteOrder.LITTLE_ENDIAN);
                    else {
                        //error: unknown endianess
                    }
                continue;
            }

            if (line.contains(NUM_LAYERS) && isModelFile) {
                idx = line.indexOf(NUM_LAYERS);
                idx += NUM_LAYERS.length() + 1;
                int numLayers = Integer.valueOf(line.substring(idx)).intValue();
                ((ModelFileMetadata)metadata).setNumLayers(numLayers);

                continue;
            }

            if (line.contains(PROPERTIES) && isModelFile) {
                idx = line.indexOf(PROPERTIES);
                String[] properties = null;
                line = line.substring(PROPERTIES.length()+1);
                properties = line.trim().split(" +");
                if (properties != null) {
                    for (String property : properties)
                        ((ModelFileMetadata)metadata).addProperty(property);
                }

                continue;
            }

            if (line.contains(HORIZONS) && isSeismicFile) {
                idx = line.indexOf(HORIZONS);
                String[] horizons = null;
                line = line.substring(HORIZONS.length()+1);
                horizons = line.trim().split(" +");
                if (horizons != null) {
                    for (String horizon : horizons)
                        ((SeismicFileMetadata)metadata).addHorizon(horizon);
                }

                continue;
            }

            if (line.contains(NAME)) {
                idx = line.indexOf(NAME);
                String[] index = line.substring(idx+NAME.length()+1).split(",");
                String name = index[0];
                String min = index[1].substring(index[1].indexOf(":")+1).trim();
                String max = index[2].substring(index[2].indexOf(":")+1).trim();
                String incr = index[3].substring(index[3].indexOf(":")+1).trim();
                GeoKeyType geoKeyType = GeoKeyType.NO_TYPE;
                if (index.length > 4) {
                    String keyType = index[4].substring(index[4].indexOf(":")+1).trim();
                    if (keyType.equals(R_TYPE)) geoKeyType = GeoKeyType.REGULAR_TYPE;
                    else if (keyType.equals(I_TYPE)) geoKeyType = GeoKeyType.IRREGULAR_TYPE;
                }

                metadata.addKey(name);
                metadata.addKeyRange(name, Double.valueOf(min).doubleValue(),
                    Double.valueOf(max).doubleValue(),
                    Double.valueOf(incr).doubleValue(),
                    geoKeyType);

                continue;
            }
        }

        return metadata;
    }
}
