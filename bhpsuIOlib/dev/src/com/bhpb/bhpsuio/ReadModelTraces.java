/*
###########################################################################
# bhpsuIOlib - Read and write geophysical data (seismic, horizon) in BHP-SU
# format.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.bhpsuio;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.SocketChannel;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.qiworkbench.api.IComponentUtils;
import com.bhpb.qiworkbench.api.IServerJobService;
import com.bhpb.qiworkbench.api.IServletDispatcher;
import com.bhpb.qiworkbench.server.SocketManager.BoundIOSocket;
import com.bhpb.qiworkbench.server.util.WriteDataToSocket;

/**
 * Read the model traces for a BHP-SU model dataset with given choosen ranges..
 */
public class ReadModelTraces {
    private static Logger logger = Logger.getLogger(ReadModelTraces.class.getName());
    //The number of trace blocks to read and process at once.
    private static final int TRACE_BLOCKS_PER_READ = 1000;

    /**
     * Generate the BHP-SU script for set of traces from a dataset
     * @param args List of input arguments:
     * <ul>
     * <ul>
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] First trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of traces in
     * the range.</li>
     * <li>[5] Number of traces to read</li>
     * <li>[6] List of properties to be included in a model trace. An empty list
     * implies all properties in the geoFile</li>
     * </ul>
     * @param tempPath Full path of the temp file to write the traces to.
     * @return List:
     * <ul>
     * <li>location preference: remote</li>
     * <li>BHP-SU script as a list of commands; null if cannot generate the script</li>
     * </ul>
     */
    static private ArrayList<String> genTraceScript(ArrayList args, String tempPath) {
        DatasetProperties properties = (DatasetProperties)args.get(2);
        ModelFileMetadata metadata = (ModelFileMetadata)properties.getMetadata();

        ArrayList<String> script = new ArrayList<String>();
        script.add("bash");
        script.add("-c");
        //Generate the BHP-SU command to read traces
        //Note: Don't take the selected model property into account when generating the command
        String bhpsuCmd = ReadFileSummary.genReadTracesCmd(properties, true);

        ArrayList<String> modelProps = (ArrayList<String>)args.get(6);
        int num = modelProps.size();
        if (num == 0) {
            num = metadata.getNumProps();
            modelProps = metadata.getProperties();
        }

        bhpsuCmd += " properties=";
        for (int i=0; i<num; i++) {
            bhpsuCmd += modelProps.get(i);
            if (i != num-1) bhpsuCmd += ",";
        }

        // read data to temp file
        script.add(" source ~/.bashrc; "+ bhpsuCmd + " > " + tempPath + " out=" + tempPath);
        logger.info("read model traces cmd: "+script);
        return script;
    }
    
//TODO: Get the size of the trace header from the BHP-SU metadata.
    /** Size of a trace header block in bytes. */
    public static final int TRACE_HEADER_SIZE = 240;

    /**
     * Read a range of model traces from a model geoFile using socket IO. First, the traces
     * are obtained using bhpread and written to a temp file from which they
     * are read and transmitted over the socket. The temp file is deleted afterwards.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>r
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile (model)</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] First trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of traces in
     * the range.</li>
     * <li>[5] Number of traces to read</li>
     * <li>[6] List of properties to be included in a model trace. An empty list
     * implies all properties in the geoFile</li>
     * </ul>
     * @return The empty string if all the traces were successfully read; otherwise, the
     * reason the read failed.
     */
    static public String _readModelTraces(IServletDispatcher servletDispatcher, IComponentUtils componentUtils, ArrayList args) {
        /** Server side channels to use for socket IO. */
        BoundIOSocket boundIOSocket = (BoundIOSocket)args.get(0);
        int port = boundIOSocket.getServerSocketChannel().socket().getLocalPort();
        logger.info("BhpSuIO::readModelTraces Read model traces from server communication port="+port);
        //The last parameter is the workbench session which uniquely
        //identifies the client.
        boundIOSocket.setWorkbenchSessionID((String)args.get(args.size()-1)); 

        /** Error message stating why read failed */
        String opStatus = "";

        //Generate the path of the temp file to hold the traces
        String tmpDir = System.getProperty("java.io.tmpdir");
        String tempPath = "";
        File tempFile = null;
        try {
            tempFile = File.createTempFile("BHP_SU", ".tmp", new File(tmpDir));
            tempPath = tempFile.getAbsolutePath();
        } catch (Exception ex) {
            logger.info("Caught exception while attempting to create tempFile or get tempPath: " + tempFile + ", " + tempPath);
            logger.info("Exception message: " + ex.getMessage());
        }

        //Generate the bhpread script for obtaining the traces and writing them
        //to a temp file.
        ArrayList<String> params = genTraceScript(args, tempPath);

        //Execute the script using a remote job service.
        //NOTE: Must be remote for this is where the temp file containing the
        //traces is created which must then be sent to the client via socket IO.
        //sent to the client via socket IO.

        // Call remote JobService directly instead of routing request since class is not
        // a qiConent and has no Messaging Manager
        IServerJobService jobService = servletDispatcher.acquireRemoteJobService();
        // create a job ID for the job service
        String jobID = componentUtils.genJobID();
        jobService.setJobID(jobID);

        ArrayList<String> stdErr;
        ArrayList<String> stdOut;

        // Compose the job request. Note: CIDs of producer and consumer irrelevant
        // since not routing request.
        IQiWorkbenchMsg request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        IQiWorkbenchMsg response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        // Get jobs's stderr
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }
        StringBuffer errorOutput = new StringBuffer();
        stdErr = (ArrayList<String>)response.getContent();
        for (int i=0; i<stdErr.size(); i++) errorOutput.append(stdErr.get(i));

        // Ignore stdOut

        // Wait until the command has finished
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        int exitValue = ((Integer)response.getContent()).intValue();

        if (exitValue != 0) {     // exit abnormal
            //delete the output file
            File file = new File(tempPath);
            file.delete();
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return "ERROR: Read BHP_SU traces: " + errorOutput.toString();
        }

        // Release the job
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.RELEASE_JOB_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        logger.info("Invoking jobService.processMsg directly.");
        response = jobService.processMsg(request);
        logger.info("Got response to RELEASE_JOB_CMD");
        if (response == null || response.isAbnormalStatus()) {
            logger.info("Response was abnormal or null, deleting temp file and returning the response");
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        // return inactive job service to pool
        logger.info("Releasing job service");
        servletDispatcher.releaseRemoteJobService(jobService);

        //Write the traces in the temp file to the socket.
        WriteDataToSocket writer = new WriteDataToSocket(boundIOSocket);

        //Establish the server socket channel on the port so client can connect;
        //otherwise, there can be an arbitrary delay until the data is read.
        logger.info("Establishing server socket on port: " + port);
        String status= writer.establishServerSocket();

        if (!status.equals("")) {
            logger.info("BhpSuIO::readModelTraces Cannot establish the server port="+port);

            tempFile.delete();
            writer.releaseSocket();
            return status;
        } else
            logger.info("BhpSuIO::readModelTraces Established the server port="+port);

        int start = Integer.parseInt((String)args.get(4));
        int num = Integer.parseInt((String)args.get(5));
        ArrayList<String> modelProps = (ArrayList<String>)args.get(6);
        DatasetProperties properties = (DatasetProperties)args.get(2);
        GeoFileDataSummary summary = properties.getSummary();

        ModelFileMetadata metadata = (ModelFileMetadata)properties.getMetadata();
        int numModelProps = modelProps.size();
        if (numModelProps == 0) {
            numModelProps = metadata.getNumProps();
        }
        //A model trace contains for each model property numLayers
        //values + the tops for each layer + bottom of last layer
        int samplesPerTrace = summary.getSamplesPerTrace();
        int len = summary.getSampleFormatLength();
        int sampleSize = samplesPerTrace * len;
        int traceBlockSize = TRACE_HEADER_SIZE + sampleSize;
        int offset = traceBlockSize * start;
        int length = traceBlockSize * (num - start);

        RandomAccessFile file = null;
        try {
            if (tempPath == null) {
                logger.warning("tempPath is null!");
            }
            logger.info("Attempting to open tempPath: " + tempPath);
            file = new RandomAccessFile(tempPath, "r");
            
            byte[] data = new byte[traceBlockSize*TRACE_BLOCKS_PER_READ];
            
            int blocksRead = 0;
            int blocksUnread = num;
            while (blocksRead < num) {
                logger.info("Seeking offset: " + offset);
                file.seek(offset);
                int blocksToRead = (blocksUnread < TRACE_BLOCKS_PER_READ) ? blocksUnread : TRACE_BLOCKS_PER_READ;
                
                logger.info("Reading data from temp file...");
                file.readFully(data, 0, blocksToRead*traceBlockSize);
                logger.info("Writing data to server socket...");
                //write trace blocks to SocketChannel
                //NOTE: The last buffer written may be partially full
                opStatus = writer.write(data);
                //Check if write to socket performed successfully
                if (!opStatus.equals("")) {
                    logger.info("Closing temp file and releasing server socket; socket write failed: status="+opStatus);
                    file.close();
                    writer.releaseSocket();
                    return opStatus;
                }
                
                blocksRead += blocksToRead;
                blocksUnread -= blocksToRead;
                offset += blocksToRead*traceBlockSize;
            }

            file.close();
            tempFile.delete();
            writer.releaseSocket();

            return opStatus;
        } catch (IOException ioe) {
            logger.severe("BhpSuIO::readModelTraces: exception: "+ioe);

            tempFile.delete();
            return "Error obtaining BHP-SU model trace(s): "+ioe.getMessage();
        } finally {
            if (file != null) {
                try {
                    file.close();
                } catch (Exception ex) {
                    logger.warning("Caught exception while attempting to close dat file: " + ex.getMessage());
                }
            }
        }
    }
}
