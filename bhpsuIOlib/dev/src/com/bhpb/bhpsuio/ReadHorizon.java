/*
###########################################################################
# bhpsuIOlib - Read and write geophysical data (seismic, horizon) in BHP-SU
# format.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.bhpsuio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentUtils;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IServerJobService;
import com.bhpb.qiworkbench.api.IServletDispatcher;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.server.SocketManager.BoundIOSocket;
import com.bhpb.qiworkbench.server.util.WriteDataToSocket;

import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

/**
 * Read a horizon stored in cross-section order in a BHP-SU dataset.
 */
public class ReadHorizon {
    private static Logger logger = Logger.getLogger(ReadHorizon.class.getName());

    /**
     * Generate the BHP-SU script for getting a horizon from a dataset
     * @param args List of input arguments:
     * <ul>
     * <ul>
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] horizon name</li>
     * </ul>
     * @param tempPath Full path of the temp file to write the horizon to.
     * @return List:
     * <ul>
     * <li>BHP-SU script as a list of commands; null if cannot generate the script</li>
     * </ul>
     */
    static private ArrayList<String> genHorizonScript(ArrayList args, String tempPath) {
        DatasetProperties properties = (DatasetProperties)args.get(2);
        GeoFileMetadata metadata = properties.getMetadata();

        ArrayList<String> script = new ArrayList<String>();
        script.add("bash");
        script.add("-c");
        //Generate the BHP-SU command to read the horizon
        String bhpsuCmd = genReadHorizonCmd(properties, (String)args.get(4), tempPath);
        
        // read data to temp file
        script.add(" source ~/.bashrc; "+ bhpsuCmd);
        logger.info("script to read a horizon: "+script);
        return script;
    }

    /**
     * Generate the BHP-SU command for reading a horizon from a dataset.
     * @param DatasetProperties: properties geoFile properties
     * @param horizonName Name of the horizon to be read
     * @param tempPath Temporary .xyz file for holding horizon
     * @return BHP-SU bhpread command
     */
    static private String genReadHorizonCmd(DatasetProperties properties, String horizonName, String tempPath) {;
        //Generate the BHP-SU command to read the horizon traces to a temp file
        String bhpsuCmd = genReadHorizonTracesCmd(properties, horizonName);
        
        GeoFileMetadata metadata = properties.getMetadata();
        int num = metadata.getNumKeys();

        bhpsuCmd += " | bhpstorehdr keys=";
        for (int i=0; i<num-1; i++) {
            String key = properties.getOrderedKey(i+1);
            bhpsuCmd += key;
            if (i != num-2) bhpsuCmd += ",";
        }

        bhpsuCmd += " horizons=yes file=" + tempPath;

        bhpsuCmd += " | surange";

        return bhpsuCmd;
    }
    
    /**
     * Generate scrtipt to read a cross-section horizon's traces into a temp file.
     * @param DatasetProperties: properties geoFile properties
     * @param horizonName Name of the horizon to be read
     * @param tempPath Temporary file for holding horizon traces
     * @return List:
     * <ul>
     * <li>BHP-SU script as a list of commands; null if cannot generate the script</li>
     * </ul>
     */
    static public ArrayList<String> genReadHorizonToFileScript(DatasetProperties properties, String horizonName, String tempPath) {
        ArrayList<String> script = new ArrayList<String>();
        script.add("bash");
        script.add("-c");
        //Generate the BHP-SU command to read the horizon's traces
        String bhpsuCmd = genReadHorizonTracesCmd(properties, horizonName);
        
        script.add(" source ~/.bashrc; "+ bhpsuCmd + " > " + tempPath + " out=" + tempPath);
        logger.info("script to read a horizon to a temp file: "+script);
        
        return script;
    }
    
    /**
     * Generate the BHP-SU command for reading a horizon's traces.
     * @param DatasetProperties: properties geoFile properties
     * @param horizonName Name of the horizon to be read
     * @return BHP-SU bhpread command
     */
    static private String genReadHorizonTracesCmd(DatasetProperties properties, String horizonName) {
        GeoFileMetadata metadata = properties.getMetadata();

        //Generate the BHP-SU command to read the horizon traces
        String bhpsuCmd = "bhpread pathlist="+metadata.getGeoFilePath()+" filename="+metadata.getGeoFileName();

        bhpsuCmd += " keys=";
        int num = metadata.getNumKeys();
        ArrayList<String>keys = metadata.getKeys();
        for (int i=0; i<num; i++) {
            bhpsuCmd += properties.getOrderedKey(i+1);
            if (i != num-1) bhpsuCmd += ",";
        }

        bhpsuCmd += " keylist=";
        boolean processedArbTrav = false;
        for (int i=0; i<num; i++) {
            String key = properties.getOrderedKey(i+1);             
            AbstractKeyRange keyRange = properties.getKeyChosenRange(key);
            if (properties.isRange(key)) {
            /*
                if (keyRange.isUnlimited()) {
                    bhpsuCmd += "*";
                } else {
                double min = ((DiscreteKeyRange)keyRange).getMin();
                    double max = ((DiscreteKeyRange)keyRange).getMax();
                    bhpsuCmd += (min == max) ? min : (min + "-" + max + "[" + ((DiscreteKeyRange)keyRange).getIncr() + "]");
                }
            */
                bhpsuCmd += keyRange.toScriptString();
                if (i != num-1) bhpsuCmd += ":";
            } else
            if (properties.isArbTrav(key) && !processedArbTrav) {
                String key1 = key;
                String key2 = "";
                //find the other key that is also an arbitrary traverse
                //Note: only 2 keys can be part of an arbitrary traverse
                for (int j=i+1; j<num; j++) {
                    key2 = keys.get(j);
                    if (properties.isArbTrav(key2)) break;
                }
                if (key2.equals("")) {
                    //this should never happen since the arb traverses have been validated
                }
                int n = properties.getArbTravSize();
                for (int j=0; j<n; j++) {
                    String val1 = String.valueOf(properties.getKeyChosenArbTrav(key1).getArbTravVal(j));
                    String val2 = String.valueOf(properties.getKeyChosenArbTrav(key2).getArbTravVal(j));
                    bhpsuCmd += val1 + "," + val2;
                    if (j != n-1) bhpsuCmd += "\\^";
                }

                processedArbTrav = true;
                if (i != num-1) bhpsuCmd += ":";
            }
        }
        
        //bugfix: omit the endian= parameter; always use the server's native byte order
        
        bhpsuCmd += " horizons=" + horizonName;
        
        return bhpsuCmd;
    }

    /**
     * Read a horizon from a geoFile using socket IO. First, the horizon
     * is obtained using bhpread and written to a temp file from which it
     * is read and transmitted over the socket. The temp file is deleted afterwards.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] horizon name</li>
     * </ul>
     * @return The empty string if the horizon was successfully read; otherwise, the
     * reason the read failed.
     */
    static public String _readHorizon(IServletDispatcher servletDispatcher, IComponentUtils componentUtils, ArrayList args) {
        /** Server side channels to use for socket IO. */
        BoundIOSocket boundIOSocket = (BoundIOSocket)args.get(0);
        int port = boundIOSocket.getServerSocketChannel().socket().getLocalPort();
        logger.info("BhpSuIO::readHorizon Write horizon to server communication port="+port);
        //The last parameter is the workbench session which uniquely
        //identifies the client.
        boundIOSocket.setWorkbenchSessionID((String)args.get(args.size()-1));  

        /** Error message stating why read failed */
        String opStatus = "";

        //Generate the path of the temp file to hold the horizon: tmp_<horizonName>.xyz
        String tmpDir = System.getProperty("java.io.tmpdir");
        String tempPath = "";
        File tempFile = null;
        try {
            tempFile = File.createTempFile("tmp_"+(String)args.get(4)+"_", ".xyz", new File(tmpDir));
            tempPath = tempFile.getAbsolutePath();
        } catch (Exception ex) {
        }

        //Generate the bhpread script for obtaining the horizon and writing it
        //to a temp file.
        ArrayList<String> params = genHorizonScript(args, tempPath);
//      logger.info("horizon script: "+params);

        //Execute the script using a remote job service.
        //NOTE: Must be remote for this is where the temp file containing the
        //horizon is created which must then be sent to the client via socket IO.
        //sent to the client via socket IO.

        // Call remote JobService directly instead of routing request since class is not
        // a qiConent and has no Messaging Manager
        IServerJobService jobService = servletDispatcher.acquireRemoteJobService();
        // create a job ID for the job service
        String jobID = componentUtils.genJobID();
        jobService.setJobID(jobID);

        ArrayList<String> stdErr;
        ArrayList<String> stdOut;

        // Compose the job request. Note: CIDs of producer and consumer irrelevant
        // since not routing request.
        IQiWorkbenchMsg request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        IQiWorkbenchMsg response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        // Get jobs's stderr
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }
        StringBuffer errorOutput = new StringBuffer();
        stdErr = (ArrayList<String>)response.getContent();
        for (int i=0; i<stdErr.size(); i++) errorOutput.append(stdErr.get(i));

        // Ignore stdOut

        // Wait until the command has finished
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        int exitValue = ((Integer)response.getContent()).intValue();

        if (exitValue != 0) {     // exit abnormal
            //delete the output file
            File file = new File(tempPath);
            file.delete();
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return "ERROR: Read BHP_SU horizon: " + errorOutput.toString();
        }

        // Release the job
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.RELEASE_JOB_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        logger.info("Invoking jobService.processMsg directly.");
        response = jobService.processMsg(request);
        logger.info("Got response to RELEASE_JOB_CMD");
        if (response == null || response.isAbnormalStatus()) {
            logger.info("Response was abnormal or null, deleting temp file and returning the response");
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        // return inactive job service to pool
        logger.info("Releasing job service");
        servletDispatcher.releaseRemoteJobService(jobService);

        //Write the horizon in the temp file to the socket.
        WriteDataToSocket writer = new WriteDataToSocket(boundIOSocket);

        //Establish the server socket channel on the port so client can connect;
        //otherwise, there can be an arbitrary delay until the data is read.
        logger.info("Establishing server socket on port: " + port);
        String status= writer.establishServerSocket();
        
        if (!status.equals("")) {
            logger.info("BhpSuIO::readHorizon Cannot establish the server port="+port);

            tempFile.delete();
            writer.releaseSocket();
            return status;
        } else
            logger.info("BhpSuIO::readHorizon Established the server port="+port);

        DatasetProperties properties = (DatasetProperties)args.get(2);
        GeoFileDataSummary summary = properties.getSummary();
        int len = summary.getSampleFormatLength();
        int numTraces = summary.getNumberOfTraces();
        int length = numTraces * len;

        BufferedReader br = null;
        ByteBuffer horizon = ByteBuffer.allocate(length);
        //bugfix: the horizon byteOrder will now be written in the server's
        //native order, not the metadata's nativeOrder.  This makes all
        //floating-point data received by the client consistent
        ByteOrder byteOrder = ByteOrder.nativeOrder();
        if (byteOrder == ByteOrder.LITTLE_ENDIAN) {
            horizon.order(ByteOrder.LITTLE_ENDIAN);
        }
        try {
            FileInputStream fis = new FileInputStream(tempFile);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read horizon file line by line
            while (line != null) {
                StringTokenizer st = new StringTokenizer(line);
                int cnt = st.countTokens();
                //extract horizon (3rd token) as a float
                st.nextToken(); st.nextToken();
                String sample = st.nextToken();
                if (cnt != 3) return "Malformed horizon file: "+tempPath;
                float val = 0;
                try {
                    val = Float.parseFloat(sample);
                } catch (NumberFormatException nfe) {
                }
                horizon.putFloat(val);
                line = br.readLine();
            }

            //write horizon to SocketChannel
            opStatus = writer.write(horizon.array());
            
            writer.releaseSocket();

            br.close();
            tempFile.delete();

            return opStatus;
        } catch (IOException ioe) {
            logger.severe("BhpSuIO::readHorizon: exception: "+ioe);

            tempFile.delete();
            return "Error obtaining BHP-SU horizon: "+ioe.getMessage();
        } finally {
            try {
                if (br != null) br.close();
            } catch (Exception ex) {
                logger.warning("Caught exception while attempting to close horizon file: " + ex.getMessage());
            }
        }
    }
    
    /**
     * Read a horizon in cross-section view from a geoFile into a temp file 
     * using bhpread. The temp file is not deleted.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] location preference (remote)</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] horizon name</li>
     * <li>[5] horizon values (ignored)</li>
     * </ul>
     * @param tempFile Temporary file into which horizon is to be read
     * @return The empty string if the horizon was successfully read; otherwise, 
     * the reason the read failed.
     */
    static public String _getXsecHorizon(IServletDispatcher servletDispatcher, IComponentUtils componentUtils, ArrayList args, File tempFile) {
        /** Error message stating why read failed */
        String opStatus = "";

        //Generate the bhpread script for obtaining the horizon and writing it
        //to a temp file.
        ArrayList<String> params = genHorizonScript(args, tempFile.getAbsolutePath());
//      logger.info("horizon script: "+params);

        //Execute the script using a remote job service.
        //NOTE: Must be remote for this is where the temp file containing the
        //horizon is created which must then be sent to the client via socket

        // Call remote JobService directly instead of routing request since class is not
        // a qiConent and has no Messaging Manager
        IServerJobService jobService = servletDispatcher.acquireRemoteJobService();
        // create a job ID for the job service
        String jobID = componentUtils.genJobID();
        jobService.setJobID(jobID);

        ArrayList<String> stdErr;
        ArrayList<String> stdOut;

        // Compose the job request. Note: CIDs of producer and consumer irrelevant
        // since not routing request.
        IQiWorkbenchMsg request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.SUBMIT_JOB_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        IQiWorkbenchMsg response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        // Get jobs's stderr
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }
        StringBuffer errorOutput = new StringBuffer();
        stdErr = (ArrayList<String>)response.getContent();
        for (int i=0; i<stdErr.size(); i++) errorOutput.append(stdErr.get(i));

        // Ignore stdOut

        // Wait until the command has finished
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        response = jobService.processMsg(request);
        if (response == null || response.isAbnormalStatus()) {
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        int exitValue = ((Integer)response.getContent()).intValue();

        if (exitValue != 0) {     // exit abnormal
            //delete the output file
            tempFile.delete();
            
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            return "ERROR: Read BHP_SU horizon: " + errorOutput.toString();
        }

        // Release the job
        params.clear();
        params.add(jobID);
        request = new QiWorkbenchMsg("", "",
                   QIWConstants.CMD_MSG, QIWConstants.RELEASE_JOB_CMD,
                   MsgUtils.genMsgID(), QIWConstants.ARRAYLIST_TYPE, params, true);
        logger.info("Invoking jobService.processMsg directly.");
        response = jobService.processMsg(request);
        logger.info("Got response to RELEASE_JOB_CMD");
        if (response == null || response.isAbnormalStatus()) {
            logger.info("Response was abnormal or null, deleting temp file and returning the response");
            // return inactive job service to pool
            servletDispatcher.releaseRemoteJobService(jobService);

            tempFile.delete();
            return (String)response.getContent();
        }

        // return inactive job service to pool
        logger.info("Releasing job service");
        servletDispatcher.releaseRemoteJobService(jobService);

        return "";
    }
}
