/*
###########################################################################
# bhpsuIOlib - Read and write geophysical data (seismic, horizon) in BHP-SU
# format.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.bhpsuio;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.api.IMessagingManager;

import com.bhpb.geoio.IGeoFileIO;
import static com.bhpb.qiworkbench.api.DataSystemConstants.*;
import com.bhpb.geoio.datasystems.GeoIO;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.qiworkbench.api.IComponentUtils;
import com.bhpb.qiworkbench.api.IServletDispatcher;

public class BhpSuIO implements IGeoFileIO {
    private static Logger logger = Logger.getLogger(BhpSuIO.class.getName());
    
    public BhpSuIO() {
        //Initialize the format descriptor for BHP-SU, i.e., parse the
        //self-describing XML files
        //Note:initFormatIO() will return if the XML descriptors have already
        //been parsed.
        GeoIO.getInstance().initFormatIO(BHP_SU_FORMAT);
    }

    /**
     * Get the metadata for a geoFile by calling BHP-SU bhpio given the file's
     * full path, then retrieve the output, parse the output and form the metadata
     * object.
     * @param args List of input arguments:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] full path of the geoFile</li>
     * </ul>
     * @param messagingMgr MessagingManager of the geoIO Service requesting the metadata.
     * @return The geoFile's metadata if it is successfully obtained; otherwise, null..
     */
    public GeoFileMetadata getFileMetadata(ArrayList<String> args, IMessagingManager messagingMgr) {
        return ReadFileMetadata._getFileMetadata(args, messagingMgr);
    }

    /**
     * Get the summary for traces in a geoFile by calling the BHP-SU bhpread summary
     * command, then retrieve the output, parse the output and form the data summary
     * object.
     * @param args List of input arguments:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] DatasetProperties: geoFile's properties which contains its metadata</li>
     * </ul>
     * @param messagingMgr MessagingManager of the geoIO Service requesting the metadata.
     * @return The geoFile's trace summary if it is successfully obtained; otherwise, null..
     */
    public GeoFileDataSummary getDataSummary(ArrayList args, IMessagingManager messagingMgr) {
        return ReadFileSummary._getDataSummary(args, messagingMgr);
    }

    /**
     * Read a range of seismic traces from a seismic geoFile using socket IO. First, the traces
     * are obtained using bhpread and written to a temp file from which they
     * are read and transmitted over the socket. The temp file is deleted afterwards.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] First trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of traces in
     * the range.</li>
     * <li>[5] Number of traces to read</li>
     * </ul>
     * @return The empty string if all the traces were successfully read; otherwise, the
     * reason the read failed.
     */
    public String readTraces(IServletDispatcher servletDispatcher, IComponentUtils componentUtils,
            ArrayList args) {
        return ReadTraces._readTraces(servletDispatcher, componentUtils, args);
    }

    /**
     * Read a horizon from a geoFile using socket IO. First, the horizon
     * is obtained using bhpread and written to a temp file from which they
     * are read and transmitted over the socket. The temp file is deleted afterwards.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] Horizon name</li>
     * </ul>
     * @return The empty string if the horizon was successfully read; otherwise, the
     * reason the read failed.
     */
    public String readHorizon(IServletDispatcher servletDispatcher, IComponentUtils componentUtils,
            ArrayList args) {
        return ReadHorizon._readHorizon(servletDispatcher, componentUtils, args);
    }
    
    /**
     * Read a horizon om cross-section view from a geoFile into a temp file 
     * using bhpread. The temp file is not deleted.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] location preference (remote)</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] horizon name</li>
     * <li>[5] horizon values (ignored)</li>
     * </ul>
     * @param tempFile Temporary file into which horizon is to be read
     * @return The empty string if the horizon was successfully read; otherwise, 
     * the reason the read failed.
     */
    static public String getXsecHorizon(IServletDispatcher servletDispatcher, IComponentUtils componentUtils, ArrayList args, File tempFile) {
        return ReadHorizon._getXsecHorizon(servletDispatcher, componentUtils, args, tempFile);
    }

    /**
     * Read horizon traces from a geoFile using socket IO. First, the horizon traces
     * are obtained using bhpread and written to a temp file from which they
     * are read and transmitted over the socket. The temp file is deleted afterwards.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] IO Preference - local or remote</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties (contains the view order)</li>
     * <li>[3] type of geoData to be read (Horizons)</li>
     * <li>[4] Horizon name</li>
     * <li>[5] First horizon trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of horizon traces in
     * the range.</li>
     * <li>[6] Number of horizon traces to read</li>
     * </ul>
     * @return The empty string if the horizon traces were successfully read; otherwise, the
     * reason the read failed.
     */
    public String readHorizonTraces(IServletDispatcher servletDispatcher, IComponentUtils componentUtils,
            ArrayList args) {
        return ReadHorizonTraces._readHorizonTraces(servletDispatcher, componentUtils, args);
    }

    /**
     * Read a range of model traces from a model geoFile using socket IO. First, the traces
     * are obtained using bhpread and written to a temp file from which they
     * are read and transmitted over the socket. The temp file is deleted afterwards.
     * <p>NOTE: This code is always executed on the server-side.
     * @param args Input parameters:
     * <ul>
     * <li>[0] port to be used for socket IO</li>
     * <li>[1] geoFormat of the geoFile</li>
     * <li>[2] DatasetProperties: geoFile properties</li>
     * <li>[3] type of geoData to be read</li>
     * <li>[4] First trace to read. Traces are in a contiguous range numbered
     * 0..NumberOfTraces-1 where NumberOfTraces is the number of traces in
     * the range.</li>
     * <li>[5] Number of traces to read</li>
     * <li>[6] List of properties to be included in a model trace. An empty list
     * implies all properties in the geoFile</li>
     * </ul>
     * @return The empty string if all the traces were successfully read; otherwise, the
     * reason the read failed.
     */
    public String readModelTraces(IServletDispatcher servletDispatcher, IComponentUtils componentUtils,
            ArrayList args) {
        return ReadModelTraces._readModelTraces(servletDispatcher, componentUtils, args);
    }

    public static void main(String[] args) {
          //do nothing
    }
}
