/*
###########################################################################
# bhpsuIOlib - Read and write geophysical data (seismic, horizon) in BHP-SU
# format.
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.bhpsuio;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.StringTokenizer;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

import static com.bhpb.qiworkbench.api.DataSystemConstants.*;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.SeismicConstants.SampleUnit;
import com.bhpb.geoio.filesystems.FileSystemConstants.MissingTraceOption;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;

/**
 * Read the summary for a BHP-SU dataset with given choosen ranges..
 */
public class ReadFileSummary {
    private static Logger logger = Logger.getLogger(ReadFileSummary.class.getName());

    /**
     * Generate the BHP-SU script for getting a dataset's data summary.
     * @param args List of input arguments:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] DatasetProperties: geoFile's properties which contains its metadata</li>
     * </ul>
     * @return List:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] BHP-SU script as a list of commands; null if cannot generate the script</li>
     * </ul>
     */
    static private ArrayList<String> genDataSummaryScript(ArrayList args) {
        ArrayList<String> script = new ArrayList<String>();

        script.add((String)args.get(0));
        script.add("bash");
        script.add("-c");
        //Note: don't ignore the selected model property or horizon when creating the command.
        String bhpsuCmd = genReadTracesCmd((DatasetProperties)args.get(1), false) + " request=summary";
        script.add(" source ~/.bashrc; " + bhpsuCmd);
        logger.info("summary script: " + script);
        return script;
    }

    /**
     * Get the summary for traces in a geoFile by calling the BHP-SU bhpread summary
     * command, then retrieve the output, parse the output and form the data summary
     * object.
     * @param args List of input arguments:
     * <ul>
     * <li>[0] location preference: local or remote</li>
     * <li>[1] DatasetProperties: geoFile's properties which contains its metadata</li>
     * </ul>
     * @param messagingMgr MessagingManager of the geoIO Service requesting the metadata.
     * @return The geoFile's trace summary if it is successfully obtained; otherwise, null..
     */
    static public GeoFileDataSummary _getDataSummary(ArrayList args, IMessagingManager messagingMgr) {
        GeoFileDataSummary dataSummary = null;
        String locpref = (String)args.get(0);

        ArrayList<String> params = genDataSummaryScript(args);

        // submit job for execution
        int status = 0;
        String errorContent = "";

        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.SUBMIT_JOB_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        //QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,QIWConstants.RESPONSE_TIMEOUT);
        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                //errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
            }
            return null;
        }

        String jobID = (String)MsgUtils.getMsgContent(response);
        status = MsgUtils.getMsgStatusCode(response);
        logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
        logger.info("status of SUBMIT_JOB_CMD normal response " + status);

        //get stdout
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID,QIWConstants.RESPONSE_TIMEOUT);

        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                //errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
            }
            return null;
        } else {
            ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
            status = MsgUtils.getMsgStatusCode(response);
            logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
            dataSummary = parseBhpreadSummaryOutput(stdOut);

        }

        //get stderr
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID,QIWConstants.RESPONSE_TIMEOUT);

        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if (response != null) {
                //errorStatus = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
                return null;
            }
            return null;
        } else {
            ArrayList<String> stdErr = (ArrayList)MsgUtils.getMsgContent(response);

            for (int i=0; i<stdErr.size(); i++)
                logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));

            status = MsgUtils.getMsgStatusCode(response);
            logger.info("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
            if (stdErr.size() > 0) {
                if (stdErr.size() != 1 || !stdErr.get(0).startsWith("Opening")) {
                    return null;
                }

            }
        }

        //Wait until the command has finished
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID,QIWConstants.RESPONSE_TIMEOUT);
        if (response == null || MsgUtils.isResponseAbnormal(response)) {
            if(response != null) {
                status = MsgUtils.getMsgStatusCode(response);
                errorContent = (String)MsgUtils.getMsgContent(response);
                logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
            }
            return null;
        } else{
            status = (Integer)MsgUtils.getMsgContent(response);
            logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
        }

        // get exitValue
        params.clear();
        params.add(locpref);
        params.add(jobID);
        msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_JOB_STATUS_CMD,
                QIWConstants.ARRAYLIST_TYPE, params, true);
        response = messagingMgr.getMatchingResponseWait(msgID,QIWConstants.RESPONSE_TIMEOUT);
        if (response == null) {
            return null;
        } else {
            //return status = (Integer)MsgUtils.getMsgContent(response);
            status = (Integer)MsgUtils.getMsgContent(response);
            logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
        }

        return dataSummary;
    }

    /**
     * Parse the output of the bhpread summary command, i.e., the data metadata for a BHP-SU file.
     * Note: Method public so it can be directly tested.
     * @param list Lines of bhpread stdout
     * @return geoFile's trace summary
     */
    static public GeoFileDataSummary parseBhpreadSummaryOutput(List<String> list) {
        GeoFileDataSummary dataSummary = new GeoFileDataSummary();

        if (list == null || list.size() <= 0)
            return dataSummary;

        //There should only be 1 line of output, namely, the summary info, each
        //value separated by a space:
        //  min, max, mean, rms, samples-per-trace, number of traces, sample units, time/dpth interval, start and end time/depth
        String summaryInfo = list.get(0);

        //get default values for trace stats
        double min = dataSummary.getMinAmplitude();
        double max = dataSummary.getMaxAmplitude();
        double avg = dataSummary.getAverage();
        double rms = dataSummary.getRMS();

        StringTokenizer tokenizer = new StringTokenizer(summaryInfo);
        int cnt = 0;
        String token = "";
        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();
            switch (cnt) {
                case 0: // min amplitude
                    try {
                        min = Double.parseDouble(token);
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 1: //max amplitude
                    try {
                        max = Double.parseDouble(token);
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 2: // average amplitude
                    try {
                        avg = Double.parseDouble(token);
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 3: // root mean square of amplitude
                    try {
                        rms = Double.parseDouble(token);
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 4: // samples per trace
                    try {
                        dataSummary.setSamplesPerTrace(Integer.parseInt(token));
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 5: // number of traces
                    try {
                        dataSummary.setNumberOfTraces(Integer.parseInt(token));
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 6: // sample units
                    try {
                        int units = Integer.parseInt(token);
                        if (units == 0) dataSummary.setSampleUnits(SampleUnit.TIME_UNIT);
                        else if (units == 1) dataSummary.setSampleUnits(SampleUnit.DEPTH_FEET_UNIT);
                        else if (units == 2) dataSummary.setSampleUnits(SampleUnit.DEPTH_METERS_UNIT);
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 7: // sample rate
                    try {
                        dataSummary.setSampleRate(Double.parseDouble(token));
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                case 8: // sample rate
                    try {
                        dataSummary.setSampleStart(Double.parseDouble(token));
                    } catch (NumberFormatException nfe) {
                        //leave as default value
                    }
                    break;

                default:
                    break;
            }
            cnt++;
        }

        dataSummary.setDataStats(min, max, avg, rms);

        return dataSummary;
    }

    /**
     * Generate the BHP-SU command for reading a set of traces from a dataset.
     * @param properties geoFile properties
     * @param ignoreSelections If true, don't add a properties= or horizons= parameters; otherwise, do
     * add the selected model property or horizon. Adding them will affect the calculated summary values.
     * @return BHP-SU bhpread command
     */
    static public String genReadTracesCmd(DatasetProperties properties, boolean ignoreSelections) {
        GeoFileMetadata metadata = properties.getMetadata();

        //Generate the BHP-SU command to read traces
        String bhpsuCmd = "bhpread pathlist="+metadata.getGeoFilePath()+" filename="+metadata.getGeoFileName();

        if (properties.isSeismic()) {
            bhpsuCmd += " missingdata="+(((SeismicProperties)properties).getMissingTraceOption() == MissingTraceOption.IGNORE_OPTION ? "ignore" : "fill");
            //NOTE: Trace IDs now formed on demand and from info in the trace header, so trace IDs need not be generated for missing traces.
        } else if (properties.isModel() && !ignoreSelections) {
            bhpsuCmd += " properties="+((ModelProperties)properties).getSelectedProperties();
        } else if (properties.isHorizon() && !ignoreSelections) {
            bhpsuCmd += " horizons="+((HorizonProperties)properties).getSelectedHorizon();
        }

        bhpsuCmd += " keys=";
        int num = metadata.getNumKeys();
        ArrayList<String>keys = metadata.getKeys();
        for (int i=0; i<num; i++) {
            bhpsuCmd += properties.getOrderedKey(i+1);
            if (i != num-1) bhpsuCmd += ",";
        }

        bhpsuCmd += " keylist=";
        boolean processedArbTrav = false;
        for (int i=0; i<num; i++) {
            String key = properties.getOrderedKey(i+1);
            AbstractKeyRange keyRange = properties.getKeyChosenRange(key);
            if (properties.isRange(key)) {
            /*
                if (keyRange.isUnlimited()) {
                    bhpsuCmd += "*";
                } else {
                    double min = ((DiscreteKeyRange)keyRange).getMin();
                    double max = ((DiscreteKeyRange)keyRange).getMax();
                    bhpsuCmd += (min == max) ? min : (min + "-" + max + "[" + ((DiscreteKeyRange)keyRange).getIncr() + "]");
                }
            */
                bhpsuCmd += keyRange.toScriptString();
                if (i != num-1) bhpsuCmd += ":";
            } else
            if (properties.isArbTrav(key) && !processedArbTrav) {
                String key1 = key;
                String key2 = "";
                //find the other key that is also an arbitrary traverse
                //Note: only 2 keys can be part of an arbitrary traverse
                for (int j=i+1; j<num; j++) {
                    key2 = keys.get(j);
                    if (properties.isArbTrav(key2)) break;
                }
                if (key2.equals("")) {
                    //this should never happen since the arb traverses have been validated
                }
                int n = properties.getArbTravSize();
                for (int j=0; j<n; j++) {
                    String val1 = String.valueOf(properties.getKeyChosenArbTrav(key1).getArbTravVal(j));
                    String val2 = String.valueOf(properties.getKeyChosenArbTrav(key2).getArbTravVal(j));
                    bhpsuCmd += val1 + "," + val2;
                    if (j != n-1) bhpsuCmd += "\\^";
                }

                processedArbTrav = true;
                if (i != num-1) bhpsuCmd += ":";
            }
        }

        //bugfix: omit the endian= parameter, always use the server's native byte order
        
        bhpsuCmd += " nearest=";
        for (int i=0; i<num; i++) {
            bhpsuCmd += properties.getKeyNearest(keys.get(i)) ? "1" : "0";
            if (i != num-1) bhpsuCmd += ",";
        }

        if (properties instanceof SeismicProperties) {
            bhpsuCmd += " binsize=";
            for (int i=0; i<num; i++) {
                bhpsuCmd += ((SeismicProperties)properties).getKeyBinsize(keys.get(i));
                if (i != num-1) bhpsuCmd += ",";
            }
        }

        return bhpsuCmd;
    }
}
