/*
 * Test the bhpio parser that process its output for various datasets generates
 * the correct geoFile metadata.
 */

package com.bhpb.bhpsuio;

import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Test parsing BHP-SU's bhpio output and creating the geoFile's metadata.
 * @author Gil Hansen (JUnit test)
 */
public class BhpioParserTest extends TestCase {
    /** filepath of various datasets */
    static public final String GATHERS_DATASET = "/Users/glinsky/qiProjects/demo///datasets/gathers.dat";
    static public final String TARANAKI_AMPEXT_DATASET = "/Users/glinsky/qiProjects/demo///datasets/taranaki_ampext.dat";
    static public final String TARANAKI_MODEL_DEPTH_DATASET = "/Users/glinsky/qiProjects/demo///datasets/taranaki_model_depth.dat";
    static public final String TARANAKI_AMPEXT_TRANSPOSE_DATASET = "/Users/glinsky/qiProjects/demo///datasets/taranaki_ampext_transpose.dat";

    /** bhpio output for various datasets */
    static public final String BHPIO_GATHERS = "Bhpio-Gathers.txt";
    static public final String BHPIO_TARANAKI_AMPEXT = "Bhpio-TaranakiAmpext.txt";
    static public final String BHPIO_TARANAKI_MODEL_DEPTH = "Bhpio-TaranakiModelDepth.txt";
    static public final String BHPIO_TARANAKI_AMPEXT_TRANSPOSE = "Bhpio-TaranakiAmpextTranspose.txt";

    /** Results of parsing bhpio output */
    static public final String GATHERS_METADATA = "GathersMetadata.txt";
    static public final String TARANAKI_AMPEXT_METADATA = "TaranakiAmpextMetadata.txt";
    static public final String TARANAKI_MODEL_DEPTH_METADATA = "TaranakiModelDepthMetadata.txt";
    static public final String TARANAKI_AMPEXT_TRANSPOSE_METADATA = "TaranakiAmpextTransposeMetadata.txt";

    static final String baseDir = System.getProperty("basedir");
    static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;

    @Override
    public void setUp() {
    }

    @Override
    public void tearDown() {
    }

    BhpSuIO bhpsuIO = new BhpSuIO();
    String actualMetadata = "";

    /** Test creating metadata for demo Gathers.dat */
    public void testParseBhpioOutput1() {
        //get the actual bhpio output
        ArrayList<String> bhpioOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPIO_GATHERS));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpioOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpio output file: "+BHPIO_GATHERS+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPIO_GATHERS+"; error message: "+ioe.getMessage());
        }

        //parse the bhpio output
        GeoFileMetadata metadata = ReadFileMetadata.parseBhpioOutput(bhpioOutput, GATHERS_DATASET);
        assertNotNull("Failed to create Gathers.dat metadata", metadata);
        actualMetadata = metadata.toString();
        //
        System.out.println("MD\n" + actualMetadata);
        //verify metadata correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+GATHERS_METADATA));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMetadata);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
                assertEquals(GATHERS_METADATA+"'s metadata incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected metadata results file: "+GATHERS_METADATA+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpio output: "+BHPIO_GATHERS+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpio output: "+BHPIO_GATHERS+"; error message: "+e.getMessage());
        }
    }

    public void testParseBhpioOutput2() {
        //get the actual bhpio output
        ArrayList<String> bhpioOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPIO_TARANAKI_AMPEXT));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpioOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpio output file: "+BHPIO_TARANAKI_AMPEXT+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPIO_TARANAKI_AMPEXT+"; error message: "+ioe.getMessage());
        }

        //parse the bhpio output
        GeoFileMetadata metadata = ReadFileMetadata.parseBhpioOutput(bhpioOutput, TARANAKI_AMPEXT_DATASET);
        assertNotNull("Failed to create Gathers.dat metadata", metadata);
        actualMetadata = metadata.toString();
//System.out.println("Actual metadata: "+actualMetadata);
        //verify metadata correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TARANAKI_AMPEXT_METADATA));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMetadata);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                assertEquals(TARANAKI_AMPEXT_METADATA+"'s metadata incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected metadata results file: "+TARANAKI_AMPEXT_METADATA+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpio output: "+BHPIO_TARANAKI_AMPEXT+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpio output: "+BHPIO_TARANAKI_AMPEXT+"; error message: "+e.getMessage());
        }
    }

    public void testParseBhpioOutput3() {
        //get the actual bhpio output
        ArrayList<String> bhpioOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPIO_TARANAKI_AMPEXT_TRANSPOSE));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpioOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpio output file: "+BHPIO_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPIO_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+ioe.getMessage());
        }

        //parse the bhpio output
        GeoFileMetadata metadata = ReadFileMetadata.parseBhpioOutput(bhpioOutput, TARANAKI_AMPEXT_TRANSPOSE_DATASET);
        assertNotNull("Failed to create Gathers.dat metadata", metadata);
        actualMetadata = metadata.toString();
//System.out.println("Actual metadata: "+actualMetadata);
        //verify metadata correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TARANAKI_AMPEXT_TRANSPOSE_METADATA));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMetadata);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                assertEquals(TARANAKI_AMPEXT_TRANSPOSE_METADATA+"'s metadata incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected metadata results file: "+TARANAKI_AMPEXT_TRANSPOSE_METADATA+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpio output: "+BHPIO_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpio output: "+BHPIO_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+e.getMessage());
        }
    }

    public void testParseBhpioOutput4() {
        //get the actual bhpio output
        ArrayList<String> bhpioOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPIO_TARANAKI_MODEL_DEPTH));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpioOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpio output file: "+BHPIO_TARANAKI_MODEL_DEPTH+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPIO_TARANAKI_MODEL_DEPTH+"; error message: "+ioe.getMessage());
        }

        //parse the bhpio output
        GeoFileMetadata metadata = ReadFileMetadata.parseBhpioOutput(bhpioOutput, TARANAKI_MODEL_DEPTH_DATASET);
        assertNotNull("Failed to create Gathers.dat metadata", metadata);
        actualMetadata = metadata.toString();
//System.out.println("Actual metadata: "+actualMetadata);
        //verify metadata correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TARANAKI_MODEL_DEPTH_METADATA));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualMetadata);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                assertEquals(TARANAKI_MODEL_DEPTH_METADATA+"'s metadata incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected metadata results file: "+TARANAKI_MODEL_DEPTH_METADATA+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpio output: "+BHPIO_TARANAKI_MODEL_DEPTH+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpio output: "+BHPIO_TARANAKI_MODEL_DEPTH+"; error message: "+e.getMessage());
        }
    }
}
