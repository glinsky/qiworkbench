/*
 * Test the correct normalized trace IDs are generated for a given set of keys
 * and their chosen ranges..
 */

package com.bhpb.bhpsuio;

import com.bhpb.geoio.datasystems.DataSystemConstants.GeoKeyType;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.BhpSuIOUtils;
import java.nio.ByteOrder;

/**
 * Test parsing BHP-SU's bhpio output and creating the geoFile's metadata.
 * @author Gil Hansen (JUnit test)
 */
public class TraceIdGenerationTest extends TestCase {
    /** Expected trace IDs for 4 keys */
    static public final String TRACE_IDS_4KEYS = "TraceIDs-4Keys.txt";
    static public final String TRACE_IDS_4KEYS2 = "TraceIDs-4Keys2.txt";

    static final String baseDir = System.getProperty("basedir");
    static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;

    @Override
    public void setUp() {
    }

    @Override
    public void tearDown() {
    }

    BhpSuIO bhpsuIO = new BhpSuIO();

    /** Test generating trace IDs for 4 keys and their chosen ranges which is different from the natural ordering. */
    public void testTraceIdGen1() {
        //generate metadata
        SeismicFileMetadata metadata = new SeismicFileMetadata();
        metadata.addKey("ep");
        metadata.addKey("cdp");
        metadata.addKey("offset");
        metadata.addKey("tracl");
        metadata.addKeyRange("ep", 3015, 3016, 1, GeoKeyType.REGULAR_TYPE);
        metadata.addKeyRange("cdp", 1350, 1450, 1, GeoKeyType.REGULAR_TYPE);
        metadata.addKeyRange("offset", 50, 3950, 100, GeoKeyType.IRREGULAR_TYPE);
        metadata.addKeyRange("tracl", 0.0, 8000.0, 4.0, GeoKeyType.NO_TYPE);
        metadata.setGeoFilePath("/scratch/qiProjects/demo/seismic/gathers.dat");
        metadata.setGeoFileName("gathers");
        metadata.setGeoFileSuffix(".dat");
        metadata.setGeoDataOrder(GeoDataOrder.CROSS_SECTION_ORDER);
        metadata.setEndianFormat(ByteOrder.BIG_ENDIAN);

        //generate properties
        DatasetProperties properties = new SeismicProperties(metadata);
        properties.setKeyOrder("ep", 3);
        properties.setKeyOrder("cdp", 1);
        properties.setKeyOrder("offset", 2);
        properties.setKeyOrder("tracl", 4);
        //2
        DiscreteKeyRange range = new DiscreteKeyRange(3015, 3016, 1, GeoKeyType.REGULAR_TYPE);
        properties.setKeyChosenRange("ep", range);
        //16
        range = new DiscreteKeyRange(1400, 1415, 1, GeoKeyType.REGULAR_TYPE);
        properties.setKeyChosenRange("cdp", range);
        //3
        range = new DiscreteKeyRange(3720, 3920, 100, GeoKeyType.IRREGULAR_TYPE);
        properties.setKeyChosenRange("offset", range);
        //2001 samples per trace
        range = new DiscreteKeyRange(0.0, 8000.0, 4.0, GeoKeyType.NO_TYPE);
        properties.setKeyChosenRange("trac;", range);

        //generate summary
        GeoFileDataSummary summary = new GeoFileDataSummary();
        summary.setNumberOfTraces(2*16*3);
        properties.setSummary(summary);

        String[] traceIDs = BhpSuIOUtils.genNormalizedTraceIDs(properties);
/*
        for (int i=0; i<traceIDs.length; i++) {
            System.out.println(traceIDs[i]);
        }
 */
        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TRACE_IDS_4KEYS));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            int cnt = 0;
            aline = traceIDs[0];
            int numTIDs = traceIDs.length;

            // compare actual and expected results line by line
            while (eline != null && cnt < numTIDs) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                aline = traceIDs[cnt];
                assertEquals(TRACE_IDS_4KEYS+"'s traceIDs are incorrect", eline, aline);

                eline = ebr.readLine();
                cnt++;
            }

            if (cnt != numTIDs)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected trace ID results file: "+TRACE_IDS_4KEYS+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading expected trace IDs: "+TRACE_IDS_4KEYS+"; error message: "+ioe.getMessage());
        }
    }

    /** Test generating trace IDs for 4 keys and their chosen ranges which is the same as the natural ordering. */
    public void testTraceIdGen2() {
        //generate metadata
        SeismicFileMetadata metadata = new SeismicFileMetadata();
        metadata.addKey("ep");
        metadata.addKey("cdp");
        metadata.addKey("offset");
        metadata.addKey("tracl");
        metadata.addKeyRange("ep", 3015, 3016, 1, GeoKeyType.REGULAR_TYPE);
        metadata.addKeyRange("cdp", 1350, 1450, 1, GeoKeyType.REGULAR_TYPE);
        metadata.addKeyRange("offset", 50, 3950, 100, GeoKeyType.IRREGULAR_TYPE);
        metadata.addKeyRange("tracl", 0.0, 8000.0, 4.0, GeoKeyType.NO_TYPE);
        metadata.setGeoFilePath("/scratch/qiProjects/demo/seismic/gathers.dat");
        metadata.setGeoFileName("gathers");
        metadata.setGeoFileSuffix(".dat");
        metadata.setGeoDataOrder(GeoDataOrder.CROSS_SECTION_ORDER);
        metadata.setEndianFormat(ByteOrder.BIG_ENDIAN);

        //generate properties
        DatasetProperties properties = new SeismicProperties(metadata);
        properties.setKeyOrder("ep", 1);
        properties.setKeyOrder("cdp", 2);
        properties.setKeyOrder("offset", 3);
        properties.setKeyOrder("tracl", 4);
        //2
        DiscreteKeyRange range = new DiscreteKeyRange(3015, 3016, 1, GeoKeyType.REGULAR_TYPE);
        properties.setKeyChosenRange("ep", range);
        //16
        range = new DiscreteKeyRange(1400, 1415, 1, GeoKeyType.REGULAR_TYPE);
        properties.setKeyChosenRange("cdp", range);
        //3
        range = new DiscreteKeyRange(3720, 3920, 100, GeoKeyType.IRREGULAR_TYPE);
        properties.setKeyChosenRange("offset", range);
        //2001 samples per trace
        range = new DiscreteKeyRange(0.0, 8000.0, 4.0, GeoKeyType.NO_TYPE);
        properties.setKeyChosenRange("trac;", range);

        //generate summary
        GeoFileDataSummary summary = new GeoFileDataSummary();
        summary.setNumberOfTraces(2*16*3);
        properties.setSummary(summary);

        String[] traceIDs = BhpSuIOUtils.genNormalizedTraceIDs(properties);
/*
        for (int i=0; i<traceIDs.length; i++) {
            System.out.println(traceIDs[i]);
        }
*/
        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TRACE_IDS_4KEYS2));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            int cnt = 0;
            aline = traceIDs[0];
            int numTIDs = traceIDs.length;

            // compare actual and expected results line by line
            while (eline != null && cnt < numTIDs) {
//System.out.println("expected=\""+eline+"\"");
//System.out.println("actual=\""+aline+"\"");
                aline = traceIDs[cnt];
                assertEquals(TRACE_IDS_4KEYS2+"'s traceIDs are incorrect", eline, aline);

                eline = ebr.readLine();
                cnt++;
            }

            if (cnt != numTIDs)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected trace ID results file: "+TRACE_IDS_4KEYS2+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading expected trace IDs: "+TRACE_IDS_4KEYS2+"; error message: "+ioe.getMessage());
        }
    }
}
