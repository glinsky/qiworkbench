/*
 * Test the bhpread parser that process its output for various datasets generates
 * the correct trace summary.
 */

package com.bhpb.bhpsuio;

import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.IOException;
import java.util.ArrayList;

import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;

/**
 * Test parsing BHP-SU's bhpread summary output and creating the geoFile's trace summary.
 * @author Gil Hansen (JUnit test)
 */
public class BhpreadSummaryParserTest extends TestCase {
    /** bhpread summary output for various datasets */
    static public final String BHPREAD_GATHERS = "BhpreadSummary-Gathers.txt";
    static public final String BHPREAD_TARANAKI_AMPEXT = "BhpreadSummary-TaranakiAmpext.txt";
    static public final String BHPREAD_TARANAKI_MODEL_DEPTH = "BhpreadSummary-TaranakiModelDepth.txt";
    static public final String BHPREAD_TARANAKI_AMPEXT_TRANSPOSE = "BhpreadSummary-TaranakiAmpextTranspose.txt";

    /** Results of parsing bhread summary output */
    static public final String GATHERS_SUMMARY = "GathersSummary.txt";
    static public final String TARANAKI_AMPEXT_SUMMARY = "TaranakiAmpextSummary.txt";
    static public final String TARANAKI_MODEL_DEPTH_SUMMARY = "TaranakiModelDepthSummary.txt";
    static public final String TARANAKI_AMPEXT_TRANSPOSE_SUMMARY = "TaranakiAmpextTransposeSummary.txt";

    static final String baseDir = System.getProperty("basedir");
    static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;

    @Override
    public void setUp() {
    }

    @Override
    public void tearDown() {
    }

    BhpSuIO bhpsuIO = new BhpSuIO();
    String actualSummary = "";

    /** Test creating trace summary for demo Gathers.dat */
    public void testParseBhpreadSummaryOutput1() {
        //get the actual bhpread summary output
        ArrayList<String> bhpreadSummaryOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPREAD_GATHERS));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpreadSummaryOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpread summary output file: "+BHPREAD_GATHERS+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPREAD_GATHERS+"; error message: "+ioe.getMessage());
        }

        //parse the bhpread summary output
        GeoFileDataSummary summary = ReadFileSummary.parseBhpreadSummaryOutput(bhpreadSummaryOutput);
        assertNotNull("Failed to create Gathers.dat summary", summary);
        actualSummary = summary.toString();
        //verify summary correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+GATHERS_SUMMARY));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualSummary);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
                assertEquals(GATHERS_SUMMARY+"'s summary incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected summary results file: "+GATHERS_SUMMARY+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpread summary output: "+BHPREAD_GATHERS+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpread summary output: "+BHPREAD_GATHERS+"; error message: "+e.getMessage());
        }
    }

    public void testParseBhpreadSummaryOutput2() {
        //get the actual bhpread summary output
        ArrayList<String> bhpreadSummaryOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPREAD_TARANAKI_AMPEXT));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpreadSummaryOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpread summary output file: "+BHPREAD_TARANAKI_AMPEXT+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPREAD_TARANAKI_AMPEXT+"; error message: "+ioe.getMessage());
        }

        //parse the bhpread summary output
        GeoFileDataSummary summary = ReadFileSummary.parseBhpreadSummaryOutput(bhpreadSummaryOutput);
        assertNotNull("Failed to create Gathers.dat summary", summary);
        actualSummary = summary.toString();
        //verify summary correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TARANAKI_AMPEXT_SUMMARY));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualSummary);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
                assertEquals(TARANAKI_AMPEXT_SUMMARY+"'s summary incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected summary results file: "+TARANAKI_AMPEXT_SUMMARY+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpread summary output: "+BHPREAD_TARANAKI_AMPEXT+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpread summary output: "+BHPREAD_TARANAKI_AMPEXT+"; error message: "+e.getMessage());
        }
    }

    public void testParseBhpreadSummaryOutput3() {
        //get the actual bhpread summary output
        ArrayList<String> bhpreadSummaryOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPREAD_TARANAKI_AMPEXT_TRANSPOSE));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpreadSummaryOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpread summary output file: "+BHPREAD_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPREAD_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+ioe.getMessage());
        }

        //parse the bhpread summary output
        GeoFileDataSummary summary = ReadFileSummary.parseBhpreadSummaryOutput(bhpreadSummaryOutput);
        assertNotNull("Failed to create Gathers.dat summary", summary);
        actualSummary = summary.toString();
        //verify summary correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TARANAKI_AMPEXT_TRANSPOSE_SUMMARY));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualSummary);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
                assertEquals(TARANAKI_AMPEXT_TRANSPOSE_SUMMARY+"'s summary incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected summary results file: "+TARANAKI_AMPEXT_TRANSPOSE_SUMMARY+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpread summary output: "+BHPREAD_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpread summary output: "+BHPREAD_TARANAKI_AMPEXT_TRANSPOSE+"; error message: "+e.getMessage());
        }
    }

    public void testParseBhpreadSummaryOutput4() {
        //get the actual bhpread summary output
        ArrayList<String> bhpreadSummaryOutput = new ArrayList<String>();
        String line = "";
        try {
            InputStream in = new FileInputStream(new File(baseTestFilePath+BHPREAD_TARANAKI_MODEL_DEPTH));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            line = ebr.readLine();
            while (line != null) {
                bhpreadSummaryOutput.add(line);
                line = ebr.readLine();
            }
        } catch (FileNotFoundException fnfe) {
            fail("Could not find bhpread summary output file: "+BHPREAD_TARANAKI_MODEL_DEPTH+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error reading: "+BHPREAD_TARANAKI_MODEL_DEPTH+"; error message: "+ioe.getMessage());
        }

        //parse the bhpread summary output
        GeoFileDataSummary summary = ReadFileSummary.parseBhpreadSummaryOutput(bhpreadSummaryOutput);
        assertNotNull("Failed to create Gathers.dat summary", summary);
        actualSummary = summary.toString();
//System.out.println("Actual summary: "+actualSummary);
        //verify summary correct

        /** Expected line */
        String eline = "";
        /** Actual line */
        String aline = "";
        try {
            //Get expected results
            InputStream in = new FileInputStream(new File(baseTestFilePath+TARANAKI_MODEL_DEPTH_SUMMARY));
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader ebr = new BufferedReader(isr);
            eline = ebr.readLine();

            //Get actual results
            StringReader sr = new StringReader(actualSummary);
            BufferedReader abr = new BufferedReader(sr);
            aline = abr.readLine();

            // compare actual and expected results line by line
            while (aline != null && eline != null) {
                assertEquals(TARANAKI_MODEL_DEPTH_SUMMARY+"'s summary incorrect", eline, aline);

                aline = abr.readLine();
                eline = ebr.readLine();
            }

            if (aline != null)
                fail("More actual results than expected: actual="+aline);
            if (eline != null)
                fail("More expected results than actual: expected="+eline);
        } catch (FileNotFoundException fnfe) {
            fail("Could not find expected summary results file: "+TARANAKI_MODEL_DEPTH_SUMMARY+"; error message: "+fnfe.getMessage());
        } catch (IOException ioe) {
            fail("IO error parsing bhpread summary output: "+BHPREAD_TARANAKI_MODEL_DEPTH+"; error message: "+ioe.getMessage());
        } catch (Exception e) {
            fail("Parsing error parsing bhpread summary output: "+BHPREAD_TARANAKI_MODEL_DEPTH+"; error message: "+e.getMessage());
        }
    }
}