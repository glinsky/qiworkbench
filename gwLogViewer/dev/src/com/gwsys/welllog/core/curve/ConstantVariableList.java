/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 * Defines the constants used in this project.
 * @author Team
 *
 */
public class ConstantVariableList {

	public static final String DEPTH_PANEL = "Depth";
	public static final String DESCRIPTION_PANEL = "Remarks";
	public static final String GROUP_PANEL = "Group";
	public static final String SEISMIC_PANEL = "Seismic";
	public static final String LITHOLOGY_PANEL = "Lithology";
	public static final String CURSOR_MARKER = "cMarker";
	public static final String FILL_LEFT = "Fill_Left";
	public static final String FILL_RIGHT = "Fill_Right";
	public static final String FILL_ALL = "Fill_All";
	public static final String FILL_NONE = "Fill_None";
	public static final String MEASUREMENT_MD = "MD";
	public static final String MEASUREMENT_TVD = "TVD";
	public static final String MEASUREMENT_TVDSS = "TVDSS";
	public static final String P10 = "P10";
	public static final String P50 = "P50";
	public static final String P90 = "P90";
	public static final String MEAN = "MEAN";
	//default values
	public static final Color CURVE_DEFAULT_COLOR = Color.BLACK;
	public static final float CURVE_STROKE = 1;
	public static final int BESIDETYPE_WIDTH = 100;

	public static final int DEPTHSCALE_50 = 50;
	public static final int DEPTHSCALE_200 = 200;
	public static final int DEPTHSCALE_500 = 500;
	public static final int DEPTHSCALE_1000 = 1000;
	public static final int DEPTHSCALE_2000 = 2000;
	public static final int DEPTHSCALE_5000 = 5000;
	public static final int DEPTHSCALE_10000 = 10000;

	public static final double METER_TO_INCH = 39.37;
	public static final double FOOT_TO_INCH = 12;
	public static final double METER_TO_FOOT = 3.28;
	public static final double FOOT_TO_METER = 0.3048;

	//grid
	public static final Color GRID_GRIDCOLOR = Color.LIGHT_GRAY;
	public static final Color GRID_BACKGROUND = Color.WHITE;
	public static final int GRID_GRIDS = 5; //how many grid between two labels
	public static final int GRID_YSPACE = 30; // just default , it will change smartly when the program is running
	public static final int GRID_XSPACE = 30;
	//mainframe
	public static final Dimension MAINFRAME_SIZE = new Dimension(900,650);
	//internalframeview
	public static final Dimension INTERNALFRAMEVIEW_SIZE = new Dimension(650,550);
	public static final int X_OFFSET = 10,Y_OFFSET = 10;
	//trackContainerView
	public static final Color TRACKCONTAINERVIEW_BACKGROUND = Color.WHITE;
	public static final int TRACK_TYPE_NONE = 0;
	public static final int TRACK_TYPE_SU = 1;
	public static final int TRACK_TYPE_NO_SU = 2;
	public static final int TRACK_TYPE_MIXED = 3;
	//axiscurvecontainer
	public static final int AXISCURVECONTAINER_DEFAULT_WIDTH = 300;
	public static final int AXISCURVECONTAINER_DEFAULT_MINWIDTH = 100;
	public static final int AXISCURVECONTAINER_DEFAULT_HEIGHT = 500;
	public static final int AXISCURVECONTAINER_DEFAULT_MINHEIGHT = 300;
	public static final Border AXISCURVECONTAINERVIEW_BORDER = new LineBorder(Color.BLACK, 1);
	public static final int AXISCURVECONTAINER_GAP = 100;
	//curveview
	public static final int CURVEVIEW_DEFAULT_WIDTH = 200;
	public static final int CURVEVIEW_DEFAULT_HEIGHT = 500;
	public static final Color CURVEVIEW_BACKGROUND = Color.WHITE;
	public static final Border CURVEVIEW_BORDER = new LineBorder(Color.BLACK, 1);
	//xaxisview
	public static final Color XVIEWPANEL_COLOR = Color.WHITE;
	public static final Dimension XVIEWSCROLLPANEL_MINIMUMSIZE = new Dimension(1,60);
	public static final Border XVIEWSCROLLPANEL_BORDER = new LineBorder(Color.BLACK, 1);
	public static final Color XAXISVIEW_BACKGROUND = Color.WHITE;
	public static final Color XAXISVIEW_BORDERCOLOR = Color.BLUE;
//	public static final Dimension XAXISVIEW_MINIMUMSIZE = new Dimension(160,20);
	public static final Dimension XAXISVIEW_TOPTYPE_PREFERREDSIZE = new Dimension(1,20);
	public static final Dimension XAXISVIEW_BESIDETYPE_PREFERREDSIZE = new Dimension(BESIDETYPE_WIDTH,20);

	//yaxisview
	public static final int LABEL_SPACE = 100;
	public static final int YDepthCutNumber = 100;
	// if the yaxis min is 1025,max is 2238, cutnumber = 100 , then , we set the min yAxis is 1000,max is 2300
	public static final int YSPACE = 100;
	public static final float VELOCITY_VALUE = 10;
	public static final Color YAXISVIEW_BACKGROUND = Color.WHITE;
	public static final Color DESCRIPTION_BACKGROUND = Color.WHITE;
	public static final Color SEISMIC_BACKGROUND = Color.WHITE;
	public static final Border YAXISVIEW_BORDER = new LineBorder(Color.BLACK, 1);
	public static final int YAXISVIEW_WIDTH = 50;
	public static final int YAXISVIEW_HEIGHT = 60; //Aug7
	public static final Dimension YAXISVIEW_PREFERREDSIZE = new Dimension(YAXISVIEW_WIDTH,CURVEVIEW_DEFAULT_HEIGHT);
	public static final Dimension YAXISHEAD_PREFERREDSIZE = new Dimension(YAXISVIEW_WIDTH,YAXISVIEW_HEIGHT);
	public static final int DESCRIPTION_WIDTH = 100;
	public static final Dimension DESCRIPTION_PREFERREDSIZE = new Dimension(DESCRIPTION_WIDTH,CURVEVIEW_DEFAULT_HEIGHT);
	public static final int SEISMIC_WIDTH = 100;
	public static final Dimension SEISMIC_PREFERREDSIZE = new Dimension(SEISMIC_WIDTH,CURVEVIEW_DEFAULT_HEIGHT);

	public static final int GROUP_SINGLE_HEIGHT = 25; //min size to keep name and unit
	//helpview
	public static final Dimension HELP_SIZE = new Dimension(800,600);

}
