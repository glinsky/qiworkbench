/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;
/**
 * Defines curve types and fill types as static final varibles.
 * @author GaoFei
 * @since July, 2006 Use "MERGE CURVES" instead of REALIZATION_TOP
 */
public interface TrackTypeList {

	public static final String REALIZATION = "Realization";
	public static final String REALIZATION_TOP = "MERGE CURVES";//"Realization_Top";
	public static final String REALIZATION_BESIDE = "BESIDE CURVES";//"Realization_Beside";
	public static final String MEAN_DEVIATION = "Mean_Deviation";
	public static final String P90_P50_P10 = "P90_P50_P10";
	public static final String COLORFILL_TOP = "ColorFill_Top";
	public static final String COLORFILL_BESIDE = "ColorFill_Beside";

	public static final String Y_DEPTH_MD = "depth_MD";
	public static final String Y_DEPTH_TVD = "depth_TVD";
	public static final String Y_DEPTH_TVDSS = "depth_TVDSS";
	public static final String Y_TIME = "time";

	public static final int TIME_TO_MD = 1;
	public static final int MD_TO_TVD = 2;
	public static final int MD_TO_TVDSS = 3;
	public static final int TVD_TO_MD = 4;
	public static final int TVDSS_TO_MD = 5;
	public static final int MD_TO_TIME = 6;

}
