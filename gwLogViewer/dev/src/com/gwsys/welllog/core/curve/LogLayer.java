/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;

/**
 * This is super class of CurveLayer and MarkerLayer.
 * @author 
 *
 */
public class LogLayer {
	
	private String layerType = TrackTypeList.REALIZATION_BESIDE;
	
	public String getLayerType() {
		return layerType;
	}

	/**
	 * The type should be TrackTypeList.REALIZATION_BESIDE or
	 * TrackTypeList.REALIZATION_TOP.
	 * @param layerType Constant from TrackTypeList
	 */
	public void setLayerType(String layerType) {
		if (!layerType.equalsIgnoreCase(TrackTypeList.REALIZATION_BESIDE)
				&&!layerType.equalsIgnoreCase(TrackTypeList.REALIZATION_BESIDE))
			return;
		this.layerType = layerType;
	}	
}
