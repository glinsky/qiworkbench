/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.BufferedImage;

import com.gwsys.welllog.icons.IconResource;

public class ImageLoader
{

    protected static final Component component;
    protected static final MediaTracker tracker;
    protected static int imageIndex = 0;

    private ImageLoader()
    {
    }

    /*
    public static Image loadImage(String protocol, String machine, String file)
        throws IOException
    {
        URL iURL = new URL(protocol, machine, file);
        return Toolkit.getDefaultToolkit().getImage(iURL);
    }
    */

    public static Image loadImage(String file)
    {
        return IconResource.getInstance().getImage(file);
    }

    public static BufferedImage loadBufferedImage(Image image)
    {
        synchronized(tracker)
        {
            tracker.addImage(image, imageIndex);
            try
            {
                tracker.waitForID(imageIndex);
            }
            catch(InterruptedException e)
            {
                throw new RuntimeException("Loading Image is interrupted");
            }
        }
        tracker.removeImage(image, imageIndex);
        imageIndex++;
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        BufferedImage bimage = new BufferedImage(width, height, 2);
        Graphics ig = bimage.createGraphics();
        ig.drawImage(image, 0, 0, null);
        return bimage;
    }

    /*
    public static BufferedImage loadBufferedImage(String protocol, String machine, String file)
        throws IOException
    {
        Image image = loadImage(protocol, machine, file);
        return loadBufferedImage(image);
    }
    */
    
    public static BufferedImage loadBufferedImage(String file)
    {
        Image image = loadImage(file);
        return loadBufferedImage(image);
    }
    
    
    static 
    {
        component = new Component() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

        };
        tracker = new MediaTracker(component);
    }
}
