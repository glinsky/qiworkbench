/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;


/**
 * The drag operation happened from which component
 * currently, there are 3 possible sources: project tree, view tree, curve view
 * @author Administrator
 *
 */
public class ActiveDndSourceMonitor {
	public static final int NONE = 0;
	public static final int PROJECT_TREE = 1;
	public static final int VIEW_TREE = 2;
	public static final int CURVE_VIEW = 3;
	/**
	 * which component is the current active drag and drop operation's source
	 * when one component's dnd operation happened, this var will be set to responsible value
	 */
	static int activeDndSource = NONE;
	/**
	 * if the activeDndSource equals CURVE_VIEW, then activeTrack represent which track is active
	 */
	static Object activeTrack = null;

	public static int getActiveDndSource() {
		return activeDndSource;
	}

	public static void setActiveDndSource(int activeTree) {
		ActiveDndSourceMonitor.activeDndSource = activeTree;
	}

	public static Object getActiveTrack() {
		return activeTrack;
	}

	public static void setActiveTrack(Object activeTrack) {
		ActiveDndSourceMonitor.activeTrack = activeTrack;
	}

}
