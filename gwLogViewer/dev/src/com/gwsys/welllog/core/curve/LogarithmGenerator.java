/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;

import java.util.NoSuchElementException;

public class LogarithmGenerator {

	private double _modelStart;
    private double _modelStop;
    private double _modelStep;
    private double _modelOrigin;
    private double _modelZero;
    private double _base;
    private double _scale;
    private boolean _intermediate;
    private double _min;
    private double _max;
    private double _pos;
    private int _curIdx;
    private int _nextIdx;
    private int _minIdx;
    private int _maxIdx;
    private int _firstIdx;
    private int _lastIdx;

    public LogarithmGenerator()
    {
        this(1.0D);
    }

    public LogarithmGenerator(double d)
    {
        this(d, 0.0D);
    }

    public LogarithmGenerator(double d, double d1)
    {
        this(d, d1, 10D);
    }

    public LogarithmGenerator(double d, double d1, double d2)
    {
        this(d, d1, d2, true);
    }

    public LogarithmGenerator(double d, double d1, double d2, boolean flag)
    {
        this(d, d1, d2, flag, 1.0D);
    }

    private LogarithmGenerator(double d, double d1, double d2, boolean flag, 
            double d3)
    {
        if(d2 <= 1.0D)
        {
            throw new IllegalArgumentException("The log base must be > 1");
        } else
        {
            _modelStep = d;
            _modelOrigin = d1;
            _intermediate = flag;
            _base = d2;
            _scale = d3;
            computeZero();
            return;
        }
    }

    public void reset(double d, double d1, double d2, double d3, double d4)
    {
        _modelStart = d;
        _modelStop = d1;
        _min = d2;
        _max = d3;
        if(_intermediate && _base == (double)Math.round(_base))
        {
            int i = (int)Math.floor((_modelStart - _modelZero) / _modelStep);
            int j = (int)Math.ceil((_modelStop - _modelZero) / _modelStep);
            for(int k = 1; (double)k <= _base; k++)
            {
                if(_modelZero + _modelStep * ((double)i + log(k, _base)) < _modelStart)
                    continue;
                _firstIdx = (int)(((double)i * (_base - 1.0D) + (double)k) - 1.0D);
                break;
            }

            for(int l = (int)_base; l >= 0; l--)
            {
                if(_modelZero + _modelStep * ((double)(j - 1) + log(l, _base)) > _modelStop)
                    continue;
                _lastIdx = (int)(((double)(j - 1) * (_base - 1.0D) + (double)l) - 1.0D);
                break;
            }

            _minIdx = _firstIdx;
            _maxIdx = _lastIdx;
        } else
        {
            _firstIdx = (int)Math.ceil((_modelStart - _modelZero) / _modelStep);
            _lastIdx = (int)Math.floor((_modelStop - _modelZero) / _modelStep);
            _minIdx = (int)Math.floor((_min - _modelZero) / _modelStep);
            _maxIdx = (int)Math.ceil((_max - _modelZero) / _modelStep);
            if(_minIdx < _firstIdx)
                _minIdx = _firstIdx;
            if(_maxIdx > _lastIdx)
                _maxIdx = _lastIdx;
        }
        _nextIdx = _minIdx;
    }

    public boolean hasNext()
    {
        return _nextIdx <= _maxIdx;
    }

    public Object next()
    {
        if(hasNext())
        {
            if(_intermediate && _base == (double)Math.round(_base))
            {
                int i;
                int j;
                if(_nextIdx >= 0)
                {
                    j = _nextIdx / (int)(_base - 1.0D);
                    i = _nextIdx % (int)(_base - 1.0D);
                } else
                {
                    j = -(-_nextIdx - 1) / (int)(_base - 1.0D) - 1;
                    i = _nextIdx - j * (int)(_base - 1.0D);
                }
                _pos = _modelZero + _modelStep * ((double)j + log(i + 1, _base));
                _curIdx = _nextIdx++;
                return new Double((double)(i + 1) * Math.pow(_base, j));
            } else
            {
                _pos = _modelZero + (double)_nextIdx * _modelStep;
                _curIdx = _nextIdx++;
                return new Double(Math.pow(_base, _curIdx));
            }
        } else
        {
            throw new NoSuchElementException();
        }
    }

    public double getTickPosition()
    {
        return _pos;
    }

    public int getTickType()
    {
        if(_curIdx == _firstIdx)
            return 1;
        return _curIdx != _lastIdx ? 2 : 3;
    }

    public int getTickGrade()
    {
        if(_intermediate && _base == (double)Math.round(_base))
            return _curIdx % (int)(_base - 1.0D) != 0 ? -1 : 1;
        else
            return 1;
    }

    private double log(double d, double d1)
    {
        return Math.log(d) / Math.log(d1);
    }

    private void computeZero()
    {
        _modelZero = _modelOrigin - _modelStep * log(_scale, _base);
    }

	public int get_firstIdx() {
		return _firstIdx;
	}

	public int get_lastIdx() {
		return _lastIdx;
	}
    
}
