/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;

import com.gwsys.welllog.icons.IconResource;

/**
 * Defines the pattern used for well log interpretation.
 * @author Team
 *
 */
public class WellLogPattern extends TexturePaint{

    private Color _fg;
    private Color _bg;
    private String _name;

    private WellLogPattern(BufferedImage image, Rectangle anchor, String name)
    {
        this(image, anchor, name, null, null);
    }

    private WellLogPattern(BufferedImage image, Rectangle anchor, String name, Color bg, Color fg)
    {
        super(image, anchor);
        _name = name;
        _bg = bg;
        _fg = fg;
    }

    public Color getForeground()
    {
        return _fg;
    }

    public Color getBackground()
    {
        return _bg;
    }

    public String getName()
    {
        return _name;
    }

    public void setName(String name)
    {
        _name = name;
    }

    public static WellLogPattern loadPattern(String image_file)
    {
        return loadPattern(image_file, image_file);
    }

    public static WellLogPattern loadPattern(String image_file, String name)
    {
        BufferedImage bimage = ImageLoader.loadBufferedImage(image_file);
        WellLogPattern tp = new WellLogPattern(bimage, new Rectangle(0, 0, bimage.getWidth(), bimage.getHeight()), name);
        return tp;
    }

    public static WellLogPattern loadPattern(String pattern_name, Color bg, Color fg)
    {
    	String imageFile = pattern_name.toLowerCase() + ".gif";
    	Image image = IconResource.getInstance().getImage(imageFile);
    	int width = image.getWidth(null);
        int height = image.getHeight(null);
        BufferedImage bimage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics ig = bimage.createGraphics();
        ig.drawImage(image, 0, 0, null);
        ig = null;
/*        int height = bimage.getHeight();
        int width = bimage.getWidth();
        for(int i = 0; i < height; i++)
        {
            for(int j = 0; j < width; j++)
            {
                if(bimage.getRGB(i, j) == Color.white.getRGB())
                {
                    if(bg != null)
                    {
                        bimage.setRGB(i, j, bg.getRGB());
                    }
                } else
                if(fg != null)
                {
                    bimage.setRGB(i, j, fg.getRGB());
                }
            }

        }
*/
        WellLogPattern tp = new WellLogPattern(bimage, new Rectangle(0, 0, bimage.getWidth(),
        		bimage.getHeight()), pattern_name, bg, fg);
        return tp;
    }

    public static WellLogPattern loadPattern(Image image, String name, Color bg, Color fg)
    {
        try
        {
            BufferedImage bimage = ImageLoader.loadBufferedImage(image);
            int height = bimage.getHeight();
            int width = bimage.getWidth();
            if(bg != null && fg != null)
            {
                for(int i = 0; i < height; i++)
                {
                    for(int j = 0; j < width; j++)
                    {
                        if(bimage.getRGB(i, j) == Color.white.getRGB())
                        {
                                bimage.setRGB(i, j, bg.getRGB());
                        } else
                        if(bimage.getRGB(i, j) == Color.black.getRGB() )
                        {
                            bimage.setRGB(i, j, fg.getRGB());
                        }
                    }

                }

            }
            WellLogPattern tp = new WellLogPattern(bimage, new Rectangle(0, 0, bimage.getWidth(), bimage.getHeight()), name, bg, fg);
            return tp;
        }
        catch(Exception e)
        {
            System.out.println(" Exception thrown at loadPattern : " + e);
        }
        return null;
    }

    public static WellLogPattern loadPattern(Image image, Color bg, Color fg)
    {
        return loadPattern(image, "", bg, fg);
    }

    public static WellLogPattern loadPattern(BufferedImage image)
    {
        return new WellLogPattern(image, new Rectangle(0, 0, image.getWidth(), image.getHeight()), "", null, null);
    }

}
