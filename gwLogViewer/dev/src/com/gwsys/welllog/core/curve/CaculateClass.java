/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core.curve;

/**
 * caculate p10.... etc, formulate
 * @author luming
 *
 */
public class CaculateClass {	
	
	public float array[];
	
	public void rankTheArray(float array[])
	{
		float temp;
		float[] arrayClone = (float[]) array.clone();
		for (int i = 0 ;i<arrayClone.length;i++)//begin at the first one, the last one not need to be compared
        {   
			for (int j = 0 ;j<arrayClone.length-i-1;j++)//change if the after one small
            {
				if (this.isBiger(arrayClone[j],arrayClone[j+1])) 
				{
					temp = arrayClone[j]; 
					arrayClone[j] = arrayClone[j + 1]; 
					arrayClone[j + 1] = temp; 
				}
			} 
        }
		this.array = arrayClone;
	}
	
	public void rankTheArray(Object array[]){
		float tmp[] = new float[array.length];
		int i;
		for(i=0;i<tmp.length;i++)
			tmp[i]=((Double)array[i]).floatValue();
		rankTheArray(tmp);
	}
	
	public float[] getArray()
	{
		return this.array;
	}
	
	public float getMin()
	{
		return this.array[0];
	}
	public float getMax()
	{
		return this.array[this.array.length-1];
	}
	public float getMiddle()
	{
		return this.array[this.array.length/2];
	}
	public float getMean()
	{
		float sum = 0;
		for(int i = 0 ; i <this.array.length;i++)
		{
			sum = sum + this.array[i];
		}
		return sum/this.array.length;
	}
	private boolean isBiger(float a,float b)
	{
		if(a>b)
			return true;
		return false;
	}
}
