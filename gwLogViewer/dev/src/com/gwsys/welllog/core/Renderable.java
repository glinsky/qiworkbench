/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.core;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;


/**
 * Defines the interface can be rendered in to java graphics.
 * @author Louis
 *
 */
public interface Renderable {
	/**
	 * Draws the renderable content into given graphics in given region.
	 * @param g2d given graphics.
	 * @param rect given region.
	 */
	public void draw(Graphics2D g2d, Rectangle rect);
	/**
	 * Prints the renderable content into given graphics in given region with ratio.
	 * @param g2d given graphics.
	 * @param rect given region.
	 * @param rateWidth Ratio of width.
	 * @param rateHeight Ratio of height.
	 */
	public void print(Graphics2D g2d, Rectangle rect, float rateWidth, float rateHeight);
	/**
	 * Update border points of a component, given its points and new points
	 * @param point current point of a component
	 * @param top new border top point
	 * @param bottom new border bottom point
	 */
	public void updateBorderByPoints(Point point, int top, int bottom);
}
