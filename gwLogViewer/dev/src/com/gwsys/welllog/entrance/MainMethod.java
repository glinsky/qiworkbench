/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.entrance;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.gwsys.welllog.actions.AboutAction;
import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.mainframe.DefaultViewer;
import com.gwsys.welllog.view.ViewCoreOperator;

/**
 * Main method launches this application.
 * @author Team.
 *
 */
public class MainMethod {

	/**
	 * @param args
	 */
	final static String LOOKANDFEEL = "System";
	
	public MainMethod(){
		JFrame.setDefaultLookAndFeelDecorated(true);
		createFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	
	}
	
	private DefaultViewer createFrame(){
		DefaultViewer mainframe = new DefaultViewer(
				"G&W-Geoscience Suite (Well Log Analysis v"+
				AboutAction.version+")");
		mainframe.setIconImage(IconResource.getInstance()
				.getImageIcon("LogViewerLogo.gif").getImage());
		ViewCoreOperator.createFrame(mainframe);
		mainframe.setVisible(true);
		ViewCoreOperator.selectFrame(mainframe.getMainBuffer()
				.getInternalFrameView().getTitle());
		mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		return mainframe;
	}
	
	public static void main(String[] args) {
		initLookAndFeel();
		new MainMethod();
	}

	private static void initLookAndFeel() {
		String lookAndFeel = null;

		if (LOOKANDFEEL != null) {
			if (LOOKANDFEEL.equals("Metal")) {
				lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
			} else if (LOOKANDFEEL.equals("System")) {
				lookAndFeel = UIManager.getSystemLookAndFeelClassName();
			} else if (LOOKANDFEEL.equals("Motif")) {
				lookAndFeel = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
			} else if (LOOKANDFEEL.equals("GTK+")) { // new in 1.4.2
				lookAndFeel = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
			} else {
				System.err.println("Unexpected value of LOOKANDFEEL specified: "
								+ LOOKANDFEEL);
				lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
			}

			try {
				UIManager.setLookAndFeel(lookAndFeel);
			} catch (ClassNotFoundException e) {
				System.err.println("Couldn't find class for specified look and feel:"
								+ lookAndFeel);
				System.err.println("Did you include the L&F library in the class path?");
				System.err.println("Using the default look and feel.");
			} catch (UnsupportedLookAndFeelException e) {
				System.err.println("Can't use the specified look and feel ("
						+ lookAndFeel + ") on this platform.");
				System.err.println("Using the default look and feel.");
			} catch (Exception e) {
				System.err.println("Couldn't get specified look and feel ("
						+ lookAndFeel + "), for some reason.");
				System.err.println("Using the default look and feel.");
				e.printStackTrace();
			}
		}
	}
}
