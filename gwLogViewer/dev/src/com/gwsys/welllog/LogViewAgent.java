/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.welllog;

import java.awt.Component;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JFileChooser;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

/**
 * Amplitude Extraction plugin, a core componet of qiWorkbench.It is
 * started as a thread by the Messenger Dispatcher that executes the
 * "Activate plugin" command.
 */
public class LogViewAgent extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(LogViewAgent.class
            .getName());

    // messaging mgr for this class only
    private MessagingManager messagingMgr;

    private QILogViewer gui;

    private static Thread pluginThread;

    /** CID for component instance. Generated before the thread is started and carried as the thread's name. */
    private static String myCID = "";

    private boolean pleaseStop = false;

    /* processMessages is set to true except when GUI needs to do message processing for IO or Job Services. GUI resets it tor true when done processing. */
    protected boolean processMessages = true;

    private static int saveAsCount = 0;

    private String project = "";

    private static final String PlugInName = "gwLogViewer";

    private ArrayList paramListForFileChooser;

    private static boolean DEBUGGING = true;

    /**
     * @since Jan 30, 2007
     * @return
     */
    protected static String getCID() {
        return myCID;
    }

    public ComponentDescriptor getComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    /** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
    private ComponentDescriptor projMgrDesc = null;

    /** Metadata of project associated with */
    private QiProjectDescriptor qiProjectDesc = new QiProjectDescriptor();

    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjectDesc;
    }

    /**
     * Get the component descriptor of the associated PM and therefore project.
     * @return Component descriptor of the associated PM
     */
    public ComponentDescriptor getQiProjMgrDescriptor() {
        return projMgrDesc;
    }

    /**
     * Initialize the plugin component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            
            messagingMgr = new MessagingManager();
            
            //GJH: 11/9/05
            myCID = Thread.currentThread().getName();
            
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.PLUGIN_AGENT_COMP, PlugInName,
                    myCID);
            
            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in LogViewAgent.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /**
     * Generate state information into xml string format.
     * MessageDispatcher uses componentType to find matching Manifest object.
     * @return  String.
     */
    private String genState() {
        StringBuffer content = new StringBuffer();
        ComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        content.append("<component componentKind=\""
                + CompDescUtils.getDescComponentKind(desc)
                + "\" componentType=\""
                + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc)
                + "\" preferredDisplayName=\""
                + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
        content.append(gui.saveState());
        content.append("</component>\n");
        return content.toString();
    }

    /**
     * Generate state information into xml string format in repsonse to save as command
     * @return  String
     */
    private String genStateAsClone() {
        StringBuffer content = new StringBuffer();
        ComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        String displayName = "";
        saveAsCount++;
        if (saveAsCount == 1)
            displayName = "CopyOf"
                    + CompDescUtils.getDescPreferredDisplayName(desc);
        else
            displayName = "Copy(" + saveAsCount + ")Of"
                    + CompDescUtils.getDescPreferredDisplayName(desc);
        content.append("<component componentKind=\""
                + CompDescUtils.getDescComponentKind(desc)
                + "\" componentType=\""
                + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc)
                + "\" preferredDisplayName=\"" + displayName + "\">\n");
        content.append(gui.saveState());
        content.append("</component>\n");

        return content.toString();
    }

    private void restoreState(Node node) {
        String preferredDisplayName = ((Element) node)
                .getAttribute("preferredDisplayName");
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(getClass().getName())) {
                    String title = ((Element) child).getAttribute("title");
                    if (preferredDisplayName != null
                            && preferredDisplayName.trim().length() > 0)
                        preferredDisplayName = title;
                    project = ((Element)child).getAttribute("project");
                    gui = new QILogViewer(preferredDisplayName, this);
                    gui.restoreState(project);

                    CompDescUtils.setDescPreferredDisplayName(messagingMgr
                            .getMyComponentDesc(), preferredDisplayName);

                }
            }
        }
        if (gui == null)
            gui = new QILogViewer("gwLogViewer", this);
    }

    /**
     * Initialize the plugin, then start processing messages its receives.
     */
    public void run() {
        init();
        //process any messages received from other components
        while (pleaseStop == false) {

            //Check if associated with a PM. If not (because was restored), discover the PM
            //with a matching PID. There will be one once its GUI comes up.
            String myPid = QiProjectDescUtils.getPid(qiProjectDesc);
            while (projMgrDesc == null && !myPid.equals("")) {
                //NOTE: Request will stay forwever on outstanding request list if no PM with matching PID.
                //      This can occur when restoring the workbench and the PM hasn't been restored
                //      or it is being restored but its GUI is not yet up.
                //Message Dispatcher will broadcast command to all PMs. Only PM with matching PID
                //will send back a response.
                messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
                try {
                    //wait and then try again
                    Thread.currentThread().sleep(1000);
                    //if a message arrived, process it. It probably is the response from a PM.
                    if (!messagingMgr.isMsgQueueEmpty()) break;
                } catch (InterruptedException ie) {
                    continue;
                }
            }

            QiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if (DEBUGGING)
                System.out.println("Next message *** " + msg.toString());
            if (msg != null
                    && !(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg
                            .skip())) {
                msg = messagingMgr.getNextMsgWait();
                processMsg(msg);
            } else {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ie) {
                    break;
                }
            }
        }
    }

    /**
     * Find the request matching the response and process the response based on the request.
     */
    @SuppressWarnings("unchecked")
    public void processMsg(QiWorkbenchMsg msg) {
        //log message traffic
        logger.fine("msg=" + msg.toString());

        // Check if a response. If so, process and consume response
        if (messagingMgr.isResponseMsg(msg)) {
            System.out.println("LogViewAgent : Response Command");
            QiWorkbenchMsg request = messagingMgr.checkForMatchingRequest(msg);
            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                String cmd = MsgUtils.getMsgCommand(request);
                if (cmd.equals(QIWConstants.NULL_CMD))
                    return;
                else if (MsgUtils.getMsgCommand(msg).equals(
                        QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD)) {
                    fileChooser = (ComponentDescriptor) MsgUtils
                            .getMsgContent(msg);
                    messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,fileChooser,QIWConstants.ARRAYLIST_TYPE,paramListForFileChooser);
                    messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,fileChooser);
                    return;
                }
            }// route the result from file chooser service back to its caller
            else if (MsgUtils.getMsgCommand(msg).equals(
                    QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
                ArrayList list = (ArrayList)MsgUtils.getMsgContent(msg);
                if (((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
                    String fileName = (String)list.get(1);
                    String action = (String)list.get(3);
                    System.out.println("LogViewAgent : File Command"+action);
                    System.out.println("LogViewAgent : File Name"+fileName);
                } else {
                    logger.info("File Chooser canceled by the user");
                }
                return;
            }
            else if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_PROJ_INFO_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);

                //reset window title (if GUI up)
                if (gui != null) {
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.resetTitle(projName);
                }
            }
            else if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                projMgrDesc = (ComponentDescriptor)projInfo.get(0);
                qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);
                String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                gui.resetTitle(projName);
            }
            logger.warning("LogViewAgent: Response message not processed: " + msg.toString());
            return;
        }
        //Check if a request. If so, process and send back a response
        else if (messagingMgr.isRequestMsg(msg)) {
            System.out.println("LogViewAgent : Request Command");
            String cmd = MsgUtils.getMsgCommand(msg);
            if (cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
                if (gui != null)
                    gui.setVisible(true);
            }if(cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
				if(gui != null){
                	messagingMgr.sendResponse(msg,Component.class.getName(),(Component)gui);
					gui.closeViewerGUI();
				}
            } else if (cmd.equals(QIWConstants.DEACTIVATE_VIEWER_AGENT_CMD)
                    || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD)
                    || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD)) {

                System.out.println("LogViewAgent is closed");

                messagingMgr.unregisterComponent(messagingMgr
                        .getComponentDesc(myCID));

                if (gui != null && gui.isVisible())
                    gui.closeViewerGUI();
                pleaseStop = true;
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, myCID
                        + " is successfully deactivated.");
            } else if (cmd.equals(QIWConstants.SAVE_COMP_CMD)) {
                System.out.println("LogViewAgent : Save Command");
                String content = genState();
                System.out.println("LogViewAgent performs SAVE_COMP_CMD -- "
                        + content);
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE,
                        content);
                //save component state as clone
            } else if (cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
                System.out.println("LogViewAgent : Save As Command");
                String content = genStateAsClone();
                System.out.println("LogViewAgent performs SAVE_COMP_AS -- "
                        + content);
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE,
                        content);

            } else if (cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
                System.out.println("LogViewAgent : Restore Command");
                Node node = (Node) MsgUtils.getMsgContent(msg);
                restoreState(node);
                messagingMgr.sendResponse(msg, QILogViewer.class.getName(), gui);
            } else if (cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
                ArrayList msgList = (ArrayList)msg.getContent();
                String invokeType = (String)msgList.get(0);
                if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)){
                    Node node = ((Node)((ArrayList)MsgUtils.getMsgContent(msg)).get(2));
                    restoreState(node);
                } else if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)){
//                  qiProjectDesc = ((QiProjectDescriptor)((ArrayList)msg.getContent()).get(2));
                    //gui = new QILogViewer("gwLogViewer", this);
                	/*ArrayList info = (ArrayList)msg.getContent();
                    ComponentDescriptor pmDesc = (ComponentDescriptor)info.get(0);
                    QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
                    //ignore message if not from associated PM
                    if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
                        qiProjectDesc = projDesc;
                        //update window title
                        String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                        gui.resetTitle(projName);
                    }*/
                	/*System.out.println("PRINTING ARRAY OUTPUT");
                	ArrayList info = (ArrayList)msg.getContent();
                	for(int i=0; i<info.size(); i++) {
                		System.out.println(info.get(i));
                	}
                    QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
                    qiProjectDesc = projDesc;
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);*/
                	gui = new QILogViewer(this.getComponentDescriptor().getPreferredDisplayName(), this);
                }
                //Send a normal response back with the plugin's JInternalFrame and
                //let the Workbench Manager add it to the desktop and make
                //it visible.
                messagingMgr.sendResponse(msg, QILogViewer.class.getName(), gui);
            }// user selected rename plugin menuitem
            else if(cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
                String preferredDisplayName = (String) MsgUtils
                        .getMsgContent(msg);
                if (preferredDisplayName != null
                        && preferredDisplayName.trim().length() > 0) {
                    messagingMgr.getMyComponentDesc().setPreferredDisplayName(
                            preferredDisplayName);
                    gui.rename(preferredDisplayName);
                }
                messagingMgr.sendResponse(msg, QIWConstants.COMP_DESC_TYPE,
                        messagingMgr.getMyComponentDesc());
            }
            else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
              //Just a notification. No response required or expected.
              projMgrDesc = (ComponentDescriptor)msg.getContent();
              //Note: Notification couldn't contain info about PM's project because GUI may not be up yet.
              if (projMgrDesc != null)
                  messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
            }
            else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
              ArrayList info = (ArrayList)msg.getContent();
              ComponentDescriptor pmDesc = (ComponentDescriptor)info.get(0);
              QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
              //ignore message if not from associated PM
              if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
                  qiProjectDesc = projDesc;
                  //update window title
                  String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                  gui.resetTitle(projName);
              }
            }
            else
                logger.warning("LogViewAgent: Request not processed, requeued: "
                                + msg.toString());
            return;
        }
    }



    /*
     * Get a file chooser service, invoke a file chooser, and get the user's selection
     * @param list File chooser input parameters
     */
    public void doFileChooser(ArrayList list) {
        paramListForFileChooser = list;
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        QiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, 10000);
        if (response == null) {
            logger.warning("Internal error occurring in getting file chooser service returning null");
            System.out.println("LogViewAgent : Failed to start file chooser service");
            return;
        }

        if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
            System.out.println("LogViewAgent : Failed to start file chooser service");
            return;
        } else {
            ComponentDescriptor cd = (ComponentDescriptor)response.getContent();

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE,list);

            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);

            return;
        }
    }

    private ComponentDescriptor fileChooser = null;

    /**
     * get messaging manager.
     *  @return  MessagingManager
     */
    public MessagingManager getMessagingManager() {
        return messagingMgr;
    }

    /** when the user click the Exit button on the Viewer calling to deactivate self.
     */
    public void deactivateSelf() {
        //ask WorkbenchManager to remove it from workbench GUI then send it back to self to deactivate self
        ComponentDescriptor wbMgr = messagingMgr
                .getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.QUIT_COMPONENT_CMD, wbMgr,
                QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

    /** Launch the gwLogViewer plugin:
     *  <ul>
     *  <li>Start up the plugin thread which will initialize the plugin.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        LogViewAgent aePluginInstance = new LogViewAgent();
        //CID has to be passed in since no way to get it back out
        String cid = args[0];
        // use the CID as the name of the thread
        pluginThread = new Thread(aePluginInstance, cid);
        pluginThread.start();
        long threadId = pluginThread.getId();
        logger.info("LogViewAgent Thread-" + Long.toString(threadId)
                + " started");
        // When the plugin's init() is finished, it will release the lock

        // wait until the plugin's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
        logger.info("LogViewAgent main finished");
    }
}
