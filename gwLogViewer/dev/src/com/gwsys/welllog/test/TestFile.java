package com.gwsys.welllog.test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TestFile implements TestRunner{
	private ArrayList<TestRunner> datasourceList = new ArrayList<TestRunner>();
	
	private String fileName;
	
	public TestFile(){
	}

	public ArrayList<TestRunner> getDatasourceList() {
		return datasourceList;
	}
	
	public void run() {
		((TestDataSource)datasourceList.get(0)).run();			
	}

	public void readFile(){
		// read file
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			System.out.println(pce);
		}
		Document doc = null;
		try {
			doc = db.parse(fileName);
		} catch (DOMException dom) {
			System.out.println(dom);
		} catch (IOException ioe) {
			System.out.println(ioe);
		} catch (SAXException sax) {
			System.out.println(sax);
		}

		// get the root of tree
		Element xmlRoot = doc.getDocumentElement();
		loadXML(xmlRoot);
		
		// invoke
		run();
	}

	public void loadXML(Element xmlRoot) {
		NodeList nodeList = null;
		nodeList = xmlRoot.getElementsByTagName("datasource");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlDataSource = (Element) nodeList.item(i);
			
			TestDataSource tds = new TestDataSource(this);
			this.datasourceList.add(tds);
			
			tds.loadXML(xmlDataSource);			
		}
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}	
}
