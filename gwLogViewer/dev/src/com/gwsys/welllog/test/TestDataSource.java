package com.gwsys.welllog.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.WellInfo;
import com.gwsys.welllog.view.curve.OperationClass;

public class TestDataSource implements TestRunner{
	public static final int LAS = 0;
	public static final int XML = 1;
	public static final int ASCII = 2;
	public static final int BHPSU = 3;
	
	private TestFile testFile;
	
	private int type;
	
	private String path;

	private String wellName;
	
	ArrayList<TestRunner> operationList = new ArrayList<TestRunner>();
	
	public static final int MENU_HEIGHT = 20;
	
	public static final int MENU_WIDTH = 80;
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public ArrayList<TestRunner> getOperationList() {
		return operationList;
	}
	
	public TestDataSource(TestFile testFile){
		this.testFile = testFile;
	}
	
	public void run() {
		readFile();
		findWell();

		((TestOperation)operationList.get(0)).run();
	}
		
	public void readFile(){
		File file = new File(this.path);
		OperationClass.initialTreeAndHashMap(file, DataBuffer.getJMainFrame());			
	}
	
	public void loadXML(Element xmlDataSource) {
		String type = xmlDataSource.getAttribute("type");
		String path = xmlDataSource.getAttribute("path");

		if(type.trim().equalsIgnoreCase("las")){
			setType(TestDataSource.LAS);				
		}else if(type.trim().equalsIgnoreCase("xml")){
			setType(TestDataSource.XML);				
		}else if(type.trim().equalsIgnoreCase("dat")){
			setType(TestDataSource.ASCII);				
		}else if(type.trim().equalsIgnoreCase("su")){
			setType(TestDataSource.BHPSU);				
		}
		setPath(path);
		
		// Hand to child
		NodeList nodeList = null;
		nodeList = xmlDataSource.getElementsByTagName("operation");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlOperation = (Element) nodeList.item(i);
			
			TestOperation tOper = new TestOperation(this);
			this.operationList.add(tOper);
			
			tOper.loadXML(xmlOperation);			
		}
	}
	
	public void findWell(){
		ArrayList wellInfos = DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList();
		String fileName="";
		if(path.indexOf('\\') != -1)
			fileName=path.substring(path.lastIndexOf('\\')+1);
		else if(path.indexOf('/') != -1)
			fileName=path.substring(path.lastIndexOf('/')+1);
		for(int i=0 ; i<wellInfos.size() ; i++){
			String tmpSource = ((WellInfo)wellInfos.get(i)).getSourceName().substring(((WellInfo)wellInfos.get(i)).getSourceName().lastIndexOf('\\')+1);
			if(tmpSource.equals(fileName))
				wellName = ((WellInfo)wellInfos.get(i)).getWellName();
		}
	}
	
	public String getWellName() {
		return wellName;
	}
	
	public void runNext(){
		for(int i=0 ; i<testFile.getDatasourceList().size() ; i++){
			if(((TestDataSource)testFile.getDatasourceList().get(i))==this && i+1<testFile.getDatasourceList().size()){
				((TestDataSource)testFile.getDatasourceList().get(i+1)).run();
				break;
			}
		}
	}
	

	class MyTimerTask extends TimerTask{
		private int index;

		public MyTimerTask(int index){
			this.index = index;
		}
		
		public void run() {
			if(index+1 < operationList.size()){
				Timer timer = new Timer();
		 		MyTimerTask myTimerTask = new MyTimerTask(index+1);
				timer.schedule(myTimerTask,500);				
			}
		}		
	}

	
}