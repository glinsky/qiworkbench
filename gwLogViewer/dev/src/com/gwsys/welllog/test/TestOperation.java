package com.gwsys.welllog.test;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.tree.DefaultMutableTreeNode;

import org.w3c.dom.Element;

import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.welllog.actions.ViewAllAction;
import com.gwsys.welllog.actions.ZoomInAction;
import com.gwsys.welllog.actions.ZoomOutAction;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.dialogs.EditMarkerDialog;
import com.gwsys.welllog.view.fill.MarkerFill;
import com.gwsys.welllog.view.projecttree.ProjectTreeController;
import com.gwsys.welllog.view.projecttree.ProjectTreeCurveNodeObject;
import com.gwsys.welllog.view.projecttree.ProjectTreeMarkerNodeObject;
import com.gwsys.welllog.view.track.AxisCurveContainerView;


public class TestOperation implements TestRunner{
	private TestDataSource datasource;

	private int type;
	private int mask;
	private int location;
	private String locationDetail = "";
	private int time = 2000;

	// type
	public static final int DELAY=0;
	public static final int MOUSE_PRESS=1;
	public static final int MOUSE_DRAG=2;
	public static final int MOUSE_SCROLL=3;

	// mask
	public static final int BUTTON1_MASK=4;
	public static final int BUTTON2_MASK=5;
	public static final int BUTTON3_MASK=6;

	// location
	public static final int MENU = 7;
	public static final int CURVE = 8;
	public static final int TRACK=9;
	public static final int GROUP=10;
	public static final int MARKER=11;

	public static final int MENU_HEIGHT = 20;

	public static final int MENU_WIDTH = 80;

	public TestOperation(TestDataSource datasource){
		this.datasource = datasource;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public String getLocationDetail() {
		return locationDetail;
	}

	public void setLocationDetail(String locationDetail) {
		this.locationDetail = locationDetail;
	}

	public int getMask() {
		return mask;
	}

	public void setMask(int mask) {
		this.mask = mask;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public void loadXML(Element xmlDataSource) {
		// type
		String type = xmlDataSource.getAttribute("type");
		if(type.trim().equalsIgnoreCase("delay")){
			this.type = TestOperation.DELAY;
		}else if(type.trim().equalsIgnoreCase("mousepress")){
			this.type = TestOperation.MOUSE_PRESS;
		}else if(type.trim().equalsIgnoreCase("mousedrag")){
			this.type = TestOperation.MOUSE_DRAG;
		}else if(type.trim().equalsIgnoreCase("mousescroll")){
			this.type = TestOperation.MOUSE_SCROLL;
		}

		// time
		if(xmlDataSource.getAttribute("time")!=null && !xmlDataSource.getAttribute("time").equals("")){
			this.time = Integer.parseInt(xmlDataSource.getAttribute("time"));
		}

		// mask
		if(xmlDataSource.getAttribute("mask")!=null && !xmlDataSource.getAttribute("mask").equals("")){
			String mask = xmlDataSource.getAttribute("mask");
			if(mask.trim().equalsIgnoreCase("button1")){
				this.mask = TestOperation.BUTTON1_MASK;
			}else if(mask.trim().equalsIgnoreCase("button2")){
				this.mask = TestOperation.BUTTON2_MASK;
			}else if(mask.trim().equalsIgnoreCase("button3")){
				this.mask = TestOperation.BUTTON3_MASK;
			}
		}

		// location
		if(xmlDataSource.getAttribute("location")!=null && !xmlDataSource.getAttribute("location").equals("")){
			String location = xmlDataSource.getAttribute("location");
			String locationSummary = location.substring(0,location.indexOf('('));
			String locationDetail = location.substring(location.indexOf('(')+1,location.indexOf(')'));
			if(locationSummary.trim().equalsIgnoreCase("curve")){
				this.location = TestOperation.CURVE;
			}else if(locationSummary.trim().equalsIgnoreCase("track")){
				this.location = TestOperation.TRACK;
			}else if(locationSummary.trim().equalsIgnoreCase("menu")){
				this.location = TestOperation.MENU;
			}else if(locationSummary.trim().equalsIgnoreCase("group")){
				this.location = TestOperation.GROUP;
			}else if(locationSummary.trim().equalsIgnoreCase("marker")){
				this.location = TestOperation.MARKER;
			}
			this.locationDetail = locationDetail;
		}
	}

	public void run() {
		Robot robot =null;
		try{
			robot = new Robot();
		}catch(AWTException e){
			return;
		}

		if(type == TestOperation.DELAY){
			robot.delay(time);

			Timer timer = new Timer();
			MyTimerTask myTimerTask = new MyTimerTask();
			timer.schedule(myTimerTask,500);

			return;
		}

		if(this.location == TestOperation.MENU){
			menuAction(robot);
		}else if(this.location == TestOperation.CURVE){
			curveAction(robot);
		}else if(this.location == TestOperation.TRACK){
			trackAction(robot);
		}else if(this.location == TestOperation.GROUP){
			groupAction(robot);
		}else if(this.location == TestOperation.MARKER){
			markerAction(robot);
		}

		Timer timer = new Timer();
		MyTimerTask myTimerTask = new MyTimerTask();
		timer.schedule(myTimerTask,500);

		// press and release mouse button
/*		if(type == TestOperation.MOUSE_PRESS){
			if(mask == TestOperation.BUTTON1_MASK){
				robot.mousePress(InputEvent.BUTTON1_MASK);
				robot.delay(2000);
				robot.mouseRelease(InputEvent.BUTTON1_MASK);
			}else if(mask == TestOperation.BUTTON2_MASK){
				robot.mousePress(InputEvent.BUTTON2_MASK);
				robot.delay(2000);
				robot.mouseRelease(InputEvent.BUTTON2_MASK);
			}else if(mask == TestOperation.BUTTON3_MASK){
				robot.mousePress(InputEvent.BUTTON3_MASK);
				robot.delay(2000);
				robot.mouseRelease(InputEvent.BUTTON3_MASK);
			}
		}else if(type == TestOperation.MOUSE_DRAG){

		}else if(type == TestOperation.MOUSE_SCROLL){
			robot.mouseWheel(0);
		}*/
	}

	class MyTimerTask extends TimerTask{
		public MyTimerTask(){
		}

		public void run() {
			runNext();
		}
	}

	public void menuAction(Robot robot){
		if(this.locationDetail.equalsIgnoreCase("View All")){
			ViewAllAction viewAllAction = new ViewAllAction(DataBuffer.getJMainFrame());
			viewAllAction.viewAll();
		}else if(this.locationDetail.equalsIgnoreCase("Zoom In")){
			ZoomInAction zoomInAction = new ZoomInAction(DataBuffer.getJMainFrame());
			zoomInAction.actionPerformed(null);
		}else if(this.locationDetail.equalsIgnoreCase("Zoom Out")){
			ZoomOutAction zoomOutAction = new ZoomOutAction(DataBuffer.getJMainFrame());
			zoomOutAction.actionPerformed(null);
		}
	}

	public void curveAction(Robot robot){
		String curveHashmapKey = datasource.getWellName() +
		OperationClass.hashMapNameSeparator + this.locationDetail;

		// select curve
		DefaultMutableTreeNode node = DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().getCurveNameNode(datasource.getWellName(),this.locationDetail);
		ProjectTreeCurveNodeObject curveNode = (ProjectTreeCurveNodeObject)node.getUserObject();
		CurveData cd = (CurveData) DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(curveNode.getCurveHashmapKey());
		if(cd.isDraged()){
			return;
		}

		ProjectTreeController controller = new ProjectTreeController();
		controller.CurveNodeClick(DataBuffer.getJMainFrame().getMainBuffer().getProjectTree(),
				DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().getCurveNameNode(datasource.getWellName(),this.locationDetail)
				,datasource.getWellName(),true);

		ViewCoreOperator.selectCurve(curveHashmapKey, datasource.getWellName(), false);
	}

	public void trackAction(Robot robot){
		ViewCoreOperator.selectTrack(datasource.getWellName());
	}

	public void groupAction(Robot robot){
		if(this.locationDetail.equalsIgnoreCase("Create")){
			ViewCoreOperator.addGroupPanel();
		}else if(this.locationDetail.split(",").length > 0){
			String groupName = this.locationDetail.split(",")[0];
			String curveName = this.locationDetail.split(",")[1];

			DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
			.addCurveToGroup(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTitle() ,
					datasource.getWellName(), datasource.getWellName()+
					OperationClass.hashMapNameSeparator+curveName,
					datasource.getWellName()+OperationClass.hashMapNameSeparator+groupName);
		}
	}

	public void markerAction(Robot robot){
		CurveView curveView = null;
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		ArrayList tracks = ifv.getTrackContainer().getAxisCurveContainerList();
		for (int i = 0; i < tracks.size(); i++) {
			if (((AxisCurveContainerView) tracks.get(i)).getWellName().equals(datasource.getWellName())) {
				curveView = ((AxisCurveContainerView) tracks.get(i)).getCurveView();
			}
		}

		if(this.locationDetail.split(",").length == 2){
			String markerName = this.locationDetail.split(",")[0];

			// Same track contain same name marker
			for(int i=0 ; i<curveView.getMarkerLayer().getMarkerList().size() ; i++){
				if(((CurveMarker)curveView.getMarkerLayer().getMarkerList().get(i)).getMarkerName().equals(markerName)){
					((CurveMarker)curveView.getMarkerLayer().getMarkerList().get(i)).setMarkerName(markerName);
					return;
				}
			}


			int depth = Integer.parseInt(this.locationDetail.split(",")[1]);

			if(DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().containsSameNameMarker(markerName) == true){
                // Other track contain same name marker
				CurveMarker marker = new CurveMarker(markerName,depth,Color.BLUE);
				curveView.getMarkerLayer().addMarkerToList(marker);

				MarkerInfo markerInfo = new MarkerInfo(datasource.getWellName(),markerName,depth);
				DataBuffer.getJMainFrame().getMainBuffer().getMarkerList().add(markerInfo);

				DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().addWellTopNode(markerInfo.getWellName(),markerInfo.getMarkerName(),ProjectTreeMarkerNodeObject.WELL_NAME,false);
				ViewCoreOperator.connectMarker(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView());

				curveView.repaint();
				DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().repaint();

			}else{
				@SuppressWarnings("unused")
				EditMarkerDialog markerDialog = new EditMarkerDialog(curveView,markerName,depth ,robot);
			}
		}else if(this.locationDetail.split(",").length == 3){
			if(this.locationDetail.split(",")[0].equalsIgnoreCase("Fill")){
				DataBuffer.getJMainFrame().setTesting(true);

				String marker1 = this.locationDetail.split(",")[1];
				String marker2 = this.locationDetail.split(",")[2];

				// If fill exits
				for (int i = 0; i < curveView.getMarkerLayer().getMarkerFillList().size(); i++) {
					MarkerFill mf = (MarkerFill) curveView.getMarkerLayer().getMarkerFillList().get(i);
					if(mf.getCm1().getMarkerName().equals(marker1) && mf.getCm2().getMarkerName().equals(marker2)){
						return;
					}
					if(mf.getCm1().getMarkerName().equals(marker2) && mf.getCm2().getMarkerName().equals(marker1)){
						return;
					}

				}

				// first
				Point p = DataBuffer.getJMainFrame().getTmpTgFillBtn().getLocationOnScreen();
				robot.mouseMove(p.x + 5, p.y + 5);
				robot.delay(1000);

				robot.mousePress(InputEvent.BUTTON1_MASK);
				robot.delay(1000);
				robot.mouseRelease(InputEvent.BUTTON1_MASK);

				for(int i=0 ; i<curveView.getMarkerLayer().getMarkerList().size() ; i++){
					if(((CurveMarker)curveView.getMarkerLayer().getMarkerList().get(i)).getMarkerName().equals(marker1)
							|| ((CurveMarker)curveView.getMarkerLayer().getMarkerList().get(i)).getMarkerName().equals(marker2))
					curveView.getMarkerLayer().setSelectMarker(i, false);
				}
				curveView.repaint();

				robot.delay(1000);
				p = DataBuffer.getJMainFrame().getTmpTgFillBtn().getLocationOnScreen();
				robot.mouseMove(p.x + 5, p.y + 5);
				robot.delay(1000);

				robot.mousePress(InputEvent.BUTTON1_MASK);
				robot.delay(1000);
				robot.mouseRelease(InputEvent.BUTTON1_MASK);

				DataBuffer.getJMainFrame().setTesting(false);
			}
		}
	}

	public void runNext(){
		for(int i=0 ; i<datasource.getOperationList().size() ; i++){
			if(((TestOperation)datasource.getOperationList().get(i))==this){
				if(i+1 < datasource.getOperationList().size()){
					((TestOperation)datasource.getOperationList().get(i+1)).run();
				}else if(i+1 == datasource.getOperationList().size()){
					datasource.runNext();
				}
				break;
			}
		}
	}

}