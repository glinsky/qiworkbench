/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.actions;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.gwsys.data.well.WksFileFilter;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.WorkSpace;

/**
 * Implements action to save project.
 * @author Team
 *
 */
public class SaveProjectAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	private Timer timer = null;

	private MyTimerTask myTimerTask = null;

	public SaveProjectAction(MainFrameView jMainFrame){
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		JFileChooser projectSaveFileChooser = new JFileChooser("Saves");
		projectSaveFileChooser.setSelectedFile(new File("*.wks"));
		projectSaveFileChooser.setFileFilter(new WksFileFilter("wks"));
		projectSaveFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		projectSaveFileChooser.setMultiSelectionEnabled(false);

		int returnVal = projectSaveFileChooser.showSaveDialog(jMainFrame.getFrame());

		if (returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}

		File projectSaveFile = projectSaveFileChooser.getSelectedFile();
		if (projectSaveFile.exists()){
			int reply=JOptionPane.showConfirmDialog(projectSaveFileChooser,
					"This project file already exists\nDo you want to replace it?");
			if (reply==JOptionPane.NO_OPTION||reply==JOptionPane.CANCEL_OPTION)
				return;
		}
		projectSaveFileChooser = null;

		timer = new Timer();
		myTimerTask = new MyTimerTask(projectSaveFile);
		timer.schedule(myTimerTask,100);
	}


	private void saveProjectFile(File file){
		myTimerTask = null;
		timer=null;
		Cursor cursor = new Cursor(Cursor.WAIT_CURSOR);
		jMainFrame.setCursor(cursor);
		if(jMainFrame.getMainBuffer().getInternalFrameView()!=null){
			((InternalFrameView)jMainFrame.getMainBuffer().
					getInternalFrameView()).setCursor(cursor);
		}

		String savefile = file.getPath();
		file.delete(); // delete last file
		WorkSpace.saveWorkSpaceToXml(savefile, jMainFrame);

		jMainFrame.setCursor(Cursor.getDefaultCursor());
		if(jMainFrame.getMainBuffer().getInternalFrameView()!=null){
			((InternalFrameView)jMainFrame.getMainBuffer().
					getInternalFrameView()).setCursor(Cursor.getDefaultCursor());
		}
		jMainFrame.setMessage("Project is saved successfully!");
		//System.out.println(WorkSpace.toXMLString());
	}

	class MyTimerTask extends TimerTask{
		private File filename;

		public MyTimerTask(File file){
			filename = file;
		}
		@Override
		public void run() {
			saveProjectFile(filename);
		}
	}
}
