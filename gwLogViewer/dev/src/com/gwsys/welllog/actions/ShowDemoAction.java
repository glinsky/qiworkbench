package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import com.gwsys.welllog.test.TestFile;

public class ShowDemoAction implements ActionListener, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public ShowDemoAction(){
	}

	public void actionPerformed(ActionEvent arg0) {
		TestFile tf = new TestFile();
		tf.setFileName("Demo.xml");
		tf.readFile();
	}
}
