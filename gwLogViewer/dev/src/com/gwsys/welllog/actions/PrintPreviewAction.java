/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.welllog.actions;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.gwsys.print.ImageGenerator;
import com.gwsys.print.PrintFrame;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.track.AxisCurveContainerView;

/**
 * Action handles printing.
 * @author Team
 *
 */
public class PrintPreviewAction implements ActionListener, Serializable {
	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	public PrintPreviewAction(MainFrameView jMainFrame){
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		InternalFrameView selectedInternalFrameView = ((MainFrameView) jMainFrame).getSonJSplitPane()
				.getRightDesktopPane().getSelectedInternalFrameView();

		if (selectedInternalFrameView == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select an InternalFrame!", "Message", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		ArrayList trackList = selectedInternalFrameView.getTrackContainer().getAxisCurveContainerList();
		int windowWidth = 0;
		int windowHeight = 0;
		if(trackList.size() == 0){
			PrinterJob printerJob = PrinterJob.getPrinterJob();
			PageFormat page = printerJob.defaultPage();
			windowWidth = (int)((float)page.getWidth() * ImageGenerator.getPrinterResolution() / 72);
			windowHeight = (int)((float)page.getHeight() * ImageGenerator.getPrinterResolution() / 72);
		}else{
			AxisCurveContainerView lastTrack = (AxisCurveContainerView)trackList.get(trackList.size() -1);
			windowWidth = lastTrack.getX() + lastTrack.getWidth() + 10;
			for(int i = 0; i < trackList.size(); i++){
				AxisCurveContainerView track = (AxisCurveContainerView)trackList.get(i);
				if(track.getHeight() > windowHeight){
					windowHeight = track.getHeight();
				}
			}
			windowHeight = windowHeight + 5;
		}

		PrintFrame pf = new PrintFrame();

		pf.setOriginalWindowWidth(windowWidth);
		pf.setOriginalWindowHeight(windowHeight);

		pf.setImageInfo();

		pf.setVisible(true);
	}
}
