/**
 * 
 */
package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.dialogs.MarkerDataViewer;

/**
 * @author Alex
 *
 */
public class LoadAnyAsciiAction implements ActionListener {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	/**
	 * @param jMainFrame
	 */
	public LoadAnyAsciiAction(MainFrameView jMainFrame) {
		this.jMainFrame = jMainFrame;
	}
	
	public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser();
		int retVal = chooser.showOpenDialog(null);
		
		if (retVal == JFileChooser.APPROVE_OPTION)	{
			File file = chooser.getSelectedFile();
			
			MarkerDataViewer preview = new MarkerDataViewer(file, jMainFrame);
			preview.setVisible(true);
		}
	}
	
}
