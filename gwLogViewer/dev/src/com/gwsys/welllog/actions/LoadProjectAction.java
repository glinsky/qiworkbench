/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.actions;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.gwsys.data.well.WksFileFilter;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.WorkSpace;
import com.gwsys.welllog.view.track.AxisCurveContainerView;

/**
 * Implements the action to load project file.
 *
 * @author Team
 *
 */
public class LoadProjectAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	boolean success = true;

	private File file;

	/**
	 * Constructor
	 *
	 * @param jMainFrame
	 */
	public LoadProjectAction(MainFrameView jMainFrame) {
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {

		JFileChooser jFileChooser = new JFileChooser("Loads");
		jFileChooser.setSelectedFile(new File("*.wks"));
		jFileChooser.addChoosableFileFilter(new WksFileFilter("wks"));
		jFileChooser.setMultiSelectionEnabled(false);
		int returnVal = jFileChooser.showOpenDialog(jMainFrame.getFrame());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			jFileChooser.setVisible(false);
			file = jFileChooser.getSelectedFile();
			if (!file.exists()) {
				success = false;
				new JOptionPane("No Such File!");
				return;
			}
		}

		jFileChooser = null;
		if (file == null)
			return;
		jMainFrame.getMainBuffer().setLoadProject(true);

		Timer timer = new Timer();
		MyTimerTask1 myTimerTask1 = new MyTimerTask1();
		timer.schedule(myTimerTask1, 100);
	}

	private void repaintMainFrame() {
		jMainFrame.getMainBuffer().setLoadProject(false);

		ViewCoreOperator.updateProjectTreeSelect();
		jMainFrame.getSonJSplitPane().getLeftTree().repaint();

		for (int i = 0; i < jMainFrame.getSonJSplitPane().getRightDesktopPane()
				.getInternalFrameViewList().size(); i++) {
			InternalFrameView internalFrameView = (InternalFrameView) jMainFrame
					.getSonJSplitPane().getRightDesktopPane()
					.getInternalFrameViewList().get(i);

			internalFrameView.revalidate();
			internalFrameView.repaint();

			for (int j = 0; j < internalFrameView.getTrackContainer()
					.getAxisCurveContainerList().size(); j++) {
				AxisCurveContainerView axisCurveContainerView = (AxisCurveContainerView) internalFrameView
						.getTrackContainer().getAxisCurveContainerList().get(j);
				axisCurveContainerView.repaintAll();
				axisCurveContainerView.getTitleAndCurveNamesPanel()
						.getHorizontalScrollBar().setValue(
								axisCurveContainerView.getHorValue());
				axisCurveContainerView.getBottomScrollPane()
						.getHorizontalScrollBar().setValue(
								axisCurveContainerView.getHorValue());
				axisCurveContainerView.getBottomScrollPane()
						.getVerticalScrollBar().setValue(
								axisCurveContainerView.getVerValue());
			}
			if (internalFrameView.getTrackContainer()
					.getSelectedAxisCurveContainer() != null) {
				ViewCoreOperator.selectTrack(internalFrameView
						.getTrackContainer().getSelectedAxisCurveContainer()
						.getWellName());
			}
		}
		if (jMainFrame.getMainBuffer().getInternalFrameView() != null) {
			ViewCoreOperator.selectFrame(jMainFrame.getMainBuffer()
					.getInternalFrameView().getTitle());
			jMainFrame.getMainBuffer().getInternalFrameView()
					.getTrackContainer().repaint();
		}

		jMainFrame.setCursor(Cursor.getDefaultCursor());
		if (jMainFrame.getMainBuffer().getInternalFrameView() != null) {
			((InternalFrameView) jMainFrame.getMainBuffer()
					.getInternalFrameView()).setCursor(Cursor
					.getDefaultCursor());
		}
		// upon request : leave active frame on the screen
		JInternalFrame[] all = jMainFrame.getMainBuffer().getDesktopPaneView()
				.getAllFrames();
		InternalFrameView active = jMainFrame.getMainBuffer()
				.getInternalFrameView();
		try {
			for (int i = 0; i < all.length; i++)
				if (!all[i].equals(active))
					all[i].setIcon(true);
				else
					all[i].setIcon(false);
		} catch (PropertyVetoException e) {
		}
		jMainFrame.getStatusBar().showProgressBar(false);
		jMainFrame.setMessage("Loading project is done!");
	}

	private void LoadFile() {
		jMainFrame.getStatusBar().showProgressBar(true);
		jMainFrame.setMessage("Loading...");
		jMainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if (jMainFrame.getMainBuffer().getInternalFrameView() != null) {
			((InternalFrameView) jMainFrame.getMainBuffer()
					.getInternalFrameView()).setCursor(Cursor
					.getPredefinedCursor(Cursor.WAIT_CURSOR));
		}

		WorkSpace.loadWorkSpaceFromXml(file.getPath(), DataBuffer
				.getJMainFrame());

		SwingUtilities.invokeLater(new RepaintTask());
		//This repainting task must be controller by swing.
	}

	class MyTimerTask1 extends TimerTask {

		@Override
		public void run() {
			LoadFile();
		}

	}

	class RepaintTask implements Runnable { //TimerTask {

		public void run() {
			repaintMainFrame();
		}

	}
}
