/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JOptionPane;

import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.track.MarkerConnectorPoint;
import com.gwsys.welllog.view.track.TrackContainerView;

/**
 * Action handles marker cables.
 * @author Team
 *
 */
public class CreateMarkerCableAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	public CreateMarkerCableAction(MainFrameView jMainFrame){
		this.jMainFrame = jMainFrame;
	}

	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent arg0) {
		if (((MainFrameView) jMainFrame).getSonJSplitPane().getRightDesktopPane().getSelectedInternalFrameView() == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select an InternalFrame!", "Message", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		TrackContainerView tcv = ((MainFrameView) jMainFrame).getSonJSplitPane().getRightDesktopPane().getSelectedInternalFrameView().getTrackContainer();
		if (tcv.getMarkerBuffer().size() < 2) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select at least two markers.", "Message", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		for (int i = 0; i < tcv.getMarkerBuffer().size() - 1; i++) {
			MarkerConnectorPoint point1 = (MarkerConnectorPoint) tcv.getMarkerBuffer().get(i);
			MarkerConnectorPoint point2 = (MarkerConnectorPoint) tcv.getMarkerBuffer().get(i+1);

			point1.getCm().setRightMarker(point2.getCm());
			point2.getCm().setLeftMarker(point1.getCm());

			tcv.getMarkerCable1().add(point1);
			tcv.getMarkerCable2().add(point2);
		}
		this.jMainFrame.repaint();
	}
}
