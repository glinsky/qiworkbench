/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;

import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.gwsys.welllog.QILogViewer;
import com.gwsys.welllog.mainframe.MainFrameView;

/**
 * Handles data from qiProjectManager component.
 * @author Louis.
 *
 */
public class ImportQiProject extends AbstractAction{
	private static final long serialVersionUID = 1L;
	private static String projectManager="qiProjectManager";

	private MainFrameView jMainFrame = null;

	/**
	 *
	 * @param jMainFrame
	 */
	public ImportQiProject(MainFrameView jMainFrame){
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		jMainFrame.setMessage("Importing data from qiProjectManger");
		if (jMainFrame instanceof QILogViewer){
			MessagingManager mesm =((QILogViewer)jMainFrame).getAgent().getMessagingManager();

			mesm.sendRequest(QIWConstants.CMD_ROUTE_MSG, QIWConstants.GET_PROJECT_CMD,
					mesm.getComponentDescFromDisplayName(projectManager),
					QIWConstants.ARRAYLIST_TYPE, new ArrayList());

			System.out.println(mesm.getMatchingResponse(projectManager)
					.getContent().toString());
		}

	}
}
