/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.io.Serializable;

import javax.swing.JOptionPane;

import com.gwsys.print.ImageGenerator;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.InternalFrameView;

public class PageSetupAction implements ActionListener, Serializable {
	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	public PageSetupAction(MainFrameView jMainFrame){
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		InternalFrameView selectedInternalFrameView = ((MainFrameView) jMainFrame).getSonJSplitPane()
		.getRightDesktopPane().getSelectedInternalFrameView();

		if (selectedInternalFrameView == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select an InternalFrame!", "Message", JOptionPane.INFORMATION_MESSAGE);
			return;
		}


		PrinterJob printerJob = PrinterJob.getPrinterJob();
		PageFormat page = printerJob.defaultPage();
		if(ImageGenerator.pageFormat != null)
		{
			page = printerJob.pageDialog(ImageGenerator.pageFormat);
		}
		else
		{
			page = printerJob.pageDialog(printerJob.defaultPage());
		}
        ImageGenerator.pageFormat = page;
        ImageGenerator.setPageLayout(true,1,1);
	}
}
