/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JOptionPane;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.dialogs.SetTimeToDepthDialog;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.YAxisView;

/**
 * Action performs a display in depth.
 * @author Team
 *
 */
public class DepthAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	public DepthAction(MainFrameView jMainFrame) {
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		if (((MainFrameView) jMainFrame).getSonJSplitPane()
				.getRightDesktopPane().getSelectedInternalFrameView() == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select a Track!", "Message",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		AxisCurveContainerView selectedAxisCurveContainerView = jMainFrame
				.getSonJSplitPane().getRightDesktopPane()
				.getSelectedInternalFrameView().getTrackContainer()
				.getSelectedAxisCurveContainer();
		if (selectedAxisCurveContainerView == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select a Track!", "Message",
					JOptionPane.INFORMATION_MESSAGE);
			// System.out.println("please select a track");
			return;
		}
		YAxisView yav = ((MainFrameView) jMainFrame).getSonJSplitPane()
				.getRightDesktopPane().getSelectedInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer()
				.getYAxisView();
		if (yav.getYType().equals(TrackTypeList.Y_DEPTH_MD)) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"The current type is already Depth.", "Message",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		if ((selectedAxisCurveContainerView.getTrackType() == ConstantVariableList.TRACK_TYPE_SU ||
				selectedAxisCurveContainerView
				.getTrackType() == ConstantVariableList.TRACK_TYPE_MIXED)
				&& selectedAxisCurveContainerView.getTimeToDepthConverter() == null) {
			if (selectedAxisCurveContainerView.getTimeToDepthConverter() == null) {
				SetTimeToDepthDialog setTimeToDepthDialog = new SetTimeToDepthDialog(
						selectedAxisCurveContainerView, null, true);
				setTimeToDepthDialog.setVisible(true);
			}
		} else {
			selectedAxisCurveContainerView.getCurveView().setCurveRedrawFlag(
					true);
			selectedAxisCurveContainerView.getCurveView().setFillRedrawFlag(
					true);
			selectedAxisCurveContainerView.getCurveView().setMarkerRedrawFlag(
					true);
			yav.setYType(TrackTypeList.Y_DEPTH_MD);
			selectedAxisCurveContainerView.repaintAll();
		}
	}
}
