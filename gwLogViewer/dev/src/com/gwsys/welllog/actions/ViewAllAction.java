/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.actions;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.TrackContainerView;

/**
 * The action performs the zooming to see all vertical range.
 *
 * @author Team
 *
 */
public class ViewAllAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	public ViewAllAction(MainFrameView jMainFrame) {
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		viewAll();
	}

	/**
	 * Show all curves in current view.
	 *
	 */
	public void viewAll() {
		if (jMainFrame.getSonJSplitPane().getRightDesktopPane()
				.getSelectedInternalFrameView() == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select a section!", "Message",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		TrackContainerView section = jMainFrame.getSonJSplitPane()
				.getRightDesktopPane().getSelectedInternalFrameView()
				.getTrackContainer();
		ArrayList accList = section.getAxisCurveContainerList();
		for (int k = 0; k < accList.size(); k++) {
			AxisCurveContainerView acc = (AxisCurveContainerView) accList
					.get(k);

			CurveView cv = acc.getCurveView();
			int CurveCount = cv.getPanelOrder().getOrders().size();
			int panelWidthSum = 0;
			for (int i = 0; i < CurveCount; i++) {
				panelWidthSum = panelWidthSum
						+ cv.getPanelOrder().getPanelWidth(i);
			}
			if (panelWidthSum > acc.getWidth()) {
				int widthCurve = (acc.getWidth() - 50) / (CurveCount - 1);
				cv.getCurveLayer().setBesideTypeWidth(widthCurve);
				cv.setCurveRedrawFlag(true);
				cv.setFillRedrawFlag(true);
			}

			int width = cv.getWidth();
			int newHeight = (int) acc.getBottomScrollPane().getVisibleRect().height;
			if (newHeight == 0)
				return;

			float depthScale = 0;
			int pixValue = acc.getCurveView().getToolkit()
					.getScreenResolution();
			float range = acc.getCurveLayer().getYSetBottomDepth()
					- acc.getCurveLayer().getYSetTopDepth();
			if (acc.getDepthUnit() == CurveData.METER
					|| acc.getDepthUnit() == CurveData.SECOND) {
				int trueHeight = (int) (range
						* ConstantVariableList.METER_TO_INCH * pixValue);
				depthScale = (float) (trueHeight / newHeight);
			} else {
				int trueHeight = (int) (range
						* ConstantVariableList.FOOT_TO_INCH * pixValue);
				depthScale = (float) (trueHeight / newHeight);
			}
			acc.setDepthScale(depthScale);

			cv.setPreferredSize(new Dimension(width, newHeight));
			acc.getYAxisView().setPreferredSize(
					new Dimension(acc.getYAxisView().getWidth(), newHeight));

			acc.getJp().revalidate();
			acc.getJp().repaint();
			acc.repaintAll();
		}
	}
}
