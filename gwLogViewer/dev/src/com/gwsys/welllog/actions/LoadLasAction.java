/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
 and distributed by BHP Billiton Petroleum under license. 

 This program is free software; you can redistribute it and/or modify it 
 under the terms of the GNU General Public License Version 2 as as published 
 by the Free Software Foundation.
 
 This program is distributed in the hope that it will be useful, 
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 more details.

 You should have received a copy of the GNU General Public License along with 
 this program; if not, write to the Free Software Foundation, Inc., 
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.actions;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.dialogs.ChooserDialog;

public class LoadLasAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;
	
	private MainFrameView jMainFrame = null;
	
	private File[] files = null;
	private Timer timer = null;	
	private MyTimerTask myTimerTask = null;
	/**
	 * Constructor.
	 * @param jMainFrame
	 */
	public LoadLasAction(MainFrameView jMainFrame){
		this.jMainFrame = jMainFrame;
	}
	
	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent arg0) {
		ChooserDialog chooser = DataBuffer.getJMainFrame().getChooser();
		chooser.clearFilter();
		chooser.setFilter("las");
		chooser.setMultiSelect(true);
		chooser.showDialog();
		((MainFrameView) jMainFrame).setFileChooser(chooser.getJfc());

		files = chooser.getFiles();
		if (files == null)
			return;
		timer = new Timer();
		myTimerTask = new MyTimerTask();
		timer.schedule(myTimerTask,100);
	}
	
	private void LoadFile(){
		myTimerTask = null;
		timer=null;
		
		jMainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if(jMainFrame.getMainBuffer().getInternalFrameView()!=null){
			Cursor cursor = new Cursor(Cursor.WAIT_CURSOR);
			((InternalFrameView)jMainFrame.getMainBuffer().getInternalFrameView()).setCursor(cursor);
			
		}
		if (files != null) {
			
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				OperationClass.initialTreeAndHashMap(file, jMainFrame);				
			}
		}
		jMainFrame.setCursor(Cursor.getDefaultCursor());
		if(jMainFrame.getMainBuffer().getInternalFrameView()!=null){
			((InternalFrameView)jMainFrame.getMainBuffer().getInternalFrameView()).setCursor(Cursor.getDefaultCursor());
		}
	}
	
	class MyTimerTask extends TimerTask{

		@Override
		public void run() {
			LoadFile();
		}		
	}
}
