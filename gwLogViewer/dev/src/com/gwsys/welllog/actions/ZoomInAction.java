/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.TrackContainerView;

/**
 * Action performs zooming in function.
 *
 * @author Team
 *
 */
public class ZoomInAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;

	public ZoomInAction(MainFrameView jMainFrame) {
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		if (((MainFrameView) jMainFrame).getSonJSplitPane()
				.getRightDesktopPane().getSelectedInternalFrameView() == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(),
					"Please select a section!", "Message",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		int zoomInParameter = 2;
		TrackContainerView section = jMainFrame.getSonJSplitPane()
				.getRightDesktopPane().getSelectedInternalFrameView()
				.getTrackContainer();
		ArrayList accList = section.getAxisCurveContainerList();
		for (int k = 0; k < accList.size(); k++) {
			AxisCurveContainerView selectedAcc = (AxisCurveContainerView) accList
					.get(k);

			selectedAcc.setDepthScale(selectedAcc.getDepthScale()
					/ zoomInParameter);
			ViewCoreOperator.setDepthScale(selectedAcc);
		}
	}
}
