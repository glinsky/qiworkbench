/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JOptionPane;

import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.dialogs.EditLithologyBesideDialog;
import com.gwsys.welllog.view.dialogs.EditLithologyTopDialog;

public class CreateLithologyCurveAction implements ActionListener, Serializable {

	private static final long serialVersionUID = 1L;

	private MainFrameView jMainFrame = null;
	
	public CreateLithologyCurveAction(MainFrameView jMainFrame){
		this.jMainFrame = jMainFrame;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		if (jMainFrame.getSonJSplitPane().getRightDesktopPane().getSelectedInternalFrameView() == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(), "No curve selected!", "Message", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		if (jMainFrame.getSonJSplitPane().getRightDesktopPane().getSelectedInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer() == null) {
			JOptionPane.showMessageDialog(jMainFrame.getFrame(), "No curve selected", "Message", JOptionPane.INFORMATION_MESSAGE);

			return;
		}
		CurveView cv;
		cv = ((MainFrameView) jMainFrame).getSonJSplitPane().getRightDesktopPane().getSelectedInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer().getCurveView();
		if (cv.getCurveLayer().getLayerType().equals(TrackTypeList.REALIZATION_TOP)) {
			if (cv.getCurveLayer().getCurveSelectedList().size() < 2) {
				JOptionPane.showMessageDialog(jMainFrame.getFrame(), "Please select at least two curves!", "Message", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			EditLithologyTopDialog cltd = new EditLithologyTopDialog(cv);
			cltd.setVisible(true);
		}
		if (cv.getCurveLayer().getLayerType().equals(TrackTypeList.REALIZATION_BESIDE)) {
			if (cv.getCurveLayer().getCurveSelectedList().size() < 1) {
				JOptionPane.showMessageDialog(jMainFrame.getFrame(), "Please select a curve!", "Message", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			EditLithologyBesideDialog clbd = new EditLithologyBesideDialog(cv);
			clbd.setVisible(true);
		}
	}


}
