/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.welllog.actions;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.mainframe.MainFrameView;

/**
 * This action shows the version info.
 *
 * @author Louis
 *
 */
public class AboutAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	public static final String version = "1.2";

	private static final String build = "2007.06.19";

	private MainFrameView jMainFrame = null;

	public AboutAction(MainFrameView jMainFrame, String caption) {
		super(caption);
		this.jMainFrame = jMainFrame;
	}

	public void actionPerformed(ActionEvent arg0) {
		JEditorPane aboutTextEditorPane = new JEditorPane();
		aboutTextEditorPane.setEditable(false);
		aboutTextEditorPane.setContentType("text/html");

		StringBuffer sb = new StringBuffer();
		sb.append("<font face=\"Arial\" size=3>");
		sb.append("<b>gwLogViewer</b> - a viewer to visualize and analyze well log data<br>");
		sb.append("Version: " + version + "<br>");
		sb.append("Build id: " + build + "<br><br>");
		sb.append("Copyright (C) 2006 G&W Systems Consulting, Corp.<br>");
		sb.append("It is distributed by BHP Billiton Petroleum under license.<br><br>");
		sb.append("The gwLogViewer comes with ABSOLUTELY NO WARRANTY.<br><br>");
		sb.append("This is free software, and you are welcome to redistribute it under certain conditions. ");
		sb.append("<br>Contact to <a href==\"http://www.g-w-systems.com/\">G&W Systems Corp.</a>");
		sb.append("</font>");
		sb.append("<P>This project includes free softwares developed by the <br>");
		sb.append("<ul><li> <a href=\"http://www.apache.org\">Apache Softrware Foundation</a>");
		sb.append("<li><a href=\"http://www.jfree.org\">Object Refinery Ltd. (JFreeChart)</a>");
		sb.append("<li><a href=\"http://www.andykhan.com/jexcelapi/index.html\">Andy Khan's JExcelAPI</a>");
		sb.append("<li><a href=\"http://java.sun.com\">Sun Microsystems, Inc.</a>");
		sb.append("</ul>");
		aboutTextEditorPane.setText(sb.toString());

		final JDialog dialog = new JDialog(jMainFrame.getFrame(),
				"Application Info");
		dialog.getContentPane()
				.add(new JLabel(IconResource.getInstance().getImageIcon(
								"info.gif")), BorderLayout.WEST);
		dialog.getContentPane().add(aboutTextEditorPane, BorderLayout.EAST);

		JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	dialog.dispose();
            }
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(okButton);
        dialog.getContentPane().add(bpanel, BorderLayout.SOUTH);

		aboutTextEditorPane.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent evt) {
				if (evt.getEventType() == HyperlinkEvent.EventType.ENTERED) {
					dialog.setCursor(new java.awt.Cursor(
							java.awt.Cursor.HAND_CURSOR));
				} else if (evt.getEventType() == HyperlinkEvent.EventType.EXITED) {
					dialog.setCursor(new java.awt.Cursor(
							java.awt.Cursor.DEFAULT_CURSOR));
				} else if (evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					openWebBrowser("http://www.g-w-systems.com");
				}
			}
		});
		dialog.pack();
		dialog.setLocationRelativeTo(jMainFrame.getFrame());
		dialog.setVisible(true);
	}

	//This method comes from qiworkbench.AboutDialog
	private void openWebBrowser(String url) {
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Mac OS")) {
				Class fileMgr = Class.forName("com.apple.eio.FileManager");
				Method openURL = fileMgr.getDeclaredMethod("openURL",
						new Class[] { String.class });
				openURL.invoke(null, new Object[] { url });
			} else if (osName.startsWith("Windows"))
				Runtime.getRuntime().exec(
						"rundll32 url.dll,FileProtocolHandler " + url);
			else { // assume Unix or Linux
				String[] browsers = { "firefox", "opera", "konqueror",
						"epiphany", "mozilla", "netscape" };
				String browser = null;
				for (int count = 0; count < browsers.length && browser == null; count++)
					if (Runtime.getRuntime().exec(
							new String[] { "which", browsers[count] })
							.waitFor() == 0)
						browser = browsers[count];
				if (browser == null)
					throw new Exception("Could not find web browser");
				else
					Runtime.getRuntime().exec(new String[] { browser, url });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Error attempting to launch web browser" + ":\n"
							+ e.getLocalizedMessage());
		}
	}
}
