/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.Serializable;

import javax.swing.JOptionPane;

import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.dialogs.ChooserDialog;
import com.gwsys.welllog.view.dialogs.SetSurveryDialog;
import com.gwsys.welllog.view.yaxis.DepthConverter;
import com.gwsys.welllog.view.yaxis.DataConverterFactory;

public class ImprotingSurveyAction implements ActionListener, Serializable {
	
private static final long serialVersionUID = 1L;
	
	public ImprotingSurveyAction(){
		
	}
	
	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent arg0) {
		ChooserDialog chooser = DataBuffer.getJMainFrame().getChooser();
		chooser.clearFilter();
		chooser.setFilter("xls");		
		chooser.setMultiSelect(false);
		chooser.showDialog();
		
		if(!chooser.isOpen()){
			return;
		}
							
		File file = chooser.getFile();			
		String fileName = file.getName();
		int nLen = fileName.lastIndexOf(".");
		String surveyHashKey = fileName.substring(0,nLen);
		
		DepthConverter converter = (DepthConverter)(DataBuffer.getJMainFrame().getMainBuffer().getSurveyHashMap().get(surveyHashKey));
		if (converter == null) {
			converter = DataConverterFactory.createInstance(file.getPath(),TrackTypeList.MD_TO_TVD);
			if (converter == null) {
				// alert something
				JOptionPane.showMessageDialog(null, "Cann't parse the selected survey file!");
				return;
			}				
		}		
		ChooserDialog.setPath(file.getPath());
		
		if (!DataBuffer.getJMainFrame().getMainBuffer().getAllSurveys().contains(file.getPath())) {
			DataBuffer.getJMainFrame().getMainBuffer().getAllSurveys().add(file.getPath()); // save
		}
		
		if(DataBuffer.getJMainFrame().getMainBuffer().getMDDepthConverter() == null){
			DataBuffer.getJMainFrame().getMainBuffer().setMDDepthConverter(converter);
		}
		
		SetSurveryDialog surveryDialog = new SetSurveryDialog(converter);
		surveryDialog.setVisible(true);
		
	}
}
