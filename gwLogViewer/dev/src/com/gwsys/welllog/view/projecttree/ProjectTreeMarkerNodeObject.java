/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.projecttree;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectTreeMarkerNodeObject implements Serializable{
	public static int WELL_NAME = 0;
	
	public static int TRACK_ID = 1;
	private static final long serialVersionUID = 1L;

	private String markerName;
	private ArrayList<String> wellNameList = new ArrayList<String>();
	
	private ArrayList<String> trackIDList = new ArrayList<String>();

    public boolean isDraged;

    /**
     * Constructor
     * @param markerName
     */
    public ProjectTreeMarkerNodeObject(String markerName) {
		this.markerName = markerName.trim();
		isDraged = true;
	}
	
	public ProjectTreeMarkerNodeObject(String markerName , String container , int markerType) {
		this(markerName);
		if(markerType == WELL_NAME)
			wellNameList.add(container);
		else
			trackIDList.add(container);
	}

	/**
	 * Same as marker name
	 */
	public String toString() {
		return markerName;
	}

	public String getMarkerName() {
		return markerName;
	}

	public void setMarkerName(String markerName) {
		this.markerName = markerName;
	}

	public boolean isDraged() {
		return isDraged;
	}

	public void setDraged(boolean isDraged) {
		this.isDraged = isDraged;
	}

	public ArrayList<String> getWellNameList() {
		return wellNameList;
	}

	public void setWellNameList(ArrayList<String> wellNameList) {
		this.wellNameList = wellNameList;
	}
	
	
	public ArrayList<String> getTrackIDList() {
		return trackIDList;
	}


	public void setTrackIDList(ArrayList<String> trackIDList) {
		this.trackIDList = trackIDList;
	}

	public boolean wellExist(String wellName){
		for(int i=0 ; i<wellNameList.size() ; i++){
		    if(wellNameList.get(i).equals(wellName))
		    	return true;
		}
		return false;
	}
	
}
