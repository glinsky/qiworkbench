package com.gwsys.welllog.view.projecttree;

import java.io.Serializable;

public class ProjectTreeCurveNodeObject implements Serializable{

	private static final long serialVersionUID = 1L;
	public String curveName;
    public String curveHashmapKey;

    public ProjectTreeCurveNodeObject(String curveName,String curveHashmapKey)
    {
        this.curveName = curveName;
        this.curveHashmapKey = curveHashmapKey; //use for getting status from hashmap
    }
    //must over this function
    public String toString() {
        return curveName;
    }
 
	public String getCurveName() {
		return curveName;
	}

	public void setCurveName(String curveName) {
		this.curveName = curveName;
	}
	public String getCurveHashmapKey() {
		return curveHashmapKey;
	}
}
