/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.projecttree;

import java.awt.HeadlessException;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.track.MarkerConnectorPoint;

/**
 * Defaine a tree to manage the data.
 * @author China Team
 *
 */
public class ProjectTreeView extends JTree implements Serializable{

	private static final long serialVersionUID = 1L;

	private DefaultMutableTreeNode root = null;

	private DefaultMutableTreeNode wells = null;
	private DefaultMutableTreeNode wellTops = null;
	private DefaultMutableTreeNode checkShots = null;

	private DefaultTreeModel defaultModel = null;
	private ProjectTreeController treeController = null;
	private String transferredObject = "";		//the selected node' key in hashmap
	private DefaultMutableTreeNode nodeDraged; //the selected node,use for checkbox

	private ProjectTreeRenderer ltr = null;

	private ArrayList<String> allWells = new ArrayList<String>();

	private ArrayList allWellTops = new ArrayList();

	public ProjectTreeView(){
		this.setModel(getTreeModel());

		if(treeController == null)
		{
			treeController = new ProjectTreeController();
		}
		this.addMouseListener(treeController);
		this.addMouseMotionListener(treeController);
		this.setTransferHandler(new TransferHandler("transferredObject"));
	}
	/**
	 * @author luming
	 * return treemodel
	 * @return
	 */
	public TreeModel getTreeModel() {
	    if(root == null)
	    {
	    	root = new DefaultMutableTreeNode("Project");

	    	wells = new DefaultMutableTreeNode(DataObjectTreeNode.WELLS);
	    	wellTops = new DefaultMutableTreeNode(DataObjectTreeNode.WELL_TOP);
	    	checkShots = new DefaultMutableTreeNode(DataObjectTreeNode.CHECK_SHOT);
	    	root.add(wells);
	    	root.add(wellTops);
	    	root.add(checkShots);
	    }
	    if(defaultModel == null)
	    {
	    	defaultModel = new DefaultTreeModel(root);
	    }
	    //if not null, set this root as the model root, because add the operation is  on this root
	    defaultModel.setRoot(root);
	    return defaultModel;
	}

	/**
	 * Initialize the tree.
	 *
	 */
	public void setInitialize() {
		root = null;
		defaultModel = null;
		this.setModel(getTreeModel());

	}
	/**
	 * Add Well Top into Project tree.
	 *
	 * @param wellName
	 * @param markerName
	 * @param markerType
	 * @param isDraged
	 */
	public void addWellTopNode(String wellName , String markerName , int markerType , boolean isDraged){
		boolean hasNode = false;
		for(int i=0; i<wellTops.getChildCount();i++){
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) wellTops.getChildAt(i);
			ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject)nodeChild.getUserObject();
			if(markerNode.getMarkerName().equals(markerName.trim())){
				hasNode = true;
				if(markerType == ProjectTreeMarkerNodeObject.WELL_NAME){
					if(!markerNode.wellExist(wellName))
						markerNode.getWellNameList().add(wellName);
				}else{
					markerNode.getTrackIDList().add(wellName);
				}
				break;
			}
		}

		if(!hasNode){
			ProjectTreeMarkerNodeObject markerNode = new ProjectTreeMarkerNodeObject(markerName , wellName , markerType);
			markerNode.setDraged(isDraged);
			DefaultMutableTreeNode nodeMarkerName = new DefaultMutableTreeNode(markerNode);
			wellTops.add(nodeMarkerName);
		}

        // reset model
		this.setModel(getTreeModel());
		// reset the render
		this.setCellRenderer(this.getLtr());
	}


	public void renameWellTopNode(String oldMarkerName , String newMarkerName){
		for(int i=0; i<wellTops.getChildCount();i++){
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) wellTops.getChildAt(i);
			ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject)nodeChild.getUserObject();
			if(markerNode.getMarkerName().equals(oldMarkerName)){
				markerNode.setMarkerName(newMarkerName);
				break;
			}
		}

        // reset model
		this.setModel(getTreeModel());
		// reset the render
		this.setCellRenderer(this.getLtr());

	}

	/**
	 * Combin smae name of markersinto one node.
	 * @param markerList
	 */
	public void connectWellTopNode(ArrayList markerList){
		// add a new node
		String markerName = ((MarkerConnectorPoint)markerList.get(0)).getCm().getMarkerName();
		ProjectTreeMarkerNodeObject markerNode = new ProjectTreeMarkerNodeObject(markerName);

        // delete all
		for(int i=0 ; i<markerList.size() ; i++){
			DefaultMutableTreeNode removeNode = this.removeWellTopNode(((MarkerConnectorPoint)markerList.get(i)).getCm().getMarkerName());
			ProjectTreeMarkerNodeObject markerNodeObj = (ProjectTreeMarkerNodeObject)removeNode.getUserObject();
			for(int j=0 ; j<markerNodeObj.getWellNameList().size() ; j++)
				markerNode.getWellNameList().add(markerNodeObj.getWellNameList().get(j));
		}

		DefaultMutableTreeNode nodeMarkerName = new DefaultMutableTreeNode(markerNode);
		wellTops.add(nodeMarkerName);

        // reset model
		this.setModel(getTreeModel());
		// reset the render
		this.setCellRenderer(this.getLtr());
	}

	public DefaultMutableTreeNode removeWellTopNode(String markerName){
		DefaultMutableTreeNode nodeChild = null;
		for(int i=0; i<wellTops.getChildCount();i++){
			nodeChild = (DefaultMutableTreeNode) wellTops.getChildAt(i);
			if(((ProjectTreeMarkerNodeObject)nodeChild.getUserObject()).getMarkerName().equals(markerName)){
				wellTops.remove(nodeChild);
				break;
			}
		}

        // reset model
		this.setModel(getTreeModel());
		// reset the render
		this.setCellRenderer(this.getLtr());

		return nodeChild;
	}

	public DefaultMutableTreeNode removeWellTopNode(String markerName , String wellName){
		DefaultMutableTreeNode nodeChild = null;
		for(int i=0; i<wellTops.getChildCount();i++){
			nodeChild = (DefaultMutableTreeNode) wellTops.getChildAt(i);
			ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject)nodeChild.getUserObject();
			if(markerNode.getMarkerName().equals(markerName)){
				markerNode.getWellNameList().remove(wellName);
				break;
			}
		}

        // reset model
		this.setModel(getTreeModel());
		// reset the render
		this.setCellRenderer(this.getLtr());

		return nodeChild;
	}
	//June 30
	public DefaultMutableTreeNode getWellTopNode(String markerName){
		DefaultMutableTreeNode nodeChild = null;
		for(int i=0; i<wellTops.getChildCount();i++){
			nodeChild = (DefaultMutableTreeNode) wellTops.getChildAt(i);
			if(((ProjectTreeMarkerNodeObject)nodeChild.getUserObject()).getMarkerName().equals(markerName)){
				break;
			}
		}
		return nodeChild;
	}

	public void updateWellTopNodeTrackName(String trackID, String wellName){
		for(int i=0; i<wellTops.getChildCount();i++){
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) wellTops.getChildAt(i);
			ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject)nodeChild.getUserObject();
			for(int j=0 ; j<markerNode.getTrackIDList().size() ; j++){
				if(markerNode.getTrackIDList().get(j).equals(trackID)){
					markerNode.getWellNameList().add(wellName);
					markerNode.getWellNameList().remove(trackID);
					break;
			    }
			}
		}
		this.revalidate();
		this.repaint();
	}

	public void addCheckShotNode(String CheckShotName, File source){
		ProjectTreeCheckShotNodeObject checkShotNode =
			new ProjectTreeCheckShotNodeObject(CheckShotName);
		DefaultMutableTreeNode node = new DataObjectTreeNode(checkShotNode,
				source.getAbsolutePath());
		checkShots.add(node);
        // reset model
		this.setModel(getTreeModel());
		// reset the render
		this.setCellRenderer(this.getLtr());
	}


	public boolean containsSameNameMarker(String newMarkerName){
		for(int i=0; i<wellTops.getChildCount();i++){
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) wellTops.getChildAt(i);
			if(((ProjectTreeMarkerNodeObject)nodeChild.getUserObject()).getMarkerName().equals(newMarkerName)){
				return true;
			}
		}
		return false;
	}

	/**
	 * add node to the tree
	 * @param wellname lv 1
	 * @param filename lv 2
	 * @param curvename lv 3
	 */
	public void addCurveNameNode(String wellname,String filename,ArrayList curveNameList)
	{

		TreeNode level1Node = checkWellName(wellname);

		if(level1Node == null) //
		{
			DefaultMutableTreeNode nodeWellName =
				new DataObjectTreeNode(wellname, filename);
			DefaultMutableTreeNode nodeWellLogs =
				new DefaultMutableTreeNode(DataObjectTreeNode.WELL_LOG);
			wells.add(nodeWellName);
			nodeWellName.add(nodeWellLogs);

			//because the first in curvelist is depth, begin from the second one
			for(int i=0;i<curveNameList.size();i++)
			{
				String curveName = (String)curveNameList.get(i);
				String curveHashKey = wellname+OperationClass.hashMapNameSeparator
				+curveName;

				DefaultMutableTreeNode nodeCurveName =
					new DefaultMutableTreeNode(
							new ProjectTreeCurveNodeObject(curveName,
									curveHashKey));
				nodeWellLogs.add(nodeCurveName);
			}
		}

		defaultModel.reload(wells);
		//reset the render
		this.setCellRenderer(this.getLtr());
	}

	//find the wellname in lv1 node , if have ,return it. if not ,return null
	public TreeNode checkWellName(String wellname)
	{
		TreeNode theWellName = null;
		if(wells.getChildCount()>0)//if have children
		{
			for(Enumeration e=wells.children(); e.hasMoreElements(); ) //for each child
			{
				TreeNode n = (TreeNode)e.nextElement(); //get each node

				if(n.toString().equals(wellname))  //cannot use ==
				{
					theWellName = n;
				}
			}
		}

		return theWellName;
	}

	//find las in lv2 node
	public TreeNode checkFileName(TreeNode nfather,String filename)
	{
		if(nfather.getChildCount()>0)
		{
			for(Enumeration e=nfather.children(); e.hasMoreElements(); )
			{
				TreeNode nc = (TreeNode)e.nextElement();

				if(nc.toString().equals(filename))
				{
					return nc;
				}
			}
		}
		return null;
	}

	/**
	 * Returns the object to be transferred.
	 * @return the object to be transferred.
	 */
	public String getTransferredObject() {
		return transferredObject;
	}

	/**
	 * Sets the dropping object.
	 * @param the dropping object.
	 */
	public void setTransferredObject(String draggingObject) {
		if (draggingObject==null){
			try {
				Clipboard clipboard= getToolkit().getSystemClipboard();
				Transferable transfer = clipboard.getContents(clipboard);
				if (transfer instanceof StringSelection){
					draggingObject = (String)transfer.getTransferData(DataFlavor.stringFlavor);
				}else{
					System.out.println(transfer);
				}
			} catch (HeadlessException e) {
				e.printStackTrace();
			} catch (UnsupportedFlavorException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		if (draggingObject==null){
			System.out.println("Can not get data from Clipboard!!");
			return;
		}else
			System.out.println(draggingObject);
		String[] drop = null;
		if (draggingObject.indexOf(";")>0){
			drop = draggingObject.split(";");
		}else
			drop = new String[]{draggingObject};
		if (!drop[0].equalsIgnoreCase("Well"))
			return;
		File newFile = new File(drop[1]);
		if (newFile.exists()){
			if (DataBuffer.getJMainFrame()==null){
				DataBuffer.setJMainFrame(getMainFrame(this));
			}
			OperationClass.initialTreeAndHashMap(newFile,
				DataBuffer.getJMainFrame());
		}
		//addCurveNameNode();
	}

	private MainFrameView getMainFrame(JComponent com){
		if (com.getParent() instanceof MainFrameView)
			return (MainFrameView)com.getParent();
		else
			return getMainFrame((JComponent)com.getParent());
	}

	public void resetNodeClicked(String nodeClicked){
		this.transferredObject = nodeClicked;
	}
	public String changeNodeValueToHashkey(DefaultMutableTreeNode node)
	{
		return node.getParent().getParent().toString()+OperationClass.hashMapNameSeparator+node.toString();
	}
	/**
	 * return the render
	 * @return
	 */
	public ProjectTreeRenderer getLtr() {
		if(ltr == null)
		{
			ltr = new ProjectTreeRenderer(this);
		}
		return ltr;
	}
	public void setLtr(ProjectTreeRenderer ltr) {
		this.ltr = ltr;
	}
	public DefaultMutableTreeNode getNodeDraged() {
		return nodeDraged;
	}
	public void setNodeDraged(DefaultMutableTreeNode nodeDraged) {
		this.nodeDraged = nodeDraged;
	}
	/**
	 * @return root node
	 */
	public DefaultMutableTreeNode getRoot() {
		return root;
	}
	/**
	 * @param root DefaultMutableTreeNode
	 */
	public void setRoot(DefaultMutableTreeNode root) {
		this.root = root;
	}
	/**
	 * reset the project tree to empty
	 *
	 */
	public void clearContent() {
		DefaultTreeModel tm=new  DefaultTreeModel(root);
		root.removeAllChildren();
		tm.reload();
	}
	public ArrayList<String> getAllWells() {
		return allWells;
	}
	public ArrayList getAllWellTops() {
		return allWellTops;
	}
	//June 16
	public DefaultMutableTreeNode getWellTops() {
		return wellTops;
	}
	public void setWellTops(DefaultMutableTreeNode wellTops) {
		this.wellTops = wellTops;
	}

	/**
	 * remove entire well log including its childs
	 *
	 * @param wellLog
	 */
	public void removeWellLog(DefaultMutableTreeNode wellLog) {
		if (wellLog == null) {
			return;
		}

		defaultModel.removeNodeFromParent(wellLog);

		defaultModel.reload(wells);
	}

	public DefaultMutableTreeNode getCurveNameNode(String wellName,String curveName)
	{
		TreeNode wellNode = checkWellName(wellName);

		for(int i=0; i<wellNode.getChildAt(0).getChildCount();i++){
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) wellNode.getChildAt(0).getChildAt(i);
			if(((ProjectTreeCurveNodeObject)nodeChild.getUserObject()).getCurveName().equals(curveName)){
				return nodeChild;
			}
		}
		return null;
	}

	/**
	 * Changes the path of project.
	 * @param name String to shown in tree root.
	 */
	public void setProject(String filename){

		root.setUserObject(new ProjectNode(filename));
		defaultModel.nodeChanged(root);
	}

	public String getProjectPath(){
		if (root.getUserObject() instanceof ProjectNode)
			return ((ProjectNode)root.getUserObject()).getPath();
		else return root.getUserObject().toString();
	}

	class ProjectNode {
		String name;
		String path;
		ProjectNode(String filename){
			path = filename;
			name = filename.substring(filename.lastIndexOf(File.separator) + 1,
					filename.lastIndexOf("."));
		}

		public String getName(){
			return name;
		}

		public String getPath(){
			return path;
		}

		public String toString(){
			return name;
		}
	}
}

