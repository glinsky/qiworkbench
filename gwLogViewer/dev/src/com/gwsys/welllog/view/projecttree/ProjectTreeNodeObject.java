/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.projecttree;

import java.io.Serializable;

import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.CurveData;


public class ProjectTreeNodeObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String curveName;
    public boolean isDraged;
    public String curveHashmapKey;

    public ProjectTreeNodeObject(String curveName, boolean isDraged,String curveHashmapKey)
    {
        this.curveName = curveName;
        this.isDraged = isDraged;
        this.curveHashmapKey = curveHashmapKey; //use for getting status from hashmap
    }
    //must over this function
    public String toString() {
        return curveName;
    }
    
	public boolean isDraged() {
		return ((CurveData)DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(curveHashmapKey)).isDraged();
//		return this.isDraged;
	}

	public String getCurveName() {
		return curveName;
	}

	public void setCurveName(String curveName) {
		this.curveName = curveName;
	}
	public String getCurveHashmapKey() {
		return curveHashmapKey;
	}
}
