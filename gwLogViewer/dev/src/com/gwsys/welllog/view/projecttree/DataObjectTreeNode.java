/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view.projecttree;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Defines the TreeNode object to hold the source of data.
 * @author Louis
 *
 */
public class DataObjectTreeNode extends DefaultMutableTreeNode{

	private static final long serialVersionUID = 1L;

	public static final String WELLS = "Wells";

	public static final String WELL_LOG = "Well Logs";

	public static final String WELL_TOP = "Well Tops";

	public static final String WELL_SURVEY = "Wells";

	public static final String CHECK_SHOT = "Check Shots";

	private String sourceName;

	DataObjectTreeNode(Object userData, String sourceName){
		super(userData);
		this.sourceName = sourceName;
	}

	public String getSourceName() {
		return sourceName;
	}

}
