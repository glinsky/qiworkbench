/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.projecttree;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;

import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.welllog.actions.LoadBhpsuAction;
import com.gwsys.welllog.actions.LoadLasAction;
import com.gwsys.welllog.actions.LoadProjectAction;
import com.gwsys.welllog.actions.SaveProjectAction;
import com.gwsys.welllog.core.curve.ActiveDndSourceMonitor;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.GlobeLithology;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.LogWinUtility;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.WellInfo;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.dialogs.DataSettingDialog;
import com.gwsys.welllog.view.dialogs.ShowMapDialog;
import com.gwsys.welllog.view.track.AxisCurveContainerView;

/**
 * The event handler for project tree.
 * @author Team
 */
public class ProjectTreeController implements MouseListener,
		MouseMotionListener, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private int checkImage = 16;

	@SuppressWarnings("unchecked")
	public void mouseClicked(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON3){
			return;
		}
		ProjectTreeView ltv = (ProjectTreeView) e.getSource();
		int row = ltv.getRowForLocation(e.getX(), e.getY());
		if(row == -1)
			return;
		Rectangle nodeRect =ltv.getRowBounds(row);
		int pointX = e.getX();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) ltv.getLastSelectedPathComponent();
		if (node == null) {
			return;
		}

		if(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() == null){
			JOptionPane.showMessageDialog(DataBuffer.getJMainFrame().getFrame(),
					"Please create a InternalFrame!", "Message", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		String WellAndTrack = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTitle();
		//click on well name
		if(node.getLevel() == 2 && node.getParent().toString() == DataObjectTreeNode.WELLS){
			if((pointX-nodeRect.x)>this.checkImage){
				return;
			}
			WellAndTrack = WellAndTrack  + OperationClass.hashMapNameSeparator + node.toString();
			if(ltv.getAllWells().contains(WellAndTrack)){
				ltv.getAllWells().remove(WellAndTrack);
				hideSelectTrack(node.toString());
			}else{
				ltv.getAllWells().add(WellAndTrack);
				CurveNodeClick(ltv, (DefaultMutableTreeNode) node.getChildAt(0).getChildAt(0),node.toString(),false);

			}

			ltv.setFocusable(false);
			ltv.setFocusable(true);
			return;
		}

		if (node.getLevel() == 4 && node.getParent().toString() == DataObjectTreeNode.WELL_LOG) {
			if((pointX-nodeRect.x)>this.checkImage){
				return;
			}
			WellAndTrack = WellAndTrack  + OperationClass.hashMapNameSeparator + node.getParent().getParent().toString();
			if(!ltv.getAllWells().contains(WellAndTrack)){
				ltv.getAllWells().add(WellAndTrack);
			}
			CurveNodeClick(ltv, node,node.getParent().getParent().toString(),true);
			return;
		}

		if(node.toString() == DataObjectTreeNode.WELL_TOP){
			if((pointX-nodeRect.x)>this.checkImage){
				return;
			}
			WellAndTrack = WellAndTrack  + OperationClass.hashMapNameSeparator + node.toString();
			boolean allShow = true;
			if(ltv.getAllWellTops().contains(WellAndTrack)){
				ltv.getAllWellTops().remove(WellAndTrack);
				allShow = false;
			}else{
				ltv.getAllWellTops().add(WellAndTrack);
			}
			for (int i = 0; i < node.getChildCount(); i++) {
				DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) node.getChildAt(i);
				markerNodeClick(childNode,true,allShow);
			}
			ltv.setFocusable(false);
			ltv.setFocusable(true);
			ltv.repaint();
			return;
		}

		if(node.getLevel() == 2 && node.getParent().toString() == DataObjectTreeNode.WELL_TOP){
			if((pointX-nodeRect.x)>this.checkImage){
				return;
			}
			markerNodeClick(node,false,false);
			ltv.setFocusable(false);
			ltv.setFocusable(true);
			return;
		}
	}
//	June 30
	private void markerNodeClick(DefaultMutableTreeNode node,boolean allSelect, boolean showMarker) {
		ProjectTreeMarkerNodeObject markerObj = (ProjectTreeMarkerNodeObject) node.getUserObject();

		// set drag
		if (allSelect) {
			if (showMarker) {
				if (!markerObj.isDraged())
					markerObj.setDraged(true);
			} else {
				if (markerObj.isDraged())
					markerObj.setDraged(false);
			}
		} else {
			if (markerObj.isDraged())
				markerObj.setDraged(false);
			else
				markerObj.setDraged(true);
		}

		String markerName = markerObj.getMarkerName();
		ArrayList<String> wellNameList = markerObj.getWellNameList();
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		if (ifv == null) {
			return;
		}

		String allNotExistsWell = "";
		boolean noWellAtAll = true;
		for (int ii = 0; ii < wellNameList.size(); ii++) {
			AxisCurveContainerView track = null;
			String wellName = wellNameList.get(ii);

			// find track from well name
			ArrayList list = ifv.getTrackContainer().getAxisCurveContainerList();
			boolean find = false;
			for (int i = 0; i < list.size(); i++) {
				AxisCurveContainerView tmpTrack = (AxisCurveContainerView) list.get(i);
				if (tmpTrack.getWellName().equals(wellName)) {
					if (track == null){
						track = tmpTrack;
					    noWellAtAll = false;
					}
					find = true;
					break;
				}
			}

			// not find
			if (find == false) {
				if (!allNotExistsWell.equals(""))
					allNotExistsWell = allNotExistsWell + ",";
				allNotExistsWell = allNotExistsWell + wellName;
				continue;
			}

			// find marker depth
			ArrayList<MarkerInfo> mList = DataBuffer.getJMainFrame().getMainBuffer().getMarkerList();
			float depthValue = 1.0f;
			for (int iMar = 0; iMar < mList.size(); iMar++) {
				MarkerInfo markerInfo = mList.get(iMar);
				if (markerInfo.getMarkerName().equals(markerName)&& markerInfo.getWellName().equals(wellName)) {
					depthValue = (float) markerInfo.getDepthValue();
					break;
				}
			}

			// find marker under track
			CurveMarker curveMarker = null;
			for (int i = 0; i < track.getCurveView().getMarkerLayer().getMarkerList().size(); i++) {
				curveMarker = (CurveMarker) (track.getCurveView().getMarkerLayer().getMarkerList().get(i));
				if (curveMarker.getMarkerName().equals(markerName)) {
					break;
				}
				curveMarker = null;
			}

			if(markerObj.isDraged() == true){
				if (curveMarker == null) {
					CurveMarker cm = new CurveMarker(markerName,depthValue, Color.BLUE);
					cm.setVisible(true);
					cm.setXLenth(track.getCurveView().getWidth());
					cm.setYHeight(track.getCurveView());
					track.getCurveView().getMarkerLayer().addMarkerToList(cm);
				} else {
					curveMarker.setVisible(true);
//					break;
				}
			}else{
				if(curveMarker != null)
					curveMarker.setVisible(false);
			}
		}

		// connect marker
		ViewCoreOperator.connectMarker(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView());
		ifv.getTrackContainer().repaint();
		/*if (!allNotExistsWell.equals("") && markerObj.isDraged() == true) {
			JOptionPane.showMessageDialog(DataBuffer.getJMainFrame(),
					"Can not find the track which be destined for well ("
							+ allNotExistsWell + ") in current well log!",
					"Message", JOptionPane.INFORMATION_MESSAGE);
		}*/
		if(noWellAtAll == true){
			markerObj.setDraged(false);
		}
	}

	private void hideSelectTrack(String wellName){
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		if (ifv == null) {
			return;
		}
		ArrayList list = ifv.getTrackContainer().getAxisCurveContainerList();
		for (int i = 0; i < list.size(); i++) {
			AxisCurveContainerView track = (AxisCurveContainerView) list.get(i);
			if(track.getWellName().equals( wellName)){
				track.setVisible(false);
				break;
			}
		}
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().revalidate();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().repaint() ;
	}

	// by zzl 2006-3-16
	public void CurveNodeClick(ProjectTreeView ltv,DefaultMutableTreeNode node,String wellName, boolean editCurve) {
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		if (ifv == null) {
			return;
		}

		ArrayList list = ifv.getTrackContainer().getAxisCurveContainerList();
		String targetTrackID = null;
		String targetTrackName = null; //use this null to decide if creating new
		// find which track curve should belong to
		if (list.isEmpty()) {
			targetTrackID = LogWinUtility.generateNewLogName();
			targetTrackName = wellName;//targetTrackID + ":" + wellName;
			ViewCoreOperator.createTrack(targetTrackID,targetTrackName, wellName);
		} else {
			// check if target track exists
			AxisCurveContainerView track = null;
			for (int i = 0; i < list.size(); i++) {
				track = (AxisCurveContainerView) list.get(i);
				if(track.getWellName().equals(wellName)){
					targetTrackID = track.getTrackID();
					targetTrackName = track.getTrackName();
					break;
				}
				track = null;
			}
			// if target track doesn't exist
			if (track == null) {
				// try to use a empty track
				// first check if the current track is a empty track, if so,
				// then put curve into it
				track = ifv.getTrackContainer().getSelectedAxisCurveContainer();
				if (track != null  && !track.isHasWell()) { //June 29
				    targetTrackID = track.getTrackID();
				    targetTrackName = wellName;//targetTrackID + ":" + wellName;
				    DataBuffer.getJMainFrame().getMainBuffer().getViewTree().renameTrack(ifv.getTitle(), track.getWellName(),targetTrackName,wellName);
					track.setTrackName(targetTrackName);
					track.setWellName(wellName );
					ltv.updateWellTopNodeTrackName(targetTrackID,wellName);
				}
				// current track isn't empty
				else {
					// try to find the first empty track
					for (int i = 0; i < list.size(); i++) {
						track = (AxisCurveContainerView) list.get(i);
						if (!track.isHasWell()) {
							targetTrackID = track.getTrackID();
							targetTrackName = wellName;//targetTrackID + ":" + wellName;
							DataBuffer.getJMainFrame().getMainBuffer().getViewTree().renameTrack(ifv.getTitle(), track.getWellName(),targetTrackName,wellName);
							track.setTrackName(targetTrackName);
							track.setWellName(wellName );
							ltv.updateWellTopNodeTrackName(track.getTrackID(),wellName);
							break;
						}
					}
					// there is no empty track, then create a new track for
					// the curve
					if (targetTrackName == null) {
						targetTrackID = LogWinUtility.generateNewLogName();
						targetTrackName = wellName;//targetTrackID + ":" + wellName;
						ViewCoreOperator.createTrack(targetTrackID,targetTrackName, wellName);
					}
				}
			}

		}

		list = ifv.getTrackContainer().getAxisCurveContainerList();
		AxisCurveContainerView currentTrack;
		for (int i = 0; i < list.size(); i++) {
			currentTrack = (AxisCurveContainerView) list.get(i);
			if(currentTrack.getTrackID().equals(targetTrackID)){
				currentTrack.setVisible(true);
				break;
			}
		}

		if(editCurve){
			ProjectTreeCurveNodeObject selectNode = (ProjectTreeCurveNodeObject)node.getUserObject();
			CurveData cd = (CurveData) DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(selectNode.getCurveHashmapKey());
			cd.setDraged(!cd.isDraged());
			if (!cd.isDraged()) {
				ViewCoreOperator.deleteCurve(cd.getHashCurveName(),wellName);
				DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().revalidate();
				DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().repaint() ;
			}
			else {
				ViewCoreOperator.tryPutCurveToTrack(cd.getHashCurveName(),wellName);
			}
		}
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ActiveDndSourceMonitor
				.setActiveDndSource(ActiveDndSourceMonitor.PROJECT_TREE);
		ProjectTreeView ptree = (ProjectTreeView) e.getSource();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)
				ptree.getLastSelectedPathComponent();

		ptree.resetNodeClicked("");// clear transfer

		if (node != null && node.getLevel() == 4) {
			ptree.resetNodeClicked(ptree.changeNodeValueToHashkey(node));
			ptree.setNodeDraged(node);
		}else if (e.getButton() == MouseEvent.BUTTON3){
			JPopupMenu popup = new JPopupMenu();
			if(node != null && node.getLevel() == 2){
				createTwoMenus(popup, node, ptree);
			}else if(node != null && node.getLevel() == 1){
				createLithologyMenu(popup, node);
			}

			// aded by Alex. July 6, 2006
			JMenuItem addLasItem = new JMenuItem("Add LAS file");
			popup.add(addLasItem);
			JMenuItem addSuItem = new JMenuItem("Add SU file");
			popup.add(addSuItem);

			popup.addSeparator();
			JMenuItem openProjectItem = new JMenuItem("Open Project");
			popup.add(openProjectItem);
			JMenuItem saveProjectItem = new JMenuItem("Save Project");
			popup.add(saveProjectItem);
			popup.addSeparator();
			JMenuItem showMapItem = new JMenuItem("Show Map");
			popup.add(showMapItem);
			showMapItem.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					ShowMapDialog showMapDialog = new ShowMapDialog();
					showMapDialog.setVisible(true);
				}
			});

			// lines Ends

			popup.show(ptree, e.getX(), e.getY());

			// added by Alex. July 6, 2006
			addLasItem.addActionListener(new LoadLasAction(DataBuffer.getJMainFrame()));
			addSuItem.addActionListener(new LoadBhpsuAction(DataBuffer.getJMainFrame()));
			openProjectItem.addActionListener(new LoadProjectAction(DataBuffer.getJMainFrame()));
			saveProjectItem.addActionListener(new SaveProjectAction(DataBuffer.getJMainFrame()));
		}
	}

	private void createLithologyMenu(JPopupMenu popup,
			final DefaultMutableTreeNode node){
		JMenuItem settingItem = new JMenuItem("Lithology");
		popup.add(settingItem);
		popup.addSeparator();
		settingItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				GlobeLithology.getInstance().showLithologyDialog();
			}
		});
	}

	private void createTwoMenus(JPopupMenu popup,
			final DefaultMutableTreeNode node, final ProjectTreeView ltv){
		JMenuItem settingItem = new JMenuItem("Settings...");
		popup.add(settingItem);
		popup.addSeparator();
		settingItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (node.getUserObject() instanceof ProjectTreeMarkerNodeObject){
					WellTopsProperty dialog=new WellTopsProperty(
							(ProjectTreeMarkerNodeObject)
							node.getUserObject());
					dialog.setLocationRelativeTo(ltv);
					dialog.setVisible(true);
				}else if (node instanceof DataObjectTreeNode){
					String sourceName = ((DataObjectTreeNode)node).getSourceName();
					DataSettingDialog dialog=new DataSettingDialog(sourceName, DataBuffer.getJMainFrame());
					dialog.setLocationRelativeTo(ltv);
					dialog.setVisible(true);
				}
			}
		});

		JMenuItem deleteItem = new JMenuItem("Delete");
		popup.add(deleteItem);
		popup.addSeparator();

		deleteItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(node.getParent().toString().equals("Wells")){
					String wellName = (String)node.getUserObject();
					ArrayList internalFrameViewList = DataBuffer.getJMainFrame().getSonJSplitPane().getDesktopPaneView().getInternalFrameViewList();
					for(int j=0 ; j<internalFrameViewList.size() ; j++){
						ViewCoreOperator.deleteTrack(wellName,(InternalFrameView)internalFrameViewList.get(j));
					}
					ltv.removeWellLog(node);
					ArrayList wellList = DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList();
					for(int j=0 ; j<wellList.size() ; j++){
						if(((WellInfo)wellList.get(j)).getWellName().equals(wellName)){
							wellList.remove(j);
						}
					}
				}else if(node.getParent().toString().equals("Well Tops")){
					ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject)node.getUserObject();
					String markerName = markerNode.getMarkerName();
					ltv.removeWellLog(node);
					ArrayList markerList = DataBuffer.getJMainFrame().getMainBuffer().getMarkerList();
					for(int j=0 ; j<markerList.size() ; j++){
						if(((MarkerInfo)markerList.get(j)).getMarkerName().equals(markerName)){
							markerList.remove(j);
						}
					}
					ViewCoreOperator.removeAllMarkerByName(markerName);
					DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().repaint();

				}else if(node.getParent().toString().equals("Check Shots")){

				}
			}

		});
	}

	public void mouseReleased(MouseEvent e) {

	}

	public void mouseDragged(MouseEvent e) {
		// Drag out
		if (((ProjectTreeView) e.getSource()).getTransferredObject().equals("")) {
			return;
		}

		JComponent c = (JComponent) e.getSource();
		TransferHandler handler = c.getTransferHandler();
		handler.exportAsDrag(c, e, TransferHandler.COPY);
	}

	public void mouseMoved(MouseEvent e) {

	}
}
