/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.projecttree;

import java.awt.Component;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.OperationClass;





public class ProjectTreeRenderer extends DefaultTreeCellRenderer {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private ProjectTreeView projectTreeView = null;
	private Icon selected;
	private Icon unSelected;
	private ImageIcon iconSelected = new ImageIcon();
	private ImageIcon iconUnSelected = new ImageIcon();

	public ProjectTreeRenderer(ProjectTreeView projectTreeView)
	{
		this.projectTreeView = projectTreeView;
		this.setNodeIcon();
	}

	public Component getTreeCellRendererComponent(
            JTree tree,
            Object value,
            boolean sel,
            boolean expanded,
            boolean leaf,
            int row,
            boolean hasFocus)
	{
		super.getTreeCellRendererComponent(
		            tree, value, sel,
		            expanded, leaf, row,
		            hasFocus);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
		if(leaf){
			if(node.getLevel()>1){
				if(node.getParent().toString() == "Well Logs"){
					if(curveNodeStatus(value))
					{
						setIcon(this.selected);
					}
					else
					{
						setIcon(this.unSelected);
					}
				}
				if(node.getParent().toString() == "Well Tops"){
					if(markerNodeStatus(value))
					{
						setIcon(this.selected);
					}
					else
					{
						setIcon(this.unSelected);
					}
				}
				if(node.getParent().toString() == "Check Shots"){
					if(checkShotNodeStatus(value))
					{
						setIcon(this.selected);
					}
					else
					{
						setIcon(this.unSelected);
					}
				}
				return this;
			}
		}
		String wellLog = "";
		if(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() != null){
			wellLog = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTitle();
		}
		if (node.getLevel() ==2 && node.getParent().toString()=="Wells" ) {
			wellLog = wellLog + OperationClass.hashMapNameSeparator + node.toString();
			ArrayList<String> wlist = this.projectTreeView.getAllWells();
			if(wlist.contains(wellLog)){
				setIcon(this.selected);
			}else{
				setIcon(this.unSelected);
			}
			return this;
		}
		if(node.toString() == "Well Tops" && !node.isLeaf()){
			wellLog = wellLog + OperationClass.hashMapNameSeparator + node.toString();
			if(this.projectTreeView.getAllWellTops().contains(wellLog)){
				setIcon(this.selected);
			}else{
				setIcon(this.unSelected);
			}
		}

		return this;
	}
	/**
	 *
	 * @param value
	 * @return node text
	 */
	public String getString(Object value)
	{
		try
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
			return ((ProjectTreeCurveNodeObject)node.getUserObject()).getCurveName();
		}
		catch(Exception e1)
		{
			return "";
		}
	}
	/**
	 *
	 * @param value
	 * @return is node seleted
	 */
	private boolean curveNodeStatus(Object value)
	{
		try
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
			ProjectTreeCurveNodeObject selectNode = (ProjectTreeCurveNodeObject)node.getUserObject();
			CurveData cd = (CurveData) DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(selectNode.getCurveHashmapKey());
			return cd.isDraged();
		}
		catch(Exception e1)
		{
			return false;
		}
	}

	private boolean markerNodeStatus(Object value)
	{
		try
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
			return ((ProjectTreeMarkerNodeObject)node.getUserObject()).isDraged();
		}
		catch(Exception e1)
		{
			return false;
		}
	}

	private boolean checkShotNodeStatus(Object value)
	{
		try
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
			return ((ProjectTreeMarkerNodeObject)node.getUserObject()).isDraged();
		}
		catch(Exception e1)
		{
			return false;
		}
	}

	private void setNodeIcon()
	{
		Image imageSelected = IconResource.getInstance().getImage("selected.gif");
		Image imageUnSelected = IconResource.getInstance().getImage("noselect.gif");

		this.iconSelected.setImage(imageSelected);
		this.iconUnSelected.setImage(imageUnSelected);

		this.selected = this.iconSelected;
		this.unSelected = this.iconUnSelected;
	}
}
