/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view;

import java.util.ArrayList;

import javax.swing.JDesktopPane;

import com.gwsys.welllog.view.track.AxisCurveContainerView;

/**
 * Extends the JDesktopPane to control the list of frames.
 * @author Team
 *
 */
public class DesktopPaneView extends JDesktopPane {

	private static final long serialVersionUID = 1L;
	ArrayList internalFrameViewList = new ArrayList();

	/**
	 * Constructor.
	 */
	public DesktopPaneView(){
		this.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
	}

    /**
     * @author luming
     *
     * @return the selected interFrameview
     */
    public InternalFrameView getSelectedInternalFrameView()
    {
    	for(int i = 0 ; i<internalFrameViewList.size();i++)
    	{
    		if(((InternalFrameView)internalFrameViewList.get(i)).isSelected())
    		{
    			return (InternalFrameView)internalFrameViewList.get(i);
    		}
    		}
    	return null;
    }

	public ArrayList getInternalFrameViewList() {
		return internalFrameViewList;
	}

	public InternalFrameView findInternalFrameViewByTitle(String title) {
		ArrayList internalFrames = this.getInternalFrameViewList();
		for(int i = 0; i < internalFrames.size(); i++) {
			InternalFrameView internalFrame = (InternalFrameView)internalFrames.get(i);
			if(internalFrame.getTitle().equals(title)) {
				return internalFrame;
			}
		}
		return null;
	}

	public InternalFrameView findTrackBelongToWhichFrame(String trackName) {
		ArrayList internalFrames = this.getInternalFrameViewList();
		for(int i = 0; i < internalFrames.size(); i++) {
			InternalFrameView internalFrame = (InternalFrameView)internalFrames.get(i);
			ArrayList tracks = internalFrame.getTrackContainer().getAxisCurveContainerList();
			for(int j = 0; j < tracks.size(); j++) {
				AxisCurveContainerView track = (AxisCurveContainerView)tracks.get(j);
				if(track.getTrackName() != null && track.getTrackName().equals(trackName)) {
					return internalFrame;
				}
			}
		}
		return null;
	}

	/**
	 * Remove all frames and clear list.
	 *
	 */
	public void clear(){
		internalFrameViewList.clear();
		removeAll();
		repaint();
	}
}
