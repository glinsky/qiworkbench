/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.fill;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.PanelOrder;

/**
 * Defines the property for curve fill.
 * @author Team
 *
 */
public class CurveFillBeside {
	private Shape myFill;
	private Color fillColor;
	private String leftOrRight=ConstantVariableList.FILL_LEFT;
	private CurvePanel cp;
	private boolean fill = true;
	private boolean selected = false;
	
	// Fill in a group
	private CurveGroup group;
	private boolean isIntoGroup = false;

	/**
	 * Creates fill shape.
	 * @param cv CurveView.
	 */
	public void constructFill(CurveView cv){
		PanelOrder order = cv.getPanelOrder();
		int widthForLeft = order.getPanelLeft(cp);
		int widthForRight = order.getPanelRight(cp);
		constructFill(widthForLeft, widthForRight);
	}
	
	/**
	 * Creates a fill shape into a curveGroup 
	 * @param cv CurveView
	 * @param group curveGroup
	 * @author Alex
	 */
	public void constructFill(CurveView cv, CurveGroup group)	{
		if (group == null)
			return;
		
		this.group = group;
		PanelOrder order = cv.getPanelOrder();
		int widthForLeft = order.getPanelLeft(group);
		int widthForRight = order.getPanelRight(group);
		constructFill(widthForLeft, widthForRight);
	}

	/**
	 *
	 * @param widthForLeft
	 * @param widthForRight
	 */
	public void constructFill(int widthForLeft, int widthForRight){

		GeneralPath path = new GeneralPath();
		myFill = path;
		if (cp == null ) return;
		if(this.getLeftOrRight().equals(ConstantVariableList.FILL_LEFT))
		{
			path.append(cp.getTranslatedCurve(),true);
			Rectangle reg=cp.getTranslatedCurve().getBounds();
			int minY=(int) reg.getMinY();
			int maxY=(int) reg.getMaxY();
			path.lineTo(widthForLeft,maxY);
			path.lineTo(widthForLeft,minY);

		}
		else if(this.getLeftOrRight().equals(ConstantVariableList.FILL_RIGHT))
		{
			path.append(cp.getTranslatedCurve(),true);
			Rectangle reg=cp.getTranslatedCurve().getBounds();
			int minY=(int) reg.getMinY();
			int maxY=(int) reg.getMaxY();
			path.lineTo(widthForRight,maxY);
			path.lineTo(widthForRight,minY);
		}
		else if(this.getLeftOrRight().equals(ConstantVariableList.FILL_ALL))
		{
			path.append(cp.getTranslatedCurve(),true);
			Rectangle reg=cp.getTranslatedCurve().getBounds();
			int minY=(int) reg.getMinY();
			int maxY=(int) reg.getMaxY();
			path.lineTo(widthForLeft,maxY);
			path.lineTo(widthForLeft,minY);

			path.append(cp.getTranslatedCurve(),true);
			reg=cp.getTranslatedCurve().getBounds();
			minY=(int) reg.getMinY();
			maxY=(int) reg.getMaxY();
			path.lineTo(widthForRight,maxY);
			path.lineTo(widthForRight,minY);
			path.lineTo(widthForRight,maxY);
			path.lineTo(widthForRight,minY);
		}
		myFill = path;
	}

	public Element saveXML(Document doc, Integer id){
		Element xmlCurveFillBeside = doc.createElement("CurveFillBeside");
		if (this.getCp()!=null){
			xmlCurveFillBeside.setAttribute("id",Integer.toString(id));
			xmlCurveFillBeside.setAttribute("curve",this.getCp().getCd().getHashCurveName());
			xmlCurveFillBeside.setAttribute("orientation",this.getLeftOrRight());
			xmlCurveFillBeside.setAttribute("color",Integer.toString(this.getFillColor().getRGB()));
			
			if (this.isFillIntoGroup())	{
				xmlCurveFillBeside.setAttribute("fillIntoGroup", Boolean.toString(this.isFillIntoGroup()));
				xmlCurveFillBeside.setAttribute("group", this.getCurveGroup().getName());
			}
		}
		return xmlCurveFillBeside;
	}

	public void loadXML(Element xmlCurveFillBeside, CurveView cv){
		CurvePanel cp = null;
		
		if (!Boolean.parseBoolean(xmlCurveFillBeside.getAttribute("fillIntoGroup")))	{
			for(int i =0 ; i < cv.getCurveLayer().getCurveList().size() ; i++){
				cp = (CurvePanel) cv.getCurveLayer().getCurveList().get(i);
				if (cp.getCd().getHashCurveName().equals(xmlCurveFillBeside.getAttribute("curve"))){
					break;
				}
			}
			if(cp == null){
				return;
			}
			this.setCp(cp);
			this.setLeftOrRight((String) xmlCurveFillBeside.getAttribute("orientation"));
			this.setFillColor(new Color(Integer.parseInt(xmlCurveFillBeside.getAttribute("color"))));
			this.constructFill(cv);
		}
		else	{
			String curveHashkey = xmlCurveFillBeside.getAttribute("curve");
			String groupName = xmlCurveFillBeside.getAttribute("group");
			CurveGroup cg = this.getThisCurveGroup(cv, groupName);
			cp = this.getThisCurvePanel(cg, curveHashkey);
			
			this.setCp(cp);
			this.setFillIntoGroup(true);
			this.setLeftOrRight((String) xmlCurveFillBeside.getAttribute("orientation"));
			this.setFillColor(new Color(Integer.parseInt(xmlCurveFillBeside.getAttribute("color"))));		
			this.constructFill(cv, cg);
		}
			
	}

	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

	public String getLeftOrRight() {
		return leftOrRight;
	}

	public void setLeftOrRight(String leftOrRight) {
		this.leftOrRight = leftOrRight;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Shape getMyFill() {
		return myFill;
	}

	public CurvePanel getCp() {
		return cp;
	}

	public void setCp(CurvePanel cp) {
		this.cp = cp;
	}

	public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}
	
	/**
	 * get curve group
	 * @return CurveGroup
	 * @author Alex
	 */
	public CurveGroup getCurveGroup()	{
		return this.group;
	}
	
	/**
	 * check is this fill is applied in a Group
	 * @return true or false
	 * @author Alex
	 */
	public boolean isFillIntoGroup()	{
		return this.isIntoGroup;
	}
	
	/**
	 * set fill must be applied in a Group
	 * @param value true or false
	 * @author Alex
	 */
	public void setFillIntoGroup(boolean value)	{
		this.isIntoGroup = value;
	}
	
	private CurveGroup getThisCurveGroup(CurveView cv, String groupName)	{		 
		for (int i = 0; i < cv.getCurveGroups().size(); i++) {
			if (((CurveGroup) cv.getCurveGroups().get(i)).getName().equals(groupName))
				return (CurveGroup) cv.getCurveGroups().get(i);
		}
		return null;
	}
	
	private CurvePanel getThisCurvePanel(CurveGroup cg, String curveHashkey)	{
		for (int i = 0; i < cg.getCurveList().size(); i++)	{
			CurvePanel cp = (CurvePanel) cg.getCurveList().get(i);
			
			if (cp.getCd().getHashCurveName().equals(curveHashkey))
				return cp;
		}
		
		return null;
	}	
}
