/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view.fill;

import java.awt.Shape;
import java.awt.geom.GeneralPath;

import com.gwsys.welllog.view.curve.LithologyObject;
import com.gwsys.welllog.view.curve.LithologyPanel;

/**
 *
 * @author Team
 *
 */
public class PanelLithology {
	private Shape myFill;
	private int whichtexture = -1;
	private LithologyObject lithologyObject;
	private boolean selected = false;

	public void constructFill(LithologyPanel lithologyPanel,int left,
			float rateWidth, float rateHeight) 	{
		lithologyObject.setStart();
		lithologyObject.setEnd();
		GeneralPath path = new GeneralPath();
		//run c1
		path.moveTo(left,(int)(lithologyObject.getYStart()*rateHeight));
		path.lineTo(left+(int)(lithologyPanel.getBesideTypeWidth()*rateWidth),
				(int)(lithologyObject.getYStart()*rateHeight));
		path.lineTo(left+(int)(lithologyPanel.getBesideTypeWidth()*rateWidth),
				(int)(lithologyObject.getYEnd()*rateHeight));
		path.lineTo(left,(int)(lithologyObject.getYEnd()*rateHeight));
		myFill = path;
	}

	public LithologyObject getLithologyObject() {
		return lithologyObject;
	}

	public void setLithologyObject(LithologyObject lithologyObject) {
		this.lithologyObject = lithologyObject;
	}

	public Shape getMyFill() {
		return myFill;
	}
	public void setMyFill(Shape myFill) {
		this.myFill = myFill;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public int getWhichtexture() {
		return whichtexture;
	}
	public void setWhichtexture(int whichtexture) {
		this.whichtexture = whichtexture;
	}
}
