/**
 * 
 */
package com.gwsys.welllog.view.fill;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * a class to create Shapes 
 * between 2 curves in a group
 * 
 * @author Alex
 *
 */
public class FillShapeBuilder {
	private int start = 0;
	
	private ArrayList<Point2D> leftList = new ArrayList<Point2D>();
	private ArrayList<Point2D> rightList = new ArrayList<Point2D>();
	
	private ArrayList<Shape> shapeList = new ArrayList<Shape>();
	private GeneralPath myPath;
	
	/**
	 * default constructor of this class
	 * 
	 * @param leftPath Shape from left curve
	 * @param rightPath Shape from right curve
	 */
	public FillShapeBuilder(Shape leftPath, Shape rightPath)	{
		ArrayList<Point2D> tmpLeft = new ArrayList<Point2D>();
		ArrayList<Point2D> tmpRight = new ArrayList<Point2D>();
		
		getPointsFromThisPath(leftPath, tmpLeft);
		getPointsFromThisPath(rightPath, tmpRight);
		
		int leftY = (int)tmpLeft.get(0).getY();
		int rightY = (int)tmpRight.get(0).getY();
		
		if (leftY <= rightY)
			filterData(tmpLeft, tmpRight, true);
		else if (leftY > rightY)
			filterData(tmpRight, tmpLeft, false);
	}
	
	/**
	 * look for matching points in both curves
	 * @param master domain curve points
	 * @param slave range curve points
	 * @param isLeft set which curve points is about which side
	 */
	private void filterData(ArrayList<Point2D> master, ArrayList<Point2D> slave, boolean isLeft) {
		int offset = 0, index = 0;
		
		offset = getOffset(master, (float) slave.get(0).getY());
		
		//while ((index < master.size() && index < slave.size()) && (index+offset < master.size() && index+offset<slave.size()))	{
		
		while (index < master.size() && index < slave.size() && index+offset < master.size())	{
			if (isLeft)	{
				leftList.add(master.get(index+offset));
				rightList.add(slave.get(index));
			}
			else	{
				leftList.add(slave.get(index));
				rightList.add(master.get(index+offset));
			}
			
			index++;
		}
	}
	
	/**
	 * seek valueToCompare in that ArrayList and return the index
	 * @param list list where to look for
	 * @param valueToCompare value to match
	 * @return offset
	 */
	private int getOffset(ArrayList<Point2D> list, float valueToCompare)	{
		for (int index = 0; index < list.size(); index++)	{
			if (match((float) list.get(index).getY(), valueToCompare))	{
				return index;
			}
		}
		
		return 0;
	}

	/**
	 * gets x/y points from given Path and fill it in an ArrayList
	 */
	private void getPointsFromThisPath(Shape myPath, ArrayList<Point2D> myList)	{
		PathIterator iterator = myPath.getPathIterator(new AffineTransform());		
		float[] temp = new float[6];
		
		while(!iterator.isDone())	{
			iterator.currentSegment(temp);
			Point2D point = new Point2D.Float(temp[0], temp[1]);
			myList.add(point);
			iterator.next();
		}
	}
	
	/**
	 * compare those values
	 * @param f
	 * @param s
	 * @return true or false
	 */
	private boolean match(float f, float s)	{		
		if ((int) f == (int) s)
			return true;
		
		return false;
	}
	
	/**
	 * build all shapes that they can be founded
	 */
	public void buildShape()	{		
		boolean flag = false;
		int x, y, rightX, counter = 0;
		
		start = 0;
		myPath = new GeneralPath();
		
		for (int i = 0; i < leftList.size(); i++)	{
			x = (int) leftList.get(i).getX();
			y = (int) leftList.get(i).getY();
			rightX = (int) rightList.get(i).getX();
			
			if (x < rightX)	{
				appendToPath(x, y, i);
				counter++;
				flag = true;
				
				if ((i == leftList.size()-1) && (counter >= 4))	{
					completeTheFill(i, start);
				}
			}
			else if (flag)	{
				if (counter >= 4)
					completeTheFill(i, start);
				myPath = new GeneralPath();
				start = i + 1;
				flag = false;
			}
			else if (!flag)
				start = i + 1;			
		}
	}
	
	private void appendToPath(int x, int y, int index)	{
		if (index == this.start)
			myPath.moveTo(x, y);
		else
			myPath.lineTo(x, y);
	}
	
	/**
	 * @param i
	 * @param start
	 */
	private void completeTheFill(int begin, int end) {
		for (int i = begin; i >= end; i--)	{
			int x = (int) rightList.get(i).getX();
			int y = (int) rightList.get(i).getY();
			
			appendToPath(x, y, -1);
		}
		
		myPath.closePath();
		shapeList.add(myPath);
	}
	
	/**
	 * get  a list of all shapes built
	 * @return shape list
	 */
	public ArrayList<Shape> getShapeList()	{
		return this.shapeList;
	}
}
