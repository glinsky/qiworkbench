/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.fill;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.MarkerLayer;

public class MarkerFill {
	
	private Shape myFill;
	private Color fillColor;
	private CurveMarker cm1,cm2;
	
	private boolean selected = false;	
	
	public void constructFill(CurveView cv) 
	{
		cm1.setYHeight(cv);
		cm2.setYHeight(cv);
		GeneralPath path = new GeneralPath();
		//run c1
		path.moveTo(0,cm1.getYHeight());
		path.lineTo(cm1.getXLenth(),cm1.getYHeight());
		path.lineTo(cm2.getXLenth(),cm2.getYHeight());
		path.lineTo(0,cm2.getYHeight());
		myFill = path;
	}

	public Element saveXML(Document doc, Integer id){
		Element xmlMarkerFill = doc.createElement("MarkerFill");
		xmlMarkerFill.setAttribute("id",Integer.toString(id));
		xmlMarkerFill.setAttribute("fillColor",Integer.toString(this.fillColor.getRGB()));
		xmlMarkerFill.setAttribute("CurveMarker1",this.cm1.getMarkerName());
		xmlMarkerFill.setAttribute("CurveMarker2",this.cm2.getMarkerName());
		
		return xmlMarkerFill;
	}
	
	public void loadXML(Element xmlMarkerFill, MarkerLayer markerLayer){
		this.setFillColor(new Color(Integer.parseInt(xmlMarkerFill.getAttribute("fillColor"))));
		this.cm1 = null;
		this.cm2 = null;
		for(int i = 0 ; i < markerLayer.getMarkerList().size() ; i++){
			CurveMarker curveMarker = (CurveMarker)markerLayer.getMarkerList().get(i);
			if(curveMarker.getMarkerName().equals(xmlMarkerFill.getAttribute("CurveMarker1"))){
				this.cm1 = curveMarker;
			}
			if(curveMarker.getMarkerName().equals(xmlMarkerFill.getAttribute("CurveMarker2"))){
				this.cm2 = curveMarker;
			}
			if(this.cm1 != null && this.cm2 != null){
				constructFill(markerLayer.getParentAxisCurveContainerView().getCurveView());
				return;
			}
		}
	}
	
	public void setCurveMarker(CurveMarker cm1,CurveMarker cm2)
	{
		this.cm1 = cm1;
		this.cm2 = cm2;
	}
	
	public CurveMarker getCm1() {
		return cm1;
	}

	public CurveMarker getCm2() {
		return cm2;
	}

	

	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

	public Shape getMyFill() {
		return myFill;
	}

	public void setMyFill(Shape myFill) {
		this.myFill = myFill;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
