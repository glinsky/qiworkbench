/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.fill;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.PanelOrder;

/**
 * Onject fills lithology in beside track.
 * @author Team
 *
 */
public class CurveLithologyBeside {

	private Shape myFill;
	private String leftOrRight; // "LEFT" or "RIGHT"
	private CurvePanel cp;
	private int whichtexture;

	private boolean selected = false;
	private boolean fill = true;

	/**
	 * Creates shape of fill
	 * @param cv CurveView
	 */
	public void constructFill(CurveView cv) {
		PanelOrder order = cv.getPanelOrder();
		int widthForLeft = order.getPanelLeft(cp);
		int widthForRight = order.getPanelRight(cp);
		constructFill(widthForLeft, widthForRight);
	}
	/**
	 * Creates shape of fill
	 * @param cv CurveView
	 * @param rateWidth Ratio
	 * @param rateHeight Ratio
	 */
	public void constructFill(int widthForLeft, int widthForRight) {
		GeneralPath path = new GeneralPath();

		if(this.getLeftOrRight().equals(ConstantVariableList.FILL_LEFT))
		{
			path.append(cp.getTranslatedCurve(),true);
			Rectangle reg=cp.getTranslatedCurve().getBounds();
			int minY=(int) reg.getMinY();
			int maxY=(int) reg.getMaxY();
			path.lineTo(widthForLeft,maxY);
			path.lineTo(widthForLeft,minY);
		}
		else if(this.getLeftOrRight().equals(ConstantVariableList.FILL_RIGHT))
		{
			path.append(cp.getTranslatedCurve(),true);
			Rectangle reg=cp.getTranslatedCurve().getBounds();
			int minY=(int) reg.getMinY();
			int maxY=(int) reg.getMaxY();
			path.lineTo(widthForRight,maxY);
			path.lineTo(widthForRight,minY);
		}
		else if(this.getLeftOrRight().equals(ConstantVariableList.FILL_ALL))
		{
			path.append(cp.getTranslatedCurve(),true);
			Rectangle reg=cp.getTranslatedCurve().getBounds();
			int minY=(int) reg.getMinY();
			int maxY=(int) reg.getMaxY();
			path.lineTo(widthForLeft,maxY);
			path.lineTo(widthForLeft,minY);

			path.append(cp.getTranslatedCurve(),true);
			reg=cp.getTranslatedCurve().getBounds();
			minY=(int) reg.getMinY();
			maxY=(int) reg.getMaxY();
			path.lineTo(widthForRight,maxY);
			path.lineTo(widthForRight,minY);
		}

		myFill = path;
	}

	public Element saveXML(Document doc, Integer id){
		Element xmlCurveLithologyBeside = doc.createElement("CurveLithologyBeside");
		xmlCurveLithologyBeside.setAttribute("id",Integer.toString(id));
		xmlCurveLithologyBeside.setAttribute("curve",this.getCp().getCd().getHashCurveName());
		xmlCurveLithologyBeside.setAttribute("orientation",this.getLeftOrRight());
		xmlCurveLithologyBeside.setAttribute("texture",Integer.toString(this.getWhichtexture()));

		return xmlCurveLithologyBeside;
	}

	public void loadXML(Element xmlCurveLithologyBeside, CurveView cv){
		CurvePanel cp = null;
		for(int i =0 ; i < cv.getCurveLayer().getCurveList().size() ; i++){
			cp = (CurvePanel) cv.getCurveLayer().getCurveList().get(i);
			if (cp.getCd().getHashCurveName().equals(xmlCurveLithologyBeside.getAttribute("curve"))){
				break;
			}
		}
		if(cp == null){
			return;
		}
		this.setCp(cp);
		this.setLeftOrRight((String) xmlCurveLithologyBeside.getAttribute("orientation"));
		this.setWhichtexture(Integer.parseInt(xmlCurveLithologyBeside.getAttribute("texture")));

		PanelOrder order = cv.getPanelOrder();
		int widthForLeft = order.getPanelLeft(cp);
		int widthForRight = order.getPanelRight(cp);
		this.constructFill(widthForLeft, widthForRight);
	}

	public String getLeftOrRight() {
		return leftOrRight;
	}

	public void setLeftOrRight(String leftOrRight) {
		this.leftOrRight = leftOrRight;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}



	public Shape getMyFill() {
		return myFill;
	}

	public int getWhichtexture() {
		return whichtexture;
	}

	public void setWhichtexture(int whichtexture) {
		this.whichtexture = whichtexture;
	}

	public CurvePanel getCp() {
		return cp;
	}

	public void setCp(CurvePanel cp) {
		this.cp = cp;
	}

	public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}
}
