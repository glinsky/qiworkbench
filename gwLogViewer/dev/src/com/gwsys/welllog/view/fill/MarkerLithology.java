/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.fill;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.MarkerLayer;
import com.gwsys.welllog.view.curve.PanelOrder;

public class MarkerLithology {
	private Shape myFill;
	private Color fillColor;
	private int whichtexture = -1;
	private CurveMarker cm1,cm2;
	private String leftOrRight = ConstantVariableList.FILL_ALL; // "LEFT" or "RIGHT"
	private Object owner;
	private boolean selected = false;

	/**
	 * Constructor.
	 * @param cv CurveView
	 */
	/**
	 * Constructor.
	 * @param cv CurveView
	 */
	public void constructFill(CurveView cv,float rateWidth, float rateHeight){
		PanelOrder order = cv.getPanelOrder();
		int widthForLeft = (int)(order.getPanelLeft(owner)*rateWidth);
		int widthForRight = (int)(order.getPanelRight(owner)*rateHeight);

		if(cm1.getMarkerDepth() > cm2.getMarkerDepth()){
			CurveMarker tmp = cm1;
			cm1=cm2;
			cm2=tmp;
		}

		float topY = cm1.getMarkerDepth();
		float bottomY = cm2.getMarkerDepth();

		if(topY == bottomY){
			this.myFill = null;
			return;
		}

		GeneralPath path = new GeneralPath();

		if(this.getLeftOrRight().equals(ConstantVariableList.FILL_LEFT))
		{
			Shape curveShape = ((CurvePanel)owner).setTranslatedCurve(cv,widthForLeft,topY,bottomY,true,rateWidth,rateHeight);

			if(curveShape == null){
				this.myFill = null;
				return;
			}
			path.append(curveShape,true);
			Rectangle reg=curveShape.getBounds();
			int minY=(int)(reg.getMinY()*1);
			int maxY=(int)(reg.getMaxY()*1);
			path.lineTo(widthForLeft,maxY);
			path.lineTo(widthForLeft,minY);
		}
		else if(this.getLeftOrRight().equals(ConstantVariableList.FILL_RIGHT))
		{
			Shape curveShape = ((CurvePanel)owner).setTranslatedCurve(cv,widthForLeft,topY,bottomY,true,rateWidth,rateHeight);
			if(curveShape == null){
				this.myFill = null;
				return;
			}
			path.append(curveShape,true);
			Rectangle reg=curveShape.getBounds();
			int minY=(int)(reg.getMinY()*1);
			int maxY=(int)(reg.getMaxY()*1);
			path.lineTo(widthForRight,maxY);
			path.lineTo(widthForRight,minY);
		}
		else if(owner instanceof CurvePanel){
			Shape curveShape = ((CurvePanel)owner).setTranslatedCurve(cv,widthForLeft,topY,bottomY,true,rateWidth,rateHeight);

			if(curveShape == null){
				this.myFill = null;
				return;
			}
			path.append(curveShape,true);
			Rectangle reg=curveShape.getBounds();
			int minY=(int)(reg.getMinY()*1);
			int maxY=(int)(reg.getMaxY()*1);
			path.lineTo(widthForLeft,maxY);
			path.lineTo(widthForLeft,minY);

			path.append(curveShape,true);
			reg=curveShape.getBounds();
			minY=(int) (reg.getMinY()*1);
			maxY=(int) (reg.getMaxY()*1);
			path.lineTo(widthForRight,maxY);
			path.lineTo(widthForRight,minY);
		}
		else if(owner instanceof CurveGroup){
			cm1.setYHeight(cv);
			cm2.setYHeight(cv);

			path.moveTo(widthForLeft,(int)(cm1.getYHeight()*rateHeight));
			path.lineTo(widthForRight,(int)(cm1.getYHeight()*rateHeight));
			path.lineTo(widthForRight,(int)(cm2.getYHeight()*rateHeight));
			path.lineTo(widthForLeft,(int)(cm2.getYHeight()*rateHeight));

		}

		this.myFill = path;

		if(owner instanceof CurvePanel)
			((CurvePanel)owner).setCurveRedrawFlag(true);
	}
	public Element saveXML(Document doc, Integer id){
		Element xmlMarkerLithology = doc.createElement("MarkerLithology");
		xmlMarkerLithology.setAttribute("id",Integer.toString(id));
		xmlMarkerLithology.setAttribute("fillColor",Integer.toString(this.fillColor.getRGB()));
		xmlMarkerLithology.setAttribute("whichtexture",Integer.toString(this.whichtexture));
		xmlMarkerLithology.setAttribute("CurveMarker1",this.cm1.getMarkerName());
		xmlMarkerLithology.setAttribute("CurveMarker2",this.cm2.getMarkerName());

		return xmlMarkerLithology;
	}

	public void loadXML(Element xmlMarkerLithology, MarkerLayer markerLayer){
		this.setFillColor(new Color(Integer.parseInt(xmlMarkerLithology.getAttribute("fillColor"))));
		this.setWhichtexture(Integer.parseInt(xmlMarkerLithology.getAttribute("whichtexture")));
		this.cm1 = null;
		this.cm2 = null;
		for(int i = 0 ; i < markerLayer.getMarkerList().size() ; i++){
			CurveMarker curveMarker = (CurveMarker)markerLayer.getMarkerList().get(i);
			if(curveMarker.getMarkerName().equals(xmlMarkerLithology.getAttribute("CurveMarker1"))){
				this.cm1 = curveMarker;
			}
			if(curveMarker.getMarkerName().equals(xmlMarkerLithology.getAttribute("CurveMarker2"))){
				this.cm2 = curveMarker;
			}
		}
	}

	public void setCurveMarker(CurveMarker cm1,CurveMarker cm2)
	{
		this.cm1 = cm1;
		this.cm2 = cm2;
	}

	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}
	public Shape getMyFill() {
		return myFill;
	}
	public void setMyFill(Shape myFill) {
		this.myFill = myFill;
	}
	public int getWhichtexture() {
		return whichtexture;
	}
	public void setWhichtexture(int whichtexture) {
		this.whichtexture = whichtexture;
	}

	public String getLeftOrRight() {
		return leftOrRight;
	}

	public void setLeftOrRight(String leftOrRight) {
		this.leftOrRight = leftOrRight;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Object getOwner() {
		return owner;
	}

	public void setOwner(Object owner) {
		this.owner = owner;
	}

	public CurveMarker getCm1() {
		return cm1;
	}

	public void setCm1(CurveMarker cm1) {
		this.cm1 = cm1;
	}

	public CurveMarker getCm2() {
		return cm2;
	}

	public void setCm2(CurveMarker cm2) {
		this.cm2 = cm2;
	}


}
