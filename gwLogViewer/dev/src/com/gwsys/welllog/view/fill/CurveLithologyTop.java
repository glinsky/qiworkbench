/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.fill;

import java.awt.Shape;
import java.awt.geom.GeneralPath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.util.ReversePathIterator;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;

public class CurveLithologyTop {
	
	private Shape myFill;
	private CurvePanel c1,c2;
	private int whichtexture = -1;
	
	//add by gaofei
	private boolean selected = false;	
	
	public void constructFill(CurveView cv) 
	{
		GeneralPath path = new GeneralPath();
		//run c1
		path.append(c1.getTranslatedCurve(),true);
		//run reverse c2
		path.append(ReversePathIterator.getReversePathIterator(c2.getTranslatedCurve()),true);
		myFill = path;
	}
	
	public Element saveXML(Document doc, Integer id){
		Element xmlCurveLithologyTop = doc.createElement("CurveLithologyTop");
		xmlCurveLithologyTop.setAttribute("id",Integer.toString(id));
		xmlCurveLithologyTop.setAttribute("curve1",this.getC1().getCd().getHashCurveName());
		xmlCurveLithologyTop.setAttribute("curve2",this.getC2().getCd().getHashCurveName());
		xmlCurveLithologyTop.setAttribute("texture",Integer.toString(this.getWhichtexture()));
		
		return xmlCurveLithologyTop;
	}
	
	public void loadXML(Element xmlCurveLithologyTop, CurveView cv){
		CurvePanel cp1 = null;
		CurvePanel cp2 = null;
		for(int i =0 ; i < cv.getCurveLayer().getCurveList().size() ; i++){
			if (((CurvePanel) cv.getCurveLayer().getCurveList().get(i))
					.getCd().getHashCurveName().equals(xmlCurveLithologyTop.getAttribute("curve1"))){
				cp1 = (CurvePanel) cv.getCurveLayer().getCurveList().get(i);
			}
			if (((CurvePanel) cv.getCurveLayer().getCurveList().get(i))
					.getCd().getHashCurveName().equals(xmlCurveLithologyTop.getAttribute("curve2"))){
				cp2 = (CurvePanel) cv.getCurveLayer().getCurveList().get(i);
			}
		}
		if(cp1 == null || cp2 == null){
			return;
		}
		this.setCurvePanel(cp1, cp2);
		this.setWhichtexture(Integer.parseInt(xmlCurveLithologyTop.getAttribute("texture")));
		this.constructFill(cv);
	}
	
	public Shape getMyFill() {
		return myFill;
	}

	public void setCurvePanel(CurvePanel c1,CurvePanel c2)
	{
		this.c1 = c1 ; 
		this.c2 = c2 ;
	}


	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}



	public int getWhichtexture() {
		return whichtexture;
	}



	public void setWhichtexture(int whichtexture) {
		this.whichtexture = whichtexture;
	}



	public CurvePanel getC1() {
		return c1;
	}



	public CurvePanel getC2() {
		return c2;
	}
	
	
		
}
