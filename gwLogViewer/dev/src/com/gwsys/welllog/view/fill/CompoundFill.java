/**
 * 
 */
package com.gwsys.welllog.view.fill;

import java.awt.Color;
import java.awt.Shape;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;

/**
 * @author Alex
 *
 */
public class CompoundFill {
	private Shape myFill;
	private Color fillColor;
	private CurvePanel leftCP, rightCP;
	private String groupName = "";
	
	private ArrayList<Shape> myFills = new ArrayList<Shape>();
	
	public void constructFill(CurvePanel leftCurve, CurvePanel rightCurve)	{
		this.leftCP = leftCurve;
		this.rightCP = rightCurve;
		
		if (this.groupName.equals(""))
			this.groupName = leftCurve.getCg().getName();
		
		FillShapeBuilder builder = new FillShapeBuilder(
				leftCurve.getTranslatedCurve(),
				rightCurve.getTranslatedCurve());
		builder.buildShape();
		myFills = builder.getShapeList();
	}
	
	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}
	
	public Shape getMyFill() {
		return myFill;
	}
	
	public CurvePanel getLeftCurve()	{
		return this.leftCP;
	}
	
	public CurvePanel getRightCurve()	{
		return this.rightCP;
	}
	
	public String getGroupName()	{
		return this.groupName;
	}
	
	public void setGroup(String group)	{
		this.groupName = group;
	}
	
	public ArrayList<Shape> getAllFills()	{
		return this.myFills;
	}
	
	public Element saveXML(Document doc, int id)	{
		Element xmlCompoundFill = doc.createElement("CompoundFill");
		
		if (this.getLeftCurve()!=null && this.getRightCurve()!=null)	{
			xmlCompoundFill.setAttribute("id",Integer.toString(id));
			xmlCompoundFill.setAttribute("leftCurve",this.getLeftCurve().getCd().getHashCurveName());
			xmlCompoundFill.setAttribute("rightCurve",this.getRightCurve().getCd().getHashCurveName());
			xmlCompoundFill.setAttribute("group",this.getGroupName());
			xmlCompoundFill.setAttribute("color",Integer.toString(this.getFillColor().getRGB()));
		}
		return xmlCompoundFill;		
	}
	
	public void loadXML(Element xmlCompoundFill, CurveView cv)	{
		CurvePanel leftCp = null;
		CurvePanel rightCp = null;
		
		leftCp = getThisCurvePanel(cv, xmlCompoundFill.getAttribute("leftCurve"), xmlCompoundFill.getAttribute("group"));
		rightCp = getThisCurvePanel(cv, xmlCompoundFill.getAttribute("rightCurve"), xmlCompoundFill.getAttribute("group"));
		
		if (leftCp == null || rightCp == null){
			return;
		}
				
		this.setGroup(xmlCompoundFill.getAttribute("group"));
		this.constructFill(leftCp, rightCp);
		this.setFillColor(new Color(Integer.parseInt(xmlCompoundFill.getAttribute("color"))));		
	}
	
	private CurvePanel getThisCurvePanel(CurveView cv, String curveHashkey, String groupName)	{
		CurveGroup cg;
		
		for (int i = 0; i < cv.getCurveGroups().size(); i++)	{
			cg = (CurveGroup) cv.getCurveGroups().get(i);
		
			if (cg.getName().equals(groupName))	{
				for (int k = 0; k < cg.getCurveList().size(); k++)	{
					CurvePanel cp = (CurvePanel) cg.getCurveList().get(k);
					
					if (cp.getCd().getHashCurveName().equals(curveHashkey))
						return cp;
				}
			}				
		}
		
		return null;
	}
}
