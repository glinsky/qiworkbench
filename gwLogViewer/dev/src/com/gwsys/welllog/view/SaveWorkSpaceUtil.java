/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.curve.CurveData;

public class SaveWorkSpaceUtil {
	/**
	 * for save workspace
	 * @param curveDataList
	 * @return namelist
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList changeCurveDataListToNameList(ArrayList curveDataList)
	{
		ArrayList curveHashNameList = new ArrayList();
		for(int i = 0 ; i<curveDataList.size();i++)
		{
			curveHashNameList.add(((CurveData)curveDataList.get(i)).getHashCurveName());
		}
		return curveHashNameList;
	}
	/**
	 * for load workspace
	 * @param curveNameList
	 * @return cd list
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList changeNameListToCurveDataList(ArrayList curveNameList)
	{
		ArrayList cdList = new ArrayList();
		for(int i = 0 ; i<curveNameList.size();i++)
		{
			cdList.add(DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get((String)curveNameList.get(i)));
		}
		return cdList;
	}
	public static CurveData getCdFromHash(String curvehashname)
	{
		return (CurveData)DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(curvehashname);
	}
	public static CurveData getCdFromCurveList(String curvehashname,ArrayList list)
	{
		for(int i = 0 ; i <list.size();i++)
		{
			CurveData cd = (CurveData)list.get(i);
			if(cd.getHashCurveName().equals(curvehashname))
			{
				return cd; 
			}
		}
		return null;
	}
	/**
	 * get draged node hashname list 
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList nodeDraged()
	{
		ArrayList list = new ArrayList();
		Iterator iterator = DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().values().iterator();
		while(iterator.hasNext())
		{
			CurveData cd = (CurveData)iterator.next();
			if(cd.isDraged())
			{
				list.add(cd.getHashCurveName());
			}
		}
		return list;
	}
	/**
	 * check a curve is a special curve?
	 * @param name
	 * @return
	 */
	public static boolean isMeanOrP105090(String name)
	{
		if(name.equals(ConstantVariableList.P10)||name.equals(ConstantVariableList.P50)||name.equals(ConstantVariableList.P90)||name.equals(ConstantVariableList.MEAN))
		{
			return true;
		}
		return false;
	}
}
