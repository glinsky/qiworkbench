/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view;

import javax.swing.JSplitPane;

import com.gwsys.welllog.view.projecttree.ProjectTreeScrollPane;
import com.gwsys.welllog.view.projecttree.ProjectTreeView;
import com.gwsys.welllog.view.viewtree.ViewTree;
import com.gwsys.welllog.view.viewtree.ViewTreeScrollPane;

public class PVTreePaneView extends JSplitPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ViewTreeScrollPane viewTreeScrollPane = null;
	private ProjectTreeScrollPane projectTreeScrollPane = null;

	public ProjectTreeView getProjectTreeView() {
		return this.projectTreeScrollPane.getLeftTreeView();
	}
	
	public ViewTree getViewTree(){
		return this.viewTreeScrollPane.getViewTree();
	}
	/**
	 * This method initializes viewTreeScrollPane	
	 * 	
	 * @return com.gwsys.welllog.Views.ViewTreeScrollPane	
	 */
	private ViewTreeScrollPane getViewTreeScrollPane() {
		if (viewTreeScrollPane == null) {
			viewTreeScrollPane = new ViewTreeScrollPane();
		}
		return viewTreeScrollPane;
	}

	/**
	 * This method initializes projectTreeScrollPane	
	 * 	
	 * @return com.gwsys.welllog.Views.ProjectTreeScrollPane	
	 */
	private ProjectTreeScrollPane getProjectTreeScrollPane() {
		if (projectTreeScrollPane == null) {
			projectTreeScrollPane = new ProjectTreeScrollPane();
		}
		return projectTreeScrollPane;
	}


	/**
	 * This is the default constructor
	 */
	public PVTreePaneView() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(300, 200);
		this.setTopComponent(getProjectTreeScrollPane());
		this.setBottomComponent(getViewTreeScrollPane());
		this.setDividerLocation(200);
		this.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
	}

}
