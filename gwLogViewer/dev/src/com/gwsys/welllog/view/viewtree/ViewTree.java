/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.viewtree;

import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.curve.PanelOrder;
import com.gwsys.welllog.view.track.TrackContainerView;

/**
 * Defines the tree structure to show log windows.
 * @author Team
 */
public class ViewTree extends JTree {
	private static final long serialVersionUID = 1L;

	private DefaultMutableTreeNode root = null;

	private DefaultTreeModel treeModel = null;

	private ViewTreeController treeController = null;

	/**
	 * the node being dragged within or outof view tree
	 */
	private DefaultMutableTreeNode nodeDnd = null;

	private String nodeDndHashmapKey = null;

	private ViewTreeCellRenderer renderer = null;

	/**
	 * constructor
	 */
	public ViewTree() {
		super();

		this.setModel(getTreeModel());
		this.addMouseListener(getTreeController());
		this.addMouseMotionListener(getTreeController());
		this.setTransferHandler(new TransferHandler("nodeDndHashmapKey"));
		this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		this.setCellRenderer(getRenderer());
	}


	/**
	 * this method is for the drag of dnd
	 *
	 * @return
	 */
	public String getNodeDndHashmapKey() {
		return nodeDndHashmapKey;
	}

	/**
	 * This is default behavior by Swing to drag n drop.
	 * It is the target of a transfer.
	 * @param nodeDndHashmapKey Property of transferring component.
	 */
	public void setNodeDndHashmapKey(String nodeDndHashmapKey) {
		// if there is no internal frame view selected, then cann't do the drag
		// and drop to view tree
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		if (ifv == null) {
			return;
		}
		this.nodeDndHashmapKey = nodeDndHashmapKey;
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) this.getSelectionPath().getLastPathComponent();
		// when drop's target is track, add new curve as the last curve

		if (selectedNode.getLevel() == 2) { //this is track
			// piece of code added by Alex. June 19, 2006
			// lines Starts. This runs fine.
			DefaultMutableTreeNode nodeDrag = findTrack(ifv.getTitle(),nodeDndHashmapKey);

			if (nodeDrag != null && nodeDrag.getLevel() == 2)	{
				// change Node order of ViewTree, only for Tracks
				int insertIndex = selectedNode.getParent().getIndex(selectedNode);			// this line was switched
				treeModel.removeNodeFromParent(nodeDrag);			// by this one
				treeModel.insertNodeInto(nodeDrag,(DefaultMutableTreeNode)selectedNode.getParent(),insertIndex);

			    //	change order of tracks in the view
			    //System.out.println("selected index: "+insertIndex);
				changeOrderOfTracks(nodeDrag.getUserObject().toString(), selectedNode.getUserObject().toString(), insertIndex);
			}

			// make sure there are no duplicate curves in view tree
			if (!checkIfNodeDndHashmapKeyExistsInThisTrack(nodeDndHashmapKey, selectedNode)) {
				//System.out.println("drag to Track");

				//June 29
				ViewCoreOperator.tryPutCurveToTrack(nodeDndHashmapKey, ((ViewTreeTrackNode)selectedNode.getUserObject()).getWellName());
			}

		}
		// when drop's target is curve, add new curve as the last curve
		else if (selectedNode.getLevel() == 3) { //this is curve into group
			// make sure there are no duplicate curves in view tree
			if (this.checkIfNodeDndHashmapKeyExistsInThisTrack(nodeDndHashmapKey, (DefaultMutableTreeNode) selectedNode.getParent())) {
				DefaultMutableTreeNode nodeDrag = getNodebynodeDndHashmapKey(nodeDndHashmapKey, (DefaultMutableTreeNode) selectedNode.getParent());
				if(nodeDrag.getParent().getIndex(nodeDrag) == selectedNode.getParent().getIndex(selectedNode)){
					return;
				}	// Add to group panel
				if((((ViewTreeNode)nodeDrag.getUserObject()).getNodeType() == ViewTreeNode.CURVE_NODE)
						&& (((ViewTreeNode)selectedNode.getUserObject()).getNodeType() == ViewTreeNode.GROUP_PANEL_NODE)){
					String curveHashmapKey = ((ViewTreeNode)nodeDrag.getUserObject()).getCurveHashmapKey();
					String groupHashmapKey = ((ViewTreeNode)selectedNode.getUserObject()).getCurveHashmapKey();
					if(ViewCoreOperator.IsHasThisCurveUnderGroup(curveHashmapKey,groupHashmapKey)){
						return;
					}
					treeModel.removeNodeFromParent(nodeDrag);
					int insertIndex = selectedNode.getChildCount();
					treeModel.insertNodeInto(nodeDrag,(DefaultMutableTreeNode)selectedNode,insertIndex);
					ViewCoreOperator.putCurveToGroupPanel(curveHashmapKey , groupHashmapKey);
					((CurveData)DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(curveHashmapKey)).setDraged(false);
					DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().revalidate();
					DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().repaint();

				}else{
					treeModel.removeNodeFromParent(nodeDrag);
				    int insertIndex = selectedNode.getParent().getIndex(selectedNode);
				    treeModel.insertNodeInto(nodeDrag,(DefaultMutableTreeNode)selectedNode.getParent(),insertIndex);
				    // change order of curve in the view
				    changeOrderOfCurve(nodeDrag.getUserObject().toString() , selectedNode.getUserObject().toString());
				}
			} else if (nodeDnd.getLevel() == 4)	{
				// added by Alex. June 28, 2006. This works good
				DefaultMutableTreeNode group = (DefaultMutableTreeNode) nodeDnd
						.getParent();
				DefaultMutableTreeNode trackNode = (DefaultMutableTreeNode) group
						.getParent();

				// remove curve in ViewTree
				this.removeCurve(nodeDnd); //setting is lost in here

				// Call to method in View
				getCurveOutFromGroup(((ViewTreeTrackNode) trackNode
						.getUserObject()).wellName, group.toString(), nodeDnd
						.toString());

				ViewCoreOperator.tryPutCurveToTrack(nodeDndHashmapKey, ((ViewTreeTrackNode)((DefaultMutableTreeNode)
						selectedNode.getParent()).getUserObject()).wellName);
				ViewCoreOperator.clearDraggedCurve();
			}
		}
	}

	/**
	 * added by Alex June 27, 2006
	 * Change order of tracks in InternalFrameView
	 *
	 * @param dragName name of dragged track
	 * @param selectedName name of selected track
	 * @param index position of selected track in track list and view
	 */
	public void changeOrderOfTracks(String dragName, String selectedName, int index)	{
		TrackContainerView trackView = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer();

		trackView.changeTrackOrder(dragName, selectedName, index);

		// refresh the view
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().repaintTrack();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().revalidate();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().repaint();

	}

	public void changeOrderOfCurve(String dragName , String selectName){
		//ArrayList curveList = DataBuffer.getInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer().getCurveView().getCurveLayer().getCurveList();
		PanelOrder panelOrder = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer().getCurveView().getPanelOrder();

		panelOrder.changeOrder(dragName , selectName);

//		DataBuffer.getInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer().getCurveView().display();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer().getCurveView().setFillRedrawFlag(true);
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer().getJp().revalidate();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().getSelectedAxisCurveContainer().repaintAll();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().repaint();

	}

	/**
	 * added by Alex June 30, 2006
	 * Calls a method to remove curve of group in InternalFrameView
	 *
	 * @param trackName
	 * @param groupName
	 * @param curveName
	 */
	public void getCurveOutFromGroup(String wellName, String groupName, String curveName)	{
		ViewCoreOperator.removeCurveFromGroup(wellName, groupName, curveName);

	}

	public ViewTreeCellRenderer getRenderer() {
		if (renderer == null) {
			renderer = new ViewTreeCellRenderer();
		}
		return renderer;
	}

	private DefaultTreeModel getTreeModel() {
		if (root == null) {
			root = new DefaultMutableTreeNode("Log Correlation");
		}
		if (treeModel == null) {
			treeModel = new DefaultTreeModel(root);
			treeModel.setRoot(root);
		}
		return treeModel;
	}

	public ViewTreeController getTreeController() {
		if (treeController == null) {
			treeController = new ViewTreeController();
		}
		return treeController;
	}

	/**
	 * this method is used to record the lastest selected left node, and only
	 * the lastest selected left node can be dragged
	 *
	 * @param node
	 */
	public void thisNodeMayBeDragged(DefaultMutableTreeNode node) {
		nodeDnd = node;
		// modified by Alex
		if (node.getUserObject() instanceof ViewTreeNode)//{
		//if (node.getLevel() == 3)
			nodeDndHashmapKey = ((ViewTreeNode)node.getUserObject() ).getCurveHashmapKey();
		else nodeDndHashmapKey = node.getUserObject().toString();
		//}
	}

	/**
	 * this method is used to set nodeDnd to null in case  that no-left node
	 * selected
	 *
	 */
	public void clearNodeMayBeDragged() {
		nodeDnd = null;
		nodeDndHashmapKey = null;
	}

	/**
	 * set the well log whose name is 'wellLogName' be selected
	 * @param trackName
	 */
	public void setActiveWellLog(String wellLogName) {
		DefaultMutableTreeNode wellLog = findWellLog(wellLogName);
		if (wellLog == null) {
			return;
		}
		this.setSelectionPath(new TreePath(wellLog.getPath()));
	}

	/**
	 * set the track whose name is 'trackName' be selected
	 * @param trackName
	 */
	public void setActiveTrack(String wellLogName, String wellName) {
		DefaultMutableTreeNode track = findTrack(wellLogName, wellName);
		if (track == null) {
			return;
		}
		this.setSelectionPath(new TreePath(track.getPath()));
	}

	/**
	 * set the curve whose name is 'curveName' be selected
	 * @param curveHashmapKey
	 * @param trackName
	 */
	public void setActiveCurve(String curveHashmapKey, String wellLogName, String wellName) {
		DefaultMutableTreeNode track = findTrack(wellLogName, wellName);
		if (track == null) {
			return;
		}
		DefaultMutableTreeNode curve = findCurveUnderTrack(curveHashmapKey, track);
		if (curve == null) {
			return;
		}
		this.setSelectionPath(new TreePath(curve.getPath()));
	}

	public void setActiveDepthOrGroup(String nodeName, String wellLogName, String wellName) {
		DefaultMutableTreeNode track = findTrack(wellLogName, wellName);
		if (track == null) {
			return;
		}
		DefaultMutableTreeNode node = null;
		for (int i = 0; i < track.getChildCount(); i++) {
			node = (DefaultMutableTreeNode) track.getChildAt(i);
			if (node.toString().equals(nodeName)) {
				break;
			}
			node = null;
		}
		if(node == null){
			return;
		}
		this.setSelectionPath(new TreePath(node.getPath()));
	}

	public void setActiveCurveUnderGroup(String wellLogName, String wellName, String groupName, String curveHashmapKey){
		DefaultMutableTreeNode track = findTrack(wellLogName, wellName);
		if (track == null) {
			return;
		}
		DefaultMutableTreeNode nodeGroup = null;
		for (int i = 0; i < track.getChildCount(); i++) {
			nodeGroup = (DefaultMutableTreeNode) track.getChildAt(i);
			if (nodeGroup.toString().equals(groupName)) {
				break;
			}
			nodeGroup = null;
		}
		if(nodeGroup == null){
			return;
		}
		DefaultMutableTreeNode curve = null;
		for (int i = 0; i < nodeGroup.getChildCount(); i++) {
			curve = (DefaultMutableTreeNode) nodeGroup.getChildAt(i);
			if (((ViewTreeNode) curve.getUserObject()).getCurveHashmapKey().equals(curveHashmapKey)) {
				break;
			}
			curve = null;
		}
		if (curve == null) {
			return;
		}
		//System.out.println("Path:"+curve.getParent());
		this.setSelectionPath(new TreePath(curve.getPath()));
	}

	/**
	 * add a new well log in the view tree as the last well log
	 *
	 * @param wellLogName
	 * @return
	 */
	public DefaultMutableTreeNode createWellLog(String wellLogName) {
		DefaultMutableTreeNode newWellLog = new DefaultMutableTreeNode(wellLogName);
		this.getTreeModel().insertNodeInto(newWellLog, this.root, this.root.getChildCount());
		this.setSelectionPath(new TreePath(newWellLog.getPath()));
		return newWellLog;
	}

	public boolean renameFrame(String oldName, String newName){
		DefaultMutableTreeNode wellLog = new DefaultMutableTreeNode("");
		for (int i = 0; i < this.root.getChildCount(); i++) {
			wellLog = (DefaultMutableTreeNode) this.root.getChildAt(i);
			if(wellLog.getUserObject().toString().equals(newName)){
				return false;
			}
		}
		for (int i = 0; i < this.root.getChildCount(); i++) {
			wellLog = (DefaultMutableTreeNode) this.root.getChildAt(i);
			if(wellLog.getUserObject().toString().equals(oldName)){
				wellLog.setUserObject(newName);
				treeModel.reload(wellLog);
				return true;
			}
		}
		return true;
	}
	/**
	 * find a well log by its name, if not found, return null.
	 * @param wellLogName
	 * @return
	 */
	public DefaultMutableTreeNode findWellLog(String wellLogName) {
		for (int i = 0; i < this.root.getChildCount(); i++) {
			DefaultMutableTreeNode track = (DefaultMutableTreeNode) this.root.getChildAt(i);
			if (((String) track.getUserObject()).equals(wellLogName)) {
				return track;
			}
		}
		return null;
	}

	/**
	 * remove entire well log including its childs
	 *
	 * @param track
	 */
	public void removeWellLog(String wellLogName) {
		DefaultMutableTreeNode wellLog = findWellLog(wellLogName);
		if (wellLog == null) {
			return;
		}
		for (int i = 0; i < wellLog.getChildCount(); i++) {
			DefaultMutableTreeNode trackNode = (DefaultMutableTreeNode) wellLog.getChildAt(i);
			this.removeTrack(wellLogName,((ViewTreeTrackNode) trackNode.getUserObject()).getWellName());
		}
		this.treeModel.removeNodeFromParent(wellLog);
		if (this.root.getChildCount() >= 1) {
			this.setSelectionPath(new TreePath(((DefaultMutableTreeNode) this.root.getFirstChild()).getPath()));
		}
	}
	/**
	 * add a new track in view tree as the last track
	 *
	 * @param trackName
	 * @return
	 */
	public DefaultMutableTreeNode createTrack(String wellLogName, String trackName ,
			String wellName, boolean selected) {
		DefaultMutableTreeNode wellLog = findWellLog(wellLogName);
		if (wellLog == null) {
			return null;
		}
		DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().getAllWells().
		add(wellLogName+OperationClass.hashMapNameSeparator+wellName);

		DefaultMutableTreeNode newTrack = new DefaultMutableTreeNode(new ViewTreeTrackNode(trackName,wellName));
		DefaultMutableTreeNode depthPanel = new DefaultMutableTreeNode(new ViewTreeNode(wellName +
				OperationClass.hashMapNameSeparator + ConstantVariableList.DEPTH_PANEL,
				ConstantVariableList.DEPTH_PANEL,ViewTreeNode.DEPTH_PANEL_NODE ));
		newTrack.add(depthPanel);
		
		this.getTreeModel().insertNodeInto(newTrack, wellLog, wellLog.getChildCount());
		if (selected)
			this.setSelectionPath(new TreePath(newTrack.getPath()));
		
		return newTrack;
	}

	public DefaultMutableTreeNode findTrack(String wellLogName , String wellName) {
		for (int i = 0; i < this.root.getChildCount(); i++) {
			DefaultMutableTreeNode wellLog = (DefaultMutableTreeNode) this.root.getChildAt(i);
			if(wellLog.toString().equals(wellLogName)){
				for (int j = 0; j < wellLog.getChildCount(); j++) {
					DefaultMutableTreeNode track = (DefaultMutableTreeNode) wellLog.getChildAt(j);
					if (((ViewTreeTrackNode) track.getUserObject()).getWellName().equals(wellName)) {
						return track;
					}
				}
			}
		}
		return null;
	}

	public void renameTrack(String wellLogName, String oldWellName, String newTrackName, String newWellName) {
		DefaultMutableTreeNode track = findTrack(wellLogName,oldWellName);
		if (track == null){
			return;
		}
		((ViewTreeTrackNode)track.getUserObject()).setTrackName(newTrackName);
		((ViewTreeTrackNode)track.getUserObject()).setWellName(newWellName);
		this.repaint();
	}

	public void renameDescriptionPanel(String wellLogName, String wellName, String oldDesName , String newDesName) {
		DefaultMutableTreeNode des = new DefaultMutableTreeNode("");
		DefaultMutableTreeNode track = this.findTrack(wellLogName,wellName);
		for(int z = 0 ; z < track.getChildCount() ; z++){
    		des = (DefaultMutableTreeNode) track.getChildAt(z);
    		if(((ViewTreeNode)des.getUserObject()).getCurveName().equals(oldDesName)){
    			((ViewTreeNode)des.getUserObject()).setCurveName(newDesName);
    			((ViewTreeNode)des.getUserObject()).setCurveHashmapKey(((ViewTreeNode)des.getUserObject()).
    					getCurveHashmapKey().split(OperationClass.hashMapNameSeparator)[0] +
    					OperationClass.hashMapNameSeparator + newDesName);
    		}
    	}
		this.repaint();
	}

	public void renameSeismicPanel(String wellLogName, String wellName, String oldSeiName , String newSeiName) {
		DefaultMutableTreeNode sei = new DefaultMutableTreeNode("");
		DefaultMutableTreeNode track = this.findTrack(wellLogName,wellName);
		for(int z = 0 ; z < track.getChildCount() ; z++){
    		sei = (DefaultMutableTreeNode) track.getChildAt(z);
    		if(((ViewTreeNode)sei.getUserObject()).getCurveName().equals(oldSeiName)){
    			((ViewTreeNode)sei.getUserObject()).setCurveName(newSeiName);
    			((ViewTreeNode)sei.getUserObject()).setCurveHashmapKey(((ViewTreeNode)sei.getUserObject()).
    					getCurveHashmapKey().split(OperationClass.hashMapNameSeparator)[0] +
    					OperationClass.hashMapNameSeparator + newSeiName);
    		}
    	}
		this.repaint();
	}
	/**
	 * remove entire track including its childs
	 *
	 * @param track
	 */
	public void removeTrack(String wellLogName , String wellName) {
		DefaultMutableTreeNode track = findTrack(wellLogName , wellName);
		if(track == null){
			return;
		}
		for (int i = 0; i < track.getChildCount(); i++) {
			this.removeCurve((DefaultMutableTreeNode) track.getChildAt(i));
		}
		this.treeModel.removeNodeFromParent(track);
		if (this.root.getChildCount() >= 1) {
			this.setSelectionPath(new TreePath(((DefaultMutableTreeNode) this.root.getFirstChild()).getPath()));
		}
	}

	/**
	 * add a curve to a track as the last curve
	 *
	 * @param track
	 *            the track node
	 * @param curveHashmapKey
	 *            curve's full name in HashMap
	 * @param curveName
	 *            curve's display name in tree
	 * @return
	 */
	public DefaultMutableTreeNode createCurve(String wellLogName , String wellName, String curveHashmapKey, String curveName) {
		return createCurve(wellLogName,wellName,curveHashmapKey,curveName, true);
	}

	/**
	 * add a curve to a track as the last curve
	 *
	 * @param track
	 *            the track node
	 * @param curveHashmapKey
	 *            curve's full name in HashMap
	 * @param curveName
	 *            curve's display name in tree
	 * @return
	 */
	public DefaultMutableTreeNode createCurve(String wellLogName , String wellName, String curveHashmapKey, String curveName, boolean selected) {
		DefaultMutableTreeNode track = findTrack(wellLogName, wellName);
		DefaultMutableTreeNode newCurve = new DefaultMutableTreeNode(new ViewTreeNode(curveHashmapKey, curveName,ViewTreeNode.CURVE_NODE));
		this.treeModel.insertNodeInto(newCurve, track, track.getChildCount());
		if (selected)
			this.setSelectionPath(new TreePath(newCurve.getPath()));
		return newCurve;
	}
	/**
	 * add a group panel to a track
	 * @param trackName
	 * @param groupPanelName
	 * @return
	 */
	public DefaultMutableTreeNode createGroupPanel(String wellLogName , String wellName, String groupPanelName, boolean selected) {
		DefaultMutableTreeNode track = findTrack(wellLogName, wellName);
		DefaultMutableTreeNode newGroupPanel = new DefaultMutableTreeNode(new ViewTreeNode(wellName+OperationClass.hashMapNameSeparator +
				groupPanelName, groupPanelName,ViewTreeNode.GROUP_PANEL_NODE));
		this.treeModel.insertNodeInto(newGroupPanel, track, track.getChildCount());
		if (selected)
			this.setSelectionPath(new TreePath(newGroupPanel.getPath()));
		return newGroupPanel;
	}

	public DefaultMutableTreeNode createCurveUnderGroupPanel(String wellLogName , String wellName, String groupPanelName,String curveHashmapKey, String curveName) {
		DefaultMutableTreeNode track = findTrack(wellLogName, wellName);
		DefaultMutableTreeNode groupPanel = findGroupPanelUnderTrack(groupPanelName,track);
		DefaultMutableTreeNode newCurve = new DefaultMutableTreeNode(new ViewTreeNode(curveHashmapKey, curveName,ViewTreeNode.CURVE_NODE));
		this.treeModel.insertNodeInto(newCurve, groupPanel, groupPanel.getChildCount());
		//this.setSelectionPath(new TreePath(newCurve.getPath()));
		return newCurve;
	}

	/**
	 * find the curve under certain track by curve's key in the HashMap
	 *
	 * @param curveHashmapKey
	 * @param track
	 * @return
	 */
	public DefaultMutableTreeNode findCurveUnderTrack(String curveHashmapKey, DefaultMutableTreeNode track) {
		for (int i = 0; i < track.getChildCount(); i++) {
			DefaultMutableTreeNode curve = (DefaultMutableTreeNode) track.getChildAt(i);
			if (((ViewTreeNode) curve.getUserObject()).getCurveHashmapKey().equals(curveHashmapKey)) {
				return curve;
			}
		}
		return null;
	}

	public DefaultMutableTreeNode findGroupPanelUnderTrack(String panelName, DefaultMutableTreeNode track) {
		for (int i = 0; i < track.getChildCount(); i++) {
			DefaultMutableTreeNode panel = (DefaultMutableTreeNode) track.getChildAt(i);
			ViewTreeNode node = (ViewTreeNode) panel.getUserObject();
			if (node.nodeType == ViewTreeNode.GROUP_PANEL_NODE && node.getCurveHashmapKey().split(
					OperationClass.hashMapNameSeparator)[1].equals(panelName)) {
				return panel;
			}
		}
		return null;
	}

	public DefaultMutableTreeNode findPanelUnderTrack(String panelName, DefaultMutableTreeNode track) {
		for (int i = 0; i < track.getChildCount(); i++) {
			DefaultMutableTreeNode panel = (DefaultMutableTreeNode) track.getChildAt(i);
			ViewTreeNode node = (ViewTreeNode) panel.getUserObject();
			if (node.nodeType == ViewTreeNode.DESCRIPTION_PANEL_NODE && node.getCurveHashmapKey().split(
					OperationClass.hashMapNameSeparator)[1].equals(panelName)) {
				return panel;
			}
			else if (node.nodeType == ViewTreeNode.SEISMIC_PANEL_NODE && node.getCurveHashmapKey().split(
					OperationClass.hashMapNameSeparator)[1].equals(panelName)) {
				return panel;
			}else if(node.nodeType == ViewTreeNode.DEPTH_PANEL_NODE && node.getCurveHashmapKey().split(
					OperationClass.hashMapNameSeparator)[1].equals(panelName)){
				return panel;
			}else if(node.nodeType == ViewTreeNode.LITHOLOGY_PANEL_NODE && node.getCurveHashmapKey().split(
					OperationClass.hashMapNameSeparator)[1].equals(panelName)){
				return panel;
			}

		}
		return null;
	}

	public DefaultMutableTreeNode findCurveUnderGroupPanel(String curveHashmapKey, DefaultMutableTreeNode track, String groupPanelHashmapKey){
		for (int i = 0; i < track.getChildCount(); i++) {
			DefaultMutableTreeNode groupPanel = (DefaultMutableTreeNode) track.getChildAt(i);
			if ((((ViewTreeNode) groupPanel.getUserObject()).getNodeType() == ViewTreeNode.GROUP_PANEL_NODE)
					&& (((ViewTreeNode) groupPanel.getUserObject()).getCurveHashmapKey().equals(groupPanelHashmapKey)) ) {
				for(int j=0;j<groupPanel.getChildCount();j++){}
					DefaultMutableTreeNode curve = (DefaultMutableTreeNode) groupPanel.getChildAt(i);
					if (((ViewTreeNode) curve.getUserObject()).getCurveHashmapKey().equals(curveHashmapKey)) {
						return curve;
				}
			}
		}
		return null;
	}
	/**
	 * remove a curve from the view tree
	 *
	 * @param curve
	 */
	public void removeCurve(DefaultMutableTreeNode curve) {
		this.treeModel.removeNodeFromParent(curve);
	}

	public DefaultMutableTreeNode getNodeDnd() {
		return nodeDnd;
	}

	public void setNodeDnd(DefaultMutableTreeNode nodeDnd) {
		this.nodeDnd = nodeDnd;
	}

	/**
	 * see if the curve whose key in hashmap has exists under one certain track
	 * @param nodeDndHashmapKey
	 * @param track
	 * @return
	 */
	public boolean checkIfNodeDndHashmapKeyExistsInThisTrack(String nodeDndHashmapKey, DefaultMutableTreeNode track) {
		for (int i = 0; i < track.getChildCount(); i++) {
			DefaultMutableTreeNode curve = (DefaultMutableTreeNode) track.getChildAt(i);
			if (((ViewTreeNode) curve.getUserObject()).getCurveHashmapKey().equals(nodeDndHashmapKey)) {
				return true;
			}
		}
		return false;
	}

	public DefaultMutableTreeNode getNodebynodeDndHashmapKey(String nodeDndHashmapKey, DefaultMutableTreeNode track){
		for (int i = 0; i < track.getChildCount(); i++) {
			DefaultMutableTreeNode curve = (DefaultMutableTreeNode) track.getChildAt(i);

			if (((ViewTreeNode) curve.getUserObject()).getCurveHashmapKey().equals(nodeDndHashmapKey)) {
				return curve;
			}
		}
		return null;
	}
	/**
	 * reset the view tree to empty
	 *
	 */
	public void clearContent() {
		//some tips: because root.getChildCount() will change during clear operation
		//so during the for(){} 's circulation, we always delete the first child of root
		int wellLogNumber = root.getChildCount();
		for(int i = 0; i < wellLogNumber; i++) {
			DefaultMutableTreeNode wellLog = (DefaultMutableTreeNode)root.getChildAt(0);
			this.removeWellLog((String)wellLog.getUserObject());
		}
	}

	public DefaultMutableTreeNode createDescriptionPanel(String wellName , String descriptionName){
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		DefaultMutableTreeNode track = findTrack(ifv.getTitle(), wellName);
		DefaultMutableTreeNode newDescriptionPanel = new DefaultMutableTreeNode(
				new ViewTreeNode(wellName+OperationClass.hashMapNameSeparator+descriptionName ,
						descriptionName ,ViewTreeNode.DESCRIPTION_PANEL_NODE));
		this.treeModel.insertNodeInto(newDescriptionPanel, track, track.getChildCount());
		this.setSelectionPath(new TreePath(newDescriptionPanel.getPath()));
		return newDescriptionPanel;
	}

	public DefaultMutableTreeNode createSeismicPanel(String wellName , String seismicName){
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		DefaultMutableTreeNode track = findTrack(ifv.getTitle(), wellName);
		DefaultMutableTreeNode newSeismicPanel = new DefaultMutableTreeNode(
				new ViewTreeNode(wellName+OperationClass.hashMapNameSeparator+seismicName ,
						seismicName ,ViewTreeNode.SEISMIC_PANEL_NODE));
		this.treeModel.insertNodeInto(newSeismicPanel, track, track.getChildCount());
		this.setSelectionPath(new TreePath(newSeismicPanel.getPath()));
		return newSeismicPanel;
	}

	public DefaultMutableTreeNode createLithologyPanel(String wellName , String lithologyName){
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		DefaultMutableTreeNode track = findTrack(ifv.getTitle(), wellName);
		DefaultMutableTreeNode newLithologyPanel = new DefaultMutableTreeNode(
				new ViewTreeNode(wellName+OperationClass.hashMapNameSeparator+lithologyName ,
						lithologyName ,ViewTreeNode.LITHOLOGY_PANEL_NODE));
		this.treeModel.insertNodeInto(newLithologyPanel, track, track.getChildCount());
		this.setSelectionPath(new TreePath(newLithologyPanel.getPath()));
		return newLithologyPanel;
	}

	public void addDepthPanel(String wellName){
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		DefaultMutableTreeNode track = findTrack(ifv.getTitle(), wellName);
		DefaultMutableTreeNode depth = findPanelUnderTrack(ConstantVariableList.DEPTH_PANEL,track);
		treeModel.removeNodeFromParent(depth);
		this.treeModel.insertNodeInto(depth, track, track.getChildCount());
	}

	public void addCurveToGroup(String wellLogName , String wellName, String curveHashmapKey, String groupHashmapKey){
		if(ViewCoreOperator.IsHasThisCurveUnderGroup(curveHashmapKey,groupHashmapKey)){
			return;
		}

		String groupName = groupHashmapKey.split(OperationClass.hashMapNameSeparator)[1];
		String curveName = curveHashmapKey.split(OperationClass.hashMapNameSeparator)[1];

		DefaultMutableTreeNode track = this.findTrack(wellLogName,wellName);

		DefaultMutableTreeNode groupNode=null;
		DefaultMutableTreeNode curveNode=null;

		for (int i = 0; i < track.getChildCount(); i++) {
			DefaultMutableTreeNode tmpNode = (DefaultMutableTreeNode) track.getChildAt(i);
			ViewTreeNode node = (ViewTreeNode) tmpNode.getUserObject();

			if (node.nodeType == ViewTreeNode.GROUP_PANEL_NODE && node.getCurveHashmapKey().split(
					OperationClass.hashMapNameSeparator)[1].equals(groupName)) {
				groupNode=tmpNode;
			}else if (node.nodeType == ViewTreeNode.CURVE_NODE && node.getCurveHashmapKey().split(
					OperationClass.hashMapNameSeparator)[1].equals(curveName)) {
				curveNode=tmpNode;
			}
		}

		treeModel.removeNodeFromParent(curveNode);
		int insertIndex = groupNode.getChildCount();
		treeModel.insertNodeInto(curveNode,(DefaultMutableTreeNode)groupNode,insertIndex);
		ViewCoreOperator.putCurveToGroupPanel(curveHashmapKey , groupHashmapKey);
		((CurveData)DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(curveHashmapKey)).setDraged(false);

		DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().revalidate();
		DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().repaint();
	}
	
	/**
	 * added by Alex
	 * retrieves well node given its name and a well Log name
	 * @param wellLog name of well log window
	 * @param wellName name of well
	 * @return well node
	 */
	public DefaultMutableTreeNode getWellNodeByName(String wellLog, String wellName)	{
		DefaultMutableTreeNode wellLogNode = findWellLog(wellLog);
		
		if (wellLogNode == null)
			return null;
		
		int childCount = wellLogNode.getRoot().getChildCount();
		
		for (int i = 0; i < childCount; i++)	{
			DefaultMutableTreeNode wellNode = (DefaultMutableTreeNode) wellLogNode.getChildAt(i);
			if (wellNode.getUserObject().toString().equals(wellName))
				return wellNode;
		}
		
		return null;
	}
}
