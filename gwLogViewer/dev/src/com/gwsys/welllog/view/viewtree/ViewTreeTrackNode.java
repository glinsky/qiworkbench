package com.gwsys.welllog.view.viewtree;

public class ViewTreeTrackNode {
	private static final long serialVersionUID = 1L;

	public String trackName = null;

	public String wellName = null;
	
	public boolean isDraged = false;

	public ViewTreeTrackNode(String trackName , String wellName){
		this.trackName = trackName;
		this.wellName = wellName;
	}
	
	public String toString() {
		return trackName;
	}

	public boolean isDraged() {
		return isDraged;
	}

	public void setDraged(boolean isDraged) {
		this.isDraged = isDraged;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getWellName() {
		return wellName;
	}

	public void setWellName(String wellName) {
		this.wellName = wellName;
	}
	
}
