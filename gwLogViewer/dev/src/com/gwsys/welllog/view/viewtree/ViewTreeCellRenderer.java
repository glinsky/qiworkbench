/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.viewtree;

import java.awt.Component;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.gwsys.welllog.icons.IconResource;


/**
 * ViewTree's Renderer
 * @author stone
 *
 */
public class ViewTreeCellRenderer extends DefaultTreeCellRenderer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ImageIcon iconSelected = new ImageIcon();
	private ImageIcon iconUnSelected = new ImageIcon();
	
	public ViewTreeCellRenderer()
	{
		this.setNodeIcon();
	}
	
	public Component getTreeCellRendererComponent(
            JTree tree,
            Object value,
            boolean sel,
            boolean expanded,
            boolean leaf,
            int row,
            boolean hasFocus)
	{
		super.getTreeCellRendererComponent(
		            tree, value, sel,
		            expanded, leaf, row,
		            hasFocus);
	

		return this;
	}
	/**
	 * 
	 * @param value
	 * @return node text
	 */
	public String getString(Object value)
	{
		try
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
			return ((ViewTreeNode)node.getUserObject()).getCurveName();
		}
		catch(Exception e1)
		{
			return "";
		}
	}
	/**
	 * 
	 * @param value
	 * @return is node seleted
	 */
	public boolean nodeStatus(Object value)
	{
		try
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
			return ((ViewTreeNode)node.getUserObject()).isDraged();
		}
		catch(Exception e1)
		{
			return false;
		}
	}
	public void setNodeIcon()
	{
		Image imageSelected = IconResource.getInstance().getImage("selected.gif");		
		Image imageUnSelected = IconResource.getInstance().getImage("noselect.gif");
		
		this.iconSelected.setImage(imageSelected);
		this.iconUnSelected.setImage(imageUnSelected);
		
	}
}