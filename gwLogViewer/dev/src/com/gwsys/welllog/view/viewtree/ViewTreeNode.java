/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.viewtree;

import java.io.Serializable;

public class ViewTreeNode implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String curveName = null;

	public boolean isDraged = false;

	public String curveHashmapKey = null;
	
	public int nodeType = 0;
	
	public static final int DEPTH_PANEL_NODE=1;
	
	public static final int CURVE_NODE=2;
	
	public static final int GROUP_PANEL_NODE=3;
	
	public static final int DESCRIPTION_PANEL_NODE=4;
	
	public static final int SEISMIC_PANEL_NODE = 5;
	
	public static final int LITHOLOGY_PANEL_NODE = 6;
	
	public ViewTreeNode(String curveHashmapKey, String curveName,int nodeType) {
		super();
		// TODO Auto-generated constructor stub
		this.curveHashmapKey = curveHashmapKey;
		this.curveName = curveName;
		this.nodeType = nodeType;
	}

	// must over this function to make drag and drop work properly
	public String toString() {
		return curveName;
	}

	public void setCurveName(String curveName) {
		this.curveName = curveName;
	}

	public boolean isDraged() {
		return isDraged;
	}

	public void setDraged(boolean isDraged) {
		this.isDraged = isDraged;
	}

	public String getCurveHashmapKey() {
		return curveHashmapKey;
	}

	public String getCurveName() {
		return curveName;
	}

	public int getNodeType() {
		return nodeType;
	}
	
	public void setCurveHashmapKey(String curveHashmapKey) {
		this.curveHashmapKey = curveHashmapKey;
	}
}
