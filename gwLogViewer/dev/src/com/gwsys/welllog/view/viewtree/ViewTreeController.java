/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.viewtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.DesktopPaneView;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.DescriptionPanel;
import com.gwsys.welllog.view.curve.LithologyPanel;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.curve.PanelOrder;
import com.gwsys.welllog.view.curve.SeismicPanel;
import com.gwsys.welllog.view.dialogs.EditBesideCurveDialog;
import com.gwsys.welllog.view.dialogs.EditCurveDialog;
import com.gwsys.welllog.view.dialogs.EditDescriptionSettingDialog;
import com.gwsys.welllog.view.dialogs.EditFrameDialog;
import com.gwsys.welllog.view.dialogs.EditGroupAreaDialog;
import com.gwsys.welllog.view.dialogs.EditGroupDialog;
import com.gwsys.welllog.view.dialogs.EditLithologySettingDialog;
import com.gwsys.welllog.view.dialogs.EditSeismicSettingDialog;
import com.gwsys.welllog.view.dialogs.EditTrackDialog;

public class ViewTreeController implements MouseListener,MouseMotionListener,Serializable ,ActionListener{
	private ViewTree tree = null;

	private DefaultMutableTreeNode windowNode = null;
	private DefaultMutableTreeNode trackNode = null;
	private DefaultMutableTreeNode descriptionNode = null;
	private DefaultMutableTreeNode seismicNode = null;
	private DefaultMutableTreeNode lithologyNode = null;
	private DefaultMutableTreeNode groupNode = null;

	private String n1 = "";
	private String n2 = "";
	private static final long serialVersionUID = 1L;


	public void mouseClicked(MouseEvent e) {

	}

	/**
	 * when mouse pressed in the view tree, first check if curve node selected.
	 * if so, then prepare to curve-drag operation;
	 * else just select node in respective level(well log, track, curve)
	 */
	public void mousePressed(MouseEvent e) {
		tree = (ViewTree)e.getSource();
		int row = tree.getRowForLocation(e.getX(), e.getY());
		if(row == -1)
			return;
		final DefaultMutableTreeNode node = (DefaultMutableTreeNode)((ViewTree)e.getSource()).getLastSelectedPathComponent();

		TreePath path = tree.getPathForRow(row);
		DefaultMutableTreeNode currentNode = null;
		if (path != null) {
			currentNode = (DefaultMutableTreeNode)path.getLastPathComponent();
		}else{
			return;
		}
		if(node == null || currentNode == null){
			return;
		}
		if(!node.equals(currentNode)){
			return;
		}
		//dragging curve or show popup menu
		if(node.getLevel() == 3) {
			curveNodeAction(node, e);
		}
		//dragging section or show popup menu
		else if(node.getLevel() ==2) {
			trackNodeAction(node, e);
			//dragging droup or show popup menu
		}else if(node.getLevel() ==4){
			groupNodeAction(node, e);

		}
		else if(node.getLevel() == 1){
			windowNode = node;
			if(e.getButton()==MouseEvent.BUTTON3){
				JPopupMenu popup = new JPopupMenu();

				JMenuItem settingItem = new JMenuItem("Settings");
				popup.add(settingItem);
				popup.addSeparator();
				JMenuItem deleteItem = new JMenuItem("Delete");
				popup.add(deleteItem);
				popup.addSeparator();
				JMenuItem showItem = new JMenuItem("Show");
				popup.add(showItem);
				popup.show(tree, e.getX(), e.getY());

				settingItem.addActionListener(this);
				settingItem.setActionCommand("WindowSettings");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("WindowDelete");
				showItem.addActionListener(this);
				showItem.setActionCommand("WindowShow");
			}
		}else if(node.getLevel() == 0){
			//show template
			if(e.getButton()==MouseEvent.BUTTON3){
				JPopupMenu popup = new JPopupMenu();
				JMenuItem settingItem = new JMenuItem("Template");
				popup.add(settingItem);
				settingItem.addActionListener(this);
				settingItem.setActionCommand("template");
				popup.show(tree, e.getX(), e.getY());
			}
		}
		else{
			tree.clearNodeMayBeDragged();
		}
//		node with level equals 1 is a will log node
		if(node != null && node.getLevel() == 1)
		{
			ViewCoreOperator.selectFrame(node.getUserObject().toString());
		}

//		node with level equals 2 is a track node
		if(node != null && node.getLevel() == 2)
		{
			ViewCoreOperator.selectFrame(((DefaultMutableTreeNode)(node.getParent())).getUserObject().toString());
			ViewCoreOperator.selectTrack(((ViewTreeTrackNode)node.getUserObject()).getWellName());
		}

//		node with level equals 3 is a curve node
		if(node != null && node.getLevel() == 3 )
		{
			String wellLog =((DefaultMutableTreeNode)(node.getParent().getParent())).getUserObject().toString();
			trackNode = (DefaultMutableTreeNode)(node.getParent());
			String wellName = ((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
			ViewCoreOperator.selectFrame(wellLog);
			if(((ViewTreeNode) node.getUserObject()).getNodeType() == ViewTreeNode.CURVE_NODE){
				ViewCoreOperator.selectCurve(((ViewTreeNode)node.getUserObject()).getCurveHashmapKey(), wellName, true);
			}else{
				ViewCoreOperator.selectDepthOrGroup(wellName, node.getUserObject().toString());
			}
		}

		if(node != null && node.getLevel() == 4 )
		{
			String wellLog =((DefaultMutableTreeNode)(node.getParent().getParent().getParent())).getUserObject().toString();
			trackNode = (DefaultMutableTreeNode)(node.getParent().getParent());
			String wellName =((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
			String groupName =((ViewTreeNode)(((DefaultMutableTreeNode)node.getParent()).getUserObject())).toString();
			String curveName =((ViewTreeNode)node.getUserObject()).getCurveHashmapKey();
			ViewCoreOperator.selectFrame(wellLog);
			ViewCoreOperator.selectCurveUnderGroup(wellName,groupName,curveName);
		}
	}

	public void mouseReleased(MouseEvent e) {
		ViewTree tree = (ViewTree)e.getSource();
		tree.clearNodeMayBeDragged();
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	/**
	 * when mouse dragged, decide if will enable drag and drop
	 */
	public void mouseDragged(MouseEvent e) {
		DefaultMutableTreeNode nodeDragged = ((ViewTree)e.getSource()).getNodeDnd();
		if(nodeDragged != null && !nodeDragged.toString().equals("")) {
			JComponent c = (JComponent)e.getSource();
			TransferHandler handler = c.getTransferHandler();
			handler.exportAsDrag(c,e,TransferHandler.COPY_OR_MOVE);
		}
	}

	public void mouseMoved(MouseEvent e) {

	}

	/**
	 * @inheritDoc
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("WindowSettings")){
			EditFrameDialog windowPropertyDialog = new EditFrameDialog();
			windowPropertyDialog.setVisible(true);
		}else if(e.getActionCommand().equals("WindowDelete")){
			String n2=(String)windowNode.getUserObject();
			ViewCoreOperator.deleteFrame(DataBuffer.getJMainFrame(),n2);
		}else if(e.getActionCommand().equals("WindowShow")){
			String frameTitle=(String)windowNode.getUserObject();
			DesktopPaneView desktop=DataBuffer.getJMainFrame().getMainBuffer().getDesktopPaneView();
			ArrayList flist = desktop.getInternalFrameViewList();
			for (int i = 0; i < flist.size(); i++) {
				InternalFrameView internalFrameView = (InternalFrameView)flist.get(i);
				if (internalFrameView.getTitle().equals(frameTitle)) {
					if (!internalFrameView.isVisible())
						internalFrameView.setVisible(true);
					if (internalFrameView.isIcon()) {
						try {
							internalFrameView.setIcon(false);
						} catch (PropertyVetoException e1) {
						}
					}
				}
			}
			//new feature since 9/21/2006
		}else if(e.getActionCommand().equals("Create Group")){
			ViewCoreOperator.addGroupPanel();
		}else if(e.getActionCommand().equals("Create Description")){
			ViewCoreOperator.addDescriptionPanel();
		}else if(e.getActionCommand().equals("Create Seismic")){
			ViewCoreOperator.addSeismicPanel();
		}else if(e.getActionCommand().equals("Create Lithology")){
			ViewCoreOperator.addLithologyPanel();
		}else if(e.getActionCommand().equals("trackDelete")){
			String n2=((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
			ViewCoreOperator.deleteTrack(n2,DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView());
		}else if(e.getActionCommand().equals("trackSettings")){
			EditTrackDialog trackPropertyDialog = new EditTrackDialog();
			trackPropertyDialog.setVisible(true);
		}else if(e.getActionCommand().equals("descriptionPanelSettings")){
			if(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() == null) {
				return;
			}
			CurveView cv=DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer()
			.getSelectedAxisCurveContainer().getCurveView();
			String descriptionName = descriptionNode.getUserObject().toString();
			for(int i=0;i<cv.getDescriptions().size();i++){
				DescriptionPanel descriptionPanel = (DescriptionPanel)cv.getDescriptions().get(i);
				if(descriptionPanel.getName().equals(descriptionName)){
					EditDescriptionSettingDialog settingDlg = new EditDescriptionSettingDialog(descriptionPanel);
					settingDlg.setVisible(true);
					break;
				}
			}
		}else if(e.getActionCommand().equals("descriptionPanelDelete")){
			String descriptionName = descriptionNode.getUserObject().toString();
			trackNode = (DefaultMutableTreeNode)(descriptionNode.getParent());
			String wellName =((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
			ViewCoreOperator.removeDescriptionPanel(wellName, descriptionName);
		}else if(e.getActionCommand().equals("seismicPanelSettings")){
			if(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() == null) {
				return;
			}
			CurveView cv=DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer()
			.getSelectedAxisCurveContainer().getCurveView();
			String seismicName = seismicNode.getUserObject().toString();
			for(int i=0;i<cv.getSeismics().size();i++){
				SeismicPanel seismicPanel = (SeismicPanel)cv.getSeismics().get(i);
				if(seismicPanel.getName().equals(seismicName)){
					EditSeismicSettingDialog settingDlg = new EditSeismicSettingDialog(seismicPanel);
					settingDlg.setVisible(true);
					break;
				}
			}
		}else if(e.getActionCommand().equals("seismicPanelDelete")){
			String seismicName = seismicNode.getUserObject().toString();
			trackNode = (DefaultMutableTreeNode)(seismicNode.getParent());
			String wellName =((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
			ViewCoreOperator.removeSeismicPanel(wellName, seismicName);
		}else if(e.getActionCommand().equals("EditCurve")){
			CurveView cv=DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer()
			.getSelectedAxisCurveContainer().getCurveView();
			if(cv.getCurveLayer().getLayerType().equals(TrackTypeList.REALIZATION_TOP)){
				new EditCurveDialog(cv,DataBuffer.getJMainFrame()).setVisible(true);

			}else{
				new EditBesideCurveDialog(cv,DataBuffer.getJMainFrame()).setVisible(true);
			}
		}else if(e.getActionCommand().equals("EditGroupCurve")){
			CurveView cv=DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer()
			.getSelectedAxisCurveContainer().getCurveView();
			new EditCurveDialog(cv,DataBuffer.getJMainFrame()).setVisible(true);
		}else if(e.getActionCommand().equals("lithologyPanelSettings")){
			if(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() == null) {
				return;
			}
			CurveView cv=DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer()
			.getSelectedAxisCurveContainer().getCurveView();
			String lithologyName = lithologyNode.getUserObject().toString();
			for(int i=0;i<cv.getLithologys().size();i++){
				LithologyPanel lithologyPanel = (LithologyPanel)cv.getLithologys().get(i);
				if(lithologyPanel.getName().equals(lithologyName)){
					EditLithologySettingDialog settingDlg = new EditLithologySettingDialog(lithologyPanel);
					settingDlg.setVisible(true);
					break;
				}
			}
		}else if(e.getActionCommand().equals("lithologyPanelDelete")){
			String lithologyName = lithologyNode.getUserObject().toString();
			trackNode = (DefaultMutableTreeNode)(lithologyNode.getParent());
			String wellName =((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
			ViewCoreOperator.removeLithologyPanel(wellName, lithologyName);
		}else if(e.getActionCommand().equals("DeleteCurve")){
			ViewCoreOperator.deleteCurve(n1,n2);
		}else if(e.getActionCommand().equals("groupPanelSettings")){
			CurveView cv=DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
			.getTrackContainer().getSelectedAxisCurveContainer().getCurveView();
			CurveGroup group = null;
			String groupNodeName = ((ViewTreeNode)groupNode.getUserObject()).getCurveName();
			for(int i=0 ; i<cv.getPanelOrder().getOrders().size() ; i++){
				if(cv.getPanelOrder().getPanelKind(i) == PanelOrder.GROUP &&
						((CurveGroup)cv.getPanelOrder().getPanel(i)).getName().equals(groupNodeName)){
					group = (CurveGroup)cv.getPanelOrder().getPanel(i);
				}
			}
			EditGroupDialog editGroupDialog = new EditGroupDialog(cv , group);
			editGroupDialog.setVisible(true);
		}else if(e.getActionCommand().equals("groupPanelDelete")){
			String groupName =((ViewTreeNode)groupNode.getUserObject()).getCurveName();
			trackNode = (DefaultMutableTreeNode)(groupNode.getParent());
			String wellName =((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
	        ViewCoreOperator.removeGroup(wellName , groupName);
	        tree.removeCurve(groupNode);
	    // added by Alex
		}else if (e.getActionCommand().equals("defineAreasInGroup")){
			String groupHashkey = ((ViewTreeNode)groupNode.getUserObject()).getCurveHashmapKey();			
			EditGroupAreaDialog setAreaDialog = new EditGroupAreaDialog(groupHashkey);			
			setAreaDialog.setVisible(true);
		}else if (e.getActionCommand().equals("saveTemplate"))	{
			String wellLog = trackNode.getParent().toString();
			String trackName = trackNode.getUserObject().toString();
			
			ViewCoreOperator.saveWellTemplate(wellLog, trackName);
		}
	}

	private void curveNodeAction(DefaultMutableTreeNode node, MouseEvent e){
		tree.thisNodeMayBeDragged(node);
		trackNode = (DefaultMutableTreeNode)(node.getParent());
		if(((ViewTreeNode) node.getUserObject()).getNodeType() == ViewTreeNode.CURVE_NODE ){
			n1=((ViewTreeNode)node.getUserObject()).getCurveHashmapKey();
			n2=((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();

			// added by Alex
			if (e.getButton()==MouseEvent.BUTTON1)
				ViewCoreOperator.setDraggedCurve(ViewCoreOperator.getCurvePanelByName(n1.split(OperationClass.hashMapNameSeparator)[1],
						null, n2));

			ViewCoreOperator.selectCurve(n1,n2,!e.isControlDown());
			if(e.getButton()==MouseEvent.BUTTON3){
				JPopupMenu popup = new JPopupMenu();

				JMenuItem editItem = new JMenuItem("Settings");
				popup.add(editItem);

				JMenuItem deleteItem = new JMenuItem("Delete");
				popup.add(deleteItem);

				popup.show(tree, e.getX(), e.getY());

				editItem.addActionListener(this);
				editItem.setActionCommand("EditCurve");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("DeleteCurve");

		    }
		}
		if(((ViewTreeNode) node.getUserObject()).getNodeType() == ViewTreeNode.GROUP_PANEL_NODE ){
			if(e.getButton()==MouseEvent.BUTTON3){
				groupNode = (DefaultMutableTreeNode)node;
				if(e.getButton()==MouseEvent.BUTTON3){
					JPopupMenu popup = new JPopupMenu();
					JMenuItem settingItem = new JMenuItem("Settings");
					popup.add(settingItem);
					JMenuItem deleteItem = new JMenuItem("Delete");
					popup.add(deleteItem);
					
					// added by Alex
					JMenuItem defAreaItem = new JMenuItem("Define Areas");
					String groupHashkey = trackNode.toString().concat(OperationClass.hashMapNameSeparator).concat(groupNode.toString());					
					defAreaItem.setEnabled(ViewCoreOperator.thisGroupHasCurves(groupHashkey));
					popup.add(defAreaItem);
					
					popup.show(tree, e.getX(), e.getY());

					settingItem.addActionListener(this);
					settingItem.setActionCommand("groupPanelSettings");
					deleteItem.addActionListener(this);
					deleteItem.setActionCommand("groupPanelDelete");
					defAreaItem.addActionListener(this);
					defAreaItem.setActionCommand("defineAreasInGroup");
				}
			}
		}
		if(((ViewTreeNode) node.getUserObject()).getNodeType() == ViewTreeNode.DESCRIPTION_PANEL_NODE ){
			descriptionNode = (DefaultMutableTreeNode)node;
			if(e.getButton()==MouseEvent.BUTTON3){
				JPopupMenu popup = new JPopupMenu();
				JMenuItem settingItem = new JMenuItem("Settings");
				popup.add(settingItem);
				JMenuItem deleteItem = new JMenuItem("Delete");
				popup.add(deleteItem);
				popup.show(tree, e.getX(), e.getY());

				settingItem.addActionListener(this);
				settingItem.setActionCommand("descriptionPanelSettings");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("descriptionPanelDelete");
			}
		}
		if(((ViewTreeNode) node.getUserObject()).getNodeType() == ViewTreeNode.SEISMIC_PANEL_NODE ){
			seismicNode = (DefaultMutableTreeNode)node;
			if(e.getButton()==MouseEvent.BUTTON3){
				JPopupMenu popup = new JPopupMenu();
				JMenuItem settingItem = new JMenuItem("Settings");
				popup.add(settingItem);
				JMenuItem deleteItem = new JMenuItem("Delete");
				popup.add(deleteItem);
				popup.show(tree, e.getX(), e.getY());

				settingItem.addActionListener(this);
				settingItem.setActionCommand("seismicPanelSettings");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("seismicPanelDelete");
			}
		}
		if(((ViewTreeNode) node.getUserObject()).getNodeType() == ViewTreeNode.LITHOLOGY_PANEL_NODE ){
			lithologyNode = (DefaultMutableTreeNode)node;
			if(e.getButton()==MouseEvent.BUTTON3){
				JPopupMenu popup = new JPopupMenu();
				JMenuItem settingItem = new JMenuItem("Settings");
				popup.add(settingItem);
				JMenuItem deleteItem = new JMenuItem("Delete");
				popup.add(deleteItem);
				popup.show(tree, e.getX(), e.getY());

				settingItem.addActionListener(this);
				settingItem.setActionCommand("lithologyPanelSettings");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("lithologyPanelDelete");
			}
		}
	}

	private void trackNodeAction(DefaultMutableTreeNode node, MouseEvent e){
		trackNode=node;
		// added by Alex. June 16, 2006
		if (e.getButton()==MouseEvent.BUTTON1)
			tree.thisNodeMayBeDragged(node);

		else if(e.getButton()==MouseEvent.BUTTON3){
			JPopupMenu popup = new JPopupMenu();

			JMenuItem settingItem = new JMenuItem("Settings");
			popup.add(settingItem);

			popup.addSeparator();

			JMenuItem createGroupItem = new JMenuItem("Create Group");
			popup.add(createGroupItem);

			JMenuItem createDescriptionItem = new JMenuItem("Create "+
					ConstantVariableList.DESCRIPTION_PANEL);
			popup.add(createDescriptionItem);

			JMenuItem createSeismicItem = new JMenuItem("Create "+
					ConstantVariableList.SEISMIC_PANEL);
			popup.add(createSeismicItem);

			JMenuItem createLithology = new JMenuItem("Create "+
					ConstantVariableList.LITHOLOGY_PANEL );
			popup.add(createLithology);
			
			popup.addSeparator();
			
			JMenuItem saveTemplateItem = new JMenuItem("Save Template");
			popup.add(saveTemplateItem);

			popup.addSeparator();

			JMenuItem deleteItem = new JMenuItem("Delete");
			popup.add(deleteItem);


			popup.show(tree, e.getX(), e.getY());

			settingItem.addActionListener(this);
			settingItem.setActionCommand("trackSettings");
			createGroupItem.addActionListener(this);
			createGroupItem.setActionCommand("Create Group");
			createDescriptionItem.addActionListener(this);
			createDescriptionItem.setActionCommand("Create Description");
			createSeismicItem.addActionListener(this);
			createSeismicItem.setActionCommand("Create Seismic");
			createLithology.addActionListener(this);
			createLithology.setActionCommand("Create Lithology");
			saveTemplateItem.addActionListener(this);
			saveTemplateItem.setActionCommand("saveTemplate");
			deleteItem.addActionListener(this);
			deleteItem.setActionCommand("trackDelete");
		}
	}

	private void groupNodeAction(final DefaultMutableTreeNode node, MouseEvent e){
		trackNode = (DefaultMutableTreeNode)(node.getParent().getParent());

		// added by Alex. June 28, 2006
		if (e.getButton()==MouseEvent.BUTTON1)	{
			tree.thisNodeMayBeDragged(node);

			// added by Alex
			ViewCoreOperator.setDraggedCurve(
					ViewCoreOperator.getCurvePanelByName(((ViewTreeNode)node.getUserObject()).getCurveHashmapKey().split(
							OperationClass.hashMapNameSeparator)[1],
							((ViewTreeNode)((DefaultMutableTreeNode)(node.getParent())).getUserObject()).getCurveName(),
							((ViewTreeTrackNode)trackNode.getUserObject()).getWellName()));
		}

		if(((ViewTreeNode) node.getUserObject()).getNodeType() == ViewTreeNode.CURVE_NODE ){

			if(e.getButton()==MouseEvent.BUTTON3){
			JPopupMenu popup = new JPopupMenu();

			JMenuItem settingItem = new JMenuItem("Settings");
			popup.add(settingItem);

			JMenuItem deleteItem = new JMenuItem("Delete");
			popup.add(deleteItem);

			popup.show(tree, e.getX(), e.getY());

			settingItem.addActionListener(this);
			settingItem.setActionCommand("EditGroupCurve");
			deleteItem.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					String curveName =((ViewTreeNode)node.getUserObject()).getCurveHashmapKey().split(
							OperationClass.hashMapNameSeparator)[1];
					String groupName =((ViewTreeNode)((DefaultMutableTreeNode)(node.getParent())).getUserObject()).getCurveName();
					String wellName =((ViewTreeTrackNode)trackNode.getUserObject()).getWellName();
					tree.removeCurve(node);
					ViewCoreOperator.removeCurveFromGroup(wellName , groupName , curveName);
				}
			});
			}
		}
	}

}
