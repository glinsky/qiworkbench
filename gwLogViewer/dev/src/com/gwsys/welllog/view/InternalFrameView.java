/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.TrackContainerView;

/**
 * Implements window to hold all tracks.
 * @author Team
 *
 */
public class InternalFrameView extends JInternalFrame implements ComponentListener{

	private static final long serialVersionUID = 1L;

	static int openFrameCount = 0; // set a signal , to mark it's order

	private JScrollPane isp = null;

	private JScrollPane jScrollPane = null; // internal scrollpane

	private TrackContainerView trackContainer = null;

	private MainFrameView jMainFrame = null;

	private String verticalMeasurement = ConstantVariableList.MEASUREMENT_MD;

	private int depthScale = ConstantVariableList.DEPTHSCALE_1000;

	private boolean setVerticalMeasurement = false, setDepthScale = false;

	private boolean isPaint = true;

	private float minVerticalValue, maxVerticalValue;
	/**
	 *
	 * @param jMainFrame
	 * @param title
	 */
	public InternalFrameView(MainFrameView parent, String title) {
		super(title,
				true, // resizable
				true, // closable
				true, // maximizable
				true);// iconifiable

		this.jMainFrame = parent;
		this.setFrameIcon(IconResource.getInstance().getImageIcon("LogViewerLogo.gif"));
		this.addComponentListener(this);
		this.addInternalFrameListener(new InternalFrameListener() {

			public void internalFrameOpened(InternalFrameEvent arg0) {
			}

			public void internalFrameClosing(InternalFrameEvent arg0) {
			}

			public void internalFrameClosed(InternalFrameEvent arg0) {
				//User asks that Not remove
				/*/ must remove the respective well log node in the view tree
				jMainFrame.getMainBuffer().getViewTree().removeWellLog(InternalFrameView.this.getTitle());
				// delete DesktopPaneView's InternalFrameViewList's respective
				// object
				jMainFrame.getMainBuffer().getDesktopPaneView().getInternalFrameViewList()
						.remove(InternalFrameView.this);
				openFrameCount--;
				if(openFrameCount == 0) {
					getJMainFrame().getMainBuffer().setInternalFrameView(null);
				}*/
			}

			public void internalFrameIconified(InternalFrameEvent arg0) {

			}

			public void internalFrameDeiconified(InternalFrameEvent arg0) {

			}

			public void internalFrameActivated(InternalFrameEvent arg0) {
				getJMainFrame().getMainBuffer().setInternalFrameView(InternalFrameView.this);
				ViewCoreOperator.updateProjectTreeSelect();
				if(getTrackContainer().getSelectedAxisCurveContainer() != null){
					getTrackContainer().getSelectedAxisCurveContainer().setSelected();
				}
				//added by Alex. July 19, 2006
				setPaint(false);
				ViewCoreOperator.synchroComboBoxes((InternalFrameView) arg0.getSource());
				setPaint(true);
			}

			public void internalFrameDeactivated(InternalFrameEvent arg0) {

			}

		});
		if (isp == null) {
			isp = new JScrollPane();
			isp.setViewportView(this.getTrackContainer());
		}
		isp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		isp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		this.setContentPane(isp);
		// ...Then set the window size or call pack...
		this.setSize(ConstantVariableList.INTERNALFRAMEVIEW_SIZE);
		// Set the window's location.
		setLocation(ConstantVariableList.X_OFFSET * openFrameCount,
				ConstantVariableList.Y_OFFSET * openFrameCount);
		openFrameCount++;
		this.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
	}
	public InternalFrameView(MainFrameView jMainFrame) {
		this(jMainFrame,LogWinUtility.generateNewFrameTitle());
	}

	public void setTrackType(){
		String type = TrackTypeList.Y_DEPTH_MD;
		if(this.verticalMeasurement.equals(ConstantVariableList.MEASUREMENT_TVD)){
			type = TrackTypeList.Y_DEPTH_TVD ;
		}else if(this.verticalMeasurement.equals(ConstantVariableList.MEASUREMENT_TVDSS )){
			type = TrackTypeList.Y_DEPTH_TVDSS ;
		}

		ArrayList tracks = this.getTrackContainer().getAxisCurveContainerList();
		for(int i = 0 ; i < tracks.size(); i++){
			AxisCurveContainerView track = (AxisCurveContainerView)tracks.get(i);
			track.getYAxisView().setYType(type);
			track.getCurveView().setFillRedrawFlag(true);
			track.getCurveView().setMarkerRedrawFlag(true);
			track.getCurveView().repaint();
			track.getParentContainer().repaint();

		}
	}

	public Element saveXML(Document doc, Integer id){
		Element xmlInternalFrameView = doc.createElement("InternalFrameView");
		xmlInternalFrameView.setAttribute("id",Integer.toString(id));
    	xmlInternalFrameView.setAttribute("title", this.getTitle());
    	xmlInternalFrameView.setAttribute("locationX",Integer.toString(this.getX()));
    	xmlInternalFrameView.setAttribute("locationY",Integer.toString(this.getY()));
    	xmlInternalFrameView.setAttribute("width",Integer.toString(this.getWidth()));
    	xmlInternalFrameView.setAttribute("height",Integer.toString(this.getHeight()));
    	xmlInternalFrameView.setAttribute("verticalMeasurement", this.getVerticalMeasurement());
    	xmlInternalFrameView.setAttribute("depthScale", Integer.toString(this.getDepthScale()));
    	xmlInternalFrameView.setAttribute("setVerticalMeasurement", Boolean.toString(this.isSetVerticalMeasurement()));
    	xmlInternalFrameView.setAttribute("setDepthScale", Boolean.toString(this.isVerticalScaleSet()));
    	boolean activeWindow = false;
		if (this.jMainFrame.getMainBuffer().getInternalFrameView()!=null){
			String windowTitle = this.jMainFrame.getMainBuffer().getInternalFrameView().getTitle();
			if(this.getTitle().equals(windowTitle)){
				activeWindow = true;
			}
		}
		xmlInternalFrameView.setAttribute("activeWindow", Boolean.toString(activeWindow));

		xmlInternalFrameView.appendChild(this.getTrackContainer().saveXML(doc));

		return xmlInternalFrameView;
	}

	@SuppressWarnings("unchecked")
	public void loadXML(Element xmlInternalFrameView){
		this.setTitle(xmlInternalFrameView.getAttribute("title"));
		this.setLocation(Integer.parseInt(xmlInternalFrameView.getAttribute("locationX")),
				Integer.parseInt(xmlInternalFrameView.getAttribute("locationY")));
		this.setSize(Integer.parseInt(xmlInternalFrameView.getAttribute("width")),
				Integer.parseInt(xmlInternalFrameView.getAttribute("height")));
		String xmlVerticalMeasurement = xmlInternalFrameView.getAttribute("verticalMeasurement");
		if (verticalMeasurement != null) {
			this.setVerticalMeasurement(xmlVerticalMeasurement);
		}
		String xmlDepthScale = xmlInternalFrameView.getAttribute("depthScale");
		if (xmlDepthScale != "") {
			this.setDepthScale(Integer.parseInt(xmlDepthScale));
		}
		String xmlSetVerticalMeasurement = xmlInternalFrameView
				.getAttribute("setVerticalMeasurement");
		if (xmlSetVerticalMeasurement != null) {
			this.enableVerticalMeasurement(ConversionClass.stringToBoolean(xmlSetVerticalMeasurement));
		}
		String xmlSetDepthScale = xmlInternalFrameView.getAttribute("setDepthScale");
		if (xmlSetDepthScale != null) {
			this.enableVerticalScale(ConversionClass.stringToBoolean(xmlSetDepthScale));
		}

		Element xmlTrackContainerView = (Element) xmlInternalFrameView.getElementsByTagName("TrackContainerView").item(0);
		this.getTrackContainer().loadXML(xmlTrackContainerView);
	}

	/**
	 *
	 * @return The TrackContainerView
	 */
	public TrackContainerView getTrackContainer() {
		if (trackContainer == null)
			trackContainer = new TrackContainerView(this);
		return trackContainer;
	}

	public JScrollPane getJScrollPane() {
		return jScrollPane;
	}

	public void setJScrollPane(JScrollPane scrollPane) {
		jScrollPane = scrollPane;
	}

	public JScrollPane getIsp() {
		return isp;
	}

	public void setIsp(JScrollPane isp) {
		this.isp = isp;
	}

	public void repaintTrack(){
		//System.out.println("enter repaint");
		trackContainer.updateAxisCurveContainersLocation(this.getHeight() - 48);
		//System.out.println("height=:" + this.getHeight());
	}

	public void componentResized(ComponentEvent e){
		repaintTrack();
	}

	public void componentHidden(ComponentEvent arg0) {

	}
	public void componentMoved(ComponentEvent arg0) {

	}
	public void componentShown(ComponentEvent arg0) {

	}
	public MainFrameView getJMainFrame() {
		return jMainFrame;
	}
	public int getDepthScale() {
		return depthScale;
	}
	public void setDepthScale(int depthScale) {
		this.depthScale = depthScale;
	}
	public String getVerticalMeasurement() {
		return verticalMeasurement;
	}
	public void setVerticalMeasurement(String verticalMeasurement) {
		this.verticalMeasurement = verticalMeasurement;
	}
	public boolean isVerticalScaleSet() {
		return setDepthScale;
	}
	public void enableVerticalScale(boolean setDepthScale) {
		this.setDepthScale = setDepthScale;
	}
	public boolean isSetVerticalMeasurement() {
		return setVerticalMeasurement;
	}
	public void enableVerticalMeasurement(boolean setVerticalMeasurement) {
		this.setVerticalMeasurement = setVerticalMeasurement;
	}
	public boolean isPaint() {
		return isPaint;
	}
	public void setPaint(boolean isPaint) {
		this.isPaint = isPaint;
	}

	public void setMinVerticalValue(float value){
		minVerticalValue = value;
	}

	public void setMaxVerticalValue(float value){
		maxVerticalValue = value;
	}

	public float getMinVerticalValue(){
		return minVerticalValue;
	}

	public float getMaxVerticalValue(){
		return maxVerticalValue;
	}
}
