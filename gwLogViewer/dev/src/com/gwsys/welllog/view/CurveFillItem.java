/**
 * 
 */
package com.gwsys.welllog.view;

import com.gwsys.welllog.view.fill.CompoundFill;
import com.gwsys.welllog.view.fill.CurveFillBeside;

/**
 * @author Alex
 *
 */
public class CurveFillItem {
	private String curve;
	private String orientation;
	private int fillColor;
	private boolean singleFill;
	private boolean intoGroup;
	
	// for group areas
	private String group;
	private String secondCurve;
	
	public CurveFillItem(String curve)	{
		this.curve = curve;
	}
	
	public CurveFillItem(CurveFillBeside cfb)	{
		this.curve = cfb.getCp().getCd().getCurveName();
		this.fillColor = cfb.getFillColor().getRGB();
		this.orientation = cfb.getLeftOrRight();
		this.singleFill = true;
		this.intoGroup = cfb.isFillIntoGroup();
		
		if (this.isIntoGroup())
			this.group = cfb.getCp().getCg().getName();
		
	}
	
	public CurveFillItem(CompoundFill cf)	{
		this.curve = cf.getLeftCurve().getCd().getCurveName();
		this.secondCurve = cf.getRightCurve().getCd().getCurveName();
		this.singleFill = false;
		this.fillColor = cf.getFillColor().getRGB();
		this.intoGroup = true;
		this.group = cf.getGroupName();
	}
	
	public String getCurve()	{
		return this.curve;
	}
	
	public String getOrientation()	{
		return this.orientation;
	}
	
	public void setOrientation(String orientation)	{
		this.orientation = orientation;
	}
	
	public int getFillColor()	{
		return this.fillColor;
	}
	
	public void setFillColor(int color)	{
		this.fillColor = color;
	}
	
	public boolean isSingleFill()	{
		return this.singleFill;
	}
	
	public void setFillSingleType(boolean value)	{
		this.singleFill = value;
	}
	
	public boolean isIntoGroup()	{
		return this.intoGroup;
	}
	
	public void setCurveIntoGroup(boolean value)	{
		this.intoGroup = value;
	}
	
	public String getSecondCurve()	{
		return this.secondCurve;
	}
	
	public void setSecondCurve(String curve)	{
		this.secondCurve = curve;
	}
	
	public String getGroup()	{
		return this.group;
	}
	
	public void setGroup(String group)	{
		this.group = group;
	}
}
