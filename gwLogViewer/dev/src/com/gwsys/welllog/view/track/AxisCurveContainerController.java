/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view.track;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.gwsys.welllog.view.ViewCoreOperator;

public class AxisCurveContainerController implements MouseListener, MouseMotionListener {

	private AxisCurveContainerView axisCurveContainer = null;

	private TrackContainerView trackContainer = null;

	private int w = 0;

	private int h = 0;
	
	private boolean isDragging = false;

	/**
	 * 
	 * @param axisCurveContainer
	 *            The axisCurveContainer which to be operated
	 * @param trackContainer
	 *            The trackContainer which to be operated
	 */
	public AxisCurveContainerController(AxisCurveContainerView axisCurveContainer, TrackContainerView trackContainer) {
		this.axisCurveContainer = axisCurveContainer;
		this.trackContainer = trackContainer;
		w = axisCurveContainer.getWidth();
		h = axisCurveContainer.getHeight();
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		if(isDragging == false) {
			axisCurveContainer.setCursor(Cursor.getDefaultCursor());
		}
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		if(!((AxisCurveContainerView)e.getSource()).isIsSelected()) {
			ViewCoreOperator.selectTrack(((AxisCurveContainerView)e.getSource()).getWellName());			
		}
		int type = axisCurveContainer.getCursor().getType();
		if(type != Cursor.DEFAULT_CURSOR) {
			isDragging = true;
		}		
	}

	public void mouseReleased(MouseEvent e) {
		isDragging = false;
		axisCurveContainer.setCursor(Cursor.getDefaultCursor());

		// change size
		trackContainer.getSelectedAxisCurveContainer().getCurveView().setPreferredSize(trackContainer.getSelectedAxisCurveContainer().getCurveView().getSize());
		//trackContainer.setAxisCurveContainerMaxHeight(h);
		
		trackContainer.updateAxisCurveContainersLocation();
		trackContainer.revalidate();
		trackContainer.repaint();
	}

	public void mouseDragged(MouseEvent e) {
		w = axisCurveContainer.getWidth();
		h = axisCurveContainer.getHeight();
		int type = axisCurveContainer.getCursor().getType();
		switch (type) {
		case Cursor.E_RESIZE_CURSOR://
			w = e.getX();
			axisCurveContainer.setSize(w, h);
			break;
		case Cursor.W_RESIZE_CURSOR:
			w = w - e.getX();
			axisCurveContainer.setSize(w, h);
			break;
		case Cursor.S_RESIZE_CURSOR:
			h = e.getY();
			axisCurveContainer.setSize(w, h);
			break;
		default:
			break;
		}

	}

	public void mouseMoved(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		Dimension currentSize = axisCurveContainer.getSize();
		if (x >= currentSize.width - 1 && x <= currentSize.width) {
			axisCurveContainer.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
			return;
		}
		// else if (x >= 0 && x <= 1) {
		// axisCurveContainer.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
		// return;
		//		}
		else if (y >= currentSize.height - 1 && y <= currentSize.height) {
			axisCurveContainer.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
			return;
		} else {
			axisCurveContainer.setCursor(Cursor.getDefaultCursor());
			return;
		}
	}

}
