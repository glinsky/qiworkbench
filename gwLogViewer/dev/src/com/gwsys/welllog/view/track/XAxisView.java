/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.NumberFormat;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.DescriptionPanel;
import com.gwsys.welllog.view.curve.LithologyPanel;
import com.gwsys.welllog.view.curve.PanelOrder;
import com.gwsys.welllog.view.curve.SeismicPanel;

/**
 * This is a component that shows name and values of curve.
 *
 * @author gaofei
 */
public class XAxisView extends JPanel implements MouseListener {
	private static final long serialVersionUID = 1L;

	private AxisCurveContainerView parent = null;

	private JPanel pHeight;

	/**
	 * Creates the XAxisView object.
	 * @param parent AxisCurveContainerView.
	 */
	public XAxisView(AxisCurveContainerView parent) {
		this.parent = parent;
		this.setBackground(ConstantVariableList.XAXISVIEW_BACKGROUND);
		this.setBorder(new LineBorder(ConstantVariableList.XAXISVIEW_BORDERCOLOR, 1));
		this.setPreferredSize(ConstantVariableList.XAXISVIEW_TOPTYPE_PREFERREDSIZE);
		this.addMouseListener(this);
		pHeight = new JPanel();
		pHeight.setLayout(null);
		pHeight.setVisible(false);
		this.add(pHeight);
	}

	/**
	 * @inheritDoc
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g.create();

		pHeight.setBounds(0,0,this.getWidth(),this.getHeight());

		print(g2,0, 1, 1);
	}

	public void print(Graphics2D g2, int left, float rateWidth, float rateHeight){
		PanelOrder panelOrder = parent.getCurveView().getPanelOrder();
		for(int i=0 ; i<panelOrder.getOrders().size() ; i++){
			if(panelOrder.getPanelKind(i) == PanelOrder.YAXIS){
				drawYHead(g2,panelOrder.getPanelLeft(panelOrder.getPanel(i)), rateWidth, rateHeight);
			}else if(panelOrder.getPanelKind(i) == PanelOrder.CURVE){
				drawCurveHead(g2 , (CurvePanel)panelOrder.getPanel(i) , panelOrder.getPanelLeft(panelOrder.getPanel(i)) , 0 , this.getHeight(),rateWidth,rateHeight);
			}else if(panelOrder.getPanelKind(i) == PanelOrder.DESCRIPTION){
				drawPanelHead(g2 , (DescriptionPanel)panelOrder.getPanel(i) , panelOrder.getPanelLeft(panelOrder.getPanel(i)),rateWidth, rateHeight);
			}else if(panelOrder.getPanelKind(i) == PanelOrder.SEISMIC){
				drawPanelHead(g2 , (SeismicPanel)panelOrder.getPanel(i) , panelOrder.getPanelLeft(panelOrder.getPanel(i)),rateWidth, rateHeight);
			}else if(panelOrder.getPanelKind(i) == PanelOrder.LITHOLOGY){
				drawPanelHead(g2 , (LithologyPanel)panelOrder.getPanel(i) , panelOrder.getPanelLeft(panelOrder.getPanel(i)),rateWidth, rateHeight);
			}else if(panelOrder.getPanelKind(i) == PanelOrder.GROUP){
				drawGroupHead(g2 , (CurveGroup)panelOrder.getPanel(i) , panelOrder.getPanelLeft(panelOrder.getPanel(i)),rateWidth,rateHeight);
			}
		}
	}

	private void drawCurveHead(Graphics2D g2 , CurvePanel cp ,
			int left , int top , int height, float rateWidth, float rateHeight){
		left=(int)(left*rateWidth);
		top = (int)(top*rateHeight);
		height = (int)(height*rateHeight);
		int width = (int)(cp.getBesideTypeWidth()*rateWidth);
		int mergeLeft = (int)(3*rateWidth);
		int mergeTop = (int)(12*rateHeight);
		g2.setColor(ConstantVariableList.XAXISVIEW_BORDERCOLOR);
		g2.drawRect(left , top , width , height);

		g2.setColor(cp.getCurveColor());
		if (cp.isSelected()){
			g2.setColor(Color.RED);
		}

		NumberFormat dformat = NumberFormat.getNumberInstance();
		dformat.setGroupingUsed(false);
		double range=Math.abs(cp.getMinXDisplay()-cp.getMaxXDisplay());
		if (range > 10) {
			dformat.setMaximumFractionDigits(0);
		} else if (range > 1) {
			dformat.setMaximumFractionDigits(1);
		} else {
			dformat.setMaximumFractionDigits(2);
		}

		String max = dformat.format(cp.getMaxXDisplay());
		String min = dformat.format(cp.getMinXDisplay());

		int minValue = g2.getFontMetrics().stringWidth(min);
		int maxValue = g2.getFontMetrics().stringWidth(max);
		if (cp.isOrderDesc()) {
			g2.drawString(max , left+mergeLeft , top + mergeTop);

			g2.drawString(min , left + width	- minValue -2 , top + mergeTop);
		} else {
			g2.drawString(min , left+mergeLeft , top + mergeTop);

			g2.drawString(max , left + width	- maxValue -2 , top + mergeTop);
		}
		//draw name
		int nameValue= g2.getFontMetrics().stringWidth(cp.getCd().getCurveName());
		int middle = (int)((cp.getBesideTypeWidth() - nameValue)*rateWidth / 2);
		g2.drawString(cp.getCd().getCurveName() ,  left + middle,
				top + mergeTop);
		//draw unit
		nameValue= g2.getFontMetrics().stringWidth(cp.getCd().getCurveUnit());
		middle = (int)((cp.getBesideTypeWidth() - nameValue)*rateWidth / 2);
		g2.drawString(cp.getCd().getCurveUnit() , left + middle , top +
				ConstantVariableList.GROUP_SINGLE_HEIGHT*rateHeight);

	}

    public void drawYHead(Graphics2D g2, int left, float rateWidth, float rateHeight){
    	left = (int)(left*rateWidth);
    	g2.setColor(ConstantVariableList.XAXISVIEW_BORDERCOLOR);
		g2.drawRect(left, 0 , (int)(ConstantVariableList.YAXISVIEW_WIDTH*rateWidth) ,
				(int)(this.getHeight()*rateHeight));

		g2.setColor(Color.BLACK);
    	FontMetrics fontMetrics = g2.getFontMetrics();
		int meterLabelWidth = fontMetrics.stringWidth("Meter");
		int secondLabelWidth = fontMetrics.stringWidth("Second");
		int feetLabelWidth = fontMetrics.stringWidth("Foot");
		int mdLabelWidth = fontMetrics.stringWidth("MD");
		int tvdLabelWidth = fontMetrics.stringWidth("TVD");
		int tvdssLabelWidth = fontMetrics.stringWidth("TVDSS");
		int height = fontMetrics.getHeight()+1;
		YAxisView yAxisView = parent.getYAxisView();
		if(yAxisView.getYType().equals(TrackTypeList.Y_TIME)) {
			g2.drawString("Time", left + (int)((yAxisView.getWidth() -
					secondLabelWidth)*rateWidth / 2), height);
			g2.drawString("Second", left + (int)((yAxisView.getWidth() -
					secondLabelWidth)*rateWidth / 2), 2* height);
		}else if (yAxisView.getYType().equals(TrackTypeList.Y_DEPTH_MD)) {
			g2.drawString("MD", left + (int)((yAxisView.getWidth() -
					mdLabelWidth) *rateWidth / 2), height);
			if(parent.getDepthUnit() == CurveData.FOOT)
				g2.drawString("Foot",left + (int)((yAxisView.getWidth() -
						feetLabelWidth)*rateWidth / 2) , 2* height);
			else
				g2.drawString("Meter",left + (int)((yAxisView.getWidth() -
						meterLabelWidth)*rateWidth / 2),	2* height);
		}else if (yAxisView.getYType().equals(TrackTypeList.Y_DEPTH_TVD)) {
			g2.drawString("TVD", left + (int)((yAxisView.getWidth() -
					tvdLabelWidth)*rateWidth / 2), height);
			if(parent.getDepthUnit() == CurveData.FOOT)
				g2.drawString("Foot",left + (int)((yAxisView.getWidth() -
						feetLabelWidth)*rateWidth / 2 ), 2* height);
			else
				g2.drawString("Meter",left + (int)((yAxisView.getWidth() -
						meterLabelWidth) * rateWidth / 2 ), 2* height);
		}else if (yAxisView.getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)) {
			g2.drawString("TVDSS", left + (int)((yAxisView.getWidth() -
					tvdssLabelWidth) * rateWidth / 2), height);
			if(parent.getDepthUnit() == CurveData.FOOT)
				g2.drawString("Foot",left + (int)((yAxisView.getWidth() -
						feetLabelWidth) * rateWidth / 2 ), 2* height);
			else
				g2.drawString("Meter",left + (int)((yAxisView.getWidth() -
						meterLabelWidth)*rateWidth / 2) , 2* height);
		}
	}

    private void drawGroupHead(Graphics2D g2 , CurveGroup group ,
    		int left, float rateWidth, float rateHeight){
        // border
    	g2.setColor(ConstantVariableList.XAXISVIEW_BORDERCOLOR);
    	g2.drawRect((int)(left*rateWidth), 0 , (int)(group.getBesideTypeWidth()*rateWidth),
    			(int)(this.getHeight()*rateHeight));

    	if (group.getCurveList().size() != 0) {
    		int singleHeight = this.getHeight()/group.getCurveList().size();
    		if(singleHeight < ConstantVariableList.GROUP_SINGLE_HEIGHT){
    			singleHeight = ConstantVariableList.GROUP_SINGLE_HEIGHT;
    		}
			for (int i = 0; i < group.getCurveList().size(); i++) {
				drawCurveHead(g2 , (CurvePanel)group.getCurveList().get(i) ,
						left , singleHeight*i , singleHeight, rateWidth, rateHeight);
			}
		}
    }

    private void drawPanelHead(Graphics2D g2 , JPanel panel ,
    		int left, float rateWidth, float rateHeight){
    	// border
    	g2.setColor(ConstantVariableList.XAXISVIEW_BORDERCOLOR);
    	g2.drawRect((int)(left*rateWidth) , 0 , (int)(panel.getWidth()*rateWidth),
    			(int)(this.getHeight()*rateHeight)+1);
    	int mergeLeft = (int)((left+10)*rateWidth);
    	int top = (int)(ConstantVariableList.GROUP_SINGLE_HEIGHT*rateHeight + 1);
    	g2.setColor(Color.BLACK);
    	if(panel instanceof DescriptionPanel)
    		g2.drawString(((DescriptionPanel)panel).getName(),mergeLeft , top);
    	else if(panel instanceof SeismicPanel)
    		g2.drawString(((SeismicPanel)panel).getName(),mergeLeft , top);
    	else if(panel instanceof LithologyPanel)
    		g2.drawString(((LithologyPanel)panel).getName(),mergeLeft , top);
    }

	public void mouseClicked(MouseEvent e) {
		PanelOrder panelOrder = parent.getCurveView().getPanelOrder();

		CurvePanel cp = null;
		for(int i=0 ; i<panelOrder.getOrders().size() ; i++){
			if(panelOrder.getPanelKind(i) == PanelOrder.CURVE){
				if(e.getX() > panelOrder.getPanelLeft(panelOrder.getPanel(i)) && e.getX() < panelOrder.getPanelRight(panelOrder.getPanel(i))){
					cp = (CurvePanel)panelOrder.getPanel(i);
					break;
				}
			}
		}
		if(cp != null){
			String curveName = cp.getCd().getHashCurveName();
			String wellName = parent.getWellName();
			ViewCoreOperator.selectCurve(curveName , wellName , !e.isControlDown());
		}
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

	public AxisCurveContainerView getParent() {
		return parent;
	}

}
