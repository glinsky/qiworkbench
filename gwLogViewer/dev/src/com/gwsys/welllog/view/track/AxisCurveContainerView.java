/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.mainframe.ZoneListener;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurveLayer;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.MarkerLayer;
import com.gwsys.welllog.view.curve.PanelOrder;
import com.gwsys.welllog.view.curve.Persistence;
import com.gwsys.welllog.view.yaxis.DepthConverter;
import com.gwsys.welllog.view.yaxis.DepthToTimeConverter;

/**
 * include YAxisView,xAxisView,CurveView
 *
 * @author gaofei
 */
public class AxisCurveContainerView extends JSplitPane implements
		WindowConstants {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private float velocityValue = ConstantVariableList.VELOCITY_VALUE; // default

	private TrackContainerView parentContainer = null; // parent container

	// mouseListener
	private AxisCurveContainerController axisCurveContainerController = null; // the

	private DepthConverter timeToDepthConverter = null;

	private DepthConverter mDDepthConverter = null;

	private DepthToTimeConverter depthToTimeConverter = null;

	private int depthUnit = 0;

	private String checkShotFileName ="";

	private JPanel jp = null;

	private boolean isSelected = false; // selected flag

	private CurveLayer curveLayer = null;

	private MarkerLayer markerLayer = null;

	private YAxisView yAxisView = null; // y axis

	private CurveView curveView = null; // curve container

	private GridLayer gridLayer = null;

	private XAxisView xAxisViewPanel = null;// jpanel which includes xaxis

	private JSplitPane topScrollPane = null;

	private JScrollPane titleAndCurveNamesPanel = null; // jscrollpane includes

	// xjpanel

	private JScrollPane bottomScrollPane; // bottom

	private Point dragPoint = new Point(0, 0);

//	private Color[] curveColorList = ConstantVariableList.CURVE_COLORLIST;
	private Color curveDefaultColor = ConstantVariableList.CURVE_DEFAULT_COLOR;

	// 2006-01-03
	private String trackName = null;

	private String trackID = null;

	private String wellName = null;

	private int besideWidth;

	private boolean isShowCurve = true;

	private boolean visibled = true;

	private boolean hasWell = false;

	private int trackType = ConstantVariableList.TRACK_TYPE_NONE;

	private JLabel jlb = null;

	private float depthScale = ConstantVariableList.DEPTHSCALE_1000;

	private int horValue = 0;
	private int verValue = 0;

	//used for rock module
	private Object zones, rocks;

	/**
	 * new AxisCurveContainerView
	 */
	public AxisCurveContainerView() {
		super();
		initialize();
	}

	public AxisCurveContainerView(TrackContainerView parent) {
		super();
		this.parentContainer = parent;
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {

		this.setOrientation(JSplitPane.VERTICAL_SPLIT);
		this.setTopComponent(this.getTopScrollPane());
		this.setBottomComponent(this.getBottomScrollPane());
		this.addMouseListener(this.getAxisCurveContainerController());
		this.setBackground(Color.WHITE);
		this.setAutoscrolls(true);
		this.setBorder(new LineBorder(SystemColor.inactiveCaption, 2));
	}

	public void repaintAll(){
		if(this.getParentContainer().getParentInternalFrameView()
				.getJMainFrame().getMainBuffer().isLoadProject())
	        return;

		int panelCount = 0;
		for(int i =0; i < this.getCurveView().getCurveGroups().size(); i++){
			if(((CurveGroup)this.getCurveView().getCurveGroups().get(i)).getCurveList().size()>panelCount){
				panelCount = ((CurveGroup)this.getCurveView().getCurveGroups().get(i)).getCurveList().size();
			}
		}
		int curveTopHeight =(int) this.getCurveTitlePanel().getHeight();
		if (panelCount*ConstantVariableList.GROUP_SINGLE_HEIGHT > curveTopHeight) {
			curveTopHeight = panelCount*ConstantVariableList.GROUP_SINGLE_HEIGHT;
		}
		this.titleAndCurveNamesPanel.getVerticalScrollBar().setMaximum(curveTopHeight);
		this.titleAndCurveNamesPanel.revalidate();
		this.titleAndCurveNamesPanel.repaint();
		xAxisViewPanel.setPreferredSize(new Dimension(this.getCurveView().getWidth(),curveTopHeight));
		this.titleAndCurveNamesPanel.revalidate();
		this.titleAndCurveNamesPanel.repaint();
		this.getCurveView().repaint();
	}
/*
	public void repaintAll() {

		if(this.getParentContainer().getParentInternalFrameView()
				.getJMainFrame().getMainBuffer().isLoadProject())
	        return;

		if(!isRepaintAll()){
			return;
		}
		this.repaintAll = false;
		xAxisViewPanel.removeAll();
		int usedWidthOfXAxisViewPanel = 0;
		if (this.getCurveLayer().getLayerType().equals(
				TrackTypeList.REALIZATION_TOP)) {
			xAxisViewPanelLayout = new BoxLayout(xAxisViewPanel,
					BoxLayout.PAGE_AXIS);
		}else{
			xAxisViewPanelLayout = new BoxLayout(xAxisViewPanel,
					BoxLayout.X_AXIS);
		}

		xAxisViewPanel.setLayout(xAxisViewPanelLayout);
		if (this.getCurveLayer().getLayerType().equals(TrackTypeList.REALIZATION_BESIDE)) {
			for (int j = 0; j < this.getCurveView().getPanelOrder().getOrders().size(); j++) {
				usedWidthOfXAxisViewPanel += this.getCurveView().getPanelOrder().getPanelWidth(j);

				if (this.getCurveView().getPanelOrder().getPanelKind(j) == PanelOrder.GROUP) {
					JPanel p = new JPanel();

					CurveGroup group = (CurveGroup) this.getCurveView().getPanelOrder().getPanel(j);
					if (group.getCurveList().size() != 0) {
						LayoutManager xGroupPanelLayout = new BoxLayout(p,BoxLayout.PAGE_AXIS);
						p.setLayout(xGroupPanelLayout);

						for (int jj = 0; jj < group.getCurveList().size(); jj++) {
							XAxisView xChild = new XAxisView(this,(CurvePanel) group.getCurveList().get(jj));

							xChild.setPreferredSize(new Dimension(this.getCurveView().getPanelOrder().getPanelWidth(j), 20));
							p.add(xChild);
						}
					} else {
						p.setBackground(ConstantVariableList.XAXISVIEW_BACKGROUND);
						p.setBorder(new LineBorder(
								ConstantVariableList.XAXISVIEW_BORDERCOLOR, 1));
						p.setPreferredSize(ConstantVariableList.XAXISVIEW_BESIDETYPE_PREFERREDSIZE);
					}

					xAxisViewPanel.add(p);
				} else if (this.getCurveView().getPanelOrder().getPanelKind(j) == PanelOrder.CURVE) {
					int index = this.getCurveView().getCurveLayer().getCurveIndex((CurvePanel) this.getCurveView().getPanelOrder().getPanel(j));
					XAxisView x = new XAxisView(this, index);
					x.setPreferredSize(new Dimension(this.getCurveView().getPanelOrder().getPanelWidth(j), 20));
					xAxisViewPanel.add(x);
				} else if (this.getCurveView().getPanelOrder().getPanelKind(j) == PanelOrder.YAXIS) {
					YHeadView yHead = new YHeadView(this);
					System.out.println("yHeadSizeBefore:" + yHead.getSize());
					yHead.setPreferredSize(new Dimension(this.getYAxisView().getWidth(),60));
					System.out.println("yHeadSizeAfter:" + yHead.getSize());
					xAxisViewPanel.add(yHead);
					xAxisViewPanelLayout = new BoxLayout(xAxisViewPanel,BoxLayout.X_AXIS);
				} else if (this.getCurveView().getPanelOrder().getPanelKind(j) == PanelOrder.DESCRIPTION) {
					DescriptionPanel desPanel = (DescriptionPanel)this.getCurveView().getPanelOrder().getPanel(j);
					PanelHeadView panelHead = new PanelHeadView(this , desPanel.getName());
					panelHead.setPreferredSize(new Dimension(desPanel.getBesideTypeWidth() , 60));
					xAxisViewPanel.add(panelHead);
					xAxisViewPanelLayout = new BoxLayout(xAxisViewPanel,BoxLayout.X_AXIS);
				} else if (this.getCurveView().getPanelOrder().getPanelKind(j) == PanelOrder.SEISMIC) {
					SeismicPanel seismicPanel = (SeismicPanel)this.getCurveView().getPanelOrder().getPanel(j);
					PanelHeadView panelHead = new PanelHeadView(this , seismicPanel.getName());
					panelHead.setPreferredSize(new Dimension(seismicPanel.getBesideTypeWidth() , 60));
					xAxisViewPanel.add(panelHead);
					xAxisViewPanelLayout = new BoxLayout(xAxisViewPanel,BoxLayout.X_AXIS);
				} else if (this.getCurveView().getPanelOrder().getPanelKind(j) == PanelOrder.LITHOLOGY){
					LithologyPanel lithologyPanel = (LithologyPanel)this.getCurveView().getPanelOrder().getPanel(j);
					PanelHeadView panelHead = new PanelHeadView(this , lithologyPanel.getName());
					panelHead.setPreferredSize(new Dimension(lithologyPanel.getBesideTypeWidth() , 60));
					xAxisViewPanel.add(panelHead);
					xAxisViewPanelLayout = new BoxLayout(xAxisViewPanel,BoxLayout.X_AXIS);
				}
			}

		}

		if (this.getCurveLayer().getLayerType().equals(	TrackTypeList.REALIZATION_TOP)) {
			for (int topJ = 0; topJ < this.getCurveView().getCurveLayer().getCurveList().size(); topJ++) {
				XAxisView x = new XAxisView(this, topJ);
				x.setPreferredSize(ConstantVariableList.XAXISVIEW_TOPTYPE_PREFERREDSIZE);
				xAxisViewPanel.add(x);
			}
		}

		if (this.getCurveLayer().getLayerType().equals(
				TrackTypeList.REALIZATION_BESIDE)) {
			if (usedWidthOfXAxisViewPanel < xAxisViewPanel.getWidth()) {
				int rightGapWidth = xAxisViewPanel.getWidth()
						- usedWidthOfXAxisViewPanel;
				JPanel rightGap = new JPanel();
				rightGap.setBackground(Color.WHITE);
				rightGap.setPreferredSize(new Dimension(rightGapWidth,
						yAxisView.getWidth()));
				rightGap.setMinimumSize(new Dimension(rightGapWidth, yAxisView
						.getWidth()));
				xAxisViewPanel.add(rightGap);
			}
		}

		jlb.setText(trackName);

		xAxisViewPanel.revalidate();

		parentContainer.revalidate();
		parentContainer.repaint();
	}
*/
	public Color getCurveDefaultColor() {
		return curveDefaultColor;
	}

	public void setCurveDefaultColor(Color curveDefaultColor) {
		this.curveDefaultColor = curveDefaultColor;
	}

	/**
	 * when this AxisCurveContainerView is selected,you must invoke this method
	 *
	 * @author GaoFei
	 */
	public void setSelected() {
		this.setIsSelected(true);
		Iterator iterator = DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().values().iterator();
		while (iterator.hasNext()) {
			CurveData cd = (CurveData) iterator.next();
			cd.setDraged(false);
		}
		for (int i = 0; i < this.getCurveView().getPanelOrder().getOrders()
				.size(); i++) {
			if (this.getCurveView().getPanelOrder().getPanelKind(i) == PanelOrder.CURVE) {
				((CurvePanel) this.getCurveView().getPanelOrder().getPanel(i)).getCd()
						.setDraged(true);
			}
		}
		/*
		 * for (int i = 0; i < this.getCurveLayer().getCurveList().size(); i++) {
		 * CurveData cd = (CurveData)
		 * this.getCurveLayer().getCurveList().get(i); cd.setDraged(true); }
		 */
		DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().repaint();
		this.setBorder(new LineBorder(SystemColor.activeCaption, 2));
		this.getTopScrollPane().getTopComponent().setBackground(
				SystemColor.activeCaption);
		this.getJlb().setForeground(SystemColor.activeCaptionText);
		this.getTopScrollPane().getTopComponent().repaint();
		this.addMouseMotionListener(axisCurveContainerController);
	}

	public void setNotSelected() {
		this.setBorder(new LineBorder(SystemColor.inactiveCaption, 2));
		this.getTopScrollPane().getTopComponent().setBackground(
				SystemColor.inactiveCaption);
		this.getJlb().setForeground(SystemColor.inactiveCaptionText);
		this.getTopScrollPane().getTopComponent().repaint();

		this.removeMouseMotionListener(axisCurveContainerController);
		this.setCursor(Cursor.getDefaultCursor());
		this.setIsSelected(false);
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public AxisCurveContainerView getLastAxisCurveContainer() {
		return parentContainer.getSelectedAxisCurveContainer();
	}

	public CurveView getCurveView() {
		if (curveView == null) {
			curveView = new CurveView(this);
		}
		return curveView;
	}

	public void setCurveView(CurveView curveView) {
		this.curveView = curveView;
	}

	/**
	 * add luming
	 *
	 * @return
	 */
	public JScrollPane getBottomScrollPane() {
		if (bottomScrollPane == null) {
			bottomScrollPane = new JScrollPane();
			bottomScrollPane.setBackground(Color.WHITE);
			bottomScrollPane.setViewportView(getJp());

			bottomScrollPane.getVerticalScrollBar().addAdjustmentListener(
					new BottomScrollBarRepaintController(this.parentContainer));
			bottomScrollPane
					.setVerticalScrollBarPolicy(javax.swing.JScrollPane.VERTICAL_SCROLLBAR_NEVER);
			bottomScrollPane
					.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			bottomScrollPane.setBorder(new LineBorder(null, 0));
		}
		return bottomScrollPane;
	}

	public JPanel getJp() {
		if (jp == null) {
			jp = new JPanel(); // add a jpanel to scrollpane
			jp.setBackground(Color.WHITE);
			jp.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();

			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 1;
			c.weighty = 1;
			c.fill = GridBagConstraints.BOTH;

			jp.add(getCurveView(),c);
		}
		return jp;
	}

	public CurveLayer getCurveLayer() {
		if (curveLayer == null) {
			curveLayer = new CurveLayer(this);
		}
		return curveLayer;
	}

	public void setCurveLayer(CurveLayer curveLayer) {
		this.curveLayer = curveLayer;
	}

	public GridLayer getGridLayer() {
		if (gridLayer == null) {
			gridLayer = new GridLayer(this);
		}
		return gridLayer;
	}

	public void setGridLayer(GridLayer gridLayer) {
		this.gridLayer = gridLayer;
	}

	public MarkerLayer getMarkerLayer() {
		if (markerLayer == null) {
			markerLayer = new MarkerLayer(this);
		}
		return markerLayer;
	}

	public void setMarkerLayer(MarkerLayer markerLayer) {
		this.markerLayer = markerLayer;
	}

	public YAxisView getYAxisView() {
		if (this.yAxisView == null) {
			this.yAxisView = new YAxisView(this);
		}
		return this.yAxisView;
	}

	public Point getDragPoint() {
		return dragPoint;
	}

	public void setDragPoint(Point dragPoint) {
		this.dragPoint = dragPoint;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
		jlb.setText(trackName);
	}

	public String getWellName() {
		return wellName;
	}

	public void setWellName(String wellName) {
		this.wellName = wellName;
	}

	public String getTrackID() {
		return trackID;
	}

	public void setTrackID(String trackID) {
		this.trackID = trackID;
	}

	public boolean isHasWell() {
		return hasWell;
	}

	public void setHasCurve(boolean hasWell) {
		this.hasWell = hasWell;
	}

	public XAxisView getXAxisViewPanel() {
		if (xAxisViewPanel == null) {
			xAxisViewPanel = new XAxisView(this);
			xAxisViewPanel.setBackground(ConstantVariableList.XVIEWPANEL_COLOR);
		}
		return xAxisViewPanel;
	}

	public JScrollPane getTitleAndCurveNamesPanel() {
		if (titleAndCurveNamesPanel == null) {
			titleAndCurveNamesPanel = new JScrollPane(getXAxisViewPanel());
			titleAndCurveNamesPanel.setMinimumSize(ConstantVariableList.XVIEWSCROLLPANEL_MINIMUMSIZE);
			titleAndCurveNamesPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			titleAndCurveNamesPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			titleAndCurveNamesPanel.setBorder(new LineBorder(null, 0));
			titleAndCurveNamesPanel.addMouseListener(new GeneralSelectionListener());
		}
		return titleAndCurveNamesPanel;
	}

	public JScrollPane getCurveTitlePanel(){
		if (titleAndCurveNamesPanel == null) {
			return getTitleAndCurveNamesPanel();
		}
		return titleAndCurveNamesPanel;
	}

	public void setTitleAndCurveNamesPanel(JScrollPane jsp) {
		this.titleAndCurveNamesPanel = jsp;
	}

	public JLabel getJlb() {
		if (jlb == null) {
			jlb = new JLabel("no track", JLabel.CENTER);
			jlb.setHorizontalTextPosition(JLabel.CENTER);
			jlb.setText("empty track");
			jlb.setFont(new java.awt.Font(null, java.awt.Font.BOLD, 11));
			jlb.setForeground(java.awt.SystemColor.inactiveCaptionText);
		}
		return jlb;
	}

	public void setJlb(JLabel jlb) {
		this.jlb = jlb;

	}

	public AxisCurveContainerController getAxisCurveContainerController() {
		if (axisCurveContainerController == null) {
			axisCurveContainerController = new AxisCurveContainerController(
					this, parentContainer);
		}
		return axisCurveContainerController;
	}

	public void setAxisCurveContainerController(
			AxisCurveContainerController axisCurveContainerController) {
		this.axisCurveContainerController = axisCurveContainerController;
	}

	public JSplitPane getTopScrollPane() {
		if (topScrollPane == null) {
			JPanel titlePanel = new JPanel();
			titlePanel.add(getJlb());
			titlePanel.setBorder(new LineBorder(null, 0));
			titlePanel.setBackground(SystemColor.inactiveCaption);

			topScrollPane = new JSplitPane();
			topScrollPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
			topScrollPane.setTopComponent(titlePanel);
			topScrollPane.setBottomComponent(getTitleAndCurveNamesPanel());
			topScrollPane.setBorder(new LineBorder(null, 0));
			topScrollPane.setDividerSize(2);
			topScrollPane.addMouseListener(new GeneralSelectionListener());
		}
		return topScrollPane;
	}

	public Element saveXML(Document doc, Integer id){
		Element xmlAxisCurveContainerView = doc.createElement("AxisCurveContainerView");
		xmlAxisCurveContainerView.setAttribute("id",Integer.toString(id));
		xmlAxisCurveContainerView.setAttribute("trackid",this.getTrackID());
		xmlAxisCurveContainerView.setAttribute("wellname",this.getWellName());
		xmlAxisCurveContainerView.setAttribute("trackName",this.getTrackName());
		xmlAxisCurveContainerView.setAttribute("horValue",Integer.toString(this.getBottomScrollPane().getHorizontalScrollBar().getValue()));
		xmlAxisCurveContainerView.setAttribute("verValue",Integer.toString(this.getBottomScrollPane().getVerticalScrollBar().getValue()));
		xmlAxisCurveContainerView.setAttribute("locationX",Integer.toString(this.getX()));
		xmlAxisCurveContainerView.setAttribute("locationY",Integer.toString(this.getY()));
		xmlAxisCurveContainerView.setAttribute("width",Integer.toString(this.getWidth()));
		xmlAxisCurveContainerView.setAttribute("height",Integer.toString(this.getHeight()));
		xmlAxisCurveContainerView.setAttribute("checkShotFileName",this.getCheckShotFileName());
		xmlAxisCurveContainerView.setAttribute("depthUnit",Integer.toString(this.getDepthUnit()));
		xmlAxisCurveContainerView.setAttribute("yType",this.getYAxisView().getYType());
		xmlAxisCurveContainerView.setAttribute("hasWell",Boolean.toString(this.isHasWell()));
		// new flag July 14
		boolean activeTrack = false;
		if (this.getParentContainer().getSelectedAxisCurveContainer() != null) {
			String selectTrackID = this.getParentContainer()
					.getSelectedAxisCurveContainer().getTrackID();
			if (this.getTrackID().equals(selectTrackID)) {
				activeTrack = true;
			}
		}
		xmlAxisCurveContainerView.setAttribute("activeTrack", Boolean.toString(activeTrack));
		if (this.getRockPhysics() instanceof Persistence)
			xmlAxisCurveContainerView.appendChild(((Persistence)getRockPhysics()).saveXML(doc));
		xmlAxisCurveContainerView.appendChild(this.getCurveView().saveXML(doc));

		return xmlAxisCurveContainerView;
	}

	public void loadXML(Element xmlAxisCurveContainerView){
		this.setTrackID(xmlAxisCurveContainerView.getAttribute("trackid"));
		this.setWellName(xmlAxisCurveContainerView.getAttribute("wellname"));
		String haswell = xmlAxisCurveContainerView.getAttribute("hasWell");
		if(haswell != null && !haswell.equals("")){
			this.setHasCurve(ConversionClass.stringToBoolean(haswell));
		}else{
			if(this.wellName != null && !wellName.equals("")){
				this.setHasCurve(true);
			}else{
				this.setHasCurve(false);
			}
		}
		this.setTrackName(xmlAxisCurveContainerView.getAttribute("trackName"));
		this.setTimeToDepthConverter((DepthConverter)DataBuffer.getJMainFrame()
				.getMainBuffer().getCheckshotHashMap().get(xmlAxisCurveContainerView.getAttribute("checkShotFileName")));
		this.setCheckShotFileName(xmlAxisCurveContainerView.getAttribute("checkShotFileName"));
		this.setDepthUnit(Integer.parseInt(xmlAxisCurveContainerView.getAttribute("depthUnit")));
		this.getYAxisView().setYType(xmlAxisCurveContainerView.getAttribute("yType"));
		this.setBounds(Integer.parseInt(xmlAxisCurveContainerView.getAttribute("locationX")),
				Integer.parseInt(xmlAxisCurveContainerView.getAttribute("locationY")),
				Integer.parseInt(xmlAxisCurveContainerView.getAttribute("width")),
				Integer.parseInt(xmlAxisCurveContainerView.getAttribute("height")));
		this.setMDDepthConverterFromHashMap();
		String active = xmlAxisCurveContainerView.getAttribute("activeTrack");
		if (active!=null){ //new attribute July 14
			boolean activeTrack = ConversionClass.stringToBoolean(active);
			if (activeTrack)
		        this.getParentContainer().setSelectedAxisCurveContainer(this);
		}
		Element xmlRockPhysics = (Element) xmlAxisCurveContainerView.getElementsByTagName("RockPhysics").item(0);
		if (this.getRockPhysics() instanceof Persistence)
			((Persistence)this.getRockPhysics()).loadXML(xmlRockPhysics, null);

		Element xmlCurveView = (Element) xmlAxisCurveContainerView.getElementsByTagName("CurveView").item(0);
		Element xmlCurveLayer = (Element) xmlCurveView.getElementsByTagName("CurveLayer").item(0);
		this.getCurveView().getCurveLayer().setLayerType(xmlCurveLayer.getAttribute("type"));
		this.getCurveView().loadXML(xmlCurveView);

		this.setHorValue(Integer.parseInt(xmlAxisCurveContainerView.getAttribute("horValue")));
		this.setVerValue(Integer.parseInt(xmlAxisCurveContainerView.getAttribute("verValue")));
	}


	public void setTopScrollPane(JSplitPane topScrollPane) {
		this.topScrollPane = topScrollPane;
	}

	class GeneralSelectionListener extends MouseAdapter {

		public void mousePressed(MouseEvent arg0) {
			ViewCoreOperator.selectTrack(AxisCurveContainerView.this
					.getWellName());
		}

	}

	public TrackContainerView getParentContainer() {
		return parentContainer;
	}

	public int getTrackType() {
		return trackType;
	}

	public void setTrackType(int trackType) {
		this.trackType = trackType;
	}

	public DepthToTimeConverter getDepthToTimeConverter() {
		return depthToTimeConverter;
	}

	public void setDepthToTimeConverter(
			DepthToTimeConverter depthToTimeConverter) {
		this.depthToTimeConverter = depthToTimeConverter;
	}

	public DepthConverter getTimeToDepthConverter() {
		return timeToDepthConverter;
	}

	public void setTimeToDepthConverter(
			DepthConverter timeToDepthConverter) {
		this.timeToDepthConverter = timeToDepthConverter;
	}

	public DepthConverter getMDDepthConverter() {
		if(mDDepthConverter == null){
			setMDDepthConverterFromHashMap();
		}
		return mDDepthConverter;
	}

	public void setMDDepthConverter(DepthConverter depthConverter) {
		mDDepthConverter = depthConverter;
	}

	public String getCheckShotFileName() {
		return checkShotFileName;
	}

	public void setCheckShotFileName(String checkShotFileName) {
		this.checkShotFileName = checkShotFileName;
	}

	public float getVelocityValue() {
		return velocityValue;
	}

	public void setVelocityValue(float velocityValue) {
		this.velocityValue = velocityValue;
	}

	public void setShowCurve(boolean isShow) {
		this.isShowCurve = isShow;
	}

	public boolean IsShowCurve() {
		return isShowCurve;
	}

	public boolean isVisibled() {
		return this.visibled;
	}

	public void setVisibled(boolean visibled) {
		this.visibled = visibled;
	}


	public int getHorValue() {
		return horValue;
	}

	public void setHorValue(int horValue) {
		this.horValue = horValue;
	}

	public int getVerValue() {
		return verValue;
	}

	public void setVerValue(int verValue) {
		this.verValue = verValue;
	}

	public int getBesideWidth() {
		return besideWidth;
	}
	public void setBesideWidth(int besideWidth) {
		this.besideWidth = besideWidth;
	}

	 // use reflection
	public Object getZoneLayer() {
		if (zones != null)
			return zones;
		try {
			Class zone = Class.forName("com.gwsys.welllog.view.zone.ZoneLayer");
			Class view = AxisCurveContainerView.class;
			Constructor construct = zone.getConstructor(new Class[] { view });
			zones = construct.newInstance(new Object[] { this });
		} catch (ClassNotFoundException e) {
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
		return zones;
	}

	public Object getRockPhysics() {
		if (rocks != null)
			return rocks;
		try {
			Class rock = Class
					.forName("com.gwsys.welllog.rockphysics.RockPhysics");

			Constructor construct = rock.getConstructor(new Class[] { String.class });
			rocks = construct.newInstance(new Object[] { getWellName() });
		} catch (ClassNotFoundException e) {
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
		return rocks;
	}

	public int getDepthUnit() {
		return depthUnit;
	}

	public void setDepthUnit(int depthUnit) {
		this.depthUnit = depthUnit;
	}

	public float getDepthScale() {
		return depthScale;
	}

	public void setDepthScale(float depthScale) {
		this.depthScale = depthScale;
	}

	public void setMDDepthConverterFromHashMap(){
		HashMap surveyHashMap = DataBuffer.getJMainFrame().getMainBuffer().getSurveyHashMap();
		if(this.wellName != null && !this.wellName.equals("")){
			if(surveyHashMap.containsKey(this.wellName)){
				this.mDDepthConverter = (DepthConverter)surveyHashMap.get(this.wellName);
				return;
			}
		}
		this.mDDepthConverter = DataBuffer.getJMainFrame().getMainBuffer().getMDDepthConverter();
	}

	/**
	 * Gets the zone listener.
	 * @return zone listener.
	 */
	public ZoneListener getZoneListener(){
		return DataBuffer.getJMainFrame().getZoneListener();
	}
}
