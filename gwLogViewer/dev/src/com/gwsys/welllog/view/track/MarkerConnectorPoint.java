/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;


import javax.swing.JPanel;

import com.gwsys.welllog.view.curve.CurveMarker;

public class MarkerConnectorPoint extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CurveMarker cm = null;
	private AxisCurveContainerView acc = null;
	
	public MarkerConnectorPoint()
	{
		
	}

	public CurveMarker getCm() {
		return cm;
	}

	public void setCm(CurveMarker cm) {
		this.cm = cm;
	}

	public AxisCurveContainerView getAcc() {
		return acc;
	}

	public void setAcc(AxisCurveContainerView acc) {
		this.acc = acc;
	}

}
