/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.dialogs.ComboBoxRender;

/**
 * A container contains a lots of AxisCurveContainers.
 *
 * @author gaofei
 */
public class TrackContainerView extends JPanel {

	private static final long serialVersionUID = 1L;

	private ArrayList<AxisCurveContainerView> axisCurveContainerList = new ArrayList<AxisCurveContainerView>();

	private MarkerConnectorPoint markerConnectLayer = null;

	private InternalFrameView parentInternalFrameView = null;

	private AxisCurveContainerView selectedAxisCurveContainer = null; // the

	private int axisCurveContainerMaxWidth;

	private int curveViewMaxWidth;

	private int curveViewMaxHeight;

	private static final int curveViewDefaultWidth = ConstantVariableList.CURVEVIEW_DEFAULT_WIDTH;

	private static final int curveViewDefaultHeight = ConstantVariableList.CURVEVIEW_DEFAULT_HEIGHT;

	private static final int axisCurveContainerDefaultWidth = ConstantVariableList.AXISCURVECONTAINER_DEFAULT_WIDTH;

	private static final int axisCurveContainerDefaultHeight = ConstantVariableList.AXISCURVECONTAINER_DEFAULT_HEIGHT;

	private ArrayList markerCable1 = new ArrayList(); // marker left point

	private ArrayList markerCable2 = new ArrayList(); // marker right point

	private ArrayList MarkerBuffer = new ArrayList();


	public TrackContainerView(InternalFrameView parent) {
		super();
		this.parentInternalFrameView = parent;
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
		axisCurveContainerMaxWidth = 100;
		curveViewMaxWidth = 100;
		curveViewMaxHeight = 100;
		this.setLayout(null);// remove layout
		this.setBackground(ConstantVariableList.TRACKCONTAINERVIEW_BACKGROUND);
		this.revalidate();

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g.create();

		if(this.getParentInternalFrameView().getJMainFrame().getMainBuffer().isLoadProject()){
			return;
		}
		AxisCurveContainerView axisCurveContainer = null;
		for (int i = 0; i < axisCurveContainerList.size(); i++) {
			axisCurveContainer = axisCurveContainerList.get(i);
			if (axisCurveContainer.isVisible()) {
				ArrayList markerList = axisCurveContainer.getCurveView()
				.getMarkerLayer().getMarkerList();
				for (int j = 0; j < markerList.size(); j++) {
					CurveMarker curveMarker = (CurveMarker) (markerList.get(j));
					curveMarker.setYHeight(axisCurveContainer.getCurveView());
				}
			}
		}

		g2.setFont(new Font("SansSerif", Font.PLAIN, 12));
		// paint marker name on the left
		print(g2, 1, 1);

	}

	@SuppressWarnings("unchecked")
	public void print(Graphics2D g2, float rateWidth, float rateHeight){
		int boundX = ConstantVariableList.AXISCURVECONTAINER_GAP;
		AxisCurveContainerView axisCurveContainer = null;
		for (int i = 0; i < axisCurveContainerList.size(); i++) {
			axisCurveContainer = axisCurveContainerList.get(i);
			if (axisCurveContainer.isVisible()) {
				int Width = (int)(axisCurveContainer.getWidth()* rateWidth);
				ArrayList markerList = axisCurveContainer.getCurveView()
						.getMarkerLayer().getMarkerList();
				for (int j = 0; j < markerList.size(); j++) {
					CurveMarker curveMarker = (CurveMarker) (markerList.get(j));
					if (!curveMarker.isVisible()) {
						continue;
					}
					String markerName = curveMarker.getMarkerName();
					if(curveMarker.getLeftMarker() != null){
						continue;
					}

					g2.setColor(curveMarker.getMarkerColor());
					int symbolSize = 10, offset = 5;
					int headerHeight=axisCurveContainer.getTopScrollPane().getHeight();
					int move = axisCurveContainer.getBottomScrollPane().getVerticalScrollBar().getValue();
					if (curveMarker.getYHeight()-move < 0)
						continue;
					g2.drawOval(boundX - symbolSize - offset,(curveMarker.getYHeight() + headerHeight)-move, 
							symbolSize, symbolSize);
					g2.drawLine(boundX - symbolSize,(curveMarker.getYHeight() + headerHeight)-move,
							boundX - symbolSize,(curveMarker.getYHeight() + symbolSize + headerHeight)-move);
					g2.drawLine(boundX - symbolSize - offset,(curveMarker.getYHeight() + symbolSize/2 + headerHeight)-move,
							boundX - offset,(curveMarker.getYHeight() + symbolSize/2 + headerHeight)-move);
					g2.drawString(markerName, boundX -symbolSize-2*offset-g2.getFontMetrics().stringWidth(markerName),
							(curveMarker.getYHeight() + symbolSize + headerHeight)-move);
				}
				boundX += ConstantVariableList.AXISCURVECONTAINER_GAP + Width;
			}
		}

		if (this.getMarkerCable1().size() != this.getMarkerCable2().size()) {
			return;
		}
		for (int i = 0; i < this.getMarkerCable1().size(); i++) {
			MarkerConnectorPoint mcp1 = (MarkerConnectorPoint) this
					.getMarkerCable1().get(i);
			MarkerConnectorPoint mcp2 = (MarkerConnectorPoint) this
					.getMarkerCable2().get(i);

	    	if(mcp1.getCm() == null || mcp2.getCm() == null){
	    		continue;
	    	}
			if(!(mcp1.getCm().isVisible() && mcp2.getCm().isVisible())){
				continue;
			}
			if (mcp1.getAcc().equals(mcp2.getAcc())) {
				continue;
			}
			//make X1 in left
			if (mcp2.getAcc().getX() < mcp1.getAcc().getX()) {
				mcp1 = (MarkerConnectorPoint) this.getMarkerCable2().get(i);
				mcp2 = (MarkerConnectorPoint) this.getMarkerCable1().get(i);
			}

			g2.setColor(mcp1.getCm().getMarkerColor());
			g2.setStroke(ComboBoxRender.getStroke(mcp1.getCm().getLineStyle(),
					mcp1.getCm().getMarkerWidth()));
			if (mcp1.getCm().isSelected()) {
				g2.setStroke(ComboBoxRender.getStroke(mcp1.getCm().getLineStyle(),
						(float)(mcp1.getCm().getMarkerWidth()+1)));
			}
			g2.drawLine((int)((mcp1.getAcc().getX() + mcp1.getAcc().getWidth())*rateWidth),
					(int) (caculateTheHeight(mcp1)*rateHeight),
					(int) (mcp2.getAcc().getX()*rateWidth),
					(int) (caculateTheHeight(mcp2)*rateHeight));
		}
	}

	public float caculateTheHeight(MarkerConnectorPoint mcp) {
		float mcHeight1 = mcp.getCm().getYHeight();
		AxisCurveContainerView acc1 = mcp.getAcc();

		float bottomOuty1 = acc1.getBottomScrollPane().getHeight()
				- acc1.getBottomScrollPane().getVerticalScrollBar().getWidth();

		int divider1 = acc1.getDividerLocation();
		if(divider1 < 90){
			divider1 = 90;
		}

		int dividerWidth = acc1.getDividerSize();
		// scroll bar 's value
		float sbValue1 = acc1.getBottomScrollPane().getVerticalScrollBar()
				.getValue();

		float smtimeTop1 = 0 + sbValue1;
		float smtimeBottom1 = smtimeTop1 + bottomOuty1;

		float bottomCoor = 0;
		if (mcHeight1 < smtimeTop1) {
			bottomCoor = 0;
		}
		if (mcHeight1 > smtimeBottom1) {
			bottomCoor = bottomOuty1;
		}
		if (mcHeight1 <= smtimeBottom1 && mcHeight1 >= smtimeTop1) {
			bottomCoor = mcHeight1 - smtimeTop1;
		}
		float theFinalHeight1 = bottomCoor + divider1 + dividerWidth;
		if(theFinalHeight1>acc1.getHeight())
			theFinalHeight1=acc1.getHeight();
		return theFinalHeight1;
	}

	public int getAxisCurveContainerMaxHeight() {
		return this.getParentInternalFrameView().getHeight()-48;
	}

	public ArrayList getAxisCurveContainerList() {
		return axisCurveContainerList;
	}

	public int getAxisCurveContainerMaxWidth() {
		return axisCurveContainerMaxWidth;
	}

	public void setAxisCurveContainerMaxWidth(int axisCurveContainerMaxWidth) {
		this.axisCurveContainerMaxWidth = axisCurveContainerMaxWidth;
	}

	/**
	 *
	 * @return the current selected AxisCurveContainerView
	 */
	public AxisCurveContainerView getSelectedAxisCurveContainer() {
		if (axisCurveContainerList.size()==1)
			return axisCurveContainerList.get(0);
		return selectedAxisCurveContainer;
	}

	public void setSelectedAxisCurveContainer(
			AxisCurveContainerView selectedAxisCurveContainer) {
		if (this.selectedAxisCurveContainer == null) {
			this.selectedAxisCurveContainer = new AxisCurveContainerView();
		}
		this.selectedAxisCurveContainer = selectedAxisCurveContainer;
	}

	/**
	 *
	 * @return curveView's height
	 */
	public int getCurveViewMaxHeight() {
		return curveViewMaxHeight;
	}

	public void setCurveViewMaxHeight(int curveViewMaxHeight) {
		this.curveViewMaxHeight = curveViewMaxHeight;
	}

	/**
	 *
	 * @return curveView's width
	 */
	public int getCurveViewMaxWidth() {
		return curveViewMaxWidth;
	}

	public void setCurveViewMaxWidth(int curveViewMaxWidth) {
		this.curveViewMaxWidth = curveViewMaxWidth;
	}

	public int getAxisCurveContainerDefaultHeight() {
		return axisCurveContainerDefaultHeight;
	}

	public int getAxisCurveContainerDefaultWidth() {
		return axisCurveContainerDefaultWidth;
	}

	public int getCurveViewDefaultHeight() {
		return curveViewDefaultHeight;
	}

	public int getCurveViewDefaultWidth() {
		return curveViewDefaultWidth;
	}

	/**
	 * update the location of all AxisCurveContainers before use this method,you
	 * should first set axisCurveContainer's size, and then use
	 * this.setAxisCurveContainerMaxHeight() method, last use this method
	 *
	 * @author gaofei
	 */
	public void updateAxisCurveContainersLocation() {
		int boundX = ConstantVariableList.AXISCURVECONTAINER_GAP; // the
		// position
		// in x axis
		// of the
		// AxisCurveContainerView
		for (int i = 0; i < axisCurveContainerList.size(); i++) {
			int Width = (axisCurveContainerList.get(i)).getWidth();
			int Height = (axisCurveContainerList.get(i)).getHeight();
			// AxisCurveContainerView
			(axisCurveContainerList.get(i)).setBounds(
					boundX, 0, Width, Height);
			boundX = boundX + Width
					+ ConstantVariableList.AXISCURVECONTAINER_GAP;
		}
		setPreferredSize(new Dimension(boundX + 100,
				this.getAxisCurveContainerMaxHeight() + 100)); // resize
		// TrackContainerView
		this.repaint();
	}

	public void updateAxisCurveContainersLocation(int height) {
		int boundX = ConstantVariableList.AXISCURVECONTAINER_GAP; // the

		for (int i = 0; i < axisCurveContainerList.size(); i++) {
			AxisCurveContainerView track = axisCurveContainerList.get(i);
			int curveViewHeight = track.getCurveView().getHeight();
			int yHeadHeight = (int) (track.getBottomScrollPane().getBounds().getY());
			if(yHeadHeight < 95){
				yHeadHeight = 95;
			}
			int trackNewHeight = curveViewHeight+ yHeadHeight;
			if(trackNewHeight > height){
				trackNewHeight = height;
			}
			int Width = track.getWidth();
			(axisCurveContainerList.get(i)).setBounds(boundX, 0, Width, trackNewHeight);
			boundX = boundX + Width
					+ ConstantVariableList.AXISCURVECONTAINER_GAP;
		}
		setPreferredSize(new Dimension(boundX + 100, height + 100)); // resize

	}

	public void createACC(String trackName) {
		this.createACC().setTrackName(trackName);
	}

	@SuppressWarnings("unchecked")
	public AxisCurveContainerView createACC() {
		int accwidth = axisCurveContainerDefaultWidth;
		if ((ConstantVariableList.CURVEVIEW_DEFAULT_WIDTH + ConstantVariableList.YAXISVIEW_PREFERREDSIZE
				.getWidth()) < axisCurveContainerDefaultWidth) {
			accwidth = (int) (ConstantVariableList.CURVEVIEW_DEFAULT_WIDTH + 
					ConstantVariableList.YAXISVIEW_PREFERREDSIZE.getWidth()) + 20;
		}

		AxisCurveContainerView newAxisCurveContainer = new AxisCurveContainerView(
				this);
		newAxisCurveContainer.getCurveLayer().setLayerType(
				TrackTypeList.REALIZATION_BESIDE);

		newAxisCurveContainer.setBounds(0, 0, accwidth,
				axisCurveContainerDefaultHeight); // default
		newAxisCurveContainer.getCurveView().setPreferredSize(
				new Dimension(ConstantVariableList.CURVEVIEW_DEFAULT_WIDTH,
						ConstantVariableList.CURVEVIEW_DEFAULT_HEIGHT));

		axisCurveContainerList.add(newAxisCurveContainer);
		this.add(newAxisCurveContainer);
		return newAxisCurveContainer;
	}

	public MarkerConnectorPoint getMarkerConnectLayer() {
		return markerConnectLayer;
	}

	public void setMarkerConnectLayer(MarkerConnectorPoint markerConnectLayer) {
		this.markerConnectLayer = markerConnectLayer;
	}

	public ArrayList getMarkerCable1() {
		return markerCable1;
	}

	public void setMarkerCable1(ArrayList markerCable1) {
		this.markerCable1 = markerCable1;
	}

	public ArrayList getMarkerCable2() {
		return markerCable2;
	}

	public void setMarkerCable2(ArrayList markerCable2) {
		this.markerCable2 = markerCable2;
	}

	public InternalFrameView getParentInternalFrameView() {
		return parentInternalFrameView;
	}

	public ArrayList getMarkerBuffer() {
		return MarkerBuffer;
	}

	public Element saveXML(Document doc){
		Element xmlTrackContainerView = doc.createElement("TrackContainerView");

		Element xmlAxisCurveContainerViewList = doc.createElement("AxisCurveContainerViewList");
		for(int i = 0 ; i< this.getAxisCurveContainerList().size();i++){
    		AxisCurveContainerView axisCurveContainerView = axisCurveContainerList.get(i);
    		xmlAxisCurveContainerViewList.appendChild(axisCurveContainerView.saveXML(doc,i));
    	}
		xmlTrackContainerView.appendChild(xmlAxisCurveContainerViewList);

		return xmlTrackContainerView;
	}

	@SuppressWarnings("unchecked")
	public void loadXML(Element xmlTrackContainerView){
		Element xmlAxisCurveContainerViewList = (Element) xmlTrackContainerView.
		getElementsByTagName("AxisCurveContainerViewList").item(0);
		NodeList nodeList = null;
		nodeList = xmlAxisCurveContainerViewList.getElementsByTagName("AxisCurveContainerView");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlAxisCurveContainerView = (Element) nodeList.item(i);
			DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createTrack(
					this.parentInternalFrameView.getTitle(),
					xmlAxisCurveContainerView.getAttribute("trackName"),
					xmlAxisCurveContainerView.getAttribute("wellname"), false);

			AxisCurveContainerView axisCurveContainerView = new AxisCurveContainerView(this);
			axisCurveContainerView.loadXML(xmlAxisCurveContainerView);

			axisCurveContainerView.getCurveView().setFillRedrawFlag(true);
			axisCurveContainerView.getCurveView().setMarkerRedrawFlag(true);
			this.getAxisCurveContainerList().add(axisCurveContainerView);
			this.add(axisCurveContainerView);
			this.setPreferredSize(new Dimension(axisCurveContainerView.getX() +
					400 + 100, axisCurveContainerView.getHeight() + 100));
		}
	}

	/**
	 * added by Alex, June 27, 2006
	 *  get a track by its name, if track not found return null
	 *
	 *  @param trackName name of track
	 *  @return the track (AxisCurveContainerView) wanted
	 */
	public AxisCurveContainerView getTrackByWellName(String wellName)	{
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();

		if (ifv == null) {
			return null;
		}

		ArrayList list = ifv.getTrackContainer().getAxisCurveContainerList();

		for (int i = 0; i < list.size(); i++)	{
			AxisCurveContainerView track = (AxisCurveContainerView) list.get(i);
			if (track.getWellName().equals(wellName))
				return track;
		}

		return null;
	}

	/**
	 * added by Alex, June 21, 2006
	 *  change track order of TrackContainerView
	 *
	 *  @param dragName name of track to insert
	 *  @param selectedName name of second track
	 *  @param index index of track list where it'll move dragged track
	 */
	@SuppressWarnings("unchecked")
	public void changeTrackOrder(String dragName, String selectedName, int index)	{
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		if (ifv == null) {
			return;
		}

		ArrayList list = ifv.getTrackContainer().getAxisCurveContainerList();

		AxisCurveContainerView drag = ifv.getTrackContainer().getTrackByWellName(dragName);

		if (drag == null){
			return;
		}

		list.remove(drag);
		list.add(index, drag);

		ifv.getTrackContainer().removeAll();
		for (int i = 0; i < list.size(); i++)	{
			AxisCurveContainerView track = (AxisCurveContainerView) list.get(i);	
			
			ifv.getTrackContainer().add(track);
		}
	}
}
