/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view.track;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.curve.CurveData;

public class YHeadView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AxisCurveContainerView parent = null;
	
	/**
	 * Constructor
	 * @param parent
	 */
	public YHeadView(AxisCurveContainerView parent) {
		super();
		this.parent = parent;
		initialize();
		this.setSize(ConstantVariableList.YAXISHEAD_PREFERREDSIZE);	
	}


	/**
	 * This is the default constructor
	 */
	public YHeadView() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setBackground(ConstantVariableList.XAXISVIEW_BACKGROUND);
		this.setBorder(new LineBorder(ConstantVariableList.XAXISVIEW_BORDERCOLOR,1));
	}

	protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
		//this.setSize(ConstantVariableList.YAXISHEAD_PREFERREDSIZE);
		//System.out.println(this.getSize() + "---size");
		FontMetrics fontMetrics = arg0.getFontMetrics();
		int meterLabelWidth = fontMetrics.stringWidth("Meter");
		int secondLabelWidth = fontMetrics.stringWidth("Second");
		int feetLabelWidth = fontMetrics.stringWidth("Foot");
		int mdLabelWidth = fontMetrics.stringWidth("MD");
		int tvdLabelWidth = fontMetrics.stringWidth("TVD");
		int tvdssLabelWidth = fontMetrics.stringWidth("TVDSS");
		
		Graphics2D g2 = (Graphics2D)arg0.create();		
		if(parent.getYAxisView().getYType().equals(TrackTypeList.Y_TIME)) {
			g2.drawString("Time", (this.getWidth() - secondLabelWidth) / 2, 
					fontMetrics.getHeight());
			g2.drawString("Second", (this.getWidth() - secondLabelWidth) / 2, 
					2* fontMetrics.getHeight());
		}else if (parent.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_MD)) {
			g2.drawString("MD", (this.getWidth() - mdLabelWidth) / 2, 
					fontMetrics.getHeight());
			if(parent.getDepthUnit() == CurveData.FOOT){
				g2.drawString("Foot",(this.getWidth() - feetLabelWidth) / 2 , 
						2* fontMetrics.getHeight());				
			}else{
				g2.drawString("Meter",(this.getWidth() - meterLabelWidth) / 2 , 
						2* fontMetrics.getHeight());
			}
		}else if (parent.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVD)) {
			g2.drawString("TVD", (this.getWidth() - tvdLabelWidth) / 2, 
					fontMetrics.getHeight());
			if(parent.getDepthUnit() == CurveData.FOOT){
				g2.drawString("Foot",(this.getWidth() - feetLabelWidth) / 2 , 
						2* fontMetrics.getHeight());				
			}else{
				g2.drawString("Meter",(this.getWidth() - meterLabelWidth) / 2 , 
						2* fontMetrics.getHeight());
			}
		}else if (parent.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)) {
			g2.drawString("TVDSS", (this.getWidth() - tvdssLabelWidth) / 2, 
					fontMetrics.getHeight());
			if(parent.getDepthUnit() == CurveData.FOOT){
				g2.drawString("Foot",(this.getWidth() - feetLabelWidth) / 2 , 
						2* fontMetrics.getHeight());				
			}else{
				g2.drawString("Meter",(this.getWidth() - meterLabelWidth) / 2 , 
						2* fontMetrics.getHeight());
			}
		}
		g2.dispose();		
	}

}
