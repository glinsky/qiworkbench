/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import com.gwsys.welllog.core.curve.ConstantVariableList;

public class PanelHeadView extends JPanel {
	public static final int DESCRIPTION = 0;

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private AxisCurveContainerView parent = null;
    private String text = "";
	
	public PanelHeadView(AxisCurveContainerView parent , String text) {
		super();
		// TODO Auto-generated constructor stub
		this.parent = parent;
		this.text = text;
		initialize();
	}
	
	private void initialize() {
		this.setBackground(ConstantVariableList.XAXISVIEW_BACKGROUND);
		this.setBorder(new LineBorder(ConstantVariableList.XAXISVIEW_BORDERCOLOR,1));
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g.create();
		g2.drawString(text,10 , 10);
		
		g2.dispose();		
	}
}
