/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Class handles the mouse event on depth panel.
 * @author Team
 *
 */
public class YAxisViewController implements MouseListener, MouseMotionListener {

	private YAxisView yAxisView = null;

	private AxisCurveContainerView track = null;

	private int w = 0, h = 0;

	private boolean readyDragging = false, isDragging = false;

	/**
	 * Creates the YAxisViewController.
	 * @param yAxisView YAxisView.
	 * @param track AxisCurveContainerView.
	 */
	public YAxisViewController(YAxisView yAxisView, AxisCurveContainerView track) {
		this.yAxisView = yAxisView;
		this.track = track;
		w = yAxisView.getWidth();
		h = yAxisView.getHeight();
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		if(isDragging == false) {
			yAxisView.setCursor(Cursor.getDefaultCursor());
		}
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		if(readyDragging) {
			isDragging = true;
		}
	}

	public void mouseReleased(MouseEvent e) {
		isDragging = false;
		readyDragging = false;
		yAxisView.setCursor(Cursor.getDefaultCursor());
		yAxisView.setSize(w,h);
		yAxisView.repaint();
		track.getCurveView().setFillRedrawFlag(true);
		track.repaintAll();
	}

	public void mouseDragged(MouseEvent e) {
		w = yAxisView.getWidth();
		if(isDragging){
			w = e.getX();
			yAxisView.setSize(w, h);
			yAxisView.repaint();
			track.getCurveView().setFillRedrawFlag(true);
			track.repaintAll();
		}
	}

	public void mouseMoved(MouseEvent e) {
		int x = e.getX();
		Dimension currentSize = yAxisView.getSize();
		if (x >= currentSize.width - 1 && x <= currentSize.width) {
			yAxisView.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
			readyDragging = true;
			return;
		}else {
			yAxisView.setCursor(Cursor.getDefaultCursor());
			return;
		}
	}

}
