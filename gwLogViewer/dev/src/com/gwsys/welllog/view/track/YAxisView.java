/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.text.NumberFormat;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.dialogs.ComboBoxRender;

/**
 * Show the depth/time axis.
 * @author Team
 *
 */
public class YAxisView extends JPanel implements ComponentListener {

	private static final long serialVersionUID = 1L;

	private AxisCurveContainerView parent = null;

	private NumberFormat format = NumberFormat.getNumberInstance();

	private float[] coordinateValueList = new float[0]; // save coordinate value

	private float[] coordinatePointList = new float[0]; // save coordinate
														// position

	private boolean reComputeFlag = false;

	private int defaultLabelSpace = ConstantVariableList.LABEL_SPACE;

	private float ySpace = ConstantVariableList.YSPACE; // this is the real
														// space.use this to
														// redraw the grid

	private String yType = TrackTypeList.Y_DEPTH_MD;

	private int offset=3;//yFirst,yEnd,

	/**
	 * Create a new YAxisView.
	 *
	 * @param parent
	 *            an instance of AxisCurveContainerView
	 */
	public YAxisView(AxisCurveContainerView parent) {
		this.parent = parent;
		this.setSize(ConstantVariableList.YAXISVIEW_PREFERREDSIZE);
		YAxisViewController controller = new YAxisViewController(this, parent);
		this.addMouseListener(controller);
		this.addMouseMotionListener(controller);
		this.addComponentListener(this);
		this.setPreferredSize(ConstantVariableList.YAXISVIEW_PREFERREDSIZE);
		this.setBackground(ConstantVariableList.YAXISVIEW_BACKGROUND);
		this.setBorder(new LineBorder(Color.BLACK,1));

		format.setGroupingUsed(false);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if(this.getParentAcc().getParentContainer().getParentInternalFrameView()
				.getJMainFrame().getMainBuffer().isLoadProject())
	        return;

//		Rectangle rect = g.getClipBounds(); // get repaint area
//		Rectangle boundModel = this.getBounds();
//		if (rect.intersects(boundModel) || rect.contains(boundModel)) {
		Graphics2D g2 = (Graphics2D) g.create();

		if (isReComputeFlag()) {
			this.reCompute();
		}
		//modify the format
		if (coordinateValueList.length>2){
			float step = Math.abs(coordinateValueList[1]-coordinateValueList[0]);
			if (step>=10)
				format.setMaximumFractionDigits(0);
			if (step<10&&step>=0.1)
				format.setMaximumFractionDigits(2);
			if (step<0.1&&step>0.01)
				format.setMaximumFractionDigits(3);
			if (step<=0.01)
				format.setMaximumFractionDigits(4);
		}
		print(g2,0,1,1);
	}

	public void print(Graphics2D g2, int left, float rateWidth, float rateHeight){
		g2.setColor(Color.BLACK);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
				RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		//fit the largest label in available space
		String tag = format.format(coordinateValueList[getCoordinateNumber()-1]);
		float fontSize = 3;
		Font font = new Font("SansSerif", Font.PLAIN, (int)fontSize);
		float availableSpace = rateWidth*this.getWidth()-2;
		int textWidth = g2.getFontMetrics(font.deriveFont(fontSize)).stringWidth(tag)+offset;
		while(textWidth<availableSpace&&textWidth<32){
			fontSize+=0.5f;
			textWidth = g2.getFontMetrics(font.deriveFont(fontSize)).stringWidth(tag)+offset;
		}

		g2.setFont(font.deriveFont(fontSize));
		int TagNumber = getCoordinateNumber();
		for (int i = 0; i < TagNumber; i++) {
			float fTag = coordinateValueList[i];
			if(this.yType.equals(TrackTypeList.Y_DEPTH_TVDSS)){
				fTag = -fTag;
			}

			int Top = 0;
			if(i>0 && i < (TagNumber-1))
			{
				Top = (int)(coordinatePointList[i]*rateHeight)+(int)(g2.getFontMetrics().getHeight()/4);
			}else{
				Top = (int)(coordinatePointList[i]*rateHeight);
			}
			g2.drawString(format.format(fTag), (int)(left+offset*rateWidth)+1,Top);
			//g2.drawLine(0,(int)(coordinatePointList[i]*rateHeight),100,(int)(coordinatePointList[i]*rateHeight));
		}

		CurveView cv = parent.getCurveView();
		if (cv.isShowMarker()) {
			if(cv.isMarkerRedrawFlag()){
				cv.getMarkerLayer().reSetMarker(cv, this.getWidth());
			}

			CurveMarker curveMarker = null;
			for (int i = 0; i < cv.getMarkerLayer().getMarkerList().size(); i++) {
				curveMarker = (CurveMarker) (cv.getMarkerLayer().getMarkerList().get(i));
				if(!curveMarker.isVisible()){
					continue;
				}
				g2.setStroke(ComboBoxRender.getStroke(curveMarker.getLineStyle(),curveMarker.getMarkerWidth()));
				g2.setColor(curveMarker.getMarkerColor());
				if(curveMarker.isSelected())
				{
					g2.setStroke(ComboBoxRender.getStroke(curveMarker.getLineStyle(),(float)(curveMarker.getMarkerWidth()+1)));
				}
				g2.drawLine(left, (int)(curveMarker.getYHeight()*rateHeight),
						(int)(left + this.getWidth()*rateWidth),(int)(curveMarker.getYHeight()*rateHeight));
			}

			//cv.getMarkerLayer().drawMarker(g2, cv, this.getWidth(),rateWidth,rateHeight);
		}
	}

	/**
	 * calculate with current coordinate type
	 */
	public void reCompute() {
		if(this.getParentAcc().getParentContainer().getParentInternalFrameView()
				.getJMainFrame().getMainBuffer().isLoadProject())
	        return;
		if(yType.equals(TrackTypeList.Y_TIME)){
			float yTopTime = parent.getCurveLayer().getYTopTime();
			float yBottomTime = parent.getCurveLayer().getYBottomTime();
			yTopTime = parent.getCurveLayer().getYSetTopTime();
			yBottomTime = parent.getCurveLayer().getYSetBottomTime();
			float yTime = yBottomTime - yTopTime;
			int componentheight = this.getHeight();
			// change the pixel to the actual depth,to find the proper depth
			float pixelPerTime = componentheight / yTime; // get one depth to pixel
			float temp = this.defaultLabelSpace / pixelPerTime; // how many depth is the most near the best space

			float timeSpace = temp;
			if(timeSpace > 2*yTime){
				coordinateValueList = new float[2];
				coordinatePointList = new float[2];
				coordinateValueList[0] = yTopTime;
				coordinateValueList[1] = yBottomTime;
				coordinatePointList[0] = 10;
				coordinatePointList[1] = this.getHeight() - 3;
			}else{
				int coordinateNumber = ConversionClass.round(yTime / timeSpace) + 1;// every depthSpace,draw a string
				this.setYSpace(timeSpace * pixelPerTime);

				coordinateValueList = new float[coordinateNumber];
				coordinatePointList = new float[coordinateNumber];

				for (int i = 0; i <= coordinateValueList.length - 1; i++) {
					coordinateValueList[i] = yTopTime + i * timeSpace;
					if (i == 0) {
						coordinatePointList[i] = 10 + i * this.getYSpace();
					} else {
						coordinatePointList[i] = 4 + i * this.getYSpace();
					}

				}
				// change to int
				coordinateValueList[coordinateNumber - 1] = yBottomTime;
				coordinatePointList[coordinateNumber - 1] = this.getHeight() - 3;
			}

		}else{
			float yTopDepth = parent.getCurveLayer().getYTopDepth();
			float yBottomDepth = parent.getCurveLayer().getYBottomDepth();
			yTopDepth = parent.getCurveLayer().getYSetTopDepth();
			yBottomDepth = parent.getCurveLayer().getYSetBottomDepth();
			float yDepth = Math.abs(yBottomDepth - yTopDepth);
			int componentheight = this.getHeight();
			// change the pixel to the actual depth,to find the proper depth
			float pixelPerDepth = componentheight / yDepth; // get one depth to pixel
			float temp = this.defaultLabelSpace / pixelPerDepth; // how many depth is the most near the best space
			if (temp < 1) {
				temp = 1;
			} else if (temp < 2) {
				temp = 2;
			} else if (temp < 5) {
				temp = 5;
			} else if (temp < 10) {
				temp = 10;
			} else if (temp < 20) {
				temp = 20;
			} else if (temp < 50) {
				temp = 50;
			} else {
				temp = ((int) (temp / 100) + 1) * 100;
			}
			// get the pixel from depth;

			float depthSpace = temp;
			if(depthSpace > 2*yDepth){
				coordinateValueList = new float[2];
				coordinatePointList = new float[2];
				coordinateValueList[0] = yTopDepth;
				coordinateValueList[1] = yBottomDepth;
				coordinatePointList[0] = 10;
				coordinatePointList[1] = this.getHeight() - 3;
			}else{
				int coordinateNumber = ConversionClass.round(yDepth / depthSpace) + 1;// every depthSpace,draw a string
				this.setYSpace(depthSpace * pixelPerDepth);

				coordinateValueList = new float[coordinateNumber];
				coordinatePointList = new float[coordinateNumber];

				for (int i = 0; i <= coordinateValueList.length - 1; i++) {
					coordinateValueList[i] = yTopDepth + i * depthSpace;
					if (i == 0) {
						coordinatePointList[i] = 10 + i * this.getYSpace();
					} else {
						coordinatePointList[i] = i * this.getYSpace();
					}

				}
				int tempbottom = (int) yBottomDepth;
				// change to int
				coordinateValueList[coordinateNumber - 1] = tempbottom;
				coordinatePointList[coordinateNumber - 1] = this.getHeight() - 3;
			}
		}

		reComputeFlag = false;
		//parent.repaintAll();
	}

	public int getYHeight(){
		reCompute();
		int trueHeight = 0;
		int pixValue = this.parent.getCurveView().getToolkit().getScreenResolution();
		float newHight = (this.parent.getCurveLayer().getYSetBottomDepth() - this.parent.getCurveLayer().getYSetTopDepth())
						/ this.parent.getDepthScale();
		if (this.parent.getDepthUnit() == CurveData.METER
				|| this.parent.getDepthUnit() == CurveData.SECOND) {
			trueHeight = (int) (newHight * ConstantVariableList.METER_TO_INCH * pixValue);
		} else {
			trueHeight = (int) (newHight * ConstantVariableList.FOOT_TO_INCH * pixValue);
		}
		return trueHeight;
	}

	public boolean isReComputeFlag() {
		return reComputeFlag;
	}

	public void setReComputeFlag(boolean reComputeFlag) {
		this.reComputeFlag = reComputeFlag;
	}

	public int getCoordinateNumber() {
		return coordinateValueList.length;
	}

	public void componentHidden(ComponentEvent e) {

	}

	public void componentMoved(ComponentEvent e) {

	}

	public void componentResized(ComponentEvent e) {
		this.setReComputeFlag(true);

	}

	public void componentShown(ComponentEvent e) {
		this.setReComputeFlag(true);

	}

	public float getYSpace() {
		return ySpace;
	}

	public void setYSpace(float space) {
		ySpace = space;
	}

	public String getYType() {
		return yType;
	}

	public void setYType(String type) {
		this.setReComputeFlag(true);
		yType = type;
	}

	public AxisCurveContainerView getParentAcc() {
		return this.parent;
	}

	public float[] getCoordinateValueList() {
		return coordinateValueList;
	}

	public float[] getCoordinatePointList() {
		return coordinatePointList;
	}

	public double getMaxYValue(){
		return coordinateValueList[coordinateValueList.length-1];
	}
}
