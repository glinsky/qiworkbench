/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.track;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

import com.gwsys.welllog.core.Renderable;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.LogarithmGenerator;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.PanelOrder;

/**
 * Draw Grid
 *
 * @author luming
 *
 */
public class GridLayer implements Renderable{
	private Color gridColor = ConstantVariableList.GRID_GRIDCOLOR;

	private Color backGroundColor = ConstantVariableList.GRID_BACKGROUND;

	private float gridXSpace = ConstantVariableList.GRID_XSPACE; // default

	private float gridYSpace = ConstantVariableList.GRID_YSPACE;

	private int lightLine = ConstantVariableList.GRID_GRIDS; // how many
																// lines between
																// two labels in
																// y axis

	private Shape myGrid = null;

	private AxisCurveContainerView parent = null;

	private float [] lineScales ;

	private int gridCount = 10;

	public GridLayer(AxisCurveContainerView parent) {
		this.parent = parent;
	}

	public Color getGridColor() {
		return gridColor;
	}

	public void setGridColor(Color gridColor) {
		this.gridColor = gridColor;
	}

	public Shape getMyGrid() {
		return myGrid;
	}

	public void draw(Graphics2D g2d,Rectangle rect){
		print(g2d, rect, 1, 1);
	}

	public void print(Graphics2D g2d,Rectangle rect,float rateWidth, float rateHeight) {
		CurveView cv = parent.getCurveView();
		g2d.setColor(gridColor);
		g2d.setStroke(new BasicStroke(cv.getDefaultstroke()));
		if (cv.isShowGrid()) {
			setMyGrid((int)(cv.getWidth()*rateWidth), (int)(cv.getHeight()*rateHeight),rateWidth,rateHeight);
			g2d.draw(getMyGrid());
		}
	}

	public void setMyGrid(float componentWidth, float componentHeight, float rateWidth, float rateHeight) {

		this.setGridYSpace((int)(parent.getYAxisView().getYSpace()*rateHeight / lightLine));
		float RowPoint[] = this.parent.getYAxisView().getCoordinatePointList();
		int RowCount = RowPoint.length;
		GeneralPath path = null;
		try {
			if (path == null) {
				path = new GeneralPath();
			}

			// horizonal line
			for (int i = 0; i < RowCount; i++) {
				int Top = (int)(RowPoint[i]*rateHeight);
				if(i==0){
					Top = (int)((RowPoint[i]-10)*rateHeight);
				}
				path.moveTo(0, Top);
				path.lineTo(componentWidth, Top);
				if(i < (RowCount-1)){
					for(int j=1;j<lightLine;j++){
						path.moveTo(0, Top+j*this.getGridYSpace());
						path.lineTo(componentWidth, Top+j*this.getGridYSpace());
					}
				}

			}

			// vertical line
			float start = 0.0f;
			float end = 0.0f;
			int j=0;
			PanelOrder panelOrder = parent.getCurveView().getPanelOrder();
			for (j = 0; j < panelOrder.getOrders().size(); j++) {
				if(!(panelOrder.getPanelKind(j) == PanelOrder.CURVE || panelOrder.getPanelKind(j) == PanelOrder.GROUP)){
					continue;
				}
				start = (int)(panelOrder.getPanelLeft(panelOrder.getPanel(j))*rateWidth);
				end = (int)(panelOrder.getPanelRight(panelOrder.getPanel(j))*rateWidth);
				if(panelOrder.getPanelKind(j) == PanelOrder.GROUP){
					makeLinear(start , end);
				}else{
					if(((CurvePanel)panelOrder.getPanel(j)).isLogarithm()){
						CurvePanel curvePanel = (CurvePanel)panelOrder.getPanel(j);
						makeLogarithmLinear(curvePanel, start, end);
					}else{
						makeLinear(start , end);
					}
				}
				//makeLinear(start , end);
				for(int i=0 ; i<gridCount ; i++){
					path.moveTo(lineScales[i] , 0);
					path.lineTo(lineScales[i] , componentHeight);
					if (i % lightLine == 0 && i != 0) {
						path.moveTo(lineScales[i] + 1, 0);
						path.lineTo(lineScales[i] + 1, componentHeight);
					}
				}

			}
			// The rest
			int allWidth = (int)(panelOrder.getPanelRight(panelOrder.getPanel(j-1))*rateWidth);
			start = allWidth;
			for (int i = 0; i < componentWidth-allWidth / (int)(ConstantVariableList.GRID_XSPACE*rateWidth); i++) {
				path.moveTo(start , 0);
				path.lineTo(start , componentHeight);
				if (i % lightLine == 0 && i != 0) {
					path.moveTo(start + 1, 0);
					path.lineTo(start + 1, componentHeight);
				}
				start = start + (int)(ConstantVariableList.GRID_XSPACE*rateWidth);
			}

			myGrid = path;
		} catch (Exception e) {
			System.out.println("Draw Grid error : " + e);
		}
	}

	public void makeLinear(float start , float end){
		gridCount = 10;
		lineScales = new float[gridCount];
		gridXSpace = (end - start) / gridCount;
		for(int i=0 ; i<gridCount ; i++){
			lineScales[i] = start + gridXSpace * i;
		}
	}

	public void makeLogarithmLinear(CurvePanel curvePanel, float start , float end){
		double d = curvePanel.getMinXDisplay();
		double d1 = curvePanel.getMaxXDisplay();
		if(d == 0.0D && d1 == 0.0D)
            return;
		double d2 = 0.0D;
        double d3 = Math.log(d) / Math.log(10D);
        double d4 = Math.log(d1) / Math.log(10D);
        double d5 = 0.0D;
        double d6 = 0.0D;
        d2 = 1.0D / (d4 - d3);
        d5 = -(d3 * d2);

        LogarithmGenerator logarithmGenerator = new LogarithmGenerator(d2, d5, 10d, true);
        logarithmGenerator.reset(0d,1d,0d,1d,240d);
        gridCount = logarithmGenerator.get_lastIdx() + 1 -logarithmGenerator.get_firstIdx();
        lineScales = new float[gridCount];
        int LineCount = 0;
        while(logarithmGenerator.hasNext()) {
        	logarithmGenerator.next();
        	//d6 = 1.0D - logarithmGenerator.getTickPosition();
        	d6 = logarithmGenerator.getTickPosition();
        	lineScales[LineCount] = start + (float)(d6*(end - start));
        	LineCount++;
        }
	}

	public void makeLogarithm(){
		// future
	}

	public float getGridXSpace() {
		return gridXSpace;
	}

	public float getGridYSpace() {
		return gridYSpace;
	}

	public Color getBackGroundColor() {
		return backGroundColor;
	}

	public void setBackGroundColor(Color backGroundColor) {
		this.backGroundColor = backGroundColor;
	}

	public void setGridXSpace(float gridXSpace) {
		this.gridXSpace = gridXSpace;
	}

	public void setGridYSpace(float gridYSpace) {
		this.gridYSpace = gridYSpace;
	}

	public void updateBorderByPoints(Point point, int top, int bottom) {

	}
}