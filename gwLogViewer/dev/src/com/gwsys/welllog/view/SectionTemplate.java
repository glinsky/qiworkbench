/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JDialog;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.gwsys.data.util.XMLFactory;

/**
 * Defines template for section.
 * Which curve, which order, which group, which color, which fill, what size
 * @author Louis
 *
 */
public class SectionTemplate {

	private static SectionTemplate instance ;

	private JDialog editor;

	private ArrayList dList;
	
	// added by Alex
	private static HashMap<String, ArrayList> wellTemplate = new HashMap<String, ArrayList>();
			
	private static DocumentBuilder builder;

	private SectionTemplate(){
		editor = initGUI();
	}

	public static SectionTemplate getInstance(){
		if (instance==null)
			instance = new SectionTemplate();
		return instance;
	}

	public void show(){
		editor.setVisible(true);
	}

	private JDialog initGUI(){
		JDialog dialog = new JDialog();
		//build GUI
		dialog.setTitle("Global Log Template");
		return dialog;
	}

	//CurveTemplate
	public ArrayList getDisplayList(){
		return dList;
	}
	
	/**
	 * put given well structure to templates
	 * @param key template name
	 * @param values template of this well
	 * @author Alex
	 */
	public static void addWellTemplate(String key, ArrayList values)	{
		wellTemplate.put(key, values);
	}
	
	/**
	 * get the values of given key
	 * @param wellKey
	 * @return ArrayList
	 * @author Alex
	 */
	public static ArrayList getWellTemplate(String key)	{
		return wellTemplate.get(key);
	}
	
	/**
	 * load a template since Xml file
	 * @param filename
	 * @return template's name
	 * @author Alex
	 */
	public static String loadXml(String filename)	{
		String template = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			System.out.println(pce);
			return null;
		}
		Document doc = null;
		try {
			doc = db.parse(filename);			
			Element root = doc.getDocumentElement();
			template = parseItems(root);
		} catch (DOMException dom) {
			System.out.println(dom);
			return null;
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return template;
	}
	
	/**
	 * get a curve item from DOM Element
	 * @param curveNode Element node
	 * @return curve item
	 * @author Alex
	 */
	private static CurveItem getCurveItemFromXML(Element curveNode)	{
		CurveItem item = new CurveItem(curveNode.getAttribute("name"));
		item.setColor(Integer.parseInt(curveNode.getAttribute("color")));
		item.setCurveStrokeType(Integer.parseInt(curveNode.getAttribute("strokeType")));
		item.setCurveStrokeWidth(Float.parseFloat(curveNode.getAttribute("strokeWidth")));
		item.setGridMode(Boolean.parseBoolean(curveNode.getAttribute("logarithm")));
		item.setMinValue(Float.parseFloat(curveNode.getAttribute("minXDisplay")));
		item.setMaxValue(Float.parseFloat(curveNode.getAttribute("maxXDisplay")));
		item.setReverse(Boolean.parseBoolean(curveNode.getAttribute("reverse")));
		
		return item;
	}
	
	/**
	 * get a group item from DOM Element
	 * @param groupNode Element node 
	 * @return group item
	 * @author Alex
	 */
	private static GroupItem getGroupItemFromXml(Element groupNode) {
		GroupItem item = new GroupItem(groupNode.getAttribute("name"));
		item.setGridMode(Boolean.parseBoolean(groupNode.getAttribute("logarithm")));
		item.setRangeValues(groupNode.getAttribute("minXDisplay"), groupNode.getAttribute("maxXDisplay"));
		item.setDisplayRange(Boolean.parseBoolean(groupNode.getAttribute("displayRange")));
		
		if (groupNode.hasChildNodes())	{
			NodeList nodes = groupNode.getChildNodes();
			
			for (int i = 0; i < nodes.getLength(); i++)	{
				Element curve = (Element) nodes.item(i);
				CurveItem curveItem = getCurveItemFromXML(curve);
				item.addCurve(curveItem);
			}
		}
		
		return item;
	}
	
	/**
	 * get a curve fill item from DOM Element
	 * @param fillNode Element node
	 * @return a curve fill item
	 * @author Alex
	 */
	private static CurveFillItem getCurveFillItemFromXml(Element fillNode)	{
		CurveFillItem item = new CurveFillItem(fillNode.getAttribute("curve"));
		item.setFillColor(Integer.parseInt(fillNode.getAttribute("fillColor")));
		
		if (fillNode.getAttribute("type").equals("CurveFillBeside"))	{
			item.setOrientation(fillNode.getAttribute("orientation"));
			item.setFillSingleType(true);
			item.setCurveIntoGroup(Boolean.parseBoolean(fillNode.getAttribute("isIntoGroup")));
			
			if (item.isIntoGroup())
				item.setGroup(fillNode.getAttribute("curveGroup"));
			
		} else if (fillNode.getAttribute("type").equals("CompoundFill"))	{
			item.setSecondCurve(fillNode.getAttribute("secondCurve"));
			item.setFillSingleType(false);
			item.setCurveIntoGroup(true);
			item.setGroup(fillNode.getAttribute("curveGroup"));
		}
		
		return item;
	}
	
	/** 
	 * get elements since a DOM Element to create a template and add this template
	 * @param root Element root
	 * @return template's name
	 * @author Alex
	 */
	@SuppressWarnings("unchecked")
	private static String parseItems(Element root) {
		ArrayList values = new ArrayList();
		//NodeList nodeList = root.getElementsByTagName("WellTemplate");
		Element well = (Element) root.getFirstChild();
		String templateName = well.getAttribute("name");
		
		NodeList list = well.getChildNodes();
		
		for (int i = 0; i < list.getLength(); i++)	{
			Element node = (Element) list.item(i);
			String type = node.getAttribute("type");
			
			if (type.equals("Curve"))	{
				CurveItem curve = getCurveItemFromXML(node);
				values.add(curve);
			}
			else if (type.equals("Group"))	{
				GroupItem group = getGroupItemFromXml(node);
				values.add(group);
			}
			else if (type.equals("CurveFillBeside") || type.equals("CompoundFill"))	{
				CurveFillItem curveFill = getCurveFillItemFromXml(node);
				values.add(curveFill);
			}			
		}
		
		// add this template
		addWellTemplate(templateName, values);
		
		return templateName;
	}

	/**
	 * save template of given well into XML file
	 * @param wellKey name of well template
	 * @param filename file name to save content
	 * @return true if template was saved, false if no
	 * @author Alex
	 */
	public static boolean saveContentIntoFile(String key, String filename)	{
		ArrayList templateContent = getWellTemplate(key);
		
		if (templateContent.size() < 1)	{
			System.out.println("There's no data to save");
			return false;
		}
		
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			//return false;
		}
		Document doc = builder.newDocument();
		
		Element root = doc.createElement("Template");
		Element well = doc.createElement("WellTemplate");
		well.setAttribute("name", key);
		//well.setAttribute("path", wellPath);
		
		for (int i = 0; i < templateContent.size(); i++)	{
			Object obj = templateContent.get(i);
			
			if (obj instanceof CurveItem)	{
				//System.out.println("is a Curve");
				Element item = getCurveElement(doc, (CurveItem)obj);				
				well.appendChild(item);
			} else if (obj instanceof GroupItem)	{
				//System.out.println("is a Group");
				Element item = getGroupElement(doc, (GroupItem)obj);
				well.appendChild(item);
			} else if (obj instanceof CurveFillItem)	{
				Element item = getCurveFillElement(doc, (CurveFillItem)obj);
				well.appendChild(item);
			}
			
		}
		root.appendChild(well);
		doc.appendChild(root);

		//Write xml into file
		XMLFactory.writeXml(doc, filename);
		
		return true;
	}
	
	/**
	 * returns a curve element to xml template
	 * @param doc Document
	 * @param curve CurveItem object
	 * @return curve element
	 * @author Alex
	 */
	private static Element getCurveElement(Document doc, CurveItem curve)	{
		Element citem = doc.createElement("CurveItem");
		citem.setAttribute("name", curve.getCurveName());
		citem.setAttribute("color", Integer.toString(curve.getColor()));
		citem.setAttribute("strokeWidth", Float.toString(curve.getCurveStrokeWidth()));
		citem.setAttribute("strokeType", Integer.toString(curve.getCurveStrokeType()));
		citem.setAttribute("logarithm", Boolean.toString(curve.isLogarithmic()));
		citem.setAttribute("minXDisplay", Float.toString(curve.getCurveMinValue()));
		citem.setAttribute("maxXDisplay", Float.toString(curve.getCurveMaxValue()));
		citem.setAttribute("reverse", Boolean.toString(curve.isReverse()));
		citem.setAttribute("type", "Curve");
		
		return citem;
	}
	
	/**
	 * returns a group element to xml template
	 * @param doc Document
	 * @param group GroupItem object
	 * @return group element
	 * @author Alex
	 */
	private static Element getGroupElement(Document doc, GroupItem group)	{
		Element gitem = doc.createElement("GroupItem");
		gitem.setAttribute("name", group.getGroupName());
		gitem.setAttribute("logarithm", Boolean.toString(group.isLogarithmic()));
		gitem.setAttribute("minXDisplay", group.getMinXDisplay());
		gitem.setAttribute("maxXDisplay", group.getMaxXDisplay());
		gitem.setAttribute("displayRange", Boolean.toString(group.isDisplayRange()));
		gitem.setAttribute("type", "Group");
		
		ArrayList curves = group.getCurveItems();
		
		if (curves.size() > 0)	{
			for (int i = 0; i < curves.size(); i++)	{
				CurveItem curve = (CurveItem) curves.get(i);
				Element citem = getCurveElement(doc, curve);
				gitem.appendChild(citem);
			}
		}
		
		return gitem;
	}
	
	/**
	 * returns a CurveFill element to sava in xml template 
	 * @param doc Document
	 * @param curveFill CurveFillItem object
	 * @return curve fill element
	 * @author Alex
	 */
	private static Element getCurveFillElement(Document doc, CurveFillItem curveFill)	{
		Element fItem = doc.createElement("CurveFillItem");
		fItem.setAttribute("curve", curveFill.getCurve());
		fItem.setAttribute("fillColor", Integer.toString(curveFill.getFillColor()));
		
		if (curveFill.isSingleFill())	{
			fItem.setAttribute("orientation", curveFill.getOrientation());
			fItem.setAttribute("type", "CurveFillBeside");
			
			if (curveFill.isIntoGroup())	{
				fItem.setAttribute("isIntoGroup", Boolean.toString(curveFill.isIntoGroup()));
				fItem.setAttribute("curveGroup", curveFill.getGroup());
			}
		}
		else	{
			fItem.setAttribute("secondCurve", curveFill.getSecondCurve());
			fItem.setAttribute("curveGroup", curveFill.getGroup());
			fItem.setAttribute("type", "CompoundFill");
		}
		
		return fItem;
	}
}
