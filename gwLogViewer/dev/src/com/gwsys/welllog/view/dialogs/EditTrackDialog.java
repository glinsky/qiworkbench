/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.data.unit.UnitDefinition;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.track.AxisCurveContainerView;

/**
 * when change Track Property,create a new EditTrackDialog
 *
 * @author gaofei
 *
 */
public class EditTrackDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	private InternalFrameView selectedInternalFrameView = null; // selected
																// InternalFrameView

	private AxisCurveContainerView selectedAxisCurveContainer = null; // selected
																		// AxisCurveContainerView

	private CurveView cv = null;

	private JPanel jContentPane = null;

	private String lastCurveType = "";

	// ///////the components of this Dialog/////////
	private JPanel jpanelTrack = null;

	JTextField tfTrack = null;

	private JPanel jpanelCurve = null;

	JTextField tfCurve = null;

	private JPanel jpanelDepth = null;

	JTextField tfDepth = null;

	private JPanel jpanelShow = null;

	JTextField tfShow = null;

	JLabel lblTrackName = null;

	JTextField tfTrackName = null;

	JLabel lblWidth = null;

	JTextField tfWidth = null; // width

	JLabel lblHeight = null;

	JTextField tfHeight = null; // height

	JLabel lblCurveType = null;

	JComboBox cbCurveType = null;

	JLabel lblFillType = null;

	JComboBox cbFillType = null;

	JCheckBox jcbGrid = null;

	JCheckBox jcbP105090 = null;

	JCheckBox jcbMean = null;

	JCheckBox jcbPlain = null;

	JCheckBox jcbMarker = null;

	JCheckBox jcbFill = null;

	JCheckBox jcbTransparent = null;

	JButton btnApply = null;

	JButton btnOk = null;

	JButton btnCancel = null;

	JCheckBox jcbScale = null;

	JLabel lblScale = null;

	JComboBox cbDepthscale = null;

	JLabel lblCurvePanelWidth = null;

	JTextField tfCurvePanelWidth = null;

	JCheckBox jcbDepthRange = null;

	JLabel lblDepthRangeMix = null;

	JLabel lblDepthRangeMax = null;

	JTextField tfDepthRangeMix = null;

	JTextField tfDepthRangeMax = null;
	
	JLabel lblUnit = null;
	
	JComboBox jcbUnit = null;

	String oldTrackName = "";

	boolean hasTrackId = false;

	int curveHeight = 0;

	float lastScale, ySetTopDepth, ySetBottomDepth;

	// /////////////////////////////////////////////

	/**
	 *
	 * @param owner
	 *            the parent of this EditTrackDialog,in this case is
	 *            MainFrameView
	 */
	public EditTrackDialog() {
		super(DataBuffer.getJMainFrame().getFrame());
		selectedInternalFrameView = DataBuffer.getJMainFrame().getMainBuffer()
				.getInternalFrameView();
		if (selectedInternalFrameView == null) {
			return;
		}
		selectedAxisCurveContainer = selectedInternalFrameView
				.getTrackContainer().getSelectedAxisCurveContainer();

		lastCurveType = selectedAxisCurveContainer.getCurveLayer()
				.getLayerType();
		cv = selectedAxisCurveContainer.getCurveView();

		initialize();
		this.setBounds(150, 100, 470, 530);

		this.setResizable(false);
		this.setModal(true);
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		this.setTitle("Change Track Property("
				+ selectedAxisCurveContainer.getTrackName() + ")");
		this.setResizable(true);
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);

			tfTrack = new JTextField();
			tfTrack.setBackground(SystemColor.control);
			tfTrack.setEnabled(false);
			tfTrack.setBorder(null);
			tfTrack.setDisabledTextColor(Color.black);
			tfTrack.setText(" Track Property");
			tfTrack.setBounds(new Rectangle(20, 10, 100, 16));

			jpanelTrack = new JPanel();
			jpanelTrack.setLayout(null);
			jpanelTrack.setBorder(BorderFactory.createEtchedBorder());
			jpanelTrack.setBounds(10, 17, 440, 60);

			lblTrackName = new JLabel("Name:");
			lblTrackName.setBounds(10, 20, 35, 25);
			oldTrackName = selectedAxisCurveContainer.getTrackName();
			/*
			 * int index =
			 * selectedAxisCurveContainer.getTrackName().indexOf(':'); String
			 * nameToModify = ""; if(index != -1){ hasTrackId = true;
			 * nameToModify =
			 * selectedAxisCurveContainer.getTrackName().substring(0 , index);
			 * }else nameToModify = selectedAxisCurveContainer.getTrackName();
			 */
			tfTrackName = new JTextField(oldTrackName);
			tfTrackName.setBounds(45, 20, 115, 25);

			int selectedCurveViewWidth = selectedAxisCurveContainer.getWidth();
			lblWidth = new JLabel("Width:");
			lblWidth.setBounds(170, 20, 40, 25);
			// tfWidth = new
			// JTextField(Integer.toString(selectedCurveViewWidth),20);//default
			// width is the width of the current selected AxisCurveContainerView
			tfWidth = new JTextField(Integer.toString(selectedCurveViewWidth));// default
																				// width
																				// is
																				// the
																				// width
																				// of
																				// the
																				// current
																				// selected
																				// AxisCurveContainerView
			tfWidth.setBounds(210, 20, 80, 25);
			int selectedCurveViewHeight = selectedAxisCurveContainer
					.getHeight();
			lblHeight = new JLabel("Height:");
			lblHeight.setBounds(300, 20, 50, 25);
			tfHeight = new JTextField(Integer.toString(selectedCurveViewHeight)); // default
																					// height
																					// is
																					// the
																					// height
																					// of
																					// the
																					// current
																					// selected
																					// AxisCurveContainerView
			tfHeight.setBounds(350, 20, 80, 25);

			jpanelTrack.add(lblTrackName);
			jpanelTrack.add(tfTrackName);
			jpanelTrack.add(lblWidth);
			jpanelTrack.add(tfWidth);
			jpanelTrack.add(lblHeight);
			jpanelTrack.add(tfHeight);

			tfCurve = new JTextField();
			tfCurve.setBackground(SystemColor.control);
			tfCurve.setEnabled(false);
			tfCurve.setBorder(null);
			tfCurve.setDisabledTextColor(Color.black);
			tfCurve.setText(" Curve Property");
			tfCurve.setBounds(new Rectangle(20, 90, 100, 16));

			jpanelCurve = new JPanel();
			jpanelCurve.setLayout(null);
			jpanelCurve.setBorder(BorderFactory.createEtchedBorder());
			jpanelCurve.setBounds(10, 97, 440, 60);

			lblCurveType = new JLabel("Type:");
			lblCurveType.setBounds(10, 20, 40, 25);
			cbCurveType = new JComboBox(); // default number is 1
			cbCurveType.addItem(TrackTypeList.REALIZATION_BESIDE);
			cbCurveType.addItem(TrackTypeList.REALIZATION_TOP);
			if (lastCurveType != null && !lastCurveType.equals("")) {
				cbCurveType.setSelectedItem(lastCurveType);
			}
			cbCurveType.setBounds(50, 20, 150, 25);

			lblCurvePanelWidth = new JLabel("Panel Width:");
			lblCurvePanelWidth.setBounds(210, 20, 80, 25);
			tfCurvePanelWidth = new JTextField(cv.getCurveLayer()
					.getBesideTypeWidth()
					+ "", 20);
			tfCurvePanelWidth.setBounds(290, 20, 80, 25);

			jpanelCurve.add(lblCurveType);
			jpanelCurve.add(cbCurveType);
			jpanelCurve.add(lblCurvePanelWidth);
			jpanelCurve.add(tfCurvePanelWidth);

			tfDepth = new JTextField();
			tfDepth.setBackground(SystemColor.control);
			tfDepth.setEnabled(false);
			tfDepth.setBorder(null);
			tfDepth.setDisabledTextColor(Color.black);
			tfDepth.setText(" Depth Property");
			tfDepth.setBounds(new Rectangle(20, 170, 100, 16));

			jpanelDepth = new JPanel();
			jpanelDepth.setLayout(null);
			jpanelDepth.setBorder(BorderFactory.createEtchedBorder());
			//jpanelDepth.setBounds(10, 177, 440, 120);
			jpanelDepth.setBounds(10, 177, 440, 150);
			
			//	for Track units
			lblUnit = new JLabel("Depth unit:");
			lblUnit.setBounds(140, 15, 120, 25);
			
			jcbUnit = new JComboBox();
			jcbUnit.addItem(UnitDefinition.METER);
			jcbUnit.addItem(UnitDefinition.FOOT);
			if (selectedAxisCurveContainer.getDepthUnit()==CurveData.METER)
				jcbUnit.setSelectedIndex(0);
			else
				jcbUnit.setSelectedIndex(1);
			jcbUnit.setBounds(260, 15, 80, 25);

			jcbDepthRange = new JCheckBox("Set depth range");
			jcbDepthRange.setBounds(10, 50, 120, 25);
			lblDepthRangeMix = new JLabel("Top depth:");
			lblDepthRangeMix.setBounds(140, 50, 120, 25);
			ySetTopDepth = selectedAxisCurveContainer.getCurveView()
					.getCurveLayer().getYSetTopDepth();
			if (selectedAxisCurveContainer.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)){
				ySetTopDepth = -ySetTopDepth;
			}
			tfDepthRangeMix = new JTextField(ySetTopDepth + "");
			tfDepthRangeMix.setBounds(260, 50, 80, 25);
			lblDepthRangeMax = new JLabel("Bottom depth:");
			lblDepthRangeMax.setBounds(140, 80, 150, 25);
			ySetBottomDepth = selectedAxisCurveContainer.getCurveView()
					.getCurveLayer().getYSetBottomDepth();
			if (selectedAxisCurveContainer.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)){
				ySetBottomDepth = -ySetBottomDepth;
			}
			tfDepthRangeMax = new JTextField(ySetBottomDepth + "");
			tfDepthRangeMax.setBounds(260, 80, 80, 25);

			jcbScale = new JCheckBox("Set depth scale");
			jcbScale.setBounds(10, 110, 120, 25);

			lblScale = new JLabel("Depth scale:   1:");
			lblScale.setBounds(140, 110, 120, 25);

			String[] depthScale = { "50", "200", "500", "1000", "2000", "5000",
					"10000" };
			cbDepthscale = new JComboBox(depthScale);
			cbDepthscale.setBounds(260, 110, 80, 25);
			
			jpanelDepth.add(jcbDepthRange);
			jpanelDepth.add(lblDepthRangeMix);
			jpanelDepth.add(tfDepthRangeMix);
			jpanelDepth.add(lblDepthRangeMax);
			jpanelDepth.add(tfDepthRangeMax);
			jpanelDepth.add(jcbScale);
			jpanelDepth.add(lblScale);
			jpanelDepth.add(cbDepthscale);
			
			// Track units
			jpanelDepth.add(lblUnit);
			jpanelDepth.add(jcbUnit);

			tfShow = new JTextField();
			tfShow.setBackground(SystemColor.control);
			tfShow.setEnabled(false);
			tfShow.setBorder(null);
			tfShow.setDisabledTextColor(Color.black);
			tfShow.setText(" Show Setting");
			//tfShow.setBounds(new Rectangle(20, 310, 100, 16));
			tfShow.setBounds(new Rectangle(20, 340, 100, 16));

			jpanelShow = new JPanel();
			jpanelShow.setLayout(null);
			jpanelShow.setBorder(BorderFactory.createEtchedBorder());
			//jpanelShow.setBounds(10, 317, 440, 110);
			jpanelShow.setBounds(10, 347, 440, 110);

			jcbGrid = new JCheckBox("BackGrid Layer");
			jcbGrid.setSelected(cv.isShowGrid());
			jcbGrid.setBounds(10, 15, 140, 25);
			//jcbGrid.setBounds(10, 45, 140, 25);
			jcbP105090 = new JCheckBox("P105090 Layer");
			jcbP105090.setSelected(cv.isShowP105090());
			jcbP105090.setBounds(150, 15, 140, 25);
			//jcbP105090.setBounds(150, 45, 140, 25);
			jcbP105090.setEnabled(false);
			jcbMean = new JCheckBox("Mean Layer");
			jcbMean.setSelected(cv.isShowMean());
			jcbMean.setBounds(290, 15, 140, 25);
			//jcbMean.setBounds(290, 45, 140, 25);
			jcbMean.setEnabled(false);
			jcbPlain = new JCheckBox("Plain Curve Layer");
			jcbPlain.setSelected(cv.isShowPlainCurve());
			jcbPlain.setBounds(10, 45, 140, 25);
			//jcbPlain.setBounds(10, 75, 140, 25);
			jcbMarker = new JCheckBox("Marker Layer");
			jcbMarker.setSelected(cv.isShowMarker());
			jcbMarker.setBounds(150, 45, 140, 25);
			//jcbMarker.setBounds(150, 75, 140, 25);
			jcbFill = new JCheckBox("Fill Layer");
			jcbFill.setSelected(cv.isShowFill());
			jcbFill.setBounds(290, 45, 140, 25);
			//jcbFill.setBounds(290, 75, 140, 25);
			jcbTransparent = new JCheckBox("Transparent Bg");
			jcbTransparent.setSelected(cv.isTransparentBackground());
			jcbTransparent.setBounds(10, 75, 140, 25);
			//jcbTransparent.setBounds(10, 105, 140, 25);

			jpanelShow.add(jcbGrid);
			jpanelShow.add(jcbP105090);
			jpanelShow.add(jcbMean);
			jpanelShow.add(jcbPlain);
			jpanelShow.add(jcbMarker);
			jpanelShow.add(jcbFill);
			jpanelShow.add(jcbTransparent);

			btnApply = new JButton(" Apply ");
			btnApply.setActionCommand("TrackPropertyDialog_btnApply");
			btnApply.addActionListener(this);
			//btnApply.setBounds(120, 440, 80, 25);
			btnApply.setBounds(120, 470, 80, 25);

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("TrackPropertyDialog_btnOk");
			btnOk.addActionListener(this);
			//btnOk.setBounds(200, 440, 80, 25);
			btnOk.setBounds(200, 470, 80, 25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("TrackPropertyDialog_btnCancel");
			btnCancel.addActionListener(this);
			//btnCancel.setBounds(280, 440, 80, 25);
			btnCancel.setBounds(280, 470, 80, 25);

			jContentPane.add(tfTrack);
			jContentPane.add(jpanelTrack);
			jContentPane.add(tfCurve);
			jContentPane.add(jpanelCurve);
			jContentPane.add(tfDepth);
			jContentPane.add(jpanelDepth);
			jContentPane.add(tfShow);
			jContentPane.add(jpanelShow);

			jContentPane.add(btnApply);
			jContentPane.add(btnOk);
			jContentPane.add(btnCancel);

		}
		return jContentPane;
	}

	public void setDepthRange() {
		ySetTopDepth = Float.parseFloat(tfDepthRangeMix.getText().trim());
		ySetBottomDepth = Float.parseFloat(tfDepthRangeMax.getText().trim());
		//float yTopDepth = selectedAxisCurveContainer.getCurveView().getCurveLayer().getYTopDepth();
		//float yBottomDepth = selectedAxisCurveContainer.getCurveView().getCurveLayer().getYBottomDepth();


		/*
		if (ySetTopDepth < yTopDepth) {
			ySetTopDepth = yTopDepth;
		}

		if (ySetBottomDepth > yBottomDepth) {
			ySetBottomDepth = yBottomDepth;
		}


		if(selectedAxisCurveContainer.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVD)){
			ySetTopDepth = selectedAxisCurveContainer.getMDDepthConverter().toDepth(ySetTopDepth,TrackTypeList.TVD_TO_MD);
			ySetBottomDepth = selectedAxisCurveContainer.getMDDepthConverter().toDepth(ySetBottomDepth,TrackTypeList.TVD_TO_MD);
		}else if(selectedAxisCurveContainer.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS )){
			ySetTopDepth = selectedAxisCurveContainer.getMDDepthConverter().toDepth(-ySetTopDepth,TrackTypeList.TVDSS_TO_MD);
			ySetBottomDepth = selectedAxisCurveContainer.getMDDepthConverter().toDepth(-ySetBottomDepth,TrackTypeList.TVDSS_TO_MD);
		}
		*/
		if (ySetTopDepth >= ySetBottomDepth){
			return;
		}

		selectedAxisCurveContainer.getCurveView().getCurveLayer()
				.setYSetTopDepth(ySetTopDepth);
		selectedAxisCurveContainer.getCurveView().getCurveLayer()
				.setYSetBottomDepth(ySetBottomDepth);
		selectedAxisCurveContainer.getCurveView().getCurveLayer().enableCustomizedRange(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("TrackPropertyDialog_btnOk")) {
			apply();
			this.setVisible(false);
			this.dispose();
			return;
		} else if (e.getActionCommand().equals("TrackPropertyDialog_btnApply")) {
			apply();
		} else if (e.getActionCommand().equals("TrackPropertyDialog_btnCancel")) {
			this.setVisible(false);
			this.dispose();
			return;
		}

	}

	public float getScale() {
		CurveView cv = selectedAxisCurveContainer.getCurveView();
		curveHeight = cv.getHeight();
		int height = cv.getHeight();
		int pixValue = cv.getToolkit().getScreenResolution();
		float newHight = cv.getParentAxisCurveContainer().getCurveLayer()
				.getYSetBottomDepth()
				- cv.getParentAxisCurveContainer().getCurveLayer()
						.getYSetTopDepth();
		int trueHeight = 0;
		if (cv.getParentAxisCurveContainer().getDepthUnit() == CurveData.METER
				|| cv.getParentAxisCurveContainer().getDepthUnit() == CurveData.SECOND) {
			trueHeight = (int) (newHight * ConstantVariableList.METER_TO_INCH * pixValue);
		} else {
			trueHeight = (int) (newHight * ConstantVariableList.FOOT_TO_INCH * pixValue);
		}

		return (float) trueHeight / (float) height;
	}

	public void setScale(boolean setScale) {
		AxisCurveContainerView selectedAxisCurveContainerView = DataBuffer
				.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer();
		if (selectedAxisCurveContainerView == null) {
			return;
		}

		if (setScale) {
			float scale = Float.parseFloat(cbDepthscale.getSelectedItem()
					.toString().trim());
			selectedAxisCurveContainerView.setDepthScale(scale);
			ViewCoreOperator.setDepthScale(selectedAxisCurveContainerView);
		} else {
			selectedAxisCurveContainerView.getJp().revalidate();
			selectedAxisCurveContainerView.getJp().revalidate();
			selectedAxisCurveContainerView.getJp().repaint();
		}
	}

	public void apply() {
		String newTrackName = tfTrackName.getText().trim();
		selectedAxisCurveContainer.setTrackName(newTrackName);
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().renameTrack(
				DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTitle(),
				selectedAxisCurveContainer.getWellName(),newTrackName,selectedAxisCurveContainer.getWellName());

		int curvePanelWidth = Integer.parseInt(tfCurvePanelWidth.getText()
				.trim());
		cv.getCurveLayer().setBesideTypeWidth(curvePanelWidth);

		NumberFormat numberFormat = NumberFormat.getIntegerInstance();
		numberFormat.setParseIntegerOnly(true);
		int width = 0;
		try {
			width = numberFormat.parse(tfWidth.getText().trim()).intValue();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		int height = 0;
		try {
			height = numberFormat.parse(tfHeight.getText().trim()).intValue();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		String curveType = cbCurveType.getSelectedItem().toString();

		if (selectedInternalFrameView == null) {
			this.setVisible(false);
			this.dispose();
			return;
		}
		if (selectedAxisCurveContainer == null) {
			this.setVisible(false);
			this.dispose();
			return;
		}

		cv.setCurveRedrawFlag(true);
		cv.setFillRedrawFlag(true);
		
		// change unit of Track. may.31.2007
		int unit = 1;
		if (jcbUnit.getSelectedIndex()==0)
			unit = CurveData.METER;
		else
			unit = CurveData.FOOT;
		
		selectedAxisCurveContainer.setDepthUnit(unit);
		
		// this codes were moved here, from below
		if (this.jcbDepthRange.isSelected()) {
			setDepthRange();
		}

		if (this.jcbScale.isSelected()) {
			setScale(true);
		} else {
			setScale(false);
		}

		selectedAxisCurveContainer.getYAxisView().setReComputeFlag(true);
		selectedAxisCurveContainer.getCurveLayer().setLayerType(curveType);
		selectedAxisCurveContainer
				.setPreferredSize(new Dimension(width, height));
		selectedAxisCurveContainer.setSize(width, height);

		cv.setShowFill(this.jcbFill.isSelected());
		cv.setShowGrid(this.jcbGrid.isSelected());
		cv.setShowMarker(this.jcbMarker.isSelected());
		cv.setShowMean(this.jcbMean.isSelected());
		cv.setShowP105090(this.jcbP105090.isSelected());
		cv.setShowPlainCurve(this.jcbPlain.isSelected());
		cv.setTransparentBackground(this.jcbTransparent.isSelected());
		cv.setProperSize();

		if (!lastCurveType.equals(curveType)) {

			if (curveType.equals(TrackTypeList.REALIZATION_TOP)) {
				selectedAxisCurveContainer
						.setBesideWidth(selectedAxisCurveContainer.getWidth());
				selectedAxisCurveContainer.setSize(new Dimension(cv
						.getCurveLayer().getBesideTypeWidth()
						+ selectedAxisCurveContainer.getYAxisView().getWidth(),
						selectedAxisCurveContainer.getHeight()));
				DataBuffer.getJMainFrame().getMainBuffer()
						.getInternalFrameView().getTrackContainer()
						.updateAxisCurveContainersLocation();
			} else {
				if (selectedAxisCurveContainer.getBesideWidth() == 0) {
					selectedAxisCurveContainer
							.setBesideWidth(selectedAxisCurveContainer
									.getWidth());
				}
				selectedAxisCurveContainer.setBounds(selectedAxisCurveContainer
						.getX(), 0,
						selectedAxisCurveContainer.getBesideWidth(),
						selectedAxisCurveContainer.getHeight());
			}
		}
				
		selectedAxisCurveContainer.getParentContainer().getParentInternalFrameView().repaintTrack();
	}
}
