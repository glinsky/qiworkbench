/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * a class to implements mouse listeners for MapPanel mouse events
 * 
 * @author Alex
 *
 */
public class MapController implements MouseListener, MouseMotionListener {
	MapPanel wellmap;
	int xini, yini, xfin, yfin;
	
	public MapController(MapPanel map)	{
		this.wellmap = map;
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3)	{
			wellmap.restoreInitialValues();
			wellmap.repaint();
		}
	}

	public void mousePressed(MouseEvent e) {
		if ((e.getX() > 100 && e.getX() < 605) && (e.getY() > 15 && e.getY() < 370) && (e.getButton() == MouseEvent.BUTTON1))	{
			xini = e.getX();
			yini = e.getY();
			wellmap.setShowRectangleSelection(true);
			wellmap.setAxisPoints(xini, yini, 0);
		}
	}

	public void mouseReleased(MouseEvent e) {		
		xfin = e.getX();
		yfin = e.getY();
		
		if (xfin == xini || yfin == yini)
			return;
		
		if (wellmap.showRectangleSelection())	{
			wellmap.setAxisPoints(e.getX(), e.getY(), 1);
			wellmap.setShowRectangleSelection(false);
			wellmap.setWellValuesFromCoordenates(xfin, xini, yfin, yini);
			wellmap.repaint();
		}			
	}
	
	public void mouseEntered(MouseEvent e) {
				
	}
	
	public void mouseExited(MouseEvent e) {
		
	}

	
	public void mouseDragged(MouseEvent e) {			
		if (wellmap.showRectangleSelection())	{
			wellmap.setAxisPoints(e.getX(), e.getY(), 1);
			wellmap.repaint();
		}		
	}

	public void mouseMoved(MouseEvent e) {
	
		
	}
}