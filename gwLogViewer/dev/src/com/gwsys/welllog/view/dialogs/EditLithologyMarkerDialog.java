/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.core.curve.WellLogPattern;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.fill.MarkerLithology;
import com.gwsys.welllog.view.zone.ZoneProp;

/**
 * Dialog edits the properites of lithology.
 * @author Team
 *
 */
public class EditLithologyMarkerDialog extends JDialog implements ActionListener {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private JPanel jContentPanel = null;

	private JLabel lblType = null;

	private JLabel lblColor = null;						// for color label

	private JButton btnShowColor = null;				//for button color


	private JComboBox cbType = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JLabel lblRemove = null;

	private JButton btnRemove = null;

	private CurveView cv = null;

	private MarkerLithology ml = null;

	/**
	 * Constructor.
	 * @param cv CurveView
	 */
	public EditLithologyMarkerDialog(CurveView cv){
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Lithology Property Dialog");
		this.cv = cv;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,230,140);
		this.setResizable(false);
		this.setModal(true);
	}

	public EditLithologyMarkerDialog(CurveView cv,MarkerLithology ml){
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Lithology Property Dialog");
		this.cv = cv;
		this.ml = ml;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,260,170);
		this.setResizable(false);
		this.setModal(true);
	}

	public JPanel getContentPanel() {
		if (jContentPanel == null) {
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);

			lblType = new JLabel("Type:");

			WellLogPattern choise[] = ZoneProp.getWellLogPatterns();
			cbType = new JComboBox(choise);

			FillComboBoxRenderer renderer= new FillComboBoxRenderer();
			renderer.setPreferredSize(new Dimension(80, 25));
	        cbType.setRenderer(renderer);
	        if(ml != null)
	        {
	        	cbType.setSelectedIndex(ml.getWhichtexture());
	        }
			if (ml != null) {
				lblRemove = new JLabel("Remove Lithology:");

				btnRemove = new JButton("remove");
				btnRemove.setActionCommand("remove");
				btnRemove.addActionListener(this);
			}
			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);

			lblColor = new JLabel("Color:");			//start for color option
			btnShowColor = new JButton("Change");
			btnShowColor.setActionCommand("showColorChooser");
			if(ml == null){
				btnShowColor.setForeground(Color.YELLOW);
			}else{
				btnShowColor.setForeground(ml.getFillColor());
			}
			btnShowColor.addActionListener(this);		//end color option...

			if (ml != null) {
				lblType.setBounds(10,10,110,25);
				cbType.setBounds(120,10,120,25);
				lblColor.setBounds(10,40,110,25);
				btnShowColor.setBounds(120,40,120,25);
				lblRemove.setBounds(10,70,110,25);
				btnRemove.setBounds(120,70,120,25);
				btnOk.setBounds(50,110,80,25);
				btnCancel.setBounds(130,110,80,25);

				jContentPanel.add(lblType);
				jContentPanel.add(cbType);
				jContentPanel.add(lblColor);
				jContentPanel.add(btnShowColor);
				jContentPanel.add(lblRemove);
				jContentPanel.add(btnRemove);
				jContentPanel.add(btnOk);
				jContentPanel.add(btnCancel);
			}else{
				lblType.setBounds(10,10,80,25);
				cbType.setBounds(90,10,120,25);
				lblColor.setBounds(10,40,80,25);
				btnShowColor.setBounds(90,40,120,25);
				btnOk.setBounds(30,80,80,25);
				btnCancel.setBounds(110,80,80,25);

				jContentPanel.add(lblType);
				jContentPanel.add(cbType);
				jContentPanel.add(lblColor);
				jContentPanel.add(btnShowColor);
				jContentPanel.add(btnOk);
				jContentPanel.add(btnCancel);
			}

		}
		return jContentPanel;
	}


	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok"))
		{
			if(ml == null ){		//create new curvefill
				ml = new MarkerLithology();
				//mf = new MarkerFill();												//color
				ArrayList list = cv.getMarkerLayer().getMarkerSelectedList();
				for(int i = 0 ; i<list.size()-1;i++)
				{
					ml.setCurveMarker((CurveMarker)list.get(i),(CurveMarker)list.get(i+1));
					ml.setFillColor(this.btnShowColor.getForeground());
					//ml.constructFill(cv);
					ml.setWhichtexture(cbType.getSelectedIndex());
					cv.getMarkerLayer().getMarkerLithologyList().add(ml);

				}
			}else{	//change curvefill property
				ml.setWhichtexture(cbType.getSelectedIndex());				//color
			}
			cv.repaintFather();
			this.setVisible(false);
			this.dispose();
			return;
		}

		/////////when button cancel is clicked//////////
		else if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}

		else if(e.getActionCommand().equals("showColorChooser"))												//add color
		{
			Color selected=JColorChooser.showDialog(this,
					"Choose Curve Color", btnShowColor.getForeground());
			if (selected!=null){
				btnShowColor.setForeground(selected);
			}
		}																								//add color

		else if(e.getActionCommand().equals("remove")){
			cv.getMarkerLayer().getMarkerLithologyList().remove(ml);
			cv.repaintFather();
			this.setVisible(false);
			this.dispose();
			return;
		}
	}


	class FillComboBoxRenderer extends JLabel implements ListCellRenderer {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		private WellLogPattern pattern;

		public FillComboBoxRenderer() {
			setOpaque(true);
		}

		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {

			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			pattern = (WellLogPattern) value;

			return this;
		}

		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setPaint(pattern);

			g2.fillRect(0, 0, getWidth(), getHeight());
		}
	}
}
