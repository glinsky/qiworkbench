package com.gwsys.welllog.view.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.gwsys.data.well.marker.AsciiFileOperator;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.curve.OperationClass;

/**
 * shows a preview of marker file content and let user 
 * choose delimiter and set column data order
 * 
 * @author Alex
 *
 */
public class MarkerDataViewer extends JDialog implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JPanel main;
	private JPanel topPanel;
	private JPanel buttonPanel;
	private JPanel leftPanel;
	private JPanel rightPanel;
	private JPanel dataPanel;
	private JPanel columnPanel;
	private JPanel textPanel;
	
	private JLabel charsetLbl;
	private JComboBox charsetCB;
	private JLabel delimiterLbl;
	private JComboBox delimiterCB;
	
	private File file;
	private String charset = "Cp1252";
	private MainFrameView jMainFrame;
	
	private JTextArea textBuffer;
	private JLabel wellLbl;
	private JLabel markerLbl;
	private JLabel depthLbl;
	private JComboBox wellCB;
	private JComboBox markerCB;
	private JComboBox depthCB;
	
	private JButton okBtn;
	
	public MarkerDataViewer(File file, MainFrameView jMainFrame)	{
		this.file = file;
		this.jMainFrame = jMainFrame;
		createGUI();
	}
	
	private void createGUI()	{
		this.setTitle("Preview of Marker file");
		this.setSize(500,320);
		this.setVisible(false);
					
		topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
		
		okBtn = new JButton("Accept");
		okBtn.addActionListener(this);
		okBtn.setActionCommand("ok_action");
		
		buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(okBtn);
		
		setLeftPanel();
		setRightPanel();
		
		main = new JPanel(new BorderLayout());
		main.add(topPanel, BorderLayout.CENTER);
		main.add(buttonPanel, BorderLayout.SOUTH);
		
		setContentPane(main);
	}
	
	/**
	 * 
	 */
	private void setLeftPanel() {
		leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		leftPanel.setPreferredSize(new Dimension(250,100));
		
		setDataPanel();
		setColumnsPanel();
		
		topPanel.add(leftPanel);
	}
	
	/**
	 * 
	 */
	private void setRightPanel() {
		rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		
		setTextArea();
		topPanel.add(rightPanel);
	}

	private void setDataPanel()	{
		dataPanel = new JPanel();
		dataPanel.setLayout(null);
		dataPanel.setBorder(BorderFactory.createTitledBorder("choose charset and delimiter"));
		
		charsetLbl = new JLabel("Charset:");
		charsetLbl.setBounds(30,30,50,15);
		charsetCB = new JComboBox(new String[]
		                              {"Cp1252", "ISO-8859-1", "UTF-8", "US-ASCII", "Unicode"});
		charsetCB.setSelectedIndex(0);
		charsetCB.setBounds(100,30,70,20);
		charsetCB.addActionListener(this);
		charsetCB.setActionCommand("change_charset");
		
		delimiterLbl = new JLabel("Delimiter:");
		delimiterLbl.setBounds(30,65,50,15);
		delimiterCB = new JComboBox(new String[]
             			              {"Tab","Space","Comma","Semicolon"});
		delimiterCB.setBounds(100,65,70,20);
		
		dataPanel.add(charsetLbl);
		dataPanel.add(charsetCB);
		dataPanel.add(delimiterLbl);
		dataPanel.add(delimiterCB);
		
		leftPanel.add(dataPanel);
	}
	
	private void setColumnsPanel()	{
		columnPanel = new JPanel();
		columnPanel.setBorder(BorderFactory.createTitledBorder("Set column data order"));
		columnPanel.setLayout(null);
		
		wellLbl = new JLabel("Well:");
		wellLbl.setBounds(30,30,70,10);
		markerLbl = new JLabel("Marker:");
		markerLbl.setBounds(30,60,70,10);
		depthLbl = new JLabel("Depth value:");
		depthLbl.setBounds(30,90,70,10);
		
		wellCB = new JComboBox(new String[] {"1", "2", "3"});
		wellCB.setSelectedIndex(0);
		wellCB.setBounds(120,28,35,17);
		markerCB = new JComboBox(new String[] {"1", "2", "3"});
		markerCB.setSelectedIndex(1);
		markerCB.setBounds(120,58,35,17);
		depthCB = new JComboBox(new String[] {"1", "2", "3"});
		depthCB.setSelectedIndex(2);
		depthCB.setBounds(120,88,35,17);
		
		columnPanel.add(wellLbl);
		columnPanel.add(markerLbl);
		columnPanel.add(depthLbl);
		columnPanel.add(wellCB);
		columnPanel.add(markerCB);
		columnPanel.add(depthCB);
		
		leftPanel.add(columnPanel);
	}
	
	private void setTextArea()	{
		textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.PAGE_AXIS));
		textPanel.setBorder(BorderFactory.createTitledBorder("Content (first 10 lines)"));
		
		textBuffer = new JTextArea(12,15);
		//textBuffer.setPreferredSize(new Dimension(15,20));
		textBuffer.setEditable(false);
		
		updateTextContent();
		
		JScrollPane scroll = new JScrollPane(textBuffer, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
										JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setPreferredSize(new Dimension(300,200));
		textPanel.add(scroll);
		rightPanel.add(textPanel);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("ok_action"))	{
			if (validSelection())	{
				AsciiFileOperator operator = new AsciiFileOperator();
				operator.setColumnOrder(wellCB.getSelectedIndex(), markerCB.getSelectedIndex(), depthCB.getSelectedIndex());
				operator.setCharset(charsetCB.getSelectedItem().toString());				
				operator.setDelimiter(getDelimiter());
				
				try {
					operator.read(this.file);
					if (operator.isReady())	{
						ArrayList markerList = operator.getMarkerList();
						OperationClass.completeWellTopsTree(this.jMainFrame, markerList, this.file.getPath());
					}					
					finish();
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(jMainFrame.getFrame() ,
							"Can not read marker file : " + this.file.getName() +
							"\nError:\nThe wrong file format.",
							"Error" , JOptionPane.ERROR_MESSAGE);
				}
			}
			else
				JOptionPane.showMessageDialog(this, "You must choose different values!");
		}
		else if (e.getActionCommand().equals("change_charset"))	{
			charset = charsetCB.getSelectedItem().toString();
			updateTextContent();			
		}
	}
	
	/**
	 * get selected delimiter character
	 * @return delimiter
	 */
	private String getDelimiter()	{
		String delimiter = "";
		
		switch (this.delimiterCB.getSelectedIndex())	{
		case 0: delimiter = "\t";
				break;
		case 1: delimiter = " ";
				break;
		case 2: delimiter = ",";
				break;
		case 3: delimiter = ";";
		}
		
		return delimiter;
	}
	
	/**
	 * set file content in text Area
	 *
	 */
	private void updateTextContent()	{
		int counter = 0;
		String line;
		
		textBuffer.setText("");
				
		try {
			FileInputStream fs = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fs, charset);
			BufferedReader reader = new BufferedReader(isr);
			
			while ((line = reader.readLine())!=null) {
				textBuffer.append(line + "\n");
				counter++;
				
				if (counter==10)
					break;
			}
			
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * check if combo boxes' selected index are differents
	 * @return true or false
	 */
	private boolean validSelection() {
		int a = this.wellCB.getSelectedIndex();
		int b = this.markerCB.getSelectedIndex();
		int c = this.depthCB.getSelectedIndex();
		
		if (a != b && a != c && b != c)
			return true;
		
		return false;
	}
	
	/**
	 * ends the process
	 *
	 */
	private void finish() {
		this.setVisible(false);
		this.dispose();
	}
}
