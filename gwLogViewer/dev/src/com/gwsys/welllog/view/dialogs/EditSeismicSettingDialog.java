/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.SeismicPanel;

/**
 * Dialog edits the seismic panel.
 * @author Administrator
 * 
 */
public class EditSeismicSettingDialog extends JDialog implements
		ActionListener {
	private static final long serialVersionUID = 1L;

	private JPanel jContentPanel = null;

	private SeismicPanel sp = null;

	private JLabel lblName = null;
	private JTextField tfName = null;
	private JLabel lblWidth = null;
	private JTextField tfWidth = null;
	private JLabel lblTraceNumber = null;
	private JTextField tfTraceNumber = null;
	private JLabel lblCurveColor = null;
	private JButton btnShowCurveColor = null;
	private JLabel lblType = null;
	private JComboBox cbType = null;
	
	private JButton btnOk = null;

	private JButton btnCancel = null;

	public EditSeismicSettingDialog(SeismicPanel sp) {
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Seismic Settings");
		this.sp = sp;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,420,180);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	public JPanel getContentPanel() {
		if (jContentPanel == null) {
			jContentPanel = new JPanel();

			jContentPanel.setLayout(null);

			lblName = new JLabel("Seismic Name:");
			lblName.setBounds(10,10,90,25);
			tfName = new JTextField();
			if (this.sp != null) {
				tfName.setText(sp.getName());
			}
			tfName.setBounds(100,10,120,25);

			lblWidth = new JLabel("Seismic Width:");
			lblWidth.setBounds(10,40,90,25);
			tfWidth = new JTextField();
			if (this.sp != null) {
				tfWidth.setText(sp.getBesideTypeWidth() + "");
			}
			tfWidth.setBounds(100,40,120,25);
			
			lblTraceNumber = new JLabel("Trace Number:");
			lblTraceNumber.setBounds(230,40,90,25);
			tfTraceNumber = new JTextField();
			if(this.sp != null){
				tfTraceNumber.setText(sp.getTraceNumber() + "");
			}
			tfTraceNumber.setBounds(320,40,80,25);

			lblType = new JLabel("Stroke Type:");
			lblType.setBounds(10,70,90,25);
    		ImageIcon [] icons = new ImageIcon[4];
			icons[0] = IconResource.getInstance().getImageIcon("solid.gif");
			icons[1] = IconResource.getInstance().getImageIcon("dash.gif");
			icons[2] = IconResource.getInstance().getImageIcon("dashdot.gif");
			icons[3] = IconResource.getInstance().getImageIcon("dot.gif");
			cbType = new JComboBox(icons);
			cbType.setBounds(100,70,120,25);			
			if(sp.getStokeType() == SeismicPanel.BEELINE)
				cbType.setSelectedIndex(0);
			else if(sp.getStokeType() == SeismicPanel.DASHED)
				cbType.setSelectedIndex(1);	
			else if(sp.getStokeType() == SeismicPanel.DASH_DOT)
				cbType.setSelectedIndex(2);
			else if(sp.getStokeType() == SeismicPanel.DOT)
				cbType.setSelectedIndex(3);
			
			lblCurveColor = new JLabel("Curve Color:");
			lblCurveColor.setBounds(230,70,90,25);
			btnShowCurveColor = new JButton("Change");
			btnShowCurveColor.setForeground(sp.getCurveColor());
			btnShowCurveColor.setActionCommand("showCurveColorChooser");
			btnShowCurveColor.addActionListener(this);
			btnShowCurveColor.setBounds(320,70,100,25);
			
			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(130,110,80,25);
			
			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(210,110,80,25);
			
			jContentPanel.add(lblName);
			jContentPanel.add(tfName);
			jContentPanel.add(lblWidth);
			jContentPanel.add(tfWidth);
			jContentPanel.add(lblTraceNumber);
			jContentPanel.add(tfTraceNumber);
			jContentPanel.add(lblCurveColor);
			jContentPanel.add(btnShowCurveColor);			
			jContentPanel.add(lblType);
			jContentPanel.add(cbType);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("ok")) {

			if (!tfName.getText().trim().equals("")) {
				String newName = tfName.getText().trim();
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
						.renameSeismicPanel(
								DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTitle(),
								DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
										.getTrackContainer()
										.getSelectedAxisCurveContainer()
										.getWellName(), sp.getName(), newName);
				sp.setName(newName);
			}

			if (!tfWidth.getText().trim().equals("")) {
				sp.setBesideTypeWidth(Integer
						.parseInt(tfWidth.getText().trim()));				
			}
			
			if (!tfTraceNumber.getText().trim().equals("")) {
				sp.setTraceNumber(Integer
						.parseInt(tfTraceNumber.getText().trim()));
			}

			sp.setCurveColor(this.btnShowCurveColor.getForeground());

			if(cbType.getSelectedIndex() == 0)
				sp.setStokeType(SeismicPanel.BEELINE);
			else if(cbType.getSelectedIndex() == 1)
				sp.setStokeType(SeismicPanel.DASHED);	
			else if(cbType.getSelectedIndex() == 2)
				sp.setStokeType(SeismicPanel.DASH_DOT);	
			else if(cbType.getSelectedIndex() == 3)
				sp.setStokeType(SeismicPanel.DOT);	
						
			sp.getParent().setFillRedrawFlag(true);
			sp.getParent().repaintFather();
			
			this.setVisible(false);
			this.dispose();
			return;
		}

		else if (e.getActionCommand().equals("cancel")) {
			this.setVisible(false);
			this.dispose();
			return;
		}
		
		else if(e.getActionCommand().equals("showCurveColorChooser"))
		{
			Color selected=JColorChooser.showDialog(this, 
					"Choose Curve Color", btnShowCurveColor.getForeground());
			if (selected!=null){
				btnShowCurveColor.setForeground(selected);
			}
		}
	}

}