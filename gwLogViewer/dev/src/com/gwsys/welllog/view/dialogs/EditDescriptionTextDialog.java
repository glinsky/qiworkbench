/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DebugGraphics;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.DescriptionObject;
import com.gwsys.welllog.view.curve.DescriptionPanel;

/**
 * @author Administrator
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EditDescriptionTextDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JPanel jContentPanel = null;
	private DescriptionPanel dp = null;
	private DescriptionObject descriptionObject = null;
	
	private JLabel lblText = null;
	private JTextArea tfText = null;
	private JLabel lblColor = null;
	private JComboBox cbColor = null;
	private JButton btnOk = null;
	private JButton btnCancel = null;	
	private JScrollPane scrollPane = null;
	
	private String text = "";
	
	private Color selectColor = Color.black;
	
	public EditDescriptionTextDialog(DescriptionPanel dp){
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Insert Description Text");
		this.dp = dp;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,280,230);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}
	
	public EditDescriptionTextDialog(DescriptionPanel dp, DescriptionObject descriptionObject){
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Edit Description Text");
		this.dp = dp;
		this.descriptionObject = descriptionObject;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,280,230);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}
	
	public JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			
			jContentPanel.setLayout(null);		
			
			lblText = new JLabel("Text:");
			lblText.setBounds(10,10,40,25);
			tfText = new JTextArea(5,10);
			if(this.descriptionObject != null){
				tfText.setText(descriptionObject.getText());
			}			
			tfText.setLineWrap(true);			
			scrollPane = new JScrollPane();
			scrollPane.setDebugGraphicsOptions(DebugGraphics.LOG_OPTION);
			scrollPane.getViewport().add(tfText, null);
			scrollPane.setBounds(50,10,210,100);
			
			lblColor = new JLabel("Color:");
			lblColor.setBounds(10,120,40,25);
			ImageIcon [] icons = new ImageIcon[8];
			icons[0] = IconResource.getInstance().getImageIcon("colorBlack.gif");
			icons[1] = IconResource.getInstance().getImageIcon("colorBlue.gif");
			icons[2] = IconResource.getInstance().getImageIcon("colorCyan.gif");
			icons[3] = IconResource.getInstance().getImageIcon("colorGreen.gif");
			icons[4] = IconResource.getInstance().getImageIcon("colorOrange.gif");
			icons[5] = IconResource.getInstance().getImageIcon("colorMagenta.gif");
			icons[6] = IconResource.getInstance().getImageIcon("colorRed.gif");
			icons[7] = IconResource.getInstance().getImageIcon("colorYellow.gif");
			cbColor = new JComboBox(icons);
			cbColor.setBounds(50,120,120,25);
			
			Color color = Color.blue;			
			if(this.descriptionObject != null)
				color = descriptionObject.getTextColor();
			else
				color = dp.getDefaultTextColor();
							
			if(color == Color.black)
				cbColor.setSelectedIndex(0);
			else if(color == Color.blue)
				cbColor.setSelectedIndex(1);
			else if(color == Color.cyan)
				cbColor.setSelectedIndex(2);
			else if(color == Color.green)
				cbColor.setSelectedIndex(3);
			else if(color == Color.orange)
				cbColor.setSelectedIndex(4);
			else if(color == Color.magenta)
				cbColor.setSelectedIndex(5);
			else if(color == Color.red)
				cbColor.setSelectedIndex(6);
			else if(color == Color.yellow)
				cbColor.setSelectedIndex(7);
			
			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(60,160,80,25);
			
			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(140,160,80,25);
			
			jContentPanel.add(lblText);
			jContentPanel.add(scrollPane);
			jContentPanel.add(lblColor);
			jContentPanel.add(cbColor);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);
			
			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}
	
	public Color getSelectColor() {
		return selectColor;
	}

	public String getText() {
		return text;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok"))
		{
			text = tfText.getText().trim();
			if(cbColor.getSelectedIndex() == 0){
				selectColor = Color.black;
			}else if(cbColor.getSelectedIndex() == 1){
				selectColor = Color.blue;
			}else if(cbColor.getSelectedIndex() == 2){
				selectColor = Color.cyan;
			}else if(cbColor.getSelectedIndex() == 3){
				selectColor = Color.green;
			}else if(cbColor.getSelectedIndex() == 4){
				selectColor = Color.orange;
			}else if(cbColor.getSelectedIndex() == 5){
				selectColor = Color.magenta;				
			}else if(cbColor.getSelectedIndex() == 6){
				selectColor = Color.red;
			}else if(cbColor.getSelectedIndex() == 7){
				selectColor = Color.yellow;
			}
			
			this.setVisible(false);
			this.dispose();
			return;
		}
		
		if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
	}
	
	public int getLine(){
		if(tfText != null)
			return tfText.getLineCount();
		else
			return 0;
	}
	
}
