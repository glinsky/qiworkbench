/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import com.gwsys.welllog.core.curve.WellLogPattern;
import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.LithologyObject;
import com.gwsys.welllog.view.fill.PanelLithology;
import com.gwsys.welllog.view.zone.ZoneProp;

public class EditLithologyObjectDialog extends JDialog implements ActionListener{

	private static final long serialVersionUID = 1L;
	private LithologyObject lithologyObject;

	private JPanel jContentPanel = null;
	JLabel lblText = null;
	JTextField tfText = null; //
	JLabel lblOrientation = null;
	JComboBox cbOrientation = null;
	JLabel lblFillType = null;
	JComboBox cbFillType = null;
	JLabel lblFillColor = null;
	JComboBox cbFillColor = null;
	JLabel lblDepthStart = null;
	JLabel lblDepthEnd = null;
	JTextField tfDepthStart = null;
	JTextField tfDepthEnd = null;

	private JButton btnOk = null;
	private JButton btnCancel = null;

	public EditLithologyObjectDialog(LithologyObject lithologyObject){
		super(DataBuffer.getJMainFrame().getFrame());
		this.lithologyObject = lithologyObject;
		this.setTitle("Settings of LithologyObject");
		this.setContentPane(getContentPanel());
		this.setBounds(150,150,260,265);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	private JPanel getContentPanel(){
		if (jContentPanel == null) {
			jContentPanel = new JPanel();

			jContentPanel.setLayout(null);

			lblText = new JLabel("Text:");
			lblText.setBounds(10,10,110,25);
			tfText = new JTextField(lithologyObject.getText());
			tfText.setBounds(120,10,120,25);

			lblOrientation = new JLabel("Text Orientation:");
			lblOrientation.setBounds(10,40,110,25);
			String[] textOrientation = { "Horizontal", "Vertical"};
			cbOrientation = new JComboBox(textOrientation);
			cbOrientation.setBounds(120,40,120,25);
			if (lithologyObject.getOrientation() == LithologyObject.ORIENTATION_HORIZONTAL) {
				cbOrientation.setSelectedIndex(0);
			} else if (lithologyObject.getOrientation() == LithologyObject.ORIENTATION_VERTICAL) {
				cbOrientation.setSelectedIndex(1);
			}

			lblFillType = new JLabel("Fill Pattern:");
			lblFillType.setBounds(10,70,110,25);

			WellLogPattern choises[] = ZoneProp.getWellLogPatterns();
			WellLogPattern choise = WellLogPattern.loadPattern("Empty" , null, null);
			WellLogPattern allChoise[] = new WellLogPattern[choises.length+1];
			allChoise[0] = choise;
			for(int i = 0 ; i < choises.length ; i++){
				allChoise[i+1] = choises[i];
			}
			cbFillType = new JComboBox(allChoise);
			FillComboBoxRenderer renderer= new FillComboBoxRenderer();
			renderer.setPreferredSize(new Dimension(80, 25));
			cbFillType.setRenderer(renderer);
			cbFillType.setBounds(120,70,120,25);
	        if(lithologyObject.getPanelLithology() != null)
	        {
	        	cbFillType.setSelectedIndex(lithologyObject.getPanelLithology().getWhichtexture());
	        }

	        lblFillColor = new JLabel("Fill Color:");
	        lblFillColor.setBounds(10,100,110,25);
			ImageIcon [] icons = new ImageIcon[8];
			icons[0] = IconResource.getInstance().getImageIcon("colorBlack.gif");
			icons[1] = IconResource.getInstance().getImageIcon("colorBlue.gif");
			icons[2] = IconResource.getInstance().getImageIcon("colorCyan.gif");
			icons[3] = IconResource.getInstance().getImageIcon("colorGreen.gif");
			icons[4] = IconResource.getInstance().getImageIcon("colorOrange.gif");
			icons[5] = IconResource.getInstance().getImageIcon("colorMagenta.gif");
			icons[6] = IconResource.getInstance().getImageIcon("colorRed.gif");
			icons[7] = IconResource.getInstance().getImageIcon("colorYellow.gif");
			cbFillColor = new JComboBox(icons);
			cbFillColor.setBounds(120,100,120,25);

			cbFillColor.setSelectedIndex(this.lithologyObject.getColorIndex());

			lblDepthStart = new JLabel("Depth Start:");
			lblDepthStart.setBounds(10, 130, 110, 25);
			tfDepthStart = new JTextField();
			if (lithologyObject.getPanelLithology() == null){
				tfDepthStart.setText("");
			}else{
				tfDepthStart.setText(lithologyObject.getDepthStart() + "");
			}
			tfDepthStart.setBounds(120, 130, 120, 25);

			lblDepthEnd = new JLabel("Depth End:");
			lblDepthEnd.setBounds(10, 160, 110, 25);
			tfDepthEnd = new JTextField();
			if (lithologyObject.getPanelLithology() == null){
				tfDepthEnd.setText("");
			}else{
				tfDepthEnd.setText(lithologyObject.getDepthEnd() + "");
			}
			tfDepthEnd.setBounds(120, 160, 120, 25);

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(50,200,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(130,200,80,25);

			jContentPanel.add(lblText);
			jContentPanel.add(tfText);
			jContentPanel.add(lblOrientation);
			jContentPanel.add(cbOrientation);
			jContentPanel.add(lblFillType);
			jContentPanel.add(cbFillType);
			jContentPanel.add(lblFillColor);
			jContentPanel.add(cbFillColor);

			jContentPanel.add(lblDepthStart);
			jContentPanel.add(tfDepthStart);
			jContentPanel.add(lblDepthEnd);
			jContentPanel.add(tfDepthEnd);

			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("ok")) {
			lithologyObject.setText(tfText.getText().trim());

			if(cbOrientation.getSelectedIndex() == 0){
				lithologyObject.setOrientation(LithologyObject.ORIENTATION_HORIZONTAL);
			}else if(cbOrientation.getSelectedIndex() == 1){
				lithologyObject.setOrientation(LithologyObject.ORIENTATION_VERTICAL);
			}

			if (lithologyObject.getPanelLithology() == null){
				lithologyObject.setPanelLithology(new PanelLithology());
				lithologyObject.getPanelLithology().setLithologyObject(lithologyObject);
			}
			lithologyObject.getPanelLithology().setWhichtexture(cbFillType.getSelectedIndex());
			if (cbFillType.getSelectedIndex() == 0){
				lithologyObject.getPanelLithology().setSelected(false);
			}else{
				lithologyObject.getPanelLithology().setSelected(true);
			}

			lithologyObject.setColorIndex(cbFillColor.getSelectedIndex());

			if(cbFillColor.getSelectedIndex() == 0){
				lithologyObject.setFillColor(Color.black);
			}else if(cbFillColor.getSelectedIndex() == 1){
				lithologyObject.setFillColor(Color.blue);
			}else if(cbFillColor.getSelectedIndex() == 2){
				lithologyObject.setFillColor(Color.cyan);
			}else if(cbFillColor.getSelectedIndex() == 3){
				lithologyObject.setFillColor(Color.green);
			}else if(cbFillColor.getSelectedIndex() == 4){
				lithologyObject.setFillColor(Color.orange);
			}else if(cbFillColor.getSelectedIndex() == 5){
				lithologyObject.setFillColor(Color.magenta);
			}else if(cbFillColor.getSelectedIndex() == 6){
				lithologyObject.setFillColor(Color.red);
			}else if(cbFillColor.getSelectedIndex() == 7){
				lithologyObject.setFillColor(Color.yellow);
			}

			float depthStart = Float.parseFloat(tfDepthStart.getText().trim());
			float depthEnd = Float.parseFloat(tfDepthEnd.getText().trim());
			lithologyObject.setDepthStart(depthStart);
			lithologyObject.setDepthEnd(depthEnd);

			this.setVisible(false);
			this.dispose();
			return;
		}

		if (e.getActionCommand().equals("cancel")) {
			this.setVisible(false);
			this.dispose();
			return;
		}
	}

	class FillComboBoxRenderer extends JLabel implements ListCellRenderer {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		private WellLogPattern pattern;

		public FillComboBoxRenderer() {
			setOpaque(true);
		}

		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {

			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			pattern = (WellLogPattern) value;

			return this;
		}

		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setPaint(pattern);

			g2.fillRect(0, 0, getWidth(), getHeight());
		}
	}
}
