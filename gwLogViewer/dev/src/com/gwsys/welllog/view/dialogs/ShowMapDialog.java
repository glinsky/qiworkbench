/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import javax.swing.JDialog;
import javax.swing.JPanel;

import com.gwsys.welllog.view.DataBuffer;

/**
 * A dialog to show positions of all wells.
 * @author Team.
 *
 */
public class ShowMapDialog extends JDialog{
	private static final long serialVersionUID = 1L;
	private JPanel jContentPanel = null;
	private MapPanel mapPanel = null;
	
	public ShowMapDialog(){
		super(DataBuffer.getJMainFrame().getFrame(), "Show Map");
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,650,450);
		this.setLocationRelativeTo(DataBuffer.getJMainFrame().getFrame());
		this.setResizable(false);
		this.setModal(true);
	}
	private JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();			
			jContentPanel.setLayout(null);	
			
			mapPanel = new MapPanel();
			
			jContentPanel.add(mapPanel);
			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}
		
}
