/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.projecttree.ProjectTreeMarkerNodeObject;
import com.gwsys.welllog.view.yaxis.DepthConverter;
/**
 * A dialog that handles creating and editing markers.
 * @author luming
 *
 */
public class EditMarkerDialog extends JDialog implements ActionListener {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private JPanel jContentPanel = null;
	private CurveView curveView = null;
	private Point clickPoint = null;
	private CurveMarker curveMarker= null;
	private String oldMarkerName = null;
	/////////the components of this Dialog/////////
	JLabel lblName = null;
	JTextField tfName = null;
	JLabel lblDepth = null;
	JTextField tfDepth = null;
	JLabel lblWidth = null; // hhb add it
	JTextField tfWidth = null; // hhb add it
	JLabel lblLineStyle = null; // hhb add it
	JComboBox cbLineStyle = null; // hhb add it
	JLabel lblColor = null;
	//JTextField tfColor = null;
	JButton btnShowColor = null;
	JButton btnOk = null;
	JButton btnCancel = null;
    private float oldDepth = 0.0f;
	///////////////////////////////////////////////

	public EditMarkerDialog(CurveView curveView,Point p){
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Create Marker");
		this.curveView = curveView;
		clickPoint = p;
		init();
		this.setModal(true);
	}
	public EditMarkerDialog(CurveView curveView,CurveMarker cm)
	{
		super(DataBuffer.getJMainFrame().getFrame());
		this.curveMarker = cm;
		this.setTitle("Modify Marker");
		this.curveView = curveView;
		init();
		this.setModal(true);
	}

	public EditMarkerDialog(CurveView curveView,String name,float depth,Robot robot)
	{
		super(DataBuffer.getJMainFrame().getFrame());

		this.setTitle("Create Marker");
		this.curveView = curveView;
		init();
		this.tfName.setText(name);
		this.tfDepth.setText(depth+"");
		this.showDemo(robot);
	}

	private void init(){
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,390,180);
		this.setResizable(false);
		this.setLocationRelativeTo(DataBuffer.getJMainFrame().getFrame());
		this.setModal(false);
	}

	public JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);

			lblName = new JLabel("Name:");
			lblName.setBounds(10,10,40,25);
			String labeltype = "";
			Color color = null;
			if(curveMarker == null){
				tfName = new JTextField("marker");		//default
				oldMarkerName = null;
				color = Color.BLUE;
			}else{
				tfName = new JTextField(this.curveMarker.getMarkerName());
				oldMarkerName = this.curveMarker.getMarkerName();
				color = curveMarker.getMarkerColor();
			}
			tfName.setBounds(50,10,120,25);

			float depthValue = 0f;
			String temp = curveView.getParentAxisCurveContainer().getYAxisView().getYType();
			if(temp.equals(TrackTypeList.Y_TIME ))
			{
				labeltype = "Time:";
				if(curveMarker != null){
					depthValue = curveMarker.getMarkerDepth()/curveView.getParentAxisCurveContainer().getVelocityValue();
				}else{
					if(clickPoint != null){
						depthValue = changeHeightToDepth(clickPoint.getY(),curveView)/curveView.getParentAxisCurveContainer().getVelocityValue();
					}
				}
			}else{
				labeltype = "Depth:";
				if(curveMarker != null){
					depthValue = curveMarker.getMarkerDepth();
				}else{
					if(clickPoint != null){
						depthValue = changeHeightToDepth(clickPoint.getY(),curveView);
					}
				}
			}

			if(curveMarker != null){
				String measurement = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getVerticalMeasurement();
			    DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
			    if(measurement.equals(ConstantVariableList.MEASUREMENT_TVD))
			    	depthValue = timeToDepthConverter.toDepth(depthValue,TrackTypeList.MD_TO_TVD);
			    else if(measurement.equals(ConstantVariableList.MEASUREMENT_TVDSS))
				    depthValue = -timeToDepthConverter.toDepth(depthValue,TrackTypeList.MD_TO_TVDSS);
			}

			lblDepth = new JLabel(labeltype);
			lblDepth.setBounds(10,40,40,25);
			tfDepth = new JTextField(depthValue + "");
			oldDepth = depthValue;
			tfDepth.setBounds(50,40,120,25);

			lblColor = new JLabel("Color:");
			lblColor.setBounds(10,70,40,25);
			btnShowColor = new JButton("Change");
			btnShowColor.setForeground(color);
			btnShowColor.setBounds(50,70,120,25);
			btnShowColor.setActionCommand("showColorChooser");
			btnShowColor.addActionListener(this);

			lblWidth = new JLabel("Line Width:");
			lblWidth.setBounds(180,40,70,25);
			if(this.curveMarker == null){
				tfWidth = new JTextField("1");
			}else{
				tfWidth = new JTextField(this.curveMarker.getMarkerWidth() + "");
			}
			tfWidth.setBounds(250,40,120,25);

			lblLineStyle = new JLabel("Line Style:");
			lblLineStyle.setBounds(180,70,70,25);
			String choise[] = new String[5];
			for (int i=0; i<5; i++) {
				choise[0] = Integer.toString(ComboBoxRender.SOLID);
				choise[1] = Integer.toString(ComboBoxRender.DASH);
				choise[2] = Integer.toString(ComboBoxRender.DASH_DOT);
				choise[3] = Integer.toString(ComboBoxRender.DOT);
				choise[4] = Integer.toString(ComboBoxRender.DASH_DOT_DOT);
			}
			cbLineStyle = new JComboBox(choise);
			ComboBoxRender renderer= new ComboBoxRender();
	        renderer.setPreferredSize(new Dimension(100, 30));
	        cbLineStyle.setRenderer(renderer);
			cbLineStyle.setBounds(250,70,120,25);

			if(this.curveMarker == null){
				cbLineStyle.setSelectedIndex(0);
			}else{
				if(this.curveMarker.getLineStyle() == ComboBoxRender.SOLID)
				    cbLineStyle.setSelectedIndex(0);
				else if(this.curveMarker.getLineStyle() == ComboBoxRender.DASH)
					cbLineStyle.setSelectedIndex(1);
				else if(this.curveMarker.getLineStyle() == ComboBoxRender.DASH_DOT)
				    cbLineStyle.setSelectedIndex(2);
				else if(this.curveMarker.getLineStyle() == ComboBoxRender.DOT)
					cbLineStyle.setSelectedIndex(3);
				else if(this.curveMarker.getLineStyle() == ComboBoxRender.DASH_DOT_DOT)
					cbLineStyle.setSelectedIndex(4);
			}

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(120,110,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(200,110,80,25);

			jContentPanel.add(lblName);
			jContentPanel.add(tfName);
			jContentPanel.add(lblDepth);
			jContentPanel.add(tfDepth);
			jContentPanel.add(lblWidth);
			jContentPanel.add(tfWidth);
			jContentPanel.add(lblLineStyle);
			jContentPanel.add(cbLineStyle);
			jContentPanel.add(lblColor);
			jContentPanel.add(btnShowColor);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}


	public static int changeDepthToHeight(float depth,CurveView curveView)
	{
		return 1;
	}
	public static float changeHeightToDepth(double height,CurveView curveView)
	{
		float depth = (float) ((height/curveView.getHeight())*(curveView.getCurveLayer()
				.getYSetBottomDepth()-curveView.getCurveLayer().getYSetTopDepth())+curveView.getCurveLayer().getYSetTopDepth());
		if(curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS )){
			depth = -depth;
		}
		return depth;
	}

	public void actionPerformed(ActionEvent e) {
		/////////////when button ok is clicked/////////////////
		if(e.getActionCommand().equals("ok"))
		{
			String name = tfName.getText().trim();

			float width = Float.parseFloat(tfWidth.getText().trim());

			String temp = curveView.getParentAxisCurveContainer().getYAxisView().getYType();
			float depth = 0 ;
			if(temp.equals(TrackTypeList.Y_TIME))
			{
				depth = Float.parseFloat(tfDepth.getText().trim())*curveView.getParentAxisCurveContainer().getVelocityValue();
			}else{
				depth = Float.parseFloat(tfDepth.getText().trim());
			}


			String measurement = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getVerticalMeasurement();
		    DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
		    if(measurement.equals(ConstantVariableList.MEASUREMENT_TVD))
		    	 depth = timeToDepthConverter.toDepth(depth,TrackTypeList.TVD_TO_MD);
		    else if(measurement.equals(ConstantVariableList.MEASUREMENT_TVDSS))
			     depth = timeToDepthConverter.toDepth(-depth,TrackTypeList.TVDSS_TO_MD);
		    //if the name of marker	is same as existing one, treat as editing

			if(curveMarker == null) //if new
			{
				curveMarker = new CurveMarker(name,depth,this.btnShowColor.getForeground());
				curveMarker.setMarkerWidth(width);
				if(cbLineStyle.getSelectedIndex() == 0)
					curveMarker.setLineStyle(ComboBoxRender.SOLID );
				else if(cbLineStyle.getSelectedIndex() == 1)
					curveMarker.setLineStyle(ComboBoxRender.DASH);
				else if(cbLineStyle.getSelectedIndex() == 2)
					curveMarker.setLineStyle(ComboBoxRender.DASH_DOT);
				else if(cbLineStyle.getSelectedIndex() == 3)
					curveMarker.setLineStyle(ComboBoxRender.DOT);

				curveMarker.setXLenth(curveView.getWidth());
				curveView.getMarkerLayer().addMarkerToList(curveMarker);

				MarkerInfo info = new MarkerInfo(curveView.getParentAxisCurveContainer().getWellName(),curveMarker.getMarkerName(), curveMarker.getMarkerDepth());
				ViewCoreOperator.addMarkerInfo(info);
			}
			else //if edit
			{
				if(!curveMarker.getMarkerName().equals(name)){
					ViewCoreOperator.renameMarkerName(curveMarker.getMarkerName(),name);
				}
				curveMarker.setMarkerName(name);
				curveMarker.setMarkerWidth(width);
				if(cbLineStyle.getSelectedIndex() == 0)
					curveMarker.setLineStyle(ComboBoxRender.SOLID);
				else if(cbLineStyle.getSelectedIndex() == 1)
					curveMarker.setLineStyle(ComboBoxRender.DASH);
				else if(cbLineStyle.getSelectedIndex() == 2)
					curveMarker.setLineStyle(ComboBoxRender.DASH_DOT);
				else if(cbLineStyle.getSelectedIndex() == 3)
					curveMarker.setLineStyle(ComboBoxRender.DOT);
				else if(cbLineStyle.getSelectedIndex() == 4)
					curveMarker.setLineStyle(ComboBoxRender.DASH_DOT_DOT);

				if(!tfDepth.getText().trim().equals(oldDepth+"")){
					curveMarker.setMarkerDepth(depth);
				}

				curveMarker.setMarkerColor(this.btnShowColor.getForeground());
			}
			curveMarker.setYHeight(curveView);
			//set the click point

			if(oldMarkerName == null){
				DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().addWellTopNode(curveView.getParentAxisCurveContainer().getWellName(),name,ProjectTreeMarkerNodeObject.WELL_NAME,true);
			}else{
				DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().renameWellTopNode(oldMarkerName,name);
			}
			this.setVisible(false);
			this.dispose();

			DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().repaint();
			//link tops
			ViewCoreOperator.connectMarker(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView());
			return;
		}

		/////////when button cancel is clicked//////////
		else if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
		else if(e.getActionCommand().equals("showColorChooser"))
		{
			Color selected=JColorChooser.showDialog(this,
					"Choose Curve Color", btnShowColor.getForeground());
			if (selected!=null){
				btnShowColor.setForeground(selected);
			}
		}
	}

	public CurveMarker getCurveMarker() {
		return curveMarker;
	}

	public void setCurveMarker(CurveMarker curveMarker) {
		this.curveMarker = curveMarker;
	}

	public void showDemo(Robot robot){
		setVisible(true);

		Point p=btnOk.getLocationOnScreen();
		robot.mouseMove(p.x + 5, p.y + 5);
		robot.delay(500);

		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.delay(500);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}
}
