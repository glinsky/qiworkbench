/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.WellInfo;
import com.gwsys.welllog.view.yaxis.DepthConverter;

public class SetSurveryDialog extends JDialog implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	private JPanel jContentPanel = null; 
	private DepthConverter converter= null;	
	private JLabel lblWellName = null;
	private JComboBox cbWellName = null;	
	private JButton btnOk;
	private JButton btnCancel;
	
	public SetSurveryDialog(DepthConverter converter){
		super(DataBuffer.getJMainFrame().getFrame());
		this.converter = converter;
		this.setTitle("Match Well");
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,260,120);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}
	
	public JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);
			
			lblWellName = new JLabel("Choose Well:");
			lblWellName.setBounds(10,10,80,25);
			cbWellName = new JComboBox();
			cbWellName.addItem(" ");
			if(DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList().size()>0){				
				for(int i=0 ; i < DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList().size(); i++){
					WellInfo wellInfo = (WellInfo)DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList().get(i);
					cbWellName.addItem(wellInfo.getWellName());
				}
			}
			cbWellName.setBounds(90,10,150,25);		
			
			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("Ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(40,55,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("Cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(120,55,80,25);
			
			jContentPanel.add(lblWellName);
			jContentPanel.add(cbWellName);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);
		}
		return jContentPanel;
	}
	
	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Ok")){
			if(cbWellName.getSelectedIndex() != 0){
				WellInfo wellInfo = (WellInfo)DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList().get(cbWellName.getSelectedIndex()-1);
				String wellName = wellInfo.getWellName();
				HashMap surveyHashMap = DataBuffer.getJMainFrame().getMainBuffer().getSurveyHashMap();
				if(surveyHashMap.containsKey(wellName)){
					surveyHashMap.remove(wellName);
				}
				surveyHashMap.put(wellName,this.converter);
			}
			this.setVisible(false);
			this.dispose();
			return;
		}
		
		if(e.getActionCommand().equals("Cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
	}
}
