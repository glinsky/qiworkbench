/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.BasicStroke;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class ComboBoxRender extends JLabel implements ListCellRenderer {

	private static final long serialVersionUID = 1L;

	private int line_type;
	
	public static final int SOLID = 0;
	public static final int DASH = 1;
	public static int DASH_DOT = 2;
	public static int DOT = 3;
	public static int DASH_DOT_DOT = 4;

	public ComboBoxRender() {
		setOpaque(true);
     }

    public Component getListCellRendererComponent(
        JList list,
        Object value,
        int index,
        boolean isSelected,
        boolean cellHasFocus) {

		if (isSelected) {
		  setBackground(list.getSelectionBackground());
		  setForeground(list.getSelectionForeground());
		} else {
		  setBackground(list.getBackground());
		  setForeground(list.getForeground());
		}

  		line_type = Integer.parseInt((String)value);
		return this;
     }

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		super.paint(g2);

		int w = getSize().width;
		int h = getSize().height;

		if(line_type == ComboBoxRender.SOLID){
			g2.setStroke(new BasicStroke(2)); 
		}
		
		if(line_type == ComboBoxRender.DASH ){
			float dash1[] = {5,5}; // dash
			g2.setStroke(new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1, dash1, 0));
		}
		if(line_type == ComboBoxRender.DASH_DOT){
			float dash2[] = {5,5,1,5}; // dash dot
			g2.setStroke(new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1, dash2, 0));
		}
		if(line_type == ComboBoxRender.DOT){
			float dash3[] = {1,5}; // dot
			g2.setStroke(new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1, dash3, 0));
		}
		if(line_type == ComboBoxRender.DASH_DOT_DOT){
			float dash4[] = {5,5,1,5,1,5}; // dash dot dot
			g2.setStroke(new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1, dash4, 0));
		}
		g2.drawLine(10, h/2, w - 10, h/2);
	}
	
	public static BasicStroke getStroke(Integer lineStyle, float lineWidth)
    {
		BasicStroke stroke = null;
	
		if(lineStyle == ComboBoxRender.SOLID)
            stroke = new BasicStroke(lineWidth, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1.0f);
        else
        	if(lineStyle == ComboBoxRender.DASH)
            {
                float af1[] = {5F, 5F};
                stroke = new BasicStroke(lineWidth, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1.0f, af1, 0.0F);
            } else
            	if(lineStyle == ComboBoxRender.DASH_DOT)
                {
                    float af2[] = {5F, 5F, 1.0F, 5F};
                    stroke = new BasicStroke(lineWidth, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1.0f, af2, 0.0F);
                } else
                	if(lineStyle == ComboBoxRender.DOT)
                    {
                		
                        float af[] = {1.0F, 5F};
                        stroke = new BasicStroke(lineWidth, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1.0f, af, 0.0F);
                    } else
                    	if(lineStyle == ComboBoxRender.DASH_DOT_DOT)
                        {
                            float af3[] = {5F, 5F, 1.0F, 5F, 1.0F, 5F};
                            stroke = new BasicStroke(lineWidth, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER , 1.0f, af3, 0.0F);
                        }
                
        return stroke;
    }
}