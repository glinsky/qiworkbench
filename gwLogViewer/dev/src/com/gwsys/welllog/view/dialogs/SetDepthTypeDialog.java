/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.yaxis.DepthConverter;
import com.gwsys.welllog.view.yaxis.DataConverterFactory;

public class SetDepthTypeDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private JPanel jContentPane = null;

	private JLabel jFileLabel = null;

	private JTextField jFileTextField = null;

	private JButton jBrowseButton = null;

	private JButton jOKButton = null;

	private JButton jCancelButton = null;
	
	private boolean selectSurveyFile = false;
	
	public SetDepthTypeDialog(){
		super(DataBuffer.getJMainFrame().getFrame());	
		initialize();
		this.setModal(true);
	}
	
	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);		
		this.setTitle("depth:MD/TVD/TVDSS");
		this.setContentPane(getJContentPane());
		this.setBounds(150,100,290,125);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}
	
	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {			
			/*
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(2);
			gridLayout.setColumns(3);
			*/
			jContentPane = new JPanel();
			jContentPane.setLayout(null);

			//jContentPane.add(getJFileRadioButton(), null);
			jFileLabel = new JLabel();
			jFileLabel.setText("Selected Survey File:");
			jFileLabel.setBounds(5,5,200,20);
			jContentPane.add(jFileLabel, null);
			jContentPane.add(getJFileTextField(), null);
			jContentPane.add(getJBrowseButton(), null);

			//jContentPane.add(getJEmptyPanel(), null);
			jContentPane.add(getJOKButton(), null);
			jContentPane.add(getJCancelButton(), null);
			//jContentPane.add(getJEmptyPanel(), null);

		}
		return jContentPane;
	}
	/**
	 * This method initializes jFileTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJFileTextField() {
		if (jFileTextField == null) {
			jFileTextField = new JTextField();
			jFileTextField.setBounds(5,30,195,25);

		}
		return jFileTextField;
	}

	/**
	 * This method initializes jFileBrowseButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJBrowseButton() {
		if (jBrowseButton == null) {
			jBrowseButton = new JButton();
			jBrowseButton.setText("Browse");
			jBrowseButton.setBounds(200,30,80,25);
			jBrowseButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ChooserDialog chooser = new ChooserDialog(DataBuffer.getJMainFrame(),ChooserDialog.getPath());
					chooser.clearFilter();
					chooser.setFilter("xls");		
					chooser.setMultiSelect(false);
					chooser.showDialog();
					
					DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getJMainFrame().setFileChooser(chooser.getJfc());

					if(!chooser.isOpen()){
						return;
					}
										
					File file = chooser.getFile();	
					ChooserDialog.setPath(file.getPath());
					
					jFileTextField.setText(file.getPath());					
				}
			});
		}
		return jBrowseButton;
	}

	/**
	 * This method initializes jOKButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJOKButton() {
		if (jOKButton == null) {
			jOKButton = new JButton();
			jOKButton.setText("OK");
			jOKButton.setBounds(120,70,80,25);
			jOKButton.addActionListener(new java.awt.event.ActionListener() {
				@SuppressWarnings("unchecked")
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (jFileTextField.getText() == null || jFileTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Please select survey file!");
						return;
					}
					
					File file = new File(jFileTextField.getText());
					DepthConverter converter = DataConverterFactory.createInstance(file.getPath(),TrackTypeList.MD_TO_TVD);
					if (converter == null) {
						// alert something
						JOptionPane.showMessageDialog(null, "Cann't parse the selected survey file!");
						return;
					}
					ChooserDialog.setPath(file.getPath());
					
					DataBuffer.getJMainFrame().getMainBuffer().setMDDepthConverter(converter);
					DataBuffer.getJMainFrame().getMainBuffer().getAllSurveys().add(file.getPath());
					
					selectSurveyFile = true;					
					
					SetDepthTypeDialog.this.dispose();
				}
			});
		}
		return jOKButton;
	}

	/**
	 * This method initializes jCancelButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJCancelButton() {
		if (jCancelButton == null) {
			jCancelButton = new JButton();
			jCancelButton.setText("Cancel");
			jCancelButton.setBounds(200,70,80,25);
			jCancelButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					selectSurveyFile = false;
					SetDepthTypeDialog.this.dispose();
				}
			});
		}
		return jCancelButton;
	}

	public boolean isSelectSurveyFile() {
		return selectSurveyFile;
	}
	
	
}
