/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.fill.CurveFillBeside;

/**
 * Dialog edits the fill properties.
 * @author Team
 */
public class EditFillBesideDialog extends JDialog implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	private JPanel jContentPanel = null;

	private JLabel lblColor = null;

	private JButton btnShowColor = null;
	
	private JLabel lblOrietation = null;
	
	private JComboBox cmbOrietation = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private CurveView cv = null;
	
	private CurveFillBeside cfb = null;
	
	public EditFillBesideDialog(CurveView cv)
	{
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Fill Beside Property Dialog");
		this.cv = cv;
		
		CurvePanel cp = (CurvePanel)cv.getCurveLayer().getCurveSelectedList().get(0);
		for(int i=0 ; i<cv.getCurveLayer().getCurveFillBesideList().size() ; i++){
			if(((CurveFillBeside)cv.getCurveLayer().getCurveFillBesideList().get(i))
					.getCp().getCd().getHashCurveName().equals(cp.getCd().getHashCurveName()))
				this.cfb = (CurveFillBeside)cv.getCurveLayer().getCurveFillBesideList().get(i);
		}	
		
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,230,150);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}
	
	public JPanel getContentPanel() {
		if (jContentPanel == null) {
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);

			lblColor = new JLabel("Fill Color:");
			lblColor.setBounds(10,10,100,25);
			btnShowColor = new JButton("Change");
			btnShowColor.setActionCommand("showColorChooser");
			if(cfb == null){
				btnShowColor.setForeground(Color.PINK);
			}else{
				btnShowColor.setForeground(cfb.getFillColor());
			}
			btnShowColor.addActionListener(this);
			btnShowColor.setBounds(110,10,100,25);
			
			lblOrietation = new JLabel("Fill Orietation:");
			lblOrietation.setBounds(10,40,100,25);
			cmbOrietation = new JComboBox();
			cmbOrietation.addItem(ConstantVariableList.FILL_LEFT);
			cmbOrietation.addItem(ConstantVariableList.FILL_RIGHT);
			cmbOrietation.addItem(ConstantVariableList.FILL_ALL);
			cmbOrietation.addItem(ConstantVariableList.FILL_NONE);			
			if(cfb == null){
				cmbOrietation.setSelectedIndex(0);
			}else{
				if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_LEFT)){
					cmbOrietation.setSelectedIndex(0);
				}else if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_RIGHT)){
					cmbOrietation.setSelectedIndex(1);
				}else if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_ALL)){
					cmbOrietation.setSelectedIndex(2);
				}else if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_NONE)){
					cmbOrietation.setSelectedIndex(3);
				}
			}		
			cmbOrietation.setBounds(110,40,100,25);
			
			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(35,80,80,25);
			
			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(115,80,80,25);
			
			jContentPanel.add(lblColor);
			jContentPanel.add(btnShowColor);

			jContentPanel.add(lblOrietation);
			jContentPanel.add(cmbOrietation);
			
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("ok"))
		{
			if(cfb == null){		//create new curvefill
				cfb = new CurveFillBeside();				
				for(int i = 0 ; i<cv.getCurveLayer().getCurveSelectedList().size();i++) //different from top
				{	
				//	cfb.setWhichCurve(cv.getCurveLayer().getCurveList().indexOf(cv.getCurveLayer().getCurveSelectedList().get(i)));
					cfb.setCp((CurvePanel) cv.getCurveLayer().getCurveSelectedList().get(i));
					cfb.setLeftOrRight((String)this.cmbOrietation.getSelectedItem());					
					cfb.constructFill(cv);
					cfb.setFillColor(this.btnShowColor.getForeground());
					cv.getCurveLayer().addFillBesideToList(cfb);
				}
			}else{	//change curvefill property
				cfb.setLeftOrRight((String)this.cmbOrietation.getSelectedItem());
				cfb.setFillColor(this.btnShowColor.getForeground());
				cfb.constructFill(cv);
			}
			cv.repaintFather();
			this.setVisible(false);
			this.dispose();
			return;
		}
		
		/////////when button cancel is clicked//////////
		else if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
		else if(e.getActionCommand().equals("showColorChooser"))
		{
			Color selected=JColorChooser.showDialog(this, 
					"Choose Curve Color", btnShowColor.getForeground());
			if (selected!=null){
				btnShowColor.setForeground(selected);
			}
		}
	}

}
