/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorChooserDialog extends JDialog implements ActionListener,ChangeListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton jSourceBtn;
	private JPanel jContentPanel = null;
	private JColorChooser tcc;
	private Color choosedColor;
	private JButton btnOk;
	private JButton btnCancel;
	
	public ColorChooserDialog(JButton jSourceBtn)
	{
		this.pack();
		
		this.jSourceBtn = jSourceBtn;
		tcc = new JColorChooser(Color.RED);
		 tcc.getSelectionModel().addChangeListener(this);
		tcc.setBorder(BorderFactory.createTitledBorder(
        "Choose Text Color"));
		
		btnOk = new JButton(" OK ");
		btnOk.setSize(30,80);
		btnOk.setActionCommand("ok");
		btnOk.addActionListener(this);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setSize(30,80);
		btnCancel.setActionCommand("cancel");
		btnCancel.addActionListener(this);
		
		if(jContentPanel == null)
		{
			jContentPanel = new JPanel();
			jContentPanel.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;	
			c.weightx = 1;
			c.weighty = 1;
			c.gridwidth = 2;
			c.gridx = 0;
			c.gridy = 0;
			jContentPanel.add(tcc,c);
			c.gridwidth = 1;
			c.insets = new Insets(10,10,10,10);
			c.gridx = 0;
			c.gridy = 1;
			jContentPanel.add(btnOk,c);
			c.gridx = 1;
			c.gridy = 1;
			jContentPanel.add(btnCancel,c);
		}		
		this.setContentPane(jContentPanel);
		this.pack();
		this.setModal(true);
	}
	public Color getChoosedColor()
	{
		return this.choosedColor;
	}
	
	//any choose changed
	public void stateChanged(ChangeEvent e) {
        choosedColor = tcc.getColor();
    }
	
	//btn clicked
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok"))
		{
			this.jSourceBtn.setBackground(this.getChoosedColor());
			this.setVisible(false);
			this.dispose();
			return;
		}
		
/////////when button cancel is clicked//////////
		if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
	}
}
