/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.fill.MarkerFill;

/**
 * Dialog edits the fill marker properties.
 * @author Team
 *
 */
public class EditFillMarkerDialog extends JDialog implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	private JPanel jContentPanel = null;

	private JLabel lblColor = null;

	private JButton btnShowColor = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;
	
	private JLabel lblRemove = null;
	
	private JButton btnRemove = null;

	private CurveView cv = null;
	
	private MarkerFill mf = null;
	

	public EditFillMarkerDialog(CurveView cv)
	{
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Fill Property Dialog");
		this.cv = cv;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,190,100);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}
	
	public EditFillMarkerDialog(CurveView cv,MarkerFill mf)
	{
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Fill Property Dialog");
		this.cv = cv;
		this.mf = mf;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,230,140);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}
	
	public EditFillMarkerDialog(CurveView cv,Robot robot)
	{
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Fill Property Dialog");
		this.cv = cv;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,190,100);
		this.setResizable(false);

		this.setModal(false);
		
		Timer timer = new Timer();
 		MyTimerTask myTimerTask = new MyTimerTask(robot);
		timer.schedule(myTimerTask,500);
	}
	
	public JPanel getContentPanel() {
		if (jContentPanel == null) {
			jContentPanel = new JPanel();
			if(mf != null){
				jContentPanel.setLayout(null);
			}else{
				jContentPanel.setLayout(null);
			}

			lblColor = new JLabel("Color:");
			btnShowColor = new JButton("Change");

			btnShowColor.setActionCommand("showColorChooser");
			if(mf == null){
				btnShowColor.setForeground(Color.YELLOW);
			}else{
				btnShowColor.setForeground(mf.getFillColor());
			}
			btnShowColor.addActionListener(this);
			
			if(mf != null){
				lblRemove = new JLabel("Remove Fill:");		
			
				btnRemove = new JButton("remove");
				btnRemove.setActionCommand("remove");
				btnRemove.addActionListener(this);
			}
			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			
			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);

			if(mf != null){
				lblColor.setBounds(10,10,80,25);
				btnShowColor.setBounds(90,10,120,25);
				lblRemove.setBounds(10,40,80,25);
				btnRemove.setBounds(90,40,120,25);
				btnOk.setBounds(30,80,80,25);
				btnCancel.setBounds(110,80,80,25);
				
				jContentPanel.add(lblColor);
				jContentPanel.add(btnShowColor);
				jContentPanel.add(lblRemove);
				jContentPanel.add(btnRemove);
				jContentPanel.add(btnOk);
				jContentPanel.add(btnCancel);
			}else{
				lblColor.setBounds(10,10,40,25);
				btnShowColor.setBounds(50,10,120,25);
				btnOk.setBounds(10,40,80,25);
				btnCancel.setBounds(90,40,80,25);
				
				jContentPanel.add(lblColor);
				jContentPanel.add(btnShowColor);
				jContentPanel.add(btnOk);
				jContentPanel.add(btnCancel);
			}
			
			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}
	
	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent e) {
//		 TODO Auto-generated method stub
		if(e.getActionCommand().equals("ok"))
		{
			if(mf == null){		//create new curvefill
				mf = new MarkerFill();
				ArrayList list = cv.getMarkerLayer().getMarkerSelectedList();
				for(int i = 0 ; i<list.size()-1;i++)
				{	
					mf.setCurveMarker((CurveMarker)list.get(i),(CurveMarker)list.get(i+1));
					mf.constructFill(cv);
					mf.setFillColor(this.btnShowColor.getForeground());
					cv.getMarkerLayer().getMarkerFillList().add(mf);
				}
			}else{	//change curvefill property
				mf.setFillColor(this.btnShowColor.getForeground());
			}
			cv.repaintFather();
			this.setVisible(false);
			this.dispose();
			return;
		}
		
		/////////when button cancel is clicked//////////
		else if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
		else if(e.getActionCommand().equals("showColorChooser"))
		{
			Color selected=JColorChooser.showDialog(this, 
					"Choose Curve Color", btnShowColor.getForeground());
			if (selected!=null){
				btnShowColor.setForeground(selected);
			}
		}
		else if(e.getActionCommand().equals("remove")){
			cv.getMarkerLayer().getMarkerFillList().remove(mf);
			cv.repaintFather();
			this.setVisible(false);
			this.dispose();
			return;
		}
	}
	
	public void showDemo(Robot robot){		
		this.setVisible(true);
		
		Point p=btnOk.getLocationOnScreen();
		robot.mouseMove(p.x + 5, p.y + 5);
		robot.delay(500);
		
		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.delay(500);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}
	
	class MyTimerTask extends TimerTask{
		private Robot robot;

		public MyTimerTask(Robot robot){
			this.robot = robot;
		}
		
		public void run() {
			showDemo(robot);
		}		
	}
}
