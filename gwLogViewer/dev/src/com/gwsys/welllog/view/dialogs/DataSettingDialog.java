/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view.dialogs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.data.util.SpringUtilities;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.WellInfo;
import com.gwsys.welllog.view.yaxis.DataTypeConverter;

/**
 * Show the properties of data source.
 * @author Team
 *
 */
public class DataSettingDialog extends JDialog implements ActionListener {

	private MainFrameView view;
	private static final long serialVersionUID = 1L;
	private JPanel jContentPanel = null;
	private JTextField tfXLocation = null;
	private JTextField tfYLocation = null;
	private JButton btnOk = null;
	private JButton btnCancel = null;
	private String sourceName = null;

	//added by Alex
	public static final String SURVEY = "Survey";
	public static final String CHECKSHOT = "Checkshot";
	public static final int CHECKSHOT_TXT = 0;
	public static final int CHECKSHOT_ASC = 1;

	/**
	 * Constructor.
	 * @param sourceName Name of data source.
	 */
	public DataSettingDialog(String sourceName, MainFrameView view){
		super(view.getFrame(), "Settings of "+sourceName, true);
		this.view = view;
		this.sourceName = sourceName;
		this.setContentPane(getContentPanel());
		this.pack();
		this.setModal(true);
	}

	private JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			jContentPanel.setLayout(new BoxLayout(jContentPanel, BoxLayout.Y_AXIS));

			JPanel butPanel= new JPanel();

			btnOk = new JButton(" OK ");
			btnOk.setEnabled(false);
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);

			btnCancel = new JButton("Close");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);

			butPanel.add(btnOk);
			butPanel.add(btnCancel);

			WellInfo wellInfo = ViewCoreOperator.getWellInfoByWellName(sourceName);

			if (wellInfo != null)	{
				String name=sourceName.substring(sourceName.lastIndexOf(File.separator)+1);
				JPanel location = new JPanel(new SpringLayout());
				location.setBorder(BorderFactory.createTitledBorder("Info of "+name));
				JLabel lblXLocation = new JLabel("Location X: ");
				tfXLocation = new JTextField(15);
				JLabel lblYLocation = new JLabel("Location Y: ");
				tfYLocation = new JTextField(15);
				if(wellInfo != null){
					tfXLocation.setText(Float.toString(wellInfo.getXLocation()));
					tfYLocation.setText(Float.toString(wellInfo.getYLocation()));
				}

				JLabel topdepth = new JLabel("Top Depth: "+wellInfo.getTopDepth());
				JLabel botDepth = new JLabel("Bottom Depth: "+wellInfo.getBottomDepth());

				location.add(lblXLocation);
				location.add(tfXLocation);
				location.add(topdepth);
				location.add(lblYLocation);
				location.add(tfYLocation);
				location.add(botDepth);
				SpringUtilities.makeCompactGrid(location,2,3,5,5,5,5);

				// survey
				JPanel survey = new JPanel();
				survey.setBorder(BorderFactory.createTitledBorder("Survey"));
				HashMap surveyMap = DataBuffer.getJMainFrame().getMainBuffer()
									.getSurveyHashMap();
				Object surveyFile = surveyMap.get(wellInfo.getWellName());
				JLabel surveyLabel = null;
				if (surveyFile!=null)
					survey.add(surveyLabel=new JLabel(((DataTypeConverter)surveyFile)
							.getFilePath()));
				else
					survey.add(surveyLabel=new JLabel("N/A"));
				surveyLabel.addMouseListener(new SurveyListener((DataTypeConverter)surveyFile,
												DataSettingDialog.SURVEY, -1, this.view));

				jContentPanel.add(location);
				jContentPanel.add(survey);

				btnOk.setEnabled(true);
			}
			else	{
				// checkshot
				int kind;
				JPanel checkshot = new JPanel();
				checkshot.setBorder(BorderFactory.createTitledBorder("Checkshot"));
				HashMap checkshotMap = DataBuffer.getJMainFrame().getMainBuffer().getCheckshotHashMap();

				int start = 1 + this.sourceName.lastIndexOf("\\");
				int end = this.sourceName.lastIndexOf(".");
				String checkshotHashKey = this.sourceName.substring(start, end);

				if (this.sourceName.endsWith("txt"))
					kind = 0;
				else if (this.sourceName.endsWith("asc"))
					kind = 1;
				else
					return null;
				Object checkshotFile = checkshotMap.get(checkshotHashKey);
				JLabel checkshotLabel = null;
				if (checkshotFile!=null)
					checkshot.add(checkshotLabel=new JLabel(((DataTypeConverter)checkshotFile)
							.getFilePath()));
				else
					checkshot.add(checkshotLabel=new JLabel("N/A"));
				checkshotLabel.addMouseListener(new SurveyListener((DataTypeConverter)checkshotFile,
													DataSettingDialog.CHECKSHOT, kind, this.view));

				jContentPanel.add(checkshot);
			}

			jContentPanel.add(butPanel);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok")){
			WellInfo wellInfo = ViewCoreOperator.getWellInfoByWellName(sourceName);
			if (wellInfo != null){
				wellInfo.setXLocation(Float.parseFloat(tfXLocation.getText().trim()));
				wellInfo.setYLocation(Float.parseFloat(tfYLocation.getText().trim()));
			}
			this.setVisible(false);
			this.dispose();
			return;
		}

		if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
	}

	class SurveyListener extends MouseAdapter implements ActionListener{
		private DataTypeConverter depthConverter = null;
		private JTable dataTable = null;
		private JScrollPane tablePanel = null;
		private JPanel buttonsPanel = null;
		private JDialog sourceDialog = null;
		private JButton closeButton = null;

		private String dataType = null;
		//private int fileType;
		private boolean isTVD = false;

		/**
		 * @param converter
		 */
		public SurveyListener(DataTypeConverter converter, String type, int fileType, MainFrameView view) {
			depthConverter = converter;
			dataType = type;
			//this.fileType = fileType;
			isTVD = (fileType < DataSettingDialog.CHECKSHOT_ASC);

			sourceDialog = new JDialog(view.getFrame());
			sourceDialog.setTitle(dataType+" data");
			//sourceDialog
			sourceDialog.getContentPane().setLayout(new BorderLayout());
			sourceDialog.setSize(420,500);
			sourceDialog.setVisible(false);
			sourceDialog.setModal(true);

			buttonsPanel = new JPanel();
			closeButton = new JButton("Close");
			closeButton.addActionListener((ActionListener) this);
			closeButton.setActionCommand("closeAction");
			buttonsPanel.add(closeButton);

			sourceDialog.add(buttonsPanel, BorderLayout.SOUTH);
		}

		public void mouseClicked(MouseEvent event){
			if (event.getClickCount()==2){
				System.out.println("Show survey values in table");

				String[] columns = new String[3];
				Object[][] data = null;

				if (depthConverter != null)	{
					if (dataType==DataSettingDialog.SURVEY)	{
						columns[0] = "MD";
						columns[1] = "TVD";
				        columns[2] = "TVDSS";

				        data = getSurveyData();
					}
					else if (dataType==DataSettingDialog.CHECKSHOT)	{
						if (isTVD)	{
							columns[0] = "MD";
							columns[1] = "TVD";
					        columns[2] = "TIME";

					        data = getCheckshotData();
						}
						else	{
							columns[0] = "MD";
							columns[1] = "TVDSS";
					        columns[2] = "TIME";

					        data = getCheckshotData();
						}
					}
					else	{
						System.out.println("unknown value type");
						return;
					}

					//Object[][] data = getDepthData();
					dataTable = new JTable(data, columns);
					tablePanel = new JScrollPane(dataTable,
							JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
							JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

					sourceDialog.add(tablePanel, BorderLayout.NORTH);
					sourceDialog.setVisible(true);
				}
			}
		}

		/**
		 * get survey/checkshot data
		 * @return bidimensional array with survey/checkshot data
		 * @author Alex
		 *
		private Object[][] getDepthData()	{
			int numRows = depthConverter.getMDValues().length;
			Object[][] data = new Object[numRows][3];

			for (int i = 0; i < numRows; i++)	{
				data[i][0] = new Float(depthConverter.getMDValues()[i]);
				data[i][1] = new Float(depthConverter.getTVDValues()[i]);
				if (this.dataType==DataSettingDialog.CHECKSHOT)
					data[i][2] = new Float(depthConverter.getTwoWayTimeValues()[i]);
				else
					data[i][2] = new Float(-depthConverter.getTVDSSValues()[i]);
			}

			return data;
		}*/

		/**
		 * get survey data
		 * @return bidimensional array with survey data
		 * @author Alex
		 */
		private Object[][] getSurveyData()	{
			int numRows = depthConverter.getMDValues().length;
			Object[][] data = new Object[numRows][3];

			for (int i = 0; i < numRows; i++)	{
				data[i][0] = new Float(depthConverter.getMDValues()[i]);
				data[i][1] = new Float(depthConverter.getTVDValues()[i]);
				data[i][2] = new Float(-depthConverter.getTVDSSValues()[i]);
			}

			return data;
		}

		/**
		 * get checkshot data
		 * @return bidimensional array with checkshot data
		 * @author Alex
		 */
		private Object[][] getCheckshotData()	{
			int numRows = depthConverter.getMDValues().length;
			Object[][] data = new Object[numRows][3];

			for (int i = 0; i < numRows; i++)	{
				data[i][0] = new Float(depthConverter.getMDValues()[i]);
				if (isTVD)
					data[i][1] = new Float(depthConverter.getTVDValues()[i]);
				else
					data[i][1] = new Float(depthConverter.getTVDSSValues()[i]);
				data[i][2] = new Float(depthConverter.getTwoWayTimeValues()[i]);
			}

			return data;
		}

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("closeAction"))	{
				this.sourceDialog.setVisible(false);
			}
		}
	}
}
