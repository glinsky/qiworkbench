/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.DescriptionPanel;

/**
 * @author Administrator
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class EditDescriptionSettingDialog extends JDialog implements
		ActionListener {
	private static final long serialVersionUID = 1L;

	private JPanel jContentPanel = null;

	private DescriptionPanel dp = null;

	private JLabel lblName = null;

	private JTextField tfName = null;

	private JLabel lblWidth = null;

	private JTextField tfWidth = null;

	private JLabel lblSize = null;

	private JComboBox cbSize = null;

	private JLabel lblColor = null;

	private JComboBox cbColor = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	public EditDescriptionSettingDialog(DescriptionPanel dp) {
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Description Settings");
		this.dp = dp;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,380,150);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	public JPanel getContentPanel() {
		if (jContentPanel == null) {
			jContentPanel = new JPanel();

			jContentPanel.setLayout(null);

			lblName = new JLabel("Name:");
			lblName.setBounds(10,10,70,25);
			tfName = new JTextField();
			if (this.dp != null) {
				tfName.setText(dp.getName());
			}
			tfName.setBounds(80,10,120,25);

			lblWidth = new JLabel("Width:");
			lblWidth.setBounds(220,10,70,25);
			tfWidth = new JTextField();
			if (this.dp != null) {
				tfWidth.setText(dp.getBesideTypeWidth() + "");
			}
			tfWidth.setBounds(290,10,70,25);
			
			lblColor = new JLabel("Text Color:");
			lblColor.setBounds(10,40,70,25);
			ImageIcon[] icons = new ImageIcon[8];
			icons[0] = IconResource.getInstance().getImageIcon("colorBlack.gif");
			icons[1] = IconResource.getInstance().getImageIcon("colorBlue.gif");
			icons[2] = IconResource.getInstance().getImageIcon("colorCyan.gif");
			icons[3] = IconResource.getInstance().getImageIcon("colorGreen.gif");
			icons[4] = IconResource.getInstance().getImageIcon("colorOrange.gif");
			icons[5] = IconResource.getInstance().getImageIcon("colorMagenta.gif");
			icons[6] = IconResource.getInstance().getImageIcon("colorRed.gif");
			icons[7] = IconResource.getInstance().getImageIcon("colorYellow.gif");
			cbColor = new JComboBox(icons);
			cbColor.setBounds(80,40,120,25);
			if (this.dp != null) {
				Color color = dp.getDefaultTextColor();
				if (color == Color.black)
					cbColor.setSelectedIndex(0);
				else if (color == Color.blue)
					cbColor.setSelectedIndex(1);
				else if (color == Color.cyan)
					cbColor.setSelectedIndex(2);
				else if (color == Color.green)
					cbColor.setSelectedIndex(3);
				else if (color == Color.orange)
					cbColor.setSelectedIndex(4);
				else if (color == Color.magenta)
					cbColor.setSelectedIndex(5);
				else if (color == Color.red)
					cbColor.setSelectedIndex(6);
				else if (color == Color.yellow)
					cbColor.setSelectedIndex(7);
			}
			
			lblSize = new JLabel("Font Size:");
			lblSize.setBounds(220,40,70,25);
			String[] fontSize = { "1", "2", "4", "6", "8", "10", "12", "14",
					"16", "18", "20", "22", "24" };
			cbSize = new JComboBox(fontSize);
			cbSize.setBounds(290,40,70,25);
			if (this.dp != null) {
				if (dp.getDefaultFontSize() == 1) {
					cbSize.setSelectedIndex(0);
				} else if (dp.getDefaultFontSize() == 2) {
					cbSize.setSelectedIndex(1);
				} else if (dp.getDefaultFontSize() == 4) {
					cbSize.setSelectedIndex(2);
				} else if (dp.getDefaultFontSize() == 6) {
					cbSize.setSelectedIndex(3);
				} else if (dp.getDefaultFontSize() == 8) {
					cbSize.setSelectedIndex(4);
				}else if (dp.getDefaultFontSize() == 10) {
					cbSize.setSelectedIndex(5);
				} else if (dp.getDefaultFontSize() == 12) {
					cbSize.setSelectedIndex(6);
				} else if (dp.getDefaultFontSize() == 14) {
					cbSize.setSelectedIndex(7);
				} else if (dp.getDefaultFontSize() == 16) {
					cbSize.setSelectedIndex(8);
				} else if (dp.getDefaultFontSize() == 18) {
					cbSize.setSelectedIndex(9);
				} else if (dp.getDefaultFontSize() == 20) {
					cbSize.setSelectedIndex(10);
				} else if (dp.getDefaultFontSize() == 22) {
					cbSize.setSelectedIndex(11);
				} else if (dp.getDefaultFontSize() == 24) {
					cbSize.setSelectedIndex(12);
				} 
			}

			

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(110,80,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(190,80,80,25);
			
			jContentPanel.add(lblName);
			jContentPanel.add(tfName);
			jContentPanel.add(lblWidth);
			jContentPanel.add(tfWidth);
			jContentPanel.add(lblSize);
			jContentPanel.add(cbSize);
			jContentPanel.add(lblColor);
			jContentPanel.add(cbColor);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("ok")) {

			if (!tfName.getText().trim().equals("")) {
				String newName = tfName.getText().trim();
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
						.renameDescriptionPanel(
								DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTitle(),
								DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
										.getTrackContainer()
										.getSelectedAxisCurveContainer()
										.getWellName(), dp.getName(), newName);
				dp.setName(newName);
				dp.getParent().repaintFather();
			}

			if (!tfWidth.getText().trim().equals("")) {
				dp.setBesideTypeWidth(Integer
						.parseInt(tfWidth.getText().trim()));
				dp.getParent().setFillRedrawFlag(true);
				dp.getParent().repaint();
				dp.getParent().repaintFather();
			}
			
			if (cbColor.getSelectedIndex() == 0) {
				dp.setDefaultTextColor(Color.black);
			} else if (cbColor.getSelectedIndex() == 1) {
				dp.setDefaultTextColor(Color.blue);
			} else if (cbColor.getSelectedIndex() == 2) {
				dp.setDefaultTextColor(Color.cyan);
			} else if (cbColor.getSelectedIndex() == 3) {
				dp.setDefaultTextColor(Color.green);
			} else if (cbColor.getSelectedIndex() == 4) {
				dp.setDefaultTextColor(Color.orange);
			} else if (cbColor.getSelectedIndex() == 5) {
				dp.setDefaultTextColor(Color.magenta);
			} else if (cbColor.getSelectedIndex() == 6) {
				dp.setDefaultTextColor(Color.red);
			} else if (cbColor.getSelectedIndex() == 7) {
				dp.setDefaultTextColor(Color.yellow);
			}
			
			dp.setDefaultFontSize(Integer.parseInt(cbSize.getSelectedItem().toString()));

			this.setVisible(false);
			this.dispose();
			return;
		}

		if (e.getActionCommand().equals("cancel")) {
			this.setVisible(false);
			this.dispose();
			return;
		}
	}

}
