/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.LithologyPanel;

/**
 * A dialog to edit lithology property.
 * @author Team
 *
 */
public class EditLithologySettingDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private JPanel jContentPanel = null;

	private LithologyPanel lp = null;

	private JLabel lblName = null;

	private JTextField tfName = null;

	private JLabel lblWidth = null;

	private JTextField tfWidth = null;

	private JLabel lblSize = null;

	private JComboBox cbSize = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;
	
	public EditLithologySettingDialog(LithologyPanel lp) {
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Lithology Settings");
		this.lp = lp;
		this.setContentPane(getContentPanel());
		this.setBounds(150,150,230,180);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	public JPanel getContentPanel() {
		if (jContentPanel == null) {
			jContentPanel = new JPanel();

			jContentPanel.setLayout(null);

			lblName = new JLabel("Name:");
			lblName.setBounds(10,10,70,25);
			tfName = new JTextField();
			if (this.lp != null) {
				tfName.setText(lp.getName());
			}
			tfName.setBounds(80,10,120,25);

			lblWidth = new JLabel("Width:");
			lblWidth.setBounds(10,40,70,25);
			tfWidth = new JTextField();
			if (this.lp != null) {
				tfWidth.setText(lp.getBesideTypeWidth() + "");
			}
			tfWidth.setBounds(80,40,120,25);
		
			lblSize = new JLabel("Font Size:");
			lblSize.setBounds(10,70,70,25);
			String[] fontSize = { "1", "2", "4", "6", "8", "10", "12", "14",
					"16", "18", "20", "22", "24" };
			cbSize = new JComboBox(fontSize);
			cbSize.setBounds(80,70,120,25);
			if (this.lp != null) {
				if (lp.getDefaultFontSize() == 1) {
					cbSize.setSelectedIndex(0);
				} else if (lp.getDefaultFontSize() == 2) {
					cbSize.setSelectedIndex(1);
				} else if (lp.getDefaultFontSize() == 4) {
					cbSize.setSelectedIndex(2);
				} else if (lp.getDefaultFontSize() == 6) {
					cbSize.setSelectedIndex(3);
				} else if (lp.getDefaultFontSize() == 8) {
					cbSize.setSelectedIndex(4);
				}else if (lp.getDefaultFontSize() == 10) {
					cbSize.setSelectedIndex(5);
				} else if (lp.getDefaultFontSize() == 12) {
					cbSize.setSelectedIndex(6);
				} else if (lp.getDefaultFontSize() == 14) {
					cbSize.setSelectedIndex(7);
				} else if (lp.getDefaultFontSize() == 16) {
					cbSize.setSelectedIndex(8);
				} else if (lp.getDefaultFontSize() == 18) {
					cbSize.setSelectedIndex(9);
				} else if (lp.getDefaultFontSize() == 20) {
					cbSize.setSelectedIndex(10);
				} else if (lp.getDefaultFontSize() == 22) {
					cbSize.setSelectedIndex(11);
				} else if (lp.getDefaultFontSize() == 24) {
					cbSize.setSelectedIndex(12);
				} 
			}

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(30,110,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(110,110,80,25);
			
			jContentPanel.add(lblName);
			jContentPanel.add(tfName);
			jContentPanel.add(lblWidth);
			jContentPanel.add(tfWidth);
			jContentPanel.add(lblSize);
			jContentPanel.add(cbSize);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	
		if (e.getActionCommand().equals("ok")) {

			if (!tfName.getText().trim().equals("")) {
				String newName = tfName.getText().trim();
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
						.renameDescriptionPanel(
								DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTitle(),
								DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
										.getTrackContainer()
										.getSelectedAxisCurveContainer()
										.getWellName(), lp.getName(), newName);
				lp.setName(newName);
				lp.getParent().repaintFather();
			}

			if (!tfWidth.getText().trim().equals("")) {
				lp.setBesideTypeWidth(Integer
						.parseInt(tfWidth.getText().trim()));
				lp.setFillRedrawFlag(true);
				lp.getParent().setFillRedrawFlag(true);
				lp.getParent().repaintFather();
			}
			
			lp.setDefaultFontSize(Integer.parseInt(cbSize.getSelectedItem().toString()));

			this.setVisible(false);
			this.dispose();
			return;
		}

		if (e.getActionCommand().equals("cancel")) {
			this.setVisible(false);
			this.dispose();
			return;
		}
	}
	
}
