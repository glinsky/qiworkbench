/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.WellInfo;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.fill.CurveFillBeside;
import com.gwsys.welllog.view.zone.ZoneCollection;

/**
 * A dialog can edit the properties of curve.
 * @author Team
 *
 */
public class EditBesideCurveDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private CurveView cv = null;

	private JPanel jContentPanel = null;
	private JLabel lblName = null;
	private JLabel lblCurve = null;
	private JLabel lblCurveColor = null;
	private JButton btnShowCurveColor = null;
	private JLabel lblStroke = null;
	private JTextField tfStroke = null;
	private JButton btnOk = null;
	private JButton btnCancel = null;
	private JCheckBox jcbVisible = null;
	private JCheckBox jcbLogarithm = null;
	private JLabel lblType = null;
	private JComboBox cbType = null;
	private JCheckBox jcbDisplayOrder = null;
	private JLabel lblCurvePanelWidth = null;
	private JTextField tfCurvePanelWidth = null;

	private JLabel lblFillColor = null;
	private JButton btnShowFillColor = null;
	private JLabel lblOrietation = null;
	private JComboBox cmbOrietation = null;

	private JLabel lblDisplayMix = null;
	private JLabel lblDisplayMax = null;
	private JTextField tfDisplayMix = null;
	private JTextField tfDisplayMax = null;

	private JLabel lblUnit = null;
	private JComboBox cmbUnit = null;

	private JPanel jpanelDisplay = null;
	JTextField tfDisplay = null;
	private JPanel jpanelLine = null;
	JTextField tfLine = null;
	private JPanel jpanelFill = null;
	JTextField tfFill = null;

	private CurvePanel cp = null;
	private CurveFillBeside cfb = null;

	//	added by Alex
	private JCheckBox jcbHoldProperties = null;
	private int indexUnitBox;
	private MainFrameView mainview;
	/**
	 * Constructor.
	 * @param cv CurveView
	 * @param view MainFrameView
	 */
	public EditBesideCurveDialog(CurveView cv, MainFrameView view)
	{
		super(view.getFrame());
		this.cv = cv;
		mainview = view;
		this.setTitle("Settings of Curve");
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,470,480);
		this.setResizable(false);
		this.setModal(true);
	}

	private JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);

			cp = (CurvePanel)cv.getCurveLayer().getCurveSelectedList().get(0);
			for(int i=0 ; i<cv.getCurveLayer().getCurveFillBesideList().size() ; i++){
				if(((CurveFillBeside)cv.getCurveLayer().getCurveFillBesideList().get(i))
						.getCp().getCd().getHashCurveName().equals(cp.getCd().getHashCurveName()))
					this.cfb = (CurveFillBeside)cv.getCurveLayer().getCurveFillBesideList().get(i);
			}

			// Name
			lblName = new JLabel("Name:");
			lblName.setBounds(10,10,50,25);
			lblCurve = new JLabel( cp.getCd().getCurveName());
			lblCurve.setBounds(60,10,100,25);

			// Hold Curve properties
			jcbHoldProperties = new JCheckBox("Enable Global Settings");
			jcbHoldProperties.setSelected(false);
			jcbHoldProperties.setBounds(300,10,135,25);

			// Display
			tfDisplay = new JTextField();
			tfDisplay.setBackground(SystemColor.control);
			tfDisplay.setEnabled(false);
			tfDisplay.setBorder(null);
			tfDisplay.setDisabledTextColor(Color.black);
			tfDisplay.setText(" Display Property");
			tfDisplay.setBounds(new Rectangle(20, 35, 110, 16));

			jpanelDisplay = new JPanel();
			jpanelDisplay.setLayout(null);
			jpanelDisplay.setBorder(BorderFactory.createEtchedBorder());
			jpanelDisplay.setBounds(10, 42, 440, 150);

			// Line
			tfLine = new JTextField();
			tfLine.setBackground(SystemColor.control);
			tfLine.setEnabled(false);
			tfLine.setBorder(null);
			tfLine.setDisabledTextColor(Color.black);
			tfLine.setText(" Line Property");
			tfLine.setBounds(new Rectangle(20, 203, 100, 16));

			jpanelLine = new JPanel();
			jpanelLine.setLayout(null);
			jpanelLine.setBorder(BorderFactory.createEtchedBorder());
			jpanelLine.setBounds(10, 210, 440, 110);

			// Fill
			tfFill = new JTextField();
			tfFill.setBackground(SystemColor.control);
			tfFill.setEnabled(false);
			tfFill.setBorder(null);
			tfFill.setDisabledTextColor(Color.black);
			tfFill.setText(" Fill Property");
			tfFill.setBounds(new Rectangle(20, 331, 100, 16));

			jpanelFill = new JPanel();
			jpanelFill.setLayout(null);
			jpanelFill.setBorder(BorderFactory.createEtchedBorder());
			jpanelFill.setBounds(10, 338 , 440, 60);

			lblCurveColor = new JLabel("Curve Color:");
			lblCurveColor.setBounds(10,24,80,25);
			btnShowCurveColor = new JButton("Change");
			btnShowCurveColor.setIcon(EditCurveDialog.createColorIcon(cp.getCurveColor()));
			btnShowCurveColor.setForeground(cp.getCurveColor());
			btnShowCurveColor.setActionCommand("showCurveColorChooser");
			btnShowCurveColor.addActionListener(this);
			btnShowCurveColor.setBounds(90,24,100,25);

			lblType = new JLabel("Stroke Type:");
			lblType.setBounds(10,67,80,25);
			String choise[] = new String[5];
			for (int i=0; i<5; i++) {
				choise[0] = Integer.toString(ComboBoxRender.SOLID);
				choise[1] = Integer.toString(ComboBoxRender.DASH);
				choise[2] = Integer.toString(ComboBoxRender.DASH_DOT);
				choise[3] = Integer.toString(ComboBoxRender.DOT);
				choise[4] = Integer.toString(ComboBoxRender.DASH_DOT_DOT);
			}
			cbType = new JComboBox(choise);
			ComboBoxRender renderer= new ComboBoxRender();
	        renderer.setPreferredSize(new Dimension(100, 30));
	        cbType.setRenderer(renderer);
			cbType.setBounds(90,67,150,25);
			if(cp.getStokeType() == ComboBoxRender.SOLID)
				cbType.setSelectedIndex(0);
			else if(cp.getStokeType() == ComboBoxRender.DASH)
				cbType.setSelectedIndex(1);
			else if(cp.getStokeType() == ComboBoxRender.DASH_DOT)
				cbType.setSelectedIndex(2);
			else if(cp.getStokeType() == ComboBoxRender.DOT)
				cbType.setSelectedIndex(3);
			else if(cp.getStokeType() == ComboBoxRender.DASH_DOT_DOT)
				cbType.setSelectedIndex(4);

			lblFillColor = new JLabel("Fill Color:");
			lblFillColor.setBounds(10,24,80,25);
			btnShowFillColor = new JButton("Change");
			Color fill = Color.PINK;
			if(cfb != null)
				fill = cfb.getFillColor();

			btnShowFillColor.setIcon(EditCurveDialog.createColorIcon(fill));
			btnShowFillColor.setForeground(fill);
			btnShowFillColor.setActionCommand("showFillColorChooser");
			btnShowFillColor.addActionListener(this);
			btnShowFillColor.setBounds(90,24,100,25);

			lblOrietation = new JLabel("Orientation:");
			lblOrietation.setBounds(200,24,80,25);
			cmbOrietation = new JComboBox();
			cmbOrietation.addItem(ConstantVariableList.FILL_LEFT);
			cmbOrietation.addItem(ConstantVariableList.FILL_RIGHT);
			cmbOrietation.addItem(ConstantVariableList.FILL_ALL);
			cmbOrietation.addItem(ConstantVariableList.FILL_NONE);
			cmbOrietation.setBounds(290,24,120,25);
			if(cfb == null){
				cmbOrietation.setSelectedIndex(0);
			}else{
				if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_LEFT)){
					cmbOrietation.setSelectedIndex(0);
				}else if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_RIGHT)){
					cmbOrietation.setSelectedIndex(1);
				}else if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_ALL)){
					cmbOrietation.setSelectedIndex(2);
				}else if(cfb.getLeftOrRight().equals(ConstantVariableList.FILL_NONE)){
					cmbOrietation.setSelectedIndex(3);
				}
			}

			lblStroke = new JLabel("Stroke Width:");
			lblStroke.setBounds(250,24,95,25);
			tfStroke = new JTextField(Float.toString(cp.getStrokeWidth()));
			tfStroke.setBounds(350,24,70,25);

			lblCurvePanelWidth = new JLabel("Curve Panel Width:");
			lblCurvePanelWidth.setBounds(10,24,115,25);
			tfCurvePanelWidth = new JTextField(cp.getBesideTypeWidth() + "" , 20);
			tfCurvePanelWidth.setBounds(125,24,115,25);

			lblDisplayMix = new JLabel("Min display value:");
			lblDisplayMix.setBounds(10,55,110,25);
			tfDisplayMix = new JTextField(Float.toString(cp.getMinXDisplay()));
			tfDisplayMix.setBounds(125,55,80,25);
			lblDisplayMax = new JLabel("Max display value:");
			lblDisplayMax.setBounds(10,86,110,25);
			tfDisplayMax = new JTextField(Float.toString(cp.getMaxXDisplay()));
			tfDisplayMax.setBounds(125,86,80,25);


			jcbDisplayOrder = new JCheckBox("Reverse");
			jcbDisplayOrder.setSelected(cp.isOrderDesc());
			//jcbDisplayOrder.setBounds(180,115,100,25);
			jcbDisplayOrder.setBounds(110,115,80,25);

			jcbVisible = new JCheckBox("Visible");
			jcbVisible.setSelected(cp.isVisible());
			//jcbVisible.setBounds(20,115,100,25);
			jcbVisible.setBounds(20,115,100,25);

			jcbLogarithm = new JCheckBox("Logarithmic");
			jcbLogarithm.setSelected(cp.isLogarithm());
			//jcbLogarithm.setBounds(340,115,80,25);
			jcbLogarithm.setBounds(200,115,80,25);

			// added by Alex
			lblUnit = new JLabel("Unit:");
			lblUnit.setBounds(315,115,80,25);

			cmbUnit = new JComboBox();
			cmbUnit.setEnabled(fillUnitComboBox());
			cmbUnit.setEditable(true);
			cmbUnit.setToolTipText("select new unit to this curve");
			cmbUnit.setBounds(340,115,80,25);
			cmbUnit.addActionListener(this);
			cmbUnit.setActionCommand("changeCurveUnit");

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(255,415,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(345,415,80,25);

			// add component
			jpanelDisplay.add(lblCurvePanelWidth);
			jpanelDisplay.add(tfCurvePanelWidth);
			jpanelDisplay.add(lblDisplayMix);
			jpanelDisplay.add(tfDisplayMix);
			jpanelDisplay.add(lblDisplayMax);
			jpanelDisplay.add(tfDisplayMax);
			jpanelDisplay.add(jcbDisplayOrder);
			jpanelDisplay.add(jcbVisible);
			jpanelDisplay.add(jcbLogarithm);
			jpanelDisplay.add(lblUnit);
			jpanelDisplay.add(cmbUnit);

			jpanelLine.add(lblCurveColor);
			jpanelLine.add(btnShowCurveColor);
			jpanelLine.add(lblType);
			jpanelLine.add(cbType);
			jpanelLine.add(lblStroke);
			jpanelLine.add(tfStroke);

			jpanelFill.add(lblFillColor);
			jpanelFill.add(btnShowFillColor);
			jpanelFill.add(lblOrietation);
			jpanelFill.add(cmbOrietation);

			jContentPanel.add(lblName);
			jContentPanel.add(lblCurve);
			jContentPanel.add(jcbHoldProperties);	//added by Alex

			jContentPanel.add(tfDisplay);
			jContentPanel.add(jpanelDisplay);

			jContentPanel.add(tfLine);
			jContentPanel.add(jpanelLine);

			jContentPanel.add(tfFill);
			jContentPanel.add(jpanelFill);

			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok"))
		{
			float curvewidth = 1;
			try {
				curvewidth = Float.parseFloat(tfStroke.getText().trim());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			cp.setStrokeWidth(curvewidth);

			if(cbType.getSelectedIndex() == 0)
				cp.setStokeType(ComboBoxRender.SOLID);
			else if(cbType.getSelectedIndex() == 1)
				cp.setStokeType(ComboBoxRender.DASH);
			else if(cbType.getSelectedIndex() == 2)
				cp.setStokeType(ComboBoxRender.DASH_DOT);
			else if(cbType.getSelectedIndex() == 3)
				cp.setStokeType(ComboBoxRender.DOT);
			else if(cbType.getSelectedIndex() == 4)
				cp.setStokeType(ComboBoxRender.DASH_DOT_DOT);


			cp.setCurveColor(this.btnShowCurveColor.getForeground());
			cp.setMinXDisplay(Float.parseFloat(this.tfDisplayMix.getText().trim()));
			cp.setMaxXDisplay(Float.parseFloat(this.tfDisplayMax.getText().trim()));
			cp.setVisible(this.jcbVisible.isSelected());
			cp.setLogarithm(this.jcbLogarithm.isSelected());
			cp.setOrderDesc(this.jcbDisplayOrder.isSelected());

			int curvePanelWidth = Integer.parseInt(tfCurvePanelWidth.getText().trim());
			cp.setBesideTypeWidth(curvePanelWidth);

			///////////////
			if(cfb == null){		//create new curvefill
				cfb = new CurveFillBeside();
				for(int i = 0 ; i<cv.getCurveLayer().getCurveSelectedList().size();i++) //different from top
				{
					cfb.setCp((CurvePanel) cv.getCurveLayer().getCurveSelectedList().get(i));
					cfb.setLeftOrRight((String)this.cmbOrietation.getSelectedItem());
					cfb.constructFill(cv);
					cfb.setFillColor(this.btnShowFillColor.getForeground());
					cv.getCurveLayer().addFillBesideToList(cfb);
				}
			}else{	//change curvefill property
				cfb.setLeftOrRight((String)this.cmbOrietation.getSelectedItem());
				cfb.setFillColor(this.btnShowFillColor.getForeground());
				cfb.constructFill(cv);
			}

			// hold global curve settings
			if (jcbHoldProperties.isSelected()){
				ViewCoreOperator.addCurveSettings(cp);
				//update all existing curves
				ViewCoreOperator.applyGlobalSettings();
			}
			if (indexUnitBox!=this.cmbUnit.getSelectedIndex()){
				if (cv.getZoneLayer() instanceof ZoneCollection){
					((ZoneCollection)cv.getZoneLayer()).zoneUpdated();
				}
				if (mainview.getZoneListener()!=null)
					mainview.getZoneListener().updateFluidProperty();
			}
			///////////////
			this.setVisible(false);
			this.dispose();
			cv.setFillRedrawFlag(true);
			cv.repaintFather();
			return;
		}

		/////////when button cancel is clicked//////////
		else if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
		else if(e.getActionCommand().equals("showCurveColorChooser"))
		{
			Color selected=JColorChooser.showDialog(this,
					"Choose Curve Color", btnShowCurveColor.getForeground());
			if (selected!=null){
				btnShowCurveColor.setIcon(EditCurveDialog.createColorIcon(selected));
				btnShowCurveColor.setForeground(selected);
			}
		}
		else if(e.getActionCommand().equals("showFillColorChooser"))
		{
			Color selected=JColorChooser.showDialog(this,
					"Choose Fill Color", btnShowFillColor.getForeground());
			if (selected!=null){
				btnShowFillColor.setIcon(EditCurveDialog.createColorIcon(selected));
				btnShowFillColor.setForeground(selected);
			}
		}
		else if(e.getActionCommand().equals("changeCurveUnit"))	{
			String item = cmbUnit.getSelectedItem().toString();
			cp.getCd().setCurveUnit(item);
		}

	}

	public void remove(){
		if(cp != null){
			ViewCoreOperator.deleteCurve(cp.getCd().getHashCurveName(), cv.getParentAxisCurveContainer().getWellName());
		}
		this.setVisible(false);
		this.dispose();
	}

	/**
	 * fill combo box of unit items
	 * @return true if combo box was filled, false if no
	 * @author Alex
	 */
	@SuppressWarnings("unchecked")
	private boolean fillUnitComboBox()	{
		String wellName = cv.getParentAxisCurveContainer().getWellName();
		WellInfo winfo = ViewCoreOperator.getWellInfoByWell(wellName);
		String[] units=winfo.getUnitAsArray();
		this.cmbUnit.addItem(cp.getCd().getCurveUnit());
		for (int i = 0; i < units.length; i++)
			this.cmbUnit.addItem(units[i]);
		this.cmbUnit.setSelectedIndex(0);

		indexUnitBox = 0;
		if (this.cmbUnit.getItemCount()>0)
			return true;

		return false;
	}

}
