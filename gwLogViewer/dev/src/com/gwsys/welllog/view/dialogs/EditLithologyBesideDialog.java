/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.WellLogPattern;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.fill.CurveLithologyBeside;
import com.gwsys.welllog.view.zone.ZoneProp;

public class EditLithologyBesideDialog extends JDialog implements ActionListener {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private JPanel jContentPanel = null;

	private JLabel lblType = null;

	private JComboBox cbType = null;

	private JLabel lblOrietation = null;

	private JComboBox cmbOrietation = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private CurveView cv = null;

	private CurveLithologyBeside clb = null;

	/**
	 * default construction
	 * @param cv
	 */
	public EditLithologyBesideDialog(CurveView cv) {
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Lithology Property Dialog");
		this.cv = cv;

		CurvePanel cp = (CurvePanel)cv.getCurveLayer().getCurveSelectedList().get(0);
		for(int i=0 ; i<cv.getCurveLayer().getCurveLithologyBesideList().size() ; i++){
			if(((CurveLithologyBeside)cv.getCurveLayer().getCurveLithologyBesideList().get(i))
					.getCp().getCd().getHashCurveName().equals(cp.getCd().getHashCurveName()))
				this.clb = (CurveLithologyBeside)cv.getCurveLayer().getCurveLithologyBesideList().get(i);
		}

		this.setContentPane(getContentPanel());
		this.setBounds(150,100,230,140);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	/**
	 * if change curveFill property,use this construction method
	 *
	 * @param cv
	 * @param cf
	 *            which CurveFillTop to change property
	 * @author GaoFei
	 */
	public EditLithologyBesideDialog(CurveView cv, CurveLithologyBeside clb) {
		super(DataBuffer.getJMainFrame().getFrame());
		this.setTitle("Lithology Property Dialog");
		this.cv = cv;
		this.clb = clb;
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,230,140);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	public JPanel getContentPanel() {
		if (jContentPanel == null) {
			jContentPanel = new JPanel();

			jContentPanel.setLayout(null);

			lblType = new JLabel("Fill Type:");
			lblType.setBounds(10,10,100,25);
			WellLogPattern choise[] = ZoneProp.getWellLogPatterns();
			cbType = new JComboBox(choise);
			FillComboBoxRenderer renderer= new FillComboBoxRenderer();
			renderer.setPreferredSize(new Dimension(80, 25));
	        cbType.setRenderer(renderer);
	        if(clb != null)
	        {
	        	cbType.setSelectedIndex(clb.getWhichtexture());
	        }
	        cbType.setBounds(110,10,100,25);

	        lblOrietation = new JLabel("Fill Orietation:");
	        lblOrietation.setBounds(10,40,100,25);
			cmbOrietation = new JComboBox();
			cmbOrietation.addItem(ConstantVariableList.FILL_LEFT);
			cmbOrietation.addItem(ConstantVariableList.FILL_RIGHT);
			cmbOrietation.addItem(ConstantVariableList.FILL_ALL);
			cmbOrietation.addItem(ConstantVariableList.FILL_NONE);
			cmbOrietation.setBounds(110,40,100,25);
			if(clb != null)
			{
				if(clb.getLeftOrRight().equals(ConstantVariableList.FILL_LEFT))
				{
					cmbOrietation.setSelectedIndex(0);
				}
				else if(clb.getLeftOrRight().equals(ConstantVariableList.FILL_RIGHT))
				{
					cmbOrietation.setSelectedIndex(1);
				}
				else if(clb.getLeftOrRight().equals(ConstantVariableList.FILL_ALL))
				{
					cmbOrietation.setSelectedIndex(2);
				}
				else if(clb.getLeftOrRight().equals(ConstantVariableList.FILL_NONE))
				{
					cmbOrietation.setSelectedIndex(3);
				}
			}
			else
			{
				cmbOrietation.setSelectedIndex(0);
			}

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(30,80,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(110,80,80,25);

			jContentPanel.add(lblType);
			jContentPanel.add(cbType);

			jContentPanel.add(lblOrietation);
			jContentPanel.add(cmbOrietation);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
		}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("ok")) {
			if (clb == null) { // create new curvefill
				clb = new CurveLithologyBeside();

				for (int i = 0; i < cv.getCurveLayer().getCurveSelectedList().size(); i++) {
					clb.setCp((CurvePanel) cv.getCurveLayer().getCurveSelectedList().get(i));
					clb.setLeftOrRight((String)this.cmbOrietation.getSelectedItem());
					clb.constructFill(cv);
					clb.setWhichtexture(cbType.getSelectedIndex());
					cv.getCurveLayer().addLithologyBesideToList(clb);// .addFillToList(cl);
				}
			} else { // change curvefill property
				clb.setLeftOrRight((String)this.cmbOrietation.getSelectedItem());
				clb.setWhichtexture(cbType.getSelectedIndex());
				clb.constructFill(cv);
			}
			cv.repaintFather();
			this.setVisible(false);
			this.dispose();
			return;
		}

		// ///////when button cancel is clicked//////////
		if (e.getActionCommand().equals("cancel")) {
			this.setVisible(false);
			this.dispose();
			return;
		}
	}

	class FillComboBoxRenderer extends JLabel implements ListCellRenderer {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		private WellLogPattern pattern;

		public FillComboBoxRenderer() {
			setOpaque(true);
		}

		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {

			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			pattern = (WellLogPattern) value;

			return this;
		}

		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setPaint(pattern);

			g2.fillRect(0, 0, getWidth(), getHeight());
		}
	}

}
