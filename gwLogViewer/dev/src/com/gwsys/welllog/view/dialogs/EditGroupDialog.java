/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;

/**
 * Dialog edits the properties of group.
 * @author Team
 *
 */
public class EditGroupDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JPanel jContentPanel = null;
	private JLabel lblCurveColor = null;
	private JButton btnShowCurveColor = null;
	private JLabel lblStroke = null;
	private JTextField tfStroke = null;
	private JButton btnOk = null;
	private JButton btnCancel = null;
	private JLabel lblType = null;
	private JComboBox cbType = null;
	private JLabel lblCurvePanelWidth = null;
	private JTextField tfCurvePanelWidth = null;

	private JPanel jpanelGroup = null;
	private JTextField tfGroup = null;
	private JPanel jpanelCurve = null;
	private JTextField tfCurve = null;

	private CurveView cv = null;
	private CurveGroup group = null;
	private JCheckBox logCheckbox = null;
	private JCheckBox jcbDisplay = null;
	private JLabel lblDisplayMix = null;
	private JLabel lblDisplayMax = null;
	private JTextField tfDisplayMix = null;
	private JTextField tfDisplayMax = null;

	/**
	 * Constructor.
	 * @param cv CurveView
	 * @param group CurveGroup
	 */
	public EditGroupDialog(CurveView cv , CurveGroup group)
	{
		super(DataBuffer.getJMainFrame().getFrame());
		this.cv = cv;
		this.group = group;
		this.setTitle("Settings of Group");
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,470,350);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	private JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);

			tfGroup = new JTextField();
			tfGroup.setBackground(SystemColor.control);
			tfGroup.setEnabled(false);
			tfGroup.setBorder(null);
			tfGroup.setDisabledTextColor(Color.black);
			tfGroup.setText(" Group Property");
			tfGroup.setBounds(new Rectangle(20, 10, 90, 16));

			jpanelGroup = new JPanel();
			jpanelGroup.setLayout(null);
			jpanelGroup.setBorder(BorderFactory.createEtchedBorder());
			jpanelGroup.setBounds(10, 17, 440, 140);

			lblCurvePanelWidth = new JLabel("Group Panel Width:");
			lblCurvePanelWidth.setBounds(10,24,125,25);
			tfCurvePanelWidth = new JTextField(ConstantVariableList.BESIDETYPE_WIDTH+"");
			tfCurvePanelWidth.setBounds(125,24,125,25);

			jcbDisplay = new JCheckBox("Set X display range");
			jcbDisplay.setBounds(10, 54, 140, 25);
			jcbDisplay.setSelected(group.isSetRange());
			lblDisplayMix = new JLabel("Min value:");
			lblDisplayMix.setBounds(160,54,70,25);
			tfDisplayMix = new JTextField(group.getMinXDisplay());
			tfDisplayMix.setBounds(230,54,80,25);
			lblDisplayMax = new JLabel("Max value:");
			lblDisplayMax.setBounds(160,84,70,25);
			tfDisplayMax = new JTextField(group.getMaxXDisplay());
			tfDisplayMax.setBounds(230,84,80,25);

			logCheckbox = new JCheckBox("Apply logarithmic grid");
			logCheckbox.setBounds(10, 104, 140, 25);
			logCheckbox.setActionCommand("applyLogarithmicGrid");
			logCheckbox.addActionListener(this);
			logCheckbox.setSelected(group.isLogarithmic());

			jpanelCurve = new JPanel();
			jpanelCurve.setLayout(null);
			jpanelCurve.setBorder(BorderFactory.createEtchedBorder());
			jpanelCurve.setBounds(10, 168, 440, 100);

			tfCurve = new JTextField();
			tfCurve.setBackground(SystemColor.control);
			tfCurve.setEnabled(false);
			tfCurve.setBorder(null);
			tfCurve.setDisabledTextColor(Color.black);
			tfCurve.setText(" Curve Property");
			tfCurve.setBounds(20, 160, 90, 16);

			lblCurveColor = new JLabel("Curve Color:");
			lblCurveColor.setBounds(10,24,80,25);
			btnShowCurveColor = new JButton("Change");
			btnShowCurveColor.setForeground(Color.BLACK);
			btnShowCurveColor.setActionCommand("showCurveColorChooser");
			btnShowCurveColor.addActionListener(this);
			btnShowCurveColor.setBounds(90,24,80,25);

			lblType = new JLabel("Stroke Type:");
			lblType.setBounds(10,67,80,25);
			String choise[] = new String[5];
			for (int i=0; i<5; i++) {
				choise[0] = Integer.toString(ComboBoxRender.SOLID);
				choise[1] = Integer.toString(ComboBoxRender.DOT);
				choise[2] = Integer.toString(ComboBoxRender.DASH);
				choise[3] = Integer.toString(ComboBoxRender.DASH_DOT);
				choise[4] = Integer.toString(ComboBoxRender.DASH_DOT_DOT);
			}
			cbType = new JComboBox(choise);
			ComboBoxRender renderer= new ComboBoxRender();
	        renderer.setPreferredSize(new Dimension(100, 30));
	        cbType.setRenderer(renderer);
			cbType.setBounds(90,67,150,25);
			cbType.setSelectedIndex(0);

			lblStroke = new JLabel("Stroke Width:");
			lblStroke.setBounds(250,24,95,25);
			tfStroke = new JTextField("1");
			tfStroke.setBounds(350,24,70,25);

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(155,280,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(235,280,80,25);

			jpanelGroup.add(lblCurvePanelWidth);
			jpanelGroup.add(tfCurvePanelWidth);
			jpanelGroup.add(jcbDisplay);
			jpanelGroup.add(lblDisplayMix);
			jpanelGroup.add(tfDisplayMix);
			jpanelGroup.add(lblDisplayMax);
			jpanelGroup.add(tfDisplayMax);
			jpanelGroup.add(logCheckbox);

			jpanelCurve.add(lblCurveColor);
			jpanelCurve.add(btnShowCurveColor);
			jpanelCurve.add(lblType);
			jpanelCurve.add(cbType);
			jpanelCurve.add(lblStroke);
			jpanelCurve.add(tfStroke);

			jContentPanel.add(tfGroup);
			jContentPanel.add(jpanelGroup);
			jContentPanel.add(tfCurve);
			jContentPanel.add(jpanelCurve);


			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
			}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok"))
		{
			float curvewidth = 1;
			try {
				curvewidth = Float.parseFloat(tfStroke.getText().trim());
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(this, "Please input a number for curve width!");
				return;
			}
			if (tfDisplayMix.getText().trim().length()==0||
					tfDisplayMax.getText().trim().length()==0){
				JOptionPane.showMessageDialog(this, "Please input a number for display range!");
				return;
			}

			float minXDisplay = Float.parseFloat(tfDisplayMix.getText().trim());
			float maxXDisplay = Float.parseFloat(tfDisplayMax.getText().trim());

			group.setSetRange(jcbDisplay.isSelected());
			if (jcbDisplay.isSelected()){
				group.setMinXDisplay(Float.toString(minXDisplay));
				group.setMaxXDisplay(Float.toString(maxXDisplay));
			}
			group.setLogarithmic(logCheckbox.isSelected());
			for(int i=0 ; i<group.getCurveList().size() ; i++){
				CurvePanel cp = (CurvePanel)group.getCurveList().get(i);

				cp.setStrokeWidth(curvewidth);

				cp.setMinXDisplay(minXDisplay);
				cp.setMaxXDisplay(maxXDisplay);

				if(cbType.getSelectedIndex() == 0)
					cp.setStokeType(ComboBoxRender.SOLID);
				else if(cbType.getSelectedIndex() == 1)
					cp.setStokeType(ComboBoxRender.DASH);
				else if(cbType.getSelectedIndex() == 2)
					cp.setStokeType(ComboBoxRender.DASH_DOT);
				else if(cbType.getSelectedIndex() == 3)
					cp.setStokeType(ComboBoxRender.DOT);
				else if(cbType.getSelectedIndex() == 4)
					cp.setStokeType(ComboBoxRender.DASH_DOT_DOT);

				cp.setCurveColor(this.btnShowCurveColor.getForeground());
				int curvePanelWidth = Integer.parseInt(tfCurvePanelWidth.getText().trim());
				cp.setBesideTypeWidth(curvePanelWidth);
			}

			this.setVisible(false);
			this.dispose();

			cv.setFillRedrawFlag(true);
			cv.repaintFather();
			return;
		}else if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}else if(e.getActionCommand().equals("showCurveColorChooser")){
			Color selected=JColorChooser.showDialog(this,
					"Choose Curve Color", btnShowCurveColor.getForeground());
			if (selected!=null){
				btnShowCurveColor.setForeground(selected);
			}
		}else if(e.getActionCommand().equals("applyLogarithmicGrid")){
			if (logCheckbox.isSelected()){
				jcbDisplay.setSelected(true);
			}
		}

	}

}
