/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.LogWinUtility;
import com.gwsys.welllog.view.ViewCoreOperator;
/**
 * 
 * @author gaofei
 *
 * the Dialog when create a new Track(AxisCurveContainerView)
 */
public class CreateTrackDialog extends JDialog implements ActionListener
  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MainFrameView parent = null;		//parent component,in this case is MainFrameView
	private InternalFrameView selectedInternalFrameView = null;	//selected InternalFrameView
	private JPanel jContentPanel = null;
	
	/////////the components of this Dialog/////////
	JLabel lblWidth = null;		
	JTextField tfWidth = null;
	JLabel lblHeight = null;
	JTextField tfHeight = null;
	JLabel lblCurveType = null;
	JComboBox cbCurveType = null;
	JLabel lblFillType = null;
	JComboBox cbFillType = null;
	JButton btnOk = null;
	JButton btnCancel = null;
	///////////////////////////////////////////////
	
	
	/**
	 * 
	 * @param owner	the parent of this CreateTrackDialog,in this case is MainFrameView
	 * @param title	title of this CreateTrackDialog
	 * 
	 */
	public CreateTrackDialog(String title, MainFrameView owner) {
		super(owner.getFrame(), title);
		parent = owner;
		selectedInternalFrameView = parent.getSonJSplitPane().getRightDesktopPane().getSelectedInternalFrameView();
		
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,255,170);
		//this.pack();
		this.setResizable(false);
		this.setModal(true);
		
	}

	/////////////initialize inner component////////////////////
	public JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);
			
			lblWidth = new JLabel("Width:");
			lblWidth.setBounds(10,10,75,25);
			tfWidth = new JTextField(ConstantVariableList.AXISCURVECONTAINER_DEFAULT_WIDTH+"");		//default width is 350
			tfWidth.setBounds(85,10,150,25);

			lblHeight = new JLabel("Height:");
			lblHeight.setBounds(10,40,75,25);
			tfHeight = new JTextField(ConstantVariableList.AXISCURVECONTAINER_DEFAULT_HEIGHT+"");	//default height is the last height
			tfHeight.setBounds(85,40,150,25);

			lblCurveType = new JLabel("Curve Type:");
			lblCurveType.setBounds(10,70,75,25);
			cbCurveType = new JComboBox();	//default number is 1
			cbCurveType.addItem(TrackTypeList.REALIZATION_BESIDE);
			cbCurveType.addItem(TrackTypeList.REALIZATION_TOP);
			cbCurveType.setBounds(85,70,150,25);

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("CreateTrackDialog_btnOk");
			btnOk.addActionListener(this);
			btnOk.setBounds(40,110,80,25);
			
			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("CreateTrackDialog_btnCancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(120,110,80,25);
			
			jContentPanel.add(lblWidth);
			jContentPanel.add(tfWidth);
			jContentPanel.add(lblHeight);
			jContentPanel.add(tfHeight);
			jContentPanel.add(lblCurveType);
			jContentPanel.add(cbCurveType);
			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);
			
			jContentPanel.setVisible(true);
		}		
		return jContentPanel;
	}
	//////////////////////////////////////////////////////////////
	
	public void actionPerformed(ActionEvent e) {
		
		
		
		/////////////when button ok is clicked/////////////////
		if(e.getActionCommand().equals("CreateTrackDialog_btnOk")){
			
			/////////need add check code here////////
			/*
			 *check  tfWidth.getText() is number value
			 */
			/////////////////////////////////////////
			
			/*
			 * add AxisCurveContainerView, if necessary , could pick it up to a single file
			 * put the following  into TrackContainerView class��as a method ��addNewAxisCurverContainer()��to call
			 * when create an AxisCurveContainerView, call it
			 * if this as an new method��TrackContainerView should add newAxisCurveContainerWidth and newAxisCurveContainerHeight
			 * To get AxisCurveContainerView'width and height from Dialog
			 */
			NumberFormat numberFormat = NumberFormat.getIntegerInstance();
			numberFormat.setParseIntegerOnly(true);
			int trackWidth = 0;
			try {
				trackWidth = numberFormat.parse(tfWidth.getText().trim()).intValue();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}//Integer.parseInt(tfWidth.getText().trim());			
			int trackHeight = 0;
			try {
				trackHeight = numberFormat.parse(tfHeight.getText().trim()).intValue();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}//Integer.parseInt(tfHeight.getText().trim());			
			String curveType = cbCurveType.getSelectedItem().toString();
	//		String fillType = cbFillType.getSelectedItem().toString();
			//int axisCurveContainerHeight = selectedInternalFrameView.getTrackContainer().getAxisCurveContainerMaxHeight();
			
			if(selectedInternalFrameView == null){
				this.setVisible(false);
				this.dispose();
				return;
			}
				
			
			//////////keep width max than default width value///////////
			//canceled by luming ,11.25
//			if(curveViewWidth < curveViewDefaultWidth){
//				curveViewWidth = curveViewDefaultWidth;
//			}
//			
//			//////////	keep height is the max one	////////////////////
//			if(curveViewHeight < curveViewDefaultHeight){	//keep height max than default height value
//				curveViewHeight = curveViewDefaultHeight;
//			}
			
			//selectedInternalFrameView.getTrackContainer().setCurveViewMaxHeight(curveViewHeight);
			if(trackWidth < ConstantVariableList.AXISCURVECONTAINER_DEFAULT_MINWIDTH)
			{
				trackWidth = ConstantVariableList.AXISCURVECONTAINER_DEFAULT_MINWIDTH;
			}
			
			
			if(trackHeight < ConstantVariableList.AXISCURVECONTAINER_DEFAULT_MINHEIGHT){				
				trackHeight = ConstantVariableList.AXISCURVECONTAINER_DEFAULT_MINHEIGHT;
			}
			///////////////////////////////////////////////////////////////////////
			String trackID = LogWinUtility.generateNewLogName();
			ViewCoreOperator.createTrack(trackID,trackID, trackID,
					trackWidth,  trackHeight, curveType,false);				
			
			
			this.setVisible(false);
			this.dispose();
			return;
		}
		
		/////////when button cancel is clicked//////////
		if(e.getActionCommand().equals("CreateTrackDialog_btnCancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}		
	}
}
