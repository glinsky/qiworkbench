/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;

/**
 * A dialog performs the log section editing functions.
 *
 * @author Team
 *
 */
public class EditFrameDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;

	private InternalFrameView selectedInternalFrameView = null;

	private JPanel jContentPane = null;

	private JPanel jpanelWindow = null;

	JTextField tfWindow = null;

	private JPanel jpanelDepth = null;

	JTextField tfDepth = null;

	JLabel lblWindowName = null;

	JTextField tfWindowName = null;

	JCheckBox jcbMeasurement = null;

	JComboBox cbMeasurement = null;

	JCheckBox jcbDepthscale = null;

	JComboBox cbDepthscale = null;

	JButton btnOk = null;

	JButton btnCancel = null;

	String oldWindowName = "";

	public EditFrameDialog() {
		super(DataBuffer.getJMainFrame().getFrame());
		selectedInternalFrameView = DataBuffer.getJMainFrame().getMainBuffer()
				.getInternalFrameView();
		if (selectedInternalFrameView == null) {
			return;
		}

		initialize();
		this.setBounds(150, 100, 285, 280);
		// this.pack();
		this.setResizable(false);
		this.setModal(true);
	}

	private void initialize() {
		this
				.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		this.setTitle("Change Log Section Property ("
				+ selectedInternalFrameView.getTitle() + ")");
		this.setResizable(true);
		this.setContentPane(getJContentPane());
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);

			tfWindow = new JTextField();
			tfWindow.setBackground(SystemColor.control);
			tfWindow.setEnabled(false);
			tfWindow.setBorder(null);
			tfWindow.setDisabledTextColor(Color.black);
			tfWindow.setText(" Window Property");
			tfWindow.setBounds(new Rectangle(20, 10, 100, 16));

			jpanelWindow = new JPanel();
			jpanelWindow.setLayout(null);
			jpanelWindow.setBorder(BorderFactory.createEtchedBorder());
			jpanelWindow.setBounds(10, 17, 260, 60);

			lblWindowName = new JLabel("Name:");
			lblWindowName.setBounds(10, 20, 35, 25);
			oldWindowName = selectedInternalFrameView.getTitle();
			tfWindowName = new JTextField(oldWindowName);
			tfWindowName.setBounds(45, 20, 150, 25);

			jpanelWindow.add(lblWindowName);
			jpanelWindow.add(tfWindowName);

			tfDepth = new JTextField();
			tfDepth.setBackground(SystemColor.control);
			tfDepth.setEnabled(false);
			tfDepth.setBorder(null);
			tfDepth.setDisabledTextColor(Color.black);
			tfDepth.setText(" Depth Property");
			tfDepth.setBounds(new Rectangle(20, 90, 100, 16));

			jpanelDepth = new JPanel();
			jpanelDepth.setLayout(null);
			jpanelDepth.setBorder(BorderFactory.createEtchedBorder());
			jpanelDepth.setBounds(10, 97, 260, 90);

			jcbMeasurement = new JCheckBox("Vertical Measurement:");
			jcbMeasurement.setBounds(10, 20, 160, 25);
			jcbMeasurement.setSelected(selectedInternalFrameView
					.isSetVerticalMeasurement());
			cbMeasurement = new JComboBox();
			cbMeasurement.addItem(ConstantVariableList.MEASUREMENT_MD);
			cbMeasurement.addItem(ConstantVariableList.MEASUREMENT_TVD);
			cbMeasurement.addItem(ConstantVariableList.MEASUREMENT_TVDSS);
			if (selectedInternalFrameView.getVerticalMeasurement().equals(
					ConstantVariableList.MEASUREMENT_MD)) {
				cbMeasurement.setSelectedIndex(0);
			} else if (selectedInternalFrameView.getVerticalMeasurement()
					.equals(ConstantVariableList.MEASUREMENT_TVD)) {
				cbMeasurement.setSelectedIndex(1);
			} else if (selectedInternalFrameView.getVerticalMeasurement()
					.equals(ConstantVariableList.MEASUREMENT_TVDSS)) {
				cbMeasurement.setSelectedIndex(2);
			}
			cbMeasurement.setBounds(170, 20, 80, 25);
			jcbDepthscale = new JCheckBox("Depth scale:        1:");
			jcbDepthscale.setBounds(10, 50, 160, 25);
			jcbDepthscale.setSelected(selectedInternalFrameView.isVerticalScaleSet());
			String[] depthScale = { "50", "200", "500", "1000", "2000", "5000",
					"10000" };
			cbDepthscale = new JComboBox(depthScale);
			if (selectedInternalFrameView.getDepthScale() ==
				ConstantVariableList.DEPTHSCALE_50) {
				cbDepthscale.setSelectedIndex(0);
			} else if (selectedInternalFrameView.getDepthScale() ==
				ConstantVariableList.DEPTHSCALE_200) {
				cbDepthscale.setSelectedIndex(1);
			} else if (selectedInternalFrameView.getDepthScale() ==
				ConstantVariableList.DEPTHSCALE_500) {
				cbDepthscale.setSelectedIndex(2);
			} else if (selectedInternalFrameView.getDepthScale() ==
				ConstantVariableList.DEPTHSCALE_1000) {
				cbDepthscale.setSelectedIndex(3);
			} else if (selectedInternalFrameView.getDepthScale() ==
				ConstantVariableList.DEPTHSCALE_2000) {
				cbDepthscale.setSelectedIndex(4);
			} else if (selectedInternalFrameView.getDepthScale() ==
				ConstantVariableList.DEPTHSCALE_5000) {
				cbDepthscale.setSelectedIndex(5);
			} else if (selectedInternalFrameView.getDepthScale() ==
				ConstantVariableList.DEPTHSCALE_10000) {
				cbDepthscale.setSelectedIndex(6);
			}
			cbDepthscale.setBounds(170, 50, 80, 25);

			jpanelDepth.add(jcbMeasurement);
			jpanelDepth.add(cbMeasurement);
			jpanelDepth.add(jcbDepthscale);
			jpanelDepth.add(cbDepthscale);

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("WindowPropertyDialog_btnOk");
			btnOk.addActionListener(this);
			btnOk.setBounds(65, 210, 80, 25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("WindowPropertyDialog_btnCancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(160, 210, 80, 25);

			jContentPane.add(tfWindow);
			jContentPane.add(jpanelWindow);
			jContentPane.add(tfDepth);
			jContentPane.add(jpanelDepth);
			jContentPane.add(btnOk);
			jContentPane.add(btnCancel);
		}
		return jContentPane;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("WindowPropertyDialog_btnOk")) {
			String newWindowName = tfWindowName.getText().trim();
			if (newWindowName == "") {
				JOptionPane.showMessageDialog(this,
						"WellLog Name can not be empty!", "Message",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			if (!newWindowName.equals(oldWindowName)) {
				boolean isNotExistName = DataBuffer.getJMainFrame()
						.getMainBuffer().getViewTree().renameFrame(
								oldWindowName, newWindowName);
				if (!isNotExistName) {
					JOptionPane.showMessageDialog(this,
									"WellLog Name \""
											+ newWindowName
											+ "\" has already exist.Please choose another name!",
									"Message", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
			}

			if (cbMeasurement.getSelectedIndex() != 0) {
				if (DataBuffer.getJMainFrame().getMainBuffer()
						.getMDDepthConverter() == null) {
					boolean isSelectSurveyFile = false;
					SetDepthTypeDialog setDepthTypeDialog = new SetDepthTypeDialog();
					setDepthTypeDialog.setVisible(true);
					isSelectSurveyFile = setDepthTypeDialog
							.isSelectSurveyFile();
					if (!isSelectSurveyFile) {
						return;
					}
				}
			}

			selectedInternalFrameView.setTitle(tfWindowName.getText().trim());
			selectedInternalFrameView.enableVerticalMeasurement(jcbMeasurement
					.isSelected());
			if (jcbMeasurement.isSelected()) {
				if (cbMeasurement.getSelectedIndex() == 0)
					selectedInternalFrameView
							.setVerticalMeasurement(ConstantVariableList.MEASUREMENT_MD);
				else if (cbMeasurement.getSelectedIndex() == 1)
					selectedInternalFrameView
							.setVerticalMeasurement(ConstantVariableList.MEASUREMENT_TVD);
				else if (cbMeasurement.getSelectedIndex() == 2)
					selectedInternalFrameView
							.setVerticalMeasurement(ConstantVariableList.MEASUREMENT_TVDSS);
				selectedInternalFrameView.setTrackType();
			}

			selectedInternalFrameView.enableVerticalScale(jcbDepthscale.isSelected());
			if (jcbDepthscale.isSelected()) {
				ViewCoreOperator.updateMeasurementScale(cbDepthscale.getSelectedIndex(),
						selectedInternalFrameView);
			}

			this.setVisible(false);
			this.dispose();
			return;
		}

		if (e.getActionCommand().equals("WindowPropertyDialog_btnCancel")) {
			this.setVisible(false);
			this.dispose();
			return;
		}
	}

}
