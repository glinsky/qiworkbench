/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class DescriptionAccessory extends JPanel{

	private static final long serialVersionUID = 1L;

	private String description = "";

	private JTextArea taDescription = new JTextArea(15,10);
	
	private JScrollPane scroll = new JScrollPane(taDescription , ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS , 
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

	public DescriptionAccessory() {
		taDescription.setBackground(new Color(236,233,216));
		taDescription.setEditable(false);
		taDescription.setText("haha");
		this.add(scroll , BorderLayout.SOUTH);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}