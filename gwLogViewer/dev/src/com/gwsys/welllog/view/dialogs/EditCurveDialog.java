/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;

/**
 * A dialog can edit the properties of curve.
 * @author Team
 *
 */
public class EditCurveDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPanel = null;
	private JLabel lblName = null;
	private JLabel lblCurve = null;
	private JLabel lblCurveColor = null;
	private JButton btnShowCurveColor = null;
	private JLabel lblStroke = null;
	private JTextField tfStroke = null;
	private CurveView cv = null;
	private JButton btnOk = null;
	private JButton btnCancel = null;
	private CurvePanel cp = null;
	private JCheckBox jcb = null;
	private JLabel lblType = null;
	private JComboBox cbType = null;
	private JCheckBox jcbDisplayOrder = null;
	private JLabel lblCurvePanelWidth = null;
	private JTextField tfCurvePanelWidth = null;
	private JLabel lblDisplayMix = null;
	private JLabel lblDisplayMax = null;
	private JTextField tfDisplayMix = null;
	private JTextField tfDisplayMax = null;

	private JPanel jpanelDisplay = null;
	JTextField tfDisplay = null;
	private JPanel jpanelLine = null;
	JTextField tfLine = null;

//	added by Alex
	private JCheckBox jcbHoldProperties = null;

	public EditCurveDialog(CurveView cv, MainFrameView view)
	{
		super(view.getFrame());
		this.cv = cv;
		this.setTitle("Settings of Curve ");
		this.setContentPane(getContentPanel());
		this.setBounds(150,100,470,390);
		this.setLocationRelativeTo(view.getFrame());
		this.setResizable(false);
		this.setModal(true);
	}

	public JPanel getContentPanel(){
		if(jContentPanel == null){
			jContentPanel = new JPanel();
			jContentPanel.setLayout(null);

			cp = (CurvePanel)cv.getCurveLayer().getCurveSelectedList().get(0);

			// Name
			lblName = new JLabel("Name:");
			lblName.setBounds(10,10,50,25);
			lblCurve = new JLabel( cp.getCd().getCurveName());
			lblCurve.setBounds(60,10,100,25);

			// Hold Curve properties
			jcbHoldProperties = new JCheckBox("Enable Global Settings");
			jcbHoldProperties.setSelected(false);
			jcbHoldProperties.setBounds(300,10,135,25);

			// Display
			tfDisplay = new JTextField();
			tfDisplay.setBackground(SystemColor.control);
			tfDisplay.setEnabled(false);
			tfDisplay.setBorder(null);
			tfDisplay.setDisabledTextColor(Color.black);
			tfDisplay.setText(" Display Property");
			tfDisplay.setBounds(new Rectangle(20, 35, 110, 16));

			jpanelDisplay = new JPanel();
			jpanelDisplay.setLayout(null);
			jpanelDisplay.setBorder(BorderFactory.createEtchedBorder());
			jpanelDisplay.setBounds(10, 42, 440, 150);

			// Line
			tfLine = new JTextField();
			tfLine.setBackground(SystemColor.control);
			tfLine.setEnabled(false);
			tfLine.setBorder(null);
			tfLine.setDisabledTextColor(Color.black);
			tfLine.setText(" Line Property");
			tfLine.setBounds(new Rectangle(20, 203, 100, 16));

			jpanelLine = new JPanel();
			jpanelLine.setLayout(null);
			jpanelLine.setBorder(BorderFactory.createEtchedBorder());
			jpanelLine.setBounds(10, 210, 440, 110);

			lblCurveColor = new JLabel("Curve Color:");
			lblCurveColor.setBounds(10,24,80,25);
			btnShowCurveColor = new JButton("Change");
			btnShowCurveColor.setIcon(createColorIcon(cp.getCurveColor()));
			btnShowCurveColor.setForeground(cp.getCurveColor());
			btnShowCurveColor.setActionCommand("showCurveColorChooser");
			btnShowCurveColor.addActionListener(this);
			btnShowCurveColor.setBounds(90,24,100,25);

			lblType = new JLabel("Stroke Type:");
			lblType.setBounds(10,67,80,25);
			String choise[] = new String[5];
			for (int i=0; i<5; i++) {
				choise[0] = Integer.toString(ComboBoxRender.SOLID);
				choise[1] = Integer.toString(ComboBoxRender.DOT);
				choise[2] = Integer.toString(ComboBoxRender.DASH);
				choise[3] = Integer.toString(ComboBoxRender.DASH_DOT);
				choise[4] = Integer.toString(ComboBoxRender.DASH_DOT_DOT);
			}
			cbType = new JComboBox(choise);
			ComboBoxRender renderer= new ComboBoxRender();
	        renderer.setPreferredSize(new Dimension(100, 30));
	        cbType.setRenderer(renderer);
			cbType.setBounds(90,67,150,25);
			if(cp.getStokeType() == ComboBoxRender.SOLID )
				cbType.setSelectedIndex(0);
			else if(cp.getStokeType() == ComboBoxRender.DASH)
				cbType.setSelectedIndex(1);
			else if(cp.getStokeType() == ComboBoxRender.DASH_DOT)
				cbType.setSelectedIndex(2);
			else if(cp.getStokeType() == ComboBoxRender.DOT)
				cbType.setSelectedIndex(3);
			else if(cp.getStokeType() == ComboBoxRender.DASH_DOT_DOT)
				cbType.setSelectedIndex(4);

			lblStroke = new JLabel("Stroke Width:");
			lblStroke.setBounds(250,24,95,25);
			tfStroke = new JTextField(Float.toString(cp.getStrokeWidth()));
			tfStroke.setBounds(350,24,70,25);

			lblCurvePanelWidth = new JLabel("Curve Panel Width:");
			lblCurvePanelWidth.setBounds(10,24,115,25);
			tfCurvePanelWidth = new JTextField(cp.getBesideTypeWidth() + "" , 20);
			tfCurvePanelWidth.setBounds(125,24,115,25);

			lblDisplayMix = new JLabel("Min display value:");
			lblDisplayMix.setBounds(10,55,110,25);
			tfDisplayMix = new JTextField(Float.toString(cp.getMinXDisplay()));
			tfDisplayMix.setBounds(125,55,80,25);
			lblDisplayMax = new JLabel("Max display value:");
			lblDisplayMax.setBounds(10,86,110,25);
			tfDisplayMax = new JTextField(Float.toString(cp.getMaxXDisplay()));
			tfDisplayMax.setBounds(125,86,80,25);


			jcbDisplayOrder = new JCheckBox("Reverse");
			jcbDisplayOrder.setSelected(cp.isOrderDesc());
			jcbDisplayOrder.setBounds(10,115,200,25);

			jcb = new JCheckBox("Visible");
			jcb.setSelected(cp.isVisible());
			jcb.setBounds(250,115,100,25);

			btnOk = new JButton(" OK ");
			btnOk.setActionCommand("ok");
			btnOk.addActionListener(this);
			btnOk.setBounds(155,330,80,25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(235,330,80,25);

			// add component
			jpanelDisplay.add(lblCurvePanelWidth);
			jpanelDisplay.add(tfCurvePanelWidth);
			jpanelDisplay.add(lblDisplayMix);
			jpanelDisplay.add(tfDisplayMix);
			jpanelDisplay.add(lblDisplayMax);
			jpanelDisplay.add(tfDisplayMax);
			jpanelDisplay.add(jcbDisplayOrder);
			jpanelDisplay.add(jcb);

			jpanelLine.add(lblCurveColor);
			jpanelLine.add(btnShowCurveColor);
			jpanelLine.add(lblType);
			jpanelLine.add(cbType);
			jpanelLine.add(lblStroke);
			jpanelLine.add(tfStroke);

			jContentPanel.add(lblName);
			jContentPanel.add(lblCurve);
			jContentPanel.add(jcbHoldProperties);	//added by Alex

			jContentPanel.add(tfDisplay);
			jContentPanel.add(jpanelDisplay);

			jContentPanel.add(tfLine);
			jContentPanel.add(jpanelLine);

			jContentPanel.add(btnOk);
			jContentPanel.add(btnCancel);

			jContentPanel.setVisible(true);
			}
		return jContentPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok"))
		{
			float curvewidth = 1;
			try {
				curvewidth = Float.parseFloat(tfStroke.getText().trim());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			cp.setStrokeWidth(curvewidth);

			if(cbType.getSelectedIndex() == 0)
				cp.setStokeType(ComboBoxRender.SOLID);
			else if(cbType.getSelectedIndex() == 1)
				cp.setStokeType(ComboBoxRender.DASH);
			else if(cbType.getSelectedIndex() == 2)
				cp.setStokeType(ComboBoxRender.DASH_DOT);
			else if(cbType.getSelectedIndex() == 3)
				cp.setStokeType(ComboBoxRender.DOT);
			else if(cbType.getSelectedIndex() == 4)
				cp.setStokeType(ComboBoxRender.DASH_DOT_DOT);

			cp.setCurveColor(this.btnShowCurveColor.getForeground());
			cp.setMinXDisplay(Float.parseFloat(this.tfDisplayMix.getText().trim()));
			cp.setMaxXDisplay(Float.parseFloat(this.tfDisplayMax.getText().trim()));
			cp.setVisible(this.jcb.isSelected());
			cp.setOrderDesc(this.jcbDisplayOrder.isSelected());

			int curvePanelWidth = Integer.parseInt(tfCurvePanelWidth.getText().trim());
			cp.setBesideTypeWidth(curvePanelWidth);

			//	hold global curve settings
			if (jcbHoldProperties.isSelected()){
				ViewCoreOperator.addCurveSettings(cp);
				//update all existing curves
				ViewCoreOperator.applyGlobalSettings();
			}

			this.setVisible(false);
			this.dispose();
			cv.setFillRedrawFlag(true);
			cv.repaintFather();
			return;
		}

		/////////when button cancel is clicked//////////
		else if(e.getActionCommand().equals("cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}
		else if(e.getActionCommand().equals("showCurveColorChooser")){
			/*
			ColorChooserDialog colorChooserDialog = new ColorChooserDialog(this.btnShowCurveColor);
			colorChooserDialog.setVisible(true);
			return;*/
			Color selected=JColorChooser.showDialog(this,
					"Choose Curve Color", cp.getCurveColor());
			if (selected!=null){
				btnShowCurveColor.setIcon(createColorIcon(selected));
				btnShowCurveColor.setForeground(selected);
			}
		}

	}

	public void remove(){
		if(cp != null){
			ViewCoreOperator.deleteCurve(cp.getCd().getHashCurveName(), cv.getParentAxisCurveContainer().getWellName());
		}
		this.setVisible(false);
		this.dispose();
	}

	public static Icon createColorIcon(Color color){
		BufferedImage image=new BufferedImage(10,10, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		g.setColor(color);
		g.fillRect(0,0,10,10);
		return new ImageIcon(image);
	}
}
