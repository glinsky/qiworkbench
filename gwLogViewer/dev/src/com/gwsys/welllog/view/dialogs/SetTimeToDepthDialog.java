/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.projecttree.ProjectTreeView;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.yaxis.DepthConverter;
import com.gwsys.welllog.view.yaxis.DataConverterFactory;
import com.gwsys.welllog.view.yaxis.DepthToTimeConverter;
;

/**
 * A dialog reads check-shot file.
 * @author Team.
 *
 */
public class SetTimeToDepthDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private AxisCurveContainerView axisCurveContainerView = null;

	String newCurveHashmapKey = null;

	private JPanel jContentPane = null;

	private JLabel jFileLabel = null;

	private JTextField jFileTextField = null;

	private JButton jBrowseButton = null;

	private JButton jOKButton = null;

	private JButton jCancelButton = null;

	private boolean selectCheckShotFile = false;

	private boolean time2depth = true;

	/**
	 * COnstructor.
	 * @param view
	 * @param newCurveHashmapKey
	 * @throws HeadlessException
	 */
	public SetTimeToDepthDialog(AxisCurveContainerView view,
			String newCurveHashmapKey, boolean time2depth)
		throws HeadlessException {
		super(DataBuffer.getJMainFrame().getFrame());

		axisCurveContainerView = view;
		this.newCurveHashmapKey = newCurveHashmapKey;
		initialize();
		this.setModal(true);
		this.time2depth = time2depth;
	}

	/**
	 * This method initializes jFileTextField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJFileTextField() {
		if (jFileTextField == null) {
			jFileTextField = new JTextField();
			jFileTextField.setBounds(5,30,195,25);

		}
		return jFileTextField;
	}

	/**
	 * This method initializes jFileBrowseButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJBrowseButton() {
		if (jBrowseButton == null) {
			jBrowseButton = new JButton();
			jBrowseButton.setText("Browse");
			jBrowseButton.setBounds(200,30,80,25);
			jBrowseButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ChooserDialog chooser = new ChooserDialog(DataBuffer.getJMainFrame(),ChooserDialog.getPath());
					chooser.clearFilter();
					chooser.setFilter("txt");
					chooser.setFilter("asc");
					chooser.setMultiSelect(false);
					chooser.showDialog();

					DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getJMainFrame().setFileChooser(chooser.getJfc());

					if(!chooser.isOpen()){
						return;
					}

					File file = chooser.getFile();
					ChooserDialog.setPath(file.getPath());

					jFileTextField.setText(file.getPath());
				}
			});
		}
		return jBrowseButton;
	}

	/**
	 * This method initializes jOKButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJOKButton() {
		if (jOKButton == null) {
			jOKButton = new JButton();
			jOKButton.setText("OK");
			jOKButton.setBounds(120,70,80,25);
			jOKButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					handleOKAction();
				}
			});
		}
		return jOKButton;
	}

	/**
	 * This method initializes jCancelButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJCancelButton() {
		if (jCancelButton == null) {
			jCancelButton = new JButton();
			jCancelButton.setText("Cancel");
			jCancelButton.setBounds(200,70,80,25);
			jCancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					selectCheckShotFile = false;
					SetTimeToDepthDialog.this.dispose();
				}
			});
		}
		return jCancelButton;
	}

	/**
	 * This is the default constructor
	 */
	public SetTimeToDepthDialog() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Time to depth");
		this.setContentPane(getJContentPane());
		this.setBounds(150,100,290,125);
		this.setResizable(false);
		this.setModal(true);
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);

			jFileLabel = new JLabel();
			jFileLabel.setText("Selected CheckShot File:");
			jFileLabel.setBounds(5,5,200,20);
			jContentPane.add(jFileLabel, null);
			jContentPane.add(getJFileTextField(), null);
			jContentPane.add(getJBrowseButton(), null);

			//jContentPane.add(getJEmptyPanel(), null);
			jContentPane.add(getJOKButton(), null);
			jContentPane.add(getJCancelButton(), null);
			//jContentPane.add(getJEmptyPanel(), null);

		}
		return jContentPane;
	}

	public boolean isSelectCheckShotFile() {
		return selectCheckShotFile;
	}

	@SuppressWarnings("unchecked")
	private void handleOKAction(){
		if (jFileTextField.getText() == null || jFileTextField.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please select checkshot file!");
			return;
		}

		File file = new File(jFileTextField.getText());

		String fileName = file.getName();
		int nLen = fileName.lastIndexOf(".");
		String checkshotHashKey = fileName.substring(0,nLen);

		ChooserDialog.setPath(file.getPath());

		axisCurveContainerView.setCheckShotFileName(checkshotHashKey);

		if(newCurveHashmapKey != null) {
			ViewCoreOperator.putCurveToTrack(newCurveHashmapKey, axisCurveContainerView.getWellName());
		}

		selectCheckShotFile = true;
		if(time2depth){
			DepthConverter converter = (DepthConverter)(DataBuffer.getJMainFrame().
					getMainBuffer().getCheckshotHashMap().get(checkshotHashKey));
			if (converter == null) {
				converter = DataConverterFactory.createInstance(file.getPath(),TrackTypeList.TIME_TO_MD);
				if (converter == null) {
					// alert something
					JOptionPane.showMessageDialog(null, "Cann't parse the selected checkshot file!");
					return;
				}

				DataBuffer.getJMainFrame().getMainBuffer().getCheckshotHashMap().put(checkshotHashKey,converter);
				DataBuffer.getJMainFrame().getMainBuffer().getAllCheckShots().add(file.getPath());

				ProjectTreeView ltv = (ProjectTreeView) DataBuffer.getJMainFrame().getMainBuffer().getProjectTree();
				ltv.addCheckShotNode(checkshotHashKey, file);
				ltv.setRootVisible(true);
			}

			axisCurveContainerView.setTimeToDepthConverter(converter);
			axisCurveContainerView.getYAxisView().setYType(TrackTypeList.Y_DEPTH_MD);
		}else{
			DepthToTimeConverter converter = DataConverterFactory.createInstance(
					file.getPath());
			if (converter == null) {
				// alert something
				JOptionPane.showMessageDialog(null, "Cann't parse the selected checkshot file!");
				return;
			}

			DataBuffer.getJMainFrame().getMainBuffer().getCheckshotHashMap().put(checkshotHashKey,converter);
			DataBuffer.getJMainFrame().getMainBuffer().getAllCheckShots().add(file.getPath());

			ProjectTreeView ltv = (ProjectTreeView) DataBuffer.getJMainFrame().getMainBuffer().getProjectTree();
			ltv.addCheckShotNode(checkshotHashKey, file);
			ltv.setRootVisible(true);
			axisCurveContainerView.setDepthToTimeConverter(converter);
			axisCurveContainerView.getYAxisView().setYType(TrackTypeList.Y_TIME);
		}
		axisCurveContainerView.getCurveView().setCurveRedrawFlag(true);
		axisCurveContainerView.getCurveView().setMarkerRedrawFlag(true);
		axisCurveContainerView.getCurveView().setFillRedrawFlag(true);
		axisCurveContainerView.repaintAll();

		SetTimeToDepthDialog.this.dispose();
	}
}
