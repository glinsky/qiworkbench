/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;

import com.gwsys.data.well.CheckShotFileFilter;
import com.gwsys.data.well.DataFileFilter;
import com.gwsys.data.well.LasFileFilter;
import com.gwsys.data.well.MarkerFileFilter;
import com.gwsys.data.well.SegyFileFilter;
import com.gwsys.data.well.SuFileFilter;
import com.gwsys.data.well.SurveyFileFilter;
import com.gwsys.data.well.XmlFileFilter;
import com.gwsys.welllog.mainframe.MainFrameView;

/**
 * Customized dialog to choose data sources.
 * @author Team
 *
 */
public class ChooserDialog extends JDialog {
	private static final long serialVersionUID = 1L;

	private JFileChooser jfc;

	private JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT,
			10, 10));

	private JLabel label = new JLabel("File description:");

	private JTextArea textArea = new JTextArea(10, 10);

	private JScrollPane textAreaSp = new JScrollPane(textArea);

	private JPanel prevPanel = new JPanel(new BorderLayout());

	private boolean open = false;

	@SuppressWarnings("unused")
	private String filterType = new String("");

	private static String path = "";

	//added by Alex
	public static final int DATA_SOURCE_LAS = 0;
	public static final int DATA_SOURCE_XML = 1;
	public static final int DATA_SOURCE_ASCII = 2;
	public static final int DATA_SOURCE_SU = 3;
	public static final int DATA_SOURCE_SEGY = 4;
	public static final int DATA_SOURCE_TOPS = 5;
	public static final int DATA_SOURCE_CHECKSHOT = 6;
	public static final int DATA_SOURCE_WELLSURVEY = 7;

	/**
	 * Constructor.
	 * @param owner
	 * @param apath
	 */
	public ChooserDialog(MainFrameView owner, String apath) {
		super(owner.getFrame(), "Open", true);
		path = apath;
		jfc = new JFileChooser(path) {
			private static final long serialVersionUID = 1L;

			public void approveSelection() {
				File[] files = getSelectedFiles();
				if (files != null) {
					open = true;
					if (files.length > 0)
						path = files[0].getAbsolutePath();
					dispose();
				} else {
					super.approveSelection();
				}
			}

			public void cancelSelection() {
				open = false;
				dispose();
			}
		};
		jfc.setMultiSelectionEnabled(true);
		jfc.setControlButtonsAreShown(true);
		jfc.setApproveButtonToolTipText("Open");

		textArea.setEditable(false);
		textArea.setBackground(label.getBackground());

		prevPanel.add(label, BorderLayout.NORTH);
		prevPanel.add(textAreaSp, BorderLayout.SOUTH);
		prevPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));

		getContentPane().add(jfc, BorderLayout.NORTH);
		getContentPane().add(buttonPanel, BorderLayout.CENTER);
		getContentPane().add(prevPanel, BorderLayout.SOUTH);

		pack();
		setLocationRelativeTo(null);
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jfc.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				String prop = e.getPropertyName();
				if (prop == JFileChooser.FILE_FILTER_CHANGED_PROPERTY ){ //SELECTED_FILE_CHANGED_PROPERTY) {
					FileFilter file = (FileFilter) e.getNewValue();
					if (file == null) {
						textArea.setText("");
					} else {
				        textArea.setText(file.toString());
					}
				}
			}
		});
	}

	public boolean isOpen() {
		return open;
	}

	public File[] getFiles() {
		if (open)
			return jfc.getSelectedFiles();
		else
			return null;
	}

	public File getFile() {
		if (open)
			return jfc.getSelectedFile();
		else
			return null;
	}

	public void clearFilter() {
		FileFilter[] fileFilter = (FileFilter[]) (jfc.getChoosableFileFilters());
		for (int i = 0; i < fileFilter.length; i++) {
			jfc.removeChoosableFileFilter(fileFilter[i]);
		}
	}

	public void setFilter(String filter) {
		filterType = filter;
		if (filter.equals("las"))
			jfc.addChoosableFileFilter(new LasFileFilter(filter));
		else if (filter.equals("xml"))
			jfc.addChoosableFileFilter(new XmlFileFilter(filter));
		else if (filter.equals("dat"))
			jfc.addChoosableFileFilter(new DataFileFilter(filter));
		else if (filter.equals("su"))
			jfc.addChoosableFileFilter(new SuFileFilter(filter));
		else if (filter.equals("sgy"))
			jfc.addChoosableFileFilter(new SegyFileFilter(filter));
		else if (filter.equals("marker"))
			jfc.addChoosableFileFilter(new MarkerFileFilter(filter));
		else if (filter.equals("txt"))
			jfc.addChoosableFileFilter(new CheckShotFileFilter(filter));
		else if (filter.equals("asc"))
			jfc.addChoosableFileFilter(new CheckShotFileFilter(filter));
		else if (filter.equals("xls"))
			jfc.addChoosableFileFilter(new SurveyFileFilter(filter));
	}

	/**
	 * added by Alex
	 * wrapper to setFilter(String filter) to set filter from constants data type
	 * @param dataType
	 */
	public void setFilter(int dataType)	{
		if (dataType == DATA_SOURCE_LAS)	{
			jfc.addChoosableFileFilter(new LasFileFilter("las"));
		}
		else if (dataType == DATA_SOURCE_XML)	{
			jfc.addChoosableFileFilter(new XmlFileFilter("xml"));
		}
		else if (dataType == DATA_SOURCE_ASCII)	{
			jfc.addChoosableFileFilter(new DataFileFilter("dat"));
		}
		else if (dataType == DATA_SOURCE_SU)	{
			jfc.addChoosableFileFilter(new SuFileFilter("su"));
		}
		else if (dataType == DATA_SOURCE_SEGY)	{
			jfc.addChoosableFileFilter(new SegyFileFilter("segy"));
		}
		else if (dataType == DATA_SOURCE_TOPS)	{
			jfc.addChoosableFileFilter(new MarkerFileFilter("marker"));
		}
		else if (dataType == DATA_SOURCE_CHECKSHOT)	{
			jfc.addChoosableFileFilter(new CheckShotFileFilter("txt"));
			jfc.addChoosableFileFilter(new CheckShotFileFilter("asc"));
		}
		else if (dataType == DATA_SOURCE_WELLSURVEY)	{
			jfc.addChoosableFileFilter(new SurveyFileFilter("xls"));
			jfc.addChoosableFileFilter(new SurveyFileFilter("txt"));
		}
	}

	public JFileChooser getJfc() {
		return jfc;
	}

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		ChooserDialog.path = path;
	}

	public void showDialog() {
		jfc.setSelectedFile(new File(""));
		setVisible(true);
	}

	public void setMultiSelect(boolean multiSelect) {
		this.jfc.setMultiSelectionEnabled(multiSelect);
	}
}
