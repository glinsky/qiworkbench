/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.dialogs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.WellInfo;

/**
 * @author Administrator
 *
 */
class MapPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private int mapWidth = 500;
	private int mapHeight = 350;
	private int VerLineCount = 5;
	private int horLineCount = 5;
	
	public static int XAXIS = 0;
	public static int YAXIS = 1;
	
	float minX = 0, minY = 0, maxX = 0, maxY = 0;
	float defminX, defminY, defmaxX, defmaxY;
	private NumberFormat format = NumberFormat.getNumberInstance();
	
	private ArrayList wellList;
	private Graphics2D g2;
	private MapController controller = null;
	
	// Rectangle selection
	private Rectangle currentRect = null;	
	private Boolean doSelection = false;
	public Point startPoint, endPoint;
	
	public MapPanel(){
		this.setBounds(5, 5, 611, 430);
		
		format.setGroupingUsed(false);
		wellList = DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList();
		
		for(int i = 0 ; i < wellList.size() ; i++){
			WellInfo wellInfo = (WellInfo)wellList.get(i);
			if(i == 0){
				minX = wellInfo.getXLocation();
				minY = wellInfo.getYLocation();
				maxX = wellInfo.getXLocation();
				maxY = wellInfo.getYLocation();
			}else{
				if(wellInfo.getXLocation() < minX){
					minX = wellInfo.getXLocation();
				}
				if(wellInfo.getYLocation() < minY){
					minY = wellInfo.getYLocation();
				}
				if(wellInfo.getXLocation() > maxX){
					maxX = wellInfo.getXLocation();
				}
				if(wellInfo.getYLocation() > maxY){
					maxY = wellInfo.getYLocation();
				}
			}
		}
		float width = Math.abs(maxX - minX);
		float height = Math.abs(maxY - minY);
		if (wellList.size() < 2){
			width = 100;
			height = 100;
		}
		
		minX = minX - width/2;
		maxX = maxX + width/2;
		minY = minY - height/2;
		maxY = maxY + height/2;
		//round up the vaule here
		minX = roundUp(minX, false);
		maxX = roundUp(maxX, true);
		minY = roundUp(minY, false);
		maxY = roundUp(maxY, true);
		// Set default values;
		defminX = minX;
		defminY = minY;
		defmaxX = maxX;
		defmaxY = maxY;
		
		// Selection
		currentRect = new Rectangle();
		
		// Mouse Listeners
		controller = new MapController(this);
		this.addMouseListener(controller);
		this.addMouseMotionListener(controller);
	}
	
	public void paintComponent(Graphics g) {	
		super.paintComponent(g);
		g2 = (Graphics2D) g.create();
		
		// Fill box in White color
		g2.setColor(Color.white);
		g2.fillRect(100,15,mapWidth+5,mapHeight+5);
		
		for(int i =0; i<= horLineCount; i++){
			g2.setColor(Color.green);
			g2.drawLine(100,(20+mapHeight)-(mapHeight/horLineCount*(i)),
					100+mapWidth+5,(20+mapHeight)-(mapHeight/horLineCount*(i)));
			g2.setColor(Color.black);
			g2.drawLine(92,(20+mapHeight)-(mapHeight/horLineCount*(i)),
					100,(20+mapHeight)-(mapHeight/horLineCount*(i))); //long tick
			for(int j=1 ; j < 5; j++){
				if (i == horLineCount) break;
				g2.drawLine(97,(20+mapHeight)-(mapHeight/horLineCount*i)-((mapHeight/horLineCount)/5*j),
						100,(20+mapHeight)-(mapHeight/horLineCount*i)-((mapHeight/horLineCount)/5*j));
			}
			
		}
		for(int i =0; i <= VerLineCount; i++){
			g2.setColor(Color.green);
			g2.drawLine(100+mapWidth/VerLineCount*(i),15,100+mapWidth/VerLineCount*(i),25+mapHeight);
			g2.setColor(Color.black);
			g2.drawLine(100+mapWidth/VerLineCount*(i),20+mapHeight,100+mapWidth/VerLineCount*(i),28+mapHeight);
			for(int j=1 ; j < 5; j++){
				g2.drawLine(100+mapWidth/VerLineCount*(i+1)-((mapWidth/VerLineCount)/5*j),20+mapHeight,
						100+mapWidth/VerLineCount*(i+1)-((mapWidth/VerLineCount)/5*j),23+mapHeight);
			}
		}
		
		// Draw a Box in Black color
		g2.setColor(Color.black);
		g2.drawRect(100,15,mapWidth+5,mapHeight+5);
				
		g2.setColor(Color.black);
		String value ="";
		
		float current;
		float step = (maxY - minY)/horLineCount;
		setFractionDigits(step);
		// draw "Y" axis values
		for(int i = 0; i<= horLineCount; i++){
			current = minY+step*i;						
			value = format.format(current);
			g2.drawString(value,88-g2.getFontMetrics().stringWidth(value),
					(24+mapHeight)-(mapHeight/horLineCount*(i)));
		}
		value="";
		step = (maxX - minX)/VerLineCount;
		setFractionDigits(step);
		// draw "X" axis values
		for(int i = 0; i <= VerLineCount; i++){
			current = minX+step*i;			
			value = format.format(current);
			int offset = g2.getFontMetrics().stringWidth(value);
			if (i < VerLineCount)
				offset = offset/2;
			g2.drawString(value,100+mapWidth/VerLineCount*(i)-offset,
					42+mapHeight);
		}
		
		// draw coordenates legend
		g2.drawString("Y(m)",10,10);
		g2.drawString("X(m)",100,60+mapHeight);
		
		// for each well, draw point and well name
		for(int i = 0 ; i < wellList.size() ; i++){
			WellInfo wellInfo = (WellInfo)wellList.get(i);
			float xLoc = wellInfo.getXLocation();
			float yLoc = wellInfo.getYLocation();
			int pointX = (int)(100 +(mapWidth*((xLoc-minX)/(maxX - minX))));			
			int pointY = (int)(20+ mapHeight-(mapHeight*((yLoc-minY)/(maxY - minY))));
			
			if ((xLoc > minX && xLoc < maxX) && (yLoc > minY && yLoc < maxY))	{
				g2.fillRect(pointX-2,pointY-2,4,4);
				g2.drawString(wellInfo.getWellName(),pointX+4,pointY);
			}			
		}
		
		// for rectangle selection
		if (this.showRectangleSelection())	{
			g2.setColor(Color.red); 
			currentRect.setFrameFromDiagonal(startPoint, endPoint);
			g2.draw(currentRect);
		}
	}
	
	/**
	 * added by Alex
	 * Set new values to draw into map
	 * 
	 * @param x1 initial X coordenate
	 * @param x2 final X coordenate
	 * @param y1 initial Y coordenate
	 * @param y2 final Y coordenate
	 */
	public void setWellValuesFromCoordenates(int x1, int x2, int y1, int y2)	{
		int xIni = Math.min(x1, x2)-100;
		int xEnd = Math.max(x1, x2)-100;
		int yIni = Math.min(y1, y2)-15;
		int yEnd = Math.max(y1, y2)-15;
		// Last works good
		float minx, maxx, miny, maxy;
		
		minx = getValueFromPixel(xIni, mapWidth + 5, maxX, minX, MapPanel.XAXIS);
		maxx = getValueFromPixel(xEnd, mapWidth + 5, maxX, minX, MapPanel.XAXIS);				
		miny = getValueFromPixel(yEnd, mapHeight + 5, maxY, minY, MapPanel.YAXIS);
		maxy = getValueFromPixel(yIni, mapHeight + 5, maxY, minY, MapPanel.YAXIS);
				
		setMinMaxValues(minx, maxx, miny, maxy);
		
	}
	
	/**
	 * added by Alex
	 * Convert pixel coordenates into well values
	 * 
	 * @param pxValue graphic coordenate
	 * @param diff range between maximum and minimum
	 * @param maximum bigger value
	 * @param minimum smaller value
	 * @param axis X|Y
	 * @return well value
	 */
	public float getValueFromPixel(int pxValue, int diff, float maximum, float minimum, int axis)	{
		/*System.out.println("\n PX: "+pxValue);
		System.out.println("w/h: "+diff);
		System.out.println("max: "+maximum);
		System.out.println("min: "+minimum);
		System.out.println("Dif: "+(maximum-minimum));*/
		
		float retVal;
		
		if (pxValue == 0)
			return minimum;
		
		retVal = pxValue * (maximum - minimum);
		retVal = retVal / diff;
		
		if (axis == MapPanel.XAXIS)	{
			retVal = minimum + retVal;
		} 
		else if (axis == MapPanel.YAXIS)	{
			retVal = maximum - retVal;
		}

		return retVal;
	}
	
	/**
	 * restore map to default max/min values
	 *
	 */
	public void restoreInitialValues()	{
		setMinMaxValues(defminX, defmaxX, defminY, defmaxY);
	}
	
	/**
	 * set minimum and maximum values
	 * 
	 * @param minx
	 * @param maxx
	 * @param miny
	 * @param maxy
	 */
	public void setMinMaxValues(float minx, float maxx, float miny, float maxy)	{
		minX = roundUp(minx, false);
		minY = roundUp(miny, false);
		maxX = roundUp(maxx, true);
		maxY = roundUp(maxy, true);
	}
	
	/**
	 * sets if paintComponent must draw rectangle selection or no
	 * 
	 * @param value true or false
	 */
	public void setShowRectangleSelection(Boolean value)	{
		this.doSelection = value;
	}
	
	/**
	 * check if must map must show rectangle selection
	 * 
	 * @return true or false
	 */
	public Boolean showRectangleSelection()	{
		return this.doSelection;
	}
	
	/**
	 * set axist points to draw rectangle selection
	 * 
	 * @param x coordenate
	 * @param y coordenate
	 * @param opt start or finish
	 */
	public void setAxisPoints(int x, int y, int opt)	{
		if (opt == 0)
			startPoint = new Point(x, y);
		else if (opt == 1)	{
			// validate coordenates
			if (x < 100) 
				x = 100;
			else if (x > 605)
				x = 605;
			
			if (y < 15) 
				y = 15;
			else if (y > 370)
				y = 370;
			
			endPoint = new Point(x, y);
		}			
	}
		
	/**
	 * Set format to well values
	 * 
	 * @param current current generated value
	 * @param next next generated value
	 * @return string value
	 */
	private void setFractionDigits(float step)	{
				
		if (step>=10)
			format.setMaximumFractionDigits(0);
		else if (step<10&&step>=0.1)
			format.setMaximumFractionDigits(2);
		else if (step<0.1&&step>0.01)
			format.setMaximumFractionDigits(3);
		else if (step<=0.01)
			format.setMaximumFractionDigits(4);
		
	}
		
	/**
	 * Round up this value to times of 10 (easy to read)
	 */
	private float roundUp(float input, boolean goup){
		//this is simple sulotion, add more codes later
		float times10 = input / 10.0f;
		if (goup){
			return ((int)times10+1)*10;
		}else{
			return ((int)times10)*10;
		}
	}
}
