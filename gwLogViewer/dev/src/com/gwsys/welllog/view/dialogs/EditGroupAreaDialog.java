/**
 * 
 */
package com.gwsys.welllog.view.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.fill.CompoundFill;
import com.gwsys.welllog.view.fill.CurveFillBeside;

/**
 * a dialog to set areas between curves
 * 
 * @author Alex
 * 
 */
public class EditGroupAreaDialog extends JDialog implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JPanel headPanel, configPanel, btnPanel;
	private JLabel leftLabel, rightLabel, fillLabel;
	private JComboBox leftCB, rightCB;
	private JButton fillBtn, applyBtn, cancelBtn;
	
	private CurveView cv;
	private CurveGroup cg;
	private String group;
	
	private Color selectedColor;
	
	private ArrayList<String> curves = new ArrayList<String>();
	
	private static EditGroupAreaDialog instance = null;

	/**
	 * default constructor
	 * @param a group
	 */
	public EditGroupAreaDialog(String group)	{
		super();
		this.group = group;
		this.cg = ViewCoreOperator.getCurveGroup(group);
		this.cv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
					.getTrackContainer().getSelectedAxisCurveContainer().getCurveView();
		getCurveFromThisGroup();
		createGUI();
	}
	
	public static EditGroupAreaDialog getInstance(String group)	{
		if (instance == null)
			instance = new EditGroupAreaDialog(group);
		
		return instance;
	}
	
	/**
	 * 
	 */
	private void getCurveFromThisGroup() {
		curves = ViewCoreOperator.getCurveHashkeysFromThisGroup(this.group);		
	}

	/**
	 * set groupe to use for this dialog
	 * @param groupName
	 */
	public void setGroup(String group)	{
		this.group = group;
	}

	/**
	 * 
	 */
	private void createGUI() {
		setTitle("Create a fill area between 2 curves");
		setSize(370,220);
		setResizable(false);
		setModal(true);
				
		getContentPane().setLayout(new BorderLayout());
		
		createHeadPanel();
		buildConfigPanel();
		createButtonPanel();
	}

	/**
	 * 
	 */
	private void createHeadPanel() {
		headPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel headLabel = new JLabel("Group: ".concat(this.group));
		headPanel.add(headLabel);
		
		getContentPane().add(headPanel, BorderLayout.NORTH);
	}

	/**
	 * 
	 */
	private void buildConfigPanel() {
		configPanel = new JPanel(null);		
		
		Border border = BorderFactory.createTitledBorder("Set up areas in this group");
		configPanel.setBorder(border);		
		
		leftLabel = new JLabel("Left:");
		leftLabel.setBounds(30,40,50,20);
		
		leftCB = new JComboBox();
		leftCB.addItem("Left Side");				
		leftCB.setBounds(60,40,80,20);
		leftCB.addActionListener(this);
		leftCB.setActionCommand("selectedLeftSide");
		fillThisCBox(leftCB, "");
				
		rightLabel = new JLabel("Right:");
		rightLabel.setBounds(200,40,50,20);
		
		rightCB = new JComboBox();
		rightCB.addItem("Right Side");		
		rightCB.setBounds(230,40,80,20);
		rightCB.addActionListener(this);
		rightCB.setActionCommand("selectedRightSide");
		fillThisCBox(rightCB, "");
		
		fillLabel = new JLabel("Fill Color:");
		fillLabel.setBounds(30,83,100,20);
		
		fillBtn = new JButton("Change");
		Color fill = Color.PINK;		
		fillBtn.setIcon(EditCurveDialog.createColorIcon(fill));
		fillBtn.setBounds(100,80,100,25);
		fillBtn.addActionListener(this);
		fillBtn.setActionCommand("showColorChooser");
		
		configPanel.add(leftLabel);
		configPanel.add(rightLabel);
		configPanel.add(leftCB);
		configPanel.add(rightCB);
		configPanel.add(fillLabel);
		configPanel.add(fillBtn);
		
		getContentPane().add(configPanel, BorderLayout.CENTER);
	}
	
	/**
	 * fill given combo box with curve names
	 * @param a combo box
	 */
	private void fillThisCBox(JComboBox comboBox, String except) {
		if (except == null)
			except = "";
		
		for (int i = 0; i < this.curves.size(); i++)	{
			String curveName = this.curves.get(i).split(OperationClass.hashMapNameSeparator)[1];
			
			if (!curveName.equals(except))
				comboBox.addItem(curveName);
		}
	}

	/**
	 * build button panel
	 */
	private void createButtonPanel() {
		btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		applyBtn = new JButton("Appply");
		applyBtn.addActionListener(this);
		applyBtn.setActionCommand("applyChanges");
		
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(this);
		cancelBtn.setActionCommand("cancel");
		
		btnPanel.add(applyBtn);
		btnPanel.add(cancelBtn);
		
		getContentPane().add(btnPanel, BorderLayout.SOUTH);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("applyChanges"))	{
			if (validateSelection())	{
				selectedColor = fillBtn.getForeground();
				
				if (leftCB.getSelectedIndex() == 0)	{					
					createSingleFill(getCurvePanel(curves.get(rightCB.getSelectedIndex()-1)), ConstantVariableList.FILL_LEFT);
				}
				else if (rightCB.getSelectedIndex() == 0)	{					
					createSingleFill(getCurvePanel(curves.get(leftCB.getSelectedIndex()-1)), ConstantVariableList.FILL_RIGHT);
				}
				else	{
					CompoundFill cf = new CompoundFill();
					cf.constructFill(getCurvePanel(curves.get(leftCB.getSelectedIndex()-1)), 
									getCurvePanel(curves.get(rightCB.getSelectedIndex()-1)));
					cf.setFillColor(selectedColor);
					
					if (cf.getAllFills().size() > 0)
						cv.getCurveLayer().addCompoundFillToList(cf);
				}
				
				// repaint CurveView
				cv.setFillRedrawFlag(true);
				cv.repaintFather();
				
				//dispose this dialog
				this.setVisible(false);
				this.dispose();
				return;
			}
			
		}
		else if (e.getActionCommand().equals("cancel"))	{
			this.setVisible(false);
			this.dispose();
		}
		else if (e.getActionCommand().equals("showColorChooser"))	{
			Color color = JColorChooser.showDialog(this, "Choose Fill Color", fillBtn.getForeground());
			
			if (color != null){
				fillBtn.setIcon(EditCurveDialog.createColorIcon(color));
				fillBtn.setForeground(color);
			}
		}
	}
	
	/**
	 * if only one curve was selected just create a single fill (CurveFillBeside)
	 * @param cp curve panel
	 * @param orientation left or right
	 */
	private void createSingleFill(CurvePanel cp, String orientation)	{		
		CurveFillBeside cfb = new CurveFillBeside();
		cfb.setCp(cp);
		cfb.setLeftOrRight(orientation);
		cfb.setFillIntoGroup(true);
		cfb.constructFill(cv, cg);
		cfb.setFillColor(selectedColor);
		cv.getCurveLayer().addFillBesideToList(cfb);
	}

	/**
	 * chek if same curves were selected
	 * @return true or false
	 */
	private boolean validateSelection() {
		if (this.leftCB.getSelectedIndex() == this.rightCB.getSelectedIndex())	{
			JOptionPane.showMessageDialog(this, "Cannot possible apply area between same curves or side", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	/**
	 * get curve panel from curvehashkey
	 * @param curveHashkey
	 * @return CurvePanel
	 */
	private CurvePanel getCurvePanel(String curveHashkey)	{
		String well = curveHashkey.split(OperationClass.hashMapNameSeparator)[0];
		String curve = curveHashkey.split(OperationClass.hashMapNameSeparator)[1];
		String group = this.group.split(OperationClass.hashMapNameSeparator)[1];
		
		return ViewCoreOperator.getCurvePanelByName(curve, group, well);
	}

}
