/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.yaxis;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Vector;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import com.gwsys.welllog.core.curve.TrackTypeList;

/**
 * Data Converter
 * @author 
 *
 */
public class DataTypeConverter implements DepthConverter{
	/**
	 * is ready to convert, true means data has been read
	 */
	boolean isReady = false;

	/**
	 * is time info has been converted into depth info 
	 */
	
	String filePath = "";
	
	int dataLength = 0;
	
	float[] twoWayTime = null;

	float[] tVD = null;

	float[] mD = null;

	float[] tVDSS = null;
	
	float[] sigmaT = null;

	float[] depthsMD = null;
	
	float[] depthsTVD = null;
	
	float[] depthsTVDSS = null;
	
	boolean isMDConverted = false;
	
	boolean isTVDConverted = false;
	
	boolean isTVDSSConverted = false;
	
	public float toDepth(float value, int type) {
		if(type == TrackTypeList.TIME_TO_MD){
			return timeToMD(value);
		}else if(type == TrackTypeList.MD_TO_TVD){
			return mDToTvd(value);
		}else if(type == TrackTypeList.MD_TO_TVDSS){
			return mDToTvdss(value);
		}else if(type == TrackTypeList.TVD_TO_MD){
			return tvdToMd(value);
		}else if(type == TrackTypeList.TVDSS_TO_MD){
			return tvdssToMd(value);
		}
		return 0;
	}

	private float timeToMD(float timeInSecond){
		for(int i = 0; i < dataLength; i++) {
			if(timeInSecond < twoWayTimeToSecond(twoWayTime[i])) {
				float startDepth = 0;
				float startSecond = 0;
				float velocity = 0;
				if(i==0){
					velocity = mD[i] / twoWayTimeToSecond(twoWayTime[i]);
					startDepth = mD[i];
					startSecond = twoWayTimeToSecond(twoWayTime[i]);
				}else{
					velocity = (mD[i]-mD[i-1]) / (twoWayTimeToSecond(twoWayTime[i])-twoWayTimeToSecond(twoWayTime[i-1]));
					startDepth = mD[i-1];
					startSecond = (twoWayTimeToSecond(twoWayTime[i-1]));
				}
				float depth = startDepth + velocity * (timeInSecond - startSecond);
				return depth;
			}
		}
//		if converting time is out of the range
		float velocity = mD[dataLength - 1]/ (twoWayTimeToSecond(twoWayTime[dataLength-1]));
		float startTvdss = twoWayTimeToSecond(twoWayTime[dataLength - 1]);
		float startDepth = mD[dataLength - 1];
		float depth = startDepth + velocity * (timeInSecond - startTvdss);
		return depth;
	}
	
	private float mDToTvd(float mDDepth) {
		for(int i = 0; i < dataLength; i++) {
			if(mDDepth < mD[i]) {
				float startDepth = 0;
				float startMd = 0;
				float velocity = 0;
				if(i==0){
					velocity = tVD[i] / mD[i];
					startDepth = tVD[i];
					startMd = mD[i];
				}else{
					velocity = (tVD[i]-tVD[i-1]) / (mD[i]-mD[i-1]);
					startDepth = tVD[i-1];
					startMd = mD[i-1];
				}
				float depth = startDepth + velocity * (mDDepth - startMd);
				return depth;
			}
		}
//		if converting time is out of the range
		float velocity = tVD[dataLength - 1]/ mD[dataLength - 1];
		float startMd = mD[dataLength - 1];
		float startDepth = tVD[dataLength - 1];
		float depth = startDepth + velocity * (mDDepth - startMd);
		return depth;
	}
	
	private float tvdToMd(float tVDDepth) {
		for(int i = 0; i < dataLength; i++) {
			if(tVDDepth < tVD[i]) {
				float startDepth = 0;
				float startTvd = 0;
				float velocity = 0;
				if(i==0){
					velocity = mD[i] / tVD[i];
					startDepth = mD[i];
					startTvd = tVD[i];
				}else{
					velocity = (mD[i]-mD[i-1]) / (tVD[i]-tVD[i-1]);
					startDepth = mD[i-1];
					startTvd = tVD[i-1];
				}
				float depth = startDepth + velocity * (tVDDepth - startTvd);
				return depth;
			}
		}

		float velocity = mD[dataLength - 1]/ tVD[dataLength - 1];
		float startTVd = tVD[dataLength - 1];
		float startDepth = mD[dataLength - 1];
		float depth = startDepth + velocity * (tVDDepth - startTVd);
		return depth;
	}

	private float mDToTvdss(float mDDepth){
		for(int i = 0; i < dataLength; i++) {
//			if converting time is in the range
			if(mDDepth < mD[i]) {
				float startDepth = 0;
				float startMd = 0;
				float velocity = 0;
				if(i==0){
					velocity = tVDSS[i] / mD[i];
					startDepth = tVDSS[i];
					startMd = mD[i];
				}else{
					velocity = (tVDSS[i]-tVDSS[i-1]) / (mD[i]-mD[i-1]);
					startDepth = tVDSS[i-1];
					startMd = mD[i-1];
				}
				float depth = startDepth + velocity * (mDDepth - startMd);
				return depth;
			}
		}
//		if converting time is out of the range
		float velocity = tVDSS[dataLength - 1] / mD[dataLength - 1];
		float startMd = mD[dataLength - 1];
		float startDepth = tVDSS[dataLength - 1];
		float depth = startDepth + velocity * (mDDepth - startMd);
		return depth;
	}
	
	private float tvdssToMd(float tvdssDepth){
		for(int i = 0; i < dataLength; i++) {
			if(tvdssDepth < tVDSS[i]) {
				float startDepth = 0;
				float startTvdss = 0;
				float velocity = 0;
				if(i==0){
					velocity = mD[i] / tVDSS[i];
					startDepth = mD[i];
					startTvdss = tVDSS[i];
				}else{
					velocity = (mD[i]-mD[i-1]) / (tVDSS[i]-tVDSS[i-1]);
					startDepth = mD[i-1];
					startTvdss = tVDSS[i-1];
				}
				float depth = startDepth + velocity * (tvdssDepth - startTvdss);
				return depth;
			}
		}
//		if converting time is out of the range
		float velocity = mD[dataLength - 1]/ tVDSS[dataLength - 1];
		float startTvdss = tVDSS[dataLength - 1];
		float startDepth = mD[dataLength - 1];
		float depth = startDepth + velocity * (tvdssDepth - startTvdss);
		return depth;
	}
	
	/**
	 * protected method, try to parse a checkshot file and validate the format
	 * 
	 * @param checkshotFilePath
	 * @return
	 */	
	
	@SuppressWarnings("unchecked") 
	boolean parseTxt(String checkshotFilePath) {
		this.filePath = checkshotFilePath;
		isReady = false;
		BufferedReader reader = null;
		Vector numberBuffer = new Vector();
		try {
			reader = new BufferedReader(new FileReader(checkshotFilePath));
		
			String line = null;
			// find data part
			while (true) {
				line = reader.readLine();
				// reach file end, but not find the data part
				if (line == null) {
					reader.close();
					return false;
				}
				// data part is just behind the line of "Sigma_T"
				if (line.startsWith("Sigma_T")||line.indexOf("2 Way-Time")>0) {
					break;
				}
			}
			// read data to temp buffer
			while (true) {
				line = reader.readLine();
				// reach the file end or reach empty line meaning the end of
				// data part
				if (line == null || line.equals("")) {
					break;
				}
				// use regular expression to split the numbers
				// in one line, numbers are split by several space characters,
				// so the regular expression be "\s+", meaning one or more space
				// characters
				String[] subStrings = line.split("\\s+");
				if (subStrings != null && subStrings.length == 4) {
					numberBuffer.add(subStrings[0]);
					numberBuffer.add(subStrings[1]);
					numberBuffer.add(subStrings[2]);
					numberBuffer.add(subStrings[3]);
				}
				// bad format checkshot.txt file
				else {
					reader.close();
					return false;
				}
			}
		} catch (FileNotFoundException e) {
			try {
				reader.close();
			} catch (IOException e1) {
			}
			return false;			
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				reader.close();
			} catch (IOException e1) {
			}
		}

		// reformat data to array
		dataLength = numberBuffer.size() / 4;
		twoWayTime = new float[dataLength];
		tVD = new float[dataLength];
		mD = new float[dataLength];
		sigmaT = new float[dataLength];
		boolean colTime = true;
		try{
			Float.parseFloat(numberBuffer.get(0).toString());
		}catch(NumberFormatException e){
			colTime = false;
			tVDSS = new float[dataLength];
		}
		for (int i = 0; i < dataLength; i++) {
			if (colTime){
				twoWayTime[i] = Float.parseFloat(numberBuffer.get(i * 4)
						.toString());
				tVD[i] = Float.parseFloat(numberBuffer.get(i * 4 + 1)
						.toString());
				mD[i] = Float
						.parseFloat(numberBuffer.get(i * 4 + 2).toString());
				sigmaT[i] = Float.parseFloat(numberBuffer.get(i * 4 + 3)
						.toString());
			}else{
				twoWayTime[i] = Float.parseFloat(numberBuffer.get(i * 4+1).toString());
				tVDSS[i] = Float.parseFloat( numberBuffer.get(i * 4 + 3).toString());
				mD[i] = Float.parseFloat(numberBuffer.get(i * 4 + 2).toString());
				
			}
		}
		isReady = true;
		return isReady;
	}

	/**
	 * convert the original twoWayTime to be in second
	 * 
	 * @param twoWayTime
	 * @return
	 */
	float twoWayTimeToSecond(float twoWayTime) {
		return twoWayTime / 1000 / 2;
	}
	
	boolean parseXls(String surveyFilePath) {
		this.filePath = surveyFilePath;
		isReady = false;
		try{
			InputStream is = new FileInputStream(surveyFilePath);
			Workbook wb = Workbook.getWorkbook(is);
			Sheet sheet = wb.getSheet(0);
			
			dataLength = sheet.getRows()-3;
			tVDSS = new float[dataLength];
			tVD = new float[dataLength];
			mD = new float[dataLength];
			for(int i=3 ; i<sheet.getRows() ; i++){
				Cell cell=sheet.getCell(0,i);	
				mD[i-3] = Float.parseFloat(cell.getContents());
			    
			    cell=sheet.getCell(1,i);
			    tVD[i-3] = Float.parseFloat(cell.getContents());
			    
			    cell=sheet.getCell(2,i);		 
			    tVDSS[i-3] = -Float.parseFloat(cell.getContents());
			}
			
			wb.close();
			is.close();
		}catch(Exception e){
			return false;
		}
		isReady = true;
		return true;
	}
	
	/**
	 * added by Alex
	 * parse a text file to get MD and TVD data
	 * @param surveyFilePath file path
	 * @return true or false
	 */
	boolean parseTxtFile(String surveyFilePath)	{
		this.filePath = surveyFilePath;
		isReady = false;
			
		String[] values = new String[3];
		ArrayList<String[]> result = getResultData(this.filePath);
		
		if (result == null)
			return isReady;
		
		dataLength = result.size();
		mD = new float[dataLength];
		tVD = new float[dataLength];
		tVDSS = new float[dataLength];
		
		for (int i = 0; i < dataLength; i++)	{
			values = result.get(i);
			mD[i] = Float.parseFloat(values[0]);
			tVD[i] = Float.parseFloat(values[1]);
			if (!values[2].equals("0"))
				tVDSS[i] = -Float.parseFloat(values[2]);
			else
				tVDSS[i] = (tVD[i] - 0.0f) * -1;			
		}
					
		isReady = true;
		return isReady;
	}

	/**
	 * added by Alex
	 * gets and arra list containing MD, TVD and TVDSS(if exists) data
	 * @param filepath to create BuffereReader
	 * @return array list
	 */			
	@SuppressWarnings("unchecked")
	private ArrayList<String[]> getResultData(String filepath) {
		int step = 0;
		ArrayList<String[]> dataList = new ArrayList();
		String[] result = new String[3];
		
		int posMD = 0, posTVD = 0, posTVDSS = 0;
		
		ArrayList<String> typeList = null;
		boolean eof = false;
		String line;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filepath));
			
			while (!eof)	{
				line = reader.readLine();
				if (line == null)
					eof = true;
				else	{
					//System.out.println(line);
					if (step==1 && !line.startsWith("#"))	{
						typeList = getTypeList(line);
						posMD = typeList.indexOf("MD");
						posTVD = typeList.indexOf("TVD");
						posTVDSS = typeList.indexOf("TVDSS");
					}
					if (step==2 && !line.startsWith("#"))	{
						typeList = getTypeList(line);
						result[0] = typeList.get(posMD);
						result[1] = typeList.get(posTVD);
						if (posTVDSS != -1)
							result[2] = typeList.get(posTVDSS);
						else
							result[2] = "0";
						dataList.add(result.clone());
					}
					if (line.startsWith("#="))
						step++;
				}
			}
			
			reader.close();			
		} catch (IOException e) {			
			return null;
		}
		
		return dataList;
	}
	
	/**
	 * added by Alex
	 * get an array list from string text
	 * @param line text will be divided
	 * @return array list containing tokens
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<String> getTypeList(String line)	{
		ArrayList<String> values = new ArrayList();
		String[] tokens = line.split(" ");
		
		for (int i = 0; i < tokens.length; i++)	{
			if (tokens[i].length() > 0)
				values.add(tokens[i]);
		}
		
		return values;
	}

	public String getFilePath() {
		return filePath;
	}
	
	// for survey data
	/**
	 * retrieves array of MD values
	 * @return MD values
	 * @author Alex
	 */
	public float[] getMDValues()	{
		return this.mD;
	}
	
	/**
	 * retrieves array of TVD values
	 * @return MD values
	 * @author Alex
	 */
	public float[] getTVDValues()	{
		return this.tVD;
	}
	
	/**
	 * retrieves array of TVDSS values
	 * @return TVDSS values
	 * @author Alex
	 */
	public float[] getTVDSSValues()	{
		return this.tVDSS;
	}
	
	// for checkshot data
	/**
	 * retrieves array of TIME values
	 * @return TwoWayTime values
	 * @author Alex
	 */
	public float[] getTwoWayTimeValues()	{
		return this.twoWayTime;
	}
}
