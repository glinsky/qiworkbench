/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.yaxis;

import com.gwsys.welllog.core.curve.TrackTypeList;

public class DataConverterFactory {
	public static DepthConverter createInstance(float velocity) {
		return new SimpleTimeToDepthConverter(velocity);
	}

	/**
	 * create a new TimeToDepthConverter
	 * @param checkshotFilePath
	 * @return
	 */
	public static DepthConverter createInstance(String FilePath, int type) {
		DataTypeConverter converter = new DataTypeConverter();
		if(type == TrackTypeList.TIME_TO_MD){
			if(converter.parseTxt(FilePath) == true) {
				return converter;
			}
			else {
				return null;
			}
		}
		if(type == TrackTypeList.MD_TO_TVD){
			if (FilePath.endsWith("xls"))	{
				if(converter.parseXls(FilePath) == true) {
					return converter;
				}
			}
			else if (FilePath.endsWith("txt"))	{
				if(converter.parseTxtFile(FilePath) == true) {
					return converter;
				}
			}
			else {
				return null;
			}
		}
		return null;
	}

	/**
	 *
	 * @param filePath
	 * @return DepthToTimeConverter
	 */
	public static DepthToTimeConverter createInstance(String filePath) {
		return new CheckshotConverter(filePath);
	}
}
