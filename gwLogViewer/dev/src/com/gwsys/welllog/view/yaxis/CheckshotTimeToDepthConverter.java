/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.yaxis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

/**
 * Supports two kinds of checkshot files.
 * @author 
 *
 */
public class CheckshotTimeToDepthConverter implements TimeToDepthConverter {
	/**
	 * is ready to convert, true means data has been read
	 */
	boolean isReady = false;

	/**
	 * is time info has been converted into depth info 
	 */
	boolean isConverted = false;

	float[] twoWayTime = null;

	float[] tVD = null;

	float[] mD = null;

	float[] sigmaT = null;

	//float[] depths = null;

	/*
	public float[] getDepths() {
		if (isConverted == false) {
			toDepths();
			isConverted = true;
		}
		return depths;
	}
	*/

	public float toDepth(float timeInSecond) {
		//float[] depths = getDepths();
		for(int i = 0; i < twoWayTime.length; i++) {
//			if converting time is in the range
			if(timeInSecond < twoWayTimeToSecond(twoWayTime[i])) {
				float startDepth = 0;
				float endTimeInSecond = 0;
				if(i != 0) {
					startDepth = mD[i];
					endTimeInSecond = twoWayTimeToSecond(twoWayTime[i]);
				}
				float velocity = mD[i] / twoWayTimeToSecond(twoWayTime[i]);
				float depth = startDepth - velocity * (endTimeInSecond - timeInSecond);
				return depth;
			}
		}
//		if converting time is out of the range
		float velocity = mD[twoWayTime.length - 1] / twoWayTimeToSecond(twoWayTime[twoWayTime.length - 1]);
		float startTimeInSecond = twoWayTimeToSecond(twoWayTime[twoWayTime.length - 1]);
		float startDepth = mD[twoWayTime.length - 1];
		float depth = startDepth + velocity * (timeInSecond - startTimeInSecond);
		return depth;
	}

	/**
	 * convert time info to depth info
	 */
	/*
	void toDepths() {
		depths = new float[twoWayTime.length];
		depths[0] = mD[0];
		for (int i = 1; i < twoWayTime.length; i++) {
			float startTimeInSecond = twoWayTimeToSecond(twoWayTime[i - 1]);
			float endTimeInSecond = twoWayTimeToSecond(twoWayTime[i]);
			float velocity = mD[i] / endTimeInSecond;
			depths[i] = depths[i - 1] + velocity * (endTimeInSecond - startTimeInSecond);
			// System.out.println(depths[i - 1] + " + " + velocity + " * (" +
			// endTimeInSecond + " - " + startTimeInSecond + ") = " +
			// depths[i]);
		}
	}
	*/

	/**
	 * protected method, try to parse a checkshot file and validate the format
	 * 
	 * @param checkshotFilePath
	 * @return
	 */		
	@SuppressWarnings("unchecked") 
	public boolean parse(String checkshotFilePath) {
		isReady = false;
		BufferedReader reader = null;

		Vector numberBuffer = new Vector();
		try {
			reader = new BufferedReader(new FileReader(checkshotFilePath));
		
			String line = null;
			// find data part
			while (true) {
				line = reader.readLine();
				// reach file end, but not find the data part
				if (line == null) {
					reader.close();
					return false;
				}
				// data part is just behind the line of "Sigma_T"
				if (line.startsWith("Sigma_T")||line.indexOf("2 Way-Time")>0) {
					break;
				}
			}
			// read data to temp buffer
			while (true) {
				line = reader.readLine();
				// reach the file end or reach empty line meaning the end of
				// data part
				if (line == null || line.equals("")) {
					break;
				}
				// use regular expression to split the numbers
				// in one line, numbers are split by several space characters,
				// so the regular expression be "\s+", meaning one or more space
				// characters
				String[] subStrings = line.split("\\s+");
				if (subStrings != null && subStrings.length == 4) {
					numberBuffer.add(subStrings[0]);
					numberBuffer.add(subStrings[1]);
					numberBuffer.add(subStrings[2]);
					numberBuffer.add(subStrings[3]);
				}
				// bad format checkshot.txt file
				else {
					reader.close();
					return false;
				}
			}
		} catch (FileNotFoundException e) {
			try {
				reader.close();
			} catch (IOException e1) {
			}
			return false;			
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				reader.close();
			} catch (IOException e1) {
			}
		}

		// reformat data to array
		int size = numberBuffer.size() / 4;
		twoWayTime = new float[size];
		tVD = new float[size];
		mD = new float[size];
		sigmaT = new float[size];
		boolean colTime = true;
		try{
			Float.parseFloat(numberBuffer.get(0).toString());
		}catch(NumberFormatException e){
			colTime = false;
		}
		for (int i = 0; i < size; i++) {
			if (colTime){
				twoWayTime[i] = Float.parseFloat(numberBuffer.get(i * 4)
						.toString());
				tVD[i] = Float.parseFloat(numberBuffer.get(i * 4 + 1)
						.toString());
				mD[i] = Float
						.parseFloat(numberBuffer.get(i * 4 + 2).toString());
				sigmaT[i] = Float.parseFloat(numberBuffer.get(i * 4 + 3)
						.toString());
			}else{
				twoWayTime[i] = Float.parseFloat(numberBuffer.get(i * 4+1).toString());
				tVD[i] = Float.parseFloat( numberBuffer.get(i * 4 + 3).toString());
				mD[i] = Float.parseFloat(numberBuffer.get(i * 4 + 2).toString());
				
			}
		}
		isReady = true;
		return true;
	}

	/**
	 * convert the original twoWayTime to be in second
	 * 
	 * @param twoWayTime
	 * @return
	 */
	float twoWayTimeToSecond(float twoWayTime) {
		return twoWayTime / 1000 / 2;
	}
	


	public void printInfo() {
		printTime();
		printDepth();
	}

	/**
	 * print the original data
	 */
	void printTime() {
		if (isReady == false) {
			System.out.println("not initialized yet...");
			return;
		}
		System.out.println("Two Way Time\tTVD\tMD\tSigma_T");
		for (int i = 0; i < twoWayTime.length; i++) {
			System.out.println(twoWayTime[i] + "\t" + tVD[i] + "\t" + mD[i] + "\t" + sigmaT[i]);
		}
	}

	/**
	 * convert time to depth and print the depth
	 */
	void printDepth() {
		if (isReady == false) {
			System.out.println("not initialized yet...");
			return;
		}
	}
}
