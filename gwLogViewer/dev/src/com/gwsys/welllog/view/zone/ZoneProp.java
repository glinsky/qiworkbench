package com.gwsys.welllog.view.zone;


import java.awt.Color;
import java.util.ArrayList;
import java.util.Hashtable;

import com.gwsys.welllog.core.curve.WellLogPattern;

/**
 * Defines the properties for Zone.
 * @author Team
 * @since Aug 10, 2006 getLithoColor
 */
public class ZoneProp{

	public static final int EMPTY = 0;
	public static final int GOOD = 1;
	public static final int PROBABLY_OK = 2;
	public static final int QUESTIONABLE = 3;

	public static final int LITHOLOGY_SHALE = 2;
	public static final int LITHOLOGY_MARL = 6;
	public static final int LITHOLOGY_CARBONATE = 5;
	public static final int LITHOLOGY_SILISTONE = 8;
	public static final int LITHOLOGY_ANHYDRITE = 11;
	public static final int LITHOLOGY_DOLOMITE = 12;
	public static final int LITHOLOGY_SAND = 17;
	public static final int LITHOLOGY_SANDSTONE = 1;

	public static String LOG_QUALITY[] = new String[]{"Unknown",
		                                              "Good",
		                                              "Probably Ok",
		                                              "Questionable"
		                                              };

    private static ArrayList<String> LITHOLOGY = new ArrayList<String>(25);

    static {LITHOLOGY.add("Undifferentiated");
    	LITHOLOGY.add("Sandstone");
		LITHOLOGY.add("Shale");
		LITHOLOGY.add("Laminated-Sand/Shale");
		LITHOLOGY.add("Coal");
		LITHOLOGY.add("Carbonate");
		LITHOLOGY.add("Marl");
		LITHOLOGY.add("Volcanic");
		LITHOLOGY.add("Siltstone");
		LITHOLOGY.add("Claystone");
		LITHOLOGY.add("Laminated-Sand/Carbonated");
		LITHOLOGY.add("Anhydrite");
		LITHOLOGY.add("Dolomite");
		LITHOLOGY.add("Limestone");
		LITHOLOGY.add("Halite");
		LITHOLOGY.add("Frac. Lim.");
		LITHOLOGY.add("Laminated-Sand");
		LITHOLOGY.add("Sand");
		LITHOLOGY.add("Clay");
		LITHOLOGY.add("Shales");
		LITHOLOGY.add("New Name");
    	                                  }
    public static String FLUID_TYPE[] = new String[]{"Non Relevant",
								        "Oil",
								        "Gas",
								        "Condensate",
								        "Biodeg Oil",
								        "Fizz Gas"
								        };
    public static String PRESSURE_TYPE[] = new String[]{"Not Available",
								        "Drilling Test",
								        "RFT"
								        };

    public static int P10 = 0;
    public static int P50 = 1;
    public static int P90 = 2;
    public static int AVG = 3;
    public static int SAMPLES = 4;

    private static Hashtable<String, Color> LithoColors=null;

    /**
     * Returns the color by lithology. if there is no matching color,
     * just return the top onr from color stack.
     * @param name
     * @return
     */
	public static Color getLithoColor(Object name){
		if (LithoColors==null){
			LithoColors=new Hashtable<String, Color>();
			LithoColors.put("limestone" , new Color(128, 255, 255)); // middle cyan
			LithoColors.put("marl" , new Color(124, 252, 0)); //lawn green
			LithoColors.put("carbonate" , Color.blue);
			LithoColors.put("sandstone" , Color.yellow); //yellow
			LithoColors.put("sand" , Color.yellow); //yellow
			LithoColors.put("shale" , new Color(127, 127, 127));
			LithoColors.put("shales" , new Color(127, 127, 127));
			LithoColors.put("clay" , new Color(127, 127, 127));
			LithoColors.put("claystone" , new Color(127, 127, 127));
			LithoColors.put("shales(lfiv)" , Color.gray);
			LithoColors.put("laminated-sand" , Color.orange);
			LithoColors.put("laminated-sand/shale" , Color.orange);
			LithoColors.put("laminated-sand/carbonated" , Color.orange);
			LithoColors.put("calcarenite" , Color.pink);
			LithoColors.put("anhydrite" , new Color(255, 128, 255));
			LithoColors.put("dolomite" , new Color(128, 128, 255));
			LithoColors.put("halite" , new Color(0x15, 0x35, 0xff));
			LithoColors.put("frac. lim" , Color.magenta);
			LithoColors.put("low vp shl" , new Color(255,99, 71) );
			LithoColors.put("coal" , Color.black );
			LithoColors.put("siltstone" , new Color(154, 255, 154) );
			LithoColors.put("silt" , new Color(154, 255, 154) );
			LithoColors.put("volcanic" , new Color(0xf8, 0x80, 0x17) );
		}
		Color obj = LithoColors.get(name.toString().toLowerCase());
		if (obj==null)
			obj = getUniqueColor();
		if (obj.getRed()>230&&obj.getGreen()>230&&obj.getBlue()>230)
			obj = Color.darkGray;
		return (Color)obj;
	}

	//Define the color table
	private static ArrayList<Color> colorTable=new ArrayList<Color>();
	static{
		colorTable.add(new Color(0x25, 0x41, 0x17));
		colorTable.add(new Color(0xf8, 0x80, 0x17));
		colorTable.add(new Color(0x7d, 0x1b, 0x7e));
		colorTable.add(new Color(0xe1, 0x8b, 0x6b));
		colorTable.add(new Color(0x8b, 0xb3, 0x81));
		colorTable.add(new Color(0xf5, 0x28, 0x87));
		colorTable.add(new Color(0x3b, 0xb9, 0xff));

		colorTable.add(new Color(0xd4, 0xa0, 0x17));
		colorTable.add(new Color(0x73, 0x6f, 0x6e));
		colorTable.add(new Color(0x00, 0xff, 0x00));
		colorTable.add(new Color(0x87, 0xf7, 0x17));
		colorTable.add(new Color(0xad, 0xdf, 0xff));
		colorTable.add(new Color(0xfa, 0xaf, 0xba));

		colorTable.add(new Color(0xff, 0x00, 0xff));
		colorTable.add(new Color(0x81, 0x05, 0x41));
		colorTable.add(new Color(0x84, 0x67, 0xd7));
		colorTable.add(new Color(0xfd, 0xe0, 0xac));
		colorTable.add(new Color(0x15, 0x05, 0x67));
		colorTable.add(new Color(0xf8, 0x7a, 0x17));
		colorTable.add(new Color(0xe5, 0x7d, 0xed));

		colorTable.add(new Color(0x79, 0xd8, 0x67));
		colorTable.add(new Color(0xae, 0xeb, 0xec));
		colorTable.add(new Color(0xc5, 0x77, 0x26));
		colorTable.add(new Color(0xfa, 0xaf, 0xbe));
		colorTable.add(new Color(0x8e, 0x35, 0xef));

		colorTable.add(new Color(0xff, 0x00, 0x00));
		colorTable.add(new Color(0xb3, 0x84, 0x81));
		colorTable.add(new Color(0x4e, 0x89, 0x75));
		colorTable.add(new Color(0x8a, 0x41, 0x17));
		colorTable.add(new Color(0x66, 0x98, 0xff));
		colorTable.add(new Color(0x4a, 0xa0, 0x2c));

		colorTable.add(new Color(0xd8, 0xaf, 0x79));
		colorTable.add(new Color(0xf7, 0x54, 0x31));
		colorTable.add(new Color(0x8d, 0x38, 0xc9));
		colorTable.add(new Color(0xf3, 0xda, 0xa9));
		colorTable.add(new Color(0xff, 0xff, 0x00));
		colorTable.add(new Color(0x52, 0xd0, 0x17));
	}

	private static int ColorIndex = 0;

	public static Color getUniqueColor(){
		if (ColorIndex==colorTable.size())
			ColorIndex=0;
		return colorTable.get(ColorIndex++);
	}

	public static int getLithologyIndex(String lithology){
		for (int i=0; i<LITHOLOGY.size(); i++){
			if (LITHOLOGY.get(i).equalsIgnoreCase(lithology.trim()))
				return i;
		}
		return 0;
	}

	public static void addLithology(String litho){
		if (!LITHOLOGY.contains(litho.trim())){
			LITHOLOGY.set(LITHOLOGY.size()-1, litho.trim());
			LITHOLOGY.add("New Name");
		}
	}

	public static String[] getLithology(){
		return LITHOLOGY.toArray(new String[0]);
	}

	public static void setLithology(String[] names){
		LITHOLOGY.clear();
		for (int i=0; i<names.length; i++)
			LITHOLOGY.add(names[i].trim());
	}

	public static ArrayList getLithoList(){
		return LITHOLOGY;
	}

	//from FillPattern to create array once
	public static WellLogPattern[] getWellLogPatterns(){
		if (choise!=null)
			return choise;
		choise = new WellLogPattern[16];
		choise[0] = WellLogPattern.loadPattern("Basement", null, Color.black);
		choise[1] = WellLogPattern.loadPattern("Chert",null, Color.black);
		choise[2] = WellLogPattern.loadPattern("Dolomite", null, Color.black);
		choise[3] = WellLogPattern.loadPattern("Limestone", null, Color.black);
		choise[4] = WellLogPattern.loadPattern("Sandstone", null, null);
		choise[5] = WellLogPattern.loadPattern("Shale", null, null);
		choise[6] = WellLogPattern.loadPattern("Siltstone", null, null);
		choise[7] = WellLogPattern.loadPattern("Carbonate", null, null);
		choise[8] = WellLogPattern.loadPattern("fract_carbon", null, Color.BLACK);
		choise[9] = WellLogPattern.loadPattern("Halite", null, Color.BLACK);
		choise[10] = WellLogPattern.loadPattern("Marl", null, Color.BLACK);
		choise[11] = WellLogPattern.loadPattern("Gravel", null, Color.BLACK);
		choise[12] = WellLogPattern.loadPattern("shaley_sd", null, Color.BLACK);
		choise[13] = WellLogPattern.loadPattern("Organic_shale", null, Color.BLACK);
		choise[14] = WellLogPattern.loadPattern("Anhydrite", null, Color.BLACK);
		choise[15] = WellLogPattern.loadPattern("Coal", null, Color.BLACK);

		return choise;
	}

	private static WellLogPattern choise[];

	private static Hashtable<String, Float> lithoDensity;

	public static float getDensityFromLithology(int litho){
		if (litho<0||litho>LITHOLOGY.size())
			return 2.65f;
		String lithology = LITHOLOGY.get(litho);
		if (lithoDensity==null){
			lithoDensity = new Hashtable<String, Float>();
			lithoDensity.put("sand", 2.65f);
			lithoDensity.put("limestone", 2.71f);
			lithoDensity.put("dolomite", 2.87f);
			lithoDensity.put("anhydrite", 2.95f);
			lithoDensity.put("gypsum", 2.35f);
			lithoDensity.put("biotite", 3.20f);
			lithoDensity.put("kaolinite", 2.64f);
			lithoDensity.put("chlorite", 2.87f);
			lithoDensity.put("barite", 4.08f);
			lithoDensity.put("siderite", 3.91f);
			lithoDensity.put("halite", 2.03f);
			lithoDensity.put("sylvite", 1.86f);
			lithoDensity.put("carnalite", 1.56f);
			lithoDensity.put("coal", 1.47f);
			lithoDensity.put("lignite", 1.19f);
		}
		Float den = lithoDensity.get(lithology.toLowerCase());
		if (den!=null)
			return den;
		return 2.65f;
	}
}
