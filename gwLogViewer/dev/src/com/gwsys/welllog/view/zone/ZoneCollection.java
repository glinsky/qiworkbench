/*
RockAnalysis - a tool to visualize and analyze rock properties.
Copyright (C) 2006 G&W Systems Consulting Corp.
http://www.g-w-systems.com
*/

package com.gwsys.welllog.view.zone;

import java.util.ArrayList;

/**
 * Defines the interface to hold list of zones.
 * @author Louis
 *
 */
public interface ZoneCollection {

	public ArrayList getZoneList();

	public void zoneUpdated();
}
