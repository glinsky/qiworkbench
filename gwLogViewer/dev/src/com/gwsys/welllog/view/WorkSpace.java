/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.data.util.XMLFactory;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.yaxis.DataConverterFactory;
import com.gwsys.welllog.view.yaxis.DataTypeConverter;
import com.gwsys.welllog.view.yaxis.DepthConverter;
import com.gwsys.welllog.view.zone.ZoneProp;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
/**
 * Save or load workspace.
 */
public class WorkSpace {
	private static final String separator = ";";
	private WorkSpace() {
	}

	@SuppressWarnings("unchecked")
	public static void saveWorkSpaceToXml(String file, MainFrameView jMainFrame) {
		String filename = file;
		if (!file.toLowerCase().endsWith(".wks"))
			filename = file + ".wks";
		Document doc = createDocument(jMainFrame);
		if (doc!=null)
			XMLFactory.writeXml(doc, filename);
	}

	private static Document createDocument(MainFrameView jMainFrame){
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			jMainFrame.setMessage(pce.getMessage());
			return null;
		}
		Document doc = db.newDocument();
		// write root
		Element xmlRoot = doc.createElement("WorkSpaceXML");

		for (int i = 0; i < DataBuffer.getJMainFrame().getMainBuffer()
				.getWellInfoList().size(); i++) {
			Element file = doc.createElement("File");
			file.setAttribute("id", Integer.toString(i));
			WellInfo wellInfo = (WellInfo)DataBuffer.getJMainFrame().getMainBuffer()
					.getWellInfoList().get(i);
			file.setAttribute("path", wellInfo.getSourceName());
			file.setAttribute("xLocation",Float.toString(wellInfo.getXLocation()));
			file.setAttribute("yLocation",Float.toString(wellInfo.getYLocation()));
			xmlRoot.appendChild(file);
		}

		for (int i = 0; i < DataBuffer.getJMainFrame().getMainBuffer()
				.getAllWellTops().size(); i++) {
			Element wellTops = doc.createElement("WellTops");
			wellTops.setAttribute("id", Integer.toString(i));
			wellTops.setAttribute("path", (String) DataBuffer.getJMainFrame()
					.getMainBuffer().getAllWellTops().get(i));
			xmlRoot.appendChild(wellTops);
		}

		Element wellInfoList = doc.createElement("wellInfoList");
		xmlRoot.appendChild(wellInfoList);
		for (int i = 0; i < DataBuffer.getJMainFrame().getMainBuffer().getMarkerList().size(); i++) {
			Element wellInfo = doc.createElement("wellInfo");
			wellInfo.setAttribute("markerName", ((MarkerInfo)DataBuffer.getJMainFrame()
					.getMainBuffer().getMarkerList().get(i)).getMarkerName());
			xmlRoot.appendChild(wellInfo);
		}

		for (int i = 0; i < DataBuffer.getJMainFrame().getMainBuffer()
				.getAllCheckShots().size(); i++) {
			Element checkShot = doc.createElement("CheckShot");
			checkShot.setAttribute("id", Integer.toString(i));
			checkShot.setAttribute("path", (String) DataBuffer.getJMainFrame()
					.getMainBuffer().getAllCheckShots().get(i));
			xmlRoot.appendChild(checkShot);
		}

		for (int i = 0; i < DataBuffer.getJMainFrame().getMainBuffer()
				.getAllSurveys().size(); i++) {
			Element survey = doc.createElement("Survey");
			survey.setAttribute("id", Integer.toString(i));
			survey.setAttribute("path", (String) DataBuffer.getJMainFrame()
					.getMainBuffer().getAllSurveys().get(i));
			xmlRoot.appendChild(survey);
		}

		Set wellNames = jMainFrame.getMainBuffer().getSurveyHashMap().keySet();
		Iterator it = wellNames.iterator();
		int j=0;
		while (it.hasNext()) {
			String wellName = it.next().toString();
			DataTypeConverter converter = (DataTypeConverter) DataBuffer
					.getJMainFrame().getMainBuffer().getSurveyHashMap().get(
							wellName);
			Element SurveyWell = doc.createElement("SurveyWell");
			SurveyWell.setAttribute("id", Integer.toString(j));
			SurveyWell.setAttribute("wellName", wellName);
			SurveyWell.setAttribute("path", converter.getFilePath());
			xmlRoot.appendChild(SurveyWell);
			j++;
		}

		for (int i = 0; i < jMainFrame.getSonJSplitPane()
				.getRightDesktopPane().getInternalFrameViewList().size(); i++) {
			InternalFrameView internalFrameView = (InternalFrameView) jMainFrame
					.getSonJSplitPane().getRightDesktopPane()
					.getInternalFrameViewList().get(i);
			Element xmlInternalFrameView = internalFrameView.saveXML(doc, i);
			xmlRoot.appendChild(xmlInternalFrameView);
		}

		//save zone listener
		if (jMainFrame.getZoneListener()!=null)
			jMainFrame.getZoneListener().saveXML(xmlRoot, doc);

		//save lithology
		Element litho = doc.createElement("Lithology");
		String[] lithoName = ZoneProp.getLithology();
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < lithoName.length; i++)
			buffer.append(lithoName[i]+separator);
		litho.setAttribute("Name", buffer.toString());
		xmlRoot.appendChild(litho);
		doc.appendChild(xmlRoot);
		return doc;
	}

	@SuppressWarnings("unchecked")
	public static void loadWorkSpaceFromXml(String filename, MainFrameView jMainFrame) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			System.out.println(pce);
			return;
		}
		Document doc = null;
		try {
			doc = db.parse(filename);
		} catch (DOMException dom) {
			System.out.println(dom);
			return;
		} catch (IOException ioe) {
			System.out.println(ioe);
			return;
		} catch (SAXException sax) {
			System.out.println(sax);
			return;
		}

		// get the root of tree
		Element xmlRoot = doc.getDocumentElement();

		loadSession(xmlRoot, filename, jMainFrame);

	}

	/**
	 *
	 * Loads the session with given XML Element.
	 */
	@SuppressWarnings("unchecked")
	public static void loadSession(Element xmlRoot, String filename,
			MainFrameView mainFrameView) {
		// delete all things of desktop panel and view tree
		ViewCoreOperator.clearContent();
		//restore Lithology
		NodeList lithList = xmlRoot.getElementsByTagName("Lithology");
		for (int i=0; i <lithList.getLength(); i++){

			if (lithList.item(i) instanceof Element){
				String name=((Element)lithList.item(i)).getAttribute("Name");
				String[] lithoName = name.split(separator);
				ZoneProp.setLithology(lithoName);
			}
		}
		NodeList nodeList = xmlRoot.getElementsByTagName("File");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element parsefilename = (Element) nodeList.item(i);
			String sourceName = parsefilename.getAttribute("path");
			File parsefile = new File(sourceName);
			OperationClass.initialTreeAndHashMap(parsefile, mainFrameView);
			String xLocation = parsefilename.getAttribute("xLocation");
			String yLocation = parsefilename.getAttribute("yLocation");
			if(xLocation != "" && yLocation != ""){
				ArrayList wellList = DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList();
				for(int j=0 ; j<wellList.size() ; j++){
					if(((WellInfo)wellList.get(j)).getSourceName().equals(sourceName)){
						((WellInfo)wellList.get(j)).setXLocation(Float.parseFloat(xLocation));
						((WellInfo)wellList.get(j)).setYLocation(Float.parseFloat(yLocation));
					}
				}
			}
		}

		nodeList = null;
		nodeList = xmlRoot.getElementsByTagName("WellTops");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlWellTops = (Element) nodeList.item(i);
			File parsefile = new File(xmlWellTops.getAttribute("path"));
			OperationClass.initialWellTopsTree(parsefile, mainFrameView);
		}

		nodeList = null;
		nodeList = xmlRoot.getElementsByTagName("wellInfoList");
		if(nodeList.getLength()>0){
			nodeList = null;
			nodeList = xmlRoot.getElementsByTagName("wellInfo");
			if(nodeList.getLength() == 0){
				DataBuffer.getJMainFrame().getMainBuffer().getMarkerList().clear();
			}else{
				ArrayList markerNameList = new ArrayList();
				for (int i = 0; i < nodeList.getLength(); i++) {
					Element xmlMarkerName = (Element) nodeList.item(i);
					markerNameList.add(xmlMarkerName.getAttribute("markerName"));
				}
				for (int i = 0; i < DataBuffer.getJMainFrame().getMainBuffer().getMarkerList().size(); i++) {
					String markerName = ((MarkerInfo)DataBuffer.getJMainFrame().getMainBuffer().getMarkerList().get(i)).getMarkerName();
					boolean hasMarker = false;
					for(int j = 0; j < markerNameList.size(); j++){
						if(((String)markerNameList.get(j)).equals(markerName)){
							hasMarker = true;
							break;
						}
					}
					if(!hasMarker){
						DataBuffer.getJMainFrame().getMainBuffer().getMarkerList().remove(i);
						DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().removeWellTopNode(markerName);
					}
				}
			}
		}

		nodeList = null;
		nodeList = xmlRoot.getElementsByTagName("CheckShot");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlCheckShot = (Element) nodeList.item(i);
			File parsefile = new File(xmlCheckShot.getAttribute("path"));
			OperationClass.initialCheckShotsTree(parsefile, mainFrameView);
		}

		HashMap surveyHashMap = new HashMap();

		nodeList = null;
		nodeList = xmlRoot.getElementsByTagName("Survey");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlSurvey = (Element) nodeList.item(i);
			String fileName = xmlSurvey.getAttribute("path");

			DepthConverter converter = (DepthConverter) (surveyHashMap.get(fileName));
			if (converter == null) {
				converter = DataConverterFactory.createInstance(fileName, TrackTypeList.MD_TO_TVD);
				if (converter == null) {
					// alert something
					JOptionPane
							.showMessageDialog(null,
									"Cann't parse the survey file in "
											+ fileName + "!");
					return;
				}
				if (!DataBuffer.getJMainFrame().getMainBuffer().getAllSurveys()
						.contains(fileName)) {
					DataBuffer.getJMainFrame().getMainBuffer().getAllSurveys()
							.add(fileName);
				}

				surveyHashMap.put(fileName, converter);

				if (DataBuffer.getJMainFrame().getMainBuffer()
						.getMDDepthConverter() == null) {
					DataBuffer.getJMainFrame().getMainBuffer()
							.setMDDepthConverter(converter);
				}
			}
		}

		nodeList = null;
		nodeList = xmlRoot.getElementsByTagName("SurveyWell");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlSurvey = (Element) nodeList.item(i);
			String fileName = xmlSurvey.getAttribute("path");
			String wellName = xmlSurvey.getAttribute("wellName");
			DataTypeConverter converter = (DataTypeConverter) surveyHashMap
					.get(fileName);
			DataBuffer.getJMainFrame().getMainBuffer().getSurveyHashMap().put(
					wellName, converter);
		}

		// second

		nodeList = xmlRoot.getElementsByTagName("InternalFrameView");
		InternalFrameView activeWindow = null;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlInternalFrameView = (Element) nodeList.item(i);

			InternalFrameView internalFrameView = ViewCoreOperator.createFrame(
					mainFrameView,
					xmlInternalFrameView.getAttribute("title"), false);

			internalFrameView.loadXML(xmlInternalFrameView);
			String xmlActiveWindow = xmlInternalFrameView.getAttribute("activeWindow");
			if (xmlActiveWindow != null) {
				if (ConversionClass.stringToBoolean(xmlActiveWindow)) {
					activeWindow = internalFrameView;
				}
			}//iconize after drawing

			ViewCoreOperator.connectMarker(internalFrameView);

		}

		if (activeWindow!=null){
			activeWindow.setVisible(true);
			DataBuffer.getJMainFrame().getMainBuffer().setInternalFrameView(activeWindow);
		}
		// use name of file as project name
		mainFrameView.getSonJSplitPane().getLeftTree().setProject(filename);

		NodeList xmlTrend = xmlRoot.getElementsByTagName("Trending");
		mainFrameView.setXMLNodeList(xmlTrend);
	}

	/**
	 * Return string.
	 * @return
	 */
	public static String toXMLString(){
		Document doc = createDocument(DataBuffer.getJMainFrame());
		if (doc!=null){
			//**Follow codes are used for qiWorkbench
			OutputFormat format    = new OutputFormat(doc);
			StringWriter stringOut = new StringWriter ();
			XMLSerializer serial   = new XMLSerializer (stringOut, format);
			try {
				serial.serialize(doc);
			} catch (IOException e) {
				return "<WorkSpaceXML>\n</WorkSpaceXML>";
			}
			return stringOut.toString();
		}
		return "<WorkSpaceXML>\n</WorkSpaceXML>";
	}
}
