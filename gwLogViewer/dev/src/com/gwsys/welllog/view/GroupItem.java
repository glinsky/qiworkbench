/**
 * 
 */
package com.gwsys.welllog.view;

import java.util.ArrayList;

/**
 * @author Alex
 *
 */
class GroupItem	{
	private String groupName;
	private boolean gridMode;
	private boolean displayRange;
	private String min;
	private String max;
	private ArrayList<CurveItem> curves = new ArrayList<CurveItem>();
	
	public GroupItem(String name)	{
		this.groupName = name;
	}
	
	public void addCurve(CurveItem curve)	{
		curves.add(curve);
	}
	
	public boolean removeCurve(CurveItem item)	{
		return curves.remove(item);
	}
	
	public int getSize()	{
		return curves.size();
	}
	
	public String getGroupName()	{
		return this.groupName;
	}
	
	public boolean isLogarithmic()	{
		return this.gridMode;
	}
	
	public void setGridMode(boolean mode)	{
		this.gridMode = mode;
	}
	
	public boolean isDisplayRange()	{
		return this.displayRange;
	}
	
	public void setDisplayRange(boolean value)	{
		this.displayRange = value;
	}
	
	public void setRangeValues(String min, String max)	{
		this.min = min;
		this.max = max;
	}
	
	public String getMinXDisplay()	{
		return this.min;
	}
	
	public String getMaxXDisplay()	{
		return this.max;
	}
	
	public ArrayList<CurveItem> getCurveItems()	{
		return this.curves;
	}	
}
