/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.welllog.core.Renderable;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.LogLayer;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.dialogs.ComboBoxRender;
import com.gwsys.welllog.view.zone.ZoneProp;
import com.gwsys.welllog.view.fill.CurveFillBeside;
import com.gwsys.welllog.view.fill.MarkerFill;
import com.gwsys.welllog.view.fill.MarkerLithology;
import com.gwsys.welllog.view.projecttree.ProjectTreeMarkerNodeObject;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.MarkerConnectorPoint;

/**
 * A layer holds marker objects.
 * @author Team
 *
 */
public class MarkerLayer extends LogLayer implements Renderable{

	private ArrayList<CurveMarker> markerList = new ArrayList<CurveMarker>();

	private ArrayList markerFillList = new ArrayList();

	private ArrayList markerLithologyList = new ArrayList();

	private AxisCurveContainerView parentAxisCurveContainerView = null;
	
	public MarkerLayer(AxisCurveContainerView acc)
	{
		this.parentAxisCurveContainerView = acc;
	}

	public void draw(Graphics2D g2d, Rectangle rect) {
		print(g2d, rect, 1, 1);
	}
	/**
	 * Prints the renderable content into given graphics in given region with ratio.
	 * @param g2d given graphics.
	 * @param rect given region.
	 * @param rateWidth Ratio of width.
	 * @param rateHeight Ratio of height.
	 */
	public void print(Graphics2D g2d, Rectangle rect, float rateWidth, float rateHeight){
		CurveView cv = this.parentAxisCurveContainerView.getCurveView();

		// Fill
		if (cv.isMarkerRedrawFlag()) {
			reSetMarker( cv,cv.getWidth());
		}
		if (cv.isShowFill()) {
			AffineTransform af = AffineTransform.getScaleInstance(rateWidth, rateHeight);
			for (int i = 0; i < markerFillList.size(); i++) {
				MarkerFill mf = (MarkerFill) markerFillList.get(i);
				if(mf.getCm1().isVisible() == false){
					continue;
				}
				if(mf.getCm2().isVisible() == false){
					continue;
				}
				if (cv.isFillRedrawFlag() || cv.isCurveRedrawFlag()) {
					mf.constructFill(cv);
				}
				g2d.setColor(mf.getFillColor());
				g2d.fill(af.createTransformedShape(mf.getMyFill()));
			}
			for (int i = 0; i < markerLithologyList.size(); i++) {
				MarkerLithology ml = (MarkerLithology) markerLithologyList.get(i);
				if(ml.getCm1().isVisible() == false){
					continue;
				}
				if(ml.getCm2().isVisible() == false){
					continue;
				}
				for(int m=0 ; m<cv.getPanelOrder().getOrders().size() ; m++){
					if(cv.getPanelOrder().getPanelKind(m) == PanelOrder.CURVE){
						CurvePanel cp = (CurvePanel)cv.getPanelOrder().getPanel(m);
						for(int n=0 ; n<cv.getCurveLayer().getCurveFillBesideList().size() ; n++){
							CurveFillBeside cfb = (CurveFillBeside)cv.getCurveLayer().getCurveFillBesideList().get(n);
							if(cfb.getCp() == cp){
								ml.setLeftOrRight(cfb.getLeftOrRight());
								break;
							}
						}
						ml.setOwner(cp);
					}else if(cv.getPanelOrder().getPanelKind(m) == PanelOrder.GROUP){
						CurveGroup cg = (CurveGroup)cv.getPanelOrder().getPanel(m);
						ml.setLeftOrRight(ConstantVariableList.FILL_ALL);
						ml.setOwner(cg);
					}else{
						continue;
					}

					ml.constructFill(cv,rateWidth, rateHeight);

					if(ml.getMyFill() != null){
						TexturePaint texturePaint =
							ZoneProp.getWellLogPatterns()[ml.getWhichtexture()];
						Shape fs = (ml.getMyFill());
						g2d.setPaint(texturePaint);
						g2d.fill(fs);
						g2d.setColor(ml.getFillColor());
						g2d.setXORMode(Color.white);
						g2d.fill(fs);
					}
				}
			}
		}

	}

	public void addMarkerToList(CurveMarker cm)
	{
		CurveMarker curveMarker = null;
		for(int i=0;i<this.markerList.size();i++){
			curveMarker = (CurveMarker)this.markerList.get(i);
			if(curveMarker.getMarkerName().equals(cm.getMarkerName())){
				this.markerList.remove(i);
				break;
			}
		}
		this.markerList.add(cm);
	}

	public CurveMarker removeMarkerToListByName(String markerName)
	{
		CurveMarker curveMarker = null;
		for(int i=0;i<this.markerList.size();i++){
			curveMarker = (CurveMarker)this.markerList.get(i);
			if(curveMarker.getMarkerName().equals(markerName)){
				this.markerList.remove(i);
				break;
			}
		}
		return curveMarker;
	}

	//if has marker select,return true
	public int setSelectMarker(Point p)
	{
		for (int i = 0; i < this.markerList.size(); i++) {
			if (p.getY() - ((CurveMarker) (markerList.get(i))).getYHeight() < 4
					&& p.getY()
							- ((CurveMarker) (markerList.get(i))).getYHeight() > -4)
			{
//				this.selectMarker = i;
				return i;
			}
		}
		return -1;
	}

	public void drawMarker(Graphics2D g2, CurveView cv, int width,
			float rateWidth, float rateHeight) {
		CurveMarker curveMarker = null;
		for (int i = 0; i < markerList.size(); i++) {
			curveMarker = (CurveMarker) (markerList.get(i));
			if(!curveMarker.isVisible()){
				continue;
			}
			g2.setStroke(ComboBoxRender.getStroke(curveMarker.getLineStyle(),
					curveMarker.getMarkerWidth()));
			g2.setColor(curveMarker.getMarkerColor());
			if(curveMarker.isSelected())
			{
				g2.setStroke(ComboBoxRender.getStroke(curveMarker.getLineStyle(),
						(float)(curveMarker.getMarkerWidth()+1)));
			}
			//g2.drawLine(0, curveMarker.getYHeight(),Length,curveMarker.getYHeight());
			g2.drawLine(0, (int)(curveMarker.getYHeight()*rateHeight),
					(int)(width*rateWidth),(int)(curveMarker.getYHeight()*rateHeight));
		}
	}

	public void reSetMarker(CurveView cv, int Length){
		CurveMarker curveMarker = null;
		for (int i = 0; i < markerList.size(); i++) {
			curveMarker = (CurveMarker) (markerList.get(i));
			if(!curveMarker.isVisible()){
				continue;
			}
			curveMarker.resetMarker(cv); //re translate marker
			curveMarker.setXLenth(Length);
		}
	}

	public Element saveXML(Document doc){
		Element xmlMarkerLayer = doc.createElement("MarkerLayer");
		Element xmlMarkerList = doc.createElement("MarkerList");
		for(int i = 0 ; i < this.getMarkerList().size(); i++){
			CurveMarker curveMarker = (CurveMarker) this.getMarkerList().get(i);
			xmlMarkerList.appendChild(curveMarker.saveXML(doc,i));
		}
		xmlMarkerLayer.appendChild(xmlMarkerList);

		Element xmlMarkerFillList = doc.createElement("MarkerFillList");
		for(int i = 0 ; i < this.getMarkerFillList().size(); i++){
			MarkerFill markerFill = (MarkerFill) this.getMarkerFillList().get(i);
			xmlMarkerFillList.appendChild(markerFill.saveXML(doc,i));
		}
		xmlMarkerLayer.appendChild(xmlMarkerFillList);

		Element xmlMarkerLithologyList = doc.createElement("MarkerLithologyList");
		for(int i = 0 ; i < this.getMarkerLithologyList().size(); i++){
			MarkerLithology markerLithology = (MarkerLithology) this.getMarkerLithologyList().get(i);
			xmlMarkerLithologyList.appendChild(markerLithology.saveXML(doc,i));
		}
		xmlMarkerLayer.appendChild(xmlMarkerLithologyList);

		return xmlMarkerLayer;
	}

	@SuppressWarnings("unchecked")
	public void loadXML(Element xmlMarkerLayer, CurveView cv){
		Element xmlMarkerList = (Element) xmlMarkerLayer.getElementsByTagName("MarkerList").item(0);
		NodeList nodeList = null;
		nodeList = xmlMarkerList.getElementsByTagName("CurveMarker");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlCurveMarker = (Element) nodeList.item(i);
			CurveMarker curveMarker = new CurveMarker(xmlCurveMarker.getAttribute("name"),
					Float.parseFloat(xmlCurveMarker.getAttribute("depth")),
					new Color(Integer.parseInt(xmlCurveMarker.getAttribute("color"))));
			curveMarker.loadXML(xmlCurveMarker,cv);
			this.addMarkerToList(curveMarker);
			String wellName = cv.getParentAxisCurveContainer().getWellName();
       		DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().addWellTopNode(wellName,
					            curveMarker.getMarkerName(),ProjectTreeMarkerNodeObject.WELL_NAME,true);
       		MarkerInfo info = new MarkerInfo(wellName, curveMarker.getMarkerName(), curveMarker.getMarkerDepth());
       		ViewCoreOperator.addMarkerInfo(info);
		}

		Element xmlMarkerFillList = (Element) xmlMarkerLayer.getElementsByTagName("MarkerFillList").item(0);
		nodeList = null;
		nodeList = xmlMarkerFillList.getElementsByTagName("MarkerFill");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlMarkerFill = (Element) nodeList.item(i);
			MarkerFill markerFill = new MarkerFill();
			markerFill.loadXML(xmlMarkerFill,this);
			this.getMarkerFillList().add(markerFill);
		}

		Element xmlMarkerLithologyList = (Element) xmlMarkerLayer.getElementsByTagName("MarkerLithologyList").item(0);
		nodeList = null;
		nodeList = xmlMarkerLithologyList.getElementsByTagName("MarkerLithology");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlMarkerLithology = (Element) nodeList.item(i);
			MarkerLithology markerLithology = new MarkerLithology();
			markerLithology.loadXML(xmlMarkerLithology,this);
			this.getMarkerLithologyList().add(markerLithology);
		}
	}

	public CurveMarker getSelectMarker() {
		return (CurveMarker) getMarkerSelectedList().get(0);
	}
	public ArrayList getMarkerList() {
		return markerList;
	}
	@SuppressWarnings("unchecked")
	public void setSelectMarker(int i,boolean resetOthers)
	{
		if(resetOthers == true)
		{
			for(int j = 0 ;j<this.getMarkerList().size();j++)
			{
				((CurveMarker)this.getMarkerList().get(j)).setSelected(false);
			}
			//deal the buffer
			this.getParentAxisCurveContainerView().getParentContainer().getMarkerBuffer().clear();
		}
		if(i != -1)
		{
			((CurveMarker)this.getMarkerList().get(i)).setSelected(true);
			MarkerConnectorPoint mcl = new MarkerConnectorPoint();
			mcl.setCm(((CurveMarker)this.getMarkerList().get(i)));
			mcl.setAcc(parentAxisCurveContainerView);
			this.getParentAxisCurveContainerView().getParentContainer().getMarkerBuffer().add(mcl);
		}
		//acc.getCurveView().repaintFather();
	}
	@SuppressWarnings("unchecked")
	public ArrayList getMarkerSelectedList()
	{
		ArrayList selectedlist = new ArrayList();
		for(int j = 0 ;j<this.getMarkerList().size();j++)
		{
			if(((CurveMarker)this.getMarkerList().get(j)).isSelected())
			{
				selectedlist.add(this.getMarkerList().get(j));
			}
		}
		return selectedlist;
	}
	public ArrayList getMarkerFillList() {
		return markerFillList;
	}
	public void setMarkerFillList(ArrayList markerFillList) {
		this.markerFillList = markerFillList;
	}
	public boolean selectMarkerFill(Point p){
		int sequenceNumber = -1;
		ArrayList list = null;

		list = this.getMarkerFillList();
		for(int i = 0; i < list.size();i++)
		{
			if(((MarkerFill)list.get(i)).getMyFill()!=null&&
					((MarkerFill)list.get(i)).getMyFill().contains(p))
				sequenceNumber = i;
			((MarkerFill)list.get(i)).setSelected(false);
		}
		if(sequenceNumber != -1){
			((MarkerFill)list.get(sequenceNumber)).setSelected(true);
		}
		return (sequenceNumber == -1)?false:true;
	}
	public boolean selectMarkerLithology(Point p){
		int sequenceNumber = -1;
		ArrayList list = null;

		list = this.getMarkerLithologyList();
		for(int i = 0; i < list.size();i++)
		{
			if(((MarkerLithology)list.get(i)).getMyFill()!=null&&
					((MarkerLithology)list.get(i)).getMyFill().contains(p))
				sequenceNumber = i;
			((MarkerLithology)list.get(i)).setSelected(false);
		}
		if(sequenceNumber != -1){
			((MarkerLithology)list.get(sequenceNumber)).setSelected(true);
		}
		return (sequenceNumber == -1)?false:true;
	}
	public MarkerFill getMarkerFillSelected(){
		MarkerFill mf = new MarkerFill();
		for(int i=0;i<this.markerFillList.size();i++){
			if( ((MarkerFill)(this.markerFillList.get(i))).isSelected())
			{
				mf = (MarkerFill)this.markerFillList.get(i);
			}
		}
		return mf;
	}
	public MarkerLithology getMarkerLithologySelected(){
		MarkerLithology ml = new MarkerLithology();
		for(int i=0;i<this.markerLithologyList.size();i++){
			if( ((MarkerLithology)(this.markerLithologyList.get(i))).isSelected())
			{
				ml = (MarkerLithology)this.markerLithologyList.get(i);
			}
		}
		return ml;
	}
	public ArrayList getMarkerLithologyList() {
		return markerLithologyList;
	}
	public void setMarkerLithologyList(ArrayList markerLithologyList) {
		this.markerLithologyList = markerLithologyList;
	}

	public AxisCurveContainerView getParentAxisCurveContainerView() {
		return parentAxisCurveContainerView;
	}

	/* (non-Javadoc)
	 * @see com.gwsys.welllog.core.Renderable#updateBorderByPoints(java.awt.Point, int, int)
	 */
	public void updateBorderByPoints(Point point, int top, int bottom) {

	}
	
}
