/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.Color;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.fill.PanelLithology;
import com.gwsys.welllog.view.yaxis.DepthConverter;

public class LithologyObject {

	private LithologyPanel parent;
	private String text="";
	private int orientation =LithologyObject.ORIENTATION_HORIZONTAL;
	private float depthStart = 0;
	private float depthEnd = 0;
	private Color fillColor = Color.BLACK;
	private int colorIndex = 0;
	private PanelLithology panelLithology;

	private int yStart = 0 ;
	private int yEnd = 0 ;

	public static final int ORIENTATION_HORIZONTAL =1;
	public static final int ORIENTATION_VERTICAL =2;

	public LithologyObject(LithologyPanel parent){
		this.parent = parent;
	}

	public PanelLithology getPanelLithology() {
		return panelLithology;
	}
	public void setPanelLithology(PanelLithology panelLithology) {
		this.panelLithology = panelLithology;
	}

	public int getOrientation() {
		return orientation;
	}
	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public float getDepthEnd() {
		return depthEnd;
	}
	public void setDepthEnd(float depthEnd) {
		this.depthEnd = depthEnd;
	}
	public float getDepthStart() {
		return depthStart;
	}
	public void setDepthStart(float depthStart) {
		this.depthStart = depthStart;
	}
	public Color getFillColor() {
		return fillColor;
	}
	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

	public int getColorIndex() {
		return colorIndex;
	}

	public void setColorIndex(int colorIndex) {
		this.colorIndex = colorIndex;
	}

	public void setStart(){
		CurveView curveView = this.parent.getParent();
		float ySetTopDepth = curveView.getCurveLayer().getYSetTopDepth();
		float yHeight = curveView.getHeight();
		float ySetBottomDepth = curveView.getCurveLayer().getYSetBottomDepth();
		float fStart = 0;
		if (curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_MD)){
			fStart = depthStart;
		}else if(curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVD)){
			DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
			fStart = timeToDepthConverter.toDepth(depthStart,TrackTypeList.MD_TO_TVD);
		}else if(curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)){
			DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
			fStart = timeToDepthConverter.toDepth(depthStart,TrackTypeList.MD_TO_TVDSS);
		}
		yStart = (int) (yHeight * (fStart - ySetTopDepth) / (ySetBottomDepth - ySetTopDepth));
	}

	public void setEnd(){
		CurveView curveView = this.parent.getParent();
		float ySetTopDepth = curveView.getCurveLayer().getYSetTopDepth();
		float yHeight = curveView.getHeight();
		float ySetBottomDepth = curveView.getCurveLayer().getYSetBottomDepth();
		float fEnd = 0;
		if (curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_MD)){
			fEnd = depthEnd;
		}else if(curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVD)){
			DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
			fEnd = timeToDepthConverter.toDepth(depthEnd,TrackTypeList.MD_TO_TVD);
		}else if(curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)){
			DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
			fEnd = timeToDepthConverter.toDepth(depthEnd,TrackTypeList.MD_TO_TVDSS);
		}
		yEnd = (int) (yHeight * (fEnd - ySetTopDepth) / (ySetBottomDepth - ySetTopDepth));
	}

	public int getYEnd() {
		return yEnd;
	}

	public int getYStart() {
		return yStart;
	}

	public Element saveXML(Document doc, Integer id){
		Element xmlLithologyObject = doc.createElement("LithologyObject"+id);
		xmlLithologyObject.setAttribute("text",this.getText());
		xmlLithologyObject.setAttribute("orientation",Integer.toString(this.getOrientation()));
		xmlLithologyObject.setAttribute("depthStart",Float.toString(this.getDepthStart()));
		xmlLithologyObject.setAttribute("depthEnd",Float.toString(this.getDepthEnd()));
		xmlLithologyObject.setAttribute("fillColor",Integer.toString(this.getFillColor().getRGB()));
		xmlLithologyObject.setAttribute("colorIndex",Integer.toString(this.getColorIndex()));
		xmlLithologyObject.setAttribute("whichtexture",Integer.toString(this.getPanelLithology().getWhichtexture()));
		xmlLithologyObject.setAttribute("selected", Boolean.toString(this.getPanelLithology().isSelected()));
		return xmlLithologyObject;
	}

	public void loadXML(Element xmlLithologyObject){
		this.setText(xmlLithologyObject.getAttribute("text"));
		this.setOrientation(Integer.parseInt(xmlLithologyObject.getAttribute("orientation")));
		this.setDepthStart(Float.parseFloat(xmlLithologyObject.getAttribute("depthStart")));
		this.setDepthEnd(Float.parseFloat(xmlLithologyObject.getAttribute("depthEnd")));
		this.setFillColor(new Color(Integer.parseInt(xmlLithologyObject.getAttribute("fillColor"))));
		this.setColorIndex(Integer.parseInt(xmlLithologyObject.getAttribute("colorIndex")));

		this.setPanelLithology(new PanelLithology());
		this.getPanelLithology().setLithologyObject(this);
		this.getPanelLithology().setWhichtexture(Integer.parseInt(xmlLithologyObject.getAttribute("whichtexture")));
		this.getPanelLithology().setSelected(ConversionClass.stringToBoolean(xmlLithologyObject.getAttribute("selected")));
		this.getPanelLithology().constructFill(this.parent, 0, 1,1);
	}
}
