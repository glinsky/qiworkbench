/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.view.curve;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;

import com.gwsys.welllog.core.Renderable;
import com.gwsys.welllog.core.Selectable;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.dialogs.EditBesideCurveDialog;
import com.gwsys.welllog.view.dialogs.EditCurveDialog;
import com.gwsys.welllog.view.dialogs.EditFillMarkerDialog;
import com.gwsys.welllog.view.dialogs.EditLithologyBesideDialog;
import com.gwsys.welllog.view.dialogs.EditLithologyMarkerDialog;
import com.gwsys.welllog.view.dialogs.EditLithologyTopDialog;
import com.gwsys.welllog.view.dialogs.EditMarkerDialog;
import com.gwsys.welllog.view.dialogs.EditTrackDialog;
import com.gwsys.welllog.view.fill.MarkerFill;
import com.gwsys.welllog.view.fill.MarkerLithology;
import com.gwsys.welllog.view.projecttree.ProjectTreeMarkerNodeObject;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.MarkerConnectorPoint;
import com.gwsys.welllog.view.track.TrackContainerView;
import com.gwsys.welllog.view.track.XAxisView;
import com.gwsys.welllog.view.track.YAxisView;
import com.gwsys.welllog.view.yaxis.DepthConverter;

/**
 * Event handler for CurveView
 *
 * @author
 *
 */
public class CurveViewController implements ActionListener, MouseListener,
		MouseMotionListener, ComponentListener, MouseWheelListener {
	private CurveView forpass_curveView;

	private CurveMarker selectMarker = null;

	private boolean isDragging, zoneCanMove = false, canDrag = false, isDepthEditing = false;

	private int yi, yf, zTop, zBase, currentPoint;	// added by Alex

	private Point zonePoint = null;		// added by Alex
	
	private float currentDepth = 0, currentData = 0, firstData = 0, selectedDepth = 0;		// added by Alex
	
	private String selectedCurveHashmap;
	
	public void mouseClicked(MouseEvent e) {
		CurveView curveView = (CurveView) e.getSource();

		if (e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e)) {
			EditTrackDialog trackPropertyDialog = new EditTrackDialog();
			trackPropertyDialog.setVisible(true);
		}

		// added by Mario -- edit rock physics zones
		// ****************************************
		if (curveView.getZoneLayer() instanceof Selectable)
			((Selectable) curveView.getZoneLayer()).selectByPoint(e.getPoint());
	}

	public void mouseEntered(MouseEvent e) {
		CurveView curveView = (CurveView) e.getSource();
		if (curveView.isCreateMarker()){
			Image image = IconResource.getInstance().getImage("marker.gif");

			curveView.setCursor( curveView.getToolkit().createCustomCursor(image,
					new Point(1, 1), ConstantVariableList.CURSOR_MARKER));
		}
	}

	public void mouseExited(MouseEvent e) {
		DataBuffer.getJMainFrame().setCurveInfo("");
	}

	public void mousePressed(MouseEvent e) {
		CurveView curveView = (CurveView) e.getSource();

		// added by Mario -- define rock physics zones
		if ((e.isAltDown() && curveView.isZoneCreationAllowed()) || curveView.isCreateZone()) {
			curveView.setTmpRectStart(new Point(e.getX(), e.getY()));
			curveView.setUpdatingZone(false);
			canDrag = true;
			return;
		} else
			canDrag = false;

		// added by Alex. Moving zone feature
		if (zoneCanMove)	{
			Point p = ((Selectable) curveView.getZoneLayer()).selectBorderPoint(e.getPoint());
			zTop = p.x;
			zBase = p.y;
			zonePoint = e.getPoint();
			curveView.setUpdatingZone(true);
			curveView.setShowTmpRect(true);
			currentPoint = e.getY();
			canDrag = true;
			return;
		}

		// added by Alex. Changing thickness of zone
		if (curveView.getZoneLayer() instanceof Selectable)	{
			Point p = ((Selectable) curveView.getZoneLayer()).selectBorderPoint(e.getPoint());

			if (p != null)	{
				zonePoint = e.getPoint();

				if (e.getY() == p.x)
					yi = p.y;
				else if (e.getY() >= p.y-2 && e.getY() <= p.y+2) //offset
					yi = p.x;
				else
					return;

				curveView.setTmpRectStart(new Point(0, yi));
				curveView.setTmpRectEnd(new Point(curveView.getWidth(), e.getY()));
				canDrag = true;
				curveView.setUpdatingZone(true);
				curveView.repaint();
				return;
			}

		}

		// new By Lili
		AxisCurveContainerView accurveView = curveView
				.getParentAxisCurveContainer();
		accurveView.setDragPoint(e.getPoint());
		// want to add a marker?
		if (curveView.isCreateMarker()) {
			curveView.setCursor(Cursor.getDefaultCursor());
			EditMarkerDialog createMarkerDialog = new EditMarkerDialog(
					curveView, e.getPoint());
			createMarkerDialog.setVisible(true);
			curveView.setCreateMarker(false);
			return;
		}
		// want to edit a marker?
		int whichmarker = curveView.getMarkerLayer().setSelectMarker(
				e.getPoint());
		// June 16
		boolean resetOthers = false;
		if (DataBuffer.getJMainFrame().isMakingCable()
				|| DataBuffer.getJMainFrame().isMakingMarkerFill()
				|| DataBuffer.getJMainFrame().isMakingMarkerLith()) {
			resetOthers = true;
		}

		curveView.getMarkerLayer().setSelectMarker(whichmarker, !resetOthers);
		if (whichmarker != -1) {
			ViewCoreOperator.selectTrack(curveView
					.getParentAxisCurveContainer().getWellName());
			selectMarker = (CurveMarker) curveView.getMarkerLayer()
					.getMarkerList().get(whichmarker);
			if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
				JPopupMenu popup = new JPopupMenu();

				JMenuItem editItem = new JMenuItem("Settings...");
				editItem.addActionListener(this);
				editItem.setActionCommand("MarkerSetting");
				popup.add(editItem);

				JMenuItem deleteItem = new JMenuItem("Delete");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("MarkerDelete");
				popup.add(deleteItem);

				popup.show(curveView, e.getX(), e.getY());
			}
			return;
		} else {
			selectMarker = null;
		}

		// before a drag, clear it's transfer
		ViewCoreOperator.selectTrack(curveView.getParentAxisCurveContainer()
				.getWellName());
		curveView.clearTransferCurveName();
		String curveHashmapKey = curveView.getCurveLayer().selectCurve(
				e.getPoint());
		if (curveHashmapKey == null) {
			String[] curveAndGroup = curveView.getCurveLayer()
					.selectCurveUnderGroup(e.getPoint());
			if (curveAndGroup != null) {
				String groupName = curveAndGroup[0];
				curveHashmapKey = curveAndGroup[1];
				ViewCoreOperator.selectCurveUnderGroup(curveView
						.getParentAxisCurveContainer().getWellName(),
						groupName, curveHashmapKey);
			}
		} else {
			// select depth
			if (e.isControlDown() && e.getButton() == MouseEvent.BUTTON1)	{
				firstData = getCurrentData();	// added Jan 2007
				selectedDepth = getCurrentDepth();
				setSelectedCurveHashmap(curveHashmapKey);
				isDepthEditing = true;
			}
			else	{
				ViewCoreOperator.selectCurve(curveHashmapKey, curveView
					.getParentAxisCurveContainer().getWellName(), !e
					.isControlDown());
			}
		}
		if (curveView.getCurveLayer().getCurveSelectedList().size() > 0) // if
																			// has
																			// clicked
																			// one
		{
			// reset the transfercurvename,to transfer
			curveView.resetTransferCurveName(curveHashmapKey);
			if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
				forpass_curveView = curveView;

				JPopupMenu popup = new JPopupMenu();

				JMenuItem editItem = new JMenuItem("Settings...");
				editItem.addActionListener(this);
				editItem.setActionCommand("CurveSetting");
				popup.add(editItem);

				JMenuItem deleteItem = new JMenuItem("Delete");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("CurveDelete");
				popup.add(deleteItem);
				
				// Export changes made in depth values. Alex, January 2007
				//String wellname = curveHashmapKey.split(OperationClass.hashMapNameSeparator)[0];
				
				JMenuItem exportItem = new JMenuItem("Export changes");				
				exportItem.setEnabled(OperationClass.hasThisLogChanged(curveHashmapKey));
				exportItem.addActionListener(this);
				exportItem.setActionCommand("exportChanges");
				popup.add(exportItem);

				popup.show(curveView, e.getX(), e.getY());
			}
						
			return;
		}

		if (curveView.getCurveLayer().selectCurveLithology(e.getPoint())) {
			if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
				if (curveView.getCurveLayer().getLayerType().equals(
						TrackTypeList.REALIZATION_TOP)) {
					EditLithologyTopDialog curveLithologyDialog = new EditLithologyTopDialog(
							curveView, curveView.getCurveLayer()
									.getCurveLithologyTopSelected());
					curveLithologyDialog.setVisible(true);
				} else {
					EditLithologyBesideDialog curveLithologyBesideDialog = new EditLithologyBesideDialog(
							curveView, curveView.getCurveLayer()
									.getCurveLithologyBesideSelected());
					curveLithologyBesideDialog.setVisible(true);
				}
			}
			return;
		}
		if (curveView.getMarkerLayer().selectMarkerLithology(e.getPoint())) {
			if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
				EditLithologyMarkerDialog elmd = new EditLithologyMarkerDialog(
						curveView, curveView.getMarkerLayer()
								.getMarkerLithologySelected());
				elmd.setVisible(true);
			}
			return;
		}
		if (curveView.getMarkerLayer().selectMarkerFill(e.getPoint())) {
			if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
				EditFillMarkerDialog efmd = new EditFillMarkerDialog(curveView,
						curveView.getMarkerLayer().getMarkerFillSelected());
				efmd.setVisible(true);
			}
			return;
		}


	}

	@SuppressWarnings("unchecked")
	public void mouseReleased(MouseEvent e) {

		CurveView curveView = (CurveView) e.getSource();
		curveView.setShowTmpRect(false);

		if (isDragging) {
			if (e.isAltDown() || curveView.isCreateZone()) {
				if (curveView.getZoneLayer() instanceof Renderable)
					((Renderable) curveView.getZoneLayer()).draw(null, null);
			}
			if (curveView.isUpdatingZone())	{
				curveView.setUpdatingZone(false);
				zTop = Math.min(curveView.getTmpRectStart().y, curveView.getTmpRectEnd().y);
				zBase = Math.max(curveView.getTmpRectStart().y, curveView.getTmpRectEnd().y);

				if (curveView.getZoneLayer() instanceof Renderable)	{
					((Renderable) curveView.getZoneLayer()).updateBorderByPoints(zonePoint, zTop, zBase);
					zonePoint = null;
				}
			}
			curveView.setCreateZone(false);
			curveView.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			//curveView.setShowTmpRect(false);
			isDragging = false;
			canDrag = false;
		}
		
		// added by Alex. December 2006
		if (isDepthEditing)	{
			// call to log editing method
			isDepthEditing = false;
			float newData = getCurrentData();
			
			if (firstData == newData)
				return;
			
			if (getSelectedCurveHashmap() != null)
				OperationClass.tryToRewriteLasData(getSelectedCurveHashmap(), selectedDepth, newData);
			
			CurveData cd = (CurveData) DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(getSelectedCurveHashmap());
			curveView.getCurveLayer().addCurveToList(new CurvePanel(cd));
			
			//isDepthEditing = false;
		}
		
		curveView.repaint();
		//curveView.setMovingZone(false);
	}

	public void mouseDragged(MouseEvent e) {
		// drag marker
		CurveView curveView = (CurveView) e.getSource();
		
		// added by Alex. if user is editing log 
		if (isDepthEditing)	{
			getXYPosition(curveView, e.getX(), e.getY());
			//newData = getCurrentData();
			return;
		}
				
		// added by Mario -- define rock physics zones
		if (canDrag) {
			isDragging = true;
		}

		// added by Alex. Changing thickness of zone
		if (!e.isAltDown() && curveView.isUpdatingZone() && isDragging )	{
			// for moving zone
			if (zoneCanMove)	{
				int diff = 0;

				if (e.getY() > currentPoint)	{
					diff = e.getY() - currentPoint;
					zTop += diff;
					zBase += diff;
				}
				else if (e.getY() < currentPoint)	{
					diff = currentPoint - e.getY();
					zTop -= diff;
					zBase -= diff;
				}

				currentPoint = e.getY();
				curveView.setTmpRectStart(new Point(0, zTop));
				curveView.setTmpRectEnd(new Point(curveView.getWidth(), zBase));
				curveView.repaint();
				return;
			}

			yf = e.getPoint().y;
			curveView.setShowTmpRect(true);
			curveView.setTmpRectEnd(new Point(curveView.getWidth(), yf));
			curveView.repaint();
			return;
		}

		if ((e.isAltDown() || curveView.isCreateZone()) && isDragging) {
			curveView.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
			curveView.setShowTmpRect(true);
			curveView.setTmpRectEnd(new Point(e.getPoint().x, e.getPoint().y));
			curveView.repaint();
		} else {
			curveView.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			isDragging = false;
			canDrag = false;
			curveView.setShowTmpRect(false);
			curveView.repaint();

			if (selectMarker != null) {

				String yType = curveView.getParentAxisCurveContainer()
						.getYAxisView().getYType();
				float markerDepth = 0.0f;
				if (yType.equals(TrackTypeList.Y_TIME)) {
					markerDepth = EditMarkerDialog.changeHeightToDepth(e
							.getPoint().getY(), curveView)
							/ curveView.getParentAxisCurveContainer()
									.getVelocityValue();
				} else if (yType.equals(TrackTypeList.Y_DEPTH_MD)) {
					markerDepth = EditMarkerDialog.changeHeightToDepth(e
							.getPoint().getY(), curveView);
				} else if (yType.equals(TrackTypeList.Y_DEPTH_TVD)) {
					markerDepth = EditMarkerDialog.changeHeightToDepth(e
							.getPoint().getY(), curveView);
					DepthConverter timeToDepthConverter = curveView
							.getParentAxisCurveContainer()
							.getMDDepthConverter();
					markerDepth = timeToDepthConverter.toDepth(markerDepth,
							TrackTypeList.TVD_TO_MD);
				} else if (yType.equals(TrackTypeList.Y_DEPTH_TVDSS)) {
					markerDepth = EditMarkerDialog.changeHeightToDepth(e
							.getPoint().getY(), curveView);
					markerDepth = -markerDepth;
					DepthConverter timeToDepthConverter = curveView
							.getParentAxisCurveContainer()
							.getMDDepthConverter();
					markerDepth = timeToDepthConverter.toDepth(markerDepth,
							TrackTypeList.TVDSS_TO_MD);
				}

				selectMarker.setMarkerDepth(markerDepth);
				selectMarker.setYHeight(curveView);

				curveView.setFillRedrawFlag(true);
				curveView.repaintFather();
				DataBuffer.getJMainFrame().getMainBuffer()
						.getInternalFrameView().getTrackContainer().repaint();
				return;
			}

			// drag out
			if (((CurveView) e.getSource()).getCurveLayer()
					.getCurveSelectedList().isEmpty()) {
				if (e.getModifiers() == MouseEvent.BUTTON1_MASK) {
					scrollView(curveView, e.getPoint());
				}
			} else {
				JComponent c = (JComponent) e.getSource();
				TransferHandler handler = c.getTransferHandler();
				handler.exportAsDrag(c, e, TransferHandler.COPY);
			}
		}
	}

	public void mouseMoved(MouseEvent e) {
		CurveView curveView = ((CurveView) e.getSource());
		//PanelOrder panelOrder = curveView.getPanelOrder();
		String showText = "";
		if (!curveView.isCreateMarker())
		// added by Alex. Changing thickness of zone
			curveView.setCursor(Cursor.getDefaultCursor());

		if (curveView.isCreateZone())
			curveView.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));

		if (curveView.getZoneLayer() instanceof Selectable)	{
			Point p = ((Selectable) curveView.getZoneLayer()).selectBorderPoint(e.getPoint());
			zoneCanMove = false;

			if (p != null)	{
				if (e.getY() == p.x){
					curveView.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR ));
				}
				else if (e.getY() >= p.y-2 && e.getY() <= p.y+2){
					curveView.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR ));
				}
				// moving zone feature
				else if (e.getY() > p.x+2 && e.getY() < p.y-2)	{
					curveView.setCursor(new Cursor(Cursor.HAND_CURSOR));
					zoneCanMove = true;
				}

				curveView.repaint();
			}
		}		
		
		showText = getXYPosition(curveView, e.getX(), e.getY());
		DataBuffer.getJMainFrame().setCurveInfo(showText);
	}

	public void componentHidden(ComponentEvent e) {

	}

	public void componentMoved(ComponentEvent e) {
		CurveView curveView = ((CurveView) e.getSource());
		XAxisView xAxisView = curveView.getParentAxisCurveContainer().getXAxisViewPanel();
		xAxisView.setBounds(curveView.getX(),xAxisView.getY(),xAxisView.getWidth(),xAxisView.getHeight());
	}

	public void componentResized(ComponentEvent e) {
		CurveView curveView = ((CurveView) e.getSource());
		curveView.setMarkerRedrawFlag(true);
		curveView.setCurveRedrawFlag(true);
		curveView.setFillRedrawFlag(true);
		curveView.setZoneRedrawFlag(true); // added by mario
		// June 16
		if (curveView.getOriginalHeight() != curveView.getHeight()) {
			// System.out.println("OriginalHeight:"+curveView.getOriginalHeight()+
			// " Height:"+curveView.getHeight());
			curveView.setOriginalHeight(curveView.getHeight());
			curveView.getParentYAxisView().setReComputeFlag(true);
		}
		XAxisView xAxisView = curveView.getParentAxisCurveContainer().getXAxisViewPanel();
		xAxisView.setPreferredSize(new Dimension(curveView.getWidth(),xAxisView.getHeight()));
		curveView.repaintFather();

	}

	public void componentShown(ComponentEvent e) {

	}


	public void mouseWheelMoved(MouseWheelEvent e) {
		scrollView((CurveView) e.getSource(), 30*e.getWheelRotation());
	}

	public void actionPerformed(ActionEvent e) {
		CurveView cv = DataBuffer.getJMainFrame().getMainBuffer()
				.getInternalFrameView().getTrackContainer()
				.getSelectedAxisCurveContainer().getCurveView();
		// Curve
		if (e.getActionCommand().startsWith("Curve")) {
			if(cv.getCurveLayer().getCurveSelectedList().size() == 0){
				return;
			}
			CurvePanel cp = (CurvePanel) cv.getCurveLayer()
					.getCurveSelectedList().get(0);
			if (cv.getCurveLayer().getLayerType().equals(
					TrackTypeList.REALIZATION_TOP)) {
				EditCurveDialog editCurveDialog = new EditCurveDialog(
						forpass_curveView, DataBuffer.getJMainFrame());
				if (e.getActionCommand().equals("CurveSetting")) {
					editCurveDialog.setVisible(true);
				} else if (e.getActionCommand().equals("CurveDelete")) {
					editCurveDialog.remove();
				}
			} else {
				if (cp.getCg() == null) {
					EditBesideCurveDialog editCurveDialog = new EditBesideCurveDialog(
							forpass_curveView, DataBuffer.getJMainFrame());
					if (e.getActionCommand().equals("CurveSetting")) {
						editCurveDialog.setVisible(true);
					} else if (e.getActionCommand().equals("CurveDelete")) {
						editCurveDialog.remove();
					}
				} else {
					EditCurveDialog editCurveDialog = new EditCurveDialog(
							forpass_curveView, DataBuffer.getJMainFrame());
					if (e.getActionCommand().equals("CurveSetting")) {
						editCurveDialog.setVisible(true);
					} else if (e.getActionCommand().equals("CurveDelete")) {
						editCurveDialog.remove();
					}
				}
			}
		}
		// Marker
		if (e.getActionCommand().equals("MarkerSetting")) {
			EditMarkerDialog editMarkerDialog = new EditMarkerDialog(cv,
					selectMarker);
			editMarkerDialog.setVisible(true);
		} else if (e.getActionCommand().equals("MarkerDelete")) {
			deleteMarker(cv);
		}
		// export changes in curve. Alex, January 2007
		if (e.getActionCommand().equals("exportChanges"))	{
			String curveHashkey = ((CurvePanel) cv.getCurveLayer().getCurveSelectedList().get(0)).getCd().getHashCurveName();
			
			// save changes in this Well
			if (OperationClass.exportChangesToFile(curveHashkey))
				JOptionPane.showMessageDialog(cv.getParentAxisCurveContainer(), "Changes was saved in " + curveHashkey.split(OperationClass.hashMapNameSeparator)[1]);
		}
	}

	public void deleteMarker(CurveView cv) {
		DefaultMutableTreeNode markerNode = DataBuffer.getJMainFrame()
				.getMainBuffer().getProjectTree().getWellTopNode(
						selectMarker.getMarkerName());
		ArrayList<String> wellNameList = ((ProjectTreeMarkerNodeObject) markerNode
				.getUserObject()).getWellNameList();

		for (int j = 0; j < wellNameList.size(); j++) {
			CurveMarker curveMarker = null;
			InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer()
					.getInternalFrameView();
			if (ifv != null) {
				ArrayList list = ifv.getTrackContainer()
						.getAxisCurveContainerList();
				AxisCurveContainerView track = null;
				for (int i = 0; i < list.size(); i++) {
					track = (AxisCurveContainerView) list.get(i);
					if (wellNameList.get(j).equals(track.getWellName())) {
						curveMarker = track.getCurveView().getMarkerLayer()
								.removeMarkerToListByName(
										selectMarker.getMarkerName());
						track.getCurveView().repaint();
					}
				}
			}

			TrackContainerView tcv = DataBuffer.getJMainFrame()
					.getSonJSplitPane().getRightDesktopPane()
					.getSelectedInternalFrameView().getTrackContainer();
			for (int i = 0; i < tcv.getMarkerCable1().size(); i++) {
				if (((MarkerConnectorPoint) tcv.getMarkerCable1().get(i))
						.getCm() == curveMarker) {
					tcv.getMarkerCable1().remove(i);
				}
			}
			for (int i = 0; i < tcv.getMarkerCable2().size(); i++) {
				if (((MarkerConnectorPoint) tcv.getMarkerCable2().get(i))
						.getCm() == curveMarker) {
					tcv.getMarkerCable2().remove(i);
				}
			}
		}

		// delete fill
		ArrayList ifvList = DataBuffer.getJMainFrame().getSonJSplitPane().getRightDesktopPane().getInternalFrameViewList();
		for (int i = 0; i < ifvList.size(); i++) {
			ArrayList trackList = ((InternalFrameView) ifvList.get(i)).getTrackContainer().getAxisCurveContainerList();
			for (int j = 0; j < trackList.size(); j++) {
				AxisCurveContainerView track = (AxisCurveContainerView) trackList.get(j);

				ArrayList markerFillList = track.getCurveView()
						.getMarkerLayer().getMarkerFillList();
				for (int m = 0; m < markerFillList.size(); m++) {
					if (((MarkerFill) markerFillList.get(m)).getCm1() == selectMarker)
						markerFillList.remove(m);
					else if (((MarkerFill) markerFillList.get(m)).getCm2() == selectMarker)
						markerFillList.remove(m);
				}

				markerFillList = track.getCurveView().getMarkerLayer()
						.getMarkerLithologyList();
				for (int m = 0; m < markerFillList.size(); m++) {
					if (((MarkerLithology) markerFillList.get(m)).getCm1() == selectMarker)
						markerFillList.remove(m);
					else if (((MarkerLithology) markerFillList.get(m)).getCm2() == selectMarker)
						markerFillList.remove(m);
				}
			}
		}

		selectMarker = null;
		cv.repaint();
		cv.getParentAxisCurveContainer().getParentContainer().repaint();
	}

	private void scrollView(CurveView curveView, int scrollAmount){
		AxisCurveContainerView accurveView = curveView.getParentAxisCurveContainer();
		JScrollPane asp = accurveView.getBottomScrollPane();

		JScrollBar ver = asp.getVerticalScrollBar();
		int verMax = ver.getMaximum() - asp.getHeight();
		int nowVer = ver.getValue();

		int ym = scrollAmount;
		boolean repain = false;
		if (nowVer - ym > 0 && nowVer - ym < verMax) {
			ver.setValue(nowVer - ym);
			repain = true;
		} else {
			if (nowVer - ym <= 0) {
				ver.setValue(0);
				if (nowVer > 0)
					repain = true;
			}
			if (nowVer - ym >= verMax) {
				ver.setValue(verMax);
				if (nowVer < verMax)
					repain = true;
			}
		}

		if (repain)
			DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
					.getTrackContainer().repaint();
	}

	private void scrollView(CurveView curveView, Point p){
		AxisCurveContainerView accurveView = curveView.getParentAxisCurveContainer();
		JScrollPane asp = accurveView.getBottomScrollPane();
		//JScrollBar hor = asp.getHorizontalScrollBar();
		JScrollBar ver = asp.getVerticalScrollBar();
		int verMax = ver.getMaximum() - asp.getHeight();
		//int horMax = hor.getMaximum() - asp.getWidth();
		int nowVer = ver.getValue();
		//int nowHor = hor.getValue();
		//int xm = (int) ((int) p.getX() - accurveView.getDragPoint().getX());
		int ym = (int) ((int) p.getY() - accurveView.getDragPoint()
				.getY());
		boolean repain = false;
		if (nowVer - ym > 0 && nowVer - ym < verMax) {
			ver.setValue(nowVer - ym);
			repain = true;
		} else {
			if (nowVer - ym <= 0) {
				ver.setValue(0);
				if (nowVer > 0)
					repain = true;
			}
			if (nowVer - ym >= verMax) {
				ver.setValue(verMax);
				if (nowVer < verMax)
					repain = true;
			}
		}
		/* Do not change horizontal position
		if (nowHor - xm > 0 && nowHor - xm < horMax) {
			hor.setValue(nowHor - xm);
			repain = true;
		} else {
			if (nowHor - xm <= 0) {
				hor.setValue(0);
				if (nowHor > 0)
					repain = true;
			}
			if (nowHor - xm >= horMax) {
				hor.setValue(horMax);
				if (nowHor < horMax)
					repain = true;
			}
		}*/
		if (repain)
			DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
					.getTrackContainer().repaint();
	}

	//to be used later
	@SuppressWarnings("unused")
	private void zoomView(CurveView curveView, int wheel){
		double zoomParameter = 1.5;
		YAxisView yAxisView = curveView.getParentAxisCurveContainer()
				.getYAxisView();
		int width = curveView.getWidth();
		int height = curveView.getHeight();
		int minHeight = (int) curveView.getParentAxisCurveContainer()
				.getBottomScrollPane().getVisibleRect().height;
		int newHeight = 0;
		if (wheel < 0) {
			newHeight = (int) (height * zoomParameter);
		} else if (wheel > 0) {
			newHeight = (int) (height / zoomParameter);
		}
		if (height < minHeight) {
			height = minHeight;
		}

		float depthScale = 0;
		int pixValue = curveView.getToolkit().getScreenResolution();
		float range = curveView.getParentAxisCurveContainer().getCurveLayer()
				.getYSetBottomDepth()
				- curveView.getParentAxisCurveContainer().getCurveLayer()
						.getYSetTopDepth();
		if (curveView.getParentAxisCurveContainer().getDepthUnit() == CurveData.METER
				|| curveView.getParentAxisCurveContainer().getDepthUnit() == CurveData.SECOND) {
			int trueHeight = (int) (range * ConstantVariableList.METER_TO_INCH * pixValue);
			depthScale = (float) (trueHeight / newHeight);
		} else {
			int trueHeight = (int) (range * ConstantVariableList.FOOT_TO_INCH * pixValue);
			depthScale = (float) (trueHeight / newHeight);
		}
		curveView.getParentAxisCurveContainer().setDepthScale(depthScale);

		curveView.setPreferredSize(new Dimension(width, newHeight));
		yAxisView.setPreferredSize(new Dimension(yAxisView.getWidth(),
				newHeight));
		AxisCurveContainerView acc = curveView.getParentAxisCurveContainer();
		acc.getJp().revalidate();
		acc.getJp().repaint();
	}
	
	/**
	 * set depth and value related a curve point 
	 * @param depth depth value
	 * @param data data value
	 * @author Alex
	 */
	private void setCurrentDepthAndData(float depth, float data)	{
		currentDepth = depth;
		currentData = data;
	}
	
	/**
	 * retrieves current depth
	 * @return depth
	 * @author Alex
	 */
	private float getCurrentDepth()	{
		return currentDepth;
	}
	
	/**
	 * retrieves current data
	 * @return data
	 * @author Alex
	 */
	private float getCurrentData()	{
		return currentData;
	}
	
	private void setSelectedCurveHashmap(String curveHashmapKey)	{
		this.selectedCurveHashmap = curveHashmapKey;
	}
	
	private String getSelectedCurveHashmap()	{
		return this.selectedCurveHashmap;
	}
	
	/**
	 * method used to calculate depth anda value
	 * @param curveView
	 * @param e
	 * @return text message to be shown in a status bar
	 */
	private String getXYPosition(CurveView curveView, int x, int y)	{
		PanelOrder panelOrder = curveView.getPanelOrder();
		String showText = "";
		
		float height = 0;
		if (curveView.getParentYAxisView().getYType().equals(
				TrackTypeList.Y_TIME)) {
			float ySetTopDepth = curveView.getCurveLayer().getYSetTopTime();
			float ySetBottomDepth = curveView.getCurveLayer()
					.getYSetBottomTime();
			height = ((float) y / (float) curveView.getHeight())
					* (ySetBottomDepth - ySetTopDepth) + ySetTopDepth;
		} else {
			float ySetTopDepth = curveView.getCurveLayer().getYSetTopDepth();
			float ySetBottomDepth = curveView.getCurveLayer()
					.getYSetBottomDepth();
			height = ((float) y / (float) curveView.getHeight())
					* (ySetBottomDepth - ySetTopDepth) + ySetTopDepth;
			if (curveView.getParentYAxisView().getYType().equals(
					TrackTypeList.Y_DEPTH_TVDSS)) {
				height = -height;
			}
		}
		for (int i = 0; i < panelOrder.getOrders().size(); i++) {
			if (panelOrder.getPanelKind(i) != PanelOrder.CURVE)
				continue;
			CurvePanel cp = (CurvePanel) panelOrder.getPanel(i);
			float left = panelOrder.getPanelLeft(cp);
			float right = panelOrder.getPanelRight(cp);
			if (x >= left && x <= right) {
				// calculate the position
				float max = cp.getMaxXDisplay();
				float min = cp.getMinXDisplay();
				if(cp.isLogarithm()){
					max = cp.log(max,10);
					min = cp.log(min,10);
				}
				float pos = (x - left) / (right - left) * (max - min)
						+ min;
				if(cp.isLogarithm()){
					pos = cp.pow(10,pos);
				}
				// update the status bar
				showText = cp.getCd().getCurveName() + " : " + pos + " , "
						+ height;
				
				// set depth and data
				setCurrentDepthAndData(height, pos);
				break;
			}
		}
		
		return showText;
	}
}
