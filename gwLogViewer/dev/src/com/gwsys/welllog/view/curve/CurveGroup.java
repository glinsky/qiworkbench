/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.util.ArrayList;

import com.gwsys.welllog.core.curve.ConstantVariableList;

/**
 * This represents group of curves.
 * @author Team
 *
 */
public class CurveGroup {
	private String name;
	private ArrayList curveList = new ArrayList();
	private String maxXDisplay = "";
	private String minXDisplay = "";
	private boolean setRange= false, logGrid = false;

	public CurveGroup(String name){
		this.name = name;
	}

	public ArrayList getCurveList() {
		return curveList;
	}

	public int getBesideTypeWidth(){
		if(curveList.size() == 0)
			return ConstantVariableList.BESIDETYPE_WIDTH;
		else
			return ((CurvePanel)curveList.get(0)).getBesideTypeWidth();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString(){
		String allCurve = name + ":";
		for(int i=0 ; i<curveList.size() ; i++){
			allCurve = allCurve + ((CurvePanel)curveList.get(i)).getCd().getCurveName();
		}
		return allCurve;
	}

	public String getMaxXDisplay() {
		return maxXDisplay;
	}

	public void setMaxXDisplay(String maxXDisplay) {
		this.maxXDisplay = maxXDisplay;
	}

	public String getMinXDisplay() {
		return minXDisplay;
	}

	public void setMinXDisplay(String minXDisplay) {
		this.minXDisplay = minXDisplay;
	}

	public boolean isSetRange() {
		return setRange;
	}

	public void setSetRange(boolean setRange) {
		this.setRange = setRange;
	}

	public void setLogarithmic(boolean log){
		logGrid = log;
	}

	public boolean isLogarithmic(){
		return logGrid;
	}
}
