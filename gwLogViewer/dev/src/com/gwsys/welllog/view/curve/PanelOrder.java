/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.util.ArrayList;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.track.YAxisView;

public class PanelOrder{
	public static final int CURVE = 0;
	public static final int GROUP = 1;
	public static final int YAXIS = 2;
	public static final int DESCRIPTION = 3;
	public static final int SEISMIC = 4;
	public static final int LITHOLOGY = 5;
	
	private ArrayList orders = new ArrayList();
	
	@SuppressWarnings("unchecked")
	public void addPanel(Object panel){
		PanelItem item = new PanelItem(panel);
		orders.add(item);		
	}
	
	public void deletePanel(Object panel){
		for(int i=0 ; i<orders.size() ; i++){
			PanelItem item = (PanelItem)orders.get(i);
			if(item.getPanel() == panel)
				orders.remove(item);
		}
	}

	public Object getPanel(int i){
		return ((PanelItem)orders.get(i)).getPanel();		
	}
	
	public Object getPanel(Object panel){
		for(int i=0 ; i<orders.size() ; i++){
			PanelItem item = (PanelItem)orders.get(i);
			if(item.getPanel() == panel)
				return item.getPanel();
		}
		return null;
	}

	public int getPanelKind(int i){
		return ((PanelItem)orders.get(i)).getPanelKind();
	}
	
	public int getPanelWidth(int i){
		return ((PanelItem)orders.get(i)).getWidth();
	}
	
	public ArrayList getOrders() {
		return orders;
	}
	
	public int getPanelLeft(Object panel){
		int left = 0;
		for(int i=0 ; i<orders.size() ; i++){
			PanelItem item = (PanelItem)orders.get(i);
			if(item.getPanel() == panel)
				break;
			left = left + item.getWidth();
		}
		return left;
	}
	
	public int getPanelRight(Object panel){
		int right = 0;
		for(int i=0 ; i<orders.size() ; i++){
			PanelItem item = (PanelItem)orders.get(i);
			right = right + item.getWidth();
			if(item.getPanel() == panel)
				break;
		}
		return right;
	}
	
	@SuppressWarnings("unchecked")
	public void changeOrder(String panel1, String panel2) {
		int selectIndex = -1;
		PanelItem item = null;

		// delete the original panel1
		for (int i = 0; i < orders.size(); i++) {
			PanelItem aItem = (PanelItem) orders.get(i);
			if (aItem.getPanelKind() == CURVE
					&& ((CurvePanel) aItem.getPanel()).getCd().getCurveName().equals(panel1)) {
				item = aItem;
				orders.remove(aItem);
				break;
			} else if (aItem.getPanelKind() == GROUP && panel1.startsWith("Group")
					&& ((CurveGroup) aItem.getPanel()).getName().equals(panel1)) {
				item = aItem;
				orders.remove(aItem);
				break;
			} else if (aItem.getPanelKind() == YAXIS
					&& panel1.equals(ConstantVariableList.DEPTH_PANEL)) {
				item = aItem;
				orders.remove(aItem);
				break;
			} else if (aItem.getPanelKind() == DESCRIPTION
					&& ((DescriptionPanel) aItem.getPanel()).getName().equals(panel1)) {
				item = aItem;
				orders.remove(aItem);
				break;
			} else if (aItem.getPanelKind() == SEISMIC
					&& ((SeismicPanel) aItem.getPanel()).getName().equals(panel1)) {
				item = aItem;
				orders.remove(aItem);
				break;
			}else if (aItem.getPanelKind() == LITHOLOGY
					&& ((LithologyPanel) aItem.getPanel()).getName().equals(panel1)) {
				item = aItem;
				orders.remove(aItem);
				break;
			}
		}

		// find the panel2
		for (int i = 0; i < orders.size(); i++) {
			PanelItem aItem = (PanelItem) orders.get(i);
			if (aItem.getPanelKind() == CURVE
					&& ((CurvePanel) aItem.getPanel()).getCd().getCurveName().equals(
							panel2)) {
				selectIndex = i;
				break;
			} else if (aItem.getPanelKind() == GROUP && panel2.startsWith("Group")
					&& ((CurveGroup) aItem.getPanel()).getName().equals(panel2)) {
				selectIndex = i;
				break;
			} else if (aItem.getPanelKind() == YAXIS
					&& panel2.equals(ConstantVariableList.DEPTH_PANEL)) {
				selectIndex = i;
				break;
			} else if (aItem.getPanelKind() == DESCRIPTION
					&& ((DescriptionPanel) aItem.getPanel()).getName().equals(panel2)) {
				selectIndex = i;
				break;
			} else if (aItem.getPanelKind() == SEISMIC
					&& ((SeismicPanel) aItem.getPanel()).getName().equals(panel2)) {
				selectIndex = i;
				break;
			} else if (aItem.getPanelKind() ==LITHOLOGY
					&& ((LithologyPanel) aItem.getPanel()).getName().equals(panel2)) {
				selectIndex = i;
				break;
			}
		}

		if (selectIndex != -1 && item != null) {
			orders.add(selectIndex, item);
		}

	}


	
	
	class PanelItem{
		Object panel;
		int width;
		
		public PanelItem(Object panel){
			this.panel = panel;
		}

		public Object getPanel() {
			return panel;
		}

		public int getWidth() {
			if(panel instanceof CurvePanel)
				width = ((CurvePanel)panel).getBesideTypeWidth();
			else if(panel instanceof YAxisView)
				width = ((YAxisView)panel).getWidth();
			else if(panel instanceof CurveGroup)
				width = ((CurveGroup)panel).getBesideTypeWidth();
			else if(panel instanceof DescriptionPanel)
				width = ((DescriptionPanel)panel).getBesideTypeWidth();
			else if(panel instanceof SeismicPanel)
				width = ((SeismicPanel)panel).getBesideTypeWidth();
			else if(panel instanceof LithologyPanel)
				width = ((LithologyPanel)panel).getBesideTypeWidth();
			return width;
		}
		
		public int getPanelKind(){
			if(panel instanceof CurvePanel)
				return PanelOrder.CURVE;
			else if(panel instanceof YAxisView)
				return PanelOrder.YAXIS;
			else if(panel instanceof CurveGroup)
				return PanelOrder.GROUP;		
			else if(panel instanceof DescriptionPanel)
				return PanelOrder.DESCRIPTION;
			else if(panel instanceof SeismicPanel)
				return PanelOrder.SEISMIC;
			else if(panel instanceof LithologyPanel){
				return PanelOrder.LITHOLOGY;
			}
			else 
				return -1;
		}
		
	}
}

