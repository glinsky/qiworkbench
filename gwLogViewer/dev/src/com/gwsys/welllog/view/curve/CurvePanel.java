/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.welllog.view.curve;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.dialogs.ComboBoxRender;
import com.gwsys.welllog.view.track.AxisCurveContainerView;

/**
 * To convert CurveData to Shape.
 * @author gaofei
 *
 */
public class CurvePanel {
	private CurveData cd;
	private CurveGroup cg;
	private float maxXDisplay;
	private float minXDisplay;

	private Color curveColor = null; // curve color//add by gaofei

	private float strokeWidth = ConstantVariableList.CURVE_STROKE;
	private Shape originalCurve = null;
	private Shape translatedCurve = null;
	private boolean visible = true;
	private boolean logarithm = false;
	private boolean selected = false;
	private boolean orderDesc = false;

	private int stokeType = ComboBoxRender.SOLID;
	private int besideTypeWidth = ConstantVariableList.BESIDETYPE_WIDTH;
	private boolean curveRedrawFlag = false;

	private int left;

	/**
	 * Constructor.
	 * @param cd CurveData.
	 */
	public CurvePanel(CurveData cd){
		this.cd = cd;
		this.maxXDisplay = cd.getMaxX();
		this.minXDisplay = cd.getMinX();
		this.curveColor = ConstantVariableList.CURVE_DEFAULT_COLOR;
	}

	public Shape getTranslatedCurve() {
		return this.translatedCurve;
	}

	public float getValue(float oldValue, String originalYType, String nowYType,
			AxisCurveContainerView track){
		float nowValue = oldValue;
		if (originalYType.equals(TrackTypeList.Y_TIME)){
			if (nowYType.equals(TrackTypeList.Y_DEPTH_MD)){
				nowValue = track.getTimeToDepthConverter().toDepth(oldValue,TrackTypeList.TIME_TO_MD);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_TVD)){
				nowValue = track.getTimeToDepthConverter().toDepth(oldValue,TrackTypeList.TIME_TO_MD);
				nowValue = track.getMDDepthConverter().toDepth(nowValue,TrackTypeList.MD_TO_TVD);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_TVDSS)){
				nowValue = track.getTimeToDepthConverter().toDepth(oldValue,TrackTypeList.TIME_TO_MD);
				nowValue = track.getMDDepthConverter().toDepth(nowValue,TrackTypeList.MD_TO_TVDSS);
			}
		}
		if (originalYType.equals(TrackTypeList.Y_DEPTH_MD)){
			if (nowYType.equals(TrackTypeList.Y_TIME)){
				nowValue = track.getDepthToTimeConverter().toTime(oldValue);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_TVD)){
				nowValue = track.getMDDepthConverter().toDepth(oldValue,TrackTypeList.MD_TO_TVD);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_TVDSS)){
				nowValue = track.getMDDepthConverter().toDepth(oldValue,TrackTypeList.MD_TO_TVDSS);
			}
		}
		if (originalYType.equals(TrackTypeList.Y_DEPTH_TVD)){
			if (nowYType.equals(TrackTypeList.Y_TIME)){
				nowValue = track.getMDDepthConverter().toDepth(oldValue,TrackTypeList.TIME_TO_MD);
				nowValue = track.getDepthToTimeConverter().toTime(nowValue);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_MD)){
				nowValue = track.getMDDepthConverter().toDepth(oldValue,TrackTypeList.TVD_TO_MD);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_TVDSS)){
				nowValue = track.getMDDepthConverter().toDepth(oldValue,TrackTypeList.TVD_TO_MD);
				nowValue = track.getMDDepthConverter().toDepth(nowValue,TrackTypeList.MD_TO_TVD);
			}
		}
		if (originalYType.equals(TrackTypeList.Y_DEPTH_TVDSS)){
			if (nowYType.equals(TrackTypeList.Y_TIME)){
				nowValue = track.getMDDepthConverter().toDepth(-oldValue,TrackTypeList.TIME_TO_MD);
				nowValue = track.getDepthToTimeConverter().toTime(nowValue);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_MD)){
				nowValue = track.getMDDepthConverter().toDepth(-oldValue,TrackTypeList.TIME_TO_MD);
			}
			if (nowYType.equals(TrackTypeList.Y_DEPTH_TVD)){
				nowValue = track.getMDDepthConverter().toDepth(-oldValue,TrackTypeList.TIME_TO_MD);
				nowValue = track.getMDDepthConverter().toDepth(nowValue,TrackTypeList.MD_TO_TVD);
			}
		}
		return nowValue;
	}

	public Element saveXML(Document doc, Integer id, boolean group) {
		String xmlCaption = "CurveData";
		if (group == true) {
			xmlCaption = xmlCaption + id;
		}
		Element xmlCurvePanel = doc.createElement(xmlCaption);
		xmlCurvePanel.setAttribute("id", Integer.toString(id));
		xmlCurvePanel.setAttribute("name", cd.getHashCurveName());
		xmlCurvePanel.setAttribute("stroke", Float.toString(this
				.getStrokeWidth()));
		xmlCurvePanel.setAttribute("color", Integer.toString(this
				.getCurveColor().getRGB()));
		xmlCurvePanel.setAttribute("stroketype", Integer.toString(this
				.getStokeType()));
		xmlCurvePanel.setAttribute("panelwidth", Integer.toString(this
				.getBesideTypeWidth()));
		xmlCurvePanel.setAttribute("MinXDisplay", Float.toString(this
				.getMinXDisplay()));
		xmlCurvePanel.setAttribute("MaxXDisplay", Float.toString(this
				.getMaxXDisplay()));
		xmlCurvePanel.setAttribute("visble", Boolean.toString(this.isVisible()));
		xmlCurvePanel.setAttribute("Logarithm", Boolean.toString(this
				.isLogarithm()));
		xmlCurvePanel.setAttribute("OrderDesc", Boolean.toString(this
				.isOrderDesc()));
		xmlCurvePanel.setAttribute("curveUnit", cd.getCurveUnit());
		return xmlCurvePanel;
	}

	public void loadXML(Element xmlCurvePanel) {
		this.setStrokeWidth(Float.parseFloat(xmlCurvePanel
				.getAttribute("stroke")));
		this.setCurveColor(new Color(Integer.parseInt(xmlCurvePanel
				.getAttribute("color"))));
		this.setStokeType(Integer.parseInt(xmlCurvePanel
				.getAttribute("stroketype")));
		this.setBesideTypeWidth(Integer.parseInt(xmlCurvePanel
				.getAttribute("panelwidth")));
		//compatible
		if (xmlCurvePanel.getAttribute("MinXDisplay") != null)
			setMinXDisplay(Float.parseFloat(xmlCurvePanel
					.getAttribute("MinXDisplay")));
		if (xmlCurvePanel.getAttribute("MaxXDisplay") != null)
			setMaxXDisplay(Float.parseFloat(xmlCurvePanel
					.getAttribute("MaxXDisplay")));
		this.setVisible(ConversionClass.stringToBoolean(xmlCurvePanel
				.getAttribute("visble")));
		this.setLogarithm(ConversionClass.stringToBoolean(xmlCurvePanel
				.getAttribute("Logarithm")));
		this.setOrderDesc(ConversionClass.stringToBoolean(xmlCurvePanel
				.getAttribute("OrderDesc")));
		String unit = xmlCurvePanel.getAttribute("curveUnit");
		if (unit!=null&&unit.length()!=0){
			cd.setCurveUnit(unit);
		}
	}



	/**
	 *
	 * @param componentWidth
	 * @param componentHeight
	 * @param xMove
	 */
	public void setTranslatedCurve(CurveView cv, float xMove, float rateWidth, float rateHeight){
		this.translatedCurve = setTranslatedCurve(cv,xMove,0,0,false, rateWidth, rateHeight);
	}

	public Shape setTranslatedCurve(CurveView cv, float xMove,
			float topY, float bottomY, boolean isMarker,float rateWidth, float rateHeight) {
		Shape myShape = null;
		float componentWidth = (int)(cv.getWidth()*rateWidth);
		float componentHeight = (int)(cv.getHeight()*rateHeight);

		if (cv.getCurveLayer().getLayerType().equals(
				TrackTypeList.REALIZATION_BESIDE)) {
			componentWidth = (int)(this.getBesideTypeWidth()*rateWidth);
		}

		double xScale, yScale, xTranslate, yTranslate;
		float maxXDisplay, minXDisplay;
		maxXDisplay = getMaxXDisplay();
		minXDisplay = getMinXDisplay();
		if(this.isLogarithm()){
			maxXDisplay = this.log(maxXDisplay, 10);
			minXDisplay = this.log(minXDisplay, 10);
		}

		if (orderDesc) {
			xTranslate = -(double) maxXDisplay;// move the min x to the left
			xScale = -(double) ((componentWidth) / (maxXDisplay - minXDisplay));
		} else {
			xTranslate = -(double) minXDisplay;// move the min x to the left
			xScale = (double) ((componentWidth) / (maxXDisplay - minXDisplay));
		}

		// the translate is complicated, because we must take the original yType
		// and current yType into account
		// there are four conditions(original yType has 2 conditions and yType
		// has 2 conditions)
		int depthUnit = cv.getParentAxisCurveContainer().getDepthUnit();
		double unitTran = 1;
		if (depthUnit == CurveData.METER) {
			if (cd.getDepthUnit() == CurveData.FOOT) {
				unitTran = ConstantVariableList.FOOT_TO_METER;

			}
		} else if (depthUnit == CurveData.FOOT) {
			if (cd.getDepthUnit() == CurveData.METER) {
				unitTran = ConstantVariableList.METER_TO_FOOT;
			}
		} else {
			if (cd.getDepthUnit() == CurveData.FOOT) {
				unitTran = ConstantVariableList.FOOT_TO_METER;

			}
		}

		String yType = cv.getParentYAxisView().getYType();
		setCurveShape(cv, unitTran,topY,bottomY,isMarker, yType);
		if(this.getOriginalCurve() == null){
			return null;
		}

		float maxValue = CurveData.getMaxNum(getCd().getY());
		float minValue = CurveData.getMinNum(getCd().getY());
		maxValue = getValue(maxValue,cd.getOriginalYType(),yType,cv.getParentAxisCurveContainer());
		minValue = getValue(minValue,cd.getOriginalYType(),yType,cv.getParentAxisCurveContainer());

		float yMove = 0;
		if (yType.equals(TrackTypeList.Y_TIME)) {
			if (cd.getOriginalYType() == TrackTypeList.Y_DEPTH_MD) {
				int curveHeight = (int) (componentHeight
						* (maxValue - minValue) / (cv
						.getCurveLayer().getYSetBottomTime() - cv
						.getCurveLayer().getYSetTopTime()));
				yScale = (double) ((curveHeight * unitTran) / (maxValue - minValue));
				yTranslate = -minValue * unitTran;
			} else {
				yScale = (double) ((componentHeight) / (cv.getCurveLayer()
						.getYSetBottomTime() - cv.getCurveLayer()
						.getYSetTopTime()));
				yTranslate = -minValue * unitTran;
			}
			yMove = componentHeight
					* (minValue * (float) unitTran - cv
							.getCurveLayer().getYSetTopTime())
					/ (cv.getCurveLayer().getYSetBottomTime() - cv
							.getCurveLayer().getYSetTopTime());
		} else {
			if (cd.getOriginalYType() == TrackTypeList.Y_DEPTH_MD) {
				yScale = (double) ((componentHeight) / (cv.getCurveLayer()
						.getYSetBottomDepth() - cv.getCurveLayer()
						.getYSetTopDepth()));
				yTranslate = -minValue * unitTran;
			} else {
				int curveHeight = (int) (componentHeight
						* (maxValue - minValue) / (cv.getCurveLayer().getYSetBottomDepth()
								- cv.getCurveLayer().getYSetTopDepth()));
				yScale = (double) ((curveHeight * unitTran) / (maxValue - minValue));
				yTranslate = -minValue * unitTran;
			}
			yMove = componentHeight
					* (minValue * (float) unitTran - cv.getCurveLayer()
							.getYSetTopDepth())
					/ (cv.getCurveLayer().getYSetBottomDepth() - cv
							.getCurveLayer().getYSetTopDepth());
		}

		AffineTransform t = new AffineTransform();
		AffineTransform t1 = new AffineTransform();
		AffineTransform t2 = new AffineTransform();
		t.translate(xTranslate, yTranslate);
		t1.scale(xScale, yScale);
		// if is beside, need take the x offset and y offset into account

		if (cv.getCurveLayer().getLayerType().equals(
				TrackTypeList.REALIZATION_BESIDE)) {
			t2.translate(xMove, yMove);
			myShape = t2.createTransformedShape(t1
					.createTransformedShape(t.createTransformedShape(this
							.getOriginalCurve())));
		}
		// if is top, need take the y offset into account
		else {
			t2.translate(0, yMove);
			myShape = t2.createTransformedShape(t1
					.createTransformedShape(t.createTransformedShape(this
							.getOriginalCurve())));
		}
		curveRedrawFlag = false;
		return myShape;
	}

	@SuppressWarnings("unchecked")
	private void setCurveShape(CurveView cv,double unitTran , float topY, float bottomY,
			boolean isMarker, String yType){
		int n = getCd().getX().length;
		ArrayList xList = new ArrayList();
		ArrayList yList = new ArrayList();
		float x,y;
		topY = getValue(topY,cd.getOriginalYType(),yType,
				cv.getParentAxisCurveContainer())*(float)unitTran;
		bottomY = getValue(bottomY,cd.getOriginalYType(),yType,
				cv.getParentAxisCurveContainer())*(float)unitTran;
		for(int i=0;i<n;i++){
			if(getCd().getX()[i] > getMaxXDisplay() ){
				x = getMaxXDisplay();
			}else if(getCd().getX()[i] < getMinXDisplay()){
				x = getMinXDisplay();
			}else{
				x = getCd().getX()[i];
			}
			if(this.isLogarithm()){
				x = this.log(x,10);
			}
			y = getValue(getCd().getY()[i],cd.getOriginalYType(),yType,cv.getParentAxisCurveContainer())*(float)unitTran;
			if (!isMarker){
				xList.add(x);
				yList.add(y);
			}else if(y>= topY && y<=bottomY){
				xList.add(x);
				yList.add(y);
			}
		}
		if(xList.size() < 1){
			setOriginalCurve(null);
			return;
		}
		float dx[] = new float[xList.size()];
		float dy[] = new float[xList.size()];
		for(int i =0 ; i < xList.size() ;i++){
			dx[i] = ((Float)xList.get(i)).floatValue();
			dy[i] = ((Float)yList.get(i)).floatValue();
		}
		//negative step
		if(dy[0]<dy[xList.size()-1]){
			setOriginalCurve(OperationClass.drawShape(dx, dy));
		}else{
			float dx2[] = new float[xList.size()];
			float dy2[] = new float[xList.size()];
			for(int i =0 ; i < xList.size() ;i++){
				dx2[i] = dx[xList.size()-1-i];
				dy2[i] = dy[xList.size()-1-i];
			}
			setOriginalCurve(OperationClass.drawShape(dx2, dy2));
		}
	}

	public float log(double d, double d1)
    {
        return (float)(Math.log(d) / Math.log(d1));
    }

	public float pow(double d, double d1){
		return (float)(Math.pow(d,d1));
	}

	public int getBesideTypeWidth() {
		return besideTypeWidth;
	}

	public void setBesideTypeWidth(int besideTypeWidth) {
		this.besideTypeWidth = besideTypeWidth;
	}

	public Color getCurveColor() {
		return curveColor;
	}

	public void setCurveColor(Color curveColor) {
		this.curveColor = curveColor;
	}

	public boolean isLogarithm() {
		return logarithm;
	}

	public void setLogarithm(boolean logarithm) {
		this.logarithm = logarithm;
	}

	public boolean isOrderDesc() {
		return orderDesc;
	}

	public void setOrderDesc(boolean orderDesc) {
		this.orderDesc = orderDesc;
	}

	public Shape getOriginalCurve() {
		return originalCurve;
	}

	public void setOriginalCurve(Shape originalCurve) {
		this.originalCurve = originalCurve;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getStokeType() {
		return stokeType;
	}

	public void setStokeType(int stokeType) {
		this.stokeType = stokeType;
	}

	public float getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(float strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public float getMaxXDisplay() {
		return maxXDisplay;
	}

	public void setMaxXDisplay(float maxXDisplay) {
		this.maxXDisplay = maxXDisplay;
	}

	public float getMinXDisplay() {
		return minXDisplay;
	}

	public void setMinXDisplay(float minxDisplay) {
		this.minXDisplay = minxDisplay;
	}

	public boolean isCurveRedrawFlag() {
		return curveRedrawFlag;
	}

	public void setCurveRedrawFlag(boolean curveRedrawFlag) {
		this.curveRedrawFlag = curveRedrawFlag;
	}

	public CurveData getCd() {
		return cd;
	}

	public CurveGroup getCg() {
		return cg;
	}

	public void setCg(CurveGroup cg) {
		this.cg = cg;
	}

	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

}
