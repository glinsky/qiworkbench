/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.view.dialogs.EditDescriptionTextDialog;
/**
 * @author Administrator
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DescriptionObject implements MouseListener,MouseMotionListener,ActionListener{
	private DescriptionPanel parentdp = null;
	//private Object control;
	private JLabel lbImage;
	private int controlType;
	private String itemID;
	private int pointX,pointY;
	private String imagePath;
	private String text;
	private Color textColor;
	private int FontSize;
	
	public static final int CONTROL_TEXT_LABEL = 1;	
	public static final int CONTROL_IMAGE_LABEL = 2;

	public DescriptionObject(DescriptionPanel parent, int controlType){
		this.parentdp = parent;
		lbImage = new JLabel();
		this.controlType = controlType;	
		lbImage.addMouseListener(this);
		lbImage.addMouseMotionListener(this);
		this.imagePath = "";
		this.setTextColor(parent.getDefaultTextColor());
		this.setFontSize(parent.getDefaultFontSize());
		this.itemID = "Description " + parentdp.getItemID();
		parentdp.setItemID(parentdp.getItemID() + 1);
	}	
	
	public Element saveXML(Document doc, Integer id){
		Element xmlDescriptionObject = doc.createElement("DescriptionObject"+id);		
		xmlDescriptionObject.setAttribute("itemID",this.getItemID());
		xmlDescriptionObject.setAttribute("controlType",Integer.toString(this.getControlType()));
		xmlDescriptionObject.setAttribute("imagePath",this.getImagePath());
		xmlDescriptionObject.setAttribute("text",this.getText());
		xmlDescriptionObject.setAttribute("FontSize",Integer.toString(this.getFontSize()));
		xmlDescriptionObject.setAttribute("TextColor",Integer.toString(this.getTextColor().getRGB()));
		xmlDescriptionObject.setAttribute("locationX",Integer.toString(lbImage.getX()));
		xmlDescriptionObject.setAttribute("locationY",Integer.toString(lbImage.getY()));
		return xmlDescriptionObject;
	}
	
	public void loadXML(Element xmlDescriptionObject){
		
	}
	
	public JLabel getLbImage() {
		return lbImage;
	}

	public String getItemID() {
		return itemID;
	}
	
	public int getControlType(){
		return controlType;
	}	
	
	public int getFontSize() {
		return FontSize;
	}

	public void setFontSize(int fontSize) {
		FontSize = fontSize;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public DescriptionPanel getParentdp() {
		return parentdp;
	}

	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
	    int setX,setY;
	    setX = (int)(lbImage.getX()+e.getX()-pointX);
        setY = (int)(lbImage.getY()+e.getY()-pointY);
	    lbImage.setBounds(setX,setY,(int)lbImage.getWidth(),(int)lbImage.getHeight());
	    
	}

	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
	    pointX = (int)(e.getPoint().x);
	    pointY = (int)(e.getPoint().y);
		if(e.getButton()==MouseEvent.BUTTON3){
			JPopupMenu popup = new JPopupMenu();
			
			JMenuItem deleteItem = new JMenuItem("Delete");
			popup.add(deleteItem);
			deleteItem.addActionListener(this);
			deleteItem.setActionCommand("Delete");
			
			if(controlType == CONTROL_TEXT_LABEL){
				
				JMenuItem editItem = new JMenuItem("Edit");
				popup.add(editItem);
				editItem.addActionListener(this);
				editItem.setActionCommand("Edit");
								
			}
			popup.show(lbImage, e.getX(), e.getY());			
		}
	}

	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("Delete")){
			removeControl();		
		}
		else if(e.getActionCommand().equals("Edit")){
			editText();	
		}
	}
	
	public void removeControl(){
		parentdp.remove(this.lbImage);
		parentdp.deleteDescriptionObject(this);
		parentdp.repaint();
	}
	
	public void editText(){
		EditDescriptionTextDialog textDialog = new EditDescriptionTextDialog(parentdp , this);
		textDialog.setVisible(true);
		if(textDialog.getText()==""){
			return;
		}
		this.setText(textDialog.getText());
		this.setTextLabel();
	}		
	
	@SuppressWarnings("unchecked")
	public void setTextLabel(){
		Font font = new Font("Arial",Font.PLAIN,this.getFontSize());
		
		BufferedImage bImg=new BufferedImage(150,100,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2=(Graphics2D)bImg.getGraphics();
		
		FontRenderContext frc = g2.getFontRenderContext();
		String rectText = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Rectangle2D bounds = font.getStringBounds(rectText,frc);
		int rowHeight = (int) (bounds.getHeight()+2);
		
		int rows=0;
		StringTokenizer tokenizer = new StringTokenizer(this.getText() , "\n");	
		
		ArrayList textList = new ArrayList();
		while(tokenizer.hasMoreTokens()){
			String lineText = tokenizer.nextToken();
			for(;;){
				int rowLen = 0;
				String rowText = "";
				for(;;){
					rowLen++;
					rowText = lineText.substring(0,rowLen);
					bounds = font.getStringBounds(rowText,frc);
					if(((int)bounds.getWidth()) > (this.parentdp.getBesideTypeWidth()-6)){
						rowLen--;
						break;
					}
					if(rowLen == lineText.length()){
						break;
					}
				}
				rowText = lineText.substring(0,rowLen);
				textList.add(rowText + "\n");
				rows ++;
				lineText = lineText.substring(rowLen,lineText.length());
				if(lineText.trim().equals("")){
					break;
				}
				if(lineText.length() <=  rowLen){
					textList.add(lineText + "\n");
					rows ++;
					break;
				}
				
			}
		}	
		
		bImg=new BufferedImage(this.parentdp.getBesideTypeWidth(),rowHeight* textList.size(),BufferedImage.TYPE_INT_ARGB);
		g2=(Graphics2D)bImg.getGraphics();
		g2.setColor(this.getTextColor());
		g2.setFont(font);
		for(int i =0; i < textList.size(); i++){
			g2.drawString((String)textList.get(i),0,rowHeight*(i+1));
		}
		ImageIcon icon = new ImageIcon(bImg);
		lbImage = null;		
		lbImage = new JLabel(icon, JLabel.CENTER);		
		lbImage.setBounds(0,0,icon.getIconWidth(),icon.getIconHeight());
		lbImage.addMouseListener(this);
		lbImage.addMouseMotionListener(this);
	}
	
	public void setImageLabel(){
		File file = new File(this.getImagePath());
		ImageIcon icon = new ImageIcon(this.getImagePath());
		int intWidth = 0;
		int intHeight = 0;
		try{
			Image src = javax.imageio.ImageIO.read(file);  
			intWidth = src.getWidth(null); 
			intHeight = src.getHeight(null);
			int scaleImage = (int)((intHeight*1000)/intWidth);
			int scalePanel = (int)((this.parentdp.getHeight()*1000)/this.parentdp.getWidth());
			boolean bResize = false;
			if(intHeight>(this.parentdp.getHeight()-2)){
				bResize = true;
			}
			if(intWidth>(this.parentdp.getWidth()-2)){
				bResize = true;
			}			
			if(bResize){			
				if(scalePanel>scaleImage){
					intWidth = this.parentdp.getWidth()-2;
					intHeight = (int) ((intWidth*scaleImage)/1000);
				}else{
					intHeight = this.parentdp.getHeight()-2;
					intWidth = (int)(intHeight/(scaleImage*1000));
				}
				BufferedImage tag = new BufferedImage(intWidth,intHeight,BufferedImage.OPAQUE);
				tag.getGraphics().drawImage(src,0,0,intWidth,intHeight,null);       //������С���ͼ
				icon = new ImageIcon(tag);
			}		
		}catch (Exception e){
			System.out.println("Open File Error");
		}		
		lbImage = null;
		lbImage = new JLabel(icon, JLabel.CENTER);		
		lbImage.setBounds(0,0,icon.getIconWidth(),icon.getIconHeight());
		lbImage.addMouseListener(this);
		lbImage.addMouseMotionListener(this);
	}
}
