/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gwsys.welllog.core.Renderable;
import com.gwsys.welllog.core.curve.CaculateClass;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.LogLayer;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.dialogs.ComboBoxRender;
import com.gwsys.welllog.view.zone.ZoneProp;
import com.gwsys.welllog.view.fill.CompoundFill;
import com.gwsys.welllog.view.fill.CurveFillBeside;
import com.gwsys.welllog.view.fill.CurveFillTop;
import com.gwsys.welllog.view.fill.CurveLithologyBeside;
import com.gwsys.welllog.view.fill.CurveLithologyTop;
import com.gwsys.welllog.view.track.AxisCurveContainerView;

/**
 * containers lots of curve. Each instance is corresponding for a curveview
 *
 * @author luming
 * @author GaoFei
 */
public class CurveLayer extends LogLayer implements Renderable{
	AxisCurveContainerView parent = null;

	private ArrayList curveList = new ArrayList();

	private ArrayList allCurveList = new ArrayList();

	private ArrayList p105090 = new ArrayList();

	private CurvePanel meanCurve;

	private ArrayList curveSelectedList = new ArrayList();

	private ArrayList curveFillTopList = new ArrayList();

	private ArrayList<CurveLithologyTop> curveLithologyTopList =
		new ArrayList<CurveLithologyTop>();

	private ArrayList curveFillBesideList = new ArrayList();
	
	private ArrayList compoundFillList = new ArrayList();

	private ArrayList<CurveLithologyBeside> curveLithologyBesideList =
		new ArrayList<CurveLithologyBeside>();

	private float yTopDepth, yBottomDepth, yTopTime, yBottomTime;

	private float ySetTopDepth,ySetBottomDepth;

	private boolean paintLine = true;

	private int besideTypeWidth = ConstantVariableList.BESIDETYPE_WIDTH;

	private int yCutNumber = ConstantVariableList.YDepthCutNumber;

	private boolean p105090meanRefresh = true, setDisplayDepth = false;

	public CurveLayer(AxisCurveContainerView parent) {
		this.parent = parent;
	}

	@SuppressWarnings("unchecked")
	public void calculateShape(float rateWidth, float rateHeight){
		allCurveList.clear();
		CurveView cv = parent.getCurveView();
		CurveGroup curveGroup = null;
		int width = 0;
		for (int i = 0 , j=0; i < cv.getPanelOrder().getOrders().size(); ) {
			boolean group = false;
			CurvePanel curvePanel = null;
			if(j==0 && cv.getPanelOrder().getPanel(i) instanceof CurvePanel){
				curvePanel = (CurvePanel)cv.getPanelOrder().getPanel(i);
				i++;
				group = false;
			}
			else if(cv.getPanelOrder().getPanel(i) instanceof CurveGroup){
				curveGroup = (CurveGroup)cv.getPanelOrder().getPanel(i);
				if(j == curveGroup.getCurveList().size()){
					width = width + (int)(curveGroup.getBesideTypeWidth()*rateWidth);
					j = 0;
					i++;
					continue;
				}
				curvePanel = (CurvePanel)curveGroup.getCurveList().get(j);
				j++;
				group = true;
			}
			else {//YaXisView, SeimiscPanel, LithologyPanel, DescriptionPanel
				width = width + (int)(cv.getPanelOrder().getPanelWidth(i)*rateWidth);
				i++;
				continue;
			}

			if (cv.isMean(curvePanel) && !cv.isShowMean()) {
				continue;
			}
			if (cv.isP105090(curvePanel) && !cv.isShowP105090()) {
				continue;
			}
			if (!cv.isShowPlainCurve() && !cv.isP105090(curvePanel) && !cv.isMean(curvePanel)) {
				continue;
			}

			if (!curvePanel.isVisible()) {
				if(group == false)
					width = width + (int)(curvePanel.getBesideTypeWidth()*rateWidth);
				continue;
			}

			if (cv.isCurveRedrawFlag()) {
				curvePanel.setLeft(width);
				curvePanel.setTranslatedCurve(cv, width, rateWidth,rateHeight);
			}else{
				if(curvePanel.isCurveRedrawFlag()){
					curvePanel.setLeft(width);
					curvePanel.setTranslatedCurve(cv, width, rateWidth,rateHeight);
				}
			}

			allCurveList.add(curvePanel);

			if(group == false)
				width = width + (int)(curvePanel.getBesideTypeWidth()*rateWidth);
		}
	}

	public void draw(Graphics2D g2d, Rectangle rect) {
		print(g2d, rect, 1, 1);
	}

	public void print(Graphics2D g2d, Rectangle rect, float rateWidth, float rateHeight) {
		CurveView cv = parent.getCurveView();
		//AffineTransform af = AffineTransform.getScaleInstance(rateWidth, rateHeight);
		// Curve fill
		if (cv.isShowFill() && !this.isPaintLine()) {
			if (getLayerType().equals(TrackTypeList.REALIZATION_TOP)) {
				for (int i = 0; i < getCurveLithologyTopList().size(); i++) {
					CurveLithologyTop clt = getCurveLithologyTopList().get(i);
					if (cv.isFillRedrawFlag() || cv.isCurveRedrawFlag()) {
						clt.constructFill(cv);
					}
					TexturePaint texturePaint = ZoneProp.getWellLogPatterns()[clt.getWhichtexture()];
					g2d.setPaint(texturePaint);
					g2d.fill((clt.getMyFill()));
				}
				for (int i = 0; i < getCurveFillTopList().size(); i++) {
					CurveFillTop cft = (CurveFillTop) getCurveFillTopList().get(i);

					if (cv.isFillRedrawFlag() || cv.isCurveRedrawFlag()) {
						cft.constructFill(cv);
					}
					g2d.setColor(cft.getFillColor());
					if (cv.isTransparentBackground())
						g2d.setXORMode(Color.white);
					g2d.fill((cft.getMyFill()));
				}				
				
			}
			if (getLayerType().equals(TrackTypeList.REALIZATION_BESIDE)) {
				for (int i = 0; i < getCurveLithologyBesideList().size(); i++) {
					CurveLithologyBeside clb = getCurveLithologyBesideList().get(i);
					CurvePanel cp = clb.getCp();
					if (cv.isFillRedrawFlag() || cv.isCurveRedrawFlag()) {
						PanelOrder order = cv.getPanelOrder();
						int widthForLeft = (int)(order.getPanelLeft(cp)*rateWidth);
						int widthForRight = (int)(order.getPanelRight(cp)*rateWidth);
						clb.constructFill(widthForLeft, widthForRight);
					}
					TexturePaint texturePaint = ZoneProp.getWellLogPatterns()[clb.getWhichtexture()];
					g2d.setPaint(texturePaint);
					g2d.fill((clb.getMyFill()));
				}

				for (int i = 0; i < getCurveFillBesideList().size(); i++) {
					CurveFillBeside cfb = (CurveFillBeside) getCurveFillBesideList().get(i);
					CurvePanel cp = cfb.getCp();					
					
					if (cv.isFillRedrawFlag() || cv.isCurveRedrawFlag()) {
						PanelOrder order = cv.getPanelOrder();
						int widthForLeft, widthForRight;
						
						// Updated by Alex. check if single Fill is in a group
						if (cfb.isFillIntoGroup())	{
							widthForLeft = (int)(order.getPanelLeft(cfb.getCurveGroup())*rateWidth);
							widthForRight = (int)(order.getPanelRight(cfb.getCurveGroup())*rateWidth);
						}
						else	{
							widthForLeft = (int)(order.getPanelLeft(cp)*rateWidth);
							widthForRight = (int)(order.getPanelRight(cp)*rateWidth);
						}
						
						cfb.constructFill(widthForLeft, widthForRight);
					}
					g2d.setColor(cfb.getFillColor());
					if (cv.isTransparentBackground())
						g2d.setXORMode(Color.white);
					g2d.fill((cfb.getMyFill()));
				}
				
				//	added by Alex
				for (int i = 0; i < getCompoundFillList().size(); i++)	{
					CompoundFill cf = (CompoundFill) getCompoundFillList().get(i);
					
					if (cv.isFillRedrawFlag() || cv.isCurveRedrawFlag()) {
						cf.constructFill(cf.getLeftCurve(), cf.getRightCurve());										
					}
					
					g2d.setColor(cf.getFillColor());
					if (cv.isTransparentBackground())
						g2d.setXORMode(Color.white);					
					
					//System.out.println("# of Shapes: "+ cf.getAllFills().size());
					for (int index = 0; index < cf.getAllFills().size(); index++)	{						
						//insertar aqui el "index" en vez de "i"
						g2d.fill(cf.getAllFills().get(index));
					}
				}
			}
		}

		if(!this.isPaintLine()){
			return;
		}

		// Curve
		for(int i=0;i<allCurveList.size();i++){
			CurvePanel curvePanel = (CurvePanel)allCurveList.get(i);
			Rectangle boundModel =curvePanel.getTranslatedCurve().getBounds();

			if (rect.intersects(boundModel) || rect.contains(boundModel)) {
				g2d.setStroke(ComboBoxRender.getStroke(curvePanel.getStokeType()
						,curvePanel.getStrokeWidth()));
				if (curvePanel.getCurveColor() == null) {
					curvePanel.setCurveColor(cv.getCurveDefaultColor());
				}
				g2d.setColor(curvePanel.getCurveColor());
				if (curvePanel.isSelected()) { //make double width for selected curve
					g2d.setStroke(ComboBoxRender.getStroke(curvePanel.getStokeType()
							,2*curvePanel.getStrokeWidth()));
				}
				g2d.draw(curvePanel.getTranslatedCurve());
                // set back
				g2d.setStroke(new BasicStroke(cv.getDefaultstroke()));
			}
		}

		// Border
		if (getLayerType().equals(TrackTypeList.REALIZATION_BESIDE)) {

			float width3 = 0;
			int height = (int)(cv.getHeight()*rateHeight);

			for (int i = 0; i < cv.getPanelOrder().getOrders().size(); i++) {

				if(cv.getPanelOrder().getPanel(i) instanceof CurvePanel){
					CurvePanel curvePanel = (CurvePanel)cv.getPanelOrder().getPanel(i);
					float width = (curvePanel.getBesideTypeWidth()*rateWidth);
					if (curvePanel.isSelected()) {
						g2d.setColor(Color.RED);
					} else {
						g2d.setColor(curvePanel.getCurveColor());
					}
					g2d.drawRect((int)width3, 0, (int)width, height - 1);
					g2d.drawRect((int)width3 + 1, 1, (int)width - 2, height - 2);

					width3 += width;
				}
				else if(cv.getPanelOrder().getPanel(i) instanceof CurveGroup){
					CurveGroup curveGroup = (CurveGroup)cv.getPanelOrder().getPanel(i);
					float width = (curveGroup.getBesideTypeWidth()*rateWidth);
					g2d.setColor(Color.BLUE);
					g2d.drawRect((int)width3, 0, (int)width,	height - 1);
					g2d.drawRect((int)width3 + 1, 1, (int)width - 2, height - 2);

					width3 += width;
				}
				else {
					width3 += (cv.getPanelOrder().getPanelWidth(i)*rateWidth);

				}
			}

		}
	}

	/**
	 * check the point p is or not on curves.if p on some curves,return curve
	 * order,else return -1.
	 *
	 * @author luming
	 * @param curveName
	 * @param pthe
	 *            click point
	 * @return the curve order
	 */
	public String selectCurve(Point pt) {

		OperationClass operationClass = new OperationClass();

		for (int i = this.getCurveList().size() - 1; i > -1; i--) 
		{
			CurvePanel thisCurve = (CurvePanel) this.getCurveList().get(i);

			if (operationClass.pick(thisCurve.getTranslatedCurve(), 
					pt.getX(), pt.getY(), 5D, false)) {
				return thisCurve.getCd().getHashCurveName();
			}
		}

		return null; 
	}

	public String[] selectCurveUnderGroup(Point pt) {
		String[] curveAndGroup = new String[2];
		OperationClass operationClass = new OperationClass();
		ArrayList curveGroups = this.parent.getCurveView().getCurveGroups();
		for(int i =0 ; i < curveGroups.size(); i++){
			CurveGroup cg = (CurveGroup)curveGroups.get(i);
			for(int j =0 ; j < cg.getCurveList().size() ; j++){
				CurvePanel thisCurve = (CurvePanel) cg.getCurveList().get(j);
				if (operationClass.pick(thisCurve.getTranslatedCurve(), pt.getX(), pt.getY(), 5D, false)) {
					curveAndGroup[0] = cg.getName();
					curveAndGroup[1] = thisCurve.getCd().getHashCurveName();
					return curveAndGroup;
				}
			}
		}

		return null; // if no one
	}

	/**
	 *
	 * @param p
	 * @return the top selected curveFill sequenceNumber in curveFillTopList
	 * @author GaoFei
	 */
	public boolean selectCurveFill(Point p) {
		int sequenceNumber = -1;
		ArrayList list = null;
		if (this.getLayerType().equals(TrackTypeList.REALIZATION_BESIDE)) {
			list = this.getCurveFillBesideList();
			for (int i = 0; i < list.size(); i++) {
				if (((CurveFillBeside) list.get(i)).getMyFill().contains(p)) {
					sequenceNumber = i;
				}
				((CurveFillBeside) list.get(i)).setSelected(false);
			}
			if (sequenceNumber != -1) {
				((CurveFillBeside) list.get(sequenceNumber)).setSelected(true);
			}
			return (sequenceNumber == -1) ? false : true;
		} else {
			list = this.getCurveFillTopList();
			for (int i = 0; i < list.size(); i++) {
				if (((CurveFillTop) list.get(i)).getMyFill().contains(p)) {
					sequenceNumber = i;
				}
				((CurveFillTop) list.get(i)).setSelected(false);
			}
			if (sequenceNumber != -1) {
				((CurveFillTop) list.get(sequenceNumber)).setSelected(true);
			}
			return (sequenceNumber == -1) ? false : true;
		}
	}

	/**
	 *
	 * @param p
	 * @return the top selected CurveLithologyTop sequenceNumber in
	 *         curveFillTopList
	 * @author GaoFei
	 */
	public boolean selectCurveLithology(Point p) {
		int sequenceNumber = -1;
		if (this.getLayerType().equals(TrackTypeList.REALIZATION_TOP)) {
			for (int i = 0; i < this.getCurveLithologyTopList().size(); i++) {
				if (((CurveLithologyTop) curveLithologyTopList.get(i)).getMyFill().contains(p)) {
					sequenceNumber = i;
				}
				((CurveLithologyTop) curveLithologyTopList.get(i)).setSelected(false);
			}
			if (sequenceNumber != -1) {
				((CurveLithologyTop) curveLithologyTopList.get(sequenceNumber)).setSelected(true);
			}
			return (sequenceNumber == -1) ? false : true;
		} else {
			for (int i = 0; i < this.getCurveLithologyBesideList().size(); i++) {
				if (((CurveLithologyBeside) curveLithologyBesideList.get(i)).getMyFill().contains(p)) {
					sequenceNumber = i;
				}
				((CurveLithologyBeside) curveLithologyBesideList.get(i)).setSelected(false);
			}
			if (sequenceNumber != -1) {
				((CurveLithologyBeside) curveLithologyBesideList.get(sequenceNumber)).setSelected(true);
			}
			return (sequenceNumber == -1) ? false : true;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList getCurveSelectedList() {
		curveSelectedList.clear();
		for (int j = 0; j < this.getCurveList().size(); j++) {
			CurvePanel cp = (CurvePanel) curveList.get(j);
			if (cp.isSelected()) {
				curveSelectedList.add(cp);
			}
		}
		for(int j = 0; j < this.parent.getCurveView().getCurveGroups().size(); j++){
			CurveGroup cg = (CurveGroup) this.parent.getCurveView().getCurveGroups().get(j);
			for(int k = 0; k < cg.getCurveList().size(); k++){
				CurvePanel cp = (CurvePanel) cg.getCurveList().get(k);
				if (cp.isSelected()) {
					curveSelectedList.add(cp);
				}
			}
		}
		return curveSelectedList;
	}

	public ArrayList getCurveList() {
		return curveList;
	}

	@SuppressWarnings("unchecked")
	public void addCurveToList(CurvePanel cp) {
		if (!this.isHaveThisCurveInList(cp)) {
			this.curveList.add(cp);
			// need to update the track's type
			if (cp.getCd().getOriginalYType().equals(TrackTypeList.Y_DEPTH_MD)) {
				switch (parent.getTrackType()) {
				case ConstantVariableList.TRACK_TYPE_NONE:
					parent.setTrackType(ConstantVariableList.TRACK_TYPE_NO_SU);
					if(!parent.getParentContainer().getParentInternalFrameView()
							.getJMainFrame().getMainBuffer().isLoadProject()){
						parent.getYAxisView().setYType(TrackTypeList.Y_DEPTH_MD);
					}
					break;
				case ConstantVariableList.TRACK_TYPE_NO_SU:
					break;
				case ConstantVariableList.TRACK_TYPE_SU:
					parent.setTrackType(ConstantVariableList.TRACK_TYPE_MIXED);
					break;
				default:
					break;
				}
			} else if (cp.getCd().getOriginalYType().equals(TrackTypeList.Y_TIME)) {
				switch (parent.getTrackType()) {
				case ConstantVariableList.TRACK_TYPE_NONE:
					parent.setTrackType(ConstantVariableList.TRACK_TYPE_SU);
					if(!parent.getParentContainer().getParentInternalFrameView()
							.getJMainFrame().getMainBuffer().isLoadProject())
						parent.getYAxisView().setYType(TrackTypeList.Y_TIME);
					break;
				case ConstantVariableList.TRACK_TYPE_NO_SU:
					parent.setTrackType(ConstantVariableList.TRACK_TYPE_MIXED);
					break;
				case ConstantVariableList.TRACK_TYPE_SU:
					break;
				default:
					break;
				}
			}
		}
	}

	public void removeCurveFromList(CurvePanel cp) {
		this.curveList.remove(cp);
		// update the track's type
		if(this.curveList.size() == 0) {
			parent.setTrackType(ConstantVariableList.TRACK_TYPE_NONE);
		}
		else if(parent.getTrackType() == ConstantVariableList.TRACK_TYPE_MIXED){
			if (cp.getCd().getOriginalYType().equals(TrackTypeList.Y_DEPTH_MD)) {
				for(int i = 0; i < this.curveList.size(); i++) {
					CurvePanel curvePanel = (CurvePanel)this.curveList.get(i);
					if(curvePanel.getCd().getOriginalYType() == TrackTypeList.Y_DEPTH_MD) {
						return;
					}
				}
				parent.setTrackType(ConstantVariableList.TRACK_TYPE_SU);
			} else if (cp.getCd().getOriginalYType().equals(TrackTypeList.Y_TIME)) {
				for(int i = 0; i < this.curveList.size(); i++) {
					CurvePanel curvePanel = (CurvePanel)this.curveList.get(i);
					if(curvePanel.getCd().getOriginalYType() == TrackTypeList.Y_TIME) {
						return;
					}
				}
				parent.setTrackType(ConstantVariableList.TRACK_TYPE_NO_SU);
			}
		}

	}

	/**
	 * @author luming
	 * @return the max y in all curves, if this is 5890 , return 5900 for good
	 *         views , remain it is times of 100
	 */
	public float getYBottomDepth() {
		boolean hasCurve = false;
		for(int i=0;i<this.parent.getCurveView().getPanelOrder().getOrders().size();i++){
			if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurvePanel){
				hasCurve = true;
				break;
			}
			if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurveGroup){
				if(((CurveGroup)this.parent.getCurveView().getPanelOrder().getPanel(i)).getCurveList().size()>0){
					hasCurve = true;
					break;
				}
			}
			if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof SeismicPanel){
				if(((SeismicPanel)this.parent.getCurveView().getPanelOrder().getPanel(i)).getLogData() != null){
					hasCurve = true;
					break;
				}
			}
		}

		if(!hasCurve){
			this.yBottomDepth = 5000;
			this.ySetBottomDepth = 5000;
		}else{
			int depthUnit = this.parent.getDepthUnit();
			float maxDepth = 0;
			float currentDepth = 0;
			hasCurve = false;
			for(int i=0;i<this.parent.getCurveView().getPanelOrder().getOrders().size();i++){
				currentDepth = maxDepth;
				if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurvePanel){
					CurvePanel cp = (CurvePanel) this.parent.getCurveView().getPanelOrder().getPanel(i);
					double unitTran = 1;
					if(depthUnit == CurveData.METER){
						if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
							unitTran = ConstantVariableList.FOOT_TO_METER;
						}
					}else if(depthUnit == CurveData.FOOT ){
						if(cp.getCd().getDepthUnit() == CurveData.METER || cp.getCd().getDepthUnit() == CurveData.SECOND ){
							unitTran = ConstantVariableList.METER_TO_FOOT;
						}
					}else{
						if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
							unitTran = ConstantVariableList.FOOT_TO_METER;
						}
					}
					currentDepth = cp.getValue(cp.getCd().getMaxValue(),cp.getCd().getOriginalYType(),TrackTypeList.Y_DEPTH_MD,this.parent)*(float)unitTran;
					if(!hasCurve){
						hasCurve = true;
						maxDepth = currentDepth;
					}
				}
				if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurveGroup){
					CurveGroup  curveGroup = (CurveGroup)this.parent.getCurveView().getPanelOrder().getPanel(i);
					for(int j=0; j<curveGroup.getCurveList().size() ;j++){
						CurvePanel cp = (CurvePanel) curveGroup.getCurveList().get(j);
						double unitTran = 1;
						if(depthUnit == CurveData.METER){
							if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
								unitTran = ConstantVariableList.FOOT_TO_METER;
							}
						}else if(depthUnit == CurveData.FOOT ){
							if(cp.getCd().getDepthUnit() == CurveData.METER || cp.getCd().getDepthUnit() == CurveData.SECOND ){
								unitTran = ConstantVariableList.METER_TO_FOOT;
							}
						}else{
							if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
								unitTran = ConstantVariableList.FOOT_TO_METER;
							}
						}
						currentDepth = cp.getValue(cp.getCd().getMaxValue(),cp.getCd().getOriginalYType(),TrackTypeList.Y_DEPTH_MD,this.parent)*(float)unitTran;
						if(!hasCurve){
							hasCurve = true;
							maxDepth = currentDepth;
						}
					}
				}
				if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof SeismicPanel){
					SeismicPanel seismicPanel = (SeismicPanel)this.parent.getCurveView().getPanelOrder().getPanel(i);
					if(seismicPanel.getLogData() != null){
						double unitTran = 1;
						if(depthUnit == CurveData.FOOT ){
							unitTran = ConstantVariableList.METER_TO_FOOT;
						}
						currentDepth = seismicPanel.getMaxDepth()*(float)unitTran;
						if(!hasCurve){
							hasCurve = true;
							maxDepth = currentDepth;
						}
					}
				}
				if(maxDepth < currentDepth){
					maxDepth = currentDepth;
				}
			}

			float newYBottomDepth = ((int) (maxDepth / this.yCutNumber) + 1) * this.yCutNumber;
			if(newYBottomDepth != this.yBottomDepth ){
				this.yBottomDepth = newYBottomDepth;
				if (!this.setDisplayDepth){
			        this.ySetBottomDepth =this.yBottomDepth;
				}
			}
		}
		return this.yBottomDepth;
	}

	/**
	 * @author luming
	 * @return min y , if this is 3750 , we return 3700 for good views remain it
	 *         is times of 100
	 */
	public float getYTopDepth() {
		boolean hasCurve = false;
		for(int i=0;i<this.parent.getCurveView().getPanelOrder().getOrders().size();i++){
			if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurvePanel){
				hasCurve = true;
				break;
			}
			if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurveGroup){
				if(((CurveGroup)this.parent.getCurveView().getPanelOrder().getPanel(i)).getCurveList().size()>0){
					hasCurve = true;
					break;
				}
			}
			if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof SeismicPanel){
				if(((SeismicPanel)this.parent.getCurveView().getPanelOrder().getPanel(i)).getLogData() != null){
					hasCurve = true;
					break;
				}
			}
		}

		if(!hasCurve){
			this.yTopDepth = 1000;
			this.ySetTopDepth = 1000;
		}else{
			int depthUnit = this.parent.getDepthUnit();
			float minDepth = 0;
			float currentDepth = 0;
			hasCurve = false;
			for(int i=0;i<this.parent.getCurveView().getPanelOrder().getOrders().size();i++){
				currentDepth = minDepth;
				if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurvePanel){
					CurvePanel cp = (CurvePanel) this.parent.getCurveView().getPanelOrder().getPanel(i);
					double unitTran = 1;
					if(depthUnit == CurveData.METER){
						if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
							unitTran = ConstantVariableList.FOOT_TO_METER;
						}
					}else if(depthUnit == CurveData.FOOT ){
						if(cp.getCd().getDepthUnit() == CurveData.METER || cp.getCd().getDepthUnit() == CurveData.SECOND ){
							unitTran = ConstantVariableList.METER_TO_FOOT;
						}
					}else{
						if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
							unitTran = ConstantVariableList.FOOT_TO_METER;
						}
					}
					currentDepth = cp.getValue(cp.getCd().getMinValue(),cp.getCd().getOriginalYType(),TrackTypeList.Y_DEPTH_MD,this.parent)*(float)unitTran;
					if(!hasCurve){
						hasCurve = true;
						minDepth = currentDepth;
					}
				}
				if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof CurveGroup){
					CurveGroup  curveGroup = (CurveGroup)this.parent.getCurveView().getPanelOrder().getPanel(i);
					for(int j=0; j<curveGroup.getCurveList().size() ;j++){
						CurvePanel cp = (CurvePanel) curveGroup.getCurveList().get(j);
						double unitTran = 1;
						if(depthUnit == CurveData.METER){
							if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
								unitTran = ConstantVariableList.FOOT_TO_METER;
							}
						}else if(depthUnit == CurveData.FOOT ){
							if(cp.getCd().getDepthUnit() == CurveData.METER || cp.getCd().getDepthUnit() == CurveData.SECOND ){
								unitTran = ConstantVariableList.METER_TO_FOOT;
							}
						}else{
							if(cp.getCd().getDepthUnit() == CurveData.FOOT ){
								unitTran = ConstantVariableList.FOOT_TO_METER;
							}
						}
						currentDepth = cp.getValue(cp.getCd().getMinValue(),cp.getCd().getOriginalYType(),TrackTypeList.Y_DEPTH_MD,this.parent)*(float)unitTran;
						if(!hasCurve){
							hasCurve = true;
							minDepth = currentDepth;
						}
					}
				}
				if(this.parent.getCurveView().getPanelOrder().getPanel(i) instanceof SeismicPanel){
					SeismicPanel seismicPanel = (SeismicPanel)this.parent.getCurveView().getPanelOrder().getPanel(i);
					if(seismicPanel.getLogData() != null){
						double unitTran = 1;
						if(depthUnit == CurveData.FOOT ){
							unitTran = ConstantVariableList.METER_TO_FOOT;
						}
						currentDepth = seismicPanel.getMinDepth()*(float)unitTran;
						if(!hasCurve){
							hasCurve = true;
							minDepth = currentDepth;
						}
					}
				}
				if(currentDepth < minDepth){
					minDepth = currentDepth;
				}
			}

			float newYTopDepth = ((int) (minDepth) / this.yCutNumber) * this.yCutNumber;
			if(newYTopDepth != this.yTopDepth ){
				this.yTopDepth = newYTopDepth;
				if(!this.setDisplayDepth){
			        this.ySetTopDepth = this.yTopDepth;
				}
			}
		}
		return this.yTopDepth;
	}

	public float getYSetBottomDepth() {
		float depthValue = ySetBottomDepth;
		if(this.parent.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVD)){
			depthValue = this.parent.getMDDepthConverter().toDepth(ySetBottomDepth,TrackTypeList.MD_TO_TVD);
		}else if(this.parent.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS )){
			depthValue = this.parent.getMDDepthConverter().toDepth(ySetBottomDepth,TrackTypeList.MD_TO_TVDSS);
		}
		return depthValue;
	}
	public void setYSetBottomDepth(float setBottomDepth) {
		this.ySetBottomDepth = setBottomDepth;
	}
	public float getYSetTopDepth() {
		float depthValue = ySetTopDepth;
		if(this.parent.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVD)){
			depthValue = this.parent.getMDDepthConverter().toDepth(ySetTopDepth,TrackTypeList.MD_TO_TVD);
		}else if(this.parent.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS )){
			depthValue = this.parent.getMDDepthConverter().toDepth(ySetTopDepth,TrackTypeList.MD_TO_TVDSS);
		}
		return depthValue;
	}

	/**
	 * Sets the depth value in MD.
	 * @param setTopDepth
	 */
	public void setYSetTopDepth(float setTopDepth) {
		this.ySetTopDepth = setTopDepth;
	}


	public boolean isCustomizedRangeSet() {
		return setDisplayDepth;
	}

	public void enableCustomizedRange(boolean setDisplayDepth) {
		this.setDisplayDepth = setDisplayDepth;
	}

	public ArrayList getCurveFillBesideList() {
		return curveFillBesideList;
	}
	
	// added by Alex
	public ArrayList getCompoundFillList()	{
		return compoundFillList;
	}

	public ArrayList getCurveFillTopList() {
		return curveFillTopList;
	}

	/**
	 * Returns list og lithologies.
	 * @return curveLithology List(all curveLithologies)
	 */
	public ArrayList<CurveLithologyTop> getCurveLithologyTopList() {
		return curveLithologyTopList;
	}

	public ArrayList<CurveLithologyBeside> getCurveLithologyBesideList() {
		return curveLithologyBesideList;
	}

	/**
	 *
	 * @return the selected curvefillList(only selected curveFills)
	 */
	public CurveFillTop getCurveFillTopSelected() {
		CurveFillTop curveFillSelected = new CurveFillTop();
		for (int i = 0; i < curveFillTopList.size(); i++) {
			if (((CurveFillTop) (curveFillTopList.get(i))).isSelected()) {
				curveFillSelected = (CurveFillTop) curveFillTopList.get(i);
			}
		}
		return curveFillSelected;
	}

	/**
	 * @author luming
	 * @return
	 */
	public CurveFillBeside getCurveFillBesideSelected() {
		CurveFillBeside curveFillBesideSelected = new CurveFillBeside();
		for (int i = 0; i < this.getCurveFillBesideList().size(); i++) {
			if (((CurveFillBeside) (curveFillBesideList.get(i))).isSelected()) {
				curveFillBesideSelected = (CurveFillBeside) curveFillBesideList.get(i);
			}
		}
		return curveFillBesideSelected;
	}

	/**
	 * @author gaofei
	 * @return the selected CurveLithologyTop
	 */
	public CurveLithologyTop getCurveLithologyTopSelected() {
		CurveLithologyTop curveLithologyTopSelected = new CurveLithologyTop();
		for (int i = 0; i < curveLithologyTopList.size(); i++) {
			if (((CurveLithologyTop) (curveLithologyTopList.get(i))).isSelected()) {
				curveLithologyTopSelected = (CurveLithologyTop) curveLithologyTopList.get(i);
			}
		}
		return curveLithologyTopSelected;
	}

	public CurveLithologyBeside getCurveLithologyBesideSelected() {
		CurveLithologyBeside curveLithologyBesideSelected = new CurveLithologyBeside();
		for (int i = 0; i < curveLithologyBesideList.size(); i++) {
			if (((CurveLithologyBeside) (curveLithologyBesideList.get(i))).isSelected()) {
				curveLithologyBesideSelected = (CurveLithologyBeside) curveLithologyBesideList.get(i);
			}
		}
		return curveLithologyBesideSelected;
	}

	public void setCurveFillList(ArrayList curveFillList) {
		this.curveFillTopList = curveFillList;
	}

	@SuppressWarnings("unchecked")
	public void addFillTopToList(CurveFillTop cf) {
		this.curveFillTopList.add(cf);
	}

	@SuppressWarnings("unchecked")
	public void addFillBesideToList(CurveFillBeside cfb) {
		this.curveFillBesideList.add(cfb);
	}
	
	@SuppressWarnings("unchecked")
	public void addCompoundFillToList(CompoundFill cf)	{
		this.compoundFillList.add(cf);
	}

	@SuppressWarnings("unchecked")
	public void addLithologyTopToList(CurveLithologyTop cl) {
		this.curveLithologyTopList.add(cl);
	}

	@SuppressWarnings("unchecked")
	public void addLithologyBesideToList(CurveLithologyBeside clb) {
		this.curveLithologyBesideList.add(clb);
	}

	public boolean isHaveThisCurveInList(CurvePanel cp) {
		for (int i = 0; i < this.getCurveList().size(); i++) {
			if (cp.getCd().getHashCurveName().equals(((CurvePanel) this.getCurveList().get(i)).getCd().getHashCurveName())) {
				return true;
			}
		}
		return false;
	}

	public int getBesideTypeWidth() {
		return besideTypeWidth;
	}

	public void setBesideTypeWidth(int besideTypeWidth) {
		for(int i=0 ; i<curveList.size() ; i++){
			((CurvePanel)curveList.get(i)).setBesideTypeWidth(besideTypeWidth);
		}
		this.besideTypeWidth = besideTypeWidth;
	}

	@SuppressWarnings("unchecked")
	public void freshMeanAndP105090() {
		if (this.isP105090meanRefresh() == false) {
			return;
		}
		if (!this.getCurveList().isEmpty()) {
			CurveP105090Mean cpm = new CurveP105090Mean(this);
			cpm.setCurveList(this.getCurveList());

			if (!cpm.constructP105090Mean()) {
				JOptionPane.showMessageDialog(null, "Can't create P105090 and Mean. \nCurves in this track don't have the same depth increment!", "Message", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			this.meanCurve = cpm.getMean();
			this.p105090.clear();
			this.p105090.add(cpm.getP10());
			this.p105090.add(cpm.getP50());
			this.p105090.add(cpm.getP90());
		}
		this.setP105090meanRefresh(false);
	}

	public CurvePanel getMeanCurve() {
		return meanCurve;
	}

	public ArrayList getP105090() {
		return p105090;
	}

	public void addMeanCurveToList() {
		this.addCurveToList(this.getMeanCurve());
	}

	public void addP105090ToList() {
		for (int i = 0; i < this.getP105090().size(); i++) {
			this.addCurveToList((CurvePanel) this.getP105090().get(i));
		}
	}

	public void deleteMeanP105090CurveFromList() {
		for (int i = 0; i < this.getCurveList().size(); i++) {
			CurvePanel cp = (CurvePanel) this.getCurveList().get(i);
			String name = cp.getCd().getCurveName();
			if (name.equals(ConstantVariableList.MEAN) || name.equals(ConstantVariableList.P10) || name.equals(ConstantVariableList.P50) || name.equals(ConstantVariableList.P90)) {
				this.removeCurveFromList(cp);
				i--;
			}
		}
	}

	public void setCurveList(ArrayList curveList) {
		this.curveList = curveList;
	}

	public boolean isP105090meanRefresh() {
		return p105090meanRefresh;
	}

	public void setP105090meanRefresh(boolean refresh) {
		p105090meanRefresh = refresh;
	}

	public boolean isPaintLine() {
		return paintLine;
	}

	public void setPaintLine(boolean paintLine) {
		this.paintLine = paintLine;
	}

	public float getYSetTopTime(){
		if(this.parent.getDepthToTimeConverter()!=null && this.getYSetTopDepth() != 0){
			return this.parent.getDepthToTimeConverter().toTime(this.getYSetTopDepth());
		}
		return this.getYTopTime();
	}

	public float getYSetBottomTime(){
		if(this.parent.getDepthToTimeConverter()!=null && this.getYSetBottomDepth() != 0){
			return this.parent.getDepthToTimeConverter().toTime(this.getYSetBottomDepth());
		}
		return this.getYBottomTime();
	}

	public float getYTopTime() {
		if (this.getCurveList().isEmpty()) {
			this.yTopTime = 1000;
		} else {
			float temp[] = new float[this.getCurveList().size()];
			CaculateClass cc = new CaculateClass();
			for (int i = 0; i < this.getCurveList().size(); i++) {
				CurvePanel cp = (CurvePanel) this.getCurveList().get(i);
				temp[i] = cp.getValue(cp.getCd().getMinValue(),cp.getCd().getOriginalYType(),TrackTypeList.Y_TIME,this.parent);
			}
			cc.rankTheArray(temp);
			this.yTopTime = cc.getMin();

		}
		return this.yTopTime;
	}

	public void setYTopTime(float topTime) {
		yTopTime = topTime;
	}

	public float getYBottomTime() {
		if (this.getCurveList().isEmpty()) {
			this.yBottomTime = 10;
		} else {
			float temp[] = new float[this.getCurveList().size()];
			CaculateClass cc = new CaculateClass();
			for (int i = 0; i < this.getCurveList().size(); i++) {
				CurvePanel cp = (CurvePanel) this.getCurveList().get(i);
				temp[i] = cp.getValue(cp.getCd().getMaxValue(),cp.getCd().getOriginalYType(),TrackTypeList.Y_TIME,this.parent);
			}
			cc.rankTheArray(temp);
			this.yBottomTime = cc.getMax();
		}
		return this.yBottomTime;
	}

	public void setYBottomTime(float bottomTime) {
		yBottomTime = bottomTime;
	}

	public int getCurveIndex(CurvePanel cp){
		for(int i=0 ; i<curveList.size() ; i++){
			if(((CurvePanel)curveList.get(i)).getCd().getHashCurveName().equals(cp.getCd().getHashCurveName()))
				return i;
		}
		return -1;
	}

	public Element saveXML(Document doc){
		Element xmlCurveLayer = doc.createElement("CurveLayer");
		xmlCurveLayer.setAttribute("type",this.getLayerType());
		xmlCurveLayer.setAttribute("YSetTopDepth",Float.toString(this.ySetTopDepth));
		xmlCurveLayer.setAttribute("YSetBottomDepth",Float.toString(this.ySetBottomDepth));
		xmlCurveLayer.setAttribute("setDisplayDepth",Boolean.toString(this.setDisplayDepth));

		Element xmlCurveFillBesideList = doc.createElement("CurveFillBesideList");
		for(int i = 0 ; i < this.getCurveFillBesideList().size(); i++){
			CurveFillBeside curveFillBeside = (CurveFillBeside) this.getCurveFillBesideList().get(i);
			xmlCurveFillBesideList.appendChild(curveFillBeside.saveXML(doc,i));
		}
		xmlCurveLayer.appendChild(xmlCurveFillBesideList);
		
		// CompoundFill. Added by Alex. may 30 20070
		Element xmlCompoundFillList = doc.createElement("CompoundFillList");
		for (int i = 0; i < this.compoundFillList.size(); i++){
			CompoundFill cf = (CompoundFill) this.compoundFillList.get(i);
			xmlCompoundFillList.appendChild(cf.saveXML(doc,i));
		}
		xmlCurveLayer.appendChild(xmlCompoundFillList);

		Element xmlCurveLithologyBesideList = doc.createElement("CurveLithologyBesideList");
		for(int i = 0 ; i < this.getCurveLithologyBesideList().size(); i++){
			CurveLithologyBeside curveLithologyBeside = (CurveLithologyBeside) this.getCurveLithologyBesideList().get(i);
			xmlCurveLithologyBesideList.appendChild(curveLithologyBeside.saveXML(doc,i));
		}
		xmlCurveLayer.appendChild(xmlCurveLithologyBesideList);

		Element xmlCurveFillTopList = doc.createElement("CurveFillTopList");
		for(int i = 0 ; i < this.getCurveFillTopList().size(); i++){
			CurveFillTop curveFillTop = (CurveFillTop) this.getCurveFillTopList().get(i);
			xmlCurveFillTopList.appendChild(curveFillTop.saveXML(doc,i));
		}
		xmlCurveLayer.appendChild(xmlCurveFillTopList);

		Element xmlCurveLithologyTopList = doc.createElement("CurveLithologyTopList");
		for(int i = 0 ; i < this.getCurveLithologyTopList().size(); i++){
			CurveLithologyTop curveLithologyTop = (CurveLithologyTop) this.getCurveLithologyTopList().get(i);
			xmlCurveLithologyTopList.appendChild(curveLithologyTop.saveXML(doc,i));
		}
		xmlCurveLayer.appendChild(xmlCurveLithologyTopList);

		return xmlCurveLayer;
	}

	public void loadXML(Element xmlCurveLayer, CurveView cv){
		this.setYSetTopDepth(Float.parseFloat(xmlCurveLayer.getAttribute("YSetTopDepth")));
		this.setYSetBottomDepth(Float.parseFloat(xmlCurveLayer.getAttribute("YSetBottomDepth")));
		String isDisplayDepth = xmlCurveLayer.getAttribute("setDisplayDepth");
		if(isDisplayDepth != ""){
			this.enableCustomizedRange(ConversionClass.stringToBoolean(isDisplayDepth));
		}
		Element xmlCurveFillBesideList = (Element) xmlCurveLayer.getElementsByTagName("CurveFillBesideList").item(0);
		NodeList nodeList = null;
		nodeList = xmlCurveFillBesideList.getElementsByTagName("CurveFillBeside");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlCurveFillBeside = (Element) nodeList.item(i);
			CurveFillBeside curveFillBeside = new CurveFillBeside(); 
			curveFillBeside.loadXML(xmlCurveFillBeside,cv);
			this.addFillBesideToList(curveFillBeside);
		}
		
		// compoundFill. Added by Alex may 30, 2007
		Element xmlCompoundFillList = (Element) xmlCurveLayer.getElementsByTagName("CompoundFillList").item(0);
		if (xmlCompoundFillList!=null)	{
			nodeList = null;
			nodeList = xmlCompoundFillList.getElementsByTagName("CompoundFill");
			for (int i = 0; i < nodeList.getLength(); i++)	{
				Element xmlCompoundFill = (Element) nodeList.item(i);
				CompoundFill cf = new CompoundFill();
				cf.loadXML(xmlCompoundFill, cv);
				this.addCompoundFillToList(cf);
			}
		}		
		
		Element xmlCurveLithologyBesideList = (Element) xmlCurveLayer.getElementsByTagName("CurveLithologyBesideList").item(0);
		nodeList = null;
		nodeList = xmlCurveLithologyBesideList.getElementsByTagName("CurveLithologyBeside");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlCurveLithologyBeside = (Element) nodeList.item(i);
			CurveLithologyBeside curveLithologyBeside = new CurveLithologyBeside();
			curveLithologyBeside.loadXML(xmlCurveLithologyBeside,cv);
			this.addLithologyBesideToList(curveLithologyBeside);
		}

		Element xmlCurveFillTopList = (Element) xmlCurveLayer.getElementsByTagName("CurveFillTopList").item(0);
		nodeList = null;
		nodeList = xmlCurveFillTopList.getElementsByTagName("CurveFillTop");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlCurveFillTop = (Element) nodeList.item(i);
			CurveFillTop curveFillTop = new CurveFillTop();
			curveFillTop.loadXML(xmlCurveFillTop,cv);
			this.addFillTopToList(curveFillTop);
		}

		Element xmlCurveLithologyTopList = (Element) xmlCurveLayer.getElementsByTagName("CurveLithologyTopList").item(0);
		nodeList = null;
		nodeList = xmlCurveLithologyTopList.getElementsByTagName("CurveLithologyTop");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element xmlCurveLithologyTop = (Element) nodeList.item(i);
			CurveLithologyTop curveLithologyTop = new CurveLithologyTop();
			curveLithologyTop.loadXML(xmlCurveLithologyTop,cv);
			this.addLithologyTopToList(curveLithologyTop);
		}
	}

	/**
	 * @see com.gwsys.welllog.core.Renderable#updateBorderByPoints
	 * (java.awt.Point, int, int)
	 */
	public void updateBorderByPoints(Point point, int top, int bottom) {

	}
}
