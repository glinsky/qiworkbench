/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view.curve;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.data.seismic.segy.StandardSegyFormat;
import com.gwsys.data.seismic.su.SuToLogDataConverter;
import com.gwsys.data.well.LogData;
import com.gwsys.data.well.SegyAdapter;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.dialogs.ChooserDialog;
import com.gwsys.welllog.view.dialogs.EditSeismicSettingDialog;
import com.gwsys.welllog.view.dialogs.SetTimeToDepthDialog;

/**
 *
 * Create panel to hold seismic data.
 * @author Administrator
 * @version 1.0
 */
public class SeismicPanel extends JPanel implements MouseListener,
		ActionListener {

	private static final long serialVersionUID = 1L;

	private LogData logData = null;

	private CurveView parent = null;

	private String name = "";

	private int besideTypeWidth = ConstantVariableList.SEISMIC_WIDTH;

	private int traceNumber = 20;

	private String filePath ="";

	private ArrayList shapeList = new ArrayList();

	private ArrayList displayShapeList = new ArrayList();

	private Color curveColor = Color.black;

	public static final int BEELINE = 0;
	public static final int DASHED = 1;
	public static int DASH_DOT = 2;
	public static int DOT = 3;

	private int stokeType = BEELINE;

	private float strokeWidth = ConstantVariableList.CURVE_STROKE;

	private float maxDepth = 0;

	private float minDepth = 0;

	private float minX[],maxX[],minY[],maxY[];

	public SeismicPanel() {
		super();
	}

	public SeismicPanel(CurveView parent, String name) {
		super();
		this.parent = parent;
		this.name = name;

		this.addMouseListener(this);
		this.setPreferredSize(ConstantVariableList.SEISMIC_PREFERREDSIZE);
		this.setBackground(ConstantVariableList.SEISMIC_BACKGROUND);
		this.setBorder(new LineBorder(Color.BLACK, 1));
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g.create();

		print(g2,0,1,1);

	}

	public void print(Graphics2D g2, int left, float rateWidth, float rateHeight){
		g2.setColor(this.curveColor);

		g2.setStroke(getStroke());

		setShapeList(left,rateWidth, rateHeight);

		for(int i = 0 ; i<displayShapeList.size();i++){
			Shape shape = (Shape)displayShapeList.get(i);
			g2.draw(shape);
		}
	}

	public BasicStroke getStroke(){
		BasicStroke stroke = null;
		if(stokeType == BEELINE)
			stroke = new BasicStroke(this.getStrokeWidth());
		else if(stokeType == DASHED){
			float miterLimit = 10.0F;
			float[] dashPattern = { 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F };
			float dashPhase = 0;
			stroke = new BasicStroke(this.getStrokeWidth() , BasicStroke.CAP_ROUND,
					BasicStroke.JOIN_ROUND, miterLimit, dashPattern, dashPhase);
		}else if (stokeType == DASH_DOT){
			float miterLimit = 5.0F;
			float[] dashPattern = { 5F, 5f, 30F, 5f, 5F, 5F , 30F, 5F, 5f, 5F,
					30F,5f, 5F , 5F , 30F , 5F , 5F , 5F , 30F , 5F ,
					5F , 5F , 30F , 5F , 5F , 5F , 30F , 5F , 5F , 5F , 30F , 5F ,
					 5F, 5f, 30F, 5f, 5F, 5F , 30F, 5F, 5f, 5F,30F , 5F,
					 5F, 5f, 30F, 5f, 5F, 5F , 30F, 5F, 5f, 5F,};
			float dashPhase = 0;
			stroke = new BasicStroke(this.getStrokeWidth(),
					BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, miterLimit,
					dashPattern, dashPhase);
		} else if (stokeType == DOT) {
			float miterLimit = 1.0F;
			float[] dashPattern = { 1F, 5F, 1F , 5F , 1F , 5F , 1F , 5F , 1F , 5F ,
					1F , 5F , 1F , 5F, 1F , 5F , 1F , 5F , 1F , 5F , 1F , 5F , 1F , 5F};
			float dashPhase = 0;
			stroke = new BasicStroke(this.getStrokeWidth(),
					BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, miterLimit,
					dashPattern, dashPhase);
		}

		return stroke;
	}

	public int getBesideTypeWidth() {
		return besideTypeWidth;
	}

	public void setBesideTypeWidth(int besideTypeWidth) {
		this.besideTypeWidth = besideTypeWidth;
	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {
			JPopupMenu popup = new JPopupMenu();

			JMenuItem importItem = new JMenuItem("Import");
			popup.add(importItem);

			popup.addSeparator();

			JMenuItem settingItem = new JMenuItem("Settings");
			popup.add(settingItem);

			popup.addSeparator();

			JMenuItem deleteItem = new JMenuItem("Delete");
			popup.add(deleteItem);

			popup.show(this, e.getX(), e.getY());

			importItem.addActionListener(this);
			importItem.setActionCommand("Import");

			settingItem.addActionListener(this);
			settingItem.setActionCommand("Settings");

			deleteItem.addActionListener(this);
			deleteItem.setActionCommand("Delete");
		}
	}

	public void mouseReleased(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Import")) {
			importData();
		} else if (e.getActionCommand().equals("Settings")) {
			settings();
		} else if (e.getActionCommand().equals("Delete")) {
			delete();
		}
	}

	public CurveView getParent() {
		return parent;
	}

	public void setParent(CurveView parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LogData getLogData() {
		return logData;
	}

	public void setLogData(LogData logData) {
		this.logData = logData;
	}

	public int getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(int traceNumber) {
		this.traceNumber = traceNumber;
	}

	private void importData(){
		ChooserDialog chooser = new ChooserDialog(DataBuffer.getJMainFrame(),
				ChooserDialog.getPath());
		chooser.clearFilter();
		chooser.setFilter("sgy");
		chooser.setFilter("su");
		chooser.setMultiSelect(false);
		chooser.showDialog();

		DataBuffer.getJMainFrame().setFileChooser(chooser.getJfc());

		if(!chooser.isOpen()){
			return;
		}
		File file = chooser.getFile();

		if (file == null) {
			JOptionPane.showMessageDialog(DataBuffer.getJMainFrame().getFrame(),
					"Can not read sgy file." , "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		ChooserDialog.setPath(file.getPath());
		setShapeFromFile(file);
		this.parent.getParentAxisCurveContainer().getYAxisView().reCompute();
	}

	@SuppressWarnings("unchecked")
	public void setShapeFromFile(File file) {
		if (file == null) {
			return;
		}
		// read and initial

		boolean isSelectCheckShotFile = false;
    	if(this.parent.getParentAxisCurveContainer().getTimeToDepthConverter() == null){
    		SetTimeToDepthDialog setTimeToDepthDialog = new SetTimeToDepthDialog(
    				parent.getParentAxisCurveContainer(), null, true);
    		setTimeToDepthDialog.setVisible(true);
    		isSelectCheckShotFile = setTimeToDepthDialog.isSelectCheckShotFile();
    	}else{
    		isSelectCheckShotFile = true;
    	}

    	if(!isSelectCheckShotFile){
    		return;
    	}

		String fileName=file.getName();
	    int index=fileName.lastIndexOf('.');
	    String extension=fileName.substring(index+1).toLowerCase();

	    if (extension.equalsIgnoreCase("sgy")){
			try {
				SegyAdapter segyReader = new SegyAdapter(file.getAbsolutePath(),
						new StandardSegyFormat(), this.getTraceNumber());


				this.setLogData(segyReader.getLogData());
			} catch (FileNotFoundException fnfe) {
				JOptionPane.showMessageDialog(null ,
						"Can not read SEG-Y file : " + fileName + "\nError:\nThe wrong file format.",
						"Error" , JOptionPane.ERROR_MESSAGE);
				return;
			}

	    }

	    if (extension.equalsIgnoreCase("su")){
	    	LogData logDataNew = SuToLogDataConverter.convert(file, traceNumber);
	    	if(logDataNew == null) {
	    		JOptionPane.showMessageDialog(null ,
	    				"Can not read Su file : " + fileName + "\nError:\nThe wrong file format.",
						"Error" , JOptionPane.ERROR_MESSAGE);
	    		return;
			}
	    	else{
	    		this.logData = logDataNew;
	    	}
	    }

	    this.filePath = file.getPath();

	    if(this.logData!=null){
			String curveNames[] = logData.getAllCurveNames();
			HashMap valueMap = logData.getCurveValueMap();
			//BigDecimal allCurveValue[];
			float[] fy = (float[]) logData.getCurveValueByName(logData.getAllCurveNames()[0]);

			shapeList.clear();
			minX = new float[curveNames.length];
			maxX = new float[curveNames.length];
			minY = new float[curveNames.length];
			maxY = new float[curveNames.length];
			for (int i = 1; i < curveNames.length; i++) {
				float[] fx = (float[]) valueMap.get(curveNames[i]);

				minX[i-1] = OperationClass.getMinNum(fx);
				maxX[i-1] = OperationClass.getMaxNum(fx);
				minY[i-1] = OperationClass.getMinNum(fy);
				maxY[i-1] = OperationClass.getMaxNum(fy);
				shapeList.add(OperationClass.drawShape(fx, fy));

				float maxDepth = this.getParent().getParentAxisCurveContainer().
				getTimeToDepthConverter().toDepth(maxY[i-1],TrackTypeList.TIME_TO_MD);
				float minDepth = this.getParent().getParentAxisCurveContainer().
				getTimeToDepthConverter().toDepth(minY[i-1],TrackTypeList.TIME_TO_MD);

				if(this.minDepth == 0 || minDepth < this.minDepth){
					this.minDepth = minDepth;
				}
				if(this.maxDepth == 0 || maxDepth > this.maxDepth){
					this.maxDepth = maxDepth;
				}
			}
	    }
	}

	@SuppressWarnings("unchecked")
	private void setShapeList(int left, float rateWidth, float rateHeight){
		if(shapeList.size()==0){
			return;
		}
		int perWidth = (int)((besideTypeWidth * rateWidth)/ traceNumber);
		int width = left;
		displayShapeList.clear();
		for(int i = 0 ; i<shapeList.size();i++){
			Shape shape = (Shape)shapeList.get(i);
			displayShapeList.add(getTraceShape(shape,minX[i],maxX[i],minY[i],maxY[i],
					width,0,parent,rateWidth,rateHeight));
			width = width + perWidth;
		}
	}

	private Shape getTraceShape(Shape shape, float minX, float maxX, float minTime, float maxTime,
			float xMove, float yMove, CurveView cv, float rateWidth, float rateHeight){
		float maxDepth = this.getParent().getParentAxisCurveContainer().
		getTimeToDepthConverter().toDepth(maxTime,TrackTypeList.TIME_TO_MD);
		float minDepth = this.getParent().getParentAxisCurveContainer().
		getTimeToDepthConverter().toDepth(minTime,TrackTypeList.TIME_TO_MD );
		String type = getParent().getParentAxisCurveContainer().getYAxisView().getYType();
		if(type.equals(TrackTypeList.Y_DEPTH_TVD)){
			maxDepth = this.getParent().getParentAxisCurveContainer().
			getMDDepthConverter().toDepth(maxDepth,TrackTypeList.MD_TO_TVD);
			minDepth = this.getParent().getParentAxisCurveContainer().
			getMDDepthConverter().toDepth(minDepth,TrackTypeList.MD_TO_TVD);
		}else if(type.equals(TrackTypeList.Y_DEPTH_TVDSS)){
			maxDepth = this.getParent().getParentAxisCurveContainer().
			getMDDepthConverter().toDepth(maxDepth,TrackTypeList.MD_TO_TVDSS);
			minDepth = this.getParent().getParentAxisCurveContainer().
			getMDDepthConverter().toDepth(minDepth,TrackTypeList.MD_TO_TVDSS);
		}

		Shape shapeLast;
		double xScale, yScale, xTranslate, yTranslate;

		xTranslate = -(double) minX;// move the min x to the left
		xScale = (double) (((double)this.besideTypeWidth/(double)this.traceNumber) / (double)(maxX - minX));

		int curveHeight = (int) ((this.getHeight()*rateHeight * (maxDepth - minDepth)) /
				(cv.getCurveLayer().getYSetBottomDepth() - cv.getCurveLayer().getYSetTopDepth()));
		yScale = (double) ((double)(curveHeight) / (double)(maxTime - minTime));
		yTranslate = -minTime;
		yMove = (this.getHeight()*rateHeight * (minDepth - cv.getCurveLayer().getYSetTopDepth()) /
				(cv.getCurveLayer().getYSetBottomDepth() - cv.getCurveLayer().getYSetTopDepth()));

		AffineTransform t = new AffineTransform();
		AffineTransform t1 = new AffineTransform();
		AffineTransform t2 = new AffineTransform();
		t.translate(xTranslate, yTranslate);
		t1.scale(xScale, yScale);
		t2.translate(xMove, yMove);
		shapeLast = t2.createTransformedShape(t1.createTransformedShape(t.createTransformedShape(shape)));

		return shapeLast;
	}

	public void delete(){
		ViewCoreOperator.removeSeismicPanel(
				this.getParent().getParentAxisCurveContainer().getWellName(), this.name);
	}

	public void settings(){
		EditSeismicSettingDialog settingDlg = new EditSeismicSettingDialog(this);
		settingDlg.setVisible(true);
	}

	public Element saveXML(Document doc){
		Element xmlSeismicPanel = doc.createElement("SeismicPanel");
		xmlSeismicPanel.setAttribute("name",this.getName());
		xmlSeismicPanel.setAttribute("traceNumber",Integer.toString(this.getTraceNumber()));
		xmlSeismicPanel.setAttribute("besideTypeWidth",Integer.toString(this.getBesideTypeWidth()));
		xmlSeismicPanel.setAttribute("curveColor",Integer.toString(this.getCurveColor().getRGB()));
		xmlSeismicPanel.setAttribute("stokeType",Integer.toString(this.getStokeType()));
		xmlSeismicPanel.setAttribute("strokeWidth",Float.toString(this.getStrokeWidth()));
		xmlSeismicPanel.setAttribute("path",this.getFilePath());
		return xmlSeismicPanel;
	}

	public void loadXML(Element xmlSeismicPanel){
		this.setTraceNumber(Integer.parseInt(xmlSeismicPanel.getAttribute("traceNumber")));
		this.setBesideTypeWidth(Integer.parseInt(xmlSeismicPanel.getAttribute("besideTypeWidth")));
		this.setCurveColor(new Color(Integer.parseInt(xmlSeismicPanel.getAttribute("curveColor"))));
		this.setStokeType(Integer.parseInt(xmlSeismicPanel.getAttribute("stokeType")));
		this.setStrokeWidth(Float.parseFloat(xmlSeismicPanel.getAttribute("strokeWidth")));
		if(xmlSeismicPanel.getAttribute("path")!=""){
			File file = new File(xmlSeismicPanel.getAttribute("path"));
			this.setShapeFromFile(file);
		}
	}

	public Color getCurveColor() {
		return curveColor;
	}

	public void setCurveColor(Color curveColor) {
		this.curveColor = curveColor;
	}

	public int getStokeType() {
		return stokeType;
	}

	public void setStokeType(int stokeType) {
		this.stokeType = stokeType;
	}

	public float getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(float strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	public float getMaxDepth() {
		return maxDepth;
	}

	public float getMinDepth() {
		return minDepth;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
