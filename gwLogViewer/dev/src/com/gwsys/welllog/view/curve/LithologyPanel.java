/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.dialogs.EditLithologyObjectDialog;
import com.gwsys.welllog.view.dialogs.EditLithologySettingDialog;
import com.gwsys.welllog.view.zone.ZoneProp;

/**
 * This panel draws litho patterns.
 * @author Team
 *
 */
public class LithologyPanel extends JPanel implements
		MouseListener,Serializable ,ActionListener{

	private static final long serialVersionUID = 1L;

	private CurveView parent = null;

	private ArrayList objectList = new ArrayList();

	private LithologyObject selectLithologyObject = null;

	private String name = "";

	private int besideTypeWidth = ConstantVariableList.DESCRIPTION_WIDTH;

	private int defaultFontSize = 10;

	private boolean fillRedrawFlag =false;

	public LithologyPanel(CurveView parent,String name){
		super();
		this.parent = parent;
		this.name = name;

		this.addMouseListener(this);
		this.setPreferredSize(ConstantVariableList.DESCRIPTION_PREFERREDSIZE);
		this.setBackground(ConstantVariableList.DESCRIPTION_BACKGROUND);
		this.setBorder(new LineBorder(Color.BLACK,1));
		this.setLayout(null);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g.create();

		print(g2,0, 1, 1);

		this.setFillRedrawFlag(false);
	}

	public void print(Graphics2D g2, int left, float rateWidth, float rateHeight){
		for(int i =0 ; i < objectList.size() ; i++){
			LithologyObject lithologyObject = (LithologyObject)objectList.get(i);
			lithologyObject.getPanelLithology().constructFill(this,left,
					rateWidth,rateHeight);

			if(lithologyObject.getPanelLithology().isSelected()){
				TexturePaint texturePaint = ZoneProp.getWellLogPatterns()
				[lithologyObject.getPanelLithology().getWhichtexture()-1];
				g2.setPaint(texturePaint);
				g2.fill(lithologyObject.getPanelLithology().getMyFill());
				g2.setColor(lithologyObject.getFillColor());
				g2.setXORMode(Color.white);
				g2.fill(lithologyObject.getPanelLithology().getMyFill());
			}else{
				g2.setColor(lithologyObject.getFillColor());
				g2.fill(lithologyObject.getPanelLithology().getMyFill());
			}
			if (!lithologyObject.getText().equals("")){
				Color textColor = new Color((255-lithologyObject.getFillColor().getRed()),
						  (255-lithologyObject.getFillColor().getGreen()),
						  (255-lithologyObject.getFillColor().getBlue()));
				g2.setColor(textColor);

				String text = lithologyObject.getText();
				int textWidth = g2.getFontMetrics().stringWidth(text);
				int textHeight = g2.getFontMetrics().getHeight();
				int x = 0;
				int y = 0;
				int yEnd = (int)(lithologyObject.getYEnd()*rateHeight);
				int yStart = (int)(lithologyObject.getYStart()*rateHeight);
				if (lithologyObject.getOrientation() == LithologyObject.ORIENTATION_HORIZONTAL){
					x = (int)((int)(this.besideTypeWidth*rateWidth) - textWidth)/2;
					if(yEnd>= yStart){
						y = (int)(yStart + (yEnd - yStart + textHeight)/2);
					}else{
						y = (int)(yEnd + (yStart - yEnd + textHeight)/2);
					}
					g2.drawString(text,left+x,y);

				}else if(lithologyObject.getOrientation() == LithologyObject.ORIENTATION_VERTICAL){
					x = (int)(this.besideTypeWidth*rateWidth/2)+left;
					if(yEnd >= yStart){
						y = yStart + (yEnd - yStart + textWidth)/2;
					}else{
						y = yEnd + (yStart - yEnd + textWidth)/2;
					}
					g2.rotate(-Math.PI/2,x,y);
					g2.translate(x,y);
					g2.drawString(text,0,0);
				}

			}
		}
	}

	public int getBesideTypeWidth() {
		return besideTypeWidth;
	}

	public void setBesideTypeWidth(int besideTypeWidth) {
		this.besideTypeWidth = besideTypeWidth;
	}

	public int getDefaultFontSize() {
		return defaultFontSize;
	}

	public void setDefaultFontSize(int defaultFontSize) {
		this.defaultFontSize = defaultFontSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList getObjectList() {
		return objectList;
	}

	public void setObjectList(ArrayList objectList) {
		this.objectList = objectList;
	}

	public CurveView getParent() {
		return parent;
	}

	public void setParent(CurveView parent) {
		this.parent = parent;
	}


	public boolean isFillRedrawFlag() {
		return fillRedrawFlag;
	}

	public void setFillRedrawFlag(boolean fillRedrawFlag) {
		this.fillRedrawFlag = fillRedrawFlag;
	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {
		if(e.getButton()==MouseEvent.BUTTON3){
			selectLithologyObject = this.getLithologyObject(e.getY());
			if(selectLithologyObject == null){
				JPopupMenu popup = new JPopupMenu();
				JMenuItem settingItem = new JMenuItem("Settings");
				popup.add(settingItem);
				popup.addSeparator();

				JMenuItem insertItem = new JMenuItem("Insert");
				popup.add(insertItem);

				popup.addSeparator();

				JMenuItem deleteItem = new JMenuItem("Delete");
				popup.add(deleteItem);

				popup.show(this, e.getX(), e.getY());

				settingItem.addActionListener(this);
				settingItem.setActionCommand("LithologySettings");
				insertItem.addActionListener(this);
				insertItem.setActionCommand("LithologyInsert");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("LithologyDelete");
			}else{
				JPopupMenu popup = new JPopupMenu();
				JMenuItem settingItem = new JMenuItem("Settings");
				popup.add(settingItem);
				popup.addSeparator();

				JMenuItem deleteItem = new JMenuItem("Delete");
				popup.add(deleteItem);

				popup.show(this, e.getX(), e.getY());

				settingItem.addActionListener(this);
				settingItem.setActionCommand("ObjectSettings");
				deleteItem.addActionListener(this);
				deleteItem.setActionCommand("ObjectDelete");
			}
		}
	}

	public void mouseReleased(MouseEvent e) {

	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("LithologySettings")){
			lithologySettings();
		}else if(e.getActionCommand().equals("LithologyInsert")){
			lithologyInsert();
		}else if(e.getActionCommand().equals("LithologyDelete")){
			String wellName = parent.getParentAxisCurveContainer().getWellName();
			ViewCoreOperator.removeLithologyPanel(wellName , this.name);
		}else if(e.getActionCommand().equals("ObjectSettings")){
			objectSettings();
		}else if(e.getActionCommand().equals("ObjectDelete")){
			this.objectList.remove(selectLithologyObject);
			this.repaint();
		}
	}

	private void lithologySettings(){
		EditLithologySettingDialog settingDlg = new EditLithologySettingDialog(this);
		settingDlg.setVisible(true);
	}

	@SuppressWarnings("unchecked")
	private void lithologyInsert(){
		LithologyObject lithologyObject = new LithologyObject(this);
		EditLithologyObjectDialog editLithologyObjectDialog = new EditLithologyObjectDialog(lithologyObject);
		editLithologyObjectDialog.setVisible(true);
		if(lithologyObject.getPanelLithology() == null){
			return;
		}
		lithologyObject.getPanelLithology().constructFill(this,0,1,1);
		this.objectList.add(lithologyObject);
		this.repaint();
	}

	private void objectSettings(){
		EditLithologyObjectDialog editLithologyObjectDialog = new EditLithologyObjectDialog(selectLithologyObject);
		editLithologyObjectDialog.setVisible(true);
		selectLithologyObject.getPanelLithology().constructFill(this,0,1,1);
		this.repaint();
	}

	private LithologyObject getLithologyObject(int pointY){
		int size = this.getObjectList().size()-1;
		for(int i = 0; i < this.getObjectList().size(); i++){
			LithologyObject lithologyObject = (LithologyObject)this.getObjectList().get(size - i);
			if(pointY>=lithologyObject.getYStart() && pointY <= lithologyObject.getYEnd()){
				return lithologyObject;
			}
		}
		return null;
	}

	public Element saveXML(Document doc){
		Element xmlLithologyPanel = doc.createElement("LithologyPanel");
		xmlLithologyPanel.setAttribute("name",this.getName());
		xmlLithologyPanel.setAttribute("besideTypeWidth",Integer.toString(this.getBesideTypeWidth()));
		xmlLithologyPanel.setAttribute("defaultFontSize",Integer.toString(this.getDefaultFontSize()));
		xmlLithologyPanel.setAttribute("size",Integer.toString(this.getObjectList().size()));
		for(int i = 0; i < this.getObjectList().size(); i++){
			LithologyObject lithologyObject = (LithologyObject)this.getObjectList().get(i);
			xmlLithologyPanel.appendChild(lithologyObject.saveXML(doc,i));
		}
		return xmlLithologyPanel;
	}

	@SuppressWarnings("unchecked")
	public void loadXML(Element xmlLithologyPanel){
		this.setName(xmlLithologyPanel.getAttribute("name"));
		this.setBesideTypeWidth(Integer.parseInt(xmlLithologyPanel.getAttribute("besideTypeWidth")));
		this.setDefaultFontSize(Integer.parseInt(xmlLithologyPanel.getAttribute("defaultFontSize")));
		int lithologyPanelSize =  Integer.parseInt(xmlLithologyPanel.getAttribute("size"));
		for(int i = 0; i < lithologyPanelSize; i++){
			Element xmlLithologyObject = (Element) xmlLithologyPanel.getElementsByTagName("LithologyObject"+i).item(0);
			LithologyObject lithologyObject = new LithologyObject(this);
			lithologyObject.loadXML(xmlLithologyObject);
			this.objectList.add(lithologyObject);
		}
	}
}
