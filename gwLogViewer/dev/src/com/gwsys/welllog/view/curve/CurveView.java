/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.TransferHandler;
import javax.swing.border.LineBorder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.Renderable;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.SaveWorkSpaceUtil;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.GridLayer;
import com.gwsys.welllog.view.track.YAxisView;

/**
 * @author luming show grid , curve
 */
public class CurveView extends JPanel {

	private static final long serialVersionUID = 1L;

	private AxisCurveContainerView parent = null; // AxisCurveContainerView

	private YAxisView yAxisView = null; // y axis

	private Color curveDefaultColor = null;

	private String transferCurveName = ""; // the curve name which draged

	private boolean markerRedrawFlag = true; // need re compute???

	private boolean curveRedrawFlag = true; // need re compute???

	private boolean fillRedrawFlag = true;

	private float defaultstroke = ConstantVariableList.CURVE_STROKE;

	private CurveViewController curveViewController;

	private CurveLayer cl = null;

	private PanelOrder panelOrder = new PanelOrder();

	private ArrayList curveGroups = new ArrayList();

	private ArrayList descriptions = new ArrayList();

	private ArrayList seismics = new ArrayList();

	private ArrayList lithologys = new ArrayList();

	private boolean showGrid = true;

	private boolean showPlainCurve = true;

	private boolean showMean = true;

	private boolean showP105090 = true;

	private boolean showMarker = true, createMarker=false;

	private boolean showFill = true;

	private boolean createZone = false;

	private boolean allowZoneCreation = false;

	private boolean firstPoint = true;
	
	private int firstPointX,firstPointY,endPointX,endPointY;

	// new By Lili
	private boolean transparentBackground = true;
	//added by Mario
	private boolean showTmpRect = false;
	private boolean movingZone = false;		// added by Alex
	private boolean updatingZone = false;		// added by Alex
	private boolean showZone = true; //added by Mario
	private boolean zoneRedrawFlag = true; //added by Mario
	private Rectangle rectZone = new Rectangle();
	//end
	private Point startPoint;
	private Point endPoint;


	private int originalHeight = 0; //June 16

	public boolean findMarker = false;


	/**
	 * This is the default constructor
	 */
	public CurveView() {
		super();
		initialize();
	}

	/**
	 * @param parent
	 *            instance of AxisCurveContainerView
	 */
	public CurveView(AxisCurveContainerView parent) {
		super();
		this.parent = parent;
		this.cl = this.getCurveLayer();
		this.yAxisView = this.parent.getYAxisView();
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setBackground(ConstantVariableList.CURVEVIEW_BACKGROUND);
		this.setBorder(new LineBorder(null, 0));
		curveDefaultColor = parent.getCurveDefaultColor();
		this.setFocusable(true);
		this.setTransferHandler(new TransferHandler("transferCurveName"));
		this.panelOrder.addPanel(this.yAxisView);
		this.setLayout(null);

		curveViewController = new CurveViewController();
		this.addMouseListener(curveViewController);
		this.addMouseMotionListener(curveViewController);
		this.addComponentListener(curveViewController);
		this.addMouseWheelListener(curveViewController);

	}

	/**
	 * @inheritDoc
	 */
	@SuppressWarnings("unchecked")
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Rectangle rect = g.getClipBounds();
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		// clear all panels
		this.removeAll();
		this.parent.setMDDepthConverterFromHashMap();

		// YAxisView
		this.addYAxisView();

		// Description Panel
		for(int i=0 ; i<descriptions.size() ; i++)
			addDescripionPanel((DescriptionPanel)descriptions.get(i));

		// Seismic Panel
		for(int i=0 ; i<seismics.size() ; i++)
			addSeismicPanel((SeismicPanel)seismics.get(i));

		// Lithology Panel
		for(int i=0 ; i < lithologys.size() ; i++)
			addLithologyPanel((LithologyPanel)lithologys.get(i));

		// zone
		if (this.isShowZone() && this.getCurveLayer().getYSetBottomDepth()>0){
			if (this.getZoneLayer() instanceof Renderable)
			((Renderable)getZoneLayer()).draw(g2,rect);
			this.setZoneRedrawFlag(false);
		}
		if (this.isShowTmpRect()&&rectZone!=null&&startPoint!=null){
			g2.setColor(Color.red); //use red color for selection
			rectZone.setFrameFromDiagonal(startPoint,endPoint);
			g2.draw(rectZone);
		}
		cl.calculateShape(1, 1);

        // curve
		if (this.getCurveLayer() instanceof Renderable){
			this.getCurveLayer().setPaintLine(false);
			((Renderable)getCurveLayer()).draw(g2,rect);
		}

        // marker
		if (this.getMarkerLayer() instanceof Renderable){
			((Renderable)getMarkerLayer()).draw(g2,rect);
		}

        // grid
		if (this.getGridLayer() instanceof Renderable){
			((Renderable)getGridLayer()).draw(g2,rect);
		}

		if (this.getCurveLayer() instanceof Renderable){
			this.getCurveLayer().setPaintLine(true);
			((Renderable)getCurveLayer()).draw(g2,rect);
		}

		this.getMarkerLayer().drawMarker(g2, this,getWidth(), 1, 1);

        // redraw
		this.setCurveRedrawFlag(true);
		this.setFillRedrawFlag(false);
		this.setMarkerRedrawFlag(false);

		// connect marker
		if(findMarker == false){
			ViewCoreOperator.loadMarker(this.parent);
			ViewCoreOperator.connectMarker(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView());
			findMarker = true;
		}
	}

	public Element saveXML(Document doc){
		Element xmlCurveView = doc.createElement("CurveView");
		xmlCurveView.setAttribute("width",Integer.toString(this.getWidth()));
		xmlCurveView.setAttribute("height",Integer.toString(this.getHeight()));

		Element xmlPanelOrderList = doc.createElement("PanelOrderList");
		xmlPanelOrderList.setAttribute("size",Integer.toString(this.getPanelOrder().getOrders().size()));
		for(int i = 0; i < this.getPanelOrder().getOrders().size(); i++){
			Element xmlPanelOrder = doc.createElement("PanelOrder"+i);
			xmlPanelOrder.setAttribute("PanelKind",Integer.toString(this.getPanelOrder().getPanelKind(i)));
			if(this.getPanelOrder().getPanelKind(i) == PanelOrder.YAXIS){
				YAxisView yAxisView = this.getParentYAxisView();
				Element xmlYAxisView = doc.createElement("YAxisView");
				xmlYAxisView.setAttribute("LocationX",Integer.toString(yAxisView.getX()));
				xmlYAxisView.setAttribute("width",Integer.toString(yAxisView.getWidth()));
				xmlPanelOrder.appendChild(xmlYAxisView);
			}
			if(this.getPanelOrder().getPanelKind(i) == PanelOrder.CURVE){
				CurvePanel curvePanel = (CurvePanel)this.getPanelOrder().getPanel(i);
				xmlPanelOrder.appendChild(curvePanel.saveXML(doc,i,false));
			}
			if(this.getPanelOrder().getPanelKind(i) == PanelOrder.GROUP){
				CurveGroup curveGroup = (CurveGroup)this.getPanelOrder().getPanel(i);
				Element xmlCurveGroup = doc.createElement("CurveGroup");
				xmlCurveGroup.setAttribute("name",curveGroup.getName());
				xmlCurveGroup.setAttribute("size",Integer.toString(curveGroup.getCurveList().size()));
				xmlCurveGroup.setAttribute("setRange",Boolean.toString(curveGroup.isSetRange()));
				xmlCurveGroup.setAttribute("maxXDisplay",curveGroup.getMaxXDisplay());
				xmlCurveGroup.setAttribute("minXDisplay",curveGroup.getMinXDisplay());
				xmlCurveGroup.setAttribute("logarithmic",Boolean.toString(curveGroup.isLogarithmic()));

				for(int j = 0 ; j < curveGroup.getCurveList().size();j++){
					CurvePanel curvePanel = (CurvePanel)curveGroup.getCurveList().get(j);
					xmlCurveGroup.appendChild(curvePanel.saveXML(doc,j,true));
				}
				xmlPanelOrder.appendChild(xmlCurveGroup);
			}
			if(this.getPanelOrder().getPanelKind(i) == PanelOrder.DESCRIPTION){
				DescriptionPanel descriptionPanel = (DescriptionPanel)this.getPanelOrder().getPanel(i);
				xmlPanelOrder.appendChild(descriptionPanel.saveXML(doc));
			}
			if(this.getPanelOrder().getPanelKind(i) == PanelOrder.SEISMIC){
				SeismicPanel seismicPanel = (SeismicPanel)this.getPanelOrder().getPanel(i);
				xmlPanelOrder.appendChild(seismicPanel.saveXML(doc));
			}
			if(this.getPanelOrder().getPanelKind(i) == PanelOrder.LITHOLOGY){
				LithologyPanel lithologyPanel = (LithologyPanel)this.getPanelOrder().getPanel(i);
				xmlPanelOrder.appendChild(lithologyPanel.saveXML(doc));
			}
			xmlPanelOrderList.appendChild(xmlPanelOrder);
		}
		xmlCurveView.appendChild(xmlPanelOrderList);

		xmlCurveView.appendChild(this.getCurveLayer().saveXML(doc));

		xmlCurveView.appendChild(this.getMarkerLayer().saveXML(doc));

		if (this.getZoneLayer() instanceof Persistence)
			xmlCurveView.appendChild(((Persistence)getZoneLayer()).saveXML(doc));

		Element xmlshow = doc.createElement("Show");

		Element xmlShowGridLayer = doc.createElement("ShowGridLayer");
		xmlShowGridLayer.setAttribute("visble", Boolean.toString(this
				.isShowGrid()));
		xmlshow.appendChild(xmlShowGridLayer);

		Element xmlShowP105090 = doc.createElement("ShowP105090");
		xmlShowP105090.setAttribute("visble", Boolean.toString(this
				.isShowP105090()));
		xmlshow.appendChild(xmlShowP105090);

		Element xmlShowMean = doc.createElement("ShowMean");
		xmlShowMean.setAttribute("visble", Boolean.toString(this.isShowMean()));
		xmlshow.appendChild(xmlShowMean);

		Element xmlShowPlain = doc.createElement("ShowPlain");
		xmlShowPlain.setAttribute("visble", Boolean.toString(this
				.isShowPlainCurve()));
		xmlshow.appendChild(xmlShowPlain);

		Element xmlShowMarker = doc.createElement("ShowMarker");
		xmlShowMarker.setAttribute("visble", Boolean.toString(this
				.isShowMarker()));
		xmlshow.appendChild(xmlShowMarker);

		Element xmlShowFill = doc.createElement("ShowFill");
		xmlShowFill.setAttribute("visble", Boolean.toString(this.isShowFill()));
		xmlshow.appendChild(xmlShowFill);

		Element xmlShowZone = doc.createElement("ShowZone");
		xmlShowZone.setAttribute("visble", Boolean.toString(this.isShowZone()));
		xmlshow.appendChild(xmlShowZone);

		xmlCurveView.appendChild(xmlshow);

		return xmlCurveView;
	}

	@SuppressWarnings("unchecked")
	public void loadXML(Element xmlCurveView){
		this.setPreferredSize(new Dimension(Integer.parseInt(xmlCurveView.getAttribute("width")),
				Integer.parseInt(xmlCurveView.getAttribute("height"))));
		this.setSize(Integer.parseInt(xmlCurveView.getAttribute("width")),
				Integer.parseInt(xmlCurveView.getAttribute("height")));

		Element xmlPanelOrderList = (Element) xmlCurveView.getElementsByTagName("PanelOrderList").item(0);
		int panelOrderSize = Integer.parseInt(xmlPanelOrderList.getAttribute("size"));
		ArrayList tempSpecialCurve = new ArrayList();
		for(int i = 0; i < panelOrderSize; i++){
			Element xmlPanelOrder = (Element) xmlPanelOrderList.getElementsByTagName("PanelOrder"+i).item(0);
			int panelKind = Integer.parseInt(xmlPanelOrder.getAttribute("PanelKind"));
			if(panelKind == PanelOrder.YAXIS){
				Element xmlYAxisView = (Element) xmlPanelOrder.getElementsByTagName("YAxisView").item(0);
				Object object = this.getPanelOrder().getPanel(this.getParentYAxisView());
				if(object != null){
					this.getPanelOrder().deletePanel((YAxisView)object);
				}
				this.getParentYAxisView().setBounds(Integer.parseInt(xmlYAxisView.getAttribute("LocationX")),0,
						Integer.parseInt(xmlYAxisView.getAttribute("width")),this.getHeight());
				this.getPanelOrder().addPanel(this.getParentYAxisView());
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree().addDepthPanel(this.getParentAxisCurveContainer().getWellName());
			}
			if(panelKind == PanelOrder.CURVE){
				Element xmlCurvePanel = (Element) xmlPanelOrder.getElementsByTagName("CurveData").item(0);
				if (SaveWorkSpaceUtil.isMeanOrP105090(xmlCurvePanel.getAttribute("name"))) {
					tempSpecialCurve.add(xmlCurvePanel.getAttribute("name"));
					continue;// jump over it here
				}
				int curveLocation = findCurveData(cl.getCurveList(),xmlCurvePanel.getAttribute("name"));
				CurvePanel cp = null;
				if(curveLocation != -1){
					cp = (CurvePanel) this.getCurveLayer().getCurveList().get(curveLocation);
				}
				else{
					CurveData cdata = SaveWorkSpaceUtil.getCdFromHash(xmlCurvePanel.getAttribute("name"));
					if (cdata==null)
						continue;
					cp = new CurvePanel(cdata);
					cp.loadXML(xmlCurvePanel);
					cp.setTranslatedCurve(this, cl.getBesideTypeWidth() * i, 1,1);
				}
//				 add curve to view tree
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createCurve(
						this.parent.getParentContainer().getParentInternalFrameView().getTitle(),
						this.getParentAxisCurveContainer().getWellName(), cp.getCd().getHashCurveName(),
						cp.getCd().getHashCurveName().split(OperationClass.hashMapNameSeparator)[1], false);
				// add curve to curve view
				this.getCurveLayer().addCurveToList(cp);
				this.getPanelOrder().addPanel(cp);
			}
			if(panelKind == PanelOrder.GROUP){
				Element xmlCurveGroup = (Element) xmlPanelOrder.getElementsByTagName("CurveGroup").item(0);
				int curveGroupSize = Integer.parseInt(xmlCurveGroup.getAttribute("size"));
				CurveGroup curveGroup = new CurveGroup(xmlCurveGroup.getAttribute("name"));
				String setRange = xmlCurveGroup.getAttribute("setRange");
				if(setRange != ""){
					curveGroup.setSetRange(ConversionClass.stringToBoolean(setRange));
					curveGroup.setMaxXDisplay(xmlCurveGroup.getAttribute("maxXDisplay"));
					curveGroup.setMinXDisplay(xmlCurveGroup.getAttribute("minXDisplay"));
				}
				String isLog = xmlCurveGroup.getAttribute("logarithmic");
				if(setRange != null)
					curveGroup.setLogarithmic(ConversionClass.stringToBoolean(isLog));
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createGroupPanel(
						this.parent.getParentContainer().getParentInternalFrameView().getTitle(),
						this.parent.getWellName(),curveGroup.getName(), false);
				for(int j = 0; j < curveGroupSize; j++){
					Element xmlCurvePanel = (Element) xmlCurveGroup.getElementsByTagName("CurveData"+j).item(0);// one
					if (SaveWorkSpaceUtil.isMeanOrP105090(xmlCurvePanel.getAttribute("name"))) {
						tempSpecialCurve.add(xmlCurvePanel.getAttribute("name"));
						continue;// jump over it here
					}
					// normal curve will be added to both curve view and
					// view tree
					int curveLocation = findCurveData(cl.getCurveList(),xmlCurvePanel.getAttribute("name"));
					CurveData cdata=SaveWorkSpaceUtil.getCdFromHash(xmlCurvePanel.getAttribute("name"));
					CurvePanel cp = null;
					if(curveLocation != -1){
						cp = (CurvePanel)cl.getCurveList().get(curveLocation);
					}
					else if (cdata!=null){
						cp = new CurvePanel(cdata);
						cp.loadXML(xmlCurvePanel);
						cp.setTranslatedCurve(this, cl.getBesideTypeWidth() * j, 1,1);
					}else
						continue;
					cp.setCg(curveGroup);
					curveGroup.getCurveList().add(cp);
					// add curve to view tree
					DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createCurveUnderGroupPanel(
							this.parent.getParentContainer().getParentInternalFrameView().getTitle(),
							this.getParentAxisCurveContainer().getWellName(), curveGroup.getName(),
							cp.getCd().getHashCurveName(), cp.getCd().getHashCurveName().split(OperationClass.hashMapNameSeparator)[1]);
				}
				this.getCurveGroups().add(curveGroup);
				this.getPanelOrder().addPanel(curveGroup);
			}
			if(panelKind == PanelOrder.SEISMIC){
				Element xmlSeismicPanel = (Element) xmlPanelOrder.getElementsByTagName("SeismicPanel").item(0);
				SeismicPanel seismicPanel = new SeismicPanel(this, xmlSeismicPanel.getAttribute("name"));
				seismicPanel.loadXML(xmlSeismicPanel);
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createSeismicPanel(this.getParentAxisCurveContainer().getWellName(),seismicPanel.getName());
				this.getSeismics().add(seismicPanel);
				this.getPanelOrder().addPanel(seismicPanel);
			}
			if(panelKind == PanelOrder.DESCRIPTION){
				Element xmlDescriptionPanel = (Element) xmlPanelOrder.getElementsByTagName("DescriptionPanel").item(0);
				DescriptionPanel descriptionPanel = new DescriptionPanel(this,xmlDescriptionPanel.getAttribute("name"));
				descriptionPanel.loadXML(xmlDescriptionPanel);
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createDescriptionPanel(this.getParentAxisCurveContainer().getWellName(),descriptionPanel.getName());
				this.getPanelOrder().addPanel(descriptionPanel);
				this.getDescriptions().add(descriptionPanel);
			}
			if(panelKind == PanelOrder.LITHOLOGY){
				Element xmlLithologyPanel = (Element) xmlPanelOrder.getElementsByTagName("LithologyPanel").item(0);
				LithologyPanel lithologyPanel = new LithologyPanel(this,"");
				lithologyPanel.loadXML(xmlLithologyPanel);
				DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createLithologyPanel(this.getParentAxisCurveContainer().getWellName(),lithologyPanel.getName());
				this.getPanelOrder().addPanel(lithologyPanel);
				this.getLithologys().add(lithologyPanel);
			}
		}
		if (tempSpecialCurve.size() > 0) {
			this.getCurveLayer().freshMeanAndP105090();
			this.getCurveLayer().addP105090ToList();
			this.getCurveLayer().addMeanCurveToList();
			this.setCurveRedrawFlag(true);
		}


		Element xmlCurveLayer = (Element) xmlCurveView.getElementsByTagName("CurveLayer").item(0);
		this.getCurveLayer().loadXML(xmlCurveLayer, this);

		Element xmlMarkerLayer = (Element) xmlCurveView.getElementsByTagName("MarkerLayer").item(0);
		this.getMarkerLayer().loadXML(xmlMarkerLayer,this);

		Element xmlZoneLayer = (Element) xmlCurveView.getElementsByTagName("ZoneLayer").item(0);
		if (this.getZoneLayer() instanceof Persistence)
			((Persistence)getZoneLayer()).loadXML(xmlZoneLayer,this);

		Element xmlshow = (Element) xmlCurveView.getElementsByTagName("Show")
				.item(0);

		Element xmlgrid = (Element) xmlshow.getElementsByTagName(
				"ShowGridLayer").item(0);
		this.setShowGrid(ConversionClass.stringToBoolean(xmlgrid
				.getAttribute("visble")));

		Element xmlp105090 = (Element) xmlshow.getElementsByTagName(
				"ShowP105090").item(0);
		this.setShowP105090(ConversionClass.stringToBoolean(xmlp105090
				.getAttribute("visble")));

		Element xmlmean = (Element) xmlshow.getElementsByTagName("ShowMean")
				.item(0);
		this.setShowMean(ConversionClass.stringToBoolean(xmlmean
				.getAttribute("visble")));

		Element xmlplain = (Element) xmlshow.getElementsByTagName("ShowPlain")
				.item(0);
		this.setShowPlainCurve(ConversionClass.stringToBoolean(xmlplain
				.getAttribute("visble")));

		Element xmlmarker = (Element) xmlshow
				.getElementsByTagName("ShowMarker").item(0);
		this.setShowMarker(ConversionClass.stringToBoolean(xmlmarker
				.getAttribute("visble")));

		Element xmlfill = (Element) xmlshow.getElementsByTagName("ShowFill")
				.item(0);
		this.setShowFill(ConversionClass.stringToBoolean(xmlfill
				.getAttribute("visble")));

	}

	private int findCurveData(ArrayList arrayList,String hashCurveName){
		for(int i=0;i<arrayList.size();i++){
			if(((CurveData)((CurvePanel)arrayList.get(i)).getCd()).getHashCurveName().equals(hashCurveName)){
				return i;
			}
		}
		return -1;
	}

	public static float changeHeightToDepth(double height,CurveView curveView)
	{
		return (float) ((height/curveView.getHeight())*(curveView.getCurveLayer().getYSetBottomDepth()-
				curveView.getCurveLayer().getYSetTopDepth())+
				curveView.getCurveLayer().getYSetTopDepth());
	}

	public String getTransferCurveName() {
		return transferCurveName;
	}

	// receive
	public void setTransferCurveName(String transferCurveName1) {
		if (SaveWorkSpaceUtil.isMeanOrP105090(transferCurveName1)) {
			return;
		}
		this.transferCurveName = transferCurveName1;
		if (transferCurveName != null && !transferCurveName.equals("")) {
			ViewCoreOperator.tryPutCurveToTrack(transferCurveName, parent.getWellName());
		}

	}

	public void clearTransferCurveName() // after drag out , clear it
	{
		this.transferCurveName = "";
	}

	public void resetTransferCurveName(String transferCurveName) {
		this.transferCurveName = transferCurveName;
	}

	public void repaintFather() {
		parent.repaintAll();
	}

	public AxisCurveContainerView getParentAxisCurveContainer() {
		return parent;
	}

	public CurveLayer getCurveLayer() {
		return parent.getCurveLayer();
	}

	public MarkerLayer getMarkerLayer() {
		return parent.getMarkerLayer();
	}

	public GridLayer getGridLayer() {
		return parent.getGridLayer();
	}

	public boolean isCurveRedrawFlag() {
		return curveRedrawFlag;
	}

	public void setCurveRedrawFlag(boolean curveRedrawFlag) {
		this.curveRedrawFlag = curveRedrawFlag;
	}

	public boolean isMarkerRedrawFlag() {
		return markerRedrawFlag;
	}

	public void setMarkerRedrawFlag(boolean markerRedrawFlag) {
		this.markerRedrawFlag = markerRedrawFlag;
	}

	public boolean isFillRedrawFlag() {
		return fillRedrawFlag;
	}

	public void setFillRedrawFlag(boolean fillRedrawFlag) {
		this.fillRedrawFlag = fillRedrawFlag;
	}

	/**
	 * @author luming if the size < curvelist.size * curvewidth , set this
	 */
	public void setProperSize() {
		int newHeight = this.parent.getYAxisView().getYHeight();
		if (cl.getLayerType().equals(TrackTypeList.REALIZATION_BESIDE)) {
			int newWidth = 0;
			for (int i = 0 ; i < panelOrder.getOrders().size(); i++) {
				newWidth = newWidth + panelOrder.getPanelWidth(i);
			} //fix bug July 14
			//newWidth = newWidth + parent.getTitleAndCurveNamesPanel().getVerticalScrollBar().getWidth();
			this.setPreferredSize(new Dimension(newWidth, newHeight));
			this.setSize(newWidth, newHeight);
		} else {
			this.setPreferredSize(new Dimension(cl.getBesideTypeWidth(), newHeight));
			this.setSize(cl.getBesideTypeWidth(), newHeight);
		}
		this.revalidate();
	}

	public boolean isShowFill() {
		return showFill;
	}

	public void setShowFill(boolean showFill) {
		this.showFill = showFill;
	}

	public boolean isShowGrid() {
		return showGrid;
	}

	public void setShowGrid(boolean showGrid) {
		this.showGrid = showGrid;
	}

	public boolean isShowMarker() {
		return showMarker;
	}

	public void setShowMarker(boolean showMarker) {
		this.showMarker = showMarker;
	}

	public boolean isShowMean() {
		return showMean;
	}

	public void setShowMean(boolean showMean) {
		this.showMean = showMean;
	}

	public boolean isShowP105090() {
		return showP105090;
	}

	public void setShowP105090(boolean showP105090) {
		this.showP105090 = showP105090;
	}

	public boolean isShowPlainCurve() {
		return showPlainCurve;
	}

	public void setShowPlainCurve(boolean showPlainCurve) {
		this.showPlainCurve = showPlainCurve;
	}

	public boolean isMean(CurvePanel cp) {
		if (cp.getCd().getCurveName().equals(ConstantVariableList.MEAN)) {
			return true;
		}
		return false;
	}

	public boolean isP105090(CurvePanel cp) {
		if (cp.getCd().getCurveName().equals(ConstantVariableList.P10) ||
				cp.getCd().getCurveName().equals(ConstantVariableList.P50) ||
				cp.getCd().getCurveName().equals(ConstantVariableList.P90)) {
			return true;
		}
		return false;
	}

	/**
	 * @return transparentBackground
	 */
	public boolean isTransparentBackground() {
		return transparentBackground;
	}

	/**
	 * @param transparentBackground
	 *            transparentBackground
	 */
	public void setTransparentBackground(boolean transparentBackground) {
		this.transparentBackground = transparentBackground;
	}

	public PanelOrder getPanelOrder() {
		return panelOrder;
	}

	public ArrayList getCurveGroups() {
		return curveGroups;
	}

	public void display(){
		// contains
		System.out.println("**********Contains************");
		System.out.println("**********Curve************");
		for(int i=0 ; i<cl.getCurveList().size() ; i++){
			System.out.println(((CurvePanel)cl.getCurveList().get(i)).getCd().getCurveName());
		}
		System.out.println("**********Group************");
		for(int i=0 ; i<curveGroups.size() ; i++){
			System.out.println((CurveGroup)curveGroups.get(i));
		}
		System.out.println("**********Des************");
		for(int i=0 ; i<descriptions.size() ; i++){
			System.out.println(((DescriptionPanel)descriptions.get(i)).getName());
		}


		// orders
		System.out.println("**********Orders************");
		for(int i=0; i<panelOrder.getOrders().size() ; i++){
			System.out.println(panelOrder.getPanel(i));
		}
		System.out.println("****************************");

	}

	public YAxisView getParentYAxisView() {
		return this.parent.getYAxisView();
	}

	public void addYAxisView(){
		this.yAxisView = getParentYAxisView();
		this.yAxisView.setBounds(this.panelOrder.getPanelLeft(this.yAxisView) , 0 ,
				this.yAxisView.getWidth() , this.getHeight());
		this.add(this.yAxisView);
		if(this.yAxisView.isReComputeFlag()){
			this.yAxisView.reCompute();
		}
		this.yAxisView.repaint();
	}

	public void addDescripionPanel(DescriptionPanel desPanel){
		desPanel.setBounds(this.panelOrder.getPanelLeft(desPanel) , 0 ,
				desPanel.getBesideTypeWidth() , this.getHeight());
		this.add(desPanel);
		desPanel.repaint();
	}

    public void addSeismicPanel(SeismicPanel seismicPanel){
		seismicPanel.setBounds(this.panelOrder.getPanelLeft(seismicPanel) , 0 ,
				seismicPanel.getBesideTypeWidth() , this.getHeight());
		this.add(seismicPanel);
		seismicPanel.repaint();
	}

    public void addLithologyPanel(LithologyPanel lithologyPanel){
    	lithologyPanel.setBounds(this.panelOrder.getPanelLeft(lithologyPanel) , 0 ,
    			lithologyPanel.getBesideTypeWidth() , this.getHeight());
		this.add(lithologyPanel);
		lithologyPanel.repaint();
	}

	public ArrayList getDescriptions() {
		return descriptions;
	}

	public ArrayList getLithologys() {
		return lithologys;
	}

	public boolean isCreateZone() {
		return this.createZone;
	}

	public void setCreateZone(boolean createZone) {
		this.createZone = createZone;
	}

	public boolean isFirstPoint() {
		return this.firstPoint;
	}

	public void setFirstPoint(boolean firstPoint) {
		this.firstPoint = firstPoint;
	}

	public int getFirstPointX() {
		return this.firstPointX;
	}

	public void setFirstPointX(int firstPointX) {
		this.firstPointX = firstPointX;
	}

	public int getFirstPointY() {
		return this.firstPointY;
	}

	public void setFirstPointY(int firstPointY) {
		this.firstPointY = firstPointY;
	}

	public int getEndPointX() {
		return this.endPointX;
	}

	public void setEndPointX(int endPointX) {
		this.endPointX = endPointX;
	}

	public int getEndPointY() {
		return endPointY;
	}

	public void setEndPointY(int endPointY) {
		this.endPointY = endPointY;
	}

	public ArrayList getSeismics() {
		return seismics;
	}

	//use reflection
	public Object getZoneLayer(){      //added by Mario
		return parent.getZoneLayer();
	}

	public Object getRockPhysics(){      //added by Mario
		return parent.getRockPhysics();
	}

//	added by Mario - define rock Physics zone
	public void setTmpRectStart(Point p){
		this.startPoint=p;
	}

	public Point getTmpRectStart(){
		return this.startPoint;
	}

	public void setTmpRectEnd(Point p){
		this.endPoint=p;
	}

	public Point getTmpRectEnd(){
		return this.endPoint;
	}

	public void setShowTmpRect(boolean tmpRect){
		this.showTmpRect=tmpRect;
	}

	public boolean isShowTmpRect(){
		return this.showTmpRect;
	}

	/**
	 * added by Alex
	 * set curve view enabled to creating zones
	 * @param value
	 */
	public void setZoneCreationAllowed(boolean value)	{
		this.allowZoneCreation = value;
	}

	/**
	 * aded by Alex
	 * returns true if zone creation is allowed, false if not
	 * @return true | false
	 */
	public boolean isZoneCreationAllowed()	{
		return this.allowZoneCreation;
	}

	// added by Alex
	public boolean isUpdatingZone()	{
		return this.updatingZone;
	}

	// added by Alex
	public void setUpdatingZone(boolean value)	{
		this.updatingZone = value;
	}

	/**
	 * added by Alex
	 * get value of movingZone variable (true|false)
	 * @return boolean value
	 */
	public boolean isMovingZone()	{
		return this.movingZone;
	}

	/**
	 * added by Alex
	 * set new value for movingZone
	 * @param new boolean value
	 */
	public void setMovingZone(boolean value)	{
		this.movingZone = value;
	}

	public void setZoneRedrawFlag(boolean zoneRedrawFlag){
		this.zoneRedrawFlag=zoneRedrawFlag;
	}

	public boolean isZoneRedrawFlag(){
		return this.zoneRedrawFlag;
	}

	public void setShowZone(boolean showZone){
		this.showZone=showZone;
	}

	public boolean isShowZone(){
		return this.showZone;
	}

	//JUne 16
	public int getOriginalHeight() {
		return originalHeight;
	}

	public void setOriginalHeight(int originalHeight) {
		this.originalHeight = originalHeight;
	}

	public Color getCurveDefaultColor() {
		return curveDefaultColor;
	}

	public void setCurveDefaultColor(Color curveDefaultColor) {
		this.curveDefaultColor = curveDefaultColor;
	}

	public float getDefaultstroke() {
		return defaultstroke;
	}

	public void setDefaultstroke(float defaultstroke) {
		this.defaultstroke = defaultstroke;
	}

	public String getWellName(){
		return parent.getWellName();
	}

	/**
	 * Returns the MD value.
	 * @param height Value in screen
	 * @return the depth in MD domain.
	 */
	public float screenToDepth(float height){
		float depth =   (float)((height/this.getHeight())*
						(this.getCurveLayer().getYSetBottomDepth()-
						this.getCurveLayer().getYSetTopDepth())+
						this.getCurveLayer().getYSetTopDepth());
		return depth;
	}

	/**
	 * Convert depth value (md) into screen.
	 * @param depth Vaule in MD.
	 * @return screen value in different domain.
	 */
	public float depthToScreen(float depth){
		//check the vertical type
		float finalDepth = depth;
		if (yAxisView.getYType().equals(TrackTypeList.Y_DEPTH_TVD)){
			finalDepth=parent.getMDDepthConverter().toDepth(depth,
					TrackTypeList.MD_TO_TVD);
		}else if (yAxisView.getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)){
			finalDepth=parent.getMDDepthConverter().toDepth(depth,
					TrackTypeList.MD_TO_TVDSS);
		}
		return (float) ((finalDepth-this.getCurveLayer().getYSetTopDepth())/
						(this.getCurveLayer().getYSetBottomDepth()-
						this.getCurveLayer().getYSetTopDepth()))*
						this.getHeight();

	}

	public boolean isCreateMarker(){
		return createMarker;
	}

	public void setCreateMarker(boolean yesno){
		createMarker = yesno;
		if (createMarker)
			createZone = !createMarker;
	}
	
	/**
	 * it's used to convert METERS to FEET and viceversa
	 * @param depthUnit object depth unit
	 * @return conversion factor
	 * @author Alex
	 */
	public float getUnitTran(int depthUnit){
		float unit = 1;
		
		if (this.getParentAxisCurveContainer().getDepthUnit() == CurveData.METER)	{
			if (depthUnit == CurveData.FOOT)	{
				unit = (float) ConstantVariableList.FOOT_TO_METER;
			}
		}
		else if (this.getParentAxisCurveContainer().getDepthUnit() == CurveData.FOOT)	{
			if (depthUnit == CurveData.METER)	{
				unit = (float) ConstantVariableList.METER_TO_FOOT;
			}
		}
		
		return unit;
	}
}
