/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.util.ConversionClass;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.dialogs.ComboBoxRender;
import com.gwsys.welllog.view.yaxis.DepthConverter;

public class CurveMarker {
	
	private int lineStyle;

	private float markerWidth; // the width of marker , hhb add it

	private float markerDepth; // the depth marker stand for

	private String markerName; // the name of this marker to show

	private int yHeight; // the coordinater this marker at

	private int xLenth; // how long

	@SuppressWarnings("unused")
	private BasicStroke basicMarkerStroke;

	private String markerFont;

	private Color markerColor;

	private boolean selected = false; // 1 selected, 0 no

	private CurveMarker leftMarker;

	private CurveMarker rightMarker;

//	private boolean isDraged = false;

	private boolean isVisible = true;
	
	private int depthUnit = 1;
	
	public CurveMarker(String markerName, float markerDepth, Color markerColor) {
		this.markerName = markerName;
		this.markerDepth = markerDepth;
		this.markerColor = markerColor;
		this.markerWidth = 0.5f;
		this.lineStyle = ComboBoxRender.SOLID;
		this.basicMarkerStroke = new BasicStroke(markerWidth,
				BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	}

	// hhb add it
	public int getLineStyle() {
		return lineStyle;
	}

	public void setLineStyle(int lineStyle) {
		this.lineStyle = lineStyle;
		if (leftMarker != null)
			setLeftLineStyle(this.lineStyle);
		if (rightMarker != null)
			setRightLineStyle(this.lineStyle);
	}

	public void setLeftLineStyle(int lineStyle) {
		this.lineStyle = lineStyle;
		if (leftMarker != null)
			leftMarker.setLeftLineStyle(this.lineStyle);
	}

	public void setRightLineStyle(int lineStyle) {
		this.lineStyle = lineStyle;
		if (rightMarker != null)
			rightMarker.setRightLineStyle(this.lineStyle);
	}

	public float getMarkerWidth() {
		return markerWidth;
	}

	public void setMarkerWidth(float markerWidth) {
		this.markerWidth = markerWidth;
		if (leftMarker != null)
			setLeftMarkerWidth(this.markerWidth);
		if (rightMarker != null)
			setRightMarkerWidth(this.markerWidth);
	}

	public void setLeftMarkerWidth(float markerWidth) {
		this.markerWidth = markerWidth;
		if (leftMarker != null)
			leftMarker.setLeftMarkerWidth(this.markerWidth);
	}

	public void setRightMarkerWidth(float markerWidth) {
		this.markerWidth = markerWidth;
		if (rightMarker != null)
			rightMarker.setRightMarkerWidth(this.markerWidth);
	}

	public Color getMarkerColor() {
		return markerColor;
	}

	public void setMarkerColor(Color markerColor) {
		this.markerColor = markerColor;
		if (leftMarker != null)
			setLeftMarkerColor(this.markerColor);
		if (rightMarker != null)
			setRightMarkerColor(this.markerColor);
	}

	public void setLeftMarkerColor(Color markerColor) {
		this.markerColor = markerColor;
		if (leftMarker != null)
			leftMarker.setLeftMarkerColor(this.markerColor);
	}

	public void setRightMarkerColor(Color markerColor) {
		this.markerColor = markerColor;
		if (rightMarker != null)
			rightMarker.setRightMarkerColor(this.markerColor);
	}

	public float getMarkerDepth() {
		return markerDepth;
	}

	public void setMarkerDepth(float markerDepth) {
		this.markerDepth = markerDepth;
	}

	public String getMarkerName() {
		return markerName;
	}

	public void setMarkerName(String markerName) {	
		// change markerinfo
		ArrayList mList = DataBuffer.getJMainFrame().getMainBuffer().getMarkerList();
		for(int i= 0; i < mList.size(); i++){
			MarkerInfo markerInfo =(MarkerInfo)mList.get(i);
			if(markerInfo.getMarkerName().equals(this.markerName)){
				markerInfo.setMarkerName(markerName);
			}
		}
		
		this.markerName = markerName;
		if (leftMarker != null)
			setLeftMarkerName(this.markerName);
		if (rightMarker != null)
			setRightMarkerName(this.markerName);
	}

	public void setLeftMarkerName(String markerName) {
		this.markerName = markerName;
		if (leftMarker != null)
			leftMarker.setLeftMarkerName(this.markerName);
	}

	public void setRightMarkerName(String markerName) {
		this.markerName = markerName;
		if (rightMarker != null)
			rightMarker.setRightMarkerName(this.markerName);
	}

	public int getYHeight() {
		return yHeight;
	}

	/**
	 * set marker y position
	 * 
	 * @param curveView
	 *            which curveView this marker in
	 */
	public void setYHeight(CurveView curveView) {
		float ySetTopDepth = curveView.getCurveLayer().getYSetTopDepth();
		float yHeight = curveView.getHeight();
		float ySetBottomDepth = curveView.getCurveLayer().getYSetBottomDepth();
		float markerDepthValue = 0;
		if (curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_MD)){
			markerDepthValue = markerDepth;
		}else if(curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVD)){
			DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
			markerDepthValue = timeToDepthConverter.toDepth(markerDepth,TrackTypeList.MD_TO_TVD);
		}else if(curveView.getParentYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_TVDSS)){
			DepthConverter timeToDepthConverter = curveView.getParentAxisCurveContainer().getMDDepthConverter();
			markerDepthValue = timeToDepthConverter.toDepth(markerDepth,TrackTypeList.MD_TO_TVDSS);
		}

		float unit = curveView.getUnitTran(this.depthUnit);
		markerDepthValue *= unit;  
		
		setMarkerDepth(markerDepthValue);
		
		if (this.depthUnit != curveView.getParentAxisCurveContainer().getDepthUnit())
			this.depthUnit = curveView.getParentAxisCurveContainer().getDepthUnit();
		
		this.yHeight = (int) (yHeight * (markerDepthValue - ySetTopDepth) / (ySetBottomDepth - ySetTopDepth));
	}
	
	public int getXLenth() {
		return xLenth;
	}

	public void setXLenth(int lenth) {
		xLenth = lenth;
	}

	public void resetMarker(CurveView cv) {
		this.setYHeight(cv);
	}

	public void setBasicMarkerStroke(BasicStroke basicMarkerStroke) {
		this.basicMarkerStroke = basicMarkerStroke;
	}

	public String getMarkerFont() {
		return markerFont;
	}

	public void setMarkerFont(String markerFont) {
		this.markerFont = markerFont;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public void addRelatedMarker(CurveMarker otherMarker) {

	}

	public CurveMarker getLeftMarker() {
		return leftMarker;
	}

	public void setLeftMarker(CurveMarker leftMarker) {
		this.leftMarker = leftMarker;
		if(leftMarker != null){
			setMarkerColor(leftMarker.getMarkerColor());
		    setMarkerName(leftMarker.getMarkerName());
		    setLineStyle(leftMarker.getLineStyle());
		    setMarkerWidth(leftMarker.getMarkerWidth());
		}
	}

	public CurveMarker getRightMarker() {
		return rightMarker;
	}

	public void setRightMarker(CurveMarker rightMarker) {
		this.rightMarker = rightMarker;
		if(rightMarker != null){
			rightMarker.setMarkerColor(getMarkerColor());
		    rightMarker.setMarkerName(getMarkerName());
		    rightMarker.setLineStyle(getLineStyle());
		    rightMarker.setMarkerWidth(getMarkerWidth());
		}
	}
/*
	public boolean isDraged() {
		return isDraged;
	}

	public void setDraged(boolean isDraged) {
		this.isDraged = isDraged;
	}
*/
	
	public Element saveXML(Document doc, Integer id){
		Element xmlCurveMarker = doc.createElement("CurveMarker");
		xmlCurveMarker.setAttribute("id",Integer.toString(id));
		xmlCurveMarker.setAttribute("name",this.getMarkerName());
		xmlCurveMarker.setAttribute("depth",Float.toString(this.getMarkerDepth()));
		xmlCurveMarker.setAttribute("color",Integer.toString(this.getMarkerColor().getRGB()));
		xmlCurveMarker.setAttribute("width",Float.toString(this.getMarkerWidth()));
		xmlCurveMarker.setAttribute("style",Integer.toString(this.getLineStyle()));
		xmlCurveMarker.setAttribute("isVisible",Boolean.toString(this.isVisible()));
		
		return xmlCurveMarker;
	}
	
	public void loadXML(Element xmlCurveMarker,CurveView cv){
		this.setDepthUnit(cv.getParentAxisCurveContainer().getDepthUnit());
		this.setLineStyle(Integer.parseInt(xmlCurveMarker.getAttribute("style")));
		this.setXLenth(cv.getWidth());							
		this.setYHeight(cv);
		String visible = xmlCurveMarker.getAttribute("isVisible");
		if(visible != null){
			this.setVisible(ConversionClass.stringToBoolean(visible));
		}
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	    if (leftMarker != null)
	    	setLeftMarkerVisible(isVisible);
		if (rightMarker != null)
			setRightMarkerVisible(isVisible);
	}
	public void setLeftMarkerVisible(boolean isVisible) {
		this.isVisible = isVisible;
		if (leftMarker != null)
			leftMarker.setLeftMarkerVisible(this.isVisible);
	}

	public void setRightMarkerVisible(boolean isVisible) {
		this.isVisible = isVisible;
		if (rightMarker != null)
			rightMarker.setRightMarkerVisible(this.isVisible);
	}
	
	public int getDepthUnit()	{
		return this.depthUnit;
	}
	
	public void setDepthUnit(int depthUnit)	{
		this.depthUnit = depthUnit;
	}
}
