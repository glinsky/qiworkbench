/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.dialogs.EditDescriptionSettingDialog;
import com.gwsys.welllog.view.dialogs.EditDescriptionTextDialog;

/**
 * @author Administrator
 *
 * Create pnael to hold remarks.
 * @version 1.0
 */
public class DescriptionPanel extends JPanel implements MouseListener,Serializable ,ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private CurveView parent = null;
	
	private ArrayList objectList = new ArrayList();
	
	private int itemID = 0;
	
	private int intX = 0;
	
	private int intY = 0;
	
	private String name = "";
	
	private int besideTypeWidth = ConstantVariableList.DESCRIPTION_WIDTH;
	
	private Color defaultTextColor = Color.BLUE;
	
	private int defaultFontSize = 10;
	
	public DescriptionPanel(){
		super();
	}
	
	public DescriptionPanel(CurveView parent,String name){
		super();
		this.parent = parent;
		this.name = name;
		
		this.addMouseListener(this);
		this.setPreferredSize(ConstantVariableList.DESCRIPTION_PREFERREDSIZE);
		this.setBackground(ConstantVariableList.DESCRIPTION_BACKGROUND);
		this.setBorder(new LineBorder(Color.BLACK,1));
		this.setLayout(null);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	public int getIntX() {
		return intX;
	}

	public int getIntY() {
		return intY;
	}

	@SuppressWarnings("unchecked")
	public void addDescriptionObject(DescriptionObject descriptionObject){
		this.objectList.add(descriptionObject);	
		this.add(descriptionObject.getLbImage());
	}
	
	public void deleteDescriptionObject(DescriptionObject descriptionObject){
		this.objectList.remove(descriptionObject);
		this.remove(descriptionObject.getLbImage());
	}
	
	
	public DescriptionObject getDescriptionObject(int i){
		return (DescriptionObject)this.objectList.get(i);
	}
		
	public ArrayList getObjectList() {
		return objectList;
	}
	
	public DescriptionObject getSelectDescriptionObject(String itemID){
		for(int i=0;i<objectList.size();i++){
			if(((DescriptionObject)objectList.get(i)).getItemID().equals(itemID)){
				return (DescriptionObject)this.objectList.get(i);
			}
		}
		return null;
	}

	public int getBesideTypeWidth() {
		return besideTypeWidth;
	}

	public void setBesideTypeWidth(int besideTypeWidth) {
		this.besideTypeWidth = besideTypeWidth;
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public void mouseClicked(MouseEvent e){
		
	}

	public void mousePressed(MouseEvent e){
		if(e.getButton()==MouseEvent.BUTTON3){
			this.intX = e.getX();
			this.intY = e.getY();
			JPopupMenu popup = new JPopupMenu();
			JMenuItem settingItem = new JMenuItem("Settings");
			popup.add(settingItem);
			popup.addSeparator();
			
			JMenuItem insertTextItem = new JMenuItem("Insert Text");
			popup.add(insertTextItem);
			JMenuItem insertImageItem = new JMenuItem("Insert Image");
			popup.add(insertImageItem);
			
			popup.addSeparator();
			
			JMenuItem deleteItem = new JMenuItem("Delete");
			popup.add(deleteItem);
			
			popup.show(this, e.getX(), e.getY());

			settingItem.addActionListener(this);
			settingItem.setActionCommand("descriptionSettings");
			insertTextItem.addActionListener(this);
			insertTextItem.setActionCommand("Insert Text");
			insertImageItem.addActionListener(this);
			insertImageItem.setActionCommand("Insert Image");
			deleteItem.addActionListener(this);
			deleteItem.setActionCommand("descriptionDelete");
		}
	}

	public void mouseReleased(MouseEvent e){
		
	}
	
	public void mouseEntered(MouseEvent e){
		
	}

	public void mouseExited(MouseEvent e){
		
	}

	public void actionPerformed(ActionEvent e){
		if(e.getActionCommand().equals("descriptionSettings")){			
			setting();
		}else if(e.getActionCommand().equals("Insert Text")){
			insertText();			
		}else if(e.getActionCommand().equals("Insert Image")){
			JFileChooser fc = new JFileChooser();
			File file;
			if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
				file = fc.getSelectedFile();
				insertImage(file.getAbsolutePath());
			}	
		}else if(e.getActionCommand().equals("descriptionDelete")){
			String wellName = parent.getParentAxisCurveContainer().getWellName();			
			ViewCoreOperator.removeDescriptionPanel(wellName , this.name);
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private void setting(){
		EditDescriptionSettingDialog settingDlg = new EditDescriptionSettingDialog(this);
		settingDlg.setVisible(true);
	}
	
	private void insertText(){
		EditDescriptionTextDialog editDescriptionTextDialog = new EditDescriptionTextDialog(this);
		editDescriptionTextDialog.setVisible(true);
		if(editDescriptionTextDialog.getText()==""){
			return;
		}
		String text = editDescriptionTextDialog.getText();
		
		DescriptionObject desObj = new DescriptionObject(this , DescriptionObject.CONTROL_TEXT_LABEL);
		desObj.setText(text);
		desObj.setTextLabel();
		desObj.getLbImage().setBounds(intX, intY,desObj.getLbImage().getWidth(),desObj.getLbImage().getHeight());
        addDescriptionObject(desObj);
	}
	
	public void insertImage(String path){
		DescriptionObject desObj = new DescriptionObject(this, DescriptionObject.CONTROL_IMAGE_LABEL);
		desObj.setImagePath(path);
		desObj.setImageLabel();
		desObj.getLbImage().setBounds(intX, intY,desObj.getLbImage().getWidth(),desObj.getLbImage().getHeight());
		
		addDescriptionObject(desObj);
		//this.repaint();
	}
	
	public Element saveXML(Document doc){
		Element xmlDescriptionPanel = doc.createElement("DescriptionPanel");	
		xmlDescriptionPanel.setAttribute("itemID",Integer.toString(this.getItemID()));
		xmlDescriptionPanel.setAttribute("name",this.getName());
		xmlDescriptionPanel.setAttribute("besideTypeWidth",Integer.toString(this.getBesideTypeWidth()));
		xmlDescriptionPanel.setAttribute("defaultFontSize",Integer.toString(this.getDefaultFontSize()));	        					
		xmlDescriptionPanel.setAttribute("defaultTextColor",Integer.toString(this.getDefaultTextColor().getRGB()));
		xmlDescriptionPanel.setAttribute("size",Integer.toString(this.getObjectList().size()));
		for(int i = 0; i < this.getObjectList().size(); i++){
			DescriptionObject descriptionObject = (DescriptionObject)this.getObjectList().get(i);
			xmlDescriptionPanel.appendChild(descriptionObject.saveXML(doc,i));
		}
		return xmlDescriptionPanel;
	}
	
	public void loadXML(Element xmlDescriptionPanel){
		this.setItemID(Integer.parseInt(xmlDescriptionPanel.getAttribute("itemID")));
		this.setBesideTypeWidth(Integer.parseInt(xmlDescriptionPanel.getAttribute("besideTypeWidth")));
		this.setSize(this.besideTypeWidth , parent.getHeight());
		this.setDefaultFontSize(Integer.parseInt(xmlDescriptionPanel.getAttribute("defaultFontSize")));
		this.setDefaultTextColor(new Color(Integer.parseInt(xmlDescriptionPanel.getAttribute("defaultTextColor"))));
		int descriptionPanelSize = Integer.parseInt(xmlDescriptionPanel.getAttribute("size"));
		for(int i = 0; i < descriptionPanelSize; i++){
			Element xmlDescriptionObject = (Element) xmlDescriptionPanel.getElementsByTagName("DescriptionObject"+i).item(0);
			
			int controlType = Integer.parseInt(xmlDescriptionObject.getAttribute("controlType"));
			String text = xmlDescriptionObject.getAttribute("text");
			int fontSize = Integer.parseInt(xmlDescriptionObject.getAttribute("FontSize"));
			Color textColor = new Color(Integer.parseInt(xmlDescriptionObject.getAttribute("TextColor")));
			String imagePath = xmlDescriptionObject.getAttribute("imagePath");
			int locationX = Integer.parseInt(xmlDescriptionObject.getAttribute("locationX"));
			int locationY = Integer.parseInt(xmlDescriptionObject.getAttribute("locationY"));
			
			DescriptionObject desObj = new DescriptionObject(this, controlType);
			desObj.setImagePath(imagePath);
			desObj.setText(text);
			desObj.setFontSize(fontSize);
			desObj.setTextColor(textColor);
			
			if(controlType == DescriptionObject.CONTROL_TEXT_LABEL){
				desObj.setTextLabel();
			}else{
				desObj.setImageLabel();
			}
			desObj.getLbImage().setBounds(locationX, locationY,desObj.getLbImage().getWidth(),desObj.getLbImage().getHeight());
			this.addDescriptionObject(desObj);
		}
	}
	
	public Color getDefaultTextColor() {
		return defaultTextColor;
	}

	public void setDefaultTextColor(Color defaultTextColor) {
		this.defaultTextColor = defaultTextColor;
	}

	public int getDefaultFontSize() {
		return defaultFontSize;
	}

	public void setDefaultFontSize(int defaultFontSize) {
		this.defaultFontSize = defaultFontSize;
	}

	public CurveView getParent() {
		return parent;
	}

	public void setParent(CurveView parent) {
		this.parent = parent;
	}
	
	
}
