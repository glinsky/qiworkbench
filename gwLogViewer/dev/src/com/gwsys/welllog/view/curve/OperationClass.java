/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view.curve;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.gwsys.data.seismic.su.SuParse;
import com.gwsys.data.well.AscParse;
import com.gwsys.data.well.DataException;
import com.gwsys.data.well.las.LasUtility;
import com.gwsys.data.well.LogData;
import com.gwsys.data.well.las.LasParse;
import com.gwsys.data.well.marker.MarkerFileOperator;
import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.data.well.xml.XmlParse;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.mainframe.SplitPaneView;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.WellInfo;
import com.gwsys.welllog.view.projecttree.ProjectTreeMarkerNodeObject;
import com.gwsys.welllog.view.projecttree.ProjectTreeView;
import com.gwsys.welllog.view.yaxis.DataConverterFactory;
import com.gwsys.welllog.view.yaxis.DepthConverter;

/**
 * Provides static utility methods.
 * @author Team
 *
 */
public class OperationClass implements Serializable {

	public static final String hashMapNameSeparator = ">";

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @author luming
	 * @param ld
	 *            lasData
	 * @param hashCurveName
	 *            hashkey
	 * @param i
	 *            which curve
	 * @return CurveData object
	 */
	public static CurveData createCurveData(LogData logdata, String hashCurveName, int i) {
		String curvenames[] = logdata.getAllCurveNames(); // save all the
															// curvename, to be
															// used
		CurveData cd = new CurveData();
		if(logdata.getDepthUnits().equals("M")||logdata.getDepthUnits().equals("m")){
			cd.setDepthUnit(CurveData.METER);
		}else if(logdata.getDepthUnits().equals("s")){
			cd.setDepthUnit(CurveData.SECOND);
		}else{
			cd.setDepthUnit(CurveData.FOOT);
		}
		cd.setOriginalYType(logdata.getCoordinateType());
		cd.setHashCurveName(hashCurveName);
		cd.setCurveName(curvenames[i]);
		cd.setCurveUnit(logdata.getCurveUnitMap().get(curvenames[i]).toString());
		cd.setDepthStep(((Double) logdata.getStep()).floatValue()); // set depthstep

		int count = 0;
		float[] fx = logdata.getCurveValueByName(curvenames[i]);
		float[] fy = logdata.getCurveValueByName(curvenames[0]);
		for(int j = 0; j < fx.length ; j++){
			if(fx[j] !=  Float.MAX_VALUE){
				count++;
			}
		}

		float[] dx = new float[count];
		float[] dy = new float[count];

		count = 0;
		for(int j = 0; j < fx.length ; j++){
			if(fx[j] !=  Float.MAX_VALUE){
				dx[count] = fx[j];
				dy[count] = fy[j];
				count++;
			}
		}

		cd.setX(dx);
		cd.setY(dy);

		return cd; // return the curvedata
	}

	public static float getMaxNum(float[] array) {
		float maxNum = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maxNum) {
				maxNum = array[i];
			}
		}
		return maxNum;
	}

	public static float getMinNum(float[] array) {
		float minNum = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] < minNum) {
				minNum = array[i];
			}
		}
		return minNum;
	}

	/**
	 * @author luming
	 * @param x
	 *            x coordinator
	 * @param y
	 * @return a curve , shape type
	 */
	public static Shape drawShape(float tempx[], float tempy[]) {
		GeneralPath path = null;
		Shape myCurve;
		try {
			if (path == null) {
				path = new GeneralPath();
			}

			path.moveTo(tempx[0], tempy[0]);
			for (int i = 1; i < tempx.length; i++) {
				path.lineTo(tempx[i], tempy[i]);
			}
			AffineTransform t = new AffineTransform();
			myCurve = t.createTransformedShape(path);

			return myCurve;

		} catch (Exception e) {
			System.out.println(" drawShape error : " + e);
			return null;
		}
	}

	/**
	 * after parse the file , do this
	 *
	 * @param logdata
	 * @param file
	 * @param jMainFrame
	 */
	@SuppressWarnings("unchecked")
	public static void initialTreeAndHashMap(File file, MainFrameView jMainFrame) {
		// if ld not null
		LogData logData = openFile(file);
		if (logData == null) {
			return;
		}

		String wellName = logData.getWell();
		String sourceName = file.getPath();
		String curveNames[] = logData.getAllCurveNames();

		WellInfo wellInfo = ViewCoreOperator.getWellInfoByWellName(sourceName);
		if(wellInfo == null){
			try{
			wellInfo = new WellInfo();
			wellInfo.setSourceName(sourceName);
			wellInfo.setWellName(wellName);
			wellInfo.setXLocation(Float.parseFloat(numberFiliter(logData.getSurfaceX())));
			wellInfo.setYLocation(Float.parseFloat(numberFiliter(logData.getSurfaceY())));
			wellInfo.setCurveNames(curveNames);
			wellInfo.setCurveUnits(logData.getCurveUnitMap());
			wellInfo.setTopDepth(logData.getStartDepth().floatValue());
			wellInfo.setBottomDepth(logData.getStopDepth().floatValue());
			}catch(NumberFormatException e){
				JOptionPane.showMessageDialog(jMainFrame.getFrame(),
						"Could not parse "+e.getMessage());
				return;
			}
			jMainFrame.getMainBuffer().getWellInfoList().add(wellInfo);
		}

		ArrayList curveNameList = new ArrayList();
		for (int i = 1; i < curveNames.length; i++) {
			// loop to save data to static variable curvehashmap
			// the key of the hashmap is added
			String curveHashKey = wellName + hashMapNameSeparator + curveNames[i];

			CurveData cd = createCurveData(logData, curveHashKey, i);
			if (cd.getX().length >0 && cd.getY().length >0) {
				curveNameList.add(curveNames[i]);
				cd.calculate();
				jMainFrame.getMainBuffer().getCurveHashMap().put(curveHashKey, cd);
			}
		}
		// update tree��curvename is a list here,so it should out of
		// the loop
		ProjectTreeView ltv = (ProjectTreeView) ((SplitPaneView) (jMainFrame).getSonJSplitPane()).getLeftTree();
		ltv.addCurveNameNode(wellName, file.getAbsolutePath(), curveNameList);
		ltv.setRootVisible(true);

	}

	//remove any no digital char
	private static String numberFiliter(String ss){
		if (ss.length()<=0) return "";
		String value=ss.trim();
		char c;
		for(int i=0; i<value.length(); i++){
	    	c=value.charAt(i);
		    if( (c<'0' || c>'9') &&(c!='-') && (c!='.') )
		        return value.substring(0,i);
		}
		return value;
	}
		
    /**
     * complete marker file process
	 * @param markerList
	 * @param path
	 */
	@SuppressWarnings("unchecked")
	public static void completeWellTopsTree(MainFrameView jMainFrame, ArrayList markerList, String path) {
		if (!DataBuffer.getJMainFrame().getMainBuffer().getAllWellTops().contains(path)) {
			DataBuffer.getJMainFrame().getMainBuffer().getAllWellTops().add(path); // save
		}
		
		for(int i=0 ; i<markerList.size() ; i++){
			ProjectTreeView ltv = (ProjectTreeView) ((SplitPaneView) (jMainFrame).getSonJSplitPane()).getLeftTree();
			MarkerInfo markerInfo = (MarkerInfo)markerList.get(i);
			ViewCoreOperator.addMarkerInfo(markerInfo);
			ltv.addWellTopNode(markerInfo.getWellName(),markerInfo.getMarkerName(),ProjectTreeMarkerNodeObject.WELL_NAME,false);
			ltv.setRootVisible(true);
		}
		ViewCoreOperator.connectMarker(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView());		
	}

	// process marker file
	public static boolean initialWellTopsTree(File file , MainFrameView jMainFrame){
		MarkerFileOperator operator = new MarkerFileOperator();
		try{
			operator.read(file);
		}catch(Exception e){
			return false;
		}
		ArrayList markerList = operator.getMarkerList();
		completeWellTopsTree(jMainFrame, markerList, file.getPath());
		return true;
	}

	@SuppressWarnings("unchecked")
	public static boolean initialCheckShotsTree(File file , MainFrameView jMainFrame){
		DepthConverter converter = DataConverterFactory.createInstance(file.getPath(),TrackTypeList.TIME_TO_MD);
		if (converter == null) {
			return false;
		}

		String fileName = file.getName();
		int nLen = fileName.lastIndexOf(".");
		String checkshotHashKey = fileName.substring(0,nLen);

		if((DepthConverter)(DataBuffer.getJMainFrame().getMainBuffer().getCheckshotHashMap().get(checkshotHashKey))!=null){
			return true;
		}

		DataBuffer.getJMainFrame().getMainBuffer().getCheckshotHashMap().put(checkshotHashKey,converter);
		DataBuffer.getJMainFrame().getMainBuffer().getAllCheckShots().add(file.getPath());

		ProjectTreeView ltv = (ProjectTreeView) ((SplitPaneView) (jMainFrame).getSonJSplitPane()).getLeftTree();
		ltv.addCheckShotNode(checkshotHashKey, file);
		ltv.setRootVisible(true);
		return true;
	}

	public boolean pickLineByPoint(double px, double py, float a1x, float a1y, float a2x, float a2y, double radius) {
		// len1 ,distance of p and a1, len2, distance of p and a2,len distance
		// of a1 and a2
		double len1 = Math.sqrt((px - (double) a1x) * (px - (double) a1x) + (py - (double) a1y) * (py - (double) a1y));
		if (len1 < radius) {
			return true;
		}

		double len2 = Math.sqrt((px - (double) a2x) * (px - (double) a2x) + (py - (double) a2y) * (py - (double) a2y));
		if (len2 < radius) {
			return true;
		}

		double len = Math.sqrt((a2x - a1x) * (a2x - a1x) + (a2y - a1y) * (a2y - a1y));
		if (len == 0.0D) {
			return false;
		}

		double m = (((double) a2x - px) * (double) (a2y - a1y) - ((double) a2y - py) * (double) (a2x - a1x)) / len;
		if (m < 0.0D) {
			m = -m;
		}
		if (m > radius) {
			return false;
		}
		if (len1 > len2) {
			return len1 * len1 - m * m < len * len;
		} else {
			return len2 * len2 - m * m < len * len;
		}
	}

	public boolean pick(Shape gp, double px, double py, 
			double radius, boolean contains) {
		PathIterator pi = gp.getPathIterator(null);
		float coords[] = new float[6];
		float x = 0.0F;
		float y = 0.0F;
		float xc = 0.0F;
		float yc = 0.0F;
		for (; !pi.isDone(); pi.next()) {
			switch (pi.currentSegment(coords)) {
			default:
				break;

			case 0: // '\0'
				x = xc = coords[0];
				y = yc = coords[1];
				break;

			case 1: // '\001'
				if (pickLineByPoint(px, py, x, y, coords[0], coords[1], radius)) {
					return true;
				}
				x = coords[0];
				y = coords[1];
				break;

			case 2: // '\002'
				if (pickLineByPoint(px, py, x, y, coords[0], coords[1], radius)) {
					return true;
				}
				if (pickLineByPoint(px, py, coords[0], coords[1], coords[2], coords[3], radius)) {
					return true;
				}
				x = coords[2];
				y = coords[3];
				break;

			case 3: // '\003'
				if (pickLineByPoint(px, py, x, y, coords[0], coords[1], radius)) {
					return true;
				}
				if (pickLineByPoint(px, py, coords[0], coords[1], coords[2], coords[3], radius)) {
					return true;
				}
				if (pickLineByPoint(px, py, coords[2], coords[3], coords[4], coords[5], radius)) {
					return true;
				}
				x = coords[4];
				y = coords[5];
				break;

			case 4: // '\004'
				if (pickLineByPoint(px, py, x, y, xc, yc, radius)) {
					return true;
				}
				if (contains && gp.contains(px, py)) {
					return true;
				}
				break;
			}
		}

		return false;
	}
	/**
	 * Parses .las/.xml/.dat/.su file and return Logdata.
	 * @since July 5, 2006 Move here from MainFrameView
	 * @author gaofei
	 */
	private static LogData openFile(File file){
		LogData logData = null;
		try {
			if (file.getName().toLowerCase().endsWith("las")) {
				LasParse lasParse = new LasParse();
				logData = lasParse.parse(file);
			} else if (file.getName().toLowerCase().endsWith("xml")) {
				XmlParse xmlParse = new XmlParse();
				logData = xmlParse.parse(file);

			} else if (file.getName().toLowerCase().endsWith("dat")) {
				AscParse ascParse = new AscParse();
				logData = ascParse.parse(file);
			} else if (file.getName().toLowerCase().endsWith("su")) {
				SuParse suParse = new SuParse();
				logData = suParse.parse(file);
			}
		} catch (DataException de) {
			DataBuffer.getJMainFrame().setMessage("Error: Cannot open file!");
			JOptionPane.showMessageDialog(null, de.getMessage());
		} catch (IOException ioe) {
			DataBuffer.getJMainFrame().setMessage("Error: Cannot open file!");
			JOptionPane.showMessageDialog(null, ioe.getMessage());
		} catch (SAXException se) {
			DataBuffer.getJMainFrame().setMessage("Error: Cannot open file!");
			JOptionPane.showMessageDialog(null, se.getMessage());
		} catch (ParserConfigurationException pce) {
			DataBuffer.getJMainFrame().setMessage("Error: Cannot open file!");
			JOptionPane.showMessageDialog(null, pce.getMessage());
		}
		return logData;
	}

	/**
	 * get source data from wellInfo to rebuild las data
	 * @param curveHashKey
	 * @param depth depth to be updated
	 * @param value new value to be set
	 * @author Alex
	 */
	public static void tryToRewriteLasData(String curveHashKey, float depth, float value)	{
		String wellName = curveHashKey.split(hashMapNameSeparator)[0];
		String filename;
		String sourceName;

		WellInfo wellInfo = ViewCoreOperator.getWellInfoByWell(wellName);

		if (wellInfo == null)
			return;

		sourceName = wellInfo.getSourceName();

		if (!DataBuffer.getJMainFrame().getMainBuffer().isThisLogExists(curveHashKey))	{
			String fname = DataBuffer.getJMainFrame().getMainBuffer().createTmpLogFile(curveHashKey);
			LasUtility.copyFileContent(sourceName, fname);
		}

		filename = DataBuffer.getJMainFrame().getMainBuffer().getTmpLogFile(curveHashKey);
		rebuildLasData(filename, curveHashKey, depth, value);
	}

	/**
	 * call a method to rewrite Las data
	 * @param source source name
	 * @param curveHashKey curvehashkey
	 * @param depth depth to update
	 * @param value new value to write
	 * @author Alex
	 */
	private static void rebuildLasData(String source, String curveHashKey, float depth, float value)	{
		String curve = curveHashKey.split(hashMapNameSeparator)[1];
		File auxfile = null;

		try {
			auxfile = File.createTempFile("aux", ".tmp");
			String output = auxfile.getPath();

			LasUtility lasUtil = new LasUtility();
			lasUtil.setSelectedDepth(depth);
			lasUtil.setNewValue(value);
			lasUtil.setSelectedCurve(curve);

			if (lasUtil.rewriteLasData(source, output))	{
				File srcFile = new File(source);

				srcFile.delete();

				if (!auxfile.renameTo(new File(source)))
					System.out.println("File was not be renamed\nFile is in: "+auxfile.getPath());

				int index = lasUtil.getSelectedCurveIndex();
				updateLasData(source, curveHashKey, index);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * called after rewrite Las file, try to update Las data into mainbuffer
	 * @param source filename
	 * @param curveHashKey well + curve
	 * @param index index of curve in hash map
	 * @author Alex
	 */
	@SuppressWarnings("unchecked")
	private static void updateLasData(String source, String curveHashKey, int index)	{
		LasParse lasParse = new LasParse();

		try {
			LogData logData = lasParse.parse(new File(source));

			if (logData==null)
				return;

			CurveData cd = createCurveData(logData, curveHashKey, index);
			cd.calculate();
			DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().put(curveHashKey, cd);

			// update changes in frameview
			String track = cd.getHashCurveName().split(OperationClass.hashMapNameSeparator)[0];
			ViewCoreOperator.deleteCurve(cd.getHashCurveName(), track);
			ViewCoreOperator.tryPutCurveToTrack(cd.getHashCurveName(), track);

		} catch (DataException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * save changes in well
	 * @param curveHashKey
	 * @return true if content of file was copied, false if not
	 * @author Alex
	 */
	public static boolean exportChangesToFile(String curveHashKey)	{
		String wellName = curveHashKey.split(hashMapNameSeparator)[0];
		WellInfo wellInfo = ViewCoreOperator.getWellInfoByWell(wellName);
		String fromFile, toFile;

		if (wellInfo == null)
			return false;

		toFile = wellInfo.getSourceName();
		fromFile = DataBuffer.getJMainFrame().getMainBuffer().getTmpLogFile(curveHashKey);

		if (LasUtility.copyFileContent(fromFile, toFile))	{
			removeTmpLogFile(curveHashKey);
			return true;
		}

		return false;
	}

	/**
	 * check if this log has changed
	 * @param curveHashKey
	 * @return true or false
	 * @author Alex
	 */
	public static boolean hasThisLogChanged(String curveHashKey)	{
		return DataBuffer.getJMainFrame().getMainBuffer().isThisLogExists(curveHashKey);
	}

	/**
	 * delete temp file related this curveHashKey
	 * @param curveHashKey
	 * @author Alex
	 */
	public static void removeTmpLogFile(String curveHashKey)	{
		String filename = DataBuffer.getJMainFrame().getMainBuffer().getTmpLogFile(curveHashKey);

		if (filename!=null)	{
			File tmpfile = new File(filename);

			if (tmpfile.delete())
				DataBuffer.getJMainFrame().getMainBuffer().removeTmpLogFile(curveHashKey);
		}
	}
}
