/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */

package com.gwsys.welllog.view.curve;

import com.gwsys.welllog.core.curve.TrackTypeList;

/**
 * A structure holds necessary curve information.
 * @author luming a curve
 */
public class CurveData {
	private String originalYType = TrackTypeList.Y_DEPTH_MD;

	private String hashCurveName = ""; // hashname
	private String curveName = ""; // original name
	private String curveUnit = "";
	private int depthUnit = CurveData.METER;

	public static final int METER = 1;
	public static final int FOOT = 2;
	public static final int SECOND = 3;

	private float[] x = null;
	private float[] y = null;

	private float maxX;
	private float minX;
	private float maxValue;
	private float minValue;

	private float depthStep = 0;

	private boolean isCalculate = false;

	private boolean isDraged = false;

	// finish

	public float[] getX() {
		return x;
	}

	public void setX(float[] x) {
		this.x = x;
	}

	public float[] getY() {
		return y;
	}

	public void setY(float[] y) {
		this.y = y;
	}

	public float getMaxX() {
		return maxX;
	}

	public void setMaxX(float maxX) {
		this.maxX = maxX;
	}

	public float getMinX() {
		return minX;
	}

	public void setMinX(float minX) {
		this.minX = minX;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	/**
	 * @author luming
	 */
	public String getHashCurveName() {
		return hashCurveName;
	}

	public void setHashCurveName(String hashCurveName) {
		this.hashCurveName = hashCurveName;
	}

	/**
	 * @return Name of Curve
	 */
	public String getCurveName() {
		return curveName;
	}

	public void setCurveName(String curveName) {
		this.curveName = curveName;
	}

	public float getDepthStep() {
		return depthStep;
	}

	public void setDepthStep(float depthStep) {
		this.depthStep = depthStep;
	}

	public String getOriginalYType() {
		return originalYType;
	}

	public void setOriginalYType(String originalType) {
		this.originalYType = originalType;
	}

	public String toString() {
		return curveName;
	}

	public int getDepthUnit() {
		return depthUnit;
	}

	public void setDepthUnit(int depthUnit) {
		this.depthUnit = depthUnit;
	}

	public static float getMaxNum(float[] array) {
		float maxNum = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maxNum) {
				maxNum = array[i];
			}
		}
		return maxNum;
	}

	public static float getMinNum(float[] array) {
		float minNum = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] < minNum) {
				minNum = array[i];
			}
		}
		return minNum;
	}

	public void calculate() {
		if (this.isCalculate) {
			return;
		}
		setMaxX(getMaxNum(x));
		setMinX(getMinNum(x));

		setMaxValue(getMaxNum(y));
		setMinValue(getMinNum(y));

		this.isCalculate = true;
	}

	public boolean isDraged() {
		return isDraged;
	}

	public void setDraged(boolean isDraged) {
		this.isDraged = isDraged;
	}

	/**
	 * Returns unit in lower case string.
	 * @return unit in lower case string.
	 */
	public String getCurveUnit(){
		return curveUnit;
	}

	/**
	 * Sets unit unit of curve data.
	 * @param unit A string represents unit of curve data.
	 */
	public void setCurveUnit(String unit){
		curveUnit = unit.toLowerCase();
	}
}
