/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.welllog.view;

import java.util.HashMap;

/**
 * Holds the header info of one well.
 * @author Team
 *
 */
public class WellInfo {

	private static final long serialVersionUID = 1L;

	private String sourceName;
	private String wellName;
	private float xLocation, yLocation;
	private float topDepth, botDepth;
	private String[] curveNames;
	private HashMap unitmap;
	/**
	 * Constructor.
	 *
	 */
	public WellInfo(){

	}

	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getWellName() {
		return wellName;
	}
	public void setWellName(String wellName) {
		this.wellName = wellName;
	}
	public float getXLocation() {
		return xLocation;
	}
	public void setXLocation(float location) {
		xLocation = location;
	}
	public float getYLocation() {
		return yLocation;
	}
	public void setYLocation(float location) {
		yLocation = location;
	}

	public String[] getCurveNames(){
		return curveNames;
	}

	public void setCurveNames(String[] names){
		curveNames = names;
	}

	public void setTopDepth(float value){
		topDepth = value;
	}

	public void setBottomDepth(float value){
		botDepth = value;
	}

	public float getTopDepth(){
		return topDepth;
	}

	public float getBottomDepth(){
		return botDepth;
	}

	public void setCurveUnits(HashMap unitmap){
		this.unitmap = unitmap;
	}

	@SuppressWarnings("unchecked")
	public String[] getUnitAsArray(){
		if (unitmap==null)
			return new String[0];
		Object[] toArray = unitmap.values().toArray(new String[0]);;
		return (String[])toArray;
	}
}
