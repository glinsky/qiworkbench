/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view;

import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;

//import javax.swing.tree.DefaultTreeModel;

import com.gwsys.data.seismic.su.SuToLogDataConverter;
import com.gwsys.data.well.XmlFileFilter;
import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.mainframe.MainBuffer;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.view.CurveItem;
import com.gwsys.welllog.view.GroupItem;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurveLayer;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.DescriptionPanel;
import com.gwsys.welllog.view.curve.LithologyPanel;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.curve.PanelOrder;
import com.gwsys.welllog.view.curve.SeismicPanel;
import com.gwsys.welllog.view.dialogs.SetDepthTypeDialog;
import com.gwsys.welllog.view.dialogs.SetTimeToDepthDialog;
import com.gwsys.welllog.view.fill.CompoundFill;
import com.gwsys.welllog.view.fill.CurveFillBeside;
import com.gwsys.welllog.view.fill.CurveFillTop;
import com.gwsys.welllog.view.fill.CurveLithologyBeside;
import com.gwsys.welllog.view.fill.MarkerFill;
import com.gwsys.welllog.view.fill.MarkerLithology;
import com.gwsys.welllog.view.projecttree.ProjectTreeMarkerNodeObject;
import com.gwsys.welllog.view.projecttree.ProjectTreeView;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.MarkerConnectorPoint;
import com.gwsys.welllog.view.track.TrackContainerView;
import com.gwsys.welllog.view.viewtree.ViewTree;
import com.gwsys.welllog.view.viewtree.ViewTreeNode;
import com.gwsys.welllog.view.viewtree.ViewTreeTrackNode;

/**
 * this class is designed to realize the synchronization of the operations to
 * both view tree and desktop panel. because the view tree and the desktop panel
 * must be always kept in same status, because they are only two different forms
 * of the same core datum.
 *
 * @author stone, lili
 *
 */
public class ViewCoreOperator {

	/**
	 * Clear existing project.
	 *
	 */
	public static void clearContent() {
		MainBuffer main = DataBuffer.getJMainFrame().getMainBuffer();
		main.getWellInfoList().clear();
		main.getCurveHashMap().clear();
		main.getAllWellTops().clear();
		main.getMarkerList().clear();
		main.getCheckshotHashMap().clear();
		main.getAllCheckShots().clear();
		main.getAllSurveys().clear();
		main.getProjectTree().setInitialize();
		SuToLogDataConverter.setWellCount(0);
		main.getDesktopPaneView().clear();

		main.getViewTree().clearContent();
	}

	/**
	 * create a frame with specific frame name, this method should be called
	 * only when load project
	 *
	 * @param frameTitle
	 * @param visible false in loading time
	 */
	@SuppressWarnings("unchecked")
	public static InternalFrameView createFrame(MainFrameView jMainFrame,
			String frameTitle, boolean visible) {
		// view tree part
		jMainFrame.getMainBuffer().getViewTree().createWellLog(frameTitle);
		// desktopPanel part
		InternalFrameView internalFrameView = new InternalFrameView(jMainFrame,
				frameTitle);
		if (visible)
			internalFrameView.setVisible(true);
		jMainFrame.getMainBuffer().getDesktopPaneView().add(internalFrameView);
		try {
			internalFrameView.setSelected(true);
		} catch (PropertyVetoException e) {
			jMainFrame.setMessage("Log window can not be selected!!");
		}
		// save the buffer , use for tree
		jMainFrame.getMainBuffer().setInternalFrameView(internalFrameView);
		jMainFrame.getMainBuffer().getDesktopPaneView()
				.getInternalFrameViewList().add(internalFrameView);
		return internalFrameView;
	}

	/**
	 * create a frame with unique frame name such as "Well Log 1,2...." format
	 */
	public static void createFrame(MainFrameView jMainFrame) {
		String frameTitle = "";

		boolean exist;
		do{
			frameTitle = LogWinUtility.generateNewFrameTitle();
			exist = false;
			ArrayList internalFrameViewList = jMainFrame.getMainBuffer().getDesktopPaneView().getInternalFrameViewList();
			for(int i=0 ; i<internalFrameViewList.size() ; i++){
				if(((InternalFrameView)internalFrameViewList.get(i)).getTitle().equals(frameTitle)){
					exist = true;
					break;
				}
			}
		}while(exist);

		createFrame(jMainFrame, frameTitle, true);
	}

	/**
	 * delete a frame from MainFrameView
	 *
	 * @param frameTitle
	 */
	public static void deleteFrame(MainFrameView jMainFrame, String frameTitle) {
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().removeWellLog(
				frameTitle);
		for (int i = 0; i < jMainFrame.getMainBuffer().getDesktopPaneView()
				.getInternalFrameViewList().size(); i++) {
			InternalFrameView internalFrameView = (InternalFrameView) jMainFrame
					.getMainBuffer().getDesktopPaneView()
					.getInternalFrameViewList().get(i);
			if (internalFrameView.getTitle().equals(frameTitle)) {
				jMainFrame.getMainBuffer().getDesktopPaneView()
						.getInternalFrameViewList().remove(i);
				jMainFrame.getMainBuffer().getDesktopPaneView().remove(
						internalFrameView);
				jMainFrame.getMainBuffer().getDesktopPaneView().repaint();
				return;
			}
		}
	}

	/**
	 * create a track with default properties
	 *
	 * @param trackName
	 */
	public static void createTrack(String trackID, String trackName,
			String wellName) {

		if (DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() == null) {
			return;
		}
		int trackWidth = ConstantVariableList.AXISCURVECONTAINER_DEFAULT_MINWIDTH;
		createTrack(trackID, trackName, wellName,trackWidth, 0,
				TrackTypeList.REALIZATION_BESIDE,true);
	}

	/**
	 * create a track with specific coordination properties
	 *
	 * @param trackName
	 * @param containerWidth
	 * @param containerHeight
	 * @param curveViewWidth
	 * @param curveViewHeight
	 * @param curveType
	 */
	@SuppressWarnings("unchecked")
	public static void createTrack(String trackID, String trackName, String wellName,
			int trackWidth, int trackHeight, String curveType, boolean hasWell) {
		if (DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() == null) {
			return;
		}

		// viewtree's part, complete
		String wellLogName = DataBuffer.getJMainFrame().getMainBuffer()
				.getInternalFrameView().getTitle();

		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createTrack(
				wellLogName, trackName,wellName, true);


		InternalFrameView selectedInternalFrameView = DataBuffer
				.getJMainFrame().getMainBuffer().getInternalFrameView();
		ArrayList axisCurveContainerList = selectedInternalFrameView
				.getTrackContainer().getAxisCurveContainerList();

		// bounxX represent the max x axis position of current internal frame
		// view's tracks
		int boundX = ConstantVariableList.AXISCURVECONTAINER_GAP; // the
																	// position
																	// in x axis
																	// of the
																	// AxisCurveContainerView
		int trackMaxHeight = 0;
		for (int i = 0; i < axisCurveContainerList.size(); i++) {
			int oldWidth = ((AxisCurveContainerView) axisCurveContainerList
					.get(i)).getWidth(); // the
			// width
			// of
			// current
			if(trackMaxHeight < ((AxisCurveContainerView) axisCurveContainerList.get(i)).getHeight()){
				trackMaxHeight = ((AxisCurveContainerView) axisCurveContainerList.get(i)).getHeight();
			}
			boundX = boundX + oldWidth
					+ ConstantVariableList.AXISCURVECONTAINER_GAP;
		}
		// create a container and initialize its properties
		AxisCurveContainerView newAxisCurveContainer = new AxisCurveContainerView(
				selectedInternalFrameView.getTrackContainer());
		newAxisCurveContainer.setTrackID(trackID);
		newAxisCurveContainer.setTrackName(trackName);
		newAxisCurveContainer.setWellName(wellName);
		newAxisCurveContainer.setHasCurve(hasWell);
		newAxisCurveContainer.getCurveLayer().setLayerType(curveType);
		newAxisCurveContainer.setDepthScale(selectedInternalFrameView.getDepthScale());

		int curveViewHeight = newAxisCurveContainer.getYAxisView().getYHeight();
		int yHeadHeight = (int) (newAxisCurveContainer.getBottomScrollPane().getBounds().getY());
		if(yHeadHeight < 95){
			yHeadHeight = 95;
		}
		int trackNewHeight = curveViewHeight+ yHeadHeight;
		if(trackNewHeight < trackHeight || trackHeight == 0){
			trackHeight = trackNewHeight;
		}
		if (trackHeight > selectedInternalFrameView.getTrackContainer().getAxisCurveContainerMaxHeight()) {
			trackHeight = (int) selectedInternalFrameView.getTrackContainer().getAxisCurveContainerMaxHeight();
		}
		trackHeight = Math.abs(trackHeight);
		if(trackMaxHeight < trackHeight){
			trackMaxHeight = trackHeight;
		}

		newAxisCurveContainer.setBounds(boundX, 0, trackWidth,
				trackHeight);

		newAxisCurveContainer.getCurveView().setPreferredSize(
				new Dimension(trackWidth, curveViewHeight));
		// add the new container to our containerList
		axisCurveContainerList.add(newAxisCurveContainer);
		// add the container component to internal frame view
		// and set internal frame view's proper size
		selectedInternalFrameView.getTrackContainer()
				.add(newAxisCurveContainer);
		selectedInternalFrameView.getTrackContainer().setPreferredSize(
				new Dimension(boundX + 400 + 100, trackMaxHeight + 100));
		// refresh
		selectedInternalFrameView.revalidate();
		selectedInternalFrameView.repaint();
		// new created track will be auto selected
		selectTrack(wellName);

		// load well template
		ViewCoreOperator.buidWellFromTemplate(wellLogName, wellName);
	}

	@SuppressWarnings("unchecked")
	public static void deleteTrack(String wellName, InternalFrameView internalFrameView) {
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().removeTrack(internalFrameView.getTitle(),wellName);

		ArrayList tracks = internalFrameView.getTrackContainer().getAxisCurveContainerList();
		for (int i = 0; i < tracks.size(); i++) {
			AxisCurveContainerView newAxisCurveContainer = (AxisCurveContainerView) tracks.get(i);
			if (newAxisCurveContainer.getWellName().equals(wellName)) {
				internalFrameView.getTrackContainer().remove(newAxisCurveContainer);
				tracks.remove(newAxisCurveContainer);
				break;
			}
		}
		
		// delete temp files used in exporting LAS changes. Alex, January 2007
		OperationClass.removeTmpLogFile(wellName);
				
		String WellAndTrack = internalFrameView.getTitle() +
		OperationClass.hashMapNameSeparator + wellName;
		if (DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().getAllWells().contains(WellAndTrack)) {
			DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().getAllWells().remove(WellAndTrack);
		}

		connectMarker(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()); //July 5th

		internalFrameView.revalidate();
		internalFrameView.repaintTrack();
		internalFrameView.repaint();
	}


	/**
	 * delete one curve from one specific track
	 *
	 * @param curveHashmapKey
	 * @param trackName
	 */
	public static void deleteCurve(String curveHashmapKey, String wellName) {
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		// viewtree's part, complete
		// by zzl 2006-3-16
		DefaultMutableTreeNode curve = DataBuffer.getJMainFrame()
				.getMainBuffer().getViewTree().findCurveUnderTrack(
						curveHashmapKey,
						DataBuffer.getJMainFrame().getMainBuffer()
								.getViewTree().findTrack(ifv.getTitle(),wellName));

		if (curve == null) {
			return;
		}
		if (((ViewTreeNode) curve.getUserObject()).getNodeType() != ViewTreeNode.CURVE_NODE) {
			return;
		}
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().removeCurve(
				curve);

		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView track = null;
		for (int i = 0; i < trackNumber; i++) {
			track = (AxisCurveContainerView) tracks.get(i);
			if (track.getWellName().equals(wellName)) {
				break;
			}
			track = null;
		}
		if (track == null)
			return;
		CurveView cv = track.getCurveView();
		CurvePanel cp = null;
		for(int i =0 ; i < cv.getCurveLayer().getCurveList().size(); i++){
			if (((CurvePanel)cv.getCurveLayer().getCurveList().get(i)).getCd().getHashCurveName().equals(curveHashmapKey)){
				cp = (CurvePanel)cv.getCurveLayer().getCurveList().get(i);
				//break;
			}
		}
		cv.getCurveLayer().removeCurveFromList(cp);
		cv.getPanelOrder().deletePanel(cp);
		cv.setCurveRedrawFlag(true);
		if (trackContainer.getSelectedAxisCurveContainer() != null) {
			trackContainer.getSelectedAxisCurveContainer().setNotSelected();
		}

		// new BY Lili
		CurveFillBeside cfb = null;
		for (int i = 0; i < cv.getCurveLayer().getCurveFillBesideList().size(); i++) {
			cfb = (CurveFillBeside) cv.getCurveLayer().getCurveFillBesideList()
					.get(i);
			if (cfb.getCp() == cp)
				cv.getCurveLayer().getCurveFillBesideList().remove(cfb);
			cv.setFillRedrawFlag(true);
		}
		CurveFillTop cft = null;
		for (int i = 0; i < cv.getCurveLayer().getCurveFillTopList().size(); i++) {
			cft = (CurveFillTop) cv.getCurveLayer().getCurveFillTopList()
					.get(i);
			if (cft.getC1() == cp || cft.getC2() == cp)
				cv.getCurveLayer().getCurveFillTopList().remove(cft);
			cv.setFillRedrawFlag(true);
		}

		cv.setProperSize();

		changeTrackSize(cv.getParentAxisCurveContainer());

		cv.getParentAxisCurveContainer().setSelected();
		cv.repaintFather();
		ViewCoreOperator.clearDraggedCurve();
	}

	/**
	 * select one specific frame(or well log for view tree)
	 *
	 * @param frameTitle
	 */
	public static void selectFrame(String frameTitle) {
		// view tree part
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.setActiveWellLog(frameTitle);
		// desktop panel part
		InternalFrameView internalFrame = DataBuffer.getJMainFrame()
				.getMainBuffer().getDesktopPaneView()
				.findInternalFrameViewByTitle(frameTitle);
		selectFrame(internalFrame);
	}

	/**
	 * select one specific frame(or well log for view tree)
	 *
	 * @param internalFrame
	 */
	public static void selectFrame(InternalFrameView internalFrame) {
		if (internalFrame == null) {
			return;
		}
		if (!internalFrame.isVisible()) {
			internalFrame.setVisible(true);
		}
		try {
			internalFrame.setSelected(true);
			internalFrame.setIcon(false);
		} catch (PropertyVetoException e) {
		}
	}

	/**
	 * Called by Wrokspace
	 *
	 */
	public static void updateProjectTreeSelect(){
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer()
				.getInternalFrameView();
		ProjectTreeView ltv = DataBuffer.getJMainFrame().getMainBuffer()
				.getProjectTree();

		for (int i = 0; i < ltv.getWellTops().getChildCount(); i++) {
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) ltv
					.getWellTops().getChildAt(i);
			ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject) nodeChild
					.getUserObject();
			ArrayList wellNameList = markerNode.getWellNameList();

			boolean show = false;
			the:for (int j = 0; j < wellNameList.size(); j++) {
				CurveMarker curveMarker = null;
				ArrayList list = ifv.getTrackContainer()
						.getAxisCurveContainerList();
				AxisCurveContainerView track = null;
				for (int z = 0; z < list.size(); z++) {
					if (wellNameList.get(j).equals(
							((AxisCurveContainerView) list.get(z))
									.getWellName())) {
						track = (AxisCurveContainerView) list.get(z);
						// find marker
						for (int ii = 0; ii < track.getCurveView()
								.getMarkerLayer().getMarkerList().size(); ii++) {
							curveMarker = (CurveMarker) track.getCurveView()
									.getMarkerLayer().getMarkerList().get(ii);
							if (curveMarker.getMarkerName().equals(
									markerNode.getMarkerName())
									&& curveMarker.isVisible() == true) {
								markerNode.setDraged(true);
								show = true;
								break the;
							}
						}
					}
				}
			}
			if (show == false)
				markerNode.setDraged(false);
		}
        ltv.repaint();
	}
	/**
	 * select one track
	 *
	 * @param trackName
	 */
	public static void selectTrack(String wellName) {
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		// New By Lili
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.setActiveTrack(ifv.getTitle(),wellName);

		TrackContainerView trackContainer = ifv.getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView currentTrack = null;
		for (int i = 0; i < trackNumber; i++) {
			currentTrack = (AxisCurveContainerView) tracks.get(i);
			if (currentTrack.getWellName().equals(wellName)) {
				break;
			}
			currentTrack = null;
		}
		if (currentTrack == null)
			return;

		if (trackContainer.getSelectedAxisCurveContainer() != null) {
			trackContainer.getSelectedAxisCurveContainer().setNotSelected();
		}
		trackContainer.setSelectedAxisCurveContainer(currentTrack);
		currentTrack.setSelected();

		for(int i =0 ; i < currentTrack.getCurveView().getCurveLayer().getCurveSelectedList().size(); i++){
			((CurvePanel)currentTrack.getCurveView().getCurveLayer().getCurveSelectedList().get(i)).setSelected(false);
		}
	}

	public static void selectDepthOrGroup(String wellName, String nodeName) {
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		selectTrack(wellName);
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.setActiveDepthOrGroup(nodeName, ifv.getTitle(),wellName);
	}

	/**
	 * select one curve, must also say the curve belongs to which track, because
	 * one curve may exists in more than one tracks
	 *
	 * @param curveHashmapKey
	 * @param trackName
	 * @param resetOthers
	 */
	public static void selectCurve(String curveHashmapKey, String wellName,
			boolean resetOthers) {
		// New By Lili
		// must select the curve's parent first
		selectTrack(wellName);

		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
		.getMainBuffer().getInternalFrameView().getTrackContainer();
		AxisCurveContainerView selectTrack = trackContainer.getSelectedAxisCurveContainer();
		if (selectTrack == null)
			return;

		if (curveHashmapKey != null) {
			DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
					.setActiveCurve(curveHashmapKey,trackContainer.getParentInternalFrameView().getTitle(), wellName);
		}

		CurveView cv = selectTrack.getCurveView();

		if (resetOthers == true) {
			for (int j = 0; j < cv.getCurveLayer().getCurveList().size(); j++) {
				if (((CurvePanel)cv.getCurveLayer().getCurveList().get(j)).getCd().getHashCurveName().equals(curveHashmapKey)){
					((CurvePanel)cv.getCurveLayer().getCurveList().get(j)).setSelected(true);
				}
			}
		}
	}

	public static void selectCurveUnderGroup(String wellName,
			String groupName, String curveHashmapKey) {
		selectTrack(wellName);

		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
		.getMainBuffer().getInternalFrameView().getTrackContainer();
		AxisCurveContainerView selectTrack = trackContainer.getSelectedAxisCurveContainer();
		if (selectTrack == null)
			return;

		if (curveHashmapKey != null) {
			DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
					.setActiveCurveUnderGroup(trackContainer.getParentInternalFrameView().getTitle(),wellName, groupName,
							curveHashmapKey);
		}

		CurveView cv = selectTrack.getCurveView();
		for(int j =0 ; j < cv.getCurveGroups().size() ; j++){
			CurveGroup cg = (CurveGroup) cv.getCurveGroups().get(j);
			for(int k =0 ; k < cg.getCurveList().size();k++){
				if (cg.getName().equals(groupName)
						&& ((CurvePanel)cg.getCurveList().get(k)).getCd().getHashCurveName().equals(curveHashmapKey)){
					((CurvePanel)cg.getCurveList().get(k)).setSelected(true);
				}
			}
		}
	}

	/**
	 * put one curve to one track, if curve already exists, then just select it.
	 *
	 * @param curveHashmapKey
	 * @param targetTrackName
	 */
	public static void putCurveToTrack(String curveHashmapKey,
			String wellName) {
		// viewtree's part, complete
		// viewTree.createCurve(targetTrackName, curveHashmapKey,
		// curveHashmapKey.split(OperationClass.hashMapNameSeparator)[1]);
		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView currentTrack = null;
		for (int i = 0; i < trackNumber; i++) {
			currentTrack = (AxisCurveContainerView) tracks.get(i);
			if (currentTrack.getWellName().equals(wellName)) {
				break;
			}
			currentTrack = null;
		}
		if (currentTrack == null) {
			return;
		}

		CurveData cd = (CurveData) DataBuffer.getJMainFrame().getMainBuffer()
				.getCurveHashMap().get(curveHashmapKey);
		CurveView cv = currentTrack.getCurveView();
		CurveLayer cl = cv.getCurveLayer();

		if (currentTrack.getCurveView().getCurveLayer().getCurveList().size() < 1) {
			currentTrack.setDepthUnit(cd.getDepthUnit());
		}

		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createCurve(
				trackContainer.getParentInternalFrameView().getTitle(),
				wellName, curveHashmapKey,
				curveHashmapKey.split(OperationClass.hashMapNameSeparator)[1]);
		CurvePanel cp = new CurvePanel(cd);

		//if this curve was dragged, copy its dragged curve's settings to this one
		if (ViewCoreOperator.isDraggedCurveExists(curveHashmapKey.split(OperationClass.hashMapNameSeparator)[1]))
			ViewCoreOperator.copyDraggedSettingsToCurve(cp);

		// apply global settings to this curve. Added by Alex
		applyGlobalSettingsToCurve(cp);

		cl.addCurveToList(cp);
		cv.getPanelOrder().addPanel(cp); // curve and other 's order

		cv.setCurveRedrawFlag(true);
		cv.setMarkerRedrawFlag(true);
		cl.deleteMeanP105090CurveFromList();
		cl.setP105090meanRefresh(true);

		if (trackContainer.getSelectedAxisCurveContainer() != null) {
			trackContainer.getSelectedAxisCurveContainer().setNotSelected();
		}

		trackContainer.setSelectedAxisCurveContainer(currentTrack);
		currentTrack.setSelected();
		cv.setProperSize();

		// //new By Lili
		if (!currentTrack.isHasWell()) {
			String currentWellName = cd.getHashCurveName().substring(
					0,
					cd.getHashCurveName().lastIndexOf(
							OperationClass.hashMapNameSeparator));
			AxisCurveContainerView oldTrack = null;
			for (int i = 0; i < trackNumber; i++) {
				oldTrack = (AxisCurveContainerView) tracks.get(i);
				if (oldTrack.getWellName().equals(currentWellName)) {
					break;
				}
				oldTrack = null;
			}
			if (oldTrack == null) {
				String TrackName = currentWellName;//currentTrack.getTrackName() + ":" + currentWellName;
				String trackID = currentTrack.getTrackID();
				DataBuffer.getJMainFrame().getMainBuffer().getProjectTree()
						.updateWellTopNodeTrackName(trackID, currentWellName);
				currentTrack.setTrackName(TrackName);
				currentTrack.setWellName(currentWellName);
				DefaultMutableTreeNode trackNode = DataBuffer.getJMainFrame()
						.getMainBuffer().getViewTree().findTrack(
								trackContainer.getParentInternalFrameView().getTitle(),wellName);
				((ViewTreeTrackNode)trackNode.getUserObject()).setDraged(true);
				((ViewTreeTrackNode)trackNode.getUserObject()).setTrackName(TrackName);
				((ViewTreeTrackNode)trackNode.getUserObject()).setWellName(currentWellName);

			}
		}

		cp.setTranslatedCurve(cv, cl.getBesideTypeWidth(), 1,1);
		CurveFillBeside cfb = new CurveFillBeside();
		cfb.setCp(cp);
		cfb.setLeftOrRight(ConstantVariableList.FILL_LEFT);
		cfb.constructFill(cv);
		cfb.setFillColor(Color.PINK);
		cv.getCurveLayer().addFillBesideToList(cfb);
		cv.setFillRedrawFlag(true);
		cv.getParentAxisCurveContainer().repaintAll();

		changeTrackSize(currentTrack);

		// clear dragged curve
		ViewCoreOperator.clearDraggedCurve();
	}

	public static void changeTrackSize(AxisCurveContainerView acc) {
		int widthTrack = acc.getCurveView().getWidth()+ 4;
		if ((widthTrack + 100) > (DataBuffer.getJMainFrame().getMainBuffer()
				.getInternalFrameView().getWidth() - 30)) {
			widthTrack = DataBuffer.getJMainFrame().getMainBuffer()
					.getInternalFrameView().getWidth() - 130;
		}
		if (widthTrack < 100) {
			return;
		}

		int oldWidth = acc.getWidth();

		if (widthTrack == oldWidth) {
			return;
		}
		acc.setBounds(acc.getX(), 0, widthTrack, acc.getHeight());

		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.revalidate();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.repaintTrack();
	}

	/**
	 * try to put one curve to one track. if the curve already exists in track,
	 * then do nothing if the curve has not responsible convert information,
	 * then try to get the information
	 *
	 * @param curveHashmapKey
	 * @param targetTrackName
	 */
	public static void tryPutCurveToTrack(String curveHashmapKey,
			String wellName) {
		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView currentTrack = null;
		for (int i = 0; i < trackNumber; i++) {
			currentTrack = (AxisCurveContainerView) tracks.get(i);
			if (currentTrack.getWellName().equals(wellName)) {
				break;
			}
			currentTrack = null;
		}
		if (currentTrack == null) {
			return;
		}

		// check if this operation is necessary
		ViewTree vtree=DataBuffer.getJMainFrame().getMainBuffer().getViewTree();
		if (vtree.checkIfNodeDndHashmapKeyExistsInThisTrack(curveHashmapKey,
				vtree.findTrack(trackContainer.getParentInternalFrameView().getTitle(),wellName))) {
			selectTrack(wellName);
			return;
		}
		CurveData cd = (CurveData) DataBuffer.getJMainFrame().getMainBuffer()
				.getCurveHashMap().get(curveHashmapKey);

		// this comparison was added, August 25, 2006
		if (cd != null)	{
			if (cd.getOriginalYType().equals(TrackTypeList.Y_DEPTH_MD)) {
				if (currentTrack.getYAxisView().getYType().equals(TrackTypeList.Y_TIME)
						&& currentTrack.getDepthToTimeConverter() == null) {
					// require convert info
					// use constructor method with correct flag, then must set the
					// curve and track
					SetTimeToDepthDialog yacd = new SetTimeToDepthDialog(
							currentTrack, curveHashmapKey, false);
					yacd.setVisible(true);
				} else {
					putCurveToTrack(curveHashmapKey, wellName);
				}
			} else {
				if (currentTrack.getTrackType() == ConstantVariableList.TRACK_TYPE_NONE) {
					currentTrack.getYAxisView().setYType(TrackTypeList.Y_TIME);
					putCurveToTrack(curveHashmapKey, wellName);
				} else {
					if (currentTrack.getYAxisView().getYType().equals(TrackTypeList.Y_DEPTH_MD)
							&& currentTrack.getTimeToDepthConverter() == null) {
						// require convert info
						if (currentTrack.getTimeToDepthConverter() == null) {
							SetTimeToDepthDialog setTimeToDepthDialog = new SetTimeToDepthDialog(
									currentTrack, curveHashmapKey, true);
							setTimeToDepthDialog.setVisible(true);
						}
					} else {
						putCurveToTrack(curveHashmapKey, wellName);
					}
				}
			}
		}

	}

	public static boolean IsHasThisCurveUnderGroup(String curveHashmapKey,
			String groupHashmapKey){
		boolean IsHas = false;

		CurveView cv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
		.getTrackContainer().getSelectedAxisCurveContainer().getCurveView();

		String groupName = groupHashmapKey.split(OperationClass.hashMapNameSeparator)[1];
		int index = groupName.lastIndexOf(' ');
		groupName = groupName.substring(index + 1);

		for (int i = 0; i < cv.getCurveGroups().size(); i++) {
			CurveGroup cg = (CurveGroup) cv.getCurveGroups().get(i);
			if (cg.getName().equals(groupName)) {
				for(int j = 0 ; j < cg.getCurveList().size() ; j++){
					CurvePanel cp = (CurvePanel)cg.getCurveList().get(j);
					if(cp.getCd().getHashCurveName().equals(curveHashmapKey)){
						return true;
					}
				}
				break;
			}
		}
		return IsHas;
	}
	public static void putCurveToGroupPanel(String curveHashmapKey,
			String groupHashmapKey) {
		AxisCurveContainerView selectedAxisCurveContainerView = DataBuffer
				.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer();
		CurveView cv = selectedAxisCurveContainerView.getCurveView();

		String curveName = curveHashmapKey.split(OperationClass.hashMapNameSeparator)[1];
		CurvePanel cp = null;
		for (int i = 0; i < cv.getCurveLayer().getCurveList().size(); i++) {
			if (((CurvePanel) cv.getCurveLayer().getCurveList().get(i)).getCd()
					.getCurveName().equals(curveName)) {
				cp = (CurvePanel) cv.getCurveLayer().getCurveList().get(i);
				break;
			}
		}
		if(cp == null){
			return;
		}

		String groupName = groupHashmapKey.split(OperationClass.hashMapNameSeparator)[1];
		int index = groupName.lastIndexOf(' ');
		groupName = groupName.substring(index + 1);

		CurveGroup cg = null;
		for (int i = 0; i < cv.getCurveGroups().size(); i++) {
			if (((CurveGroup) cv.getCurveGroups().get(i)).getName().equals(
					groupName)) {
				cg = (CurveGroup) cv.getCurveGroups().get(i);
				break;
			}
		}
		if(cg == null){
			return;
		}
		if (cg.getCurveList().size() > 0)
			cp.setBesideTypeWidth(cg.getBesideTypeWidth());
		if(cg.isSetRange()){
			cp.setMaxXDisplay(Float.parseFloat(cg.getMaxXDisplay()));
			cp.setMinXDisplay(Float.parseFloat(cg.getMinXDisplay()));
		}
		putCurveToGroupPanel(cv,cp, cg);

	}

	@SuppressWarnings("unchecked")
	public static void putCurveToGroupPanel(CurveView cv, CurvePanel cp, CurveGroup group) {
		CurveLayer cl = cv.getCurveLayer();
		PanelOrder order = cv.getPanelOrder();

		cp.setCg(group);
		group.getCurveList().add(cp);

		order.deletePanel(cp);
		cl.getCurveList().remove(cp);

		for (int i = 0; i < cl.getCurveFillBesideList().size(); i++) {
			CurveFillBeside beside = (CurveFillBeside) cl.getCurveFillBesideList().get(i);
			if (beside.getCp().getCd().getHashCurveName().equals(cp.getCd().getHashCurveName()))
				cl.getCurveFillBesideList().remove(beside);
		}

		for (int i = 0; i < cl.getCurveLithologyBesideList().size(); i++) {
			CurveLithologyBeside beside = (CurveLithologyBeside) cl
					.getCurveLithologyBesideList().get(i);
			if (beside.getCp().getCd().getHashCurveName().equals(cp.getCd().getHashCurveName()))
				cl.getCurveLithologyBesideList().remove(beside);
		}

		// repaint
		cv.setShowFill(true);
		cv.setFillRedrawFlag(true);
		cv.setCurveRedrawFlag(true);
		cv.setProperSize();
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer()
				.repaintAll();
	}

	public static void removeGroup(String wellName, String groupName) {
		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView track = null;
		for (int i = 0; i < trackNumber; i++) {
			track = (AxisCurveContainerView) tracks.get(i);
			if (track.getWellName().equals(wellName)) {
				break;
			}
			track = null;
		}
		if (track == null)
			return;
		CurveView cv = track.getCurveView();

		for (int i = 0; i < cv.getCurveGroups().size(); i++) {
			CurveGroup group = (CurveGroup) cv.getCurveGroups().get(i);
			if (group.getName().equals(groupName)) {
				cleanGroup(cv, group);		// by Alex. April 18, 2007
				cv.getCurveGroups().remove(i);
				cv.getPanelOrder().deletePanel(group);
			}
		}

		// repaint
		cv.setShowFill(true);
		cv.setFillRedrawFlag(true);
		changeTrackSize(cv.getParentAxisCurveContainer());
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer()
				.repaintAll();

	}
	
	/**
	 * used to delete all Group Fills
	 * @param cv CurveView
	 * @param group CurveGroup
	 * @author Alex
	 */
	private static void cleanGroup(CurveView cv, CurveGroup group)	{
		for (int i = 0; i < group.getCurveList().size(); i++)	{
			String curveName = ((CurvePanel) group.getCurveList().get(i)).getCd().getCurveName();
			removeGroupFill(cv, curveName);
		}
	}

	public static void removeCurveFromGroup(String wellName, String groupName,
			String curveName) {
		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView track = null;
		for (int i = 0; i < trackNumber; i++) {
			track = (AxisCurveContainerView) tracks.get(i);
			if (track.getWellName().equals(wellName)) {
				break;
			}
			track = null;
		}
		if (track == null)	{
			DataBuffer.getJMainFrame().setMessage("Track to be removed is null!!");
			return;
		}
		CurveView cv = track.getCurveView();

		CurveGroup group = null;
		for (int i = 0; i < cv.getCurveGroups().size(); i++) {
			if (((CurveGroup) cv.getCurveGroups().get(i)).getName().equals(
					groupName)) {

				group = (CurveGroup) cv.getCurveGroups().get(i);
				for (int j = 0; j < group.getCurveList().size(); j++) {
					if (((CurvePanel) group.getCurveList().get(j)).getCd()
							.getCurveName().equals(curveName)) {
						group.getCurveList().remove(j);
						break;
					}
				}
			}
		}
				
		// added by Alex. April 16, 2007
		removeGroupFill(cv, curveName);

		cv.setShowFill(true);
		cv.setFillRedrawFlag(true);
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer()
				.repaintAll();
	}
	
	/**
	 * remove a group fill when given curve was deleted
	 * @param cv CurveView
	 * @param curveName curve name
	 * @author Alex
	 */
	private static void removeGroupFill(CurveView cv, String curveName)	{
		CompoundFill cf;		
		for (int i = 0; i < cv.getCurveLayer().getCompoundFillList().size(); i++)	{
			cf = (CompoundFill) cv.getCurveLayer().getCompoundFillList().get(i);
			if (cf.getLeftCurve().getCd().getCurveName().equals(curveName)
				|| cf.getRightCurve().getCd().getCurveName().equals(curveName))	{
				cv.getCurveLayer().getCompoundFillList().remove(i);
				i--;
			}
		}
		
		// placed here by Alex. April 18, 2007
		CurveFillBeside cfb = null;
		for (int i = 0; i < cv.getCurveLayer().getCurveFillBesideList().size(); i++) {
			cfb = (CurveFillBeside) cv.getCurveLayer().getCurveFillBesideList()
					.get(i);
			if (cfb.getCp().getCd().getCurveName().equals(curveName))
				cv.getCurveLayer().getCurveFillBesideList().remove(cfb);
		}
	}

	@SuppressWarnings("unchecked")
	public static void addGroupPanel() {
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		AxisCurveContainerView selectedAxisCurveContainer = ifv.getTrackContainer().getSelectedAxisCurveContainer();

		CurveView curveView = selectedAxisCurveContainer.getCurveView();
		int counter = curveView.getCurveGroups().size()+1;
		//fix the bug : number of group is based on the existing groups
		String groupPanelName = ConstantVariableList.GROUP_PANEL + counter;
		// get track name
		String wellName = selectedAxisCurveContainer.getWellName();

		// add group panel node to view tree
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.createGroupPanel(ifv.getTitle(),wellName, groupPanelName, true);

		// add group panel border to track view
		CurveGroup curveGroup = new CurveGroup(groupPanelName);
		curveView.getCurveGroups().add(curveGroup);
		curveView.getPanelOrder().addPanel(curveGroup);

		changeTrackSize(selectedAxisCurveContainer);
		selectedAxisCurveContainer.repaintAll();
	}

	@SuppressWarnings("unchecked")
	public static void addDescriptionPanel() {
		AxisCurveContainerView selectedAxisCurveContainer = DataBuffer
				.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer();
		String desPanelName = ConstantVariableList.DESCRIPTION_PANEL
				+ " "
				+ (selectedAxisCurveContainer.getCurveView().getDescriptions()
						.size() + 1);

		// get track name
		String wellName = selectedAxisCurveContainer.getWellName();

		// add group panel node to view tree
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.createDescriptionPanel(wellName, desPanelName);

		// add group panel border to track view
		CurveView curveView = selectedAxisCurveContainer.getCurveView();
		DescriptionPanel desPanel = new DescriptionPanel(curveView,
				desPanelName);
		curveView.getDescriptions().add(desPanel);
		curveView.getPanelOrder().addPanel(desPanel);

		changeTrackSize(selectedAxisCurveContainer);
		selectedAxisCurveContainer.repaintAll();
	}

	public static void removeDescriptionPanel(String wellName,
			String descriptionName) {
		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView track = null;
		for (int i = 0; i < trackNumber; i++) {
			track = (AxisCurveContainerView) tracks.get(i);
			if (track.getWellName().equals(wellName)) {
				break;
			}
			track = null;
		}
		if (track == null)
			return;
		CurveView cv = track.getCurveView();

		DescriptionPanel desPanel = null;
		for (int i = 0; i < cv.getDescriptions().size(); i++) {
			if (((DescriptionPanel) cv.getDescriptions().get(i)).getName()
					.equals(descriptionName)) {
				desPanel = (DescriptionPanel) cv.getDescriptions().get(i);
				cv.getDescriptions().remove(desPanel);
			}
		}

		cv.getPanelOrder().deletePanel(desPanel);

		changeTrackSize(track);

		cv.setFillRedrawFlag(true);
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer()
				.repaintAll();

		// view tree
		DefaultMutableTreeNode trackNode = DataBuffer.getJMainFrame()
				.getMainBuffer().getViewTree().findTrack(trackContainer.getParentInternalFrameView().getTitle(),wellName);
		DefaultMutableTreeNode desPanelNode = DataBuffer.getJMainFrame()
				.getMainBuffer().getViewTree().findPanelUnderTrack(
						descriptionName, trackNode);
		if (desPanelNode == null) {
			return;
		}
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().removeCurve(
				desPanelNode);

	}


	@SuppressWarnings("unchecked")
	public static void addSeismicPanel() {
		AxisCurveContainerView selectedAxisCurveContainer = DataBuffer
				.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer();
		String seismicPanelName = ConstantVariableList.SEISMIC_PANEL
				+ " "
				+ (selectedAxisCurveContainer.getCurveView().getSeismics()
						.size() + 1);

		// get track name
		String wellName = selectedAxisCurveContainer.getWellName();

		// add group panel node to view tree
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.createSeismicPanel(wellName, seismicPanelName);

		// add group panel border to track view
		CurveView curveView = selectedAxisCurveContainer.getCurveView();
		SeismicPanel seismicPanel = new SeismicPanel(curveView,
				seismicPanelName);
		curveView.getSeismics().add(seismicPanel);
		curveView.getPanelOrder().addPanel(seismicPanel);

		changeTrackSize(selectedAxisCurveContainer);
		selectedAxisCurveContainer.repaintAll();
	}

	public static void removeSeismicPanel(String wellName, String seismicName) {
		// System.out.println("trackName:" + trackName + ",descriptionName:" +
		// descriptionName);
		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView track = null;
		for (int i = 0; i < trackNumber; i++) {
			track = (AxisCurveContainerView) tracks.get(i);
			if (track.getWellName().equals(wellName)) {
				break;
			}
			track = null;
		}
		if (track == null)
			return;
		CurveView cv = track.getCurveView();

		SeismicPanel seismicPanel = null;
		for (int i = 0; i < cv.getSeismics().size(); i++) {
			if (((SeismicPanel) cv.getSeismics().get(i)).getName().equals(
					seismicName)) {
				seismicPanel = (SeismicPanel) cv.getSeismics().get(i);
				cv.getSeismics().remove(seismicPanel);
			}
		}

		cv.getPanelOrder().deletePanel(seismicPanel);

		track.getYAxisView().reCompute();

		changeTrackSize(track);

		cv.setFillRedrawFlag(true);
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer()
				.repaintAll();

		// view tree
		DefaultMutableTreeNode trackNode = DataBuffer.getJMainFrame()
				.getMainBuffer().getViewTree().findTrack(trackContainer.getParentInternalFrameView().getTitle(),wellName);
		DefaultMutableTreeNode seismicPanelNode = DataBuffer.getJMainFrame()
				.getMainBuffer().getViewTree().findPanelUnderTrack(seismicName,
						trackNode);
		if (seismicPanelNode == null) {
			return;
		}
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().removeCurve(
				seismicPanelNode);
	}


	@SuppressWarnings("unchecked")
	public static void addLithologyPanel() {
		AxisCurveContainerView selectedAxisCurveContainer = DataBuffer
				.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer();
		if(selectedAxisCurveContainer.getCurveView().getLithologys().size()>0){
			return;
		}
		String lithologyName = ConstantVariableList.LITHOLOGY_PANEL+ "";

		// get track name
		String wellName = selectedAxisCurveContainer.getWellName();

		// add group panel node to view tree
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.createLithologyPanel(wellName, lithologyName);

		// add group panel border to track view
		CurveView curveView = selectedAxisCurveContainer.getCurveView();
		LithologyPanel lithologyPanel = new LithologyPanel(curveView,
				lithologyName);
		curveView.getLithologys().add(lithologyPanel);
		curveView.getPanelOrder().addPanel(lithologyPanel);

		changeTrackSize(selectedAxisCurveContainer);
		selectedAxisCurveContainer.repaintAll();
	}

	public static void removeLithologyPanel(String wellName,
			String lithologyName) {
		// System.out.println("trackName:" + trackName + ",descriptionName:" +
		// descriptionName);
		TrackContainerView trackContainer = DataBuffer.getJMainFrame()
				.getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView track = null;
		for (int i = 0; i < trackNumber; i++) {
			track = (AxisCurveContainerView) tracks.get(i);
			if (track.getWellName().equals(wellName)) {
				break;
			}
			track = null;
		}
		if (track == null)
			return;
		CurveView cv = track.getCurveView();

		LithologyPanel lithologyPanel = null;
		for (int i = 0; i < cv.getLithologys().size(); i++) {
			if (((LithologyPanel) cv.getLithologys().get(i)).getName()
					.equals(lithologyName)) {
				lithologyPanel = (LithologyPanel) cv.getLithologys().get(i);
				cv.getLithologys().remove(lithologyPanel);
			}
		}

		cv.getPanelOrder().deletePanel(lithologyPanel);

		changeTrackSize(track);

		cv.setFillRedrawFlag(true);
		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
				.getTrackContainer().getSelectedAxisCurveContainer()
				.repaintAll();

		// view tree
		DefaultMutableTreeNode trackNode = DataBuffer.getJMainFrame()
				.getMainBuffer().getViewTree().findTrack(trackContainer.getParentInternalFrameView().getTitle(),wellName);
		DefaultMutableTreeNode lithologyNode = DataBuffer.getJMainFrame()
				.getMainBuffer().getViewTree().findPanelUnderTrack(
						lithologyName, trackNode);
		if (lithologyNode == null) {
			return;
		}
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree().removeCurve(
				lithologyNode);

	}


	public static double Logarithm(double value, double base) {
		return Math.log(value) / Math.log(base);
	}

	public static void setDepthScale(AxisCurveContainerView track) {
		//int verMax = track.getBottomScrollPane().getVerticalScrollBar().getMaximum();
		int verCurrent = track.getBottomScrollPane().getVerticalScrollBar().getValue();

		CurveView cv = track.getCurveView();
		int width = cv.getWidth();

		int trueHeight = 0;
		int pixValue = cv.getToolkit().getScreenResolution();
		float newHight = (cv.getParentAxisCurveContainer().getCurveLayer()
				.getYSetBottomDepth() - cv.getParentAxisCurveContainer()
				.getCurveLayer().getYSetTopDepth())
				/ track.getDepthScale();
		if (cv.getParentAxisCurveContainer().getDepthUnit() == CurveData.METER
				|| cv.getParentAxisCurveContainer().getDepthUnit() == CurveData.SECOND) {
			trueHeight = (int) (newHight * ConstantVariableList.METER_TO_INCH * pixValue);
		} else {
			trueHeight = (int) (newHight * ConstantVariableList.FOOT_TO_INCH * pixValue);
		}

		int trackHeight = trueHeight
				+ (int) (track.getBottomScrollPane().getBounds().getY());
		if (trackHeight > DataBuffer.getJMainFrame().getMainBuffer()
				.getInternalFrameView().getTrackContainer()
				.getAxisCurveContainerMaxHeight()) {
			trackHeight = (int) DataBuffer.getJMainFrame().getMainBuffer()
					.getInternalFrameView().getTrackContainer()
					.getAxisCurveContainerMaxHeight();
		}

		trackHeight = Math.abs(trackHeight);
		track.setPreferredSize(new Dimension(track.getWidth(), trackHeight));
		track.setSize(track.getWidth(), trackHeight);
		track.getJp().revalidate();
		track.getJp().repaint();

		cv.setPreferredSize(new Dimension(width, trueHeight));
		track.getYAxisView().setReComputeFlag(true);
		track.getCurveView().repaint();
		track.getBottomScrollPane().getVerticalScrollBar().setValue((int)(track.getDepthScale()*verCurrent));
	}

	public static void loadMarker(AxisCurveContainerView track){
		ProjectTreeView ltv = DataBuffer.getJMainFrame().getMainBuffer().getProjectTree();
        ArrayList markerList = DataBuffer.getJMainFrame().getMainBuffer().getMarkerList();
		for(int i=0 ; i<markerList.size() ; i++){
			MarkerInfo markerInfo = (MarkerInfo)markerList.get(i);
			// add marker to track
			if (track.getWellName().equals(markerInfo.getWellName())) {
				CurveMarker cm = null;
				for(int j = 0; j < track.getCurveView().getMarkerLayer().getMarkerList().size(); j++){
					if(((CurveMarker)track.getCurveView().getMarkerLayer().getMarkerList().get(j)).getMarkerName().equals(markerInfo.getMarkerName())){
						cm = (CurveMarker)track.getCurveView().getMarkerLayer().getMarkerList().get(j);
					}
				}
				if(cm == null){
					cm = new CurveMarker(markerInfo.getMarkerName(),
							Float.parseFloat(markerInfo.getDepthValue()+""), Color.BLUE);
					cm.setXLenth(track.getCurveView().getWidth());
					cm.setYHeight(track.getCurveView());
				}

				for (int j = 0; j < ltv.getWellTops().getChildCount(); j++) {
					DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) ltv.getWellTops().getChildAt(j);
					ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject) nodeChild.getUserObject();
					if(markerNode.getMarkerName().equals(cm.getMarkerName())){
						if(markerNode.isDraged()){
							track.getCurveView().getMarkerLayer().addMarkerToList(cm);
							cm.setVisible(true);
						}
					}
				}
	        }
		}
	}
	//Aug 23
	@SuppressWarnings("unchecked")
	public static void connectMarker(InternalFrameView ifv){
		if(ifv == null)
			return;
		ProjectTreeView ltv;
		TrackContainerView tcv;

		try{
			ltv = DataBuffer.getJMainFrame().getMainBuffer()
				.getProjectTree();
		    tcv = DataBuffer.getJMainFrame().getSonJSplitPane()
				.getRightDesktopPane().getSelectedInternalFrameView()
				.getTrackContainer();
		}catch(NullPointerException npe){
			return;
		}
		tcv.getMarkerCable1().clear(); //July 5th
		tcv.getMarkerCable2().clear();
		for (int i = 0; i < ltv.getWellTops().getChildCount(); i++) {
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode) ltv.getWellTops().getChildAt(i);
			ProjectTreeMarkerNodeObject markerNode = (ProjectTreeMarkerNodeObject) nodeChild.getUserObject();
			ArrayList wellNameList = markerNode.getWellNameList();
    		tcv.getMarkerBuffer().clear();
    		ArrayList list = ifv.getTrackContainer().getAxisCurveContainerList();
    		AxisCurveContainerView track = null;
    		int lastTrack=-1;

    		for (int z = 0; z < list.size(); z++) {
    			track = (AxisCurveContainerView) list.get(z);
track: 			for (int j = 0; j < wellNameList.size(); j++) {
    				if (wellNameList.get(j).equals(track.getWellName())) {
    					CurveMarker curveMarker = null;
						// find marker
						for (int ii = 0; ii < track.getCurveView().getMarkerLayer().getMarkerList().size(); ii++) {
							curveMarker = (CurveMarker) track.getCurveView().getMarkerLayer().getMarkerList().get(ii);
							if (curveMarker.getMarkerName().equals(markerNode.getMarkerName())) {
								curveMarker = (CurveMarker)track.getCurveView().getMarkerLayer().getMarkerList().get(ii);
								curveMarker.setLeftMarker(null);
								curveMarker.setRightMarker(null);

								// add to buffer
								MarkerConnectorPoint mc = new MarkerConnectorPoint();
								mc.setCm(curveMarker);
								mc.setAcc(track);
								tcv.getMarkerBuffer().add(mc);

								if(z-lastTrack > 1){
									tcv.getMarkerBuffer().clear();
									tcv.getMarkerBuffer().add(mc);
									lastTrack = z;
									break track;
								}
								lastTrack = z;
							}
						}

						if(tcv.getMarkerBuffer().size() == 2){
				    		// make connect
							MarkerConnectorPoint point1 = (MarkerConnectorPoint) tcv.getMarkerBuffer().get(0);
							MarkerConnectorPoint point2 = (MarkerConnectorPoint) tcv.getMarkerBuffer().get(1);

							point1.getCm().setRightMarker(point2.getCm());
							point2.getCm().setLeftMarker(point1.getCm());

							tcv.getMarkerCable1().add(point1);
							tcv.getMarkerCable2().add(point2);

							// clear
							tcv.getMarkerBuffer().remove(0);
						}
					}
				}
    		}
		}

		DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().repaint();
	}

	public static WellInfo getWellInfoByWellName(String sourceName){
		ArrayList wellInfoList = DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList();
		for(int i = 0 ; i < wellInfoList.size() ; i++){
			WellInfo wellInfo = (WellInfo)wellInfoList.get(i);
			if(wellInfo.getSourceName().equals(sourceName)){
				return wellInfo;
			}
		}
		return null;
	}

	public static WellInfo getWellInfoByWell(String wellName){
		ArrayList wellInfoList = DataBuffer.getJMainFrame().getMainBuffer().getWellInfoList();
		for(int i = 0 ; i < wellInfoList.size() ; i++){
			WellInfo wellInfo = (WellInfo)wellInfoList.get(i);
			if(wellInfo.getWellName().equals(wellName)){
				return wellInfo;
			}
		}
		return null;
	}

	public static void removeAllMarkerByName(String markerName){
		ArrayList ifvList = DataBuffer.getJMainFrame().getSonJSplitPane()
			.getRightDesktopPane().getInternalFrameViewList();
		for(int i =0 ; i < ifvList.size(); i++){
			ArrayList trackList = ((InternalFrameView)ifvList.get(i)).getTrackContainer().getAxisCurveContainerList();
			for(int j = 0; j < trackList.size(); j++){
				AxisCurveContainerView track = (AxisCurveContainerView)trackList.get(j);

				CurveMarker cm = null;
				ArrayList markerList = track.getCurveView().getMarkerLayer().getMarkerList();
				for(int k =0 ; k < markerList.size(); k++){
					if(((CurveMarker)markerList.get(k)).getMarkerName().equals(markerName)){
						cm = (CurveMarker)markerList.get(k);
						markerList.remove(k);
					}
				}

				ArrayList markerFillList = track.getCurveView().getMarkerLayer().getMarkerFillList();
				for(int m = 0 ; m < markerFillList.size() ; m++){
					if(((MarkerFill)markerFillList.get(m)).getCm1() == cm)
						markerFillList.remove(m);
					else if(((MarkerFill)markerFillList.get(m)).getCm2() == cm)
						markerFillList.remove(m);
				}

				markerFillList = track.getCurveView().getMarkerLayer().getMarkerLithologyList();
				for(int m = 0 ; m < markerFillList.size() ; m++){
					if(((MarkerLithology)markerFillList.get(m)).getCm1() == cm)
						markerFillList.remove(m);
					else if(((MarkerLithology)markerFillList.get(m)).getCm2() == cm)
						markerFillList.remove(m);
				}

				cm = null;
			}
			connectMarker((InternalFrameView)ifvList.get(i));
		}
	}

	/**
	 * added by Alex/ July 19, 2006
	 *
	 * a method to synchronize JComboBoxes of MainFrame
	 * @param iframe the selected Well Log window
	 */
	public static void synchroComboBoxes(InternalFrameView iframe)	{
		if (iframe != null)	{
			MainFrameView mfv = DataBuffer.getJMainFrame();
			if(iframe.getVerticalMeasurement().equals(ConstantVariableList.MEASUREMENT_MD)){
				mfv.getMeasurCBox().setSelectedIndex(0);
			}else if(iframe.getVerticalMeasurement().equals(ConstantVariableList.MEASUREMENT_TVD)){
				mfv.getMeasurCBox().setSelectedIndex(1);
			}else if(iframe.getVerticalMeasurement().equals(ConstantVariableList.MEASUREMENT_TVDSS)){
				mfv.getMeasurCBox().setSelectedIndex(2);
			}

			if(iframe.getDepthScale() == ConstantVariableList.DEPTHSCALE_50){
				mfv.getScaleCBox().setSelectedIndex(0);
			}else if(iframe.getDepthScale() == ConstantVariableList.DEPTHSCALE_200){
				mfv.getScaleCBox().setSelectedIndex(1);
			}else if(iframe.getDepthScale() == ConstantVariableList.DEPTHSCALE_500){
				mfv.getScaleCBox().setSelectedIndex(2);
			}else if(iframe.getDepthScale() == ConstantVariableList.DEPTHSCALE_1000){
				mfv.getScaleCBox().setSelectedIndex(3);
			}else if(iframe.getDepthScale() == ConstantVariableList.DEPTHSCALE_2000){
				mfv.getScaleCBox().setSelectedIndex(4);
			}else if(iframe.getDepthScale() == ConstantVariableList.DEPTHSCALE_5000){
				mfv.getScaleCBox().setSelectedIndex(5);
			}else if(iframe.getDepthScale() == ConstantVariableList.DEPTHSCALE_10000){
				mfv.getScaleCBox().setSelectedIndex(6);
			}
		}
	}

	/**
	 * added by Alex
	 * get Curve settings to save into Global settings
	 * @param cp CurvePanel
	 */
	@SuppressWarnings("unchecked")
	public static void addCurveSettings(CurvePanel cp)	{
		if (cp == null)
			return;

		String curveName = cp.getCd().getCurveName();
		Color curveColor = cp.getCurveColor();
		float curveStroke = cp.getStrokeWidth();
		boolean curveGridMode = cp.isLogarithm();
		float minValue = cp.getMinXDisplay();
		float maxValue = cp.getMaxXDisplay();
		ArrayList values = new ArrayList();
		values.add(curveColor);
		values.add(new Float(curveStroke));
		values.add(new Boolean(curveGridMode));
		values.add(new Float(minValue));
		values.add(new Float(maxValue));
		// add curve setting
		GlobeCurveSetting.addCurveSetting(curveName, values);
	}

	/**
	 * added by Alex
	 * apply the glbal settings to all curve on this frame view
	 */
	public static void applyGlobalSettings()	{
		TrackContainerView trackContainer = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();

		for (int i = 0; i < tracks.size(); i++)	{
			AxisCurveContainerView track = (AxisCurveContainerView) tracks.get(i);
			ArrayList curveList = track.getCurveLayer().getCurveList();

			for (int k = 0; k < curveList.size(); k++)	{
				CurvePanel cp = (CurvePanel) curveList.get(k);
				applyGlobalSettingsToCurve(cp);
			}
		}
	}

	/**
	 * added by Alex
	 * apply global settings to new curve, if this curve already exists
	 * @param cp CurvePanel to apply settings
	 */
	public static void applyGlobalSettingsToCurve(CurvePanel cp)	{
		String curveName = cp.getCd().getCurveName();

		if (GlobeCurveSetting.isEmpty())
			return;

		if (!GlobeCurveSetting.thisCurveExists(curveName))
			return;

		// apply global setings
		cp.setCurveColor(GlobeCurveSetting.getCurveColor(curveName));
		cp.setStrokeWidth(GlobeCurveSetting.getCurveStroke(curveName));
		cp.setLogarithm(GlobeCurveSetting.getCurveGridMode(curveName));
		cp.setMinXDisplay(GlobeCurveSetting.getCurveMinValue(curveName));
		cp.setMaxXDisplay(GlobeCurveSetting.getCurveMaxValue(curveName));
	}

	/**
	 * added by Alex
	 * when a curve is being dragged, copy its settings to dragged Curve
	 * @param cp Curve being dragged
	 */
	public static void setDraggedCurve(CurvePanel cp)	{
		DataBuffer.getJMainFrame().getMainBuffer().mbSetDraggedCurve(cp);
	}

	/**
	 * added by Alex
	 * clear curve previously dragged
	 */
	public static void clearDraggedCurve()	{
		DataBuffer.getJMainFrame().getMainBuffer().mbClearDraggedCurve();
	}

	/**
	 * added by Alex
	 * apply settings of previously dragged curve to this curve
	 * @param cp curve that will be modified
	 */
	private static void copyDraggedSettingsToCurve(CurvePanel cp)	{
		String curveName = cp.getCd().getCurveName();
		CurvePanel dCurve = DataBuffer.getJMainFrame().getMainBuffer().mbGetDraggedCurve();

		if (!dCurve.getCd().getCurveName().equals(curveName))
			return;

		cp.setCurveColor(dCurve.getCurveColor());
		cp.setStrokeWidth(dCurve.getStrokeWidth());
		cp.setLogarithm(dCurve.isLogarithm());
		cp.setMinXDisplay(dCurve.getMinXDisplay());
		cp.setMaxXDisplay(dCurve.getMaxXDisplay());
	}

	/**
	 * added by Alex
	 * check if given curve was dragged before
	 * @param curveName curve name
	 * @return true or false
	 */
	public static boolean isDraggedCurveExists(String curveName)	{
		CurvePanel dCurve = DataBuffer.getJMainFrame().getMainBuffer().mbGetDraggedCurve();
		if (dCurve != null)
			if (dCurve.getCd().getCurveName().equals(curveName))
				return true;

		return false;
	}
	
	/**
	 * get curvePanel by a curvehashkey
	 * @param curveHashkey
	 * @return curvepanel
	 * @author Alex
	 */
	public static CurvePanel getCurvePanel(String curveHashkey)	{
		String well = curveHashkey.split(OperationClass.hashMapNameSeparator)[0];
		String curve = curveHashkey.split(OperationClass.hashMapNameSeparator)[1];
		
		return getCurvePanelByName(curve, null, well);
	}
		
	/**
	 * added by Alex
	 * get CurvePanel using its name, group or wellname
	 * @param curveName curve name
	 * @param groupName group containing this curve, this parameter may be null if curve is not under group
	 * @param wellName welltrack name
	 * @return CurvePanel
	 */
	public static CurvePanel getCurvePanelByName(String curveName, String groupName, String wellName)	{
		CurvePanel cp = null;

		TrackContainerView trackContainer = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();

		int trackNumber = tracks.size();
		AxisCurveContainerView currentTrack = null;

		for (int i = 0; i < trackNumber; i++) {
			currentTrack = (AxisCurveContainerView) tracks.get(i);
			if (currentTrack.getWellName().equals(wellName))
				break;

		}

		if (currentTrack == null)
			return null;

		CurveView cv = currentTrack.getCurveView();
		if (cv==null)
			return null;

		if (groupName != null)	{
			CurveGroup group = null;

			for (int i = 0; i < cv.getCurveGroups().size(); i++) {
				group = (CurveGroup) cv.getCurveGroups().get(i);
				if (group.getName().equals(groupName))	{
					cp = getCurveByName(group.getCurveList(), curveName);
					break;
				}
			}
		}
		else
			cp = getCurveByName(cv.getCurveLayer().getCurveList(), curveName);

		return cp;

	}

	/**
	 * added by Alex
	 * search and get a curvepanel in given curve list
	 * @param curveList curve list to looking for into
	 * @param curveName name of curve to search
	 * @return a curve panel
	 */
	private static CurvePanel getCurveByName(ArrayList curveList, String curveName)	{
		CurvePanel cp = null;

		for (int i = 0; i < curveList.size(); i++)	{
			cp = (CurvePanel) curveList.get(i);
			if (cp.getCd().getCurveName().equals(curveName))
				return cp;
		}

		return null;
	}

	/**
	 * Invoked by JComboBox of Measurement/Scale
	 * @param index
	 * @param selectedInternalFrameView
	 */
	public static void updateMeasurementScale(int index,
			InternalFrameView selectedInternalFrameView){
		if(index == 0)
			selectedInternalFrameView.setDepthScale(ConstantVariableList.DEPTHSCALE_50);
		else if(index == 1)
			selectedInternalFrameView.setDepthScale(ConstantVariableList.DEPTHSCALE_200);
		else if(index == 2)
			selectedInternalFrameView.setDepthScale(ConstantVariableList.DEPTHSCALE_500);
		else if(index == 3)
			selectedInternalFrameView.setDepthScale(ConstantVariableList.DEPTHSCALE_1000);
		else if(index == 4)
			selectedInternalFrameView.setDepthScale(ConstantVariableList.DEPTHSCALE_2000);
		else if(index == 5)
			selectedInternalFrameView.setDepthScale(ConstantVariableList.DEPTHSCALE_5000);
		else if(index == 6)
			selectedInternalFrameView.setDepthScale(ConstantVariableList.DEPTHSCALE_10000);


		ArrayList trackList = selectedInternalFrameView.getTrackContainer().getAxisCurveContainerList();
		float minDepth=0, maxDepth=0;
		for(int i = 0 ; i < trackList.size() ; i++){
			AxisCurveContainerView track = (AxisCurveContainerView) trackList.get(i);
			track.setDepthScale(selectedInternalFrameView.getDepthScale());

			if (i == 0){
				minDepth=track.getCurveView().getCurveLayer().getYSetTopDepth();
				maxDepth=track.getCurveView().getCurveLayer().getYSetBottomDepth();
			}else{
				minDepth=Math.min(minDepth, track.getCurveView().getCurveLayer().getYSetTopDepth());
				maxDepth=Math.max(maxDepth, track.getCurveView().getCurveLayer().getYSetBottomDepth());
			}
		}
		//apply range for all views
		for(int i = 0 ; i < trackList.size() ; i++){
			AxisCurveContainerView track = (AxisCurveContainerView) trackList.get(i);
			track.getCurveView().getCurveLayer()
				.setYSetTopDepth(minDepth);
			track.getCurveView().getCurveLayer()
				.setYSetBottomDepth(maxDepth);
			track.getCurveView().getCurveLayer().enableCustomizedRange(true);
			ViewCoreOperator.setDepthScale(track);
		}
		// added by Alex. July 19, 2006
		ViewCoreOperator.synchroComboBoxes(selectedInternalFrameView);
	}

	/**
	 * Invoked by JComboBox of Measurement/Scale
	 * @param index
	 * @param selectedInternalFrameView
	 */
	public static void updateMeasurementType(int index,
			InternalFrameView selectedInternalFrameView){

		if(index != 0){
			if(DataBuffer.getJMainFrame().getMainBuffer().getMDDepthConverter() == null){
				boolean isSelectSurveyFile = false;
				SetDepthTypeDialog setDepthTypeDialog = new SetDepthTypeDialog();
				setDepthTypeDialog.setVisible(true);
				isSelectSurveyFile = setDepthTypeDialog.isSelectSurveyFile();
				if(!isSelectSurveyFile){
					DataBuffer.getJMainFrame().getMeasurCBox().setSelectedIndex(0);
					return;
				}
			}
		}

		if(index == 0)
			selectedInternalFrameView.setVerticalMeasurement(ConstantVariableList.MEASUREMENT_MD);
		else if(index == 1)
			selectedInternalFrameView.setVerticalMeasurement(ConstantVariableList.MEASUREMENT_TVD);
		else if(index == 2)
			selectedInternalFrameView.setVerticalMeasurement(ConstantVariableList.MEASUREMENT_TVDSS);
		selectedInternalFrameView.setTrackType();
	}

	@SuppressWarnings("unchecked")
	public static void addMarkerInfo(MarkerInfo info){
		ArrayList mList = DataBuffer.getJMainFrame().getMainBuffer().getMarkerList();
		for(int i= 0; i < mList.size(); i++){
			MarkerInfo markerInfo =(MarkerInfo)mList.get(i);
			if(markerInfo.getWellName().equals(info.getWellName()) && markerInfo.getMarkerName().equals(info.getMarkerName())){
				markerInfo.setDepthValue(info.getDepthValue());
				return;
			}
		}
		mList.add(info);
	}

	public static void renameMarkerName(String oldName, String newName){
		ArrayList windowList = DataBuffer.getJMainFrame().getSonJSplitPane().getRightDesktopPane().getInternalFrameViewList();
		for(int i= 0; i < windowList.size(); i++){
			ArrayList trackList = ((InternalFrameView)windowList.get(i)).getTrackContainer().getAxisCurveContainerList();
			for (int j = 0; j < trackList.size(); j++) {
				ArrayList markerList = ((AxisCurveContainerView) trackList.get(j)).getMarkerLayer().getMarkerList();
				for(int z=0 ; z<markerList.size() ; z++){
					if (((CurveMarker)markerList.get(z)).getMarkerName().equals(oldName)) {
						((CurveMarker)markerList.get(z)).setMarkerName(newName);
					}
				}
			}
			((InternalFrameView)windowList.get(i)).repaint();
		}
	}

	/**
	 * added by Alex
	 * build a well since a template if it does exist
	 * @param wellLog wellLog window title
	 * @param wellName well name
	 */
	public static void buidWellFromTemplate(String wellLog, String wellName)	{
		String template = DataBuffer.getJMainFrame().getMainBuffer().getDefaultTemplate();
		ArrayList items = SectionTemplate.getWellTemplate(template);

		if (items == null)
			return;

		for (int i = 0; i < items.size(); i++)	{
			Object obj = items.get(i);
			
			if (obj instanceof CurveItem)	{
				CurveItem curve = (CurveItem) obj;
				CurveData cd = (CurveData)DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(
								wellName + OperationClass.hashMapNameSeparator + curve.getCurveName());

				// check if curve exists in this well
				if (cd != null)	{
					// add curve to curve view
					tryPutCurveToTrack(cd.getHashCurveName(), wellName);
					CurvePanel cp = ViewCoreOperator.getCurvePanelByName(cd.getCurveName(), null, wellName);
					//DataBuffer.getJMainFrame().getMainBuffer().getProjectTree().getCurveNameNode(wellName, cd.getCurveName());
					applyTemplateSettingsToCurve(cp, curve);
				}
			} 
			else if (obj instanceof GroupItem)	{
				GroupItem group = (GroupItem) obj;
				buildGroupFromTemplate(wellLog, wellName, group.getGroupName(), group);
			}
		}
		
		buildCurveFillFromTemplate(items, wellName);
	}
	
	private static CurveData getCurveData(String curveName, String wellName)	{
		return (CurveData)DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(
				wellName + OperationClass.hashMapNameSeparator + curveName);
	}

	/**
	 * build a CurveFillBeside or CompoundFill from template
	 * 
	 * @param items curve fill objects
	 * @param wellName well name
	 * @author Alex
	 */
	private static void buildCurveFillFromTemplate(ArrayList items, String wellName) {
		for (int i = 0; i < items.size(); i++)	{
			Object obj = items.get(i);
			
			if (obj instanceof CurveFillItem)	{
				CurveFillItem curveFill = (CurveFillItem) obj;
				
				if (getCurveData(curveFill.getCurve(), wellName) != null)
					createCurveFill(curveFill, wellName);
			}
		}
		
	}
	
	/**
	 * used for create a fill and set its properties
	 * 
	 * @param curveFill
	 * @param wellName
	 * @author Alex
	 */
	private static void createCurveFill(CurveFillItem curveFill, String wellName)	{
		CurveView cv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
					.getTrackContainer().getSelectedAxisCurveContainer().getCurveView();
		
		if (curveFill.isSingleFill())	{
			if (curveFill.isIntoGroup())	{
				CurvePanel cp = ViewCoreOperator.getCurvePanelByName(curveFill.getCurve().split(OperationClass.hashMapNameSeparator)[0],
																curveFill.getGroup(), wellName);
				CurveFillBeside cfb = ViewCoreOperator.getCurveFillBeside(cp);
				
				if (cfb == null)	{
					cfb = new CurveFillBeside();
					cfb.setCp(cp);
				}
				
				cfb.setLeftOrRight(curveFill.getOrientation());
				cfb.constructFill(cv, ViewCoreOperator.getCurveGroup(curveFill.getGroup(), wellName));
				cfb.setFillIntoGroup(true);
				cfb.setFillColor(new Color(curveFill.getFillColor()));
				cv.getCurveLayer().addFillBesideToList(cfb);
			}
			else	{
				CurvePanel cp = ViewCoreOperator.getCurvePanelByName(curveFill.getCurve(), curveFill.getGroup(), wellName);
					//ViewCoreOperator.getCurvePanel(curveFill.getCurve());
				CurveFillBeside cfb = ViewCoreOperator.getCurveFillBeside(cp);
				cfb.setLeftOrRight(curveFill.getOrientation());
				cfb.setFillColor(new Color(curveFill.getFillColor()));
			}
		}
		else	{
			CurvePanel leftCp = ViewCoreOperator.getCurvePanelByName(curveFill.getCurve(), curveFill.getGroup(), wellName);
			CurvePanel rightCp = ViewCoreOperator.getCurvePanelByName(curveFill.getSecondCurve(), curveFill.getGroup(), wellName);
			
			if (leftCp == null || rightCp == null)
				return;
			
			CompoundFill cf = new CompoundFill();
			cf.constructFill(leftCp, rightCp);
			cf.setFillColor(new Color(curveFill.getFillColor()));
			
			if (cf.getAllFills().size() > 0)
				cv.getCurveLayer().addCompoundFillToList(cf);
		}
		
		// repaint CurveView
		cv.setFillRedrawFlag(true);
		cv.repaintFather();
	}

	/**
	 * added by Alex
	 * get a curvepanel by name and set it with default template configuration
	 * @param cp Curve panel
	 * @param curve Curve item
	 */
	private static void applyTemplateSettingsToCurve(CurvePanel cp, CurveItem curve)	{
		cp.setCurveColor(new Color(curve.getColor()));
		cp.setMinXDisplay(curve.getCurveMinValue());
		cp.setMaxXDisplay(curve.getCurveMaxValue());
		cp.setStokeType(curve.getCurveStrokeType());
		cp.setStrokeWidth(curve.getCurveStrokeWidth());
		cp.setOrderDesc(curve.isReverse());
	}

	/**
	 * added by Alex
	 * build a group panel since group item
	 * @param wellLog well log title
	 * @param wellName well name
	 * @param groupName group name
	 * @param groupItem group item with information
	 */
	@SuppressWarnings("unchecked")
	private static void buildGroupFromTemplate(String wellLog, String wellName, String groupName, GroupItem groupItem)	{
		InternalFrameView ifv = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		AxisCurveContainerView selectedAxisCurveContainer = ifv.getTrackContainer().getSelectedAxisCurveContainer();
		CurveView curveView = selectedAxisCurveContainer.getCurveView();

		// add group panel node to view tree
		DataBuffer.getJMainFrame().getMainBuffer().getViewTree()
				.createGroupPanel(wellLog, wellName, groupName, false);
		
		String minX = groupItem.getMinXDisplay();		//	added dec.12
		String maxX = groupItem.getMaxXDisplay();		//	added dec.12

		// add group panel border to track view
		CurveGroup curveGroup = new CurveGroup(groupName);
		curveGroup.setLogarithmic(groupItem.isLogarithmic());
		
		if (groupItem.isDisplayRange())	{
			curveGroup.setSetRange(groupItem.isDisplayRange());		//	added may.31.07
			curveGroup.setMinXDisplay(minX);						//	added dec.12
			curveGroup.setMaxXDisplay(maxX);						//	added dec.12
		}	

		curveView.getCurveGroups().add(curveGroup);
		curveView.getPanelOrder().addPanel(curveGroup);

		if (groupItem.getSize()>0)	{
			for (int i = 0; i < groupItem.getSize(); i++)	{
				CurveItem curve = groupItem.getCurveItems().get(i);
				CurveData cd = (CurveData)DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(
						wellName + OperationClass.hashMapNameSeparator + curve.getCurveName());

				if (cd != null)	{
					CurvePanel cp = new CurvePanel(cd);
					
					//cp.setOrderDesc(curve.isReverse());
					cp.setTranslatedCurve(curveView, curveView.getCurveLayer().getBesideTypeWidth(), 1,1);
					applyTemplateSettingsToCurve(cp, curve);
					//cp.setCurveColor(new Color(curve.getColor()));					
					
					if (curveGroup.isSetRange())	{
						cp.setMinXDisplay(Float.parseFloat(minX));		//	added may.31.07
						cp.setMaxXDisplay(Float.parseFloat(maxX));		//	added may.31.07
					}
					/*else	{
						cp.setMinXDisplay(curve.getCurveMinValue());
						cp.setMaxXDisplay(curve.getCurveMaxValue());
					}*/					
					
					//added dec.12
					if (groupItem.isLogarithmic())
						cp.setLogarithm(groupItem.isLogarithmic());
					
					cp.setCg(curveGroup);

					// ViewTree
					DataBuffer.getJMainFrame().getMainBuffer().getViewTree().createCurveUnderGroupPanel(
							wellLog, wellName, groupName, cd.getHashCurveName(), cd.getCurveName());

					// frame view
					putCurveToGroupPanel(curveView, cp, curveGroup);					
				}
			}
			changeTrackSize(selectedAxisCurveContainer);
			selectedAxisCurveContainer.repaintAll();
		}
	}

	/**
	 * added by Alex
	 * load well template from Xml file
	 * @param filename selected file
	 * @return template's name
	 */
	public static String loadWellTemplate(String filename)	{
		return SectionTemplate.loadXml(filename);
	}

	/**
	 * added by Alex
	 * save current well display settings as a template
	 * @param wellLog well log name
	 * @param trackName well name
	 */
	public static void saveWellTemplate(String wellLog, String trackName)	{
		Boolean saveFile = false;

		DefaultMutableTreeNode trackNode = DataBuffer.getJMainFrame().getMainBuffer().getViewTree().getWellNodeByName(wellLog, trackName);

		if (trackNode == null)
			return;

		int response = JOptionPane.showConfirmDialog(null, "Would you like to save this well template into file, too?");

		if (response==JOptionPane.OK_OPTION)
			saveFile = true;

		ViewCoreOperator.saveWellTemplate(trackNode, saveFile);
	}

	/**
	 * added by Alex
	 * save current well template getting its nodes since trackNode
	 * @param trackNode well node
	 * @param fileOption set if tis template must be saved into file
	 */
	@SuppressWarnings("unchecked")
	public static void saveWellTemplate(DefaultMutableTreeNode trackNode, Boolean fileOption) {
		int retVal = -1;
		File file = null;

		ArrayList wellStructure = new ArrayList();
		CurveItem curveItem = null;
		String trackName = trackNode.getUserObject().toString();
		String keyName = trackName;

		if (trackNode.getChildCount() < 1)
			return;

		for (int i = 0; i < trackNode.getChildCount(); i++)	{
			DefaultMutableTreeNode childObj = (DefaultMutableTreeNode) trackNode.getChildAt(i);
			ViewTreeNode childNode = (ViewTreeNode) childObj.getUserObject();

			if (childNode.getNodeType() == ViewTreeNode.CURVE_NODE)	{
				CurvePanel curve = getCurvePanelByName(childNode.getCurveName(), null, trackName);
				curveItem = new CurveItem(curve);

				wellStructure.add(curveItem);

			} else if (childNode.getNodeType() == ViewTreeNode.GROUP_PANEL_NODE)	{
				int childCount = childObj.getChildCount();
				CurveGroup cg = getCurveGroup(childNode.getCurveName(), trackName);
				GroupItem groupItem = new GroupItem(childNode.getCurveName());
				groupItem.setGridMode(cg.isLogarithmic());
				groupItem.setRangeValues(cg.getMinXDisplay(), cg.getMaxXDisplay());
				groupItem.setDisplayRange(cg.isSetRange());

				if (childCount > 0)	{
					for (int j = 0; j < childCount; j++)	{
						ViewTreeNode curveNode = (ViewTreeNode)((DefaultMutableTreeNode) childObj.getChildAt(j)).getUserObject();
						CurvePanel curve = getCurvePanelByName(curveNode.getCurveName(), childNode.getCurveName(), trackName);
						curveItem = new CurveItem(curve);
						
						//added dec.12
						if (cg.isLogarithmic())
							curveItem.setGridMode(cg.isLogarithmic());
						
						groupItem.addCurve(curveItem);
					}
				}

				wellStructure.add(groupItem);
			}
		}
		
		// for fill areas
		ArrayList curveFillBesideList = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
					.getTrackContainer().getSelectedAxisCurveContainer().getCurveView().getCurveLayer().getCurveFillBesideList();
		
		for (int i = 0; i < curveFillBesideList.size(); i++)	{
			CurveFillBeside cfb = (CurveFillBeside) curveFillBesideList.get(i);
			CurveFillItem cfbItem = new CurveFillItem(cfb);
			
			wellStructure.add(cfbItem);
		}
		
		ArrayList compoundFillList = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
						.getTrackContainer().getSelectedAxisCurveContainer().getCurveView().getCurveLayer().getCompoundFillList();
		
		for (int i = 0; i < compoundFillList.size(); i++)	{
			CompoundFill cf = (CompoundFill) compoundFillList.get(i);
			CurveFillItem cfItem = new CurveFillItem(cf);
			
			wellStructure.add(cfItem);
		}
		

		if (fileOption)	{
			JFileChooser fc = new JFileChooser();
			fc.setSelectedFile(new File("*.xtf"));
			fc.setFileFilter(new XmlFileFilter("xtf"));
			retVal = fc.showSaveDialog(null);

			if (retVal == JFileChooser.APPROVE_OPTION)	{
				file = fc.getSelectedFile();
				String filename = file.getPath();					
				
				if (!filename.endsWith(".xtf"))	{
					file.renameTo(new File(file.getPath().concat(".xtf")));
					filename = file.getPath();
					//filename.concat(".xtf");
				}
				
				keyName = filename.substring(filename.lastIndexOf("\\")+1, filename.indexOf("."));
				System.out.println("Key: "+keyName);
			}
		}

		// add this template
		SectionTemplate.addWellTemplate(keyName, wellStructure);
		DataBuffer.getJMainFrame().getMainBuffer().setDefaultTemplate(keyName);

		if (fileOption && (retVal == JFileChooser.APPROVE_OPTION))	{
			SectionTemplate.saveContentIntoFile(keyName, file.getPath());
		}
	}
	
	/**
	 * get CurveGroup by its groupHashkey
	 * @param groupHashkey
	 * @return CurveGroup
	 * @author Alex
	 */
	public static CurveGroup getCurveGroup(String groupHashkey)	{
		String well = groupHashkey.split(OperationClass.hashMapNameSeparator)[0];
		String group = groupHashkey.split(OperationClass.hashMapNameSeparator)[1];
		
		return getCurveGroup(group, well);
	}

	/**
	 * added by Alex
	 * get CurveGroup by its name, looking into given well
	 * @param groupName
	 * @param curveName
	 * @return curve group
	 */
	public static CurveGroup getCurveGroup(String groupName, String wellName)	{
		AxisCurveContainerView selectedAxisCurveContainerView = DataBuffer
		.getJMainFrame().getMainBuffer().getInternalFrameView()
		.getTrackContainer().getSelectedAxisCurveContainer();

		if (!selectedAxisCurveContainerView.getWellName().equals(wellName))	{
			return null;
		}

		CurveView cv = selectedAxisCurveContainerView.getCurveView();

		if (cv == null)	{
			return null;
		}
		else if (cv.getCurveGroups().size() < 1)	{
			return null;
		}

		CurveGroup cg = null;
		for (int i = 0; i < cv.getCurveGroups().size(); i++) {
			if (((CurveGroup) cv.getCurveGroups().get(i)).getName().equals(groupName)) {
				cg = (CurveGroup) cv.getCurveGroups().get(i);
				break;
			}
		}
		return cg;
	}
	
	/**
	 * look for curves in this group
	 * @param groupHashkey
	 * @return true or false
	 * @author Alex
	 */
	public static boolean thisGroupHasCurves(String groupHashkey)	{		
		CurveGroup cg = getCurveGroup(groupHashkey);
		
		if (cg == null || cg.getCurveList().size() <= 0)
			return false;
		
		return true;
	}
	
	/**
	 * get curvehashkey from this group
	 * @param groupHashkey
	 * @return arraylist of curves
	 * @author Alex
	 */
	public static ArrayList<String> getCurveHashkeysFromThisGroup(String groupHashkey)	{
		if (!thisGroupHasCurves(groupHashkey))
			return null;
		
		ArrayList<String> curves = new ArrayList<String>();
		CurveGroup cg = getCurveGroup(groupHashkey);
		
		for (int i = 0; i < cg.getCurveList().size(); i++)	{
			String curveName = ((CurvePanel)cg.getCurveList().get(i)).getCd().getHashCurveName();
			curves.add(curveName);
		}
		
		return curves;
	}
	
	/**
	 * get a CurveFillBeside object given a CurvePanel
	 * @param cp CurvePanel
	 * @return CurveillBeside object
	 * 
	 * @author Alex
	 */
	public static CurveFillBeside getCurveFillBeside(CurvePanel cp)	{
		CurveLayer cl = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView()
					.getTrackContainer().getSelectedAxisCurveContainer().getCurveView().getCurveLayer();		
		
		for(int i=0 ; i < cl.getCurveFillBesideList().size() ; i++){
			if(((CurveFillBeside) cl.getCurveFillBesideList().get(i))
					.getCp().getCd().getHashCurveName().equals(cp.getCd().getHashCurveName()))
				return (CurveFillBeside) cl.getCurveFillBesideList().get(i);
		}
		
		return null;
	}

}

