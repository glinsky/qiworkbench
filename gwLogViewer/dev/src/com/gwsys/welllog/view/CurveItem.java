/**
 * 
 */
package com.gwsys.welllog.view;

import com.gwsys.welllog.view.curve.CurvePanel;

/**
 * @author Alex
 *
 */
class CurveItem	{
	private String curveName;
	private int curveColor;
	private float curveStrokeWidth;
	private int curveStrokeType;
	private boolean gridMode;
	private boolean reverse;
	private float curveMinValue;
	private float curveMaxValue;
	
	public CurveItem(String name)	{
		this.curveName = name;
	}
	
	public CurveItem(CurvePanel cp)	{
		this.curveName = cp.getCd().getCurveName();
		this.setColor(cp.getCurveColor().getRGB());
		this.setCurveStrokeWidth(cp.getStrokeWidth());
		this.setCurveStrokeType(cp.getStokeType());
		this.setGridMode(cp.isLogarithm());
		this.setMinValue(cp.getMinXDisplay());
		this.setMaxValue(cp.getMaxXDisplay());
		this.setReverse(cp.isOrderDesc());
	}
	
	public String getCurveName()	{
		return this.curveName;
	}
		
	public int getColor()	{
		return this.curveColor;
	}
	
	public void setColor(int color)	{
		this.curveColor = color;
	}
	
	public float getCurveStrokeWidth()	{
		return this.curveStrokeWidth;
	}
	
	public void setCurveStrokeWidth(float stroke)	{
		this.curveStrokeWidth = stroke;
	}
	
	public int getCurveStrokeType()	{
		return this.curveStrokeType;
	}
	
	public void setCurveStrokeType(int stroke)	{
		this.curveStrokeType = stroke;
	}
	
	public boolean isLogarithmic()	{
		return this.gridMode;
	}
	
	public void setGridMode(boolean gridMode)	{
		this.gridMode = gridMode;
	}
	
	public float getCurveMinValue()	{
		return this.curveMinValue;
	}
	
	public void setMinValue(float minValue)	{
		this.curveMinValue = minValue;
	}
	public float getCurveMaxValue()	{
		return this.curveMaxValue;
	}
	
	public void setMaxValue(float maxValue)	{
		this.curveMaxValue = maxValue;
	}
	
	public boolean isReverse()	{
		return this.reverse;
	}
	
	public void setReverse(boolean value)	{
		this.reverse = value;
	}
}