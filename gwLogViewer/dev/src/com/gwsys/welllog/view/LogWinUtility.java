/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.view;

import java.awt.Dimension;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.gwsys.data.seismic.su.SuParse;
import com.gwsys.data.well.AscParse;
import com.gwsys.data.well.LogData;
import com.gwsys.data.well.las.LasParse;
import com.gwsys.data.well.xml.XmlParse;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.core.curve.TrackTypeList;
import com.gwsys.welllog.view.curve.CurveData;
import com.gwsys.welllog.view.curve.OperationClass;
import com.gwsys.welllog.view.track.AxisCurveContainerView;

public class LogWinUtility {
	public static final String hashMapNameSeparator = OperationClass.hashMapNameSeparator;
	/**
	 * @author luming from view.curve.OperationClass.java
	 * @param ld
	 *            lasData
	 * @param hashCurveName
	 *            hashkey
	 * @param i
	 *            which curve
	 * @return CurveData object
	 */
	public static CurveData createCurveData(LogData logdata, String hashCurveName) {
		CurveData cd = new CurveData();
		cd.setOriginalYType(logdata.getCoordinateType());
		cd.setHashCurveName(hashCurveName);
		cd.setCurveName(hashCurveName);
		// have to transform
		float[] fx = logdata.getCurveValueByName(hashCurveName);
		float[] fy = logdata.getCurveValueByName(logdata.getAllCurveNames()[0]);
		cd.setDepthStep(((BigDecimal) logdata.getStep()).floatValue()); 

		cd.setX(fx);
		cd.setY(fy);
		
		return cd; 
	}
	
	
	public static float getMaxNum(float[] array) {
		float maxNum = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maxNum) {
				maxNum = array[i];
			}
		}
		return maxNum;
	}

	public static float getMinNum(float[] array) {
		float minNum = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] < minNum) {
				minNum = array[i];
			}
		}
		return minNum;
	}
	/**
	 * create a track with default properties
	 * 
	 * @param trackName
	 */
	@SuppressWarnings("unchecked")
	public static void createTrack(String trackName) {

		if (DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView() == null) {
			return;
		}
		int curveViewDefaultWidth = ConstantVariableList.CURVEVIEW_DEFAULT_WIDTH;
		int curveViewDefaultHeight = ConstantVariableList.CURVEVIEW_DEFAULT_HEIGHT;
		int axisCurveContainerDefaultWidth = ConstantVariableList.AXISCURVECONTAINER_DEFAULT_WIDTH;
		int axisCurveContainerDefaultHeight = ConstantVariableList.AXISCURVECONTAINER_DEFAULT_HEIGHT;
		int axisCurveContainerHeight = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().getAxisCurveContainerMaxHeight();
		if (axisCurveContainerHeight < axisCurveContainerDefaultHeight) {
			axisCurveContainerHeight = axisCurveContainerDefaultHeight;
			//DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().setAxisCurveContainerMaxHeight(axisCurveContainerHeight);
		}
		
		InternalFrameView selectedInternalFrameView = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		ArrayList axisCurveContainerList = selectedInternalFrameView.getTrackContainer().getAxisCurveContainerList();

		// bounxX represent the max x axis position of current internal frame
		// view's tracks
		int boundX = 0; // the position in x axis of the AxisCurveContainerView
		for (int i = 0; i < axisCurveContainerList.size(); i++) {
			int oldWidth = ((AxisCurveContainerView) axisCurveContainerList.get(i)).getWidth(); // the
			// width
			// of
			// current
			boundX = boundX + oldWidth + ConstantVariableList.AXISCURVECONTAINER_GAP;
		}
		// create a container and initialize its properties
		AxisCurveContainerView newAxisCurveContainer = new AxisCurveContainerView(selectedInternalFrameView.getTrackContainer());
		newAxisCurveContainer.setTrackName(trackName);
		newAxisCurveContainer.getCurveLayer().setLayerType(TrackTypeList.REALIZATION_BESIDE);
		newAxisCurveContainer.setBounds(boundX, 0, axisCurveContainerDefaultWidth, axisCurveContainerHeight);
		newAxisCurveContainer.getCurveView().setPreferredSize(new Dimension(curveViewDefaultWidth, curveViewDefaultHeight));
		// add the new container to our containerList
		axisCurveContainerList.add(newAxisCurveContainer);
		// add the container component to internal frame view
		// and set internal frame view's proper size
		selectedInternalFrameView.getTrackContainer().add(newAxisCurveContainer);
		selectedInternalFrameView.getTrackContainer().setPreferredSize(
				new Dimension(boundX + 400 + 100, axisCurveContainerHeight + 100));
		// refresh
		selectedInternalFrameView.revalidate();
		selectedInternalFrameView.repaint();
	}
	/**
 	 * @author gaofei
 	 * From mainframe.MainFrameView
 	 */
	public static LogData openLogFile(File file) {
		LogData logData = null;
		if(file.getName().toLowerCase().endsWith("las")){	        
	        LasParse lasParse = new LasParse();
	        try {
	            logData = lasParse.parse(file);
	        } catch (Exception ex) {
	            System.out.println("LayeredView.openFile error :" + ex);
	        }
	   	}else if(file.getName().toLowerCase().endsWith("xml")){	        
	        try {
	        	XmlParse xmlParse = new XmlParse();
	            logData = xmlParse.parse(file);
	        } catch (Exception ex) {
	            System.out.println("LayeredView.openFile error :" + ex);
	        }
	       
	   	}
	   	else if(file.getName().toLowerCase().endsWith("dat")){
	   		try {
	        	AscParse ascParse = new AscParse();
	            logData = ascParse.parse(file);
	        } catch (Exception ex) {
	            System.out.println("LayeredView.openFile error :" + ex);
	        }
	   	}
	   	else if(file.getName().toLowerCase().endsWith("su")){
	   		try {
	        	SuParse suParse = new SuParse();
	            logData = suParse.parse(file);
	        } catch (Exception ex) {
	            System.out.println("LayeredView.openFile error :" + ex);
	        }
	   	}
		 return logData;
	}
	
	
	/*
	public static void putCurveToTrack(String curveHashmapKey, String targetTrackName) {
		
		CurveData cd = (CurveData) DataBuffer.getJMainFrame().getMainBuffer().getCurveHashMap().get(curveHashmapKey);
		TrackContainerView trackContainer = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer();
		ArrayList tracks = trackContainer.getAxisCurveContainerList();
		int trackNumber = tracks.size();
		AxisCurveContainerView currentTrack = null;
		for (int i = 0; i < trackNumber; i++) {
			currentTrack = (AxisCurveContainerView) tracks.get(i);
			if (currentTrack.getTrackName().equals(targetTrackName)) {
				break;
			}
			currentTrack = null;
		}
		if (currentTrack == null)
			return;
		CurveView cv = currentTrack.getCurveView();
		CurveLayer cl = cv.getCurveLayer();
		CurvePanel cp = new CurvePanel(cd);
		boolean fillDrawFlag = cl.isHaveThisCurveInList(cp);// New BY Lili
		cl.addCurveToList(cp);
		cv.setCurveRedrawFlag(true);
		cv.setMarkerRedrawFlag(true);
		cl.deleteMeanP105090CurveFromList();
		cl.setP105090meanRefresh(true);
		if (trackContainer.getSelectedAxisCurveContainer() != null) {
			trackContainer.getSelectedAxisCurveContainer().setNotSelected();
		}
		trackContainer.setSelectedAxisCurveContainer(cv.getParentAxisCurveContainer());
		currentTrack.setSelected();
		currentTrack.getYAxisView().setYType(currentTrack.getYAxisView().getYType());

		cv.setProperSize();

		// new by LiLi
		if (!fillDrawFlag) {
			cp.setTranslatedCurve(cv.getWidth(), cv.getHeight(), cl.getBesideTypeWidth(), 0, cv, cl.getYTopDepth());
			CurveFillBeside cfb = new CurveFillBeside();
			cfb.setCp(cp);
			cfb.setLeftOrRight(ConstantVariableList.FILL_LEFT);
			cfb.constructFill(cv);
			cfb.setFillColor(Color.PINK);
			cv.getCurveLayer().addFillBesideToList(cfb);
			cv.setFillRedrawFlag(true);
		}

	}
	*/
	
	private static int countLog = 0;
	public static String generateNewLogName() {
		return "Log " + ++countLog;
	}
	
	private static int countWin = 0;
	public static String generateNewFrameTitle() {
		return "Section " + ++countWin;
	}
}

