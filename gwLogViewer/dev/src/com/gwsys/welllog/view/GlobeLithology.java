/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import com.gwsys.data.util.SpringUtilities;
import com.gwsys.welllog.view.zone.ZoneProp;

/**
 * Class holds lithology info for globe use. It can search the
 * lithology name by given depth value.
 * @author Loui
 *
 */
public class GlobeLithology implements ActionListener{
	private ArrayList<LithologyItem> lithoList =
		new ArrayList<LithologyItem>();
	private static GlobeLithology instance;

	// added by Alex
	private JTable lithoTable;
	private DefaultTableModel myTableModel = null;

	private JDialog gdialog;
	private JTextField topField, bottomField;
	private JComboBox nameCBox;
	private JButton addButton, delButton, cloButton;

	private GlobeLithology(){

	}

	public static GlobeLithology getInstance(){
		if (instance==null)
			instance=new GlobeLithology();
		return instance;
	}

	public void showLithologyDialog(){
		if (gdialog == null) {
			gdialog = new JDialog();
			gdialog.setTitle("Global Lithology Settings");
			gdialog.setContentPane(createContentPane());
			gdialog.setSize(600, 280);
		}
		gdialog.setVisible(true);
	}

	/**
	 * build content panel
	 * @return content panel
	 */
	private JPanel createContentPane()	{
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(getTablePanel(), BorderLayout.CENTER);
		contentPanel.add(getControlPanel(), BorderLayout.EAST);

		return contentPanel;
	}

	/**
	 * make control panel with input fields and buttons
	 * @return control panel
	 */
	private Component getControlPanel() {
		JPanel cPanel = new JPanel();
		cPanel.setLayout(new BoxLayout(cPanel, BoxLayout.Y_AXIS));

		JLabel topLabel = new JLabel("Top: ");
		JLabel bottomLabel = new JLabel("Bottom: ");
		JLabel name = new JLabel("Name: ");

		topField = new JTextField();
		topField.setPreferredSize(new Dimension(25, 25));
		topField.setColumns(10);
		bottomField = new JTextField();
		bottomField.setPreferredSize(new Dimension(25, 25));
		bottomField.setColumns(10);
		nameCBox = new JComboBox(ZoneProp.getLithology());
		nameCBox.setPreferredSize(new Dimension(25, 25));
		JPanel inputPanel = new JPanel(new SpringLayout());
		inputPanel.setBorder(BorderFactory.createTitledBorder("New item "));
		inputPanel.add(topLabel);
		inputPanel.add(topField);
		inputPanel.add(bottomLabel);
		inputPanel.add(bottomField);
		inputPanel.add(name);
		inputPanel.add(nameCBox);
		inputPanel.add(Box.createHorizontalStrut(20));
		addButton = new JButton("Add");
		addButton.addActionListener(this);
		addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		inputPanel.add(addButton);

		inputPanel.setFocusable(true);
		inputPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		SpringUtilities.makeCompactGrid(inputPanel, 4, 2, 5, 5, 5, 5);

		JPanel deletePanel = new JPanel();
		deletePanel.setBorder(BorderFactory.createTitledBorder("Selected item "));
		delButton = new JButton("Delete");
		delButton.setEnabled(false);
		delButton.addActionListener(this);
		deletePanel.add(delButton);

		JPanel closePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		cloButton = new JButton("Close");
		cloButton.addActionListener(this);
		closePanel.add(cloButton);

		cPanel.add(inputPanel);
		cPanel.add(deletePanel);
		cPanel.add(closePanel);
		return cPanel;
	}

	/**
	 * build table data
	 * @return panel with table
	 */
	private Component getTablePanel() {
		String[] columnNames = {"Lithology name", "Top depth", "Bottom depth"};
		Object[][] data = getLithologyData();

		if (data != null)
			myTableModel = new DefaultTableModel(data, columnNames);
		else
			myTableModel = new DefaultTableModel(columnNames, 0);

		lithoTable= new JTable(myTableModel);
		lithoTable.getColumnModel().getColumn(0).setPreferredWidth(150);
		lithoTable.getColumnModel().getColumn(1).setPreferredWidth(75);
		lithoTable.getColumnModel().getColumn(2).setPreferredWidth(75);
		lithoTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		ListSelectionModel rowSel = lithoTable.getSelectionModel();
		rowSel.addListSelectionListener(
				new ListSelectionListener()	{
					public void valueChanged(ListSelectionEvent e) {
						ListSelectionModel lsm = (ListSelectionModel)e.getSource();
						if (!lsm.isSelectionEmpty())
							delButton.setEnabled(true);
						else
							delButton.setEnabled(false);
					}
				});

		JScrollPane tablePanel = new JScrollPane(lithoTable,
									JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
									JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		return tablePanel;
	}

	/**
	 * added by Alex
	 * get lithology data for fill table up
	 * @return lithology data
	 */
	private Object[][] getLithologyData()	{
		Object[][] data = null;

		if (lithoList.size() > 0)	{
			for (int i = 0; i < lithoList.size(); i++)	{
				LithologyItem item = lithoList.get(i);
				data[i][0] = item.getLithologyName();
				data[i][1] = new Float(item.getTop());
				data[i][2] = new Float(item.getBottom());
			}
		}
		return data;
	}

	/**
	 * Returns lithology name by given depth value.
	 * @param depth
	 * @return lithology name or empty string.
	 */
	public String getLithologyName(float depth){
		for (int i=0; i<lithoList.size(); i++){
			LithologyItem item = lithoList.get(i);
			if (item.getTop()<depth&&item.getBottom()>depth)
				return item.getLithologyName();
		}
		return "";
	}

	public void addLithology(float top, float bottom, String name){
		lithoList.add(new LithologyItem(top, bottom, name));
	}

	/**
	 * added by Alex
	 * prepare input to receive new data
	 */
	private void clearInput()	{
		nameCBox.setSelectedIndex(0);
		topField.setText("");
		bottomField.setText("");
	}

	/**
	 * added by Alex
	 * chek if entered data are valid ones
	 * @return true if data is correct, false if no
	 */
	private boolean validateInput()	{
		if (!topField.getText().matches("[0-9]+|[0-9]+\\.[0-9]+"))	{
			JOptionPane.showMessageDialog(gdialog, "You must enter valid float values");
			topField.setFocusable(true);
			return false;
		}

		if (!bottomField.getText().matches("[0-9]+|[0-9]+\\.[0-9]+"))	{
			JOptionPane.showMessageDialog(gdialog, "You must enter valid float values");
			return false;
		}

		if (Float.parseFloat(topField.getText()) >= Float.parseFloat(bottomField.getText()))	{
			JOptionPane.showMessageDialog(gdialog, "Bottom depth must be larger than Top depth");
			return false;
		}

		if (nameCBox.getSelectedIndex()==0)	{
			JOptionPane.showMessageDialog(gdialog, "You must choose a lithology");
			return false;
		}

		return true;
	}

	/**
	 * added by Alex
	 * check if previuos entered data already exist in list
	 * @return true if exist, false if no
	 */
	private boolean isDataExist(float top, float bottom) {
		if (lithoList.size() > 0)	{
			for (int i = 0; i < lithoList.size(); i++)	{
				LithologyItem item = lithoList.get(i);
				if (item.getBottom()==bottom && item.getTop()==top)
					return true;
			}
		}

		return false;
	}

	class LithologyItem{
		float top, bottom;
		String name;
		LithologyItem(float top, float bottom, String name){
			this.top = top;
			this.bottom = bottom;
			this.name = name;
		}

		public float getTop(){
			return top;
		}

		public float getBottom(){
			return bottom;
		}

		public String getLithologyName(){
			return name;
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==addButton)	{
			if (!validateInput())
				return;

			String lithoName = nameCBox.getSelectedItem().toString();
			float top = Float.parseFloat(topField.getText());
			float bottom = Float.parseFloat(bottomField.getText());

			// depth values cannot have different lithologys
			if (isDataExist(top, bottom))
				return;

			// add data to lithology list
			addLithology(top, bottom, lithoName);

			// add data to table
			Object[] dataRow = {lithoName, new Float(top), new Float(bottom)};
			myTableModel.addRow(dataRow);

			clearInput();
		}
		else if (e.getSource()==delButton)	{
			int index = lithoTable.getSelectedRow();

			// delete selected row from table and list
			myTableModel.removeRow(index);
			lithoList.remove(index);
		}else if (e.getSource()==cloButton){
			gdialog.setVisible(false);
		}
	}

}
