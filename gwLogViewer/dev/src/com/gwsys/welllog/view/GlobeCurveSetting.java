/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.view;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * a class that holds properties of curves
 * @author Alex
 *
 */
public class GlobeCurveSetting {
	private static HashMap<String, ArrayList> curveSettings = new HashMap<String, ArrayList>();

	/**
	 * add given curve settings
	 * @param key cuve name
	 * @param
	 */
	public static void addCurveSetting(String key, ArrayList values)	{

		curveSettings.put(key, values);
	}

	/**
	 *  returns Curve color
	 *  @param key: curve name will be searched
	 *  @return color of the curve requested
	 */
	public static Color getCurveColor(String key)	{
		Object obj = getAnyValueAt(key, 0);
		if (obj != null)
			return (Color) obj;

		return Color.BLACK;
	}

	/**
	 * returns line width of curve
	 * @param key curve name will be searched
	 * @return stroke of curve line
	 */
	public static float getCurveStroke(String key)	{
		Object obj = getAnyValueAt(key, 1);
		if (obj != null)
			return ((Float) obj).floatValue();

		return 1.0f;
	}

	/**
	 * returns grid mode of Curve
	 * @param key curve name will be search
	 * @return grid mode
	 */
	public static boolean getCurveGridMode(String key)	{
		Object obj = getAnyValueAt(key, 2);
		if (obj != null)
			return ((Boolean) obj).booleanValue();

		return false;
	}

	/**
	 * returns min range value
	 * @param key curve name will be searched
	 * @return min range value
	 */
	public static float getCurveMinValue(String key)	{
		Object obj = getAnyValueAt(key, 3);
		if (obj != null)
			return ((Float) obj).floatValue();

		return 0;
	}

	/**
	 * returns max range vlaue
	 * @param key curve name will be searched
	 * @return max range value
	 */
	public static float getCurveMaxValue(String key)	{
		Object obj = getAnyValueAt(key, 4);
		if (obj != null)
			return ((Float) obj).floatValue();

		return 0;
	}

	/**
	 * returns any object in given index
	 * @param key curve name will be searched
	 * @param index position of this object
	 * @return object
	 */
	private static Object getAnyValueAt(String key, int index)	{
		ArrayList values = (ArrayList) curveSettings.get(key);
		if (values != null)
			return values.get(index);

		return null;
	}

	/**
	 * check if this key exists in this settings
	 * @param key curve name will be searched
	 * @return true if key exists, false if no
	 */
	public static boolean thisCurveExists(String key)	{
		return curveSettings.containsKey(key);
	}

	/**
	 * check if settings is empty
	 * @return true if is empty, false if no
	 */
	public static boolean isEmpty()	{
		return curveSettings.isEmpty();
	}

	/**
	 * Returns the list of settings by curve name as key.
	 * @param key Curve name.
	 * @return the list of settings by curve name as key.
	 */
	public List getListSettings(String key){
		return curveSettings.get(key);
	}
}
