/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;

import com.gwsys.welllog.LogViewAgent;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.mainframe.MainBuffer;
import com.gwsys.welllog.mainframe.MainFrameView;
import com.gwsys.welllog.mainframe.MenuBarView;
import com.gwsys.welllog.mainframe.SplitPaneView;
import com.gwsys.welllog.mainframe.StatusBar;
import com.gwsys.welllog.mainframe.ToolbarCreator;
import com.gwsys.welllog.mainframe.ZoneListener;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.WorkSpace;
import com.gwsys.welllog.view.dialogs.ChooserDialog;
import com.gwsys.welllog.view.dialogs.CreateTrackDialog;

/**
 * Viewer is used for qiWorkbench.
 * @author Team
 */
public class QILogViewer extends JInternalFrame implements
    InternalFrameListener, MainFrameView{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    protected Dimension defaultSize = ConstantVariableList.MAINFRAME_SIZE;

    private JPanel jContentPane = null;

    private SplitPaneView splitPaneView = null;

    private MenuBarView menuBar = null;

    private JFileChooser fileChooser = null;

    private JFileChooser projectSaveFileChooser = null; // save project

    private JFileChooser projectLoadFileChooser = null;

    private ChooserDialog chooser;

    private CreateTrackDialog createTrackDialog = null;

    private StatusBar messageBar = null;

    private MainBuffer mainBuffer = null;

    private ToolbarCreator toolbar;

    private ZoneListener zoneListener;
    private NodeList trendList;
    /**
     * Constructor
     * @param Title Title of Frame.
     */
    public QILogViewer(String Title) {
        super(Title);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.addInternalFrameListener(this);

        menuBar = new MenuBarView(this);

        this.setJMenuBar(menuBar);

        this.setContentPane(this.getJContentPane());

        setSize(defaultSize);

        this.mainBuffer = new MainBuffer(this);

        DataBuffer.setJMainFrame(this);
    }
    /**
     *
     * @param title
     */
    public QILogViewer(String title, LogViewAgent agent) {
        super(title, true, false, true, true);
        if (agent!=null){
            manager = agent.getMessagingManager();

            this.agent = agent;
        }

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        if (menuBar == null) {
            menuBar = new MenuBarView(this);
        }

        this.setJMenuBar(menuBar);

        this.setContentPane(this.getJContentPane());

        setSize(defaultSize);
        this.setFrameIcon(IconResource.getInstance()
                .getImageIcon("LogViewerLogo.gif"));
        mainBuffer = new MainBuffer(this);

        ViewCoreOperator.createFrame(this);
        DataBuffer.setJMainFrame(this);
    }

    /**
     *
     * @return new createTrackDialog
     * @author gaofei
     */
    public CreateTrackDialog getCreateTrackDialog() {
        if (createTrackDialog == null) {
            createTrackDialog = new CreateTrackDialog(
                    "Create Log Section", this);
        }
        return createTrackDialog;
    }

    public void setCreateTrackDialog(CreateTrackDialog createTrackDialog) {
        this.createTrackDialog = createTrackDialog;
    }

    /**
     * This method initializes jContentPane
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJContentPane() {
        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BorderLayout());
            jContentPane.add(getSonJSplitPane(), java.awt.BorderLayout.CENTER);
            toolbar = new ToolbarCreator();
            jContentPane.add(toolbar.buildIconBar(this), BorderLayout.NORTH);
            messageBar = new StatusBar();
            jContentPane.add(messageBar, BorderLayout.SOUTH);
        }
        return jContentPane;
    }

    /**
     * SplitPaneView
     *
     * @return SplitPaneView
     */
    public SplitPaneView getSonJSplitPane() {
        if (splitPaneView == null) {
            splitPaneView = new SplitPaneView();
        }
        return splitPaneView;
    }

    public MainBuffer getMainBuffer() {
        return mainBuffer;
    }

    public JFileChooser getFileChooser() {
        if (fileChooser == null) {
            fileChooser = new JFileChooser();
        }
        return fileChooser;
    }

    public void setFileChooser(JFileChooser fileChooser) {
        this.fileChooser = fileChooser;
    }

    public JFileChooser getProjectSaveFileChooser() {
        if (projectSaveFileChooser == null) {
            projectSaveFileChooser = new JFileChooser();
        }
        return projectSaveFileChooser;
    }

    public void setProjectSaveFileChooser(JFileChooser projectSaveFileChooser) {
        this.projectSaveFileChooser = projectSaveFileChooser;
    }

    public JFileChooser getProjectLoadFileChooser() {
        if (projectLoadFileChooser == null) {
            projectLoadFileChooser = new JFileChooser();
        }
        return projectLoadFileChooser;
    }

    public void setProjectLoadFileChooser(JFileChooser projectLoadFileChooser) {
        this.projectLoadFileChooser = projectLoadFileChooser;
    }

    /**
     * Unique ID
     */
    public String getCID() {
        return getClass().getName() + "#" + hashCode();
    }

    public void setCurveInfo(String info) {
        messageBar.updateCurveInfo(info);
    }

    public void setMessage(String message) {
        messageBar.updateStatusBar(message);
    }

    public StatusBar getStatusBar(){
        return messageBar;
    }


    public void windowActivated(WindowEvent e) {
        DataBuffer.setJMainFrame((MainFrameView) e.getSource());
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowOpened(WindowEvent e) {

    }

    //State of ToggleButton June 16
    public boolean isMakingCable() {
        return toolbar.isMakingCable();
    }

    public boolean isMakingMarkerFill() {
        return toolbar.isMakingMarkerFill();
    }

    public boolean isMakingMarkerLith() {
        return toolbar.isMakingMarkerLith();
    }

    public ChooserDialog getChooser(){
        if (chooser == null)
            chooser = new ChooserDialog(this, null);
        return chooser;
    }

    public Frame getFrame(){
        Frame frame = JOptionPane.getRootFrame();
        frame.setIconImage(IconResource.getInstance()
                .getImageIcon("LogViewerLogo.gif").getImage());
        return frame;
    }

    /**
     * added by Alex
     * get Vertical Measurement JComboBox
     * @return mCBox
     */
    public JComboBox getMeasurCBox()    {
        return toolbar.getMeasurCBox();
    }

    /**
     * added by Alex
     * get Depth Scale JComboBox
     * @return scaleCBox
     */
    public JComboBox getScaleCBox() {
        return toolbar.getScaleCBox();
    }

    //merge from China
    public JToggleButton getTmpTgFillBtn() {
        return toolbar.getMarkerFillButton();
    }

    public boolean isTesting() {
        return toolbar.isTesting();
    }

    public void setTesting(boolean isTesting) {
        toolbar.setTesting(isTesting);
    }

    /**
     * For QiWorkbench
     * @return
     */
    public LogViewAgent getAgent(){
        return agent;
    }

    public MessagingManager getManager(){
        return manager;
    }

    //**Implements API in qiWorkbench.
    /**
     * Save the current state in qiWorkbench.
     * @return String.
     */
    public String saveState(){
        StringBuffer content = new StringBuffer();
        content.append("<" + this.getClass().getName() + " ");
        String path = getSonJSplitPane().getLeftTree().getProjectPath();
        if (path.indexOf(".wks")<0){
            String home = System.getProperty("user.home");
            path = home+File.separator+"qiworkbench"+File.separator+path+".wks";
        }

        final String qipath = path;
        content.append("title=\"" + this.getTitle() + "\" ");
        content.append("project=\"" + path + "\" ");
        content.append("filesystem=\"" + path + "\" >\n");
        //save tree structure
        new Thread(){
            public void run(){
                WorkSpace.saveWorkSpaceToXml(qipath, QILogViewer.this);
            }
        }.start();
        content.append("</" + this.getClass().getName() + ">\n");
        return content.toString();
    }

    public void restoreState(String xmlFile){
        WorkSpace.loadWorkSpaceFromXml(xmlFile, this);
    }

    public void rename(String name){
//        this.setTitle(name);
        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        resetTitle(QiProjectDescUtils.getQiProjectName(qpDesc));
    }

    public void internalFrameOpened(InternalFrameEvent e) {
        DataBuffer.setJMainFrame((MainFrameView) e.getSource());
    }
    public void internalFrameClosing(InternalFrameEvent arg0) {

    }
    public void internalFrameClosed(InternalFrameEvent arg0) {

    }
    public void internalFrameIconified(InternalFrameEvent arg0) {

    }
    public void internalFrameDeiconified(InternalFrameEvent arg0) {

    }
    public void internalFrameActivated(InternalFrameEvent e) {
        DataBuffer.setJMainFrame((MainFrameView) e.getSource());

    }
    public void internalFrameDeactivated(InternalFrameEvent arg0) {

    }

    private MessagingManager manager;

    private LogViewAgent agent;

    public void closeViewerGUI() {
        if (jContentPane == null)
            return;
        jContentPane.removeAll();
        setVisible(false);
        dispose();
    }

    /**
     * Gets the zone listener.
     * @return zone listener.
     */
    public ZoneListener getZoneListener(){
        return zoneListener;
    }

    public void setZoneListener(ZoneListener listener){
        zoneListener = listener;
        if (trendList!=null&&zoneListener!=null)
            loadXML();
    }

    public void setXMLNodeList(NodeList nlist){
        trendList =nlist;
        if (zoneListener!=null){
            loadXML();
        }
    }

    private void loadXML(){
        for (int i=0; i<trendList.getLength(); i++){
            if (trendList.item(i) instanceof Element)
                zoneListener.loadXML((Element)trendList.item(i));
        }
        trendList = null;
    }

    /**
     * Reset the GUI's title
     *
     * @param name Name of the associated project.
     */
    public void resetTitle(String projName) {
        String compName = "";
        ComponentDescriptor agentDesc = agent.getComponentDescriptor();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0)
            compName = pdn.trim();
        else
            compName = screenName;

        //get the type of qiComponent
        int idx = screenName.indexOf("#");
        if (idx != -1) screenName = screenName.substring(0, idx);
        this.setTitle(screenName+": " + compName + "  Project: " + projName);
    }
}
