/*
gwLogLib - a devkit for a Log Viewer
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.icons;

import java.awt.Image;

import javax.swing.ImageIcon;

public class IconResource {
	

	public static final String B_NEWLOG = "newlog.gif";
	
	public static final String B_EDITCURVE="editcurve.gif";
	
	public static final String B_OPENLAS = "openlas.gif";
	
    public static final String B_OPENSU = "opensu.gif";

	public static final String B_SAVE = "save.gif";

	public static final String B_WLADDTRACK = "addtrack.gif";

	public static final String B_WLEDITTRACK = "edittrack.gif";

	public static final String B_WLFILLMAKER = "fillmaker.gif";

	public static final String B_WLLITHMAKER = "lithmaker.gif";

	public static final String B_WLMARKERMAKE = "markermake.gif";
	
	public static final String B_MARKERCABLE = "markercable.gif";
	
    public static final String B_WELLICON = "gwlogo.gif";
	private static IconResource _instance = null;

	protected IconResource() {
	}

	public static IconResource getInstance() {
		if (_instance == null)
			_instance = new IconResource();
		return _instance;
	}

	public ImageIcon getImageIcon(String imageFile) {
		ImageIcon result = null;
		try {
			result = new ImageIcon(this.getClass().getResource(imageFile));
		} catch (Exception ex) {
			System.out.println("IconResource.getImageIcon exception: ["
					+ imageFile + "]");
			System.out.println("    " + ex.toString());
			return null;
		}
		return result;
	}
	
	public Image getImage(String imageFile){
		ImageIcon result = getImageIcon(imageFile);
		if (result==null){
			return null;
		}else{
			return result.getImage();
		}
	}
}
