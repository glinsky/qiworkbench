/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.mainframe;

import java.awt.Cursor;
import java.awt.Frame;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JToggleButton;

import org.w3c.dom.NodeList;

import com.gwsys.welllog.view.dialogs.ChooserDialog;
import com.gwsys.welllog.view.dialogs.CreateTrackDialog;

/**
 * Frame holds many log windows.
 * @author Team
 */
public interface MainFrameView {

	public CreateTrackDialog getCreateTrackDialog();

	public void setCreateTrackDialog(CreateTrackDialog createTrackDialog) ;

	/**
	 * SplitPaneView
	 *
	 * @return SplitPaneView
	 */
	public SplitPaneView getSonJSplitPane();

	public MainBuffer getMainBuffer();

	public JFileChooser getFileChooser();

	public void setFileChooser(JFileChooser fileChooser);

	public JFileChooser getProjectSaveFileChooser();

	public void setProjectSaveFileChooser(JFileChooser projectSaveFileChooser);

	public JFileChooser getProjectLoadFileChooser() ;

	public void setProjectLoadFileChooser(JFileChooser projectLoadFileChooser);


	public void setCurveInfo(String info);

	public void setMessage(String message);

	/**
	 * Gets the status bar.
	 * @return
	 */
	public StatusBar getStatusBar();

	/**
	 * Check the status of current action.
	 * @return
	 */
	public boolean isMakingCable();

	public boolean isMakingMarkerFill();

	public boolean isMakingMarkerLith();

	public ChooserDialog getChooser();

	public Frame getFrame();

	/**
	 * added by Alex
	 * get Vertical Measurement JComboBox
	 * @return mCBox
	 */
	public JComboBox getMeasurCBox();

	/**
	 * added by Alex
	 * get Depth Scale JComboBox
	 * @return scaleCBox
	 */
	public JComboBox getScaleCBox()	;

	public JToggleButton getTmpTgFillBtn() ;

	public boolean isTesting();

	public void setTesting(boolean isTesting);

	/**
	 * Gets the zone listener.
	 * @return zone listener.
	 */
	public ZoneListener getZoneListener();

	public void setZoneListener(ZoneListener listener);

	public void setXMLNodeList(NodeList nlist);

	public void repaint();

	public void dispose();

	public void setVisible(boolean vis);

	public void setCursor(Cursor cursor);
}
