/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.mainframe;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import com.gwsys.data.well.XmlFileFilter;
import com.gwsys.welllog.actions.AddInternalFrameAction;
import com.gwsys.welllog.actions.CreateMarkerAction;
import com.gwsys.welllog.actions.CreateTrackAction;
import com.gwsys.welllog.actions.EditCurveAction;
import com.gwsys.welllog.actions.EditTrackAction;
import com.gwsys.welllog.actions.LoadBhpsuAction;
import com.gwsys.welllog.actions.LoadLasAction;
import com.gwsys.welllog.actions.SaveProjectAction;
import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.ViewCoreOperator;
import com.gwsys.welllog.view.curve.CurveMarker;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.dialogs.EditFillMarkerDialog;
import com.gwsys.welllog.view.dialogs.EditLithologyMarkerDialog;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.MarkerConnectorPoint;
import com.gwsys.welllog.view.track.TrackContainerView;

/**
 * Decouple from MainFrameView.
 * @author Team
 *
 */
public class ToolbarCreator implements ActionListener{
	private JToggleButton tmpTgBtn = new JToggleButton();
	private JToggleButton tmpTgFillBtn = new JToggleButton();
	private JToggleButton tmpTgLithBtn = new JToggleButton();

	private boolean makingCable = false;
	private boolean makingMarkerFill = false;
	private boolean makingMarkerLith = false;

	private JComboBox mCBox = null;
	private JComboBox scaleCBox = null;
	
	// Added by Alex. this is for templates
	private JComboBox tmplCBox = null;
	private JButton tmplButton = null;

	private boolean selectTgBtn = false;
	private boolean selectFillBtn = false;
	private boolean selectLithBtn = false;
	private boolean isTesting = false;
//	 by mexico team
	public JToolBar buildIconBar(MainFrameView view) {
		JToolBar ToolBarView = new JToolBar();

		JButton tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_NEWLOG));
		tmpBtn.setToolTipText("New Section");
		tmpBtn.addActionListener(new AddInternalFrameAction(view));
		ToolBarView.add(tmpBtn);

		tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_OPENLAS));
		tmpBtn.setToolTipText("Open LAS");
		tmpBtn.addActionListener(new LoadLasAction(view));
		ToolBarView.add(tmpBtn);

		tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_OPENSU));
		tmpBtn.setToolTipText("Open SU");
		tmpBtn.addActionListener(new LoadBhpsuAction(view));
		ToolBarView.add(tmpBtn);

		tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_SAVE));
		tmpBtn.setToolTipText("Save Project");
		tmpBtn.addActionListener(new SaveProjectAction(view));
		ToolBarView.add(tmpBtn);

		ToolBarView.addSeparator();

		tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_WLEDITTRACK));
		tmpBtn.setToolTipText("Edit Log");
		tmpBtn.addActionListener(new EditTrackAction(view));
		ToolBarView.add(tmpBtn);

		tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_EDITCURVE));
		tmpBtn.setToolTipText("Edit Curve");
		tmpBtn.addActionListener(new EditCurveAction(view));
		ToolBarView.add(tmpBtn);

		ToolBarView.addSeparator();

		tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_WLADDTRACK));
		tmpBtn.setToolTipText("New Log");
		tmpBtn.addActionListener(new CreateTrackAction(view));
		ToolBarView.add(tmpBtn);

		tmpBtn = new JButton();
		tmpBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_WLMARKERMAKE));
		tmpBtn.setToolTipText("New Marker");
		tmpBtn.addActionListener(new CreateMarkerAction(view));
		ToolBarView.add(tmpBtn);

		tmpTgBtn = new JToggleButton();
		tmpTgBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_MARKERCABLE));
		tmpTgBtn.setToolTipText("Link Marker Together");
		ToolBarView.add(tmpTgBtn);
		tmpTgBtn.addItemListener(new ItemListener() {
			@SuppressWarnings({"unchecked","unchecked"})
			public void itemStateChanged(ItemEvent e) {
				int state = e.getStateChange();
				MainFrameView jMainFrame = DataBuffer.getJMainFrame();
				if (jMainFrame.getSonJSplitPane().getRightDesktopPane()
						.getSelectedInternalFrameView() == null) {
					JOptionPane.showMessageDialog(jMainFrame.getFrame(),
							"Please select an InternalFrame!", "Message",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				TrackContainerView tcv = jMainFrame.getSonJSplitPane()
				.getRightDesktopPane()
				.getSelectedInternalFrameView().getTrackContainer();
				if (state == ItemEvent.SELECTED) {
					tmpTgFillBtn.setEnabled(false);
					tmpTgLithBtn.setEnabled(false);
					tcv.getMarkerBuffer().clear();
					for(int i=0 ; i< tcv.getAxisCurveContainerList().size() ; i++){
						ArrayList markerList = ((AxisCurveContainerView)tcv.getAxisCurveContainerList().get(i)).getCurveView().getMarkerLayer().getMarkerList();
						for(int j=0 ; j<markerList.size() ; j++){
							((CurveMarker)markerList.get(j)).setSelected(false);
						}
						((AxisCurveContainerView)tcv.getAxisCurveContainerList().get(i)).getCurveView().repaint();
					}
					makingCable = true;
				} else {
					tmpTgFillBtn.setEnabled(true);
					tmpTgLithBtn.setEnabled(true);

					if (tcv.getMarkerBuffer().size() < 2) {
						JOptionPane.showMessageDialog(jMainFrame.getFrame(),
								"Please select at least two markers.",
								"Message", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					//change project tree June 16
					DataBuffer.getJMainFrame().getMainBuffer().getProjectTree()
					.connectWellTopNode(tcv.getMarkerBuffer());
					//change name July 5th
					for (int i = 1; i < tcv.getMarkerBuffer().size(); i++) {
						 ((MarkerConnectorPoint) tcv.getMarkerBuffer().get(i)).getCm()
						 .setMarkerName(((MarkerConnectorPoint) tcv.getMarkerBuffer().get(0)).getCm().getMarkerName());
					}
					// connect
					ViewCoreOperator.connectMarker(DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView());
                    DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView().getTrackContainer().repaint();
					//repaint();
					makingCable = false;
				}
			}
		});

		tmpTgFillBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_WLFILLMAKER));
		tmpTgFillBtn.setToolTipText("Fill Color Between Markers");
		ToolBarView.add(tmpTgFillBtn);
		tmpTgFillBtn.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int state = e.getStateChange();
				MainFrameView jMainFrame = DataBuffer.getJMainFrame();
				if (jMainFrame.getSonJSplitPane().getRightDesktopPane()
						.getSelectedInternalFrameView() == null) {
					JOptionPane.showMessageDialog(jMainFrame.getFrame(),
							"Please select an InternalFrame!", "Message",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				TrackContainerView tcv = jMainFrame.getSonJSplitPane()
				.getRightDesktopPane()
				.getSelectedInternalFrameView().getTrackContainer();
				if (state == ItemEvent.SELECTED) {
					tmpTgBtn.setEnabled(false);
					tmpTgLithBtn.setEnabled(false);
					for(int i=0 ; i< tcv.getAxisCurveContainerList().size() ; i++){
						CurveView cv = ((AxisCurveContainerView)tcv.getAxisCurveContainerList().get(i)).getCurveView();
						for(int j=0 ; j<cv.getMarkerLayer().getMarkerList().size() ; j++){
							((CurveMarker)cv.getMarkerLayer().getMarkerList().get(j)).setSelected(false);
						}
						cv.getMarkerLayer().getMarkerSelectedList().clear();
						cv.repaint();
					}
					setSelectFillBtn(true);
					makingMarkerFill = true;
					setSelectTgBtn(false);
					tmpTgBtn.setSelected(false);
					setSelectLithBtn(false);
					tmpTgLithBtn.setSelected(false);
				}else{
					tmpTgBtn.setEnabled(true);
					tmpTgLithBtn.setEnabled(true);
					makingMarkerFill = false;
					if(!isSelectFillBtn()||tcv.getSelectedAxisCurveContainer()==null){
						return;
					}
					CurveView cv = tcv.getSelectedAxisCurveContainer().getCurveView();
					if (cv.getMarkerLayer().getMarkerSelectedList().size() < 2) {
						JOptionPane.showMessageDialog(jMainFrame.getFrame(),
								"Please select at least two markers.",
								"Message", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					if(isTesting()){
						Robot robot=null;
						try{
							robot = new Robot();
						}catch(AWTException awte){
							return;
						}
						@SuppressWarnings("unused") EditFillMarkerDialog
						efmd = new EditFillMarkerDialog(cv,robot);
					}else{
						EditFillMarkerDialog efmd = new EditFillMarkerDialog(cv);
						efmd.setVisible(true);
					}
				}
			}
		});

		tmpTgLithBtn.setIcon(IconResource.getInstance().getImageIcon(
				IconResource.B_WLLITHMAKER));
		tmpTgLithBtn.setToolTipText("Fill Lithology Between Markers");
		ToolBarView.add(tmpTgLithBtn);
		tmpTgLithBtn.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int state = e.getStateChange();
				MainFrameView jMainFrame = DataBuffer.getJMainFrame();
				if (jMainFrame.getSonJSplitPane().getRightDesktopPane()
						.getSelectedInternalFrameView() == null) {
					JOptionPane.showMessageDialog(jMainFrame.getFrame(),
							"Please select an InternalFrame!", "Message",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				TrackContainerView tcv = jMainFrame.getSonJSplitPane()
				.getRightDesktopPane()
				.getSelectedInternalFrameView().getTrackContainer();
				if (state == ItemEvent.SELECTED) {
					tmpTgFillBtn.setEnabled(false);
					tmpTgBtn.setEnabled(false);
					for(int i=0 ; i< tcv.getAxisCurveContainerList().size() ; i++){
						CurveView cv = ((AxisCurveContainerView)tcv.getAxisCurveContainerList().get(i)).getCurveView();
						for(int j=0 ; j<cv.getMarkerLayer().getMarkerList().size() ; j++){
							((CurveMarker)cv.getMarkerLayer().getMarkerList().get(j)).setSelected(false);
						}
						cv.getMarkerLayer().getMarkerSelectedList().clear();
						cv.repaint();
					}
					setSelectLithBtn(true);
					makingMarkerLith = true;
					setSelectTgBtn(false);
					tmpTgBtn.setSelected(false);
					setSelectFillBtn(false);
					tmpTgFillBtn.setSelected(false);
				}else{
					makingMarkerLith = false;
					tmpTgBtn.setEnabled(true);
					tmpTgFillBtn.setEnabled(true);
					if(!isSelectLithBtn()||tcv.getSelectedAxisCurveContainer()==null){
						return;
					}
					CurveView cv = tcv.getSelectedAxisCurveContainer().getCurveView();
					if (cv.getMarkerLayer().getMarkerSelectedList().size() < 2) {
						JOptionPane.showMessageDialog(jMainFrame.getFrame(),
								"Please select at least two markers.",
								"Message", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					EditLithologyMarkerDialog elmd = new EditLithologyMarkerDialog(cv);
					elmd.setVisible(true);
				}
			}
		});

		// Added by Alex. July 19, 2006
		ToolBarView.addSeparator();

		JLabel mLabel = new JLabel("Measure.:");
		mCBox = new JComboBox();
		mCBox.setToolTipText("Vertical Measurement");
		mCBox.setMaximumSize(new Dimension(100,20));
		mCBox.addItem(ConstantVariableList.MEASUREMENT_MD);
		mCBox.addItem(ConstantVariableList.MEASUREMENT_TVD);
		mCBox.addItem(ConstantVariableList.MEASUREMENT_TVDSS);
		mCBox.addActionListener(this);
		mCBox.setActionCommand("changeMeasurement");

		JLabel scaleLabel = new JLabel("Scale:");
		String[] depthScale = {"50","200","500","1000","2000","5000","10000"};
		scaleCBox = new JComboBox(depthScale);
		scaleCBox.setMaximumSize(new Dimension(100,20));
		scaleCBox.setToolTipText("Vertical Scale");
		scaleCBox.addActionListener(this);
		scaleCBox.setActionCommand("changeScale");
		
		ToolBarView.add(mLabel);
		ToolBarView.add(mCBox);

		ToolBarView.addSeparator();
		ToolBarView.add(scaleLabel);
		ToolBarView.add(scaleCBox);
		// lines Ends.
		
		// Added by Alex. November 16, 2006
		ToolBarView.addSeparator();
		
		JLabel tLabel = new JLabel("Templates:");
		tmplCBox = new JComboBox();
		tmplCBox.setToolTipText("Select one template to use");
		tmplCBox.setMaximumSize(new Dimension(100,20));
		tmplCBox.setEnabled(false);
		tmplCBox.addActionListener(this);
		tmplCBox.setActionCommand("useTemplate");
		
		tmplButton = new JButton("Add");
		tmplButton.setToolTipText("Add templates");
		tmplButton.addActionListener(this);
		tmplButton.setActionCommand("addTemplate");
		
		ToolBarView.add(tLabel);
		ToolBarView.add(tmplCBox);
		ToolBarView.add(tmplButton);

		return ToolBarView;
	}

//  added by Alex. July 19, 2006
	//  action events for JComboBoxes (measurement, depth scale)
	public void actionPerformed(ActionEvent e) {
		InternalFrameView selectedInternalFrameView =
			DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		if (selectedInternalFrameView == null)
			return;
		if(!selectedInternalFrameView.isPaint()){
			return;
		}

		if (e.getActionCommand().equals("changeMeasurement"))	{
			selectedInternalFrameView.enableVerticalMeasurement(true);

			ViewCoreOperator.updateMeasurementType(getMeasurCBox().getSelectedIndex(),
					selectedInternalFrameView);

		}
		else if (e.getActionCommand().equals("changeScale"))	{
			selectedInternalFrameView.enableVerticalScale(true);

			ViewCoreOperator.updateMeasurementScale(getScaleCBox().getSelectedIndex(),
					selectedInternalFrameView);
		}
		else if (e.getActionCommand().equals("useTemplate"))	{
			String selected = getTemplateCBox().getSelectedItem().toString();
			System.out.println("use template: "+selected);
			
			// set this template as default
			DataBuffer.getJMainFrame().getMainBuffer().setDefaultTemplate(selected);
		}
		else if (e.getActionCommand().equals("addTemplate"))	{
			// call method - prototype: getTemplate
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new XmlFileFilter("xtf"));
			int retVal = fc.showOpenDialog(null);
			
			if (retVal == JFileChooser.APPROVE_OPTION)	{
				String filename = fc.getSelectedFile().getPath();				
				String template = ViewCoreOperator.loadWellTemplate(filename);
				
				if (template != null)	{
					getTemplateCBox().addItem(template);
					getTemplateCBox().setEnabled(true);
				}
			}			
		}
	}

	public boolean isMakingCable() {
		return makingCable;
	}

	public boolean isMakingMarkerFill() {
		return makingMarkerFill;
	}

	public boolean isMakingMarkerLith() {
		return makingMarkerLith;
	}

	public void setMakingMarkerLith(boolean makingMarkerLith) {
		this.makingMarkerLith = makingMarkerLith;
	}

	public boolean isSelectLithBtn() {
		return selectLithBtn;
	}

	public void setSelectLithBtn(boolean selectLithBtn) {
		this.selectLithBtn = selectLithBtn;
	}

	public boolean isSelectTgBtn() {
		return selectTgBtn;
	}

	public void setSelectTgBtn(boolean selectTgBtn) {
		this.selectTgBtn = selectTgBtn;
	}

	public boolean isSelectFillBtn() {
		return selectFillBtn;
	}

	public void setSelectFillBtn(boolean selectFillBtn) {
		this.selectFillBtn = selectFillBtn;
	}

	/**
	 * added by Alex
	 * get Vertical Measurement JComboBox
	 * @return mCBox
	 */
	public JComboBox getMeasurCBox()	{
		return mCBox;
	}

	/**
	 * added by Alex
	 * get Depth Scale JComboBox
	 * @return scaleCBox
	 */
	public JComboBox getScaleCBox()	{
		return scaleCBox;
	}
	
	/**
	 * added by Alex
	 * get templates' JComboBox
	 * @return templCBox
	 */
	public JComboBox getTemplateCBox()	{
		return tmplCBox;
	}

	public JToggleButton getMarkerFillButton() {
		return tmpTgFillBtn;
	}

	public boolean isTesting() {
		return isTesting;
	}

	public void setTesting(boolean isTesting) {
		this.isTesting = isTesting;
	}
}
