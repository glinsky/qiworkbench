/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.mainframe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gwsys.welllog.core.curve.ConstantVariableList;
import com.gwsys.welllog.icons.IconResource;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.dialogs.ChooserDialog;
import com.gwsys.welllog.view.dialogs.CreateTrackDialog;

/**
 * Starts A Stand Alone Application.
 * @author Team
 *
 */
public class DefaultViewer extends JFrame implements
		MouseListener, WindowListener, MainFrameView{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	protected Dimension defaultSize = ConstantVariableList.MAINFRAME_SIZE;

	private JPanel jContentPane = null;

	private SplitPaneView splitPaneView = null;

	private MenuBarView menuBar = null;

	private JFileChooser fileChooser = null;

	private JFileChooser projectSaveFileChooser = null; // save project

	private JFileChooser projectLoadFileChooser = null;

	private ChooserDialog chooser;

	private CreateTrackDialog createTrackDialog = null;

	private StatusBar messageBar = null;

	private MainBuffer mainBuffer = null;

	private ToolbarCreator toolbar;

	/**
	 * Constructor
	 * @param Title Title of Frame.
	 */
	public DefaultViewer(String Title) {
		super(Title);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		this.addWindowListener(this);

		menuBar = new MenuBarView(this);

		this.setJMenuBar(menuBar);

		this.setContentPane(this.getJContentPane());

		setSize(defaultSize);

		this.mainBuffer = new MainBuffer(this);

		DataBuffer.setJMainFrame(this);
	}

	/**
	 *
	 * @return new createTrackDialog
	 * @author gaofei
	 */
	public CreateTrackDialog getCreateTrackDialog() {
		if (createTrackDialog == null) {
			createTrackDialog = new CreateTrackDialog("Create Log Section", this);
		}
		return createTrackDialog;
	}

	public void setCreateTrackDialog(CreateTrackDialog createTrackDialog) {
		this.createTrackDialog = createTrackDialog;
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getSonJSplitPane(), java.awt.BorderLayout.CENTER);
			toolbar = new ToolbarCreator();
			jContentPane.add(toolbar.buildIconBar(this), BorderLayout.NORTH);
			messageBar = new StatusBar();
			messageBar.addMouseListener(this);
			jContentPane.add(messageBar, BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	/**
	 * SplitPaneView
	 *
	 * @return SplitPaneView
	 */
	public SplitPaneView getSonJSplitPane() {
		if (splitPaneView == null) {
			splitPaneView = new SplitPaneView();
			splitPaneView.getRightDesktopPane().addMouseListener(this);
			splitPaneView.getLeftTree().addMouseListener(this);
		}
		return splitPaneView;
	}

	public MainBuffer getMainBuffer() {
		return mainBuffer;
	}

	public JFileChooser getFileChooser() {
		if (fileChooser == null) {
			fileChooser = new JFileChooser();
		}
		return fileChooser;
	}

	public void setFileChooser(JFileChooser fileChooser) {
		this.fileChooser = fileChooser;
	}

	public JFileChooser getProjectSaveFileChooser() {
		if (projectSaveFileChooser == null) {
			projectSaveFileChooser = new JFileChooser();
		}
		return projectSaveFileChooser;
	}

	public void setProjectSaveFileChooser(JFileChooser projectSaveFileChooser) {
		this.projectSaveFileChooser = projectSaveFileChooser;
	}

	public JFileChooser getProjectLoadFileChooser() {
		if (projectLoadFileChooser == null) {
			projectLoadFileChooser = new JFileChooser();
		}
		return projectLoadFileChooser;
	}

	public void setProjectLoadFileChooser(JFileChooser projectLoadFileChooser) {
		this.projectLoadFileChooser = projectLoadFileChooser;
	}

	/**
	 * Unique ID
	 */
	public String getCID() {
		return getClass().getName() + "#" + hashCode();
	}

	public void setCurveInfo(String info) {
		messageBar.updateCurveInfo(info);
	}

	public void setMessage(String message) {
		messageBar.updateStatusBar(message);
	}


	public StatusBar getStatusBar() {
		return messageBar;
	}

	public void mouseClicked(MouseEvent arg0) {

	}

	public void mousePressed(MouseEvent arg0) {

	}

	public void mouseReleased(MouseEvent arg0) {

	}

	public void mouseEntered(MouseEvent arg0) {

	}

	public void mouseExited(MouseEvent arg0) {

	}

	public void windowActivated(WindowEvent e) {
		DataBuffer.setJMainFrame((MainFrameView) e.getSource());
	}

	public void windowClosed(WindowEvent e) {

	}

	public void windowClosing(WindowEvent e) {

	}

	public void windowDeactivated(WindowEvent e) {

	}

	public void windowDeiconified(WindowEvent e) {

	}

	public void windowIconified(WindowEvent e) {

	}

	public void windowOpened(WindowEvent e) {

	}

	//State of ToggleButton June 16
	public boolean isMakingCable() {
		return toolbar.isMakingCable();
	}

	public boolean isMakingMarkerFill() {
		return toolbar.isMakingMarkerFill();
	}

	public boolean isMakingMarkerLith() {
		return toolbar.isMakingMarkerLith();
	}

	public ChooserDialog getChooser(){
		if (chooser == null)
			chooser = new ChooserDialog(this, null);
		return chooser;
	}

	public Frame getFrame(){
		if (this instanceof Frame)
			return this;
		Frame frame = JOptionPane.getRootFrame();
		frame.setIconImage(IconResource.getInstance()
				.getImageIcon("LogViewerLogo.gif").getImage());
		return frame;
	}

	/**
	 * added by Alex
	 * get Vertical Measurement JComboBox
	 * @return mCBox
	 */
	public JComboBox getMeasurCBox()	{
		return toolbar.getMeasurCBox();
	}

	/**
	 * added by Alex
	 * get Depth Scale JComboBox
	 * @return scaleCBox
	 */
	public JComboBox getScaleCBox()	{
		return toolbar.getScaleCBox();
	}

	//merge from China
	public JToggleButton getTmpTgFillBtn() {
		return toolbar.getMarkerFillButton();
	}

	public boolean isTesting() {
		return toolbar.isTesting();
	}

	public void setTesting(boolean isTesting) {
		toolbar.setTesting(isTesting);
	}

	/**
	 * Gets the zone listener.
	 * @return zone listener.
	 */
	public ZoneListener getZoneListener(){
		return zoneListener;
	}

	public void setZoneListener(ZoneListener listener){
		zoneListener = listener;
		if (trendList!=null&&zoneListener!=null)
			loadXML();
	}

	public void setXMLNodeList(NodeList nlist){
		trendList =nlist;
		if (zoneListener!=null){
			loadXML();
		}
	}

	private void loadXML(){
		for (int i=0; i<trendList.getLength(); i++){
			if (trendList.item(i) instanceof Element)
				zoneListener.loadXML((Element)trendList.item(i));
		}
		trendList = null;
	}

	private NodeList trendList;
	private ZoneListener zoneListener;
}

