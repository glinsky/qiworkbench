/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.mainframe;

import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;

import com.gwsys.welllog.core.curve.ConstantVariableList;

public class HelpView extends JFrame implements HyperlinkListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JEditorPane jep = null;
	private JScrollPane jsp = null;
	
	public HelpView()
	{
	//	this.getContentPane().add(this.getjep(),null);
		this.setContentPane(this.getjsp());
		this.setSize(ConstantVariableList.HELP_SIZE);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//this.pack();
	}
	public JScrollPane getjsp()
	{
		if(this.jsp == null)
		{
			jsp = new JScrollPane();
			jsp.setViewportView(this.getjep());
			jsp.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			jsp.setVerticalScrollBarPolicy(javax.swing.JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		}
		return this.jsp;
	}
	public JEditorPane getjep()
	{
		if(jep == null)
		{
			jep = new JEditorPane();
			URL url = this.getClass().getResource("help/index.html");
			try {

				jep.setPage(url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jep.setEditable(false);
			jep.setContentType("text/html");
			jep.addHyperlinkListener(this);
			
		}
		return jep;
	}
	public void hyperlinkUpdate(HyperlinkEvent e) {
		// TODO Auto-generated method stub
		if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
		{
		    JEditorPane pane = (JEditorPane) e.getSource();
		    if (e instanceof HTMLFrameHyperlinkEvent) {
		      HTMLFrameHyperlinkEvent evt = (HTMLFrameHyperlinkEvent) e;
		      HTMLDocument doc = (HTMLDocument) pane.getDocument();
		      doc.processHTMLFrameHyperlinkEvent(evt);
		    } else {
		      try {
		        pane.setPage(e.getURL());
		      }
		      catch (Throwable t) {
		        t.printStackTrace();
		      }
		    }
		}
	}
}
