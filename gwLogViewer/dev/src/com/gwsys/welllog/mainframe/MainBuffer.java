/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.mainframe;

import java.io.IOException;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.gwsys.data.well.marker.MarkerInfo;
import com.gwsys.welllog.view.DesktopPaneView;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.projecttree.ProjectTreeView;
import com.gwsys.welllog.view.viewtree.ViewTree;
import com.gwsys.welllog.view.yaxis.DepthConverter;

public class MainBuffer implements Serializable{
	private static final long serialVersionUID = 1L;
	private boolean loadProject = false;
	private MainFrameView parentMainFrame = null;
	private ArrayList wellInfoList = new ArrayList();
	//private ArrayList filePathList = new ArrayList();
	//private ArrayList wellList = new ArrayList();
	private ArrayList allCheckShots = new ArrayList();
	private ArrayList allSurveys = new ArrayList();
	private ArrayList allWellTops = new ArrayList();
	private HashMap curveHashMap = new HashMap();	
	private HashMap checkshotHashMap = new HashMap();	
	private HashMap surveyHashMap = new HashMap();
	private InternalFrameView internalFrameView = null;
	private DepthConverter mDDepthConverter = null;
	private ArrayList<MarkerInfo> markerList = new ArrayList<MarkerInfo>();//June 30
	private HashMap<String, String> tmpLogFiles = new HashMap<String, String>();	//Alex. January 2007
	
	private static CurvePanel draggedCurve = null;	// for dragging curve
	
	private String defaultTemplate = null; 	// added by Alex. for manage templates
	
	public MainBuffer(MainFrameView parentMainFrame){
		this.parentMainFrame = parentMainFrame;
	}
	
	public MainFrameView getParentMainFrame() {
		return parentMainFrame;
	}

	public void serializeStaticState(ObjectOutputStream os) throws IOException{
		os.writeObject(curveHashMap);
	}
	public void deserializeStaticState(ObjectInputStream os) throws IOException, ClassNotFoundException{
		curveHashMap = (HashMap)os.readObject();
	}
	public HashMap getCurveHashMap() {
		return curveHashMap;
	}
	
	public ArrayList getAllCheckShots() {
		return allCheckShots;
	}
	public InternalFrameView getInternalFrameView() {
		return internalFrameView;
	}

	public DesktopPaneView getDesktopPaneView(){
		return this.parentMainFrame.getSonJSplitPane().getDesktopPaneView();
	}
	
	public ProjectTreeView getProjectTree() {
		return this.parentMainFrame.getSonJSplitPane().getPVTreePaneView().getProjectTreeView();
	}

	public ViewTree getViewTree() {
		return this.parentMainFrame.getSonJSplitPane().getPVTreePaneView().getViewTree();
	}
	public void setInternalFrameView(InternalFrameView internalFrameView) {
		this.internalFrameView = internalFrameView;
	}
	public HashMap getCheckshotHashMap() {
		return checkshotHashMap;
	}
	
	public HashMap getSurveyHashMap() {
		return surveyHashMap;
	}

	public DepthConverter getMDDepthConverter() {
		return mDDepthConverter;
	}

	public void setMDDepthConverter(DepthConverter depthConverter) {
		mDDepthConverter = depthConverter;
	}

	public ArrayList getAllSurveys() {
		return allSurveys;
	}

	public ArrayList getAllWellTops() {
		return allWellTops;
	}
	
	public ArrayList<MarkerInfo> getMarkerList() {
		return markerList;
	}

	public void setMarkerList(ArrayList<MarkerInfo> markerList) {
		this.markerList = markerList;
	}

	public boolean isLoadProject() {
		return loadProject;
	}

	public void setLoadProject(boolean loadProject) {
		this.loadProject = loadProject;
	}

	public ArrayList getWellInfoList() {
		return wellInfoList;
	}	
	
	/**
	 *  for dragging curve
	 */
	
	/**
	 * get previously dragged curve
	 */
	public CurvePanel mbGetDraggedCurve()	{
		return draggedCurve;		
	}
	
	/**
	 * set dragged curve
	 * @param cp curve panel to drag
	 */
	public void mbSetDraggedCurve(CurvePanel cp)	{
		draggedCurve = new CurvePanel(cp.getCd());
		draggedCurve.setCurveColor(cp.getCurveColor());
		draggedCurve.setLogarithm(cp.isLogarithm());
		draggedCurve.setStrokeWidth(cp.getStrokeWidth());
		draggedCurve.setMaxXDisplay(cp.getMaxXDisplay());
		draggedCurve.setMinXDisplay(cp.getMinXDisplay());
		draggedCurve.setStokeType(cp.getStokeType());
	}
	
	/**
	 * clear previously dragged curve
	 */
	public void mbClearDraggedCurve()	{
		draggedCurve = null;
	}
	
	/**
	 * for well templates
	 */
	
	/**
	 * clear current template
	 * @author Alex
	 */
	public void clearTemplate()	{
		this.defaultTemplate = null;
	}
	
	/**
	 * get current default template
	 * @return default template
	 * @author Alex
	 */
	public String getDefaultTemplate()	{
		return this.defaultTemplate;
	}
	
	/**
	 * set default template
	 * @param template name of default template
	 * @author Alex
	 */
	public void setDefaultTemplate(String template)	{
		this.defaultTemplate = template;
	}
	
	/**
	 * create an empty temp file
	 * @param source well name
	 * @return filepath newly created
	 * @author Alex
	 */
	public String createTmpLogFile(String source)	{
		String filepath = null;
		if (isThisLogExists(source))
			return getTmpLogFile(source);
				
		try {
			File tmpFile = File.createTempFile("logdata", ".tmp");
			filepath = tmpFile.getPath();
			this.tmpLogFiles.put(source, filepath);
			//System.out.println("created a new temp file: "+filepath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return filepath;
	}
	
	public String getTmpLogFile(String source)	{
		return this.tmpLogFiles.get(source);
	}
	
	public boolean isThisLogExists(String source)	{
		return this.tmpLogFiles.containsKey(source);
	}
	
	public boolean removeTmpLogFile(String wellName)	{
		if (this.tmpLogFiles.remove(wellName) != null)
			return true;
		
		return false;
	}
	
}
