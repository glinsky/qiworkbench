/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.mainframe;

import javax.swing.JSplitPane;

import com.gwsys.welllog.view.DesktopPaneView;
import com.gwsys.welllog.view.PVTreePaneView;
import com.gwsys.welllog.view.projecttree.ProjectTreeView;

public class SplitPaneView extends JSplitPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PVTreePaneView PVTreePaneView = null;
	private DesktopPaneView desktopPaneView = null;

	/**
	 * This method initializes PVTreePaneView	
	 * 	
	 * @return com.gwsys.welllog.Views.PVTreePaneView	
	 */
	public PVTreePaneView getPVTreePaneView() {
		if (PVTreePaneView == null) {
			PVTreePaneView = new PVTreePaneView();
		}
		return PVTreePaneView;
	}

	/**
	 * This method initializes desktopPaneView	
	 * 	
	 * @return com.gwsys.welllog.Views.DesktopPaneView	
	 */
	public DesktopPaneView getDesktopPaneView() {
		if (desktopPaneView == null) {
			desktopPaneView = new DesktopPaneView();
		}
		return desktopPaneView;
	}



	/**
	 * This is the default constructor
	 */
	public SplitPaneView() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(300, 200);
		this.setDividerLocation(200);
		this.setRightComponent(getDesktopPaneView());
		this.setLeftComponent(getPVTreePaneView());
	}
	
	public DesktopPaneView getRightDesktopPane() {
		return desktopPaneView;
	}
	
	public ProjectTreeView getLeftTree() {
		return PVTreePaneView.getProjectTreeView();
		
	}

}
