/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.welllog.mainframe;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.BevelBorder;

import com.gwsys.welllog.icons.IconResource;

/**
 * This is a status panel to show messages.
 *
 * @author Louis
 */
public class StatusBar extends JPanel {
	private static final long serialVersionUID = 1L;

	private JLabel message;

	private JLabel curveInfo;

	private JProgressBar progress;

	/**
	 * Constructor.
	 *
	 */
	public StatusBar() {
		super();
		this.setLayout(new GridLayout(1, 3));
		this.setBorder(new BevelBorder(BevelBorder.LOWERED));
		message = new JLabel("Ready", IconResource.getInstance().getImageIcon(
				IconResource.B_WELLICON), JLabel.LEFT);
		this.add(message);
		progress = new JProgressBar();
		progress.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		progress.setVisible(false);
		curveInfo = new JLabel();
		this.add(curveInfo);
		add(progress);
	}

	public void updateStatusBar(String Text) {
		message.setText(Text);
	}

	public void updateCurveInfo(String info) {
		curveInfo.setText(info);
	}

	public void showProgressBar(boolean vis){
		progress.setIndeterminate(vis);
		progress.setVisible(vis);
	}
}
