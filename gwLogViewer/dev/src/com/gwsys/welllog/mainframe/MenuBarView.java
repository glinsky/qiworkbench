/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp.
and distributed by BHP Billiton Petroleum under license.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License Version 2 as as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/
package com.gwsys.welllog.mainframe;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.gwsys.welllog.actions.AboutAction;
import com.gwsys.welllog.actions.AddInternalFrameAction;
import com.gwsys.welllog.actions.CreateFillCurveAction;
import com.gwsys.welllog.actions.CreateFillMarkerAction;
import com.gwsys.welllog.actions.CreateLithologyCurveAction;
import com.gwsys.welllog.actions.CreateMarkerAction;
//import com.gwsys.welllog.actions.CreateMeanp105090Action;
import com.gwsys.welllog.actions.CreateTrackAction;
import com.gwsys.welllog.actions.DepthAction;
import com.gwsys.welllog.actions.EditCurveAction;
import com.gwsys.welllog.actions.EditMarkerAction;
import com.gwsys.welllog.actions.EditTrackAction;
import com.gwsys.welllog.actions.ExitAction;
import com.gwsys.welllog.actions.HelpContentAction;
import com.gwsys.welllog.actions.ImportQiProject;
import com.gwsys.welllog.actions.ImportSurveyAction;
import com.gwsys.welllog.actions.LoadAnyAsciiAction;
import com.gwsys.welllog.actions.LoadAsciiAction;
import com.gwsys.welllog.actions.LoadBhpsuAction;
import com.gwsys.welllog.actions.LoadCheckShotsAction;
import com.gwsys.welllog.actions.LoadLasAction;
import com.gwsys.welllog.actions.LoadProjectAction;
import com.gwsys.welllog.actions.LoadWellTopsAction;
import com.gwsys.welllog.actions.LoadXmlAction;
import com.gwsys.welllog.actions.MinimizeAction;
import com.gwsys.welllog.actions.PageSetupAction;
import com.gwsys.welllog.actions.PrintAction;
import com.gwsys.welllog.actions.PrintPreviewAction;
import com.gwsys.welllog.actions.SaveProjectAction;
import com.gwsys.welllog.actions.TileWindowAction;
//import com.gwsys.welllog.actions.ShowDemoAction;
import com.gwsys.welllog.actions.TimeAction;
import com.gwsys.welllog.actions.ViewAllAction;
import com.gwsys.welllog.actions.ZoomInAction;
import com.gwsys.welllog.actions.ZoomOutAction;
import com.gwsys.welllog.entrance.MainMethod;
import com.gwsys.welllog.icons.IconResource;

/**
 * Create Menu Bar.
 * @author Team
 * @since Aug 10, 2006 Add print menus
 */
public class MenuBarView extends JMenuBar {

	// carga de ayuda // help system loader //START
	public static HelpBroker broker = null;

	public static HelpSet helpSet = null;

	public static void showHelp() {
		try {
			broker.setDisplayed(true);
		} catch (javax.help.UnsupportedOperationException uoe) {
			System.err.println("Couldn't show help: " + uoe);
		}
	}

	public void AttachHelp() {
		System.out.println("Connecting Help System");
		try {
			ClassLoader cl = MainMethod.class.getClassLoader();
			URL helpSetURL = HelpSet.findHelpSet(cl, "help/gwHelp.hs");
			helpSet = new HelpSet(cl, helpSetURL);
			broker = helpSet.createHelpBroker();
		} catch (Exception e) {
			System.err.println("Couldn't load help set: " + e);
			e.printStackTrace();
		}
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private JMenu jMenuFile = null;

	private JMenu jMenuEdit = null;
	
	private JMenu jMenuData = null;

	private JMenu jMenuAction = null;

	private JMenu jMenuHelp = null;

	private JMenu jMenuView = null;

	private MainFrameView jMainFrame = null;

	public MenuBarView(MainFrameView jMainFrame) {

		this.jMainFrame  = jMainFrame;

		AttachHelp();

		this.add(getMenuFile());
		this.add(getMenuEdit());
		this.add(getMenuData());
		this.add(getMenuView());
		this.add(getMenuAction());
		JMenu menu = getMenuRockPhysics();
		if (menu!=null)
			this.add(menu);  //added by Mario
		this.add(getMenuWindow());
		this.add(getMenuHelp());

	}

	private JMenu getMenuFile() {
		if (jMenuFile == null) {
			jMenuFile = new JMenu("File");
		}

		//JMenu loadMenu = new JMenu("Import Data");

		JMenuItem item = null;

		// New
		item = new JMenuItem("New Window");
		item.addActionListener(new AddInternalFrameAction(this.jMainFrame));
		jMenuFile.add(item);

		// Separator
		jMenuFile.insertSeparator(1);

		//print
		item = new JMenuItem("Print");
		item.setIcon(IconResource.getInstance().getImageIcon("print.gif"));
		item.addActionListener(new PrintAction(this.jMainFrame));
		jMenuFile.add(item);

		//print
		item = new JMenuItem("Print Preview");
		item.addActionListener(new PrintPreviewAction(this.jMainFrame));
		jMenuFile.add(item);

		item = new JMenuItem("Page Setup");
		item.addActionListener(new PageSetupAction(this.jMainFrame));
		jMenuFile.add(item);

		//Separator
		jMenuFile.addSeparator();
		
//		 Load
		/*JMenu loadWellLogs = new JMenu("Well Logs");
		loadMenu.add(loadWellLogs);

		JMenu loadWellTops = new JMenu("Well Tops");
		//item.addActionListener(new LoadWellTopsAction(this.jMainFrame));
		loadMenu.add(loadWellTops);

		item = new JMenuItem("Check Shots");
		item.addActionListener(new LoadCheckShotsAction(this.jMainFrame));
		loadMenu.add(item);

		item = new JMenuItem("Survey");
		item.addActionListener(new ImportSurveyAction());
		loadMenu.add(item);

		try{
			Class.forName("com.gwsys.qiComponent.main.QiProjectManager");
			item = new JMenuItem("From qiProjectManager");
			item.addActionListener(new ImportQiProject(this.jMainFrame));
			loadMenu.add(item);
		}catch(ClassNotFoundException e){

		}
		// Load Data
		item = new JMenuItem("LAS");
		item.addActionListener(new LoadLasAction(this.jMainFrame));
		loadWellLogs.add(item);

		item = new JMenuItem("LogXML");
		item.addActionListener(new LoadXmlAction(this.jMainFrame));
		loadWellLogs.add(item);

		item = new JMenuItem("LogAscII");
		item.addActionListener(new LoadAsciiAction(this.jMainFrame));
		loadWellLogs.add(item);

		item = new JMenuItem("BhpSU");
		item.addActionListener(new LoadBhpsuAction(this.jMainFrame));
		loadWellLogs.add(item);
		
		//load Well Tops
		item = new JMenuItem("G&W Format (.marker)");
		item.addActionListener(new LoadWellTopsAction(this.jMainFrame));
		loadWellTops.add(item);
		
		item = new JMenuItem("Any Text Format");
		item.addActionListener(new LoadAnyAsciiAction(jMainFrame));
		loadWellTops.add(item);
		

		jMenuFile.add(loadMenu);
		jMenuFile.addSeparator();
		jMenuFile.add(new JMenu("Export Data"));
		*/
		// Separator
		jMenuFile.addSeparator();

		// Load Project
		//item = new JMenuItem("Load Project");
		item = new JMenuItem("Import");
		item.addActionListener(new LoadProjectAction(this.jMainFrame));
		jMenuFile.add(item);

		// Save Project
		//item = new JMenuItem("Save Project");
		item = new JMenuItem("Export");
		item.addActionListener(new SaveProjectAction(this.jMainFrame));
		jMenuFile.add(item);

		// Separator
		jMenuFile.addSeparator();
		// Exit
		item = new JMenuItem();
		//item.setAction(new ExitAction("Exit", jMainFrame));
		item.setAction(new ExitAction("Quit", jMainFrame));
		jMenuFile.add(item);
		JMenuItem saveMenuItem = new JMenuItem("Save");
		saveMenuItem.setEnabled(false);
		jMenuFile.add(saveMenuItem);
		JMenuItem saveQuitMenuItem = new JMenuItem("Save, Quit");
		saveQuitMenuItem.setEnabled(false);
		jMenuFile.add(saveQuitMenuItem);

		return jMenuFile;
	}

	private JMenu getMenuData(){
		if (jMenuData == null) {
			jMenuData = new JMenu("Data");
		}

		JMenu loadMenu = new JMenu("Import");
		JMenuItem item = null;
//		 Load
		JMenu loadWellLogs = new JMenu("Well Logs");
		loadMenu.add(loadWellLogs);

		JMenu loadWellTops = new JMenu("Well Tops");
		//item.addActionListener(new LoadWellTopsAction(this.jMainFrame));
		loadMenu.add(loadWellTops);

		item = new JMenuItem("Check Shots");
		item.addActionListener(new LoadCheckShotsAction(this.jMainFrame));
		loadMenu.add(item);

		item = new JMenuItem("Survey");
		item.addActionListener(new ImportSurveyAction());
		loadMenu.add(item);

		try{
			Class.forName("com.gwsys.qiComponent.main.QiProjectManager");
			item = new JMenuItem("From qiProjectManager");
			item.addActionListener(new ImportQiProject(this.jMainFrame));
			loadMenu.add(item);
		}catch(ClassNotFoundException e){

		}
		// Load Data
		item = new JMenuItem("LAS");
		item.addActionListener(new LoadLasAction(this.jMainFrame));
		loadWellLogs.add(item);

		item = new JMenuItem("LogXML");
		item.addActionListener(new LoadXmlAction(this.jMainFrame));
		loadWellLogs.add(item);

		item = new JMenuItem("LogAscII");
		item.addActionListener(new LoadAsciiAction(this.jMainFrame));
		loadWellLogs.add(item);

		item = new JMenuItem("BhpSU");
		item.addActionListener(new LoadBhpsuAction(this.jMainFrame));
		loadWellLogs.add(item);
		
		//load Well Tops
		item = new JMenuItem("G&W Format (.marker)");
		item.addActionListener(new LoadWellTopsAction(this.jMainFrame));
		loadWellTops.add(item);
		
		item = new JMenuItem("Any Text Format");
		item.addActionListener(new LoadAnyAsciiAction(jMainFrame));
		loadWellTops.add(item);
		

		jMenuData.add(loadMenu);
		jMenuData.add(new JMenu("Export"));
		return jMenuData;
	}
	
	private JMenu getMenuEdit() {
		if (jMenuEdit == null) {
			jMenuEdit = new JMenu("Edit");
		}

		JMenuItem item = null;
		// edit track menu item
		item = new JMenuItem("Log");
		item.addActionListener(new EditTrackAction(this.jMainFrame));
		jMenuEdit.add(item);
		// edit curve menu item
		item = new JMenuItem("Curve");
		item.addActionListener(new EditCurveAction(this.jMainFrame));
		jMenuEdit.add(item);
		// edit marker menu item
		item = new JMenuItem("Marker");
		item.addActionListener(new EditMarkerAction(this.jMainFrame));
		jMenuEdit.add(item);

		return jMenuEdit;
	}

	private JMenu getMenuView() {
		  if (jMenuView == null) {
		   jMenuView = new JMenu("View");
		  }
		  JMenuItem item = null;
		  //create view all menu item
		  item = new JMenuItem("View All");
		  item.addActionListener(new ViewAllAction(this.jMainFrame));
		  jMenuView.add(item);
		  // create zoomin menu item
		  item = new JMenuItem("Zoom In");
		  item.addActionListener(new ZoomInAction(this.jMainFrame));
		  jMenuView.add(item);
		  // create zoomout menu item
		  item = new JMenuItem("Zoom Out");
		  item.addActionListener(new ZoomOutAction(this.jMainFrame));
		  jMenuView.add(item);
		  // convert y axis
		  JMenu subMenu = new JMenu("Vertical Axis");
		  // depth
		  item = new JMenuItem("Depth");
		  item.addActionListener(new DepthAction(this.jMainFrame));
		  subMenu.add(item);
		  // time
		  item = new JMenuItem("Time");
		  item.addActionListener(new TimeAction(this.jMainFrame));
		  subMenu.add(item);
		  jMenuView.add(subMenu);
		  return jMenuView;
		 }

	private JMenu getMenuAction() {
		if (jMenuAction == null) {
			jMenuAction = new JMenu("Action");
		}
		JMenuItem item = null;
		JMenu subMenu = null;
		// create track menu item
		item = new JMenuItem("Create Log");
		item.addActionListener(new CreateTrackAction(this.jMainFrame));
		jMenuAction.add(item);
		// create marker menu item
		item = new JMenuItem("Create Marker");
		item.addActionListener(new CreateMarkerAction(this.jMainFrame));
		jMenuAction.add(item);
/*
		item = new JMenuItem("Create Marker Cable");
		item.addActionListener(new CreateMarkerCableAction(this.jMainFrame));
		jMenuAction.add(item);
*/
		// create fill menu item
		subMenu = new JMenu("Create Fill");
		item = new JMenuItem("Curve");
		item.addActionListener(new CreateFillCurveAction(this.jMainFrame));
		subMenu.add(item);
		item = new JMenuItem("Marker");
		item.addActionListener(new CreateFillMarkerAction(this.jMainFrame));
		subMenu.add(item);
		jMenuAction.add(subMenu);

		// create Lithology menu item
		subMenu = new JMenu("Create Lithology");
		item = new JMenuItem("Curve");
		item.addActionListener(new CreateLithologyCurveAction(this.jMainFrame));
		subMenu.add(item);
		//item = new JMenuItem("Marker");
		//item.addActionListener(new CreateLithologyMarkerAction(this.jMainFrame));
		//subMenu.add(item);
		jMenuAction.add(subMenu);
/*
		item = new JMenuItem("Create Mean/P105090");
		item.addActionListener(new CreateMeanp105090Action(this.jMainFrame));
		jMenuAction.add(item);
		jMenuAction.addSeparator();
		item = new JMenuItem("Run Demo");
		item.addActionListener(new ShowDemoAction());
		jMenuAction.add(item);
		*/
		return jMenuAction;
	}

	private JMenu getMenuHelp() {
		if (jMenuHelp == null) {
			jMenuHelp = new JMenu("Help");
		}

		JMenuItem item = null;

		item = new JMenuItem();
		item.setAction(new HelpContentAction("Help Content"));
		jMenuHelp.add(item);
		item = new JMenuItem();
		item.setAction(new AboutAction(this.jMainFrame,"About"));
		jMenuHelp.add(item);

		return jMenuHelp;
	}

	private JMenu getMenuWindow() {
		JMenu window= new JMenu("Window");

		JMenuItem item = new JMenuItem();
		item.setAction(new MinimizeAction(jMainFrame,"Minimize All"));
		window.add(item);
		item = new JMenuItem();
		item.setAction(new TileWindowAction(jMainFrame,"Tile All"));
		window.add(item);

		return window;
	}

	private JMenu getMenuRockPhysics() {
		try {
			Class rock=Class.forName("com.gwsys.welllog.rockphysics.RockMenuBuilder");

			Method builder=rock.getMethod("buildMenu",
					Class.forName("com.gwsys.welllog.mainframe.MainFrameView"));

			Object menu = builder.invoke(new Object[]{jMainFrame}, new Object[]{jMainFrame});

			return (JMenu)menu;
		} catch (ClassNotFoundException e) {
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
		return null;
	}

}
