/*
gwLogViewer - a viewer to display and analyze well log data
This program module Copyright (C) 2006 G&W Systems Consulting Corp. 
and distributed by BHP Billiton Petroleum under license. 

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License Version 2 as as published 
by the Free Software Foundation.
 
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
or visit the link http://www.gnu.org/licenses/gpl.txt.

To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
or visit http://qiworkbench.org to learn more.
*/

package com.gwsys.welllog.mainframe;

public class CommandList {
	public static final String ADD_INTERNALFRAME = "AddInternalFrame";
	public static final String EXIT = "exit";
	public static final String OPEN_FILE = "openfile";
	public static final String LOAD_LAS = "loadlas";
	public static final String LOAD_XML = "loadxml";
	public static final String LOAD_ASCII = "loadascii";
	public static final String LOAD_BHPSU = "loadbhpsu";
	public static final String LOAD_WELL_TOPS = "loadwelltops";
	public static final String LOAD_CHECK_SHOTS = "loadcheckshots";
	public static final String PROJECT_LOAD = "loadproject";
	public static final String PROJECT_SAVE = "saveproject";
	public static final String EDIT_TRACK = "edittrack";
	public static final String EDIT_CURVE = "editcurve";
	public static final String EDIT_MARKER = "editmarker";
	public static final String CREATE_TRACK = "createtrack";
	public static final String CREATE_MARKER = "createmarker";
	public static final String CREATE_MARKER_CABLE = "createmarkercable";
	public static final String CREATE_Fill = "createfillcurve";
	public static final String CREATE_Fill_MARKER = "createfillmarker";
	public static final String CREATE_LITHOLOGY = "createlithologycurve";
	public static final String CREATE_LITHOLOGY_MARKER = "createlithologymarker";
	public static final String CREATE_P105090 = "createp105090";
	public static final String CREATE_MEANP105090 = "createmeanp105090";
	public static final String CREATE_MEAN = "createmean";
	public static final String CREATE_ZONE = "createzone";
	public static final String HELP_ABOUT = "helpabout";
	public static final String HELP_HELPCONTENT = "helpcontent";
	public static final String SAVE_TREE_XML = "savetreexml";
	public static final String LOAD_TREE_XML = "loadtreexml";
	public static final String VIEW_ALL = "viewall";
	public static final String ZOOM_IN = "zoomin";
	public static final String ZOOM_OUT = "zoomout";
	public static final String TIME = "time";
	public static final String DEPTH = "depth";
	public static final String SET_VISIBLE = "setvisible";
	public static final String SET_INVISIBLE = "setinvisible";
	public static final String SAVE_ZONES = "savezones"; //Mex
	public static final String CURVE_ASSIGNMENT = "curveassignment";
	public static final String WELL_DEFAULTS = "welldefaults";
	public static final String ROCK_PHYSICS_PROCESING = "rockphysicsprocesing";

}
