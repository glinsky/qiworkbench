/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.print;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

class PreviewCanvas extends JPanel{
	private static final long serialVersionUID = 1L;
	private Image img;

	public PreviewCanvas() {
		this.setBorder(new LineBorder(Color.BLACK));
		this.setBackground(Color.WHITE);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		if(img != null){
			Image showImage = getNeedImage(img, this.getWidth(),this.getHeight());
			g2.drawImage(showImage,0,0,showImage.getWidth(null),showImage.getHeight(null),null);
		}
	}

	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}

	private Image getNeedImage(Image img, int Width, int Height){
 		if(img == null){
 			return null;
 		}
 		BufferedImage bImg = new BufferedImage(Width,Height,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
 		g2.drawImage(img,0,0,Width,Height,null);
 		return bImg;
 	}
}
