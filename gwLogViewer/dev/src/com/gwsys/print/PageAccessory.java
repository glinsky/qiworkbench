/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.print;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

public class PageAccessory {
	public static int LEFT = 0;
	public static int CENTER = 1;
	public static int RIGHT = 2;
	public static final int HEIGHTLIMIT = 50;

	private String title = "";

	private Color color = Color.BLACK;

	private Font font = Font.getFont("System");

	private int size = 10;

	private int HeightLimit = HEIGHTLIMIT;

	private int textPosition = LEFT;

	private int iconPosition = LEFT;

	private Image iconImg ;

	public PageAccessory(){
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getHeightLimit() {
		return HeightLimit;
	}

	public void setHeightLimit(int heightLimit) {
		HeightLimit = heightLimit;
	}

	public Image getIconImg() {
		return iconImg;
	}

	public void setIconImg(Image iconImg) {
		this.iconImg = iconImg;
	}

	public int getIconPosition() {
		return iconPosition;
	}

	public void setIconPosition(int iconPosition) {
		this.iconPosition = iconPosition;
	}

	public int getTextPosition() {
		return textPosition;
	}

	public void setTextPosition(int textPosition) {
		this.textPosition = textPosition;
	}

}