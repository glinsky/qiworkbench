/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.print;

import java.awt.Image;
import java.awt.Rectangle;

public class PageLayout {
	private Image previewBackImage;
	private Image printBackImage;
	private Rectangle previewContentRect;
	private Rectangle printContentRect;
	private Rectangle previewPageRect;
	private Rectangle printPageRect;
	public Image getPreviewBackImage() {
		return previewBackImage;
	}
	public void setPreviewBackImage(Image previewBackImage) {
		this.previewBackImage = previewBackImage;
	}
	public Rectangle getPreviewContentRect() {
		return previewContentRect;
	}
	public void setPreviewContentRect(Rectangle previewContentRect) {
		this.previewContentRect = previewContentRect;
	}
	public Rectangle getPreviewPageRect() {
		return previewPageRect;
	}
	public void setPreviewPageRect(Rectangle previewPageRect) {
		this.previewPageRect = previewPageRect;
	}
	public Image getPrintBackImage() {
		return printBackImage;
	}
	public void setPrintBackImage(Image printBackImage) {
		this.printBackImage = printBackImage;
	}
	public Rectangle getPrintContentRect() {
		return printContentRect;
	}
	public void setPrintContentRect(Rectangle printContentRect) {
		this.printContentRect = printContentRect;
	}
	public Rectangle getPrintPageRect() {
		return printPageRect;
	}
	public void setPrintPageRect(Rectangle printPageRect) {
		this.printPageRect = printPageRect;
	}

}
