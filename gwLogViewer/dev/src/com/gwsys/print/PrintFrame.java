/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.print;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterGraphics;
import java.awt.print.PrinterJob;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.gwsys.welllog.view.DataBuffer;

public class PrintFrame extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;

	private JButton btnPrint;
	private JButton btnNextPage;
	private JButton btnPrevPage;
	private JButton btnMultiplePages;
	private JLabel lblScale;
	private JComboBox cbScale;
	private JButton btnClose;
	private JButton btnHeader;
	private JButton btnFooter;
	private JButton btnSave;

	private JPanel jButtonPanel = null;
	private JScrollPane jScrollPane = null;
	private JPanel previewPanel = null;

	private Image previewWindowImage;
	private Image printWindowImage;
	private ArrayList previewCanvaList = new ArrayList();
	private ArrayList previewImageList = new ArrayList();
	private ArrayList printImageList = new ArrayList();
	private ArrayList rectList = new ArrayList();
	private int currentPage = 0;

	private int originalWindowWidth = 0;
	private int originalWindowHeight = 0;
	private int printWidth = 0;
	private int printHeight = 0;
	private int previewWidth = 0;
	private int previewHeight = 0;

	public static final int BORDER_SPACE = 10;
	public static final int INTEVAL_SPACE = 5;
	public static final int CONVERSION_VALUE = 72;

	private int scale = SCALE_ORIGINAL;
	public static final int SCALE_ORIGINAL = 0;
	public static final int SCALE_FIT_WIDTH = 1;
	public static final int SCALE_FIT_HEIGHT = 2;
	public static final int SCALE_BEST_FIT = 3;

	private int preViewHeight = 0;
	private int preViewWidth = 0;

	private int showType = PrintFrame.SHOW_ONE_PAGE;
	private static final int SHOW_ONE_PAGE = 1;
	private static final int SHOW_ALL_PAGES = 2;

	public PrintFrame(){
		super(DataBuffer.getJMainFrame().getFrame(),"Print");
		Toolkit kit = this.getToolkit();
		Dimension dimension = kit.getScreenSize();
		preViewHeight = (int)dimension.getHeight()-75;
		preViewWidth = (int)dimension.getWidth()-25;
		this.setSize(dimension);
		this.setLayout();
	}

	public void setLayout(){
		 this.getContentPane().setLayout(null);
	     getButtonPanel();

	     jButtonPanel.setBounds(0,0,this.getWidth()-50,30);
	     this.getContentPane().add(jButtonPanel);

	     previewPanel = new JPanel();
	     previewPanel.setBackground(Color.GRAY);
	     previewPanel.setLayout(null);
	     previewPanel.setPreferredSize(new Dimension(preViewWidth,preViewHeight));

	     jScrollPane = new JScrollPane();
	     jScrollPane.setBounds(0,30,preViewWidth+20,preViewHeight+20);
	     jScrollPane.setViewportView(previewPanel);
	     jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	     jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		 this.getContentPane().add(jScrollPane);

		 if(ImageGenerator.pageLayout == null)
		 {
			 ImageGenerator.setPageLayout(true,1,1);
		 }
	}

	public void getButtonPanel(){
		if(jButtonPanel == null){
			jButtonPanel = new JPanel();
			jButtonPanel.setSize(new Dimension(preViewWidth+20,30));

			jButtonPanel.setLayout(null);

			btnPrint = new JButton("Print");
			btnPrint.setBounds(0,3,88,25);
			btnPrint.addActionListener(this);
			btnPrint.setActionCommand("Print");
			jButtonPanel.add(btnPrint);

			lblScale = new JLabel("Scale:");
			lblScale.setBounds(98,3,40,25);
			String[] Scale ={"Keep Original","Fit Width","Fit Height","Best Fit"};
			cbScale = new JComboBox(Scale);
			cbScale.setBounds(140,3,118,25);
			cbScale.addActionListener(new ActionListener()
					{
					public void actionPerformed(ActionEvent e){
						setScale(cbScale.getSelectedIndex());
						setImageInfo();
					}
					});
			jButtonPanel.add(lblScale);
			jButtonPanel.add(cbScale);

			btnHeader = new JButton("Header");
			btnHeader.setBounds(270,3,88,25);
			btnHeader.addActionListener(this);
			btnHeader.setActionCommand("Header");
			jButtonPanel.add(btnHeader);

			btnFooter = new JButton("Footer");
			btnFooter.setBounds(360,3,88,25);
			btnFooter.addActionListener(this);
			btnFooter.setActionCommand("Footer");
			jButtonPanel.add(btnFooter);

			btnNextPage = new JButton("Next Page");
			btnNextPage.setBounds(450,3,88,25);
			btnNextPage.addActionListener(this);
			btnNextPage.setActionCommand("NextPage");
			jButtonPanel.add(btnNextPage);

			btnPrevPage = new JButton("Prev Page");
			btnPrevPage.setBounds(540,3,88,25);
			btnPrevPage.addActionListener(this);
			btnPrevPage.setActionCommand("PrevPage");
			jButtonPanel.add(btnPrevPage);

			btnMultiplePages = new JButton("Multiple Pages");
			btnMultiplePages.setBounds(630,3,118,25);
			btnMultiplePages.addActionListener(this);
			btnMultiplePages.setActionCommand("MultiplePages");
			jButtonPanel.add(btnMultiplePages);

			btnClose = new JButton("Close");
			btnClose.setBounds(750,3,88,25);
			btnClose.addActionListener(this);
			btnClose.setActionCommand("Close");
			jButtonPanel.add(btnClose);

			btnSave = new JButton("Save");
			btnSave.setBounds(850,3,88,25);
			btnSave.addActionListener(this);
			btnSave.setActionCommand("Save");
			//jButtonPanel.add(btnSave);

			setButtonStats();
		}
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Print")){
			printImage();
		}else if(e.getActionCommand().equals("Header")){
			PageHeaderFooterSettingDialog headerDialog = new PageHeaderFooterSettingDialog(this,PageHeaderFooterSettingDialog.PAGE_HEADER);
			headerDialog.setVisible(true);
		}else if(e.getActionCommand().equals("Footer")){
			PageHeaderFooterSettingDialog footerDialog = new PageHeaderFooterSettingDialog(this,PageHeaderFooterSettingDialog.PAGE_FOOTER);
			footerDialog.setVisible(true);
		}else if(e.getActionCommand().equals("NextPage")){
			if(this.currentPage < (this.previewImageList.size() -1)){
				this.currentPage++;
				setButtonStats();
				this.showPreviewCanvas();
			}
		}else if(e.getActionCommand().equals("PrevPage")){
			if(this.currentPage > 0 ){
				this.currentPage--;
				setButtonStats();
				this.showPreviewCanvas();
			}
		}else if(e.getActionCommand().equals("MultiplePages")){
			if(this.getShowType() == PrintFrame.SHOW_ONE_PAGE){
				this.setShowType(PrintFrame.SHOW_ALL_PAGES);
			}else{
				this.setShowType(PrintFrame.SHOW_ONE_PAGE);
			}
			this.setImageInfo();
			setButtonStats();
			//setRectList();
			this.showPreviewCanvas();
		}else if(e.getActionCommand().equals("Close")){
			this.setVisible(false);
			this.dispose();
			return;
		}else if(e.getActionCommand().equals("Save")){
			if(previewImageList.size() > 0){
				BufferedImage saveImage =(BufferedImage)(printImageList.get(currentPage));
				ImageSave imageSave = new ImageSave();
				imageSave.SaveImage(saveImage);
			}
		}
	}

	private void setButtonStats(){
		if(this.getShowType() == PrintFrame.SHOW_ONE_PAGE){
			btnMultiplePages.setText("Multiple Pages");
			if(this.currentPage == 0){
				btnPrevPage.setEnabled(false);
			}else{
				btnPrevPage.setEnabled(true);
			}
			if(this.currentPage < this.previewImageList.size() - 1){
				btnNextPage.setEnabled(true);
			}else{
				btnNextPage.setEnabled(false);
			}
		}else{
			btnMultiplePages.setText("One Page");
			btnNextPage.setEnabled(false);
			btnPrevPage.setEnabled(false);
		}
	}

	public void setImageInfo(){
		this.previewPanel.repaint();
		this.makePrintWindowImage();
		this.makePreWindowImage();
		showPreviewCanvas();
		setButtonStats();
	}

	public void changeHeaderFooter(){
		ImageGenerator.setPageLayout(true,1,1);
		Rectangle rect = (Rectangle)this.getRectList().get(0);
		ImageGenerator.setPageLayout(false,(float)(rect.width / ImageGenerator.pageLayout.getPrintPageRect().getWidth()),
				(float)(rect.width / ImageGenerator.pageLayout.getPrintPageRect().getWidth()));
	}

	private void makePrintWindowImage()
	{
		if(this.getScale() == PrintFrame.SCALE_ORIGINAL){
			this.printWidth = this.originalWindowWidth ;
			this.printHeight = this.originalWindowHeight ;
		}else if(this.getScale() == PrintFrame.SCALE_FIT_WIDTH){
			this.printWidth = (int)ImageGenerator.pageLayout.getPrintContentRect().getWidth();
			this.printHeight = (int)(this.printWidth * ((float)this.originalWindowHeight / (float)this.originalWindowWidth ));
		}else if(this.getScale() == PrintFrame.SCALE_FIT_HEIGHT){
			this.printHeight = (int)(ImageGenerator.pageLayout.getPrintContentRect().getHeight());
			this.printWidth = (int)(this.printHeight * ((float)this.originalWindowWidth / (float)this.originalWindowHeight));
		}else if(this.getScale() == PrintFrame.SCALE_BEST_FIT)
		{
			this.printWidth = (int)(ImageGenerator.pageLayout.getPrintContentRect().getWidth());
			this.printHeight = (int)(ImageGenerator.pageLayout.getPrintContentRect().getHeight());
		}
		float rateWidth = (float)this.printWidth /(float)this.originalWindowWidth;
		float rateHeight = (float)this.printHeight / (float)this.originalWindowHeight;
		this.setPrintWindowImage(ImageGenerator.getWindowImage(this.originalWindowWidth,this.originalWindowHeight,rateWidth, rateHeight));
		this.setPrintImageList(ImageGenerator.getImageList(this.printWindowImage,
				(int)ImageGenerator.pageLayout.getPrintContentRect().getWidth(),
				(int)ImageGenerator.pageLayout.getPrintContentRect().getHeight(),true));
	}
	private void makePreWindowImage(){
		this.setRectList();
		Rectangle rect = (Rectangle)this.getRectList().get(0);
		float ratePrintPreview = (float)rect.getWidth() / (float)ImageGenerator.pageLayout.getPrintPageRect().getWidth();

		ImageGenerator.setPageLayout(false,ratePrintPreview,ratePrintPreview);

		if(this.getScale() == PrintFrame.SCALE_ORIGINAL){
			this.previewWidth = (int)(this.printWidth*ratePrintPreview);
			this.previewHeight = (int)(this.printHeight*ratePrintPreview );
		}else if(this.getScale() == PrintFrame.SCALE_FIT_WIDTH){
			this.previewWidth = (int)(ImageGenerator.pageLayout.getPreviewContentRect().getWidth());
			this.previewHeight = (int)(this.previewWidth *((float)this.originalWindowHeight/(float)this.originalWindowWidth));
		}else if(this.getScale() == PrintFrame.SCALE_FIT_HEIGHT){
			this.previewHeight = (int)(ImageGenerator.pageLayout.getPreviewContentRect().getHeight());
			this.previewWidth = (int)(this.previewHeight *((float)this.originalWindowWidth /(float)this.originalWindowHeight ));
		}else if(this.getScale() == PrintFrame.SCALE_BEST_FIT)
		{
			this.previewWidth = (int)(ImageGenerator.pageLayout.getPreviewContentRect().getWidth());
			this.previewHeight = (int)(ImageGenerator.pageLayout.getPreviewContentRect().getHeight());
		}

		float rateWidth = (float)this.previewWidth /(float)this.originalWindowWidth;
		float rateHeight = (float)this.previewHeight / (float)this.originalWindowHeight;

		this.setPreviewWindowImage(ImageGenerator.getWindowImage(this.originalWindowWidth,this.originalWindowHeight,rateWidth, rateHeight));

		this.setPreviewImageList(ImageGenerator.getImageList(this.previewWindowImage,
				(int) (ImageGenerator.pageLayout.getPreviewContentRect().getWidth()),
				(int)(ImageGenerator.pageLayout.getPreviewContentRect().getHeight()),false));
		this.getPreviewCanvas();
	}


	@SuppressWarnings("unchecked")
	public void getPreviewCanvas(){
		this.previewCanvaList.clear();
		if(this.previewImageList.size() == 0){
			this.previewCanvaList.add(new PreviewCanvas());
		}else{
			for(int i = 0; i < previewImageList.size(); i++){
				PreviewCanvas canvas = new PreviewCanvas();
				canvas.setImg((Image)previewImageList.get(i));
				this.previewCanvaList.add(canvas);
			}
		}
		this.currentPage = 0;
	}

	public ArrayList getRectList() {
		return rectList;
	}

	@SuppressWarnings("unchecked")
	public void setRectList() {
		rectList.clear();

		int maxWidth = preViewWidth - PrintFrame.BORDER_SPACE*2;
		int maxHeight = preViewHeight - PrintFrame.BORDER_SPACE*2;

		int pageWidth = (int)ImageGenerator.pageLayout.getPrintPageRect().getWidth() ;
		int pageHeight = (int)ImageGenerator.pageLayout.getPrintPageRect().getHeight();

		int panelWidth = 0;
		int panelHeight = 0;
		int panelTop = 0;
		int panelLeft = 0;
		if(this.getShowType() == PrintFrame.SHOW_ONE_PAGE ){
			if((float)maxHeight/(float)maxWidth > (float)pageHeight/(float)pageWidth){
				panelWidth = maxWidth;
				panelHeight = (int)(panelWidth * ((float)pageHeight / (float)pageWidth));
				panelTop = PrintFrame.BORDER_SPACE + (maxHeight - panelHeight)/2;
				panelLeft = PrintFrame.BORDER_SPACE;
			}else{
				panelHeight = maxHeight;
				panelWidth = (int)(panelHeight * ((float)pageWidth / (float)pageHeight));
				panelTop = PrintFrame.BORDER_SPACE;
				panelLeft = PrintFrame.BORDER_SPACE + (maxWidth - panelWidth)/2;
			}
			Rectangle rect = new Rectangle(panelLeft,panelTop,panelWidth,panelHeight);
			this.getRectList().add(rect);

		}else{
			int totalPages = this.printImageList.size();
			int rows = 1;
			int cols = totalPages / rows;
			for(;;){
				panelHeight =(int)((maxHeight - PrintFrame.INTEVAL_SPACE * (rows -1)) / rows);
				panelWidth = (int)(panelHeight * ((float)pageWidth / (float)pageHeight));
				int rowWidth = panelWidth * cols + PrintFrame.INTEVAL_SPACE * (cols -1);
				if(rowWidth < maxWidth){
					break;
				}
				rows ++;
				cols = totalPages/rows;
				if(cols < ((float)totalPages/(float)rows)){
					cols ++;
				}
				if(rows > cols){
					rows --;
					cols = totalPages/rows;
					if(cols < ((float)totalPages/(float)rows)){
						cols ++;
					}
					panelWidth = (int)((maxWidth - PrintFrame.INTEVAL_SPACE * (cols -1))/cols);
					panelHeight = (int)(panelWidth * ((float)pageHeight / (float)pageWidth));
					break;
				}
			}
			int left = (int)PrintFrame.BORDER_SPACE +((maxWidth -(panelWidth * cols + PrintFrame.INTEVAL_SPACE * (cols -1)))/2);
			int top = (int)PrintFrame.BORDER_SPACE + ((maxHeight - (panelHeight * rows + PrintFrame.INTEVAL_SPACE * (rows -1)))/2);
			for(int i = 0; i < rows; i++){
				for(int j =0; j < cols; j++){
					panelTop = top + (panelHeight + PrintFrame.INTEVAL_SPACE) * i;
					panelLeft = left + (panelWidth + PrintFrame.INTEVAL_SPACE)* j;
					Rectangle rect = new Rectangle(panelLeft,panelTop,panelWidth,panelHeight);
					this.getRectList().add(rect);
				}
			}
		}
	}

	public void repaintAll(){
		if(this.getShowType() == PrintFrame.SHOW_ONE_PAGE){
			((PreviewCanvas)previewCanvaList.get(this.currentPage)).repaint();
		}else{
			for(int i =0; i < previewCanvaList.size(); i++){
				((PreviewCanvas)previewCanvaList.get(i)).repaint();
			}
		}
	}

	public void showPreviewCanvas(){
		previewPanel.removeAll();
		previewPanel.repaint();

		int end = 0;
		int start = 0;
		if(this.getShowType() == PrintFrame.SHOW_ONE_PAGE){
			start = this.currentPage;
			end = this.currentPage;
		}else{
			start = 0;
			end = this.previewCanvaList.size()-1;
		}
		int j = 0;
		for(int i = start; i < end+1; i++){
			PreviewCanvas canvas = (PreviewCanvas) this.previewCanvaList.get(i);
			Rectangle rect = (Rectangle)this.getRectList().get(j);
			canvas.setPreferredSize(new Dimension((int)rect.getWidth(),(int)rect.getHeight()));
			canvas.setBounds((int)rect.getX(),(int)rect.getY(),(int)rect.getWidth(),(int)rect.getHeight());
			previewPanel.add(canvas);
			canvas.repaint();
			j++;
		}
	}

	public void printImage(){
		PrinterJob printerJob = PrinterJob.getPrinterJob();
		Book bk = new Book();
		for(int i = 0; i < this.printImageList.size(); i++){
			Painter painter = new Painter(i);
			bk.append(painter,ImageGenerator.pageFormat);
		}
		printerJob.setPageable(bk);

		if (!printerJob.printDialog())
        {
            return;
        }
		int printCopy = printerJob.getCopies();
		for(int i = 0; i < printCopy; i++){
			try
	        {
	        	printerJob.print();
	        }catch(PrinterException pe)
	        {
	            pe.printStackTrace();
	        }
		}
		printerJob = null;
	}

	public int getOriginalWindowHeight() {
		return originalWindowHeight;
	}

	public void setOriginalWindowHeight(int originalWindowHeight) {
		this.originalWindowHeight = originalWindowHeight;
	}

	public int getOriginalWindowWidth() {
		return originalWindowWidth;
	}

	public void setOriginalWindowWidth(int originalWindowWidth) {
		this.originalWindowWidth = originalWindowWidth;
	}

	public Image getPreviewWindowImage() {
		return previewWindowImage;
	}

	public void setPreviewWindowImage(Image previewWindowImage) {
		this.previewWindowImage = previewWindowImage;
	}

	public Image getPrintWindowImage() {
		return printWindowImage;
	}

	public void setPrintWindowImage(Image printWindowImage) {
		this.printWindowImage = printWindowImage;
	}

	public JPanel getPreviewPanel() {
		return previewPanel;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public ArrayList getPreviewImageList() {
		return previewImageList;
	}

	public void setPreviewImageList(ArrayList previewImageList) {
		this.previewImageList = previewImageList;
	}

	public ArrayList getPrintImageList() {
		return printImageList;
	}

	public void setPrintImageList(ArrayList printImageList) {
		this.printImageList = printImageList;
	}

	public int getShowType() {
		return showType;
	}

	public void setShowType(int showType) {
		this.showType = showType;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	class Painter implements Printable {
		private int currentPage;
		public Painter(int currentPage){
			this.currentPage = currentPage;
		}

		public int print(Graphics g, PageFormat pf, int iPage) throws PrinterException {
			try {
    			Graphics2D g2 = (Graphics2D) g;
    			PrinterJob prjob = ((PrinterGraphics) g2).getPrinterJob();
    			PageFormat newPage = (PageFormat)ImageGenerator.pageFormat.clone();
    			Paper paper = newPage.getPaper();
    			paper.setImageableArea(20,20,paper.getWidth()-40,paper.getHeight()-40);
    			newPage.setPaper(paper);
    			prjob.setPrintable(this,newPage);
    			Image printImage = (Image)(printImageList.get(currentPage));
    			int imgWidth = printImage.getWidth(null);
    			int imgHeight = printImage.getHeight(null);
    			g2.drawImage(printImage, 0, 0, imgWidth, imgHeight,null);

    		} catch (Exception ex) {
    			   throw new PrinterException(ex.getMessage());
    		}
    		return PAGE_EXISTS;
    	}
    }

}
