/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.print;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.PageAttributes;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import com.gwsys.welllog.core.Renderable;
import com.gwsys.welllog.view.DataBuffer;
import com.gwsys.welllog.view.InternalFrameView;
import com.gwsys.welllog.view.curve.CurveGroup;
import com.gwsys.welllog.view.curve.CurvePanel;
import com.gwsys.welllog.view.curve.CurveView;
import com.gwsys.welllog.view.curve.DescriptionObject;
import com.gwsys.welllog.view.curve.DescriptionPanel;
import com.gwsys.welllog.view.curve.LithologyPanel;
import com.gwsys.welllog.view.curve.PanelOrder;
import com.gwsys.welllog.view.curve.SeismicPanel;
import com.gwsys.welllog.view.track.AxisCurveContainerView;
import com.gwsys.welllog.view.track.XAxisView;
import com.gwsys.welllog.view.track.YAxisView;

public class ImageGenerator {

	public static PageFormat pageFormat;
	public static PageAccessory pageHeader;
	public static PageAccessory pageFooter;
	public static PageLayout pageLayout;

	public static Image getWindowImage(int windowImageWidth, int windowImageHeight, float rateWidth, float rateHeight){
		int windowWidth = (int)(windowImageWidth * rateWidth);
		int windowHeight = (int)(windowImageHeight * rateHeight);

		BufferedImage bImg = new BufferedImage(windowWidth,windowHeight,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		g2.setColor(Color.white);
		g2.fillRect(0,0,windowWidth,windowHeight);
		g2.setBackground(Color.white);

		InternalFrameView selectedInternalFrameView = DataBuffer.getJMainFrame().getMainBuffer().getInternalFrameView();
		ArrayList trackList = selectedInternalFrameView.getTrackContainer().getAxisCurveContainerList();

		if(trackList.size() == 0){
			return bImg;
		}

		Image[] trackImage = new Image[trackList.size()];
		for(int i = 0; i < trackList.size(); i++){
			AxisCurveContainerView track = (AxisCurveContainerView)trackList.get(i);
			int trackLeft = (int)(track.getX() * rateWidth);
			if(track.getHeight() > windowHeight){
				windowHeight = track.getHeight();
			}
			trackImage[i] = getTrackImage(track,rateWidth,rateHeight);
			int trackWidth = trackImage[i].getWidth(null);
			int trackHeight = trackImage[i].getHeight(null);
			g2.drawImage(trackImage[i],trackLeft,0,trackWidth,trackHeight,null);
		}
		g2.setFont(ImageGenerator.getFontSize(g2, new Font("SansSerif", Font.PLAIN, 12),
				rateWidth,rateHeight));
		selectedInternalFrameView.getTrackContainer().print(g2,rateWidth,rateHeight);

		return bImg;
	}

	public static Image getTrackImage(AxisCurveContainerView track, float rateWidth, float rateHeight){
		int trackWidth = (int)(track.getWidth()*rateWidth)+4;
		int trackHeight = (int)(track.getHeight()*rateHeight)+2;
		BufferedImage bImg = new BufferedImage(trackWidth,trackHeight,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setColor(Color.white);
		g2.fillRect(0,0,trackWidth,trackHeight);

		g2.setBackground(Color.white);

		int titleHeight = (int)((track.getJlb().getHeight()*2-1)*rateHeight);
		g2.setColor(track.getTopScrollPane().getTopComponent().getBackground());
		g2.fillRect(1,1,trackWidth-7,titleHeight);
		Font newFont = getFontByHSpace(g2, titleHeight,rateWidth,rateHeight);
		g2.setFont(newFont);
		g2.setColor(Color.white);
		int stringLenth = g2.getFontMetrics().stringWidth(track.getTrackName());
		g2.drawString(track.getTrackName(), (trackWidth - stringLenth)/2,titleHeight/2+(int)(5*rateHeight));

		Image XImage = getNeedXImage(track,rateWidth,rateHeight);
		g2.drawImage(XImage,1,titleHeight+3,XImage.getWidth(null),XImage.getHeight(null),null);

		int xHeight = (int)(track.getTopScrollPane().getBottomComponent().getHeight()*rateHeight);
		g2.setColor(SystemColor.lightGray);
		g2.fillRect(1,titleHeight + xHeight +1,trackWidth-5,(int)(5*rateHeight));

		g2.setColor(Color.blue);
		g2.drawRect(1,titleHeight+(int)(3*rateHeight),XImage.getWidth(null), xHeight -(int)(3*rateHeight));


		int curveTop = titleHeight + xHeight + (int)(6*rateHeight);
		Image curveImage = getNeedCurveImage(track,rateWidth,rateHeight);
		g2.drawImage(curveImage,1,curveTop,curveImage.getWidth(null),curveImage.getHeight(null),null);

		g2.setColor(SystemColor.black);
		g2.drawRect(1,curveTop,curveImage.getWidth(null),curveImage.getHeight(null));

		g2.drawRect(1,1,trackWidth-7,trackHeight-5);

		return bImg;
	}

	public static Image getNeedCurveImage(AxisCurveContainerView track, float rateWidth, float rateHeight){
		Image curveImage = getCurveViewImage(track.getCurveView(),rateWidth,rateHeight);
		if(curveImage == null){
			return null;
		}
		int width = (int)(track.getBottomScrollPane().getWidth()*rateWidth);
		int height = (int)(track.getBottomScrollPane().getHeight()*rateHeight);
		BufferedImage bImg = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setColor(Color.white);
		g2.fillRect(0,0,width,height);

		g2.setBackground(Color.white);

		int top = -(int)(track.getBottomScrollPane().getVerticalScrollBar().getValue()*rateWidth);
		int left = -(int)(track.getBottomScrollPane().getHorizontalScrollBar().getValue()*rateWidth);

		g2.drawImage(curveImage,left,top,curveImage.getWidth(null),curveImage.getHeight(null),null);

		PanelOrder panelOrder = track.getCurveView().getPanelOrder();
		g2.setColor(Color.black);
		width = 0;
		for(int i = 0 ; i < panelOrder.getOrders().size(); i++){
			if(panelOrder.getPanel(i) instanceof YAxisView){
				YAxisView yAxisView = (YAxisView)panelOrder.getPanel(i);
				g2.drawRect(width,0,(int)(yAxisView.getWidth()*rateWidth),curveImage.getHeight(null));
				width = width + (int)(yAxisView.getWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof CurvePanel){
				CurvePanel cp = (CurvePanel)panelOrder.getPanel(i);
				g2.drawRect(width,0,(int)(cp.getBesideTypeWidth()*rateWidth),curveImage.getHeight(null));
				width = width + (int)(cp.getBesideTypeWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof CurveGroup){
				CurveGroup cg = (CurveGroup)panelOrder.getPanel(i);
				g2.drawRect(width,0,(int)(cg.getBesideTypeWidth()*rateWidth),curveImage.getHeight(null));
				width = width + (int)(cg.getBesideTypeWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof DescriptionPanel){
				DescriptionPanel descriptionPanel = (DescriptionPanel)panelOrder.getPanel(i);
				g2.drawRect(width,0,(int)(descriptionPanel.getWidth()*rateWidth),curveImage.getHeight(null));
				width = width + (int)(descriptionPanel.getWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof SeismicPanel){
				SeismicPanel seismicPanel = (SeismicPanel)panelOrder.getPanel(i);
				g2.drawRect(width,0,(int)(seismicPanel.getWidth()*rateWidth),curveImage.getHeight(null));
				width = width + (int)(seismicPanel.getWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof LithologyPanel){
				LithologyPanel lithologyPanel = (LithologyPanel)panelOrder.getPanel(i);
				g2.drawRect(width,0,(int)(lithologyPanel.getWidth()*rateWidth),curveImage.getHeight(null));
				width = width + (int)(lithologyPanel.getWidth()*rateWidth);
			}
		}



		return bImg;
	}

	public static Image getCurveViewImage(CurveView cv,  float rateWidth, float rateHeight){
		int cvWidth = (int)(cv.getWidth()*rateWidth);
		int cvHeight = (int)(cv.getHeight()*rateHeight);
		BufferedImage bImg = new BufferedImage(cvWidth,cvHeight,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setColor(Color.white);
		g2.fillRect(0,0,cvWidth,cvHeight);

		g2.setBackground(Color.white);

		g2.setFont(getFontSize(g2, new Font("SansSerif", Font.PLAIN, 11),rateWidth,rateHeight));

		PanelOrder panelOrder = cv.getPanelOrder();
		Rectangle rect = new Rectangle(0,0,cvWidth,cvHeight);

		cv.getCurveLayer().calculateShape(rateWidth, rateHeight);

		if (cv.getCurveLayer() instanceof Renderable){
			cv.getCurveLayer().setPaintLine(false);

			((Renderable)cv.getCurveLayer()).print(g2,rect,rateWidth, rateHeight);
		}

        // marker
		if (cv.getMarkerLayer() instanceof Renderable){
			((Renderable)cv.getMarkerLayer()).print(g2,rect,rateWidth, rateHeight);
		}

        // grid
		if (cv.getGridLayer() instanceof Renderable){
			((Renderable)cv.getGridLayer()).print(g2,rect,rateWidth, rateHeight);
		}

		if (cv.getCurveLayer() instanceof Renderable){
			cv.getCurveLayer().setPaintLine(true);
			((Renderable)cv.getCurveLayer()).print(g2,rect,rateWidth, rateHeight);
		}

		if (cv.getZoneLayer() instanceof Renderable){
			((Renderable)cv.getZoneLayer()).print(g2,rect,rateWidth, rateHeight);
		}

		cv.getMarkerLayer().drawMarker(g2, cv,cv.getWidth(),rateWidth,rateHeight);

		int width = 0;
		for(int i = 0 ; i < panelOrder.getOrders().size(); i++){
			if(panelOrder.getPanel(i) instanceof YAxisView){
				YAxisView yAxisView = (YAxisView)panelOrder.getPanel(i);
				g2.clearRect(width+1,0,(int)(yAxisView.getWidth()*rateWidth)-2,(int)(yAxisView.getHeight()*rateHeight));
				g2.setColor(Color.white);
				g2.fillRect(width+2,1,(int)(yAxisView.getWidth()*rateWidth)-4,
						(int)(yAxisView.getHeight()*rateHeight)-2);
				yAxisView.print(g2,width,rateWidth,rateHeight);
				width = width + (int)(yAxisView.getWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof CurvePanel){
				CurvePanel cp = (CurvePanel)panelOrder.getPanel(i);
				width = width + (int)(cp.getBesideTypeWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof CurveGroup){
				CurveGroup cg = (CurveGroup)panelOrder.getPanel(i);
				width = width + (int)(cg.getBesideTypeWidth()*rateWidth);
			}else if(panelOrder.getPanel(i) instanceof DescriptionPanel){
				DescriptionPanel descriptionPanel = (DescriptionPanel)panelOrder.getPanel(i);
				g2.clearRect(width+1,0,(int)(descriptionPanel.getWidth()*rateWidth)-2,(int)(descriptionPanel.getHeight()*rateHeight));
				g2.setColor(Color.white);
				g2.fillRect(width+2,1,(int)(descriptionPanel.getWidth()*rateWidth)-4,(int)(descriptionPanel.getHeight()*rateHeight)-2);

				Image descriptionImage = getDescriptionImage(descriptionPanel,rateWidth, rateHeight);

				g2.drawImage(descriptionImage,width,0,descriptionImage.getWidth(null),descriptionImage.getHeight(null),null);

				width = width + (int)(descriptionPanel.getWidth()*rateWidth);

			}else if(panelOrder.getPanel(i) instanceof SeismicPanel){
				SeismicPanel seismicPanel = (SeismicPanel)panelOrder.getPanel(i);
				g2.clearRect(width+1,0,(int)(seismicPanel.getWidth()*rateWidth)-2,(int)(seismicPanel.getHeight()*rateHeight));
				g2.setColor(Color.white);
				g2.fillRect(width+1,1,(int)(seismicPanel.getWidth()*rateWidth)-4,(int)(seismicPanel.getHeight()*rateHeight)-2);

				seismicPanel.print(g2,width,rateWidth,rateHeight);

				width = width + (int)(seismicPanel.getWidth()*rateWidth);

			}else if(panelOrder.getPanel(i) instanceof LithologyPanel){
				LithologyPanel lithologyPanel = (LithologyPanel)panelOrder.getPanel(i);
				g2.clearRect(width+1,0,(int)(lithologyPanel.getWidth()*rateWidth)-2,(int)(lithologyPanel.getHeight()*rateHeight));
				g2.setColor(Color.white);
				g2.fillRect(width+2,1,(int)(lithologyPanel.getWidth()*rateWidth)-4,(int)(lithologyPanel.getHeight()*rateHeight)-2);

				lithologyPanel.print(g2,width,rateWidth,rateHeight);

				width = width + (int)(lithologyPanel.getWidth()*rateWidth);

			}
		}

		return bImg;
	}

	public static Image getDescriptionImage(DescriptionPanel descriptionPanel,  float rateWidth, float rateHeight){
		int width = (int)(descriptionPanel.getWidth()*rateWidth);
		int height = (int)(descriptionPanel.getHeight()*rateHeight);
		BufferedImage bImg = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setColor(Color.white);
		g2.fillRect(0,0,width,height);

		g2.setBackground(Color.white);

		for(int i = 0; i < descriptionPanel.getObjectList().size(); i++){
			DescriptionObject object = (DescriptionObject)descriptionPanel.getObjectList().get(i);
			if(object.getControlType() == DescriptionObject.CONTROL_IMAGE_LABEL){
				JLabel label = object.getLbImage();
				Image img = ((ImageIcon)label.getIcon()).getImage();
				g2.drawImage(img,(int)(label.getX()*rateWidth),(int)(label.getY()*rateHeight),
						(int)(label.getWidth()*rateWidth),(int)(label.getHeight()*rateHeight),null);
			}
		}
		for(int i = 0; i < descriptionPanel.getObjectList().size(); i++){
			DescriptionObject object = (DescriptionObject)descriptionPanel.getObjectList().get(i);
			if(object.getControlType() == DescriptionObject.CONTROL_TEXT_LABEL){
				JLabel label = object.getLbImage();
				Image img = ((ImageIcon)label.getIcon()).getImage();
				g2.drawImage(img,(int)(label.getX()*rateWidth),(int)(label.getY()*rateHeight),
						(int)(label.getWidth()*rateWidth),(int)(label.getHeight()*rateHeight),null);
			}
		}
		return bImg;
	}


	public static Image getNeedXImage(AxisCurveContainerView track,  float rateWidth, float rateHeight){
		Image xImage = getXImage(track,rateWidth,rateHeight);
		if(xImage == null){
			return null;
		}
		int width = (int)(track.getTitleAndCurveNamesPanel().getWidth()*rateWidth);
		int height = (int)(track.getTitleAndCurveNamesPanel().getHeight()*rateHeight);
		BufferedImage bImg = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setColor(Color.white);
		g2.fillRect(0,0,width,height);

		g2.setBackground(Color.white);

		int top = -(int)(track.getTitleAndCurveNamesPanel().getVerticalScrollBar().getValue()*rateHeight);
		int left = -(int)(track.getTitleAndCurveNamesPanel().getHorizontalScrollBar().getValue()*rateWidth);

		g2.drawImage(xImage,left,top,xImage.getWidth(null),xImage.getHeight(null),null);

		g2.setColor(Color.blue);
		g2.drawRect(0,0,width,height);

		return bImg;
	}

	public static Image getXImage(AxisCurveContainerView track, float rateWidth, float rateHeight){

		XAxisView xAxisView = (XAxisView)track.getXAxisViewPanel();

		BufferedImage bImg = new BufferedImage((int)(xAxisView.getWidth()*rateWidth),
				(int)(xAxisView.getHeight()*rateHeight),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setFont(ImageGenerator.getFontSize(g2, new Font("SansSerif", Font.PLAIN, 11),rateWidth,rateHeight));
		g2.setColor(Color.white);
		g2.fillRect(0,0,(int)(xAxisView.getWidth()*rateWidth),(int)(xAxisView.getHeight()*rateHeight));

		g2.setBackground(Color.white);

		xAxisView.print(g2,0,rateWidth,rateHeight);

		return bImg;
	}

	public static Image getNeedImage(Image img, int Width, int Height){
 		if(img == null){
 			return null;
 		}
 		BufferedImage bImg = new BufferedImage(Width,Height,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		int imgWidht = img.getWidth(null);
		int imgHeight = img.getHeight(null);
 		g2.drawImage(img,0,0,imgWidht,imgHeight,null);
 		return bImg;
 	}

	public static Image[][] splitImage(Image img, int rows, int cols)
    {
        int width = img.getWidth(null);
        int height = img.getHeight(null);

        int subWidth = width / cols;
        int subHeight = height / rows;

        BufferedImage bImg = null;
        if (img instanceof BufferedImage)
            bImg = (BufferedImage) img;
        else
        {
            bImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics g = bImg.getGraphics();
            g.drawImage(img, 0, 0, null);
        }

        BufferedImage[][] result = new BufferedImage[rows][cols];
        ColorModel colorModel = bImg.getColorModel();
        WritableRaster raster = bImg.getRaster();
        int w = subWidth;
        int h = subHeight;
        int x = 0;
        int y = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                WritableRaster subRaster = raster.createWritableChild(x, y, w, h, 0, 0, null);
                result[i][j] = new BufferedImage(colorModel, subRaster, true, null);
                x += w;
                if (j == cols - 1)
                    w = width - x;
            }
            y += h;
            if (i == rows - 1)
                h = height - y;
            x = 0;
            w = subWidth;
        }
        return result;
    }

	public static int getPrinterResolution(){
        PageAttributes pageAttributes = new PageAttributes();

        int value[] = pageAttributes.getPrinterResolution();
        int pixValue = value[0];
        return pixValue;
	}

	public static int  getScreenResolution(){
		Toolkit kit = Toolkit.getDefaultToolkit();
		int pixValue = kit.getScreenResolution();
        return pixValue;
	}

	public static void setPageLayout(boolean IsPrint, float rateWidth, float rateHeight){
		if(pageFormat == null){
			PrinterJob printerJob = PrinterJob.getPrinterJob();
			pageFormat = printerJob.defaultPage();
		}
		if(pageHeader == null){
			pageHeader = new PageAccessory();
		}
		if(pageFooter == null){
			pageFooter = new PageAccessory();
		}
		if(pageLayout == null){
			pageLayout = new PageLayout();
		}
		Paper paper = pageFormat.getPaper();
		int pageWidth = (int)((float)paper.getWidth() * rateWidth * getPrinterResolution() / 72);
		int pageHeight = (int)((float)paper.getHeight() * rateHeight * getPrinterResolution() / 72);
		int showTop = (int)((float)paper.getImageableY() * rateHeight * getPrinterResolution() / 72);
		int showLeft = (int)((float)paper.getImageableX() * rateWidth * getPrinterResolution() / 72);
		int showWidth = (int)((float)paper.getImageableWidth() * rateWidth * getPrinterResolution() / 72);
		int showHeight = (int)((float)paper.getImageableHeight() * rateHeight * getPrinterResolution() / 72);
		if(ImageGenerator.pageFormat.getOrientation() == 0){
			int value = pageWidth;
			pageWidth = pageHeight;
			pageHeight = value;

			value = showWidth;
			showWidth = showHeight;
			showHeight = value;
		}

		BufferedImage bImg = new BufferedImage(pageWidth,pageHeight,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setColor(Color.white);
		g2.fillRect(0,0,pageWidth,pageHeight);
		g2.setBackground(Color.white);

		int left = 0;
		int headerHeightLimit = (int)(pageHeader.getHeightLimit()*rateHeight);
		Image iconHeader = getLimitImage(pageHeader.getIconImg(),showWidth,headerHeightLimit);
		if(iconHeader != null && !pageHeader.getTitle().equals("")
				&& pageHeader.getTextPosition() == pageHeader.getIconPosition())
		{
			String text = pageHeader.getTitle();
			g2.setColor(pageHeader.getColor());
			g2.setFont(getFontSize(g2, new Font("SansSerif", Font.PLAIN, pageHeader.getSize()),rateWidth,rateHeight));
			int headerWidth = g2.getFontMetrics().stringWidth(text) + (int)(10 * rateWidth) + iconHeader.getWidth(null);
			if(pageHeader.getTextPosition() == PageAccessory.CENTER)
			{
				left = showLeft + (showWidth - headerWidth)/2;
			}else if(pageHeader.getTextPosition() == PageAccessory.RIGHT)
			{
				left = showLeft + showWidth - headerWidth;
			}else{
				left = showLeft;
			}
			g2.drawString(text, left, showTop + headerHeightLimit);
			g2.drawImage(iconHeader,left + (headerWidth - iconHeader.getWidth(null)),
					showTop+ headerHeightLimit - iconHeader.getHeight(null),
					iconHeader.getWidth(null),iconHeader.getHeight(null),null);

		}else if(!ImageGenerator.pageHeader.getTitle().equals("")){
			String text = pageHeader.getTitle();
			g2.setColor(pageHeader.getColor());
			g2.setFont(getFontSize(g2, new Font("SansSerif", Font.PLAIN, pageHeader.getSize()),rateWidth,rateHeight));
			if(pageHeader.getTextPosition() == PageAccessory.CENTER)
			{
				left = showLeft + (showWidth - g2.getFontMetrics().stringWidth(text))/2;
			}else if(pageHeader.getTextPosition() == PageAccessory.RIGHT)
			{
				left = showLeft + showWidth - g2.getFontMetrics().stringWidth(text);
			}else{
				left = showLeft;
			}
			g2.drawString(text, left, showTop + headerHeightLimit);
		}else if(iconHeader != null){
			if(pageHeader.getIconPosition() == PageAccessory.CENTER)
			{
				left = showLeft + (showWidth - iconHeader.getWidth(null))/2;
			}else if(pageHeader.getIconPosition() == PageAccessory.RIGHT)
			{
				left = showLeft + showWidth - iconHeader.getWidth(null);
			}else{
				left = showLeft;
			}
			g2.drawImage(iconHeader,left,
					showTop + headerHeightLimit - iconHeader.getHeight(null),
					iconHeader.getWidth(null),iconHeader.getHeight(null),null);
		}

		int footerHeightLimit = (int)(pageFooter.getHeightLimit()*rateHeight);
		Image iconFooter = getLimitImage(pageFooter.getIconImg(),showWidth,footerHeightLimit);
		if(iconFooter != null && !pageFooter.getTitle().equals("")
				&& pageFooter.getTextPosition() == pageFooter.getIconPosition())
		{
			String text = pageFooter.getTitle();
			g2.setColor(pageFooter.getColor());
			g2.setFont(getFontSize(g2, new Font("SansSerif", Font.PLAIN, pageFooter.getSize()),rateWidth,rateHeight));
			int headerWidth = g2.getFontMetrics().stringWidth(text) + (int)(10 * rateWidth) + iconFooter.getWidth(null);
			if(pageFooter.getTextPosition() == PageAccessory.CENTER)
			{
				left = showLeft + (showWidth - headerWidth)/2;
			}else if(pageFooter.getTextPosition() == PageAccessory.RIGHT)
			{
				left = showLeft + showWidth - headerWidth;
			}else{
				left = showLeft;
			}
			g2.drawString(text, left, showTop + showHeight);
			g2.drawImage(iconFooter,left + (headerWidth - iconFooter.getWidth(null)),
					showTop + showHeight - iconFooter.getHeight(null),
					iconFooter.getWidth(null),iconFooter.getHeight(null),null);

		}else if(!ImageGenerator.pageFooter.getTitle().equals("")){
			String text = pageFooter.getTitle();
			g2.setColor(pageFooter.getColor());
			g2.setFont(getFontSize(g2, new Font("SansSerif", Font.PLAIN, pageFooter.getSize()),rateWidth,rateHeight));
			if(pageFooter.getTextPosition() == PageAccessory.CENTER)
			{
				left = showLeft + (showWidth - g2.getFontMetrics().stringWidth(text))/2;
			}else if(pageFooter.getTextPosition() == PageAccessory.RIGHT)
			{
				left = showLeft + showWidth - g2.getFontMetrics().stringWidth(text);
			}else{
				left = showLeft;
			}
			g2.drawString(text, left, showTop+showHeight);
		}else if(iconFooter != null){
			if(pageFooter.getIconPosition() == PageAccessory.CENTER)
			{
				left = showLeft + (showWidth - iconFooter.getWidth(null))/2;
			}else if(pageFooter.getIconPosition() == PageAccessory.RIGHT)
			{
				left = showLeft + showWidth - iconFooter.getWidth(null);
			}else{
				left = showLeft;
			}
			g2.drawImage(iconFooter,left,
					showTop + showHeight - iconFooter.getHeight(null),
					iconFooter.getWidth(null),iconFooter.getHeight(null),null);
		}

		if ( IsPrint ){
			pageLayout.setPrintBackImage(bImg);
			pageLayout.setPrintContentRect(new Rectangle(showLeft,(showTop+headerHeightLimit+(int)(10*rateWidth)),
					showWidth,(showHeight - headerHeightLimit - footerHeightLimit - (int)(20*rateHeight))));
			pageLayout.setPrintPageRect(new Rectangle(0,0,pageWidth,pageHeight));
		}else{
			pageLayout.setPreviewBackImage(bImg);
			pageLayout.setPreviewContentRect(new Rectangle(showLeft,(showTop+headerHeightLimit+(int)(10*rateWidth)),
					showWidth,(showHeight - headerHeightLimit - footerHeightLimit - (int)(20*rateHeight))));
			pageLayout.setPreviewPageRect(new Rectangle(0,0,pageWidth,pageHeight));
		}
	}

	public static Image getLimitImage(Image img, int widthLimit,int heightLimit){
		if(img == null){
			return null;
		}
		int width = img.getWidth(null);
		int height = img.getHeight(null);
		if(width <= widthLimit && height <= heightLimit){
			return img;
		}
		if((int)((height*1000)/width) > (int)((heightLimit*1000)/widthLimit)){
			width = (int)(heightLimit * width / height);
			height = heightLimit;
		}else{
			height = (int)(widthLimit * height / width);
			width = widthLimit;
		}

		BufferedImage bImg = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		bImg.getGraphics().drawImage(img,0,0,width,height,null);
		return bImg;
	}
	public static Font getFontByHSpace(Graphics2D g2, int space, float rateWidth, float  rateHeight){

		int fontHeight = (int)(g2.getFontMetrics().getHeight()*rateHeight);
		int fontSize = 0;
		for(;;){
			fontSize++;
			g2.setFont(new Font("SansSerif", Font.PLAIN, fontSize));
			if (g2.getFontMetrics().getHeight() >= fontHeight){
				fontSize = fontSize - 1;
				break;
			}
		}
		if(fontSize < 1){
			fontSize = 1;
		}

		return  new Font("SansSerif", Font.PLAIN, fontSize);
	}
	public static Font getFontSize(Graphics2D g2, Font oldFont, float rateWidth, float  rateHeight){
		g2.setFont(oldFont);
		int fontWidth = (int)(g2.getFontMetrics().stringWidth("W")*rateWidth);
		int fontHeight = (int)(g2.getFontMetrics().getHeight()*rateHeight);
		int fontSize = 0;
		for(;;){
			fontSize = fontSize + 1;
			g2.setFont(new Font("SansSerif", Font.PLAIN, fontSize));
			if (g2.getFontMetrics().stringWidth("W") > fontWidth){
				fontSize = fontSize - 1;
				break;
			}else if(g2.getFontMetrics().stringWidth("W") == fontWidth){
				break;
			}else if (g2.getFontMetrics().getHeight() > fontHeight){
				fontSize = fontSize - 1;
				break;
			}if (g2.getFontMetrics().getHeight() == fontHeight){
				break;
			}
		}
		if(fontSize < 1){
			fontSize = 1;
		}

		return  new Font("SansSerif", Font.PLAIN, 3*fontSize/2);
	}

	@SuppressWarnings("unchecked")
	public static ArrayList getImageList(Image needImage, int needWidth, int needHeight, boolean IsPrint){
		ArrayList imgList = new ArrayList();
		if (needImage == null){
			return imgList;
		}
		int imgWidth = needImage.getWidth(null);
		int imgHeight = needImage.getHeight(null);
		int rows = (int)imgHeight/needHeight;
		int cols = (int)imgWidth/needWidth;
		if(cols*needWidth < imgWidth){
			cols++;
		}
		if(rows*needHeight < imgHeight){
			rows++;
		}

		Image neededImg = ImageGenerator.getNeedImage(needImage, needWidth * cols, needHeight * rows);
		Image[][] current = ImageGenerator.splitImage(neededImg,rows,cols);

		for(int i = 0 ; i < rows; i++){
			for(int j = 0; j < cols; j++){
				imgList.add(getAllImage(current[i][j],IsPrint));
			}
		}
		return imgList;
	}

	public static Image getAllImage(Image needImage, boolean IsPrint){
		int Width = 0;
		int Height = 0;
		int contentLeft = 0;
		int contentTop = 0;
		Image backImage = null;
		if (IsPrint){
			Width = (int)ImageGenerator.pageLayout.getPrintPageRect().getWidth();
			Height = (int)ImageGenerator.pageLayout.getPrintPageRect().getHeight();
			contentLeft = (int)ImageGenerator.pageLayout.getPrintContentRect().getX();
			contentTop = (int)ImageGenerator.pageLayout.getPrintContentRect().getY();
			backImage = ImageGenerator.pageLayout.getPrintBackImage();
		}else {
			Width = (int)ImageGenerator.pageLayout.getPreviewPageRect().getWidth();
			Height = (int)ImageGenerator.pageLayout.getPreviewPageRect().getHeight();
			contentLeft = (int)ImageGenerator.pageLayout.getPreviewContentRect().getX();
			contentTop = (int)ImageGenerator.pageLayout.getPreviewContentRect().getY();
			backImage = ImageGenerator.pageLayout.getPrintBackImage();
		}
		BufferedImage bImg = new BufferedImage(Width,Height,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		g2.setColor(Color.white);
		g2.fillRect(0,0,Width,Height);
		g2.setBackground(Color.white);

		g2.drawImage(backImage,0,0,backImage.getWidth(null),backImage.getHeight(null),null);

		g2.drawImage(needImage,contentLeft,contentTop,needImage.getWidth(null),needImage.getHeight(null),null);

		return bImg;
	}
}
