/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.print;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class ImageSave {

	public void SaveImage(BufferedImage bufferedImage){
		String outputFileName = new String("d:\\desktop.jpg");
		float outputQuality = 0.80f;

		Image saveImage = null;
	    int width = bufferedImage.getWidth();
	    int height = bufferedImage.getHeight();
	    saveImage = bufferedImage.getScaledInstance(width, height, BufferedImage.TYPE_INT_RGB);

	    try{
	    	FileOutputStream fos = new FileOutputStream(outputFileName);
	        encodeJPEG(fos, saveImage, outputQuality);
	        fos.flush();
	        fos.close();
	    }catch(Exception ex){
	    	ex.printStackTrace();
	    }
	}

	private void encodeJPEG(OutputStream outputStream, Image outputImage, float outputQuality) throws java.io.IOException
	{
		int outputWidth = outputImage.getWidth(null);
		if (outputWidth < 1)
		   {
			   throw new IllegalArgumentException("output image width " + outputWidth + " is out of range");
		   }

		   int outputHeight = outputImage.getHeight(null);
		   if (outputHeight < 1)
		   {
			   throw new IllegalArgumentException("output image height " + outputHeight + " is out of range");
		   }

		   BufferedImage bi = new BufferedImage(outputWidth, outputHeight, BufferedImage.TYPE_INT_RGB);
		   Graphics2D biContext = bi.createGraphics();
		   biContext.drawImage(outputImage, 0, 0, null);

		   JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(outputStream);
		   // The default quality is 0.75.
		   JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(bi);
		   jep.setQuality(outputQuality, true);
		   encoder.encode(bi, jep);
		   outputStream.flush();

	}

}
