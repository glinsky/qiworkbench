/*
 gwLogViewer - a viewer to display and analyze well log data
 This program module Copyright (C) 2006 G&W Systems Consulting Corp.
 and distributed by BHP Billiton Petroleum under license.

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License Version 2 as as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 or visit the link http://www.gnu.org/licenses/gpl.txt.

 To contact BHP Billiton about this software you can e-mail info@qiworkbench.org
 or visit http://qiworkbench.org to learn more.
 */
package com.gwsys.print;

import java.awt.Color;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.gwsys.welllog.icons.IconResource;

public class PageHeaderFooterSettingDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	private JTextField tfText = null;

	private JTextField tfImage = null;

	private JLabel lblContent = null;

	private JTextArea tfContent = null;

	private JScrollPane scrollContent = null;

	private JLabel lblColor = null;

	private JComboBox cbColor = null;

	private JLabel lblSize = null;

	private JComboBox cbSize = null;

	private JTextField tfImageFile = null;

	private JButton btnBrowse = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JPanel textPanel = null;

	private JPanel imagePanel = null;

	private JLabel lblTextPlace = null;

	private JLabel lblIconPlace = null;

	private JComboBox cbTextPlace = null;

	private JComboBox cbIconPlace = null;

	private JPanel jContentPane = null;

	private PrintFrame printFrame = null;

	private int Accessory;
	public static final int PAGE_HEADER = 1;
	public static final int PAGE_FOOTER = 2;

	private PageAccessory accessory;

	public PageHeaderFooterSettingDialog(PrintFrame owerFrame, int Accessory){
		super(owerFrame);

		printFrame = owerFrame;

		this.Accessory = Accessory;

		initialize();
		this.setSize(450, 330);
		this.setResizable(false);
		this.setModal(true);
	}

	private void initialize() {
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		if(Accessory == PAGE_HEADER){
			this.setTitle("Setting Page Head Property");
			if(ImageGenerator.pageHeader == null){
				ImageGenerator.pageHeader = new PageAccessory();
			}
			accessory = ImageGenerator.pageHeader;
		}else if(Accessory == PAGE_FOOTER){
			this.setTitle("Setting Page Foot Property");
			if(ImageGenerator.pageFooter == null){
				ImageGenerator.pageFooter = new PageAccessory();
			}
			accessory = ImageGenerator.pageFooter;
		}

		this.setContentPane(getJContentPane());
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);

			// Header
			tfText = new JTextField(" Text");
			tfText.setBackground(SystemColor.control);
			tfText.setEnabled(false);
			tfText.setBorder(null);
			tfText.setDisabledTextColor(Color.black);
			tfText.setBounds(20,6,40,25);

			lblContent = new JLabel("Text:");
			lblContent.setBounds(10,10,50,25);

			tfContent = new JTextArea(70,5);
			scrollContent = new JScrollPane(tfContent);
			scrollContent.setBounds(70,10,340,40);
			tfContent.setText(accessory.getTitle());

			lblColor = new JLabel("Color:");
			lblColor.setBounds(10,60,50,25);
			ImageIcon [] icons = new ImageIcon[8];
			icons[0] = IconResource.getInstance().getImageIcon("colorBlack.gif");
			icons[1] = IconResource.getInstance().getImageIcon("colorBlue.gif");
			icons[2] = IconResource.getInstance().getImageIcon("colorCyan.gif");
			icons[3] = IconResource.getInstance().getImageIcon("colorGreen.gif");
			icons[4] = IconResource.getInstance().getImageIcon("colorOrange.gif");
			icons[5] = IconResource.getInstance().getImageIcon("colorMagenta.gif");
			icons[6] = IconResource.getInstance().getImageIcon("colorRed.gif");
			icons[7] = IconResource.getInstance().getImageIcon("colorYellow.gif");

			cbColor = new JComboBox(icons);
			cbColor.setBounds(70,60,110,25);
			if (accessory.getColor() == Color.BLACK) {
				cbColor.setSelectedIndex(0);
			} else if (accessory.getColor() == Color.BLUE) {
				cbColor.setSelectedIndex(1);
			} else if (accessory.getColor() == Color.CYAN) {
				cbColor.setSelectedIndex(2);
			} else if (accessory.getColor() == Color.GREEN) {
				cbColor.setSelectedIndex(3);
			} else if (accessory.getColor() == Color.orange) {
				cbColor.setSelectedIndex(4);
			}else if (accessory.getColor() == Color.MAGENTA) {
				cbColor.setSelectedIndex(5);
			} else if (accessory.getColor() == Color.RED) {
				cbColor.setSelectedIndex(6);
			} else if (accessory.getColor() == Color.YELLOW) {
				cbColor.setSelectedIndex(7);
			}

			lblSize = new JLabel("Font Size:");
			lblSize.setBounds(210,60,70,25);
			String[] fontSize = { "1", "2", "4", "6", "8", "10", "12", "14",
					"16", "18", "20", "22", "24" };
			cbSize = new JComboBox(fontSize);
			cbSize.setBounds(280,60,120,25);
			if (accessory.getSize() == 1) {
				cbSize.setSelectedIndex(0);
			} else if (accessory.getSize() == 2) {
				cbSize.setSelectedIndex(1);
			} else if (accessory.getSize() == 4) {
				cbSize.setSelectedIndex(2);
			} else if (accessory.getSize() == 6) {
				cbSize.setSelectedIndex(3);
			} else if (accessory.getSize() == 8) {
				cbSize.setSelectedIndex(4);
			}else if (accessory.getSize() == 10) {
				cbSize.setSelectedIndex(5);
			} else if (accessory.getSize() == 12) {
				cbSize.setSelectedIndex(6);
			} else if (accessory.getSize() == 14) {
				cbSize.setSelectedIndex(7);
			} else if (accessory.getSize() == 16) {
				cbSize.setSelectedIndex(8);
			} else if (accessory.getSize() == 18) {
				cbSize.setSelectedIndex(9);
			} else if (accessory.getSize() == 20) {
				cbSize.setSelectedIndex(10);
			} else if (accessory.getSize() == 22) {
				cbSize.setSelectedIndex(11);
			} else if (accessory.getSize() == 24) {
				cbSize.setSelectedIndex(12);
			}

			lblTextPlace = new JLabel("Position:");
			lblTextPlace.setBounds(10,90,60,25);
			String[] placeOption = {"Left","Center","Right"};
			cbTextPlace = new JComboBox(placeOption);
			cbTextPlace.setBounds(70,90,110,25);
			cbTextPlace.setSelectedIndex(accessory.getTextPosition());

			textPanel = new JPanel();
			textPanel.setLayout(null);
			textPanel.setBorder(BorderFactory.createEtchedBorder());
			textPanel.setBounds(10, 17, 430, 130);

			// image
			tfImage = new JTextField(" Choose image file");
			tfImage.setBackground(SystemColor.control);
			tfImage.setEnabled(false);
			tfImage.setBorder(null);
			tfImage.setDisabledTextColor(Color.black);
			tfImage.setBounds(20,159,130,25);

			tfImageFile = new JTextField();
			tfImageFile.setBounds(10,20,300,25);

			btnBrowse = new JButton();
			btnBrowse.setText("Browse");
			btnBrowse.setBounds(320,20,80,25);
			btnBrowse.addActionListener(this);
			btnBrowse.setActionCommand("Browse");

			lblIconPlace = new JLabel("Position:");
			lblIconPlace.setBounds(10,50,60,25);
			cbIconPlace = new JComboBox(placeOption);
			cbIconPlace.setBounds(70,50,110,25);
			if(accessory != null){
				cbIconPlace.setSelectedIndex(accessory.getIconPosition());
			}else{
				cbIconPlace.setSelectedIndex(0);
			}

			textPanel.add(lblContent);
			textPanel.add(scrollContent);
			textPanel.add(lblColor);
			textPanel.add(cbColor);
			textPanel.add(lblSize);
			textPanel.add(cbSize);
			textPanel.add(lblTextPlace);
			textPanel.add(cbTextPlace);

			imagePanel = new JPanel();
			imagePanel.setLayout(null);
			imagePanel.setBorder(BorderFactory.createEtchedBorder());
			imagePanel.setBounds(10, 170, 430, 90);

			imagePanel.add(tfImageFile);
			imagePanel.add(btnBrowse);
			imagePanel.add(lblIconPlace);
			imagePanel.add(cbIconPlace);

			btnOk = new JButton("OK");
			btnOk.setActionCommand("OK");
			btnOk.addActionListener(this);
			btnOk.setBounds(120, 270, 80, 25);

			btnCancel = new JButton("Cancel");
			btnCancel.setActionCommand("Cancel");
			btnCancel.addActionListener(this);
			btnCancel.setBounds(230, 270, 80, 25);

			jContentPane.add(tfText);
			jContentPane.add(textPanel);

			jContentPane.add(tfImage);
			jContentPane.add(imagePanel);

			jContentPane.add(btnOk);
			jContentPane.add(btnCancel);
		}
		return this.jContentPane;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("OK")){
			// Header
			accessory.setTitle(tfContent.getText().trim());

			if(cbColor.getSelectedIndex() == 0){
				accessory.setColor(Color.black);
			}else if(cbColor.getSelectedIndex() == 1){
				accessory.setColor(Color.blue);
			}else if(cbColor.getSelectedIndex() == 2){
				accessory.setColor(Color.cyan);
			}else if(cbColor.getSelectedIndex() == 3){
				accessory.setColor(Color.green);
			}else if(cbColor.getSelectedIndex() == 4){
				accessory.setColor(Color.orange);
			}else if(cbColor.getSelectedIndex() == 5){
				accessory.setColor(Color.magenta);
			}else if(cbColor.getSelectedIndex() == 6){
				accessory.setColor(Color.red);
			}else if(cbColor.getSelectedIndex() == 7){
				accessory.setColor(Color.yellow);
			}

			if (cbSize.getSelectedIndex() == 0) {
				accessory.setSize(1);
			} else if (cbSize.getSelectedIndex() == 1) {
				accessory.setSize(2);
			} else if (cbSize.getSelectedIndex() == 2) {
				accessory.setSize(4);
			} else if (cbSize.getSelectedIndex() == 3) {
				accessory.setSize(6);
			} else if (cbSize.getSelectedIndex() == 4) {
				accessory.setSize(8);
			}else if (cbSize.getSelectedIndex() == 5) {
				accessory.setSize(10);
			} else if (cbSize.getSelectedIndex() == 6) {
				accessory.setSize(12);
			} else if (cbSize.getSelectedIndex() == 7) {
				accessory.setSize(14);
			} else if (cbSize.getSelectedIndex() == 8) {
				accessory.setSize(16);
			} else if (cbSize.getSelectedIndex() == 9) {
				accessory.setSize(18);
			} else if (cbSize.getSelectedIndex() == 10) {
				accessory.setSize(20);
			} else if (cbSize.getSelectedIndex() == 11) {
				accessory.setSize(22);
			} else if (cbSize.getSelectedIndex() == 12) {
				accessory.setSize(24);
			}

			accessory.setTextPosition(cbTextPlace.getSelectedIndex());
			accessory.setIconPosition(cbIconPlace.getSelectedIndex());

			if(!tfImageFile.getText().trim().equals("")){
				String filePath = tfImageFile.getText().trim();

				File file = new File(filePath);
				try{
					Image img = javax.imageio.ImageIO.read(file);
					if(img != null){
						accessory.setIconImg(img);
					}
				}catch (Exception ex){
					System.out.println("Open File Error");
				}
			}
			printFrame.changeHeaderFooter();
			this.setVisible(false);
			printFrame.repaintAll();
			this.dispose();
			return;
		}else if(e.getActionCommand().equals("Cancel")){
			this.setVisible(false);
			this.dispose();
			return;
		}else if(e.getActionCommand().equals("Browse")){
			JFileChooser fc = new JFileChooser();
			File file;
			if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
				file = fc.getSelectedFile();
				tfImageFile.setText(file.getPath());
			}
		}
	}
}
