/*
 ###########################################################################
 # XMLEditor - an editor for creating and editing XML files based on an XML Schema.
 # This program module Copyright (C) 2006  BHP Billiton Petroleum
 #
 # This program is free software; you can redistribute it and/or modify it
 # under the terms of the GNU General Public License Version 2 as published
 # by the Free Software Foundation.
 #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY
 # WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 # FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 # details.
 #
 # You should have received a copy of the GNU General Public License along
 # with this program; if not, write to the Free Software Foundation, Inc.,
 # 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
 # link http://www.gnu.org/licenses/gpl.txt.
 #
 # To contact BHP Billiton about this software you can e-mail
 # info@qiworkbench.org or visit http://qiworkbench.org to learn more.
 ###########################################################################
 */
package com.bhpb.xmleditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.help.BadIDException;
import javax.help.CSH;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.tree.DefaultTreeModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bhpb.xmleditor.model.DEditAttributeInfo;
import com.bhpb.xmleditor.model.DEditDataType;
import com.bhpb.xmleditor.model.DEditDebug;
import com.bhpb.xmleditor.model.DEditLeafDialog;
import com.bhpb.xmleditor.model.DEditNode;
import com.bhpb.xmleditor.model.DEditPlugin;
import com.bhpb.xmleditor.model.DEditSchema;
import com.bhpb.xmleditor.model.DEditSchemaException;
import com.bhpb.xmleditor.model.DEditSimpleFileFilter;
import com.bhpb.xmleditor.model.DEditTreeCellRenderer;
import com.bhpb.xmleditor.model.DEditXMLWindow;
import com.bhpb.xmleditor.actions.XmlEditorAbout;
import com.bhpb.xmleditor.actions.XmlEditorConfig;
import com.bhpb.xmleditor.actions.LoadPluginAction;
import com.bhpb.xmleditor.actions.PackageAction;
import com.bhpb.xmleditor.actions.DefaultAction;
import com.bhpb.xmleditor.plugins.script.ScriptPlugin;
import com.bhpb.xmleditor.plugins.delivery.DeliveryPlugin;
import com.bhpb.xmleditor.swing.model.EditorFileFilter;
import com.bhpb.distribution.DistConst;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.model.PackageLoad;
import com.bhpb.qiworkbench.compAPI.model.NodeFile;
import com.bhpb.qiworkbench.compAPI.model.NodePackage;
import com.bhpb.qiworkbench.compAPI.model.PluginModel;

/**
 * The GUI for the XmlEditor plugin. It is not considered a separate component
 * so it has no its own message queue. Instead it use its plugin agent for
 * handling message passing. It is not necessary to start the GUI as a thread
 * because by definition a Swing GUI is a separate thread.
 */
public class XmlEditorGUI extends JInternalFrame {
    private static Logger logger = Logger.getLogger(XmlEditorGUI.class
            .getName());

    static final long serialVersionUID = 1l;

    private static String openErrString = null;

    final private static int DEFAULT_CUTOFF = 20; // # items in a sub menu

    // Resource bundle for internationalized and accessible text
    private ResourceBundle bundle = null;

    Component gui;

    String project = "";

    String projectPath = "";

    XmlEditorPlugin agent;

    private File curXMLDir; // the directory to start looking for xml files in

    private File curXSDDir; // the directory to start looking for xsd files in

    private DEditSchema schema; // the Schema used by the XML Editor

    private DEditPlugin plugin = null; // the currently loaded plugin

    private boolean relaxed = true;

    private boolean allowNewFile = true; // true if making a new file is
                                            // allowed

    private boolean allowOpenFile = true; // true if opening files is allowed

    private boolean allowLoadSchema = false; // true if opening schemas is
                                                // allowed

    private JDesktopPane jdp; // desktop pane for XML Windows etc.

    private JMenuBar menuBar; // the XML Editor's menu bar

    private JToolBar toolbar; // the toolbar with all the buttons

    private JPopupMenu popup; // the right click popup menu

    private JMenu newChildTagMenu; // all the sub menus ...

    private JMenu newChildTagPopupMenu;

    private JMenu newTagBeforeMenu;

    private JMenu newTagBeforePopupMenu;

    private JMenu newTagAfterMenu;

    private JMenu newTagAfterPopupMenu;

    private JMenu switchTagToMenu;
    private JMenuItem renameMenuItem;

    private JMenu switchTagToPopupMenu;

    private JMenu pasteChildMenu;

    private JMenu pasteChildPopupMenu;

    private JMenuItem verifyItem; // its listener verifies the xml

    private JMenuItem exportMenuItem; // it's listener does "Save As..." on the
                                    // xml

    private ButtonGroup pGroup = new ButtonGroup();

    private JMenuItem newItem;

    private JMenuItem importMenuItem;
    private JMenuItem quit;
    private JMenuItem saveQuit;

    private JMenuItem loadSchemaItem;

    private JButton newXMLButton;

    private JButton openXMLButton;

    private Action saveAction;

    private Action saveAsAction;
    private Action nodeHelpAction;

    private EditAction editValAction;

    private EditAction copyAction;

    private EditAction cutAction;

    private EditAction pasteBeforeAction;

    private EditAction pasteAfterAction;

    private EditAction undoAction;

    private DEditLeafDialog dialog;

    private DEditTreeCellRenderer renderer;

    private JInternalFrame pManager;

    private MouseListener ml; // double click -> edit value dialog listener

    private MouseListener popupListener = new PopupListener();

    private JApplet applet = null; // holds the applet if there is one

    private boolean isApplet = false; // if DEditor is running as an applet

    final private static int MAX_WINDOWS = 10; // max open xml files

    private Vector<DEditXMLWindow> windows = new Vector<DEditXMLWindow>();

    private Hashtable<String, String> properties = new Hashtable<String, String>();

    HelpSet deditHelpSet = null;

    HelpBroker helpBroker = null;

    private DEditNode[] copied = null; // the currently copied nodes

    private final static int BUTTON_ICON_SIZE = 33; // size of icon buttons

    private final Dimension buttonDim = new Dimension(BUTTON_ICON_SIZE,
            BUTTON_ICON_SIZE);

    private final static Insets NO_MARGIN_INSETS = new Insets(0, 0, 0, 0);

    int prefWith = -1;

    int prefHeight = -1;

    boolean loaded = true;

    PackageLoad pLoad;

    String openedXml;

    public boolean fromParent = true;
    private Properties props;
    /**
     * Constructor
     *
     * @param agent
     *            XmlEditorPlugin agent
     */
    public XmlEditorGUI(XmlEditorPlugin agent) {
        this(agent, new PackageLoad());
    }

    /*
     * Constructor @param agent xmlEditor Plugin agent @param project Selected
     * project @param projPath Project path
     */
    public XmlEditorGUI(XmlEditorPlugin agent, String project, String projPath) {
        this(agent, new PackageLoad());
    }

    /*
     * Constructor @param agent xmlEditor Plugin agent @param project Selected
     * project @param projPath Project path
     */
    public XmlEditorGUI(XmlEditorPlugin agent, PackageLoad pLoad) {

        gui = (Component) this;
        this.pLoad = pLoad;

        this.agent = agent;
        QiProjectDescriptor desc = agent.getQiProjectDescriptor();
        if (desc != null)
            this.project = QiProjectDescUtils.getProjectPath(desc);
        String strProj = this.project;
        if (strProj == null) {
            strProj = "";
        }
        this.projectPath = "";
        this.curXMLDir = new File(strProj);
        this.curXSDDir = this.curXMLDir;
        this.allowLoadSchema = false;
        this.allowNewFile = true;
        this.allowOpenFile = true;
        this.relaxed = true;
        if (pLoad.getKeyValue("fromParent") != null) {
            fromParent = false;
        }

        initialize(prefWith, prefHeight);
        buildGUI();
        loadEditorPlugin();
        loadSchemaFile(pLoad.getSchemaFile());

        /*
        String xmlFile = pLoad.getXmlFile();
        if (xmlFile == null || "NONE".equalsIgnoreCase(xmlFile)) {
            openFile(pLoad.getLastXml());

        } else {
            openFile(pLoad.getXmlFile());
        }
        */
        // openFile(pLoad.getXmlFile());
        finalizeGUI();
    }

    private void buildGUI() {
        buildMenu();
        buildToolBar();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    private void initialize(int width, int height) {
        if (width <= 0 || height <= 0) {
            width = XmlEditorConstants.PREF_WIDTH;
            height = XmlEditorConstants.PREF_HEIGHT;
        }
        this.setSize(width, height);
        this.setResizable(true);
        this.maximizable = true;
        this.iconable = true;

        String namedDir = pLoad.getPkgSet();
        if (namedDir != null && namedDir.length() != 0) {
            namedDir = namedDir.toUpperCase();
            if (namedDir.equals(DistConst.CONFIG)) {
                getAgent().getPrepare().makeNamedDir(namedDir);
            }
        }

        this.schema = new DEditSchema(this, relaxed);
        setHelpSet();
        if (pLoad.getSchemaFile() == null
                || pLoad.getSchemaFile().length() == 0
                || pLoad.getSchemaFile().equals("NONE")) {
            pLoad.setSchemaFile(null);
            this.allowLoadSchema = true;
            this.allowNewFile = true;
            //this.allowOpenFile = true;
            //this.allowNewFile = false;
            this.allowOpenFile = false;
        }

        jdp = new JDesktopPane();
        this.getContentPane().setLayout(new BorderLayout());
        jdp.setPreferredSize(new Dimension(600, 400));
        this.getContentPane().add(jdp, BorderLayout.CENTER);
        toolbar = new JToolBar();
        menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        this.getContentPane().add(toolbar, BorderLayout.NORTH);

    }

    private void loadEditorPlugin() {
        loadConfigFile(pLoad.getConfigFile());
        loadPlugin();
    }

    public String getPluginClass() {
        return pLoad.getPluginClass();
    }

    public String getConfigFile() {
        return pLoad.getConfigFile();
    }

    public String getSchemaFile() {
        if (pLoad.getSchemaFile() == null) {
            return "Schema File Not Loaded Yet";
        }
        return pLoad.getSchemaFile();
    }

    public PackageLoad getPackageLoad() {
        return pLoad;
    }

    /**
     * Save the state information for the Wavelet Decomposition GUI.
     *
     * @return xml string.
     */
    public String saveState() {
        return null;
    }

    /* Close GUI and dispose */
    public void closePluginGUI() {
        setVisible(false);
        dispose();
    }

    public HelpSet getHelpSet() {
        return deditHelpSet;
    }

    public HelpBroker getHelpBroker() {
        return helpBroker;
    }

    private void setHelpSet() {
        String helpSetName = "help/DEditHelp.hs";
        try {
            ClassLoader cl = this.getClass().getClassLoader();
            URL hsURL = HelpSet.findHelpSet(cl, helpSetName);
            this.deditHelpSet = new HelpSet(cl, hsURL);
            helpBroker = deditHelpSet.createHelpBroker();
        } catch (Exception e) {
            logger.warning("Helpset " + helpSetName + " not found.");
        }
    }

    /**
     * loads a plugin for this DEditor which modifies behavior.
     *
     * @param plugin
     *            the plugin to load into DEditor
     */
    private void loadPlugin() {
        if (this.plugin == null) {
            // String args[] = {"-r"};
            String args[] = getArgs();
            if (pLoad.getPluginClass().equals(XmlEditorConstants.DELIV_CLASS)) {
                plugin = new DeliveryPlugin("", args);

            } else {
                plugin = new ScriptPlugin("", args);

            }
            plugin.load(this, jdp, schema, menuBar);
        } else {
            JOptionPane.showMessageDialog(this, "Plugin already loaded.");
        }
    }

    
    private String[] getArgs() {
        String args[] = { "ok", "ok" };
        boolean edit = pLoad.isEditable();
        String help = pLoad.getHelpFile();
        if (edit == false) {
            args[0] = "-r";
        }
        if (!("DEFAULT".equals(help) || "NONE".equals(help) || "".equals(help))) {
            String hs = "hs=" + help;
            args[1] = hs;
        }
        return args;
    }

    public void loadConfigFile(String fileName) {
        String[][] leafPairs = null;
        String[][] rendererTextPairs = null;
        String[][] rendererIconPairs = null;
        Document configDoc = null;
        configDoc = createConfigDOM(fileName);
        if (configDoc != null) {
            leafPairs = getLeafPairs(configDoc);
            rendererTextPairs = getRendererTextPairs(configDoc);
            rendererIconPairs = getRendererIconPairs(configDoc);
        } else {
            System.err.println(openErrString);
        }

        this.dialog = new DEditLeafDialog(this.curXMLDir, leafPairs, isApplet);
        renderer = new DEditTreeCellRenderer(rendererTextPairs,
                rendererIconPairs);

    }

    public void loadSchemaFile(String sFile) {
        boolean enable = false;
        if (sFile == null || sFile.length() == 0 || "NONE".equals(sFile)) {
            // agent.saveGlobalState(agent.getCID());
            loaded = false;
            allowNewFile = true;
            //allowOpenFile = true;
            //ltl modified
            //allowNewFile = false;
            allowOpenFile = false;
            allowLoadSchema = true;
            return;
        }
        try {
            enable = schema.loadSchema(sFile);
            changeEditOptions();
            // agent.saveGlobalState(agent.getCID());
            verifyItem.setEnabled(enable);

            allowNewFile = true;
            allowOpenFile = true;
            allowLoadSchema = false;
        } catch (DEditSchemaException e) {
            JOptionPane.showMessageDialog(this, "Couldn't open Schema: "
                    + e.toString());
            enable = false;
        }
    }

    private void finalizeGUI() {
        buildHelpMenu();
        ClassLoader cl = XmlEditorGUI.class.getClassLoader();
        addHelpButton(cl, XmlEditorConstants.TB_HELP);
        updateGUI();

    }

    private void buildMenu() {
        buildFileMenu();
        buildEditMenu();
        buildToolMenu();
        buildEditorMenu();
        buildPopupMenu();
    }

    private void buildToolBar() {
        ClassLoader cl = this.getClass().getClassLoader();
        addNewButton(cl, XmlEditorConstants.TB_NEW);
        addOpenButton(cl, XmlEditorConstants.TB_OPEN);
        addSaveButton(cl, XmlEditorConstants.TB_SAVE);
        addInsertBeforeButton(cl, XmlEditorConstants.TB_INSERT_BEFORE);
        addInsertChildButton(cl, XmlEditorConstants.TB_INSERT_CHILD);
        addInsertAfterButton(cl, XmlEditorConstants.TB_INSERT_AFTER);
        addSwitchButton(cl, XmlEditorConstants.TB_SWITCH);
        addEditButton(cl, XmlEditorConstants.TB_EDIT);
        addCopyButton(cl, XmlEditorConstants.TB_COPY);
        addCutButton(cl, XmlEditorConstants.TB_CUT);
        addPasteBeforeButton(cl, XmlEditorConstants.TB_PASTE_BEFORE);
        addPasteChildButton(cl, XmlEditorConstants.TB_PASTE_CHILD);
        addPasteAfterButton(cl, XmlEditorConstants.TB_PASTE_AFTER);
    }

    private void addNewButton(ClassLoader cl, String imgName) {
        ImageIcon newXMLIcon = new ImageIcon(cl.getResource(imgName));
        newXMLButton = new JButton(newXMLIcon);
        newXMLButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                makeNewFile();
            }
        });
        newXMLButton.setToolTipText("Make new XML file");
        CSH.setHelpIDString(newXMLButton, XmlEditorConstants.NEW_XML_ID);
        addIconButton(newXMLButton);
    }

    private void addOpenButton(ClassLoader cl, String imgName) {
        ImageIcon openXMLIcon = new ImageIcon(cl.getResource(imgName));
        openXMLButton = new JButton(openXMLIcon);
        openXMLButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                openXML();
            }
        });
        openXMLButton.setToolTipText("Import XML file");
        CSH.setHelpIDString(openXMLButton, XmlEditorConstants.OPEN_XML_ID);
        addIconButton(openXMLButton);
    }

    private void addSaveButton(ClassLoader cl, String imgName) {
        ImageIcon saveXMLIcon = new ImageIcon(cl.getResource(imgName));
        JButton saveXMLButton = new JButton(saveAction);
        saveXMLButton.setIcon(saveXMLIcon);
        saveXMLButton.setToolTipText("Export XML file");
        CSH.setHelpIDString(saveXMLButton, XmlEditorConstants.SAVE_XML_ID);
        addIconButton(saveXMLButton);
    }

    private void addEditButton(ClassLoader cl, String imgName) {
        JButton editValButton = new JButton(editValAction);
        ImageIcon editIcon = new ImageIcon(cl.getResource(imgName));
        editValButton.setIcon(editIcon);
        editValButton.setToolTipText("Edit item");
        CSH.setHelpIDString(editValButton, XmlEditorConstants.EDIT_VALUE_ID);
        addIconButton(editValButton);
    }

    private void addCopyButton(ClassLoader cl, String imgName) {
        JButton copyButton = new JButton(copyAction);
        ImageIcon copyIcon = new ImageIcon(cl.getResource(imgName));
        copyButton.setIcon(copyIcon);
        copyButton.setToolTipText("Copy");
        CSH.setHelpIDString(copyButton, XmlEditorConstants.COPY_ID);
        addIconButton(copyButton);
    }

    private void addCutButton(ClassLoader cl, String imgName) {
        JButton cutButton = new JButton(cutAction);
        ImageIcon cutIcon = new ImageIcon(cl.getResource(imgName));
        cutButton.setIcon(cutIcon);
        cutButton.setToolTipText("Cut");
        CSH.setHelpIDString(cutButton, XmlEditorConstants.CUT_ID);
        addIconButton(cutButton);
    }

    private void addSwitchButton(ClassLoader cl, String imgName) {
        ImageIcon switchIcon = new ImageIcon(cl.getResource(imgName));
        JButton switchButton = new JButton(switchIcon);
        switchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "Select a window first.");
                    return;
                }
                Action[] acts = window.getSwitchTagToActions();
                setUpActionList(acts, "Switch Item To", "Switch");
            }
        });
        switchButton.setToolTipText("Switch item to another item");
        CSH.setHelpIDString(switchButton, XmlEditorConstants.SWITCH_ID);
        addIconButton(switchButton);

    }

    private void addInsertBeforeButton(ClassLoader cl, String imgName) {
        ImageIcon insBeforeIcon = new ImageIcon(cl.getResource(imgName));
        JButton insBeforeButton = new JButton(insBeforeIcon);
        insBeforeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "Select a window first.");
                    return;
                }
                Action[] acts = window.getNewTagBeforeActions();
                setUpActionList(acts, "Insert Item Above", "Insert");
            }
        });
        insBeforeButton.setToolTipText("Insert item above");
        CSH
                .setHelpIDString(insBeforeButton,
                        XmlEditorConstants.INSERT_ABOVE_ID);
        addIconButton(insBeforeButton);
    }

    private void addInsertChildButton(ClassLoader cl, String imgName) {
        // Insert As Sub-Item
        ImageIcon insChildIcon = new ImageIcon(cl.getResource(imgName));
        JButton insChildButton = new JButton(insChildIcon);
        insChildButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "Select a window first.");
                    return;
                }
                Action[] acts = window.getNewChildTagActions();
                setUpActionList(acts, "Insert As Sub-Item", "Insert");
            }
        });
        insChildButton.setToolTipText("Insert sub-item");
        CSH.setHelpIDString(insChildButton, XmlEditorConstants.INSERT_CHILD_ID);
        addIconButton(insChildButton);
    }

    private void addInsertAfterButton(ClassLoader cl, String imgName) {
        ImageIcon insAfterIcon = new ImageIcon(cl.getResource(imgName));
        JButton insAfterButton = new JButton(insAfterIcon);
        insAfterButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "Select a window first.");
                    return;
                }
                Action[] acts = window.getNewTagAfterActions();
                setUpActionList(acts, "Insert Item Below", "Insert");
            }
        });
        insAfterButton.setToolTipText("Insert item below");
        CSH.setHelpIDString(insAfterButton, XmlEditorConstants.INSERT_BELOW_ID);
        addIconButton(insAfterButton);
    }

    private void addPasteBeforeButton(ClassLoader cl, String imgName) {
        JButton pasteBeforeButton = new JButton(pasteBeforeAction);
        ImageIcon pbIcon = new ImageIcon(cl.getResource(imgName));
        pasteBeforeButton.setIcon(pbIcon);
        pasteBeforeButton.setToolTipText("Paste copied item(s) above");
        CSH.setHelpIDString(pasteBeforeButton,
                XmlEditorConstants.PASTE_ABOVE_ID);
        addIconButton(pasteBeforeButton);
    }

    private void addPasteChildButton(ClassLoader cl, String imgName) {
        ImageIcon pasteChildIcon = new ImageIcon(cl.getResource(imgName));
        JButton pasteChildButton = new JButton(pasteChildIcon);
        pasteChildButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "Select a window first.");
                    return;
                }
                Action[] acts = getPasteChildActions(window);
                setUpActionList(acts, "Paste Sub-Item", "Paste");
            }
        });
        pasteChildButton.setToolTipText("Paste copied item(s) as sub-items");
        CSH
                .setHelpIDString(pasteChildButton,
                        XmlEditorConstants.PASTE_CHILD_ID);
        addIconButton(pasteChildButton);
    }

    private void addPasteAfterButton(ClassLoader cl, String imgName) {
        ImageIcon paIcon = new ImageIcon(cl.getResource(imgName));
        JButton pasteAfterButton = new JButton(pasteAfterAction);
        pasteAfterButton.setIcon(paIcon);
        pasteAfterButton.setToolTipText("Paste copied item(s) below");
        CSH
                .setHelpIDString(pasteAfterButton,
                        XmlEditorConstants.PASTE_BELOW_ID);
        addIconButton(pasteAfterButton);
    }

    private void addHelpButton(ClassLoader cl, String imgName) {
        ImageIcon helpIcon = new ImageIcon(cl.getResource(imgName));
        JButton helpButton = new JButton(helpIcon);
        helpButton.addActionListener(new CSH.DisplayHelpAfterTracking(
                helpBroker));
        addIconButton(helpButton);
    }

    /**
     * Adds a JButton (which should have an associated icon) to the icon button
     * panel at the top of the window. Any text will be removed and the icon
     * will be resized to fit existing buttons. As this is done only once,
     * before JFrame.show(), it's acceptable to use smooth scaling.
     *
     * @param button
     *            the JButton to add to the button panel
     */
    public void addIconButton(JButton button) {
        Icon icon = button.getIcon();
        if (icon != null && icon instanceof ImageIcon) {
            ImageIcon imic = (ImageIcon) icon;
            Image image = imic.getImage();
            image = image.getScaledInstance(BUTTON_ICON_SIZE, BUTTON_ICON_SIZE,
                    Image.SCALE_SMOOTH);
            imic.setImage(image);
        }
        button.setText("");
        button.setMargin(NO_MARGIN_INSETS);
        button.setSize(buttonDim);
        toolbar.add(button);
    }

    /**
     * Sets up and displays a dialog containing a JList which contains all the
     * Action's passed each. All actions are assumed to have something useful
     * provided by toString() in order to display nicely in the JList. When the
     * "Accept"-type button is chosen, the selected action is performed.
     *
     * @param actions
     *            the Action's that will be displayed in the JList
     * @param title
     *            the title for the dialog with the JList in it
     * @param acceptMessage
     *            the message to put on the "Accept" button
     */
    private void setUpActionList(Action[] actions, String title,
            String acceptMessage) {
        if (actions == null || actions.length == 0) {
            JOptionPane.showMessageDialog(this, "No options available for "
                    + "\"" + title + "\".");
            return;
        }

        final JDialog selectDialog = new JDialog(JOptionPane.getRootFrame(),
                title, true);
        JPanel mainPanel = new JPanel(new BorderLayout());
        final JList items = new JList(actions);
        // items.setEditable(false);
        items.setSelectedIndex(0);
        JScrollPane itemsPane = new JScrollPane(items);
        mainPanel.add(itemsPane, BorderLayout.NORTH);
        JButton acceptButton = new JButton(acceptMessage);
        acceptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                Action act = (Action) items.getSelectedValue();
                if (act == null)
                    return;
                act.actionPerformed(ae);
                selectDialog.dispose();
            }
        });
        mainPanel.add(acceptButton, BorderLayout.WEST);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                selectDialog.dispose();
            }
        });
        mainPanel.add(cancelButton, BorderLayout.EAST);
        selectDialog.getContentPane().add(mainPanel);
        selectDialog.pack();
        selectDialog.setLocationRelativeTo(this);
        selectDialog.show();
    }

    /**
     * I'd put it in DEditXMLWindow but copied can't be final. Returns an array
     * of Actions which represent pasting the copied DEditNode's (in order)
     * before various DEditNodes that are children of the currently selected
     * one, or after all of the currently selected one's children.
     *
     * @return an array of Actions that will paste the copied DEditNode's as a
     *         child of the currently selected node.
     */
    private Action[] getPasteChildActions(DEditXMLWindow window) {
        return window.getPasteChildActions(copied);
    }

    /**
     * Get DOM from an XML, locating the file locally only if it is not an
     * applet.
     *
     * @param xmlFileName
     *            the name of the file to open
     * @param return
     *            a DOM Document for the XML in "xmlFileName"
     */
    public Document createDOMforXML(String xmlFileName) {
        if (!isApplet)
            return createDOMforLocalXML(xmlFileName);
        String contents = readRemoteFile(xmlFileName);
        InputSource is = new InputSource(new StringReader(contents));
        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            return builder.parse(is);
        } catch (SAXException sxe) {
            Exception x = sxe;
            if (sxe.getException() != null)
                x = sxe.getException();
            openErrString = x.toString();
            return null;
        } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            openErrString = pce.toString();
            return null;
        } catch (IOException ioe) {
            openErrString = ioe.toString();
            return null;
        } catch (Exception e) {
            openErrString = e.toString();
            return null;
        }
    }

    
    public void removeXmlWindow(DEditXMLWindow window){
    	if(windows.contains(window))
    		windows.remove(window);
    }
    
    public Document createDOMfromXML(String xml) {
    	if(xml == null || xml.trim().length() == 0)
    		return null;
    	
    	try{
    		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    		return dbf.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
        } catch (SAXException sxe) {
            Exception x = sxe;
            if (sxe.getException() != null)
                x = sxe.getException();
            openErrString = x.toString();
            return null;
        } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            openErrString = pce.toString();
            return null;
        } catch (IOException ioe) {
            openErrString = ioe.toString();
            return null;
        } catch (Exception e) {
            openErrString = e.toString();
            return null;
        }
    }
    
    /**
     * Creates a DOM Document given the pathname of a local XML file.
     *
     * @param xmlFileName
     *            the filename of the local XML to be converted to DOM
     * @return a Document derived from the input local XML file
     */
    public static Document createDOMforLocalXML(String xmlFileName) {
        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            URL xmlURL = ClassLoader.getSystemResource(xmlFileName);
            if (xmlURL != null) {
                xmlFileName = xmlURL.toString();
            }
            return builder.parse(xmlFileName);
        } catch (SAXException sxe) {
            Exception x = sxe;
            if (sxe.getException() != null)
                x = sxe.getException();
            openErrString = x.toString();
            return null;
        } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            openErrString = pce.toString();
            return null;
        } catch (IOException ioe) {
            openErrString = ioe.toString();
            return null;
        } catch (Exception e) {
            openErrString = e.toString();
            return null;
        }
    }

    /**
     * Creates a DOM Document given the pathname of a local XML file.
     *
     * @param xmlFileName
     *            the filename of the local XML to be converted to DOM
     * @return a Document derived from the input local XML file
     */
    public Document createConfigDOM(String xmlFileName) {
        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            // URL xmlURL = ClassLoader.getSystemResource(xmlFileName);
            ClassLoader cl = this.getClass().getClassLoader();
            URL xmlURL = cl.getResource(xmlFileName);
            if (xmlURL != null) {
                xmlFileName = xmlURL.toString();
            }
            return builder.parse(xmlFileName);
        } catch (SAXException sxe) {
            Exception x = sxe;
            if (sxe.getException() != null)
                x = sxe.getException();
            openErrString = x.toString();
            return null;
        } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            openErrString = pce.toString();
            return null;
        } catch (IOException ioe) {
            openErrString = ioe.toString();
            return null;
        } catch (Exception e) {
            openErrString = e.toString();
            return null;
        }
    }

    /**
     * Reads the contents of a remote file and returns them as a String using
     * the servlet from BhpViewer to get the InputStream containing the data
     * (with getRemoteFileInputStream(String)).
     *
     * @param filename
     *            the name of the file whose contents will be retrieved
     * @return the contents of "filename" as a String
     */
    public String readRemoteFile(String filename) {
        try {
            InputStream is = getRemoteFileInputStream(filename);
            if (is == null) {
                return null;
            }
            StringWriter out = new StringWriter();
            int c;

            while ((c = is.read()) != -1)
                out.write(c);

            is.close();
            return out.toString();
        } catch (Exception e) {
            System.err.println("reading exception for " + filename + ": "
                    + e.toString());
        }
        return null;
    }

    /**
     * Gets the InputStream containing data from the remote file specified by
     * "filename" using the servlet from BhpViewer
     *
     * @param filename
     *            the name of the remote file whose data will be retrievable
     *            through the returned InputStream
     * @return an InputStream containing the data from file "filename"
     */
    public InputStream getRemoteFileInputStream(String filename) {
        try {
            String urlString = getProperty("servletReadFilecfgURL");
            URL url = new URL(urlString + "?FileName=" + filename);
            URLConnection conn = url.openConnection();
            InputStream is = conn.getInputStream();
            return is;
        } catch (Exception e) {
            System.err.println("error getting input stream for " + filename
                    + ": " + e.toString());
        }
        return null;
    }

    /**
     * Returns a property value, which only has meaning when running as an
     * applet or when that property has earlier been set with a call to
     * setProperty(). First this function tries to get a value from a previous
     * setProperty(). If that fails and it is an applet, it tries
     * applet.getParameters(propName).
     *
     * @param propName
     *            the name of the property to retrieve
     * @return the value for the input property
     */
    public String getProperty(String propName) {
        String value = (String) properties.get(propName);
        if (value != null)
            return value;
        if (applet == null || !isApplet)
            return null;
        return applet.getParameter(propName);
    }

    /**
     * Sets the value for a property so that the next call to getProperty() for
     * that property name will return the value input here. For an applet this
     * overrides the property of the parameter by name "propName " for that
     * applet
     *
     * @param propName
     *            the name of the property to set
     * @param value
     *            the value to set property "propName" to
     */
    public void setProperty(String propName, String value) {
        if (propName != null && value != null)
            properties.put(propName, value);

    }

    /**
     * Returns whether or not the editor is running as an applet.
     *
     * @return whether or not the editor is running as an applet
     */
    public boolean isApplet() {
        return isApplet;
    }

    /**
     * Gets an array of 2-sized arrays from the Document d which correspond to
     * the key and value pairs for customization of the edit dialog box
     *
     * @param d
     *            the Document to derived the pairs from
     * @return an array of pairs of strings which are key, value pairs
     */
    private String[][] getLeafPairs(Document d) {
        return getPairs("edit_dialog", d);
    }

    /**
     * Derives an array of pairs of strings which are key, value pairs for
     * customizing some aspect of the editor. It gets the pairs for the feature
     * specified by tagName (i.e., <tagName>) found in Document d.
     *
     * @param tagName
     *            the tag name to search for customizations for
     * @param d
     *            the Document in which to search for pairs
     * @return an array of pairs of strings which are key, value pairs
     */
    private String[][] getPairs(String tagName, Document d) {
        NodeList nl = d.getElementsByTagName(tagName);
        if (nl != null && nl.getLength() > 0) {
            Node n = nl.item(0);
            NodeList customs = n.getChildNodes();
            Vector<Object> forRet = new Vector<Object>();
            for (int i = 0; i < customs.getLength(); i++) {
                Node custom = customs.item(i);
                if (custom.getNodeType() == Node.ELEMENT_NODE) {
                    String name = null, value = null, effect = null;
                    NodeList vals = custom.getChildNodes();
                    for (int j = 0; j < vals.getLength(); j++) {
                        Node temp = vals.item(j);
                        String nodeName = temp.getNodeName();
                        if (nodeName.equals("name")) {
                            name = temp.getFirstChild().getNodeValue();
                        } else if (nodeName.equals("value")) {
                            value = temp.getFirstChild().getNodeValue();
                        } else if (nodeName.equals("effect")) {
                            effect = temp.getFirstChild().getNodeValue();
                        } else if (nodeName.equals("any_value")) {
                            value = "";
                        }
                    }
                    if (name != null && value != null && effect != null) {
                        String[] customization = new String[] {
                                name + "=" + value, effect };
                        forRet.add(customization);
                    }
                }
            }
            String[][] ret = new String[forRet.size()][2];
            forRet.toArray(ret);
            return ret;
        } else {
            return null;
        }
    }

    /**
     * Gets an array of 2-sized arrays from the Document d which correspond to
     * the key and value pairs for customization of the display of text for
     * elements in the JTree
     *
     * @param d
     *            the Document to derived the pairs from
     * @return an array of pairs of strings which are key, value pairs
     */
    private String[][] getRendererTextPairs(Document d) {
        return getPairs("tree_text_display", d);
    }

    /**
     * Gets an array of 2-sized arrays from the Document d which correspond to
     * the key and value pairs for customization of the display of icons for
     * elements in the JTree
     *
     * @param d
     *            the Document to derived the pairs from
     * @return an array of pairs of strings which are key, value pairs
     */
    private String[][] getRendererIconPairs(Document d) {
        NodeList nl = d.getElementsByTagName("tree_icon_display");
        if (nl != null && nl.getLength() > 0) {
            Node n = nl.item(0);
            NodeList customs = n.getChildNodes();
            Vector<Object> forRet = new Vector<Object>();
            for (int i = 0; i < customs.getLength(); i++) {
                Node custom = customs.item(i);
                String icon_name = null, node_names = null;
                NodeList vals = custom.getChildNodes();
                for (int j = 0; j < vals.getLength(); j++) {
                    Node temp = vals.item(j);
                    String nodeName = temp.getNodeName();
                    if (nodeName.equals("icon_name")) {
                        icon_name = temp.getFirstChild().getNodeValue();
                    } else if (nodeName.equals("nodes_for_icon")) {
                        node_names = temp.getFirstChild().getNodeValue();
                    } else {

                    }
                }
                if (icon_name != null && node_names != null) {
                    String[] customization = new String[] { icon_name,
                            node_names };
                    forRet.add(customization);
                }
            }
            String[][] ret = new String[forRet.size()][2];
            forRet.toArray(ret);
            return ret;
        } else {
            return null;
        }
    }

    public void setStdOutTextArea(String text) {
        // final String txt = text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // stdOutTextArea.setText(txt);
            }
        });

    }

    public void setStdErrTextArea(String text) {
        // final String txt = text;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // stdErrTextArea.setText(txt);
            }
        });

    }

    /**
     * Rename the plugin
     *
     * @param name
     *            from plugin message processor
     */
    public void renamePlugin(String name) {
//        this.setTitle(name);
        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        resetTitle(QiProjectDescUtils.getQiProjectName(qpDesc));
    }

    private void nodeHelpAction() {
        nodeHelpAction = new AbstractAction("Get Help For Item") {
            static final long serialVersionUID = 1l;

            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow selWin = getSelectedWindow();
                if (selWin == null)
                    return;

                DEditNode selected = selWin.getSelected();
                if (selected == null)
                    return;

                String name = selected.getNode().getNodeName();
                try {
                    helpBroker.setCurrentID(name);
                    helpBroker.setDisplayed(true);
                } catch (BadIDException bie) {
                    String message = "No help available.";
                    JOptionPane.showMessageDialog(XmlEditorGUI.this, message);
                    return;
                } catch (javax.help.UnsupportedOperationException e) {
                    DEditDebug.println(this, 6, "couldn't show help");
                }
            }
        };
    }
    private JMenuItem saveAsItem;
    private void buildFileMenu() {
        nodeHelpAction();

        JMenu fileMenu = new JMenu("File");
        // new
        newItem = new JMenuItem("New");
        newItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                makeNewFile();
            }
        });
        fileMenu.add(newItem);

        // Load Schema
        loadSchemaItem = new JMenuItem("Load Schema File");// load
        loadSchemaItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if(windows != null && windows.size() > 0){
                	int st = JOptionPane.showConfirmDialog(gui,"All the current XML windows will be closed before loading a new schema. You might lose some xml data. Are you sure you want to proceed?","Switch Package Confirmation",JOptionPane.YES_NO_OPTION);
                    if(st == JOptionPane.NO_OPTION)
                    	return;
                    closeAllXMLWindows();
                }
                String filename; // have user select .xsd Schema file
                JFileChooser chooser = new JFileChooser(curXSDDir);
                chooser.setDialogTitle("Load Schema File");
                chooser.setAcceptAllFileFilterUsed(true);
                DEditSimpleFileFilter filter = new DEditSimpleFileFilter(
                        ".xsd", "XML Schema Files");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(XmlEditorGUI.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = chooser.getSelectedFile();
                    curXSDDir = file.getParentFile();
                    filename = file.getAbsolutePath();
                } else {
                    return;
                }
                boolean enable;
                try {
                    enable = schema.loadSchema(filename);
                    pLoad.setSchemaFile(filename);
                    for (int i = 0; i < windows.size(); i++) {
                        DEditXMLWindow window = (DEditXMLWindow) windows.get(i);
                        window.update(false); // validate etc.
                    }
                    // agent.saveGlobalState(agent.getCID());
                } catch (DEditSchemaException e) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "Couldn't open Schema: " + e.toString());
                    enable = false;
                }

                loaded = true;
                allowNewFile = true;
                allowOpenFile = true;
                allowLoadSchema = false;
                // reset affected components
                changeEditOptions();
                verifyItem.setEnabled(enable);
                updateGUI();
            }
        });
        fileMenu.add(loadSchemaItem);

        // import
        fileMenu.addSeparator();
        importMenuItem = new JMenuItem("Import");
        importMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                openXML();
            }
        });
        fileMenu.add(importMenuItem);

        // export
        exportMenuItem = new JMenuItem("Export");
        exportMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "Nothing changed /Or File empty, no action taken");
                    return;
                }
                String openFilename = getSaveFile();
                if (openFilename == null) {
                    return;
                }
                window.saveFile(openFilename);
            }
        });
        exportMenuItem.setEnabled(false);
        fileMenu.add(exportMenuItem);

        fileMenu.addSeparator();

        quit = new JMenuItem("Quit");
        quit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
            	if(agent.getRequestMsgManager() != null){
            		ArrayList params = new ArrayList();
                    params.add(null);
                    params.add(null);
                    IComponentDescriptor customer = agent.getRequestMsgManager().getMyComponentDesc();
                    agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_COMPONENT_CMD, customer, QIWConstants.ARRAYLIST_TYPE, params);
            	}
            	if (fromParent) 
                	agent.deactivateSelf();
                gui.setVisible(false);
                dispose();
            }
        });
        fileMenu.add(quit);

        // save
        saveAction = new AbstractAction("Save") {
            static final long serialVersionUID = 1l;
            /*//original implementation
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "File Unchanged /Or Empty, no action taken");
                    return;
                }
                saveXML();
                agent.saveMailConfigFile(true);
                agent.savePkgConfigFile(true);
            }
            */
            
            public void actionPerformed(ActionEvent ae) {
            	if(agent.getRequestMsgManager() == null)
            		agent.saveState();
            	else{
            		DEditXMLWindow window = getSelectedWindow();
                    if (window == null) {
                        JOptionPane.showMessageDialog(XmlEditorGUI.this,
                                "No XML Window selected or found, no action taken");
                        return;
                    }
                    ArrayList params = new ArrayList();
                    Document doc = createDOMfromXML(window.toXMLString());
                    params.add(doc);
                    params.add(agent.getMessagingMgr());
                    IComponentDescriptor customer = agent.getRequestMsgManager().getMyComponentDesc();
                    agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_COMPONENT_CMD, customer, QIWConstants.ARRAYLIST_TYPE, params);
            	}
            }
            
        };
        JMenuItem saveItem = new JMenuItem(saveAction);
        saveAction.setEnabled(false); // not enabled until file is opened
        fileMenu.add(saveItem);
        
//      save as
        saveAsAction = new AbstractAction("Save As") {
            static final long serialVersionUID = 1l;

            public void actionPerformed(ActionEvent ae) {
            	agent.saveStateAsClone();
            }
        };
        saveAsItem = new JMenuItem(saveAsAction);
        //saveAsItem.setEnabled(false);
        saveAction.setEnabled(false); // not enabled until file is opened
        fileMenu.add(saveAsItem);

        
        // Save and Exit
        saveQuit = new JMenuItem("Save,Quit");
        saveQuit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                /*DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    JOptionPane.showMessageDialog(XmlEditorGUI.this,
                            "File Unchanged /Or Empty, no action taken");
                } else {
                  saveXML();
                  agent.saveMailConfigFile(true);
                  agent.savePkgConfigFile(true);
                }
                if (fromParent) agent.deactivateSelf();
                gui.setVisible(false);
                dispose();
                */
            	if(agent.getRequestMsgManager() != null){
            		DEditXMLWindow window = getSelectedWindow();
                    if (window == null) {
                        JOptionPane.showMessageDialog(XmlEditorGUI.this,
                                "No XML Window selected or found, no action taken");
                        return;
                    }
            		ArrayList params = new ArrayList();
            		Document doc = createDOMfromXML(window.toXMLString());
                    params.add(doc);
                    params.add(null);
                    IComponentDescriptor customer = agent.getRequestMsgManager().getMyComponentDesc();
                    agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_COMPONENT_CMD, customer, QIWConstants.ARRAYLIST_TYPE, params);
                    if (fromParent) 
                    	agent.deactivateSelf();
                    gui.setVisible(false);
                    dispose();
            	}else
            	agent.saveStateThenQuit();
            }
        });
        fileMenu.add(saveQuit);


        menuBar.add(fileMenu);
    }

    private void buildEditMenu() {
        JMenu editMenu = new JMenu("Edit");
        // clear submenus every time invoked
        editMenu.addMenuListener(new MenuListener() {
            public void menuCanceled(MenuEvent e) {
            }

            public void menuDeselected(MenuEvent e) {
            }

            public void menuSelected(MenuEvent e) {
                newTagBeforeMenu.removeAll();
                newChildTagMenu.removeAll();
                newTagAfterMenu.removeAll();
                switchTagToMenu.removeAll();
                pasteChildMenu.removeAll();
            }
        });

        // create sub menus and add listeners to determine contents
        // on selection
        newTagBeforeMenu = new JMenu("Insert Item Above...");
        newChildTagMenu = new JMenu("Insert Sub-Item...");
        newTagAfterMenu = new JMenu("Insert Item Below...");
        switchTagToMenu = new JMenu("Switch Item To...");
        renameMenuItem = new JMenuItem("Rename");
        final Component comp = this;
        renameMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		DEditXMLWindow window = getSelectedWindow();
        		if(window == null){
        			JOptionPane.showMessageDialog(comp,
        	              "Must select a XML window to perform rename.",
        	              "Rename Error", JOptionPane.WARNING_MESSAGE);
        			return;
        		}
        		String name = (String) JOptionPane.showInputDialog(comp, "New Title:",window.getTitle());
        		//if (name != null && name.trim().length() == 0) {
        		//	JOptionPane.showMessageDialog(comp,
                //    "The title can not be empty.",
                //    "Invalid data entry", JOptionPane.WARNING_MESSAGE);
        		//	return;
        		//}
        		if(name == null)
        			name = "";
        		window.getTreeFrame().setTitle(name);
        	}
        });
        pasteChildMenu = new JMenu("Paste As Sub-Item...");
        EditMenuListener eml = new EditMenuListener(newTagBeforeMenu,
                EditMenuListener.INSERT_ABOVE_TYPE);
        newTagBeforeMenu.addMenuListener(eml);

        eml = new EditMenuListener(newChildTagMenu,
                EditMenuListener.INSERT_CHILD_TYPE);
        newChildTagMenu.addMenuListener(eml);

        eml = new EditMenuListener(newTagAfterMenu,
                EditMenuListener.INSERT_BELOW_TYPE);
        newTagAfterMenu.addMenuListener(eml);

        eml = new EditMenuListener(switchTagToMenu,
                EditMenuListener.SWITCH_TYPE);
        switchTagToMenu.addMenuListener(eml);

        eml = new EditMenuListener(pasteChildMenu,
                EditMenuListener.PASTE_CHILD_TYPE);
        pasteChildMenu.addMenuListener(eml);

        // create Actions
        editValAction = new EditAction("Edit Value...",
                EditAction.EDIT_TAG_TYPE);
        copyAction = new EditAction("Copy", EditAction.COPY_TYPE);
        cutAction = new EditAction("Cut", EditAction.CUT_TYPE);
        undoAction = new EditAction("Undo", EditAction.UNDO_TYPE);
        pasteBeforeAction = new EditAction("Paste Before",
                EditAction.PASTE_BEFORE_TYPE);
        pasteAfterAction = new EditAction("Paste After",
                EditAction.PASTE_AFTER_TYPE);
        // disable nearly everything until at least a file is opened
        newChildTagMenu.setEnabled(false);
        newTagBeforeMenu.setEnabled(false);
        newTagAfterMenu.setEnabled(false);
        switchTagToMenu.setEnabled(false);
        editValAction.setEnabled(false);
        copyAction.setEnabled(false);
        cutAction.setEnabled(false);
        undoAction.setEnabled(false);
        pasteChildMenu.setEnabled(false);
        pasteBeforeAction.setEnabled(false);
        pasteAfterAction.setEnabled(false);
        // add them all to the Edit menu
        editMenu.add(nodeHelpAction);
        editMenu.add(undoAction);
        editMenu.add(editValAction);
        editMenu.add(copyAction);
        editMenu.add(cutAction);
        editMenu.add(pasteBeforeAction);
        editMenu.add(pasteAfterAction);
        editMenu.add(pasteChildMenu);
        editMenu.add(newChildTagMenu);
        editMenu.add(newTagBeforeMenu);
        editMenu.add(newTagAfterMenu);
        editMenu.add(switchTagToMenu);
        editMenu.add(renameMenuItem);
        menuBar.add(editMenu);
    }

    private void buildToolMenu() {
        JMenu toolsMenu = new JMenu("Tools");
        // Verify
        verifyItem = new JMenuItem("Verify");
        verifyItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                verify();
            }
        });
        // disabled until Schema is loaded and file is open
        verifyItem.setEnabled(false);
        toolsMenu.add(verifyItem);

        // Show XML
        JMenuItem showXMLItem = new JMenuItem("Show XML Text");
        showXMLItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                DEditXMLWindow window = getSelectedWindow();
                if (window == null) {
                    DEditDebug.println(this, 3, "no active XML window");
                    return;
                }
                window.showTextFrame();
            }
        });
        toolsMenu.add(showXMLItem);
        menuBar.add(toolsMenu);
    }

    public Vector<DEditXMLWindow> getWindows(){
    	return windows;
    }
    
    public void closeAllXMLWindows(){
    	for(int i = 0; windows != null && i < windows.size(); i++){
    		DEditXMLWindow window = windows.get(i);
    		window.getTreeFrame().setVisible(false);
    	}
    	windows.clear();
    }
    
    public void switchPackage(PackageLoad pLoad){
    	this.pLoad = pLoad;
    	if (pLoad.getKeyValue("fromParent") != null) {
            fromParent = true;
        }
    	Component[] components = this.getContentPane().getComponents();
    	this.getContentPane().remove(toolbar);
    	this.getContentPane().remove(jdp);
        initialize(prefWith,prefHeight);
        if (pLoad.getSchemaFile() != null && 
            pLoad.getSchemaFile().equals("xsd/freeform.xsd")) {
            this.allowLoadSchema = true;
        }
        
        buildGUI();
        
        plugin = null;
        loadEditorPlugin();
        loadSchemaFile(pLoad.getSchemaFile());
        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        resetTitle(QiProjectDescUtils.getQiProjectName(qpDesc));
        finalizeGUI();
        
    }
    private JMenu pluginMenu;
    private void buildEditorMenu() {
        pluginMenu = new JMenu("Packages");
        JMenu buildin = new JMenu("BuiltIns");
        pluginMenu.add(buildin);
        List<NodePackage> bList = agent.getPkgConfig().getBuildinPackages();
        PackageAction pAction = null;
        JMenuItem item = null;
        NodePackage info = null;
        String temp = null;
        int size = bList.size();
        for (int i = 0; i < size; i++) {
            info = bList.get(i);
            temp = info.getDisplayName();
            item = new JMenuItem(temp);
            pAction = new PackageAction(this, temp);
            item.addActionListener(pAction);
            buildin.add(item);
        }

        JMenu custom = new JMenu("Custom");
        pluginMenu.add(custom);
        List<NodePackage> cList = agent.getPkgConfig().getCustomPackages();
        size = cList.size();
        if (size == 0) {
            item = new JMenuItem("NONE");
            custom.add(item);
        } else {
            for (int i = 0; i < size; i++) {
                info = cList.get(i);
                temp = info.getDisplayName();
                item = new JMenuItem(temp);
                pAction = new PackageAction(this, temp);
                item.addActionListener(pAction);
                custom.add(item);
            }
        }

        JMenuItem importM = new JMenuItem("Import ...");
        importM.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fc = new JFileChooser();
                EditorFileFilter filter = new EditorFileFilter(new String[] {
                        "qid", "QID", "qic", "QIC" }, "Distribution");
                fc.addChoosableFileFilter(filter);
                fc.setFileFilter(filter);
                fc.setFileHidingEnabled(false);
                // fc.setControlButtonsAreShown(false);
                // show the filechooser
                // Frame parent = (Frame)
                // SwingUtilities.getAncestorOfClass(Frame.class, gui);

                int retVal = fc.showOpenDialog(new JFrame());
                if (retVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    String fname = file.getName();
                    String pkgSet = fname.substring(0, fname.length() - 4);
                    agent.getPrepare().bhpUnZip(fname, file.getAbsolutePath());
                    agent.activateEditorPlugin("", pkgSet);
                }
            }
        });

        JMenu dfault = new JMenu("Set Default");
        String dPack = agent.getPkgConfig().getActivePackage();
        for (int i = 0; i < bList.size(); i++) {
            info = bList.get(i);
            temp = info.getDisplayName();
            item = new JRadioButtonMenuItem(temp);
            DefaultAction dAction = new DefaultAction(this, temp);
            item.addActionListener(dAction);
            dfault.add(item);
            if (dPack.equals(temp)) {
                item.setSelected(true);
            }
            pGroup.add(item);
        }

        for (int i = 0; i < cList.size(); i++) {
            info = cList.get(i);
            temp = info.getDisplayName();
            item = new JRadioButtonMenuItem(temp);
            DefaultAction dAction = new DefaultAction(this, temp);
            item.addActionListener(dAction);
            dfault.add(item);
            if (dPack.equals(temp)) {
                item.setSelected(true);
            }
            pGroup.add(item);
        }

        pluginMenu.add(importM);
        pluginMenu.addSeparator();
        pluginMenu.add(dfault);
        pluginMenu.addSeparator();

        JMenuItem pManager = new JMenuItem("Edit ...");
        pManager.addActionListener(new LoadPluginAction(this));

        pluginMenu.add(pManager);
        menuBar.add(pluginMenu);
    }

    public void buildHelpMenu() {
        JMenu helpMenu = new JMenu("Help");
        JMenuItem aboutItem = new JMenuItem("About...");
        final Component comp = this;
        aboutItem.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(props == null){
                    String compName = agent.getMessagingMgr().getRegisteredComponentDisplayNameByDescriptor(agent.getComponentDescriptor());
                    props = ComponentUtils.getComponentVersionProperies(compName);
                    new XmlEditorAbout(comp,props);
                }

            }
        });
        helpMenu.add(aboutItem);

        JMenuItem configItem = new JMenuItem("More About...");
        configItem.addActionListener(new XmlEditorConfig(this));
        helpMenu.add(configItem);

        JMenuItem getHelpItem = new JMenuItem("Open Help...");
        getHelpItem
                .addActionListener(new CSH.DisplayHelpFromSource(helpBroker));
        helpMenu.add(getHelpItem);
        menuBar.add(helpMenu);
    }

    private void buildPopupMenu() {
        popup = new JPopupMenu();
        popup.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuCanceled(PopupMenuEvent e) {
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                newTagBeforePopupMenu.removeAll();
                newChildTagPopupMenu.removeAll();
                newTagAfterPopupMenu.removeAll();
                switchTagToPopupMenu.removeAll();
                pasteChildPopupMenu.removeAll();
            }
        });
        // create and disable sub menus
        newTagBeforePopupMenu = new JMenu("Insert Item Above...");
        newTagBeforePopupMenu.setEnabled(false);
        newChildTagPopupMenu = new JMenu("Insert Sub-Item...");
        newChildTagPopupMenu.setEnabled(false);
        newTagAfterPopupMenu = new JMenu("Insert Item Below...");
        newTagAfterPopupMenu.setEnabled(false);
        pasteChildPopupMenu = new JMenu("Paste Sub-Item...");
        pasteChildPopupMenu.setEnabled(false);
        switchTagToPopupMenu = new JMenu("Switch Item To...");
        switchTagToPopupMenu.setEnabled(false);
        // add listeners to fill menus on selection
        EditMenuListener eml = new EditMenuListener(newTagBeforePopupMenu,
                EditMenuListener.INSERT_ABOVE_TYPE);
        newTagBeforePopupMenu.addMenuListener(eml);

        eml = new EditMenuListener(newChildTagPopupMenu,
                EditMenuListener.INSERT_CHILD_TYPE);
        newChildTagPopupMenu.addMenuListener(eml);

        eml = new EditMenuListener(newTagAfterPopupMenu,
                EditMenuListener.INSERT_BELOW_TYPE);
        newTagAfterPopupMenu.addMenuListener(eml);

        eml = new EditMenuListener(switchTagToPopupMenu,
                EditMenuListener.SWITCH_TYPE);
        switchTagToPopupMenu.addMenuListener(eml);

        eml = new EditMenuListener(pasteChildPopupMenu,
                EditMenuListener.PASTE_CHILD_TYPE);
        pasteChildPopupMenu.addMenuListener(eml);

        // add them all to the popup Edit menu
        popup.add(nodeHelpAction);
        popup.add(undoAction);
        popup.add(editValAction);
        popup.add(copyAction);
        popup.add(cutAction);
        popup.add(pasteBeforeAction);
        popup.add(pasteAfterAction);
        popup.add(pasteChildPopupMenu);
        popup.add(newChildTagPopupMenu);
        popup.add(newTagBeforePopupMenu);
        popup.add(newTagAfterPopupMenu);
        popup.add(switchTagToPopupMenu);
        // create double click invokes edit dialog listener. It is added
        // to every DEditXMLWindow's JTree.
        ml = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                // should this do more thorough click checking / change
                // selection / etc.???
                if (e.getClickCount() == 2 && editValAction.isEnabled()) {
                    editTag(); // change for edit in place ???
                }
            }
        };
    }

    /**
     * Creates a new XML file based on the currently loaded XML Schema.
     */
    private void makeNewFile() {
//    	 create new document
        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = dbfactory.newDocumentBuilder();
        } catch (ParserConfigurationException pce) {
            JOptionPane.showMessageDialog(this, "Can't make new file : "
                    + pce.toString());
            return;
        }
        Document document = builder.newDocument();
        if (!schema.isReady()) {
            //JOptionPane.showMessageDialog(this,"Please load an XML Schema template first.");
            //JOptionPane.showMessageDialog(this,"free-form editing is not yet fully supported.");
            DEditXMLWindow newWindow = new DEditXMLWindow(this, jdp, schema,
                    renderer, document, null, plugin, true, true);
            // if there are too many windows open, dispose the new one
            if (windows.size() < MAX_WINDOWS) {
                windows.add(newWindow);
            } else {
                JOptionPane.showMessageDialog(this,
                        "Too many windows open.  Close "
                                + "some before opening any new windows.");
                newWindow.dispose();
            }
            // update the editing options to those of the new window and reset
            // affected compoenents
            changeEditOptions(newWindow);
            // copied = null;
            //resetFrameString();
            resetTitle(project);
            exportMenuItem.setEnabled(true);
            saveAction.setEnabled(true);
            return;
        }
        
        // create top of document, put it in, then create a DEditXMLWindow
        // from that and other important components
        Node n = document.createElement(schema.getTopName());
        document.appendChild(n);
        MouseListener[] mls = new MouseListener[] { ml, popupListener };
        DEditXMLWindow newWindow = new DEditXMLWindow(this, jdp, schema,
                renderer, document, mls, plugin, true);
        // if there are too many windows open, dispose the new one
        if (windows.size() < MAX_WINDOWS) {
            windows.add(newWindow);
        } else {
            JOptionPane.showMessageDialog(this,
                    "Too many windows open.  Close "
                            + "some before opening any new windows.");
            newWindow.dispose();
        }
        // update the editing options to those of the new window and reset
        // affected compoenents
        changeEditOptions(newWindow);
        // copied = null;
        //resetFrameString();
        resetTitle(project);
        exportMenuItem.setEnabled(true);
        saveAction.setEnabled(true);
    }

    /**
     * Opens an XML file by displaying a file chooser, getting a file, and then
     * calling openFile() on the filename chosen. Uses RemoteFileChooser when
     * running as an applet.
     */
    public void openXML() {
        JFileChooser chooser = new JFileChooser(curXMLDir);
        chooser.setDialogTitle("Import XML File");
        chooser.setAcceptAllFileFilterUsed(true);
        DEditSimpleFileFilter filter = new DEditSimpleFileFilter(".xml",
                "XML Files");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(XmlEditorGUI.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            curXMLDir = f.getParentFile();

            openFile(f.getAbsolutePath());

        }
    }

    /**
     * Saves an XML file. First it check to see if it has been saved yet. If so,
     * saves it to its current name. Otherwise it prompts the user with a file
     * chooser dialog.
     */
    private void saveXML() {
        DEditXMLWindow window = getSelectedWindow();
        String openFilename = null;
        if (window.unsaved()) {
            openFilename = getSaveFile();
            if (openFilename == null)
                return;
        } else {
            openFilename = window.getOpenFilename();
        }
        window.saveFile(openFilename);
    }

    public void open2Files(String schemaFile, String xmlFile) {
        loadSchemaFile(schemaFile);
        openFile(xmlFile);
    }

    /**
     * Opens an XML file given a filename.
     *
     * @param filename
     *            the name of the XML file to open.
     */
    public void openFile(String filename) {
        if (filename == null || filename.length() == 0)
            return;
        if (!loaded)
            return;

        if (filename.indexOf(".xml") < 0) {
            makeNewFile();
            return;
        }

        File f = new File(filename);
        String destPath = agent.getPrepare().getDirByName(DistConst.XML);

        if (!destPath.equalsIgnoreCase(f.getParent())) {
            destPath += File.separator + f.getName();
            try {
                agent.getPrepare().copyFile(f.getAbsolutePath(), destPath);
            } catch (IOException ex) {
                logger.info("Error at Copying File: ");
                ex.printStackTrace();
            }
        } else {
            destPath = filename;
        }

        openedXml = destPath;
        agent.getPkgConfig().setLastXml(openedXml);

        PluginModel model = agent.getPkgConfig();
        NodeFile node = new NodeFile();
        node.setDisplayName(f.getName());
        node.setFilePath(destPath);
        model.addXml(node);

        filename = destPath;
        Document document = createDOMforXML(filename);
        if (document == null) {
            JOptionPane.showMessageDialog(this, "Couldn't open file: "
                    + openErrString);
            return;
        }
        // just like makeNewFile from here on
        MouseListener[] mls = new MouseListener[] { ml, popupListener };
        DEditXMLWindow newWindow = new DEditXMLWindow(this, jdp, schema,
                filename, renderer, document, mls, plugin);

        if (newWindow == null) {
            return;
        }
        if (windows.size() < MAX_WINDOWS) {
            windows.add(newWindow);
        } else {
            JOptionPane.showMessageDialog(this,
                    "Too many windows open.  Close "
                            + "some before opening any new windows.");
            newWindow.dispose();
        }
        changeEditOptions(newWindow);
        //resetFrameString();
        resetTitle(project);
        saveAction.setEnabled(true);
        exportMenuItem.setEnabled(true);
        if (schema.isReady()) {
            verifyItem.setEnabled(true);
        } else {
            verifyItem.setEnabled(false);
        }

        NodeFile info = new NodeFile(filename);
        info.setStatus(1);

        // agent.updateFile("xml", info);
    }

    private String nodeToXMLString(Node node){
    	String xml = "";
    	if(node == null)
    		return xml;
    	try {   
    		TransformerFactory tFactory = TransformerFactory.newInstance();
    		Transformer tranformer = tFactory.newTransformer();
    	   	DOMSource source = new DOMSource(node);
    	   	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	   	StreamResult result = new StreamResult(baos);
    	   	tranformer.transform(source,result);
    	   	xml = baos.toString();
        } catch (TransformerConfigurationException tce) {
        	tce.printStackTrace();
        } catch (TransformerException te) {
        	te.printStackTrace();
        }
        return xml;
    }
    
    /**
     * Opens an XML file given a filename.
     *
     * @param filename
     *            the name of the XML file to open.
     */
    public void buildWindowsFromXMLNode(Node node) {
       //if (!loaded)
       //     return;
        
       NodeList children = node.getChildNodes();
       String title = "";
       for(int i = 0; children != null && i < children.getLength(); i++){
    	   Node child = children.item(i);
           if(child.getNodeType() == Node.ELEMENT_NODE){
               if(child.getNodeName().equals("window")){
                   Element el = (Element) child;
                   title = el.getAttribute("title");
                   
                   String hStr = el.getAttribute("height");
                   int height = Integer.valueOf(hStr).intValue();
                   String wStr = el.getAttribute("width");
                   int width = Integer.valueOf(wStr).intValue();

                   String X = el.getAttribute("x");
                   int x = Integer.valueOf(X).intValue();
                   String Y = el.getAttribute("y");
                   int y = Integer.valueOf(Y).intValue();

                   boolean free = false;
                   String freeform = el.getAttribute("freeform");
                   if(freeform != null && freeform.equals("yes"))
                	   free = true;
                   
                   if(!loaded && !free)
                	   continue;
                   String xml = nodeToXMLString(child.getFirstChild());
		           Document doc = createDOMfromXML(xml);
                   
		           String destPath = agent.getPrepare().getDirByName(DistConst.XML);

		           PluginModel model = agent.getPkgConfig();

		           // just like makeNewFile from here on
		           MouseListener[] mls = new MouseListener[] { ml, popupListener };
		           DEditXMLWindow newWindow = null;
		           
		           if(!free)
		        	   newWindow = new DEditXMLWindow(this, jdp, schema,
		        		   title, renderer, doc, mls, plugin);
		           else
		        	   newWindow = new DEditXMLWindow(this, jdp, schema,
		                       renderer, doc, null, plugin, false, true);
		           

        
		           newWindow.getTreeFrame().setLocation(x,y);
		           newWindow.getTreeFrame().setSize(width,height);

		           if (newWindow == null) {
		        	   return;
		           }
		           if (windows.size() < MAX_WINDOWS) {
		        	   windows.add(newWindow);
		           } else {
		        	   JOptionPane.showMessageDialog(this,
		        			   "Too many windows open.  Close "
		        			   + "some before opening any new windows.");
		        	   newWindow.dispose();
		           }
		           changeEditOptions(newWindow);
		           //resetFrameString();
		           resetTitle(project);
		           saveAction.setEnabled(true);
		           exportMenuItem.setEnabled(true);
		           if (schema.isReady()) {
		        	   verifyItem.setEnabled(true);
		           } else {
		        	   verifyItem.setEnabled(false);
		           }
               }
           }
       }

        //NodeFile info = new NodeFile(filename);
        //info.setStatus(1);

        // agent.updateFile("xml", info);
    }
    
    
    public void resetMenuItems(boolean enable){
    	pluginMenu.setEnabled(enable);
    	newItem.setEnabled(enable);
    	importMenuItem.setEnabled(enable);
    	saveAsItem.setEnabled(enable);
    	newXMLButton.setEnabled(enable);
    	openXMLButton.setEnabled(enable);
    }
    public void buildXmlEditorFromXMLNode1(Node node) {
    	if(node == null)
    		return;
    	Document doc = (Document)node;
                
    	//String destPath = agent.getPrepare().getDirByName(DistConst.XML);

    	//PluginModel model = agent.getPkgConfig();

       // just like makeNewFile from here on
        MouseListener[] mls = new MouseListener[] { ml, popupListener };
        //DEditXMLWindow newWindow = null;
        //if(windows != null && windows.size() > 0){       
        //	newWindow = windows.get(0);
        //	DefaultTreeModel tmodel = new DefaultTreeModel(DEditNode.EMPTY_NODE);
        //	DEditNode rootNode = new DEditNode(doc.getDocumentElement(),
		//			   tmodel);
        //	tmodel.setRoot(rootNode);
        //	newWindow.setTreeModel(tmodel);
        //}else   
        DEditXMLWindow	newWindow = new DEditXMLWindow(this, jdp, schema,
        		   title, renderer, doc, mls, plugin);

 

        if (newWindow == null) {
       	   return;
        }
        //if (windows.size() < MAX_WINDOWS) {
    	//   windows.add(newWindow);
        //} else {
    	//   JOptionPane.showMessageDialog(this,
    	//		   "Too many windows open.  Close "
		//	   + "some before opening any new windows.");
    	//   newWindow.dispose();
        //}
        if(windows.size() == 0)
        	windows.add(newWindow);
        else
        	windows.set(0, newWindow);
        changeEditOptions(newWindow);
       //resetFrameString();
        resetTitle(project);
        saveAction.setEnabled(true);
        exportMenuItem.setEnabled(true);
        if (schema.isReady()) {
        	verifyItem.setEnabled(true);
        } else {
        	verifyItem.setEnabled(false);
        }
     }    
    /**
     * Changes the available options under the edit menu based on the currently
     * selected DEditXMLWindow.
     */
    public void changeEditOptions() {
        DEditXMLWindow current = getSelectedWindow();
        if (current != null) {
            changeEditOptions(current);
        } else {
            changeEditOptions(null, null);
        }
    }

    /**
     * Helper or regularly called method which changes the edit options for the
     * passed in DEditXMLWindow which should in all cases ==
     * getSelectedWindow().
     *
     * @param window
     *            the currently selected DEditXMLWindow, == getSelectedWindow()
     */
    public void changeEditOptions(DEditXMLWindow window) {
        DEditNode[] selectedNodes = window.getMultipleSelected();
        changeEditOptions(window, selectedNodes);
    }

    /**
     * Gets the name of a file to use to save the currently open XML. Uses
     * RemoteFileChooser when running as an applet.
     *
     * @return a name of a file to use to save the currently open XML
     */
    public String getSaveFile() {
        JFileChooser chooser = new JFileChooser(curXMLDir);
        chooser.setDialogTitle("Export XML File");
        chooser.setAcceptAllFileFilterUsed(true);
        DEditSimpleFileFilter filter = new DEditSimpleFileFilter(".xml",
                "XML Files");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            curXMLDir = f.getParentFile();
            return f.getAbsolutePath();
        } else {
            return null;
        }
    }

    /**
     * Changes the available options under the edit menu based on the input
     * DEditXMLWindow (which should be the currently selected DEditXMLWindow)
     * and the given selected nodes. If more than one node is selected, only
     * copy and cut can be chooseable.
     *
     * @param window
     *            the DEditXMLWindow to use to determine what's allowable --
     *            should be the currently selected DEditXMLWindow
     * @param selectedNodes
     *            the currently selected DEditNodes
     */
    public void changeEditOptions(DEditXMLWindow window,
            DEditNode[] selectedNodes) {
        long startTime = System.currentTimeMillis();
        undoAction.setEnabled(window != null && window.canUndo());
        if (selectedNodes == null || selectedNodes.length != 1) {
            // # of selected items != 1, most stuff is disabled
            copyAction.setEnabled(false);
            cutAction.setEnabled(false);
            pasteChildMenu.setEnabled(false);
            pasteChildPopupMenu.setEnabled(false);
            pasteBeforeAction.setEnabled(false);
            pasteAfterAction.setEnabled(false);
            editValAction.setEnabled(false);
            newTagBeforeMenu.setEnabled(false);
            newChildTagMenu.setEnabled(false);
            newTagAfterMenu.setEnabled(false);
            switchTagToMenu.setEnabled(false);
            newTagBeforePopupMenu.setEnabled(false);
            newChildTagPopupMenu.setEnabled(false);
            newTagAfterPopupMenu.setEnabled(false);
            switchTagToPopupMenu.setEnabled(false);
            // re-enable copy and cut for multiple selection
            if (selectedNodes != null && selectedNodes.length > 1) {
                copyAction.setEnabled(true);
                cutAction.setEnabled(true);
            }
        } else {
            // enable major menus
            pasteChildMenu.setEnabled(true);
            pasteChildPopupMenu.setEnabled(true);
            newTagBeforeMenu.setEnabled(true);
            newChildTagMenu.setEnabled(true);
            newTagAfterMenu.setEnabled(true);
            switchTagToMenu.setEnabled(true);
            newTagBeforePopupMenu.setEnabled(true);
            newChildTagPopupMenu.setEnabled(true);
            newTagAfterPopupMenu.setEnabled(true);
            switchTagToPopupMenu.setEnabled(true);
            // only one node is selected, the first and only node in the array
            DEditNode selected = selectedNodes[0];
            // copy should be enabled unless plugin says otherwise
            copyAction.setEnabled(true);
            if (plugin != null) {
                copyAction.setEnabled(plugin.isCopyable(selected));
            }
            // check current state (to compare to later state)
            boolean schemaReady = schema.isReady();
            boolean validBefore = (schemaReady && window.isValid());
            // determine if it is editable -- ask plugin after normal check
            if (schemaReady) {
                Node n = selected.getNode();
                boolean enabled = ((schema.isLeaf(n) && schema.getDataType(n) != null) || schema
                        .getAttributes(n) != null);
                if (plugin != null)
                    enabled = plugin.isEditable(selected, enabled);
                editValAction.setEnabled(enabled);
            } else {
                // schema's not ready, but we'll let them try and edit
                editValAction.setEnabled(true);
            }
            // can't cut root node
            if (selected.isRoot()) {
                cutAction.setEnabled(false);
            } else {
                // check if cutting is allowed (legal && plugin allows)
                DEditNode parent = (DEditNode) selected.getParent();
                Node next = selected.getNode().getNextSibling();
                selected.removeNode();
                boolean enable = !validBefore || window.isValid();
                if (plugin != null) {
                    enable = enable && plugin.isCuttable(selected);
                }
                cutAction.setEnabled(enable);
                if (next == null) {
                    parent.reinsertNodeAtEnd(selected);
                } else {
                    parent.reinsertNodeBefore(selected, next);
                }
            }
            // if nothing is copied then nothing is pasteable
            if (copied == null) {
                pasteChildPopupMenu.setEnabled(false);
                pasteChildMenu.setEnabled(false);
                pasteAfterAction.setEnabled(false);
                pasteBeforeAction.setEnabled(false);
            } else {
                // check paste before and after validity, leave paste child
                // for when its menu is expanded
                boolean enablePasteBefore = true;
                boolean enablePasteAfter = true;
                if (plugin != null) {
                    enablePasteBefore = plugin.pasteBeforeAllowed(selected);
                    enablePasteAfter = plugin.pasteAfterAllowed(selected);
                }
                if (enablePasteBefore) {
                    enablePasteBefore = !validBefore
                            || window.pasteBeforeCheck(selected, copied);
                }
                if (enablePasteAfter) {
                    enablePasteAfter = !validBefore
                            || window.pasteAfterCheck(selected, copied);
                }
                pasteBeforeAction.setEnabled(enablePasteBefore);
                pasteAfterAction.setEnabled(enablePasteAfter);
            }
            long totalTime = System.currentTimeMillis() - startTime;
            if (selected != null) {
                DEditDebug.println(this, 5, "total time = " + totalTime + ", "
                        + selected.getNode().getNodeName());
            }
        }
        // update selected window
        if (window != null)
            window.update(false);
    }

    /**
     * Resets the string on top of the JFrame main window.
     * @deprecated Use resetTitle() instead. XMLeditor info integrated into
     * title bar.standard for all qiComponents.
     */
    public void resetFrameString() {
        String frameString = null;
        String schemaFilename = schema.getFilename();
        if (schemaFilename != null) {
            frameString = pLoad.getPackageName() + agent.getDisplayNum() + " "
                    + getAgent().getPrepare().getName() + "  schema: "
                    + schemaFilename;
        } else {
            frameString = pLoad.getPackageName() + agent.getDisplayNum() + " "
                    + getAgent().getPrepare().getName() + "  schema Not Loaded";
        }
        setTitle(frameString);
    }

    /**
     * Checks validity of XML document given schema and informs user.
     */
    private void verify() {
        DEditXMLWindow window = getSelectedWindow();
        if (window == null) {
            JOptionPane.showMessageDialog(this, "Select a tree to verify.");
        } else if (!schema.isReady()) {
            JOptionPane.showMessageDialog(this, "Please load schema first.");
        } else {
            window.update(false);
            JOptionPane.showMessageDialog(this, schema.getVerifyString());
        }
    }

    /**
     * Gets the currently selected DEditXMLWindow.
     *
     * @return the currently selected DEditXMLWindow
     */
    public DEditXMLWindow getSelectedWindow() {
        for (int i = 0; i < windows.size(); i++) {
            DEditXMLWindow temp = (DEditXMLWindow) windows.get(i);
            if (temp.isSelected()) {
                return temp;
            }
        }
        return null;
    }

    /**
     * Called when the user wants to close the application. It checks to make
     * sure all open XML documents have been saved, cancelling at any point if
     * the user decides to do so.
     */
    public void close() {
        logger.info("test ----------1 ");
        while (windows.size() > 0) {
            DEditXMLWindow window = (DEditXMLWindow) windows.get(0);
            logger.info("test ----------2 ");
            boolean closed = window.close();
            logger.info("test ----------3 ");
            if (!closed) {
                return;
            }
        }
        logger.info("test ----------4 ");
        DEditDebug.println(this, 2, "#windows " + windows.size());
        dispose();
        logger.info("test ----------5 ");
        if (!isApplet) {
            logger.info("test ----------6 ");
            this.close();
            logger.info("test ----------7 ");
        }
    }

    private void setUpMenu(JMenu menu, int which) {
        long start = System.currentTimeMillis();
        DEditXMLWindow window = getSelectedWindow();
        undoAction.setEnabled(window != null && window.canUndo());
        if (window == null)
            return;

        DEditNode selected = window.getSelected();
        if (selected == null) {
            menu.setEnabled(false);
            return;
        }
        Action[] actions = null;

        if (which == EditMenuListener.INSERT_ABOVE_TYPE) {
            if (plugin == null || plugin.newTagBeforeAllowed(selected)) {
                actions = window.getNewTagBeforeActions(selected);
            }
        } else if (which == EditMenuListener.INSERT_CHILD_TYPE) {
            if (plugin == null || plugin.newChildTagAllowed(selected)) {
                actions = window.getNewChildTagActions(selected);
            }
        } else if (which == EditMenuListener.INSERT_BELOW_TYPE) {
            if (plugin == null || plugin.newTagAfterAllowed(selected)) {
                actions = window.getNewTagAfterActions(selected);
            }
        } else if (which == EditMenuListener.SWITCH_TYPE) {
            if (plugin == null || plugin.switchTagToAllowed(selected)) {
                actions = window.getSwitchTagToActions(selected);
            }
        } else {
            if (plugin == null || plugin.pasteChildAllowed(selected)) {
                actions = window.getPasteChildActions(selected, copied);
            }
        }
        if (actions == null || actions.length == 0) { // nothing available
            JMenuItem emptyItem = new JMenuItem("None available");
            emptyItem.setEnabled(false);
            menu.add(emptyItem);
            menu.setEnabled(true);
        } else {
            // create menu items from the actions...could we just
            // add actions to menu instead???
            JMenuItem[] subItems = new JMenuItem[actions.length];
            for (int j = 0; j < subItems.length; j++) {
                subItems[j] = new JMenuItem(actions[j]);
            }
            menu.setEnabled(true);
            fillMenuRecursively(menu, subItems, DEFAULT_CUTOFF);
        }
        long stop = System.currentTimeMillis();
        DEditDebug.println(this, 6, "set up menu " + which + ": "
                + (stop - start));
    }

    /**
     * Edits the currently selected node in the currently selected window.
     */
    private void editTag() {
        DEditXMLWindow window = getSelectedWindow();
        if (window != null)
            editTag(window);
    }

    /**
     * Edits information about a tag and its element.
     *
     * @param window
     *            the window to get the selected tag from and edit
     */
    private void editTag(DEditXMLWindow window) {
        /*
         * if it is a leaf, show change value and/or set attributes dialog. if
         * it is a branch, show add element dialog if they can add plus
         * attributes.
         */
        DEditNode toEdit = window.getSelected();
        if (toEdit == null) {
            DEditDebug.println(this, 0, "improper selection");
            return;
        }

        // give plugin a chance to intercept
        if (plugin != null) {
            boolean stop = plugin.edit(toEdit);
            if (stop) {
                DEditDebug.println(this, 3, "editing stopped by plugin.");
                return;
            }
        }

        // determine if it is a leaf
        boolean isLeaf = toEdit.isLeaf();
        Node node = toEdit.getNode();
        String oldVal = null;
        NodeList nlist = node.getChildNodes();
        Node child = null;
        for (int i = 0; i < nlist.getLength(); i++) {
            Node temp = nlist.item(i);
            if (temp.getNodeType() == Node.TEXT_NODE) {
                child = temp;
                break;
            }
        }
        // determine its data type
        DEditDataType dt = null;
        if (isLeaf) {
            if (child == null) {
                child = node.getOwnerDocument().createTextNode("");
                node.appendChild(child);
            }
            oldVal = child.getNodeValue();
            dt = schema.getDataType(node);
        }

        // get state information
        boolean validBefore = false;
        if (schema.isReady()) {
            validBefore = window.isValid();
        }
        // no value, no attributes -- no editing
        DEditAttributeInfo[] attInfo = schema.getAttributes(node);
        if (!isLeaf && attInfo == null) {
            DEditDebug.println(this, 3, "nothing to edit.");
            return;
        }

        DEditAttributeInfo[] backup = null; // holds old values should
        // a reset be necessary
        // fill up attInfo with current attribute information and values
        NamedNodeMap nnm = null;
        if (attInfo != null) {
            nnm = node.getAttributes();
            if (nnm == null) {
                DEditDebug.println(this, 3, "NOT an Element!!");
                return;
            }
            if (nnm.getLength() > 0) {
                for (int i = 0; i < attInfo.length; i++) {
                    Attr att = (Attr) nnm.getNamedItem(attInfo[i].name);
                    if (att != null) {
                        attInfo[i].exists = true;
                        attInfo[i].value = att.getValue();
                    }
                }
            }
            backup = new DEditAttributeInfo[attInfo.length];
            for (int i = 0; i < backup.length; i++) {
                backup[i] = attInfo[i].copy();
            }
        }
        // idetermine which dialog to show and show it
        String newVal = null;
        String titleString = "Edit " + toEdit.getDisplayName();
        if (isLeaf && oldVal != null && dt != null) {
            if (attInfo != null) {
                newVal = dialog.showAttValDialog(this, titleString, dt, oldVal,
                        attInfo);
            } else {
                newVal = dialog.showValueDialog(this, titleString, dt, oldVal);
            }
            if (newVal == null)
                return;

        } else if (attInfo != null) {
            attInfo = dialog.showAttributeDialog(this, titleString, attInfo);
        } else { // not a leaf, no attributes -- no editing
            return;
        }
        // update attributes if not canceled
        if (attInfo != null) {
            for (int i = 0; i < attInfo.length; i++) {
                if (attInfo[i].exists) {
                    Attr att = window.createAttribute(attInfo[i].name);
                    att.setValue(attInfo[i].value);
                    nnm.setNamedItem(att);
                } else {
                    if (nnm.getNamedItem(attInfo[i].name) != null) {
                        nnm.removeNamedItem(attInfo[i].name);
                    }
                }
            }
        }
        // set node value if applicable
        if (isLeaf && child != null) {
            child.setNodeValue(newVal);
        }
        // check to make sure xml is still valid. If not, move back to
        // old values and inform user
        if (validBefore && !window.isValid()) {
            if (isLeaf) {
                child.setNodeValue(oldVal);
            }
            if (attInfo != null) {
                attInfo = backup;
                for (int i = 0; i < attInfo.length; i++) {
                    if (attInfo[i].exists) {
                        Attr att = window.createAttribute(attInfo[i].name);
                        DEditDebug.println(this, 0, attInfo[i].value);
                        att.setValue(attInfo[i].value);
                        nnm.setNamedItem(att);
                    } else {
                        if (nnm.getNamedItem(attInfo[i].name) != null) {
                            nnm.removeNamedItem(attInfo[i].name);
                        }
                    }
                }
            }
            JOptionPane.showMessageDialog(XmlEditorGUI.this, "Can't set tag: "
                    + schema.getVerifyString());
        }
        window.update(true);
        window.nodeChanged(toEdit);
    }

    /**
     * Copies the currently selected subtree.
     */
    private void copySubTree(DEditXMLWindow window) {
        DEditNode[] toCopy = window.getMultipleSelected();
        if (toCopy == null)
            return;
        copied = new DEditNode[toCopy.length];
        for (int i = 0; i < copied.length; i++) {
            copied[i] = toCopy[i].copy();
        }
        changeEditOptions(window, toCopy);
    }

    /**
     * Cuts the currently selected subtree.
     */
    private void cutSubTree(DEditXMLWindow window) {
        DEditNode[] toCopy = window.cutSubTree(window.getMultipleSelected());
        if (toCopy == null)
            return;
        copied = new DEditNode[toCopy.length];
        for (int i = 0; i < copied.length; i++) {
            copied[i] = toCopy[i].copy();
        }
        changeEditOptions(window);
    }

    /**
     * Pastes the currently copied subtree as the sibling before the currently
     * selected one (vertically above).
     */
    private void pasteSibTreeBefore(DEditXMLWindow window) {
        window.pasteSibTreeBefore(copied);
        changeEditOptions(window);
    }

    /**
     * Calls undo on the last action performed in DEditXMLWindow window.
     *
     * @param window
     *            the window to call undo on
     */
    private void undo(DEditXMLWindow window) {
        window.undo();
        undoAction.setEnabled(window.canUndo());
    }

    /**
     * Pastes the currently copied subtree as the sibling after the currently
     * selected one (vertically below).
     */
    private void pasteSibTreeAfter(DEditXMLWindow window) {
        window.pasteSibTreeAfter(copied);
        changeEditOptions(window);
    }

    /**
     * Convenient helper which constructs a menu with items such that when there
     * are cutoff items in a menu, it adds a menu to that menu and starts
     * putting more items there until cutoff is reached, and then puts another
     * menu in that menu, etc.
     *
     * @param menu
     *            the starting, main JMenu to fill
     * @param subItems
     *            the JMenuItem's to add to menu
     * @param cutoff
     *            maximum number of items in a menu - 1 (for the link to the
     *            next menu)
     */
    private static void fillMenuRecursively(JMenu menu, JMenuItem[] subItems,
            int cutoff) {
        int n = 0;
        while (n < subItems.length) {
            for (int j = 0; j < cutoff; j++) {
                if (n + j >= subItems.length) {
                    return;
                }
                menu.add(subItems[n + j]);
            }
            n += cutoff;
            JMenu temp = new JMenu("More..."); // change? configureable???
            menu.add(temp);
            menu = temp;
        }
    }

    class EditMenuListener implements MenuListener {
        private static final int INSERT_ABOVE_TYPE = 0;

        private static final int INSERT_CHILD_TYPE = 1;

        private static final int INSERT_BELOW_TYPE = 2;

        private static final int SWITCH_TYPE = 3;

        private static final int PASTE_CHILD_TYPE = 4;

        private int type; // which type of items to fill menu with

        private JMenu menu; // the menu being filled

        private EditMenuListener(JMenu menu, int type) {
            this.type = type;
            this.menu = menu;
        }

        public void menuCanceled(MenuEvent e) {
        }

        public void menuDeselected(MenuEvent e) {
        }

        public void menuSelected(MenuEvent e) {
            if (menu.getItemCount() == 0)
                setUpMenu(menu, type);
        }
    }

    /**
     * Class which implements action to provide uniform behavior for standard
     * edit menu and popup edit menu.
     */
    class EditAction extends AbstractAction {
        static final long serialVersionUID = 1l;

        private static final int EDIT_TAG_TYPE = 3;

        private static final int COPY_TYPE = 4;

        private static final int CUT_TYPE = 5;

        private static final int PASTE_BEFORE_TYPE = 6;

        private static final int PASTE_AFTER_TYPE = 7;

        private static final int UNDO_TYPE = 8;

        private int type;

        private EditAction(String name, int thetype) {
            super(name);
            type = thetype;
        }

        public void actionPerformed(ActionEvent ae) {
            DEditXMLWindow window = getSelectedWindow();
            if (window == null) {
                JOptionPane.showMessageDialog(XmlEditorGUI.this,
                        "Select a window first.");
                return;
            }

            switch (type) {
            case EDIT_TAG_TYPE:
                editTag(window);
                break;
            case COPY_TYPE:
                copySubTree(window);
                break;
            case CUT_TYPE:
                cutSubTree(window);
                break;
            case PASTE_BEFORE_TYPE:
                pasteSibTreeBefore(window);
                break;
            case PASTE_AFTER_TYPE:
                pasteSibTreeAfter(window);
                break;
            case UNDO_TYPE:
                undo(window);
                break;
            }
        }
    }

    /**
     * Class used by DEditor to implement the display of the popup menu.
     */
    class PopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }

    public XmlEditorPlugin getAgent() {
        return agent;
    }

    public void updateGUI() {
        //resetFrameString();
        resetTitle(project);
        newItem.setEnabled(allowNewFile);
        importMenuItem.setEnabled(allowOpenFile);
        loadSchemaItem.setEnabled(allowLoadSchema);
        newXMLButton.setEnabled(allowNewFile);
        openXMLButton.setEnabled(allowOpenFile);
    }

    public void setInternalFrame(JPanel demo) {
        pManager = new JInternalFrame("Plugin Manager", true, true, true, true);
        pManager.getContentPane().add(demo);
        pManager.setLocation(100, 10);
        jdp.add(pManager);
        pManager.pack();
        pManager.show();
    }

    public void setInternalFrame(JTabbedPane demo) {
        pManager = new JInternalFrame("Package Manager", true, true, true, true);
        pManager.getContentPane().add(demo);
        pManager.setLocation(50, 0);
        jdp.add(pManager);
        pManager.pack();
        pManager.show();
    }

    public void removePluginManager() {
        if (pManager != null)
            pManager.setVisible(false);
        jdp.remove(pManager);
    }

    /**
     * Returns the resource bundle associated with this demo. Used to get
     * accessable and internationalized strings.
     */
    public ResourceBundle getResourceBundle() {
        if (bundle == null) {
            bundle = ResourceBundle.getBundle(XmlEditorConstants.RESOURCES);
        }
        return bundle;
    }

    /**
     * Creates an icon from an image contained in the "images" directory.
     */
    public ImageIcon createImageIcon(String filename, String description) {
        String path = "/res/" + filename;
        return new ImageIcon(getClass().getResource(path));
    }

    /**
     * Save the state information for GUI.
     *
     * @return xml string.
     */
    public String genState() {
        StringBuffer content = new StringBuffer();
        content.append("<" + this.getClass().getName() + " ");
        content.append("height=\"" + this.getHeight() + "\" ");
        content.append("width=\"" + this.getWidth() + "\" ");
        content.append("x=\"" + this.getX() + "\" ");
        content.append("y=\"" + this.getY() + "\" ");
        String temp = agent.getPrepare().getName();
        content.append("distribution=\"" + temp + "\" ");
        temp = pLoad.getPackageName();
        content.append("package=\"" + temp + "\" ");
        // content.append("openxml=\"" + openedXml + "\" ");
        content.append("/> ");
        for(int i = 0; windows != null && i < windows.size(); i++){
        	String title = windows.get(i).getTitle();
        	Point p = windows.get(i).getLocation();
        	Dimension d = windows.get(i).getDimension();
        	boolean freeform = windows.get(i).isFreeForm();
        	String xml1 = null;
        	if(freeform)
        		xml1 = "<window title=\"" + title + "\" x=\"" + p.x + "\" y=\"" + p.y + "\" width=\"" + d.width + "\" height=\"" + d.height + "\" freeform=\"" + "yes" + "\">";
        	else
        		xml1 = "<window title=\"" + title + "\" x=\"" + p.x + "\" y=\"" + p.y + "\" width=\"" + d.width + "\" height=\"" + d.height  + "\">";
        	String xml = windows.get(i).toXMLString2();
        	xml = xml.substring(xml.indexOf("?>")+2);
        	content.append(xml1);
        	content.append(xml);
        	String xml2 = "</window>";
        	content.append(xml2);
        }
        return content.toString();
    }

    
    /**
     * Reset the GUI's title
     *
     * @param name Name of the associated project.
     */
    public void resetTitle(String projName) {
        String editorInfo = null;
        String schemaFilename = schema.getFilename();
        if (schemaFilename != null) {
            editorInfo = pLoad.getPackageName() + agent.getDisplayNum() + " "
                    + getAgent().getPrepare().getName() + "  schema: "
                    + schemaFilename;
        } else {
            editorInfo = pLoad.getPackageName() + agent.getDisplayNum() + " "
                    + getAgent().getPrepare().getName() + "  schema: Not Loaded";
        }

        String compName = "";
        IComponentDescriptor agentDesc = agent.getComponentDescriptor();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0)
            compName = pdn.trim();
        else
            compName = screenName;

        //get the type of qiComponent
        int idx = screenName.indexOf("#");
        if (idx != -1) screenName = screenName.substring(0, idx);
        this.setTitle(screenName+": " + compName + "  " + editorInfo + "  Project: " + projName);
    }
}
