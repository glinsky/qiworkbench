/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor;

/**
 * Constants used within XmlEditor
 */
public final class XmlEditorConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private XmlEditorConstants() {}

    public static final String NOT_SPECIFIED = "Not Specified";
    public static final String RESOURCES = "res.xmlEditor";
    public static final String D_SCRIPT   = "xsd/script.xsd";
    public static final String D_DELIVERY = "xsd/delivery.xsd";
    public static final String SCHEMAS[]  = {"", "xsd/script.xsd",       "xsd/delivery.xsd",
    	                                         "deliveryMassager.xsd", "waveletExtraction.xsd"};
 
    public static final String ADD_PACKS     = "newPacks";
    public static final String CONFIG_NAME   = "editConfig";

    /** plugin GUI's display name */
    public static final String SPEC_XMLEDITOR_GUI_NAME = "XmlEditor GUI";
    
    public static final int PREF_WIDTH  = 750;
    public static final int PREF_HEIGHT = 650;

    // debug print: 0=none, 1=moderate, 2=verbose
    public static final int DEBUG_PRINT = 2;

    // integer status
    public static final int ERROR_STATUS = 1;
    public static final int OK_STATUS = 0;
    public static final int JOB_RUNNING_STATUS = 1;
    public static final int JOB_ENDED_STATUS = 2;
    public static final int JOB_UNKNOWN_STATUS = 3;
    public static final int JOB_UNSUBMITTED_STATUS = 0;
    
    //Toolbar Image Files
    public static final String TB_NEW    = "icons/new_xml.jpg";
    public static final String TB_OPEN   = "icons/open_xml.jpg";
    public static final String TB_SAVE   = "icons/save_xml.jpg";
    public static final String TB_EDIT   = "icons/edit.jpg";
    public static final String TB_COPY   = "icons/copy.jpg";
    public static final String TB_CUT    = "icons/cut.jpg";
    public static final String TB_HELP   = "icons/help.jpg";
    public static final String TB_SWITCH = "icons/switch.jpg";
    public static final String TB_PASTE_BEFORE  = "icons/paste_before.jpg";
    public static final String TB_PASTE_AFTER   = "icons/paste_after.jpg";
    public static final String TB_PASTE_CHILD   = "icons/paste_child.jpg";
    public static final String TB_INSERT_BEFORE = "icons/insert_before.jpg";
    public static final String TB_INSERT_AFTER  = "icons/insert_after.jpg";
    public static final String TB_INSERT_CHILD  = "icons/insert_child.jpg";
    
    //Toolbar ID
    public static final String NEW_XML_ID      = "new_xml";
    public static final String OPEN_XML_ID     = "open_xml";
    public static final String SAVE_XML_ID     = "save_xml";
    public static final String INSERT_ABOVE_ID = "insert_above";
    public static final String INSERT_CHILD_ID = "insert_child";
    public static final String INSERT_BELOW_ID = "insert_below";
    public static final String SWITCH_ID       = "switch";
    public static final String EDIT_VALUE_ID   = "edit_value";
    public static final String COPY_ID         = "copy";
    public static final String CUT_ID          = "cut";
    public static final String PASTE_ABOVE_ID  = "paste_above";
    public static final String PASTE_CHILD_ID  = "paste_child";
    public static final String PASTE_BELOW_ID  = "paste_below";	
    
    public static final String SCRIPT_NAME   = "scriptPlugin";
    public static final String DELIV_NAME    = "deliveryPlugin"; 
    public static final String SCRIPT_CLASS  = "com.bhpb.xmleditor.plugins.script.ScriptPlugin";
    public static final String DELIV_CLASS   = "com.bhpb.xmleditor.plugins.delivery.DeliveryPlugin";
    
    public static final String DELIV_CONFIG   = "config/deliveryConfig.xml";
    public static final String SCRIPT_CONFIG  = "config/scriptConfig.xml";	
    
    public static final String DELIV_SCHEMA       = "xsd/delivery.xsd";
    public static final String SCRIPT_SCHEMA      = "xsd/script.xsd";	
    public static final String EXTRA_ATTRI_SCHEMA = "xsd/extraAttributes.xsd";
    public static final String WAV_EXT_SCHEMA     = "xsd/waveletExtraction.xsd";	
    
    	
    
}
