/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.model;

import java.awt.Component;
import javax.swing.*;

/**
  * Used in an attempt to keep swing from bolding elements of a table that 
  * are being edited.  Still used, but does not accomplish its goal.  The 
  * reason for wanting no bolding is that it affected where the cursor
  * appears in editing after clicking a box to edit it.
  */
public class NoBoldTableCellEditor extends DefaultCellEditor {
    public NoBoldTableCellEditor(JComboBox jcb) {
	super(jcb);
    }
    public NoBoldTableCellEditor(JTextField jtf) {
	super(jtf);
    }
    public NoBoldTableCellEditor(JCheckBox jcb) {
	super(jcb);
    }
    public Component getTableCellEditorComponent(JTable table, Object value,
						 boolean isSelected,
						 int row, int column) {
	return super.getTableCellEditorComponent(table,value,false,
						 row,column);
    }
}
