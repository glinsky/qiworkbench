/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.model;

import com.bhpb.model.ui.NameValue;
import com.bhpb.model.ui.NameValues;
import com.bhpb.xmleditor.XmlEditorGUI;
import com.bhpb.xmleditor.swing.ParameterDialog;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;

import java.awt.event.*;
import java.awt.Dimension;
import org.w3c.dom.*;

import javax.help.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
/**
  * Class which represents a window for editing the XML tree but also contains 
  * it's text frame and a number of other features.  It is essentially the 
  * graphical and logical components of an opened XML file from the
  * perspective of the editor.
  */
public final class DEditXMLWindow {
    
    final private JTree tree; // the tree displaying the XML
    final private JTextField docStatus;  // status bar at the bottom of 
    // XML window
    final private JTextArea text; // text of XML to be displayed when asked
    final private JInternalFrame treeFrame; // the window in which the xml 
    // tree is displayed
    final private JInternalFrame textFrame; // the window in which the xml 
    // text is displayed
    private Document document = null;  // the DOM document for the XML
    private String openFilename = null; // name of the file that is open
    private boolean unsaved = false; // true if the file has ever been saved
    private boolean modifiedSinceSaved = false; // true if the file has been 
    // modified since it was last saved
    private XmlEditorGUI owner; // the DEditor that this DEditXMLWindow is inside of
    private DefaultTreeModel model; //TreeModel for TreeNode manipulation
    final private DEditSchema schema; // the Schema being used
    private static int unsavedIndex = 1; // used to give names to unsaved files
    private static final int UNDO_BUFFER_SIZE = 4; // number of trees in the 
    // undo buffer == max number of undo's that can be performed at any given 
    // time
    private Vector undoBuffer = new Vector(); // the buffer of root DEditNodes
    // Map IDs for JavaHelp
    final private static String TREE_ID = "tree"; 
    final private static String STATUS_ID = "status";
    final private static String TEXT_ID = "text";
    private List nameList;
    private Toolkit toolkit = Toolkit.getDefaultToolkit();
    private int newNodeSuffix = 1;
    private DEditNode rootNode;
    private Node oldEditNode;
    private DefaultTableModel parameterTableModel;
    private JTable paramTable = new JTable();
    private JPopupMenu popupMenu;
    private boolean freeform = false;
    /**
      * Creates a DEditXMLWindow for an existing file by opening it as for 
      * a new file then changing the relevant information.
      * @param owner the DEditor this DEditXMLWindow belongs to
      * @param jdp the JDesktopPane that this DEditXMLWindow's JInternalFrames
      * will appear in
      * @param schema the DEditSchema used for this DEditXMLWindow (and all 
      * others as well)
      * @param filename the name of the file that produced document
      * @param renderer the DEditTreeCellRenderer that will be used to 
      * render the JTree in this DEditXMLWindow
      * @param document the DOM Document currently open
      * @param mls MouseListeners to install into treeFrame
      */
    public DEditXMLWindow(final XmlEditorGUI owner, JDesktopPane jdp,
			  DEditSchema schema, String filename,
			  DEditTreeCellRenderer renderer, Document document,
			  MouseListener[] mls, DEditPlugin plugin) {
	   this(owner,jdp,schema,renderer,document,mls,plugin,false);	
	   treeFrame.setTitle(filename);
	   openFilename = filename; // reset from generic to actual file name
	   unsavedIndex--; // don't waste a generic
	   unsaved = false; // it has been saved
	   update(false); // update, but nothing's changed
    }

    /**
      * Creates a DEditXMLWindow by setting up it's appropriate JInternalFrames
      * and installing MouseListeners and a (DEdit)TreeCellRenderer.
      * @param owner the DEditor this DEditXMLWindow belongs to
      * @param jdp the JDesktopPane that this DEditXMLWindow's JInternalFrames
      * will appear in
      * @param schema the DEditSchema used for this DEditXMLWindow (and all 
      * others as well)
      * @param renderer the DEditTreeCellRenderer that will be used to 
      * render the JTree in this DEditXMLWindow
      * @param document the DOM Document currently open
      * @param mls MouseListeners to install into treeFrame
      * @param generate true if the schema should generate all required 
      * children for the root of the tree
      */
    public DEditXMLWindow(final XmlEditorGUI owner, JDesktopPane jdp,
			  DEditSchema schema, DEditTreeCellRenderer renderer,
			  Document document, MouseListener[] mls,
			  DEditPlugin plugin, boolean generate) {
	    // set fields
	    this.owner = owner;
	    this.schema = schema;
	    this.document = document;
	    this.unsaved = true;
	    this.tree = new JTree();
	    String treeID = null;
	    if (plugin != null) {
	       treeID = plugin.getTreeHelpID();
	    }
	    if (treeID == null) {
	       treeID = TREE_ID;
	    }
	    CSH.setHelpIDString(tree,treeID);
	    TreeSelectionModel tsm = tree.getSelectionModel();
	    // allow contiguous selection for copy and cut
	    tsm.setSelectionMode(TreeSelectionModel.CONTIGUOUS_TREE_SELECTION);
	    model = new DefaultTreeModel(DEditNode.EMPTY_NODE);
	    if(schema.getFilename().equals("xsd/freeform.xsd"))
	    	tree.setEditable(true);
	    else
	    	tree.setEditable(false);
	    
	    JScrollPane treeView = new JScrollPane(); // holds the JTree
	    text = new JTextArea(33,40);
	    CSH.setHelpIDString(text,TEXT_ID);
	    text.setEditable(false);
	    docStatus = new JTextField();
	    CSH.setHelpIDString(docStatus,STATUS_ID);
	    docStatus.setEditable(false);
	    docStatus.addMouseListener(new MouseAdapter() {
		public void mouseClicked(MouseEvent me) {
		    if (me.getClickCount() > 1) {
			resetDocStatus();
			JOptionPane.showMessageDialog(owner,
						      docStatus.getText());
			
		    }
		}
	    });
	//openFilename = "Untitled<" + unsavedIndex + ">";
	openFilename = "Untitled #" + unsavedIndex;
	unsavedIndex++; // increment anonymous file index
	JPanel leftPanel = new JPanel(new BorderLayout());
	leftPanel.add(treeView,BorderLayout.CENTER);
	leftPanel.add(docStatus,BorderLayout.SOUTH);
	JScrollPane textView = new JScrollPane(text);
	treeView.setPreferredSize(new Dimension(440,511));
	treeFrame = new JInternalFrame(openFilename,true,true,true,true);
	treeFrame.setDefaultCloseOperation(JInternalFrame.DO_NOTHING_ON_CLOSE);
	treeFrame.addInternalFrameListener(new InternalFrameAdapter() {
		public void internalFrameActivated(InternalFrameEvent e) {
		    owner.changeEditOptions(DEditXMLWindow.this);
		    update(false);
		}
		public void internalFrameClosing(InternalFrameEvent e) {
		    close();
		}

		public void internalFrameClosed(InternalFrameEvent e) {
		    //owner.removeXMLWindow(DEditXMLWindow.this);
		    textFrame.dispose();
		    //treeFrame.dispose();
		}
	    });
	treeFrame.getContentPane().add(leftPanel);
	textFrame = new JInternalFrame("XML Text",true,true,true,true);
	textFrame.getContentPane().add(textView);
	textFrame.setLocation(450,0);
	textFrame.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
	tree.addTreeSelectionListener(new TreeSelectionListener() {
		public void valueChanged(TreeSelectionEvent e) {
		    owner.changeEditOptions(DEditXMLWindow.this);
		}
	    });
	treeView.setViewportView(tree);
	jdp.add(textFrame);
	jdp.add(treeFrame);
	textFrame.pack();
	//textFrame.show();
	treeFrame.pack();
	treeFrame.show();
	for (int i = 0; i < mls.length; i++) {
	    tree.addMouseListener(mls[i]);
	}
	DEditNode rootNode = new DEditNode(document.getDocumentElement(),
					   model);
	model.setRoot(rootNode);
	tree.setModel(model);
	if (generate) {
	    schema.generateRequiredChildren(rootNode,model);
	}
	model.reload();
	tree.setCellRenderer(renderer);	
	update(false);
    }

    public DEditXMLWindow(final XmlEditorGUI owner, JDesktopPane jdp,
			  DEditSchema schema, DEditTreeCellRenderer renderer,
			  Document document, MouseListener[] mls,
			  DEditPlugin plugin, boolean generate, boolean freeform) {
	    // set fields
	    this.owner = owner;
	    this.schema = schema;
	    this.document = document;
	    this.unsaved = true;
	    this.freeform = freeform;
	    //model = new DefaultTreeModel(rootNode);
	    model = new DefaultTreeModel(DEditNode.EMPTY_NODE);
	    model.addTreeModelListener(new MyTreeModelListener());
	    MouseListener treeListener = new MouseHandler();
	    this.tree = new JTree(model);
	    String treeID = null;
	    if (plugin != null) {
	       treeID = plugin.getTreeHelpID();
	    }
	    if (treeID == null) {
	       treeID = TREE_ID;
	    }
	    tree.addMouseListener(treeListener);
        tree.addTreeSelectionListener(new TreeSelectionListener(){
        	
        	public void valueChanged(TreeSelectionEvent e) {
        		DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                tree.getLastSelectedPathComponent();
        		if (node == null) return;
        		Object nodeInfo = node.getUserObject();
        		
        	}
        });
        //tree.setEditable(true);
	    CSH.setHelpIDString(tree,treeID);
	    TreeSelectionModel tsm = tree.getSelectionModel();
	    // allow contiguous selection for copy and cut
	   // tsm.setSelectionMode(TreeSelectionModel.CONTIGUOUS_TREE_SELECTION);
	    tsm.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	    //model = new DefaultTreeModel(DEditNode.EMPTY_NODE);
	    if(freeform)
	    	tree.setEditable(true);
	    else
	    	tree.setEditable(false);
	    tree.setShowsRootHandles(true);
	    JScrollPane treeView = new JScrollPane(); // holds the JTree
	    text = new JTextArea(33,40);
	    CSH.setHelpIDString(text,TEXT_ID);
	    text.setEditable(false);
	    docStatus = new JTextField();
	    CSH.setHelpIDString(docStatus,STATUS_ID);
	    docStatus.setEditable(false);
	    docStatus.addMouseListener(new MouseAdapter() {
		public void mouseClicked(MouseEvent me) {
		    if (me.getClickCount() > 1) {
			resetDocStatus();
			JOptionPane.showMessageDialog(owner,
						      docStatus.getText());
			
		    }
		}
	    });
	//openFilename = "Untitled<" + unsavedIndex + ">";
	openFilename = "Untitled #" + unsavedIndex;
	unsavedIndex++; // increment anonymous file index
	
	JPanel leftPanel = new JPanel(new BorderLayout());
	leftPanel.add(treeView,BorderLayout.CENTER);
	leftPanel.add(docStatus,BorderLayout.SOUTH);
	JScrollPane textView = new JScrollPane(text);
	treeView.setPreferredSize(new Dimension(440,511));
	treeFrame = new JInternalFrame(openFilename,true,true,true,true);
	treeFrame.setDefaultCloseOperation(JInternalFrame.DO_NOTHING_ON_CLOSE);
	treeFrame.addInternalFrameListener(new InternalFrameAdapter() {
		public void internalFrameActivated(InternalFrameEvent e) {
		    owner.changeEditOptions(DEditXMLWindow.this);
		    update(false);
		}
		public void internalFrameClosing(InternalFrameEvent e) {
		    close();
		}

		public void internalFrameClosed(InternalFrameEvent e) {
		    //owner.removeXMLWindow(DEditXMLWindow.this);
		    textFrame.dispose();
		    //treeFrame.dispose();
		}
	    });
	treeFrame.getContentPane().add(leftPanel);
	textFrame = new JInternalFrame("XML Text",true,true,true,true);
	textFrame.getContentPane().add(textView);
	textFrame.setLocation(450,0);
	textFrame.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
	//tree.addTreeSelectionListener(new TreeSelectionListener() {
	//	public void valueChanged(TreeSelectionEvent e) {
	//	    owner.changeEditOptions(DEditXMLWindow.this);
	//	}
	//    });
	treeView.setViewportView(tree);
	jdp.add(textFrame);
	jdp.add(treeFrame);
	textFrame.pack();
	//textFrame.show();
	treeFrame.pack();
	treeFrame.show();
	for (int i = 0; mls != null && i < mls.length; i++) {
	    tree.addMouseListener(mls[i]);
	}
	
	if(!document.hasChildNodes()){
		Node n = document.createElement("Root");
		document.appendChild(n);
	}
	rootNode = new DEditNode(document.getDocumentElement(),
					   model);
	
	model.setRoot(rootNode);
	tree.setModel(model);
	//if (generate) {
	//    schema.generateRequiredChildren(rootNode,model);
	//}
	model.reload();
	tree.setCellRenderer(renderer);	
	update(false);
  }
    
    
    public boolean isFreeForm(){
    	return freeform;
    }
    
    public DefaultTreeModel getTreeModel(){
    	return model;
    }
    
    public void setTreeModel(DefaultTreeModel model){
    	this.model = model;
    	tree.setModel(model);
    	model.reload();
    	update(false);
    }
    
    public void setDocument(Document doc){
    	document = doc;
    }
    public JInternalFrame getTreeFrame(){
    	return treeFrame;
    }
    /**
      * Resets the text in the document status text field.
      */
    private void resetDocStatus() {
	if (!schema.isReady()) {
	    docStatus.setText("No schema loaded.");
	} else if (openFilename == null) { // this shouldn't ever happen
	    docStatus.setText("No XML document opened.");
	} else {
	    isValid();
	    docStatus.setText(schema.getVerifyString());
	    docStatus.setCaretPosition(0);
	}
    }

    
    public Document getDocument(){
    	return document;
    }
    /**
      * Cuts the passed in subtrees and returns a copy of the cut trees.
      * @param toCut the subtrees to cut from the tree
      * @return the passed in array of subtrees which have now been removed
      * from the JTree
      */
    public DEditNode[] cutSubTree(DEditNode[] toCut) {
	addToUndoBuffer();
	for (int i = 0; i < toCut.length; i++) {
	    toCut[i].removeSelf();
	}
	update(true);
	return toCut;
    }

    /**
      * Undo back to the most recent tree stored in the undo buffer.
      */
    public void undo() {
	if (undoBuffer.size() == 0) {
	    return;
	}
	DEditNode oldRoot = (DEditNode)undoBuffer.remove(undoBuffer.size()-1);
	Node domRoot = document.getDocumentElement();
	document.replaceChild(oldRoot.getNode(),domRoot);
	model.setRoot(oldRoot);
	update(true);
    }

    /**
      * Add the current tree to the undo buffer (do this right before
      * cutting, pasting, inserting, etc.).
      */
    private void addToUndoBuffer() {
	if (undoBuffer.size() == UNDO_BUFFER_SIZE) {
	    undoBuffer.remove(0);
	}
	undoBuffer.add(((DEditNode)model.getRoot()).copy());
    }

    /**
      * Used to determine if undo button or menu item should be enabled.
      * @return true if this DEditXMLWindow can "undo."
      */
    public boolean canUndo() {
	return (undoBuffer.size() != 0);
    }

    /**
      * Pastes a replicated version of the copied DEditNodes into the tree 
      * before the currently selected node.
      * @param copied the copied DEditNodes to create DEditNodes suitable 
      * for this JTree from and then paste in
      */
    public void pasteSibTreeBefore(DEditNode[] copied) {
	    DEditNode denode = getSelected();
	    if (denode == null) {
	       return;
	    }
	    addToUndoBuffer();
	    pasteSibTreeBefore(denode,copied);
    }

    /**
      * Pastes a replicated version of the copied DEditNodes into the tree 
      * before the input DEditNode denode.
      * @param denode the DEditNode to paste before
      * @param copied the copied DEditNodes to create DEditNodes suitable for 
      * this JTree from and then paste in
      */
    private DEditNode[] pasteSibTreeBefore(DEditNode denode,
					   DEditNode[] copied) {
	DEditNode parent = (DEditNode)(denode.getParent());
	if (parent == null) {
	    return null; // shouldn't happen
	}
	DEditNode[] inserted = new DEditNode[copied.length];
	for (int i = 0; i < copied.length; i++) {
	    Node imported = document.importNode(copied[i].getNode(),true);
	    DEditNode toInsert = new DEditNode(imported,model);	
	    int index = model.getIndexOfChild(parent,denode);
	    parent.insertAtIndex(toInsert,index,denode.getNode());
	    model.nodeChanged(toInsert);
	    inserted[i] = toInsert;
	}
	update(true);
	return inserted;
    }

    /**
      * Pastes a replicated version of the copied DEditNode into the tree after
      * the currently selected node.
      * @param copied the copied DEditNode to create a DEditNode suitable for 
      * this JTree from and then paste in
      */
    public void pasteSibTreeAfter(DEditNode[] copied) {
	DEditNode denode = getSelected();
	if (denode == null) {
	    return;
	}
	addToUndoBuffer();
	pasteSibTreeAfter(denode,copied);
    }
    
    /**
      * Pastes an altered version of the copied DEditNode into the tree after 
      * the input DEditNode denode.
      * @param denode the DEditNode to paste after
      * @param copied the copied DEditNode to create a DEditNode suitable for 
      * this JTree from and then paste in
      */
    private DEditNode[] pasteSibTreeAfter(DEditNode denode,
					 DEditNode[] copied) {
	DEditNode parent = (DEditNode)(denode.getParent());
	if (parent == null) {
	    return null; // shouldn't happen
	}
	DEditNode[] inserted = new DEditNode[copied.length];
	int index = model.getIndexOfChild(parent,denode);
	Node refChild = denode.getNode().getNextSibling();
	for (int i = 0; i < copied.length; i++) {
	    Node imported = document.importNode(copied[i].getNode(),true);
	    DEditNode toInsert = new DEditNode(imported,model);	
	    parent.insertAtIndex(toInsert,index+1+i,
				 refChild);
	    model.nodeChanged(toInsert);
	    inserted[i] = toInsert;
	}
	update(true);
	return inserted;
    }

    /**
      * Gets Action's which represent creating and inserting new sub trees 
      * before / above the currently selected subtree.
      * @return an array of Action's which insert DEditNodes into the JTree
      */
    public Action[] getNewTagBeforeActions() {
	    DEditNode selected = getSelected();
	    if (selected == null) {
	       return null;
	    }
	    return getNewTagBeforeActions(selected);
    }
    
    /**
      * Gets Action's which represent creating and inserting new sub trees 
      * before / above the input subtree.
      * @param selected the DEditNode to insert before / above
      * @return an array of Action's which insert DEditNodes into the JTree
      */
    public Action[] getNewTagBeforeActions(DEditNode selected) {
	    String[] names = schema.getNewTagBeforeNames(selected);
        nameList = new ArrayList();
	    if (names != null) {
            for(int i=0; i<names.length; i++) nameList.add(names[i]);
            Collections.sort(nameList);
            for(int i=0; i<names.length; i++)
               names[i] = nameList.get(i).toString();
	           DEditNode parent = (DEditNode)selected.getParent();
	           int index = parent.getIndex(selected);
	           return createInsertActions(names,parent,index);
	        }
	        return null;
    }

    /**
      * Gets Action's which represent creating and inserting new sub trees 
      * after / below the currently selected subtree.
      * @return an array of Action's which insert DEditNodes into the JTree
      */
    public Action[] getNewTagAfterActions() {
	   DEditNode selected = getSelected();
	   if (selected == null) {
	      return null;
	   }
	   return getNewTagAfterActions(selected);
    }
    
    /**
      * Gets Action's which represent creating and inserting new sub trees 
      * after / below the input subtree.
      * @param selected the DEditNode to insert after / below
      * @return an array of Action's which insert DEditNodes into the JTree
      */
    public Action[] getNewTagAfterActions(DEditNode selected) {
        nameList = new ArrayList();
	    String[] names = schema.getNewTagAfterNames(selected);
	    if (names != null) {
            for(int i=0; i<names.length; i++) nameList.add(names[i]);
            Collections.sort(nameList);
            for(int i=0; i<names.length; i++) names[i] = nameList.get(i).toString();
	        DEditNode parent = (DEditNode)selected.getParent();
	        int index = parent.getIndex(selected);
	        return createInsertActions(names,parent,index+1);
	    }
	    return null;
    }

    /**
      * Gets Action's which will create and insert a chosen subtree into 
      * the JTree as a child of the currently selected node on the JTree.
      * @return an array of Action's which create and insert subtrees into the
      * currently selected node on the JTree.
      */
    public Action[] getNewChildTagActions() {
	DEditNode parent = getSelected();
	if (parent == null) {
	    return null;
	}
	return getNewChildTagActions(parent);
    }

    /**
      * Gets Action's which will create and insert a chosen subtree into 
      * the JTree as a child of the input node on the JTree.
      * @param parent the DEditNode which the returned Action's will insert 
      * subtrees into as children
      * @return an array of Action's which create and insert subtrees into the
      * input node on the JTree.
      */
    public Action[] getNewChildTagActions(final DEditNode parent) {
	String[] names = schema.getNewChildTagNames(parent);
	if (names == null || names.length == 0) {
	    return null;
	}
	Action[] ret = new Action[names.length];
	for (int i = 0; i < ret.length; i++) {
	    final String name = names[i];
	    ret[i] = new AbstractAction(name) {
		    public void actionPerformed(ActionEvent ae) {
			createNewChildTag(parent,name);
			update(true);
		    } 

		    public String toString() {
			return (String)getValue(Action.NAME);
		    }
		};
	}
	return ret;
    }    
    
    /**
      * Creates a new child element tag in the XML by delegating the task to
      * the DEditSchema.
      * @param parent the parent to create a new child node for
      * @param newTagName the name of the new node to be created
      */
    private void createNewChildTag(DEditNode parent, String newTagName) {
	schema.createNewChildTag(parent,newTagName,model);
    }

    /**
      * Gets Action's which will switch the currently selected DEditNode
      * to a different DEditNode.
      * @return Action's which will switch the currently selected DEditNode
      * to a different DEditNode
      */
    public Action[] getSwitchTagToActions() {
	DEditNode current = getSelected();
	if (current == null) {
	    return null;
	}
	return getSwitchTagToActions(current);
    }
    
     /**
      * Gets Action's which will switch the input DEditNode
      * to a different DEditNode.
      * @param selected the returned Action's will change this DEditNode 
      * into a different DEditNode
      * @return Action's which will switch the input DEditNode
      * to a different DEditNode
      */
    public Action[] getSwitchTagToActions(final DEditNode selected) {
        nameList = new ArrayList();
	String[] nodeNames = schema.getSwitchTagToNames(selected,model);
	if (nodeNames == null || nodeNames.length == 0) {
	    return null;
	}
	Node next = null;
	final DEditNode parent = (DEditNode)selected.getParent();
	if (parent == null) {
	    return null;
	}
        for(int i=0; i<nodeNames.length; i++)
          nameList.add(nodeNames[i]);
        Collections.sort(nameList);
        for(int i=0; i<nodeNames.length; i++)
          nodeNames[i] = nameList.get(i).toString();
	final int index = parent.getIndex(selected);
	if (index+1 < parent.getChildCount()) {
	    DEditNode nextDEN = (DEditNode)parent.getChildAt(index+1);
	    next = nextDEN.getNode();
	}
	Action[] ret = new Action[nodeNames.length];
	final Node insertBefore = next;
	for (int i = 0; i < ret.length; i++) {
	    final String name = nodeNames[i];
	    Action temp = new AbstractAction(name) {
		    public void actionPerformed(ActionEvent ae) {
			DEditNode addable = makeDEditNode(name);
			addToUndoBuffer();
			selected.removeSelf();
			parent.insertAtIndex(addable,index,insertBefore);
			schema.generateRequiredChildren(addable,model);
		    }
		    public String toString() {
			return (String)getValue(Action.NAME);
		    }
		};
	    ret[i] = temp;
	}
	return ret;
    }

    /**
      * Gets Action's which paste the currently copied DEditNodes in various 
      * locations as children of the currently selected node
      * @param copied the currently copied DEditNodes
      * @return Action's which paste the currently copied DEditNodes in 
      * various locations as children of the currently selected node
      */
    public Action[] getPasteChildActions(DEditNode[] copied) {
	DEditNode selected = getSelected();
	if (selected == null) {
	    return null;
	}
	return getPasteChildActions(selected,copied);
    }

    /**
      * Returns an array of Actions to be used to create a "where you 
      * can paste" submenu.
      * @param denode the node being checked to see where the copied
      * DEditNode can be pasted as one of it's children
      * @param copied the currently copied DEditNode's
      * @return an array of Actions detailing paste-able locations.
      */
    public Action[] getPasteChildActions(DEditNode denode,
					 final DEditNode[] copied) {
	if (denode == null || copied == null) {
	    // shouldn't happen, paste should be 
	    //disallowed in this case
	    return null;
	}
	if (denode.isLeaf()) {
	    return null;
	}
	boolean validBefore = schema.isReady() && isValid();
	Vector toBeAdded = new Vector();
	Enumeration kids = denode.children();
	while (kids.hasMoreElements()) {
	    final DEditNode child = (DEditNode)kids.nextElement();
	    int index = getIndexOfChild(denode,child);
	    if (!validBefore || pasteBeforeCheck(child,copied)) {
		Action act = new AbstractAction("Paste before " 
						   + child.getDisplayName()) {
			public void actionPerformed(ActionEvent ae) {
			    addToUndoBuffer();
			    pasteSibTreeBefore(child,copied);
			}
			public String toString() {
			    return (String)getValue(Action.NAME);
			}
		    };
		toBeAdded.add(act);
	    }	
	}
	final DEditNode lastChild = (DEditNode)denode.getLastChild();
	if (lastChild != null) {
	    int index = getIndexOfChild(denode,lastChild);
	    
	    if (!validBefore 
		|| pasteAfterCheck(lastChild,copied)) {
		Action act = new AbstractAction("Paste at end") {
			public void actionPerformed(ActionEvent ae) {
			    addToUndoBuffer();
			    pasteSibTreeAfter(lastChild,copied);
			}
			public String toString() {
			    return (String)getValue(Action.NAME);
			}
		    };
		toBeAdded.add(act);
	    }	
	}
	Action[] ret = new Action[toBeAdded.size()];
	toBeAdded.toArray(ret);
	return ret;
    }

    /**
      * Creates Action's which will be for inserting the DEditNodes in nodes 
      * into DEditNode parent at the index provided.  
      * (createInsertActions(...)[i] inserts nodes[i])
      * @param nodes the DEditNode's the returned Action's will insert
      * @param parent the DEditNode into which the returned Action's will 
      * insert DEditNode's as children
      * @param index the index of parent's children at which to insert
      */
    private Action[] createInsertActions(final String[] nodeNames, 
					 final DEditNode parent,
					 final int index) {
	Action[] ret = new Action[nodeNames.length];
	int numSpaces = (parent.getLevel()) * 2;
	char[] spaces = new char[numSpaces];
	for (int i = 0; i < spaces.length; i++) {
	    spaces[i] = ' ';
	}
	final String indentSpaces = new String(spaces);		
	for (int i = 0; i < ret.length; i++) {
	    final String name = nodeNames[i];
	    ret[i] = new AbstractAction(name) {
		    public void actionPerformed(ActionEvent ae) {
			addToUndoBuffer();
			DEditNode nextSibling = null;
			Node parNode = parent.getNode();
			Document d = parNode.getOwnerDocument();
			Node nextNode = null;
			DEditDebug.println(this,6,"i " + index + " cc "
					   + parent.getChildCount());
			
			if (index < parent.getChildCount()) {
			    nextSibling = (DEditNode)parent.getChildAt(index);
			}
			if (nextSibling != null) {
			    nextNode = nextSibling.getNode();
			}
			DEditNode node = makeDEditNode(name);
			parent.insertAtIndex(node,index,nextNode);
			schema.generateRequiredChildren(node,model);
			update(true);
			owner.changeEditOptions(DEditXMLWindow.this);
		    }
		    
		    public String toString() {
			return (String)getValue(Action.NAME);
		    }
		};
	}
	return ret;
    }

    /**
      * Creates a new DEditNode (useful because DEditNode always wants a
      * DefaultTreeModel and the correct one is stored in this class, and 
      * it must have a Node created from the proper Document, also stored 
      * in this class).
      * @param nodeName the name of the Node to create and put in the 
      * returned DEditNode
      * @return a DEditNode created with the provided name
      */
    public DEditNode makeDEditNode(String nodeName) {
	return new DEditNode(document.createElement(nodeName),model);
    }

    /**
      * Creates a new DEditNode (useful because DEditNode always wants a
      * DefaultTreeModel and the correct one is stored in this class).
      * @param node the Node to put in the new DEditNode
      * @return a DEditNode created with the provided Node
      */
    public DEditNode makeDEditNode(Node node) {
	return new DEditNode(node,model);
    }

    /**
      * A convenience method that asks the schema to generate the children 
      * with the right model, and then tells the model that the node who just 
      * had children created for it changed.
      * @param node the DEditNode to generate children for
      */
    public void generateRequiredChildren(DEditNode node) {
	schema.generateRequiredChildren(node,model);
	model.nodeChanged(node);
    }
    
    /**
      * Limited accessor to TreeModel knowledge which returns the root 
      * DEditNode in this tree.
      * @return the root DEditNode in this tree
      */
    public DEditNode getRoot() {
	return (DEditNode)(model.getRoot());
    }

    /**
      * Tells the TreeModel that a node in the tree has changed
      * @param node the node that has changed
      */
    public void nodeChanged(DEditNode node) {
	model.nodeChanged(node);
    }

    /**
      * Tells the TreeModel that a node in the tree has drastically changed
      * @param node the node that has changed
      */
    public void nodeStructureChanged(DEditNode node) {
	model.nodeStructureChanged(node);
    }

    /**
      * Returns the index of a child w/respect to it's parent in the tree. 
      * Again this is just the DEditXMLWindow asking the model for information 
      * to shield the model from exposure to other classes.
      * @param parent the parent whose child's index we are looking for
      * @param child the node whose index as a child of parent we are 
      * searching for
      * @return the index of child in the children of parent
      */
    public int getIndexOfChild(DEditNode parent, DEditNode child) {
	return model.getIndexOfChild(parent,child);
    }

    /**
      * Asks the DOM Document of this DEditXMLWindow to create an attribute 
      * typed Node with given name.
      * @param attName the name to give to the attribute Node
      * @return an attribute typed Node with the given name
      */
    public Attr createAttribute(String attName) {
	return document.createAttribute(attName);
    }

    /**
      * Returns the first DEditNode currently selected in the tree.
      * @return the first DEditNode currently selected in the tree
      */
    public DEditNode getSelected() {
	   DEditNode[] selected = getMultipleSelected();
	   if (selected == null || selected.length != 1) {
	      return null;
	   }
	   return selected[0];
    }

    /**
      * Returns the DEditNodes currently selected in the tree.
      * @return the DEditNodes currently selected in the tree
      */
    public DEditNode[] getMultipleSelected() {
	   TreePath[] tp = tree.getSelectionPaths();
	   if (tp == null || tp.length == 0) {
	      return null;
	   }
	   DEditNode[] ret = new DEditNode[tp.length];
	   for (int i = 0; i < ret.length; i++) {
	      ret[i] = (DEditNode)tp[i].getLastPathComponent();
	   }
	   return ret;
    }

    /**
      * Checks to see if it would be legal to paste DEditNode copied before 
      * DeditNode selected.
      * @param selected the DEditNode to see if pasting before it with 
      * the copied DEditNode is legal
      * @param copied the DEditNode being pasted into the tree
      * @return true if it's legal to paste copied before selected
      */
    public boolean pasteBeforeCheck(DEditNode selected, DEditNode[] copied) {
	if (selected.isRoot()) {
	    return false;
	}
	boolean modified = modifiedSinceSaved;
	DEditNode[] newNodes = pasteSibTreeBefore(selected,copied);
	boolean ret = isValid();
	// undo changes made to check legality of pasting
	for (int i = 0; i < newNodes.length; i++) {
	    newNodes[i].removeSelf();
	}
	modifiedSinceSaved = modified;
	return ret;
    }
    
    /**
      * Checks to see if it would be legal to paste DEditNode copied after 
      * DeditNode selected.
      * @param selected the DEditNode to see if pasting after it with 
      * the copied DEditNode is legal
      * @param copied the DEditNode being pasted into the tree
      * @return true if it's legal to paste copied after selected
      */
    public boolean pasteAfterCheck(DEditNode selected, DEditNode[] copied) {
	if (selected.isRoot()) {
	    return false;
	}
	boolean modified = modifiedSinceSaved;
	DEditNode[] newNodes = pasteSibTreeAfter(selected,copied);
	boolean ret = isValid();
	// undo changes made to check legality of pasting
	for (int i = 0; i < newNodes.length; i++) {
	    newNodes[i].removeSelf();
	}
	modifiedSinceSaved = modified;
	return ret;
    }

    public void updateParameters(List<NameValue> list){
    	if(list == null)
    		return;
    	
    		
    }
    /**
      * Checks to see if XML document is valid which sets String in the 
      * DEditSchema that may be used to get more information
      * @return true if XML is valid given schema
      */
    public boolean isValid() {
	return schema.isValid(document);
    }

    /**
      * Updates some features of the DEditXMLWindow.
      */
    public void update(boolean changed) {
	modifiedSinceSaved = (modifiedSinceSaved || changed);
	resetDocStatus();
	text.setText(toXMLString());
    }

    /**
      * Returns true if the file represented by the DEditXMLWindow is unsaved,
      * meaning "Save" functions as "Save As...".
      * @return true if the file represented by the DEditXMLWindow is unsaved
      */
    public boolean unsaved() {
	return unsaved;
    }

    /**
      * Returns true if the XML Document has been modified since it was last 
      * saved, which means before closing it should prompt for saving. 
      * @return true if the XML Document has been modified since it was last 
      * saved
      */
    public boolean modifiedSinceSaved() {
	return modifiedSinceSaved;
    }

    /**
      * Saves the file currently being edited.
      * @param filename the name of the file to save.
      */
    public void saveFile(String filename) {
	   if (filename == null) return;
        // Add ".xml" if not present
        if(!filename.endsWith(".xml"))
             filename = filename.concat(".xml");
        System.out.println("Saving " + filename);
	
	    try {
		   FileWriter fw = new FileWriter(filename);
		   fw.write(toXMLString());
		   fw.close();
	    } catch (IOException ioe) {
		   JOptionPane.showMessageDialog(owner,"Couldn't save file: "  + ioe.toString());
		   return;
	    }
	    modifiedSinceSaved = false;
	    openFilename = filename;
	    treeFrame.setTitle(openFilename);
    }

    /**
      * Converts the DOM document into an String for display or 
      * printing.  Uses a workaround hand-made function for the applet version.
      * @return the DOM document as a String
      */
    public String toXMLString() {
	if (owner.isApplet()) { // (toXMLString2() crashes as applet)
	    return domWalk(document,0);
	} else {
	    return toXMLString2();
	}
    }
    
    
    /**
      * This method walks the DOM tree and converts it into a string for use
      * in display or printing to file.  This is a modified form of a simple 
      * function which printed to the terminal that I found (w/no copyright)
      * on a DOM tutorial online.
      * @param node the current node in our recursive descent
      * @param indent the indent to use on this recursion
      * @return a string representation of everything under node plus what 
      * came before */
    private String domWalk(Node node,int length) {
        String ret = "";
	DEditNode selTreeNode = getSelected();
	Node selected = null;
	if (selTreeNode != null) {
	    selected = selTreeNode.getNode();
	} else {
	    //caretPos = 0;
	}
	if (node == selected) {
	    //caretPos = length;
	    //System.out.println("caretPos = " + length);
	}
	int type = node.getNodeType();
        
	if (type == Node.DOCUMENT_NODE) { // this first string maybe should be
	    // formatted differently !!!
	    ret += "<?xml version=\"1.0\" encoding=\"" +
		"UTF-8" + "\"?>";                   
	}//end of document
	else if (type == Node.ELEMENT_NODE) {
	    ret += '<' + encode(node.getNodeName());
	    NamedNodeMap nnm = node.getAttributes();
	    if(nnm != null ) {
		int len = nnm.getLength() ;
		Attr attr;
		for (int i = 0; i < len; i++) {
		    attr = (Attr)nnm.item(i);
		    ret += ' ' + encode(attr.getNodeName()) + "=\""
			+ encode(attr.getNodeValue()) +  '"';
		}
	    }
	    ret += '>';
	}//end of element
	else if (type == Node.ENTITY_REFERENCE_NODE) {		    
	    ret += '&' + node.getNodeName() + ';';		    
	}//end of entity
        else if (type == Node.CDATA_SECTION_NODE) {
	    ret += "<![CDATA[" + node.getNodeValue() + "]]>";
	} else if (type == Node.TEXT_NODE) {
	    ret += encode(node.getNodeValue());
	} else if (type == Node.PROCESSING_INSTRUCTION_NODE) {
	    ret += "<?" + node.getNodeName();
	    String data = node.getNodeValue();
	    if (data != null && data.length() > 0) {
		ret += ' ' + data;
	    }
	    ret += "?>";		    
	} else {
	    System.err.println("Invalid type of node encountered while "
			       +"traversing DOM."); // bail out of function ???
	}
	
	//recurse
	for(Node child = node.getFirstChild(); child != null;
	    child = child.getNextSibling()) {
		ret += domWalk(child,length + ret.length());
	}
	
	//without this the ending tags will miss
	if (type == Node.ELEMENT_NODE) {
	    ret +="</" + encode(node.getNodeName()) + ">";
	}
	return ret;
    }

    /**
      * Helper for applet-induced hand-made transition from DOM to String.
      * Turns all standard escape characters for XML into the appropriate 
      * sequences. -- Perhaps should use URLEnconder.encode() instead !!!
      * @param input the string to change and return
      * @return input with special characters escaped to &__; sequences
      */
    private static String encode(String input) {
	input = input.replaceAll("[&]","&amp;");
	input = input.replaceAll("[<]","&lt;");
	input = input.replaceAll("[>]","&gt;");
	input = input.replaceAll("[\"]","&quot;");
	input = input.replaceAll("[']","&apos;");
	return input;
    }

    /**
      * Turns standard XML escapes back into proper characters. 
      * -- Perhaps should use URLEnconder.decode() instead !!!
      * @param input the input String to transform.
      * @return the transformed String
      */
    public static String decode(String input) {
	input = input.replaceAll("[&]lt;","<");
	input = input.replaceAll("[&]gt;",">");
	input = input.replaceAll("[&]quot;","\"");
	input = input.replaceAll("[&]apos;","'");
	input = input.replaceAll("[&]amp;","&");
	return input;
    }

    /**
      * Converts the DOM document into an String for display or 
      * printing.
      * @return the DOM document as a String
      */
    public String toXMLString2() {
	Node match = null;
	DEditNode selTreeNode = getSelected();
	if (selTreeNode != null) {
	    match = selTreeNode.getNode();
	}
	try {
	    TransformerFactory tFactory = TransformerFactory.newInstance();
	    Transformer tranformer = tFactory.newTransformer();
	    DOMSource source = new DOMSource(document);
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    StreamResult result = new StreamResult(baos);
	    tranformer.transform(source,result);
	    return baos.toString();
	} catch (TransformerConfigurationException tce) {
	    JOptionPane.showMessageDialog(owner,"Couldn't convert to XML: "
					  + tce.toString());
	} catch (TransformerException te) {
	    JOptionPane.showMessageDialog(owner,"Couldn't convert to XML: "
					  + te.toString());
	}
	return null;
    }
    
    /**
      * Instructs this DEditXMLWindow to display it's JInternalFrame which 
      * contains the text of the XML that would be generated by the tree.
      */
    public void showTextFrame() {
	textFrame.show();
    }

    /**
      * Gets the name of the file that is currently opened, or null for an 
      * unsaved file.
      * @return the name of the file that is currently opened, null for unsaved
      */
    public String getOpenFilename() {
	return openFilename;
    }
    
    /**
      * Returns true if this DEditXMLWindow's JInternalFrame is displayed. 
      * Might change to include textFrame and/or ask Plugin???
      * @return true if this DEditXMLWindow's JInternalFrame is displayed
      */
    public boolean isSelected() {
	return treeFrame.isSelected();
    }

    public String getTitle(){
    	return treeFrame.getTitle();
    }
    
    public Point getLocation(){
    	return treeFrame.getLocation();
    }
    
    public Dimension getDimension(){
    	return treeFrame.getSize();
    }
    
    /**
      * Perform closing operations on this DEditXMLWindow and then close it.
      * @return true if the window was actually closed
      */
    public boolean close() {
	if (modifiedSinceSaved) {
	    String currentName = openFilename;
	    String msg = "File " + openFilename + " changed but not saved.  "
		+"Save before closing?";
	    int choice = JOptionPane.showConfirmDialog(owner,msg,
						       "File Not Saved",
						       JOptionPane.YES_NO_CANCEL_OPTION);
	    if (choice == JOptionPane.YES_OPTION) {
		if (unsaved) {
		    openFilename = owner.getSaveFile();
		}
		if (openFilename == null) {
		    openFilename = currentName;
		    return false;
		}
		saveFile(openFilename);
	    }
	    if (choice == JOptionPane.CANCEL_OPTION) {
		return false;
	    }
	}
	owner.removeXmlWindow(this);
	treeFrame.dispose();
	return true;
    }

    /**
      * Disposes the DEditXMLWindow.  Needs only dispose the treeFrame 
      * because on close it disposes the other frames.
      */
    public void dispose() {
	treeFrame.dispose();
    }

    
    /** Remove the currently selected node. */
    public void removeCurrentNode() {
        TreePath currentSelection = tree.getSelectionPath();
        if (currentSelection != null) {
            DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode)
                         (currentSelection.getLastPathComponent());
            MutableTreeNode parent = (MutableTreeNode)(currentNode.getParent());
            if (parent != null) {
                model.removeNodeFromParent(currentNode);
                return;
            }
        } 

        // Either there was no selection, or the root was selected.
        toolkit.beep();
        JTreeToXMLDocument();
        update(true);
    }
    
    /** Remove the currently selected node. */
    public void editCurrentNode() {
        final TreePath currentSelection = tree.getSelectionPath();
        final javax.swing.JTextField text = new javax.swing.JTextField();
  	  	text.addFocusListener(new FocusAdapter(){
  		  public void focusLost(FocusEvent e){
  			  DEditNode myNode = (DEditNode)currentSelection.getLastPathComponent();
  			  TreePath parentPath = currentSelection.getParentPath();
  			  DEditNode parentNode = null;
  			  if(parentPath == null)
  				  parentNode = rootNode ;
  			  else 
  				  parentNode = (DEditNode)parentPath.getLastPathComponent();
  			  
  			  Node oldNode = myNode.getNode();
  			  Node newNode = oldNode.getOwnerDocument().createElement(text.getText());
  			  NodeList nl = oldNode.getChildNodes();
  			  for(int i =0; nl != null && i < nl.getLength(); i++)
  				  newNode.appendChild(nl.item(i));
  			  Node parent = parentNode.getNode();
  			  parent.replaceChild(newNode, oldNode);
  			  myNode.setUserObject(text.getText());
  			  //DEditNode pNode = (DEditNode)myNode.getParent();
  			  //int index = pNode.getIndex(myNode);
  			  //String newTagName = text.getText();
  			  //Node node = pNode.getNode().getOwnerDocument().createElement(newTagName);
  			  //DEditNode cNode = new DEditNode(node,model);
  			  //pNode.insertAtIndex(cNode,index,myNode.getNode());
  			  //myNode.removeSelf();
  			  tree.scrollPathToVisible(currentSelection);
  			  tree.setSelectionPath(currentSelection);
  		  }
  	  	});
  	  	DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
  	  	TreeCellEditor comboEditor = new DefaultCellEditor(text);
  	  	TreeCellEditor editor = new DefaultTreeCellEditor(tree, renderer, comboEditor);
  	  	tree.setCellEditor(editor);
        if (currentSelection != null) {
        	tree.scrollPathToVisible(currentSelection);
        	tree.setSelectionPath(currentSelection);
            tree.startEditingAtPath(currentSelection);
        } 
        update(true);
    }
    
    
    /** Remove the currently selected node. */
    public void editCurrentNode1() {
        final TreePath currentSelection = tree.getSelectionPath();
        final javax.swing.JTextField text = new javax.swing.JTextField();
  	  	text.addFocusListener(new FocusAdapter(){
  		  public void focusLost(FocusEvent e){
  			  DefaultMutableTreeNode node = (DefaultMutableTreeNode)currentSelection.getLastPathComponent();
  			  node.setUserObject(text.getText());
  			  tree.scrollPathToVisible(currentSelection);
  			  tree.setSelectionPath(currentSelection);
  		  }
  	  	});
  	  	DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
  	  	TreeCellEditor comboEditor = new DefaultCellEditor(text);
  	  	TreeCellEditor editor = new DefaultTreeCellEditor(tree, renderer, comboEditor);
  	  	tree.setCellEditor(editor);
        if (currentSelection != null) {
        	tree.scrollPathToVisible(currentSelection);
        	tree.setSelectionPath(currentSelection);
            tree.startEditingAtPath(currentSelection);
        } 
        JTreeToXMLDocument();
        update(true);
    }    
    public void JTreeToXMLDocument() {
        try {
            final DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            final Document document = builder.newDocument();
            DEditNode node = (DEditNode)model.getRoot();
            String name = (String)node.getUserObject();
            final Element root = document.createElement(name);
            //root.setAttribute("version", "1");
 
            addNode(document, root, rootNode);
 
            document.appendChild(root);
            this.document = document;
            //final Transformer t = TransformerFactory.newInstance().newTransformer();
            //OutputStream outputStream = new FileOutputStream("test.xml");
            //t.transform(new DOMSource(document), new StreamResult(outputStream));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        //} catch (TransformerException e) {
        //    e.printStackTrace();
        //} catch (FileNotFoundException e) {
        //    e.printStackTrace();
        }
        
    }
     
    protected Element createElement(DefaultMutableTreeNode node, Document document) {
        final Object data = node.getUserObject();
        final TreePath path = new TreePath(node.getPath());
        System.out.println("data = " + data);
        String tagName = data.toString();
        tagName = tagName.replaceAll("[\\s]", "_");
        System.out.println("tagName = " + tagName);
        final Element element = document.createElement(tagName);
        element.setAttribute("expanded", Boolean.toString(tree.isExpanded(path)));
        element.setAttribute("selected", Boolean.toString(tree.isPathSelected(path)));
        return element;
    }
 
    protected void addNode(Document document, Element parentNode, DefaultMutableTreeNode treeNode) {
        Enumeration children = treeNode.children();
        while (children.hasMoreElements()) {
            final DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
            final Element element = createElement(node, document);
            parentNode.appendChild(element);
            addNode(document, element, node);
        }
    }
    
    public void insertItemAbove(Object sibling){
        TreePath selectedPath = tree.getSelectionPath();
        DEditNode myNode = (DEditNode)selectedPath.getLastPathComponent();
        DEditNode pNode = (DEditNode)myNode.getParent();
        int index = pNode.getIndex(myNode);
        Node node = document.createElement((String)sibling);
        DEditNode cNode = new DEditNode(node,model);
        model.insertNodeInto(cNode, pNode, 
        		index);
        update(true);
    }
    
    public void insertItemBelow(Object sibling){
        TreePath selectedPath = tree.getSelectionPath();
        DEditNode myNode = (DEditNode)selectedPath.getLastPathComponent();
        DEditNode pNode = (DEditNode)myNode.getParent();
        int index = pNode.getIndex(myNode);
        Node node = document.createElement((String)sibling);
        DEditNode cNode = new DEditNode(node,model);
        model.insertNodeInto(cNode, pNode, 
        		index+1);
        update(true);
    }

    /** Add child to the currently selected node. */
    public DefaultMutableTreeNode addObject(Object child) {
        DEditNode parentNode = null;
        TreePath parentPath = tree.getSelectionPath();

        if (parentPath == null) {
            parentNode = rootNode;
        } else {
            parentNode = (DEditNode)
                         (parentPath.getLastPathComponent());
        }
        

        return addObject(parentNode, child, true);
    }

    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
                                            Object child) {
        return addObject(parent, child, false);
    }

    public DEditNode addObject(DefaultMutableTreeNode parent,
                                            Object child, 
                                            boolean shouldBeVisible) {
        //DefaultMutableTreeNode childNode = 
        //        new DefaultMutableTreeNode(child);
    	Node node = document.createElement((String)child);
    	DEditNode childNode = new DEditNode(node,model);
        if (parent == null) {
            parent = rootNode;
        }

        model.insertNodeInto(childNode, parent, 
                                 parent.getChildCount());
        
        // Make sure the user can see the lovely new node.
        if (shouldBeVisible) {
            tree.scrollPathToVisible(new TreePath(childNode.getPath()));
        }
        JTreeToXMLDocument();
        update(true);
        return childNode;
    }
    
    class MyTreeModelListener implements TreeModelListener {
        public void treeNodesChanged(TreeModelEvent e) {
        	TreePath currentSelection = tree.getSelectionPath();
            DEditNode myNode;
            myNode = (DEditNode)currentSelection.getLastPathComponent();
            document.renameNode(oldEditNode, document.getNamespaceURI(), (String)myNode.getUserObject());
			update(true);
            /*
             * If the event lists children, then the changed
             * node is the child of the node we've already
             * gotten.  Otherwise, the changed node and the
             * specified node are the same.
             */
			DEditNode node = null;
            try {
                int index = e.getChildIndices()[0];
                node = (DEditNode)(e.getTreePath().getLastPathComponent());
                node = (DEditNode)
                       (node.getChildAt(index));
            } catch (NullPointerException exc) {}

            System.out.println("The user has finished editing the node.");
            System.out.println("New value: " + node.getUserObject());
        }
        public void treeNodesInserted(TreeModelEvent e) {
        }
        public void treeNodesRemoved(TreeModelEvent e) {
        }
        public void treeStructureChanged(TreeModelEvent e) {
        }
    }
    class MouseHandler extends MouseAdapter{
        MouseHandler () {}

        public void mouseReleased(MouseEvent e) {
          int x = e.getX();
          int y = e.getY();
          TreePath path = tree.getPathForLocation(x,y);
          if (path == null){
              return;
          }
          tree.scrollPathToVisible(path);
          tree.setSelectionPath(path);
          DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();

          
          if( e.isPopupTrigger() || SwingUtilities.isRightMouseButton(e) ){ //right-click
            JPopupMenu popup = null;
            
                popup = new JPopupMenu();

                JMenuItem item = null;
                item = new JMenuItem("Insert Sub-Item");
                item.addActionListener(new ActionListener(){
                	public void actionPerformed(ActionEvent e){
                		TreePath path = tree.getSelectionPath();
                		DEditNode parent = (DEditNode)path.getLastPathComponent();
                		String newTagName = "New_Node_" + newNodeSuffix++;
                		Document d = parent.getNode().getOwnerDocument();
                		Node node = d.createElement(newTagName);
                		DEditNode newDENode = new DEditNode(node,model);
                		parent.insertAtEnd(newDENode);
                		update(true);
                	}
                });
                popup.add(item);
                if(!node.isRoot()){
                item = new JMenuItem("Insert Item Above");
                item.addActionListener(new ActionListener(){
                	public void actionPerformed(ActionEvent e){
                		//insertItemAbove("New Node " + newNodeSuffix++);
                		TreePath selectedPath = tree.getSelectionPath();
                		DEditNode myNode = (DEditNode)selectedPath.getLastPathComponent();
                		DEditNode pNode = (DEditNode)myNode.getParent();
                        int index = pNode.getIndex(myNode);
                        String newTagName = "New_Node_" + newNodeSuffix++;
                        Node node = pNode.getNode().getOwnerDocument().createElement(newTagName);
                        DEditNode cNode = new DEditNode(node,model);
                        pNode.insertAtIndex(cNode,index,myNode.getNode());
                        update(true);
                	}
                });
                popup.add(item);
                item = new JMenuItem("Insert Item Below");
                item.addActionListener(new ActionListener(){
                	public void actionPerformed(ActionEvent e){
                		//insertItemBelow("New Node " + newNodeSuffix++);
                		TreePath selectedPath = tree.getSelectionPath();
                		DEditNode myNode = (DEditNode)selectedPath.getLastPathComponent();
                		DEditNode pNode = (DEditNode)myNode.getParent();
                        int index = pNode.getIndex(myNode);
                        String newTagName = "New_Node_" + newNodeSuffix++;
                        Node node = pNode.getNode().getOwnerDocument().createElement(newTagName);
                        DEditNode cNode = new DEditNode(node,model);
                        pNode.insertAtIndex1(cNode,index+1,myNode.getNode());
                        update(true);

                	}
                });
                popup.add(item);
                
                item = new JMenuItem("Delete");
                item.addActionListener(new ActionListener(){
                	public void actionPerformed(ActionEvent e){
                		//removeCurrentNode();
                		TreePath path = tree.getSelectionPath();
                		DEditNode selected = (DEditNode)path.getLastPathComponent();
                		selected.removeSelf();
                		update(true);
                	}
                });
                popup.add(item);

                }
                item = new JMenuItem("Set Attributes");
                item.addActionListener(new ActionListener(){
                	public void actionPerformed(ActionEvent e){
                		TreePath path = tree.getSelectionPath();
                		DEditNode edit = (DEditNode)path.getLastPathComponent();
                		Node node = edit.getNode();
                		NamedNodeMap map = node.getAttributes();
                		Vector list = new Vector();
                		for(int i = 0; map != null && i < map.getLength(); i++){
                			Node n = map.item(i);
                			if(n.getNodeType() == Node.ATTRIBUTE_NODE){
                				Vector v = new Vector();
                				v.add(n.getNodeName());
                				v.add(n.getNodeValue());
                				list.add(v);
                			}
                		}
                		ParameterDialog dialog = new ParameterDialog(owner,"Edit Parameter(s)",false, (Element)node);
                		dialog.setVisible(true);
                	}
                });
                popup.add(item);
                item = new JMenuItem("Set Text Value");
                item.addActionListener(new ActionListener(){
                	public void actionPerformed(ActionEvent e){
                		String text = (String) JOptionPane.showInputDialog(owner, "Enter a value:","");
                		if(text == null)
                			text = "";
                		TreePath path = tree.getSelectionPath();
                		DEditNode edit = (DEditNode)path.getLastPathComponent();
                		Node node = edit.getNode();
                		node.setTextContent(text);
                		update(true);
                	}
                });
                popup.add(item);
                item = new JMenuItem("Edit");
                item.addActionListener(new ActionListener(){
                	public void actionPerformed(ActionEvent e){
                		TreePath path = tree.getSelectionPath();
                		DEditNode edit = (DEditNode)path.getLastPathComponent();
                		oldEditNode = edit.getNode();
                		
                		
                		final TreePath currentSelection = tree.getSelectionPath();
                        final javax.swing.JTextField text = new javax.swing.JTextField();
                  	  	text.addFocusListener(new FocusAdapter(){
                  		  public void focusLost(FocusEvent e){
                  			  DEditNode myNode = (DEditNode)currentSelection.getLastPathComponent();
                  			  myNode.setUserObject(text.getText());
                  			  tree.scrollPathToVisible(currentSelection);
                			  tree.setSelectionPath(currentSelection);
                  		  }
                  	  	});
                		//if (path != null) {
                        //	tree.scrollPathToVisible(path);
                        //	tree.setSelectionPath(path);
                        //    tree.startEditingAtPath(path);
                        //}
                		
                		
                  	  	DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
                	  	TreeCellEditor defaultEditor = new DefaultCellEditor(text);
                	  	TreeCellEditor editor = new DefaultTreeCellEditor(tree, renderer, defaultEditor);
                	  	tree.setCellEditor(editor);
                	  	if (currentSelection != null) {
                	  		tree.scrollPathToVisible(currentSelection);
                	  		tree.setSelectionPath(currentSelection);
                	  		tree.startEditingAtPath(currentSelection);
                	  	} 
                		//editCurrentNode();
                	}
                });
                     
                popup.add(item);
                
                popup.show(e.getComponent(),x,y);
          }
        }
      }
          
      class PopupListener extends MouseAdapter {
  	    public void mousePressed(MouseEvent e) {
  	      showPopup(e);
  	    }
  	    public void mouseReleased(MouseEvent e) {
  	      showPopup(e);
  	    }
  	    private void showPopup(MouseEvent e) {
  	      if (e.isPopupTrigger()) {
  	        popupMenu.show(e.getComponent(), e.getX(), e.getY());
  	      }
  	    }
  	  }          
}
