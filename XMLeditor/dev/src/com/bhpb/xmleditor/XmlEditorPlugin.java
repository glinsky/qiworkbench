/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.swing.JOptionPane;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;       // JE FIEL
import com.bhpb.mail.Smtp;
import com.bhpb.mail.SmtpConverter;
import com.bhpb.distribution.DistConst;
import com.bhpb.xmleditor.distribution.EditorDistConst;
import com.bhpb.xmleditor.distribution.EditorDistribution;
import com.bhpb.xmleditor.model.DEditXMLWindow;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.distribution.IDistribution;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.model.PluginModel;
import com.bhpb.qiworkbench.compAPI.model.PackageLoad;
import com.bhpb.qiworkbench.compAPI.model.NodePackage;
import com.bhpb.qiworkbench.compAPI.model.PluginModelConverter;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
/**
 * XmlEditorPlugin, a plugin componet of qiWorkbench. It is started as a thread by
 * the Messenger Dispatcher that executes the "Activate plugin" command.
 */
public class XmlEditorPlugin extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(XmlEditorPlugin.class.getName());
    private static String myCID = "";
    private static Thread pluginThread;

    private XmlEditorGUI gui;
    private MessagingManager messagingMgr;
    private boolean pleaseStop = false;
    //private static SimpleDateFormat
    //       formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
    String project = "";
    String fileSystem = "";
    public static final String defaultPackage = "script";
    IDistribution prepare;
    private static int saveAsCount = 0;
    private MessagingManager requestMsgManager = null;
    
    protected static String getCID() {
        return myCID;
    }

    /** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
    private IComponentDescriptor projMgrDesc = null;

    /** Metadata of project associated with */
    private QiProjectDescriptor qiProjDesc = new QiProjectDescriptor();

    
    public MessagingManager getRequestMsgManager(){
    	return requestMsgManager;
    }
    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjDesc;
    }

    /**
     * Get the component descriptor of the associated PM and therefore project.
     * @return Component descriptor of the associated PM
     */
    public IComponentDescriptor getQiProjMgrDescriptor() {
        return projMgrDesc;
    }

    public IComponentDescriptor getComponentDescriptor(){
        return messagingMgr.getMyComponentDesc();
    }

    /** Get Messaging Manager of WaveletDecomp component.
     *  @return  MessagingManager
     */
    public MessagingManager getMessagingMgr(){
        return messagingMgr;
    }

    /*Initialize the plugin component:
     *<ul>
     *   <li>Create its messaging manager</li>
     *   <li>Register with the Message Dispatcher</li>
     *</ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();

            messagingMgr = new MessagingManager();
            myCID = Thread.currentThread().getName();
            messagingMgr.registerComponent(QIWConstants.PLUGIN_AGENT_COMP,
                    EditorDistConst.EDITOR, myCID);

            prepare = new EditorDistribution();
            prepare.makeNamedDir(prepare.getProperty(EditorDistribution.LAST_DIST));
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in xmlEditorPlugin.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

    /*Initialize the plugin component*/
    public void run() {
        init();

        //Check if associated with a PM. If not (because was restored), discover the PM
        //with a matching PID. There will be one once its GUI comes up.
        String myPid = QiProjectDescUtils.getPid(qiProjDesc);
        while (projMgrDesc == null && !myPid.equals("")) {
            //NOTE: Request will stay forwever on outstanding request list if no PM with matching PID.
            //      This can occur when restoring the workbench and the PM hasn't been restored
            //      or it is being restored but its GUI is not yet up.
            //Message Dispatcher will broadcast command to all PMs. Only PM with matching PID
            //will send back a response.
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
            try {
                //wait and then try again
                Thread.currentThread().sleep(1000);
                //if a message arrived, process it. It probably is the response from a PM.
                if (!messagingMgr.isMsgQueueEmpty()) break;
            } catch (InterruptedException ie) {
                continue;
            }
        }

        //process any messages received from other components
        while(pleaseStop == false) {
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if (msg != null){
//            	 If it is a response with a timed-out request, dequeue it
                if (msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip()) {
                    if (messagingMgr.getMatchingOutstandingRequest(msg) == null) {
                        msg = messagingMgr.getNextMsg();
                        logger.warning("Consuming response message which lacks a matching outstanding request. Response msg: " + msg);
                    }
                } else { // it is a request or asynchronous response, process it
                	msg = messagingMgr.getNextMsgWait();
                	try{
                		processMsg(msg);
                	}catch(Exception e){
                		e.printStackTrace();
                		logger.warning(ExceptionMessageFormatter.getFormattedMessage(e));
                	}
                }
            }
        }
    }
    /* Launch the XmlEditor plugin:
     *<ul>
     *  <li>Start up the plugin thread which will initialize the plugin.</li>
     *</ul>
     *<p>
     *NOTE: Each thread's init() must finish before the next thread is
     *started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        XmlEditorPlugin sdPluginInstance = new XmlEditorPlugin();
        String cid = args[0];
        pluginThread = new Thread(sdPluginInstance, cid);
        pluginThread.start();
        long threadId = pluginThread.getId();
        logger.info("XmlEditor Thread-"+Long.toString(threadId)+" started");
        //wait until the plugin's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }

    /**
     * Find the request matching the response and process the response based on the request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        IQiWorkbenchMsg request = null;
//        String cmd = msg.getCommand();
        List<String> strList;
        if(messagingMgr.isResponseMsg(msg)) {  // if a response, process and consume it
            String cmd  = msg.getCommand();
            logger.info("--- cmd = " + cmd);
            //request = messagingMgr.checkForMatchingRequest(msg);

            if (messagingMgr.isResponseFromMsgDispatcher(msg) &&
                !cmd.equals(QIWConstants.ACTIVATE_PLUGIN_CMD) ) { //from the message dispatcher ?

              if(cmd.equals(QIWConstants.NULL_CMD)) return;

            } else if(cmd.equals(QIWConstants.ACTIVATE_PLUGIN_CMD)) {
                strList = (List<String>)msg.getContent();
                String which  = strList.get(1);
                String pkgSet = strList.get(2);
                prepare.makeNamedDir(pkgSet);
                PackageLoad pLoad = getPackageLoad(pkgSet, which);
                pLoad.setKeyValue("fromParent", "false");
                Point p = gui.getLocation();
                Dimension d = gui.getSize();
                gui = new XmlEditorGUI(this, pLoad);
                gui.setLocation(p);
                gui.setSize(d);
                IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
                msg.setProducerCID(wbMgr.getCID());
                msg.setCommand(QIWConstants.INVOKE_SELF_CMD);
                messagingMgr.sendResponse(msg, XmlEditorGUI.class.getName(), gui);
                return;
            } else  if (cmd.equals("OPEN")) {
                strList = (List<String>)msg.getContent();
                gui.open2Files(strList.get(0), strList.get(1));

            } else  if (cmd.equals(QIWConstants.GET_PROJ_INFO_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                qiProjDesc = (QiProjectDescriptor)projInfo.get(1);

                //reset window title (if GUI up)
                if (gui != null) {
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
                    gui.resetTitle(projName);
                }
            } else if (cmd.equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                projMgrDesc = (IComponentDescriptor)projInfo.get(0);
                qiProjDesc = (QiProjectDescriptor)projInfo.get(1);
                String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
                gui.resetTitle(projName);
            } else
                logger.warning("XmlEditorPlugin: Response to " + cmd + " command not processed " + MsgUtils.toString(msg));
            return;
        } else if(messagingMgr.isRequestMsg(msg)) { //if a request, process and send back a response
//          String cmd = msg.getCommand();
          String cmd = MsgUtils.getMsgCommand(msg);
          if(cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD)    || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) ||
             cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) || cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
            if(XmlEditorConstants.DEBUG_PRINT > 0)
                System.out.println(CompDescUtils.getDescPreferredDisplayName(messagingMgr.getMyComponentDesc()) + " quitting");
            try {
                if(gui != null && gui.isVisible()) gui.closePluginGUI();
                messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
            } catch (Exception e) {
                IQiWorkbenchMsg res = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Unknown exception occurred with " + myCID);
                messagingMgr.routeMsg(res);
            }
            pleaseStop = true;
            messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,myCID + " is successfully deactivated.");
          } else if(cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
                if(gui != null) gui.setVisible(true);

          } else if(cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
              if(XmlEditorConstants.DEBUG_PRINT > 0) System.out.println(myCID + " quitting");
              if(gui != null){
                  messagingMgr.sendResponse(msg,Component.class.getName(),(Component)gui);
                  gui.closePluginGUI();
              }

          } else if(cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
              String preferredDisplayName = (String)msg.getContent();
              if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                  CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                  gui.renamePlugin(preferredDisplayName);
              }
              messagingMgr.sendResponse(msg,QIWConstants.COMP_DESC_TYPE, messagingMgr.getMyComponentDesc());

          } else if(cmd.equals(QIWConstants.SAVE_COMP_CMD)) {
              messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, genState());

          } else if(cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
              Node node = (Node)msg.getContent();
              restoreState(node);
              messagingMgr.sendResponse(msg, XmlEditorGUI.class.getName(), gui);

          } else if(cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
              messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, genStateAsClone());

          } else if(cmd.equals(QIWConstants.INVOKE_SELF_CMD )) {
              ArrayList msgList = (ArrayList)msg.getContent();
              String invokeType = (String)msgList.get(0);
              if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)){
                  Node node = ((Node)((ArrayList)msg.getContent()).get(2));
                  restoreState(node);
              } else if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)){
//                qiProjDesc = ((QiProjectDescriptor)((ArrayList)msg.getContent()).get(2));
//TODO: get project once notified of associated PM
                  project = QiProjectDescUtils.getProjectPath(qiProjDesc);
                  //PackageLoad pLoad = getPackageLoad();
                  PackageLoad pLoad = getInitialDefaultPackageLoad();
                  //gui = new XmlEditorGUI(this, null);
                  pLoad.setLastXml("");
                  gui = new XmlEditorGUI(this, pLoad);
                  java.awt.Point p = (java.awt.Point)msgList.get(1);
                  if(p != null)
                	  gui.setLocation(p);
              }

              //Send a normal response back with the plugin's JInternalFrame and
              //let the Workbench Manager add it to the desktop and make it visible.

              messagingMgr.sendResponse(msg, XmlEditorGUI.class.getName(), gui);

          } else if(cmd.equals(QIWConstants.RENAME_COMPONENT)) {
              ArrayList<String> names = new ArrayList<String>(2);
              names = (ArrayList)msg.getContent();
              if(XmlEditorConstants.DEBUG_PRINT > 0)
                gui.renamePlugin(names.get(1));
          }
          else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
              //Just a notification. No response required or expected.
              projMgrDesc = (IComponentDescriptor)msg.getContent();
              //Note: Cannot get info about PM's project because GUI may not be up yet.
              if (projMgrDesc != null)
                  messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
          } else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
              ArrayList info = (ArrayList)msg.getContent();
              IComponentDescriptor pmDesc = (IComponentDescriptor)info.get(0);
              QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
              //ignore message if not from associated PM
              if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
                  qiProjDesc = projDesc;
//TODO: update PID [get from projDesc?]
                  //update window title
                  String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
                  gui.resetTitle(projName);
              }
          } else if(cmd.equals(QIWConstants.CONTROL_COMPONENT_CMD)){
        	  ArrayList params = (ArrayList)msg.getContent();
        	  requestMsgManager = (MessagingManager)params.get(0);
        	  String dist = (String)params.get(1);
        	  String pkg = (String)params.get(2);
        	  Node node = (Node)params.get(3);
        	  //logger.info("xml = " + nodeToXMLString(node));
        	  PackageLoad pLoad = getPackageLoad(dist, pkg);
              prepare.makeNamedDir(dist);
              //prepare.makeNamedDir(pkgSet);
              //PackageLoad pLoad = getPackageLoad(pkgSet, packName);
              pLoad.setKeyValue("fromParent", "true");
              gui.switchPackage(pLoad);
              //activateEditorPlugin(pkg,dist);
              gui.buildXmlEditorFromXMLNode1(node);
        	  gui.resetMenuItems(false);
          }else if(cmd.equals(QIWConstants.IS_COMPONENT_UI_READY_CMD)){
        	  if(gui != null)
        		  messagingMgr.sendResponse(msg, Boolean.class.getName(), new Boolean(true));
        	  else
        		  messagingMgr.sendResponse(msg, Boolean.class.getName(), new Boolean(false));
          }
          else
            logger.warning("XmlEditorPlugin: Request not processed, requeued: " + msg.toString());
          return;
        }
    }
    
    private String nodeToXMLString(Node node){
    	String xml = "";
    	if(node == null)
    		return xml;
    	try {   
    		TransformerFactory tFactory = TransformerFactory.newInstance();
    		Transformer tranformer = tFactory.newTransformer();
    	   	DOMSource source = new DOMSource(node);
    	   	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	   	StreamResult result = new StreamResult(baos);
    	   	tranformer.transform(source,result);
    	   	xml = baos.toString();
        } catch (TransformerConfigurationException tce) {
        	tce.printStackTrace();
        } catch (TransformerException te) {
        	te.printStackTrace();
        }
        return xml;
    }

    /**
     * Generate state information into xml string format
     * @return  String
     */
    public String genState(){
        savePkgConfigFile(false);
        saveMailConfigFile(false);
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
         content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
         content.append(gui.genState());
         content.append("</component>\n");
         return content.toString();
    }

    /**
     * Have the state manager store the state
     */
    public void saveState() {
         IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
             messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());        		 
    }
    
    public void saveStateAsClone() {
    	IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,stMgrDesc,
                                             QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }    
    /**
     * Generate state information into xml string format in repsonse to save as command
     * @return  String
     */
    public String genStateAsClone(){
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        String displayName = "";
        saveAsCount++;
        if (saveAsCount == 1)
            displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
        else
            displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
        content.append(gui.genState());
        content.append("</component>\n");

        return content.toString();

    }

    /**
     * call the state manager to store the state then quit the component
     */
    public void saveStateThenQuit() {
         IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
         messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_THEN_QUIT_CMD,stMgrDesc,
                                              QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }
    
    /** Send a message to the Workbench Manager to remove this instance of XmlEditor from the component tree and send a message to it to deactivate itself.
     */
    public void deactivateSelf(){
        //savePkgConfigFile(true);
        //ask Workbench Manager to remove xmlEditor from workbench GUI and then send it back to self to deactivate self
        if (gui.fromParent) {
          IComponentDescriptor wbMgr =
              messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
          messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.QUIT_COMPONENT_CMD,wbMgr,
                                 QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
        }else {
            gui.close();
        }

    }

    public String getDisplayNum() {
        String retStr = null;
        String dispId = CompDescUtils.getDescDisplayName(messagingMgr.getMyComponentDesc());
        int index = dispId.indexOf("#");
        if (index > 0) {
             retStr = dispId.substring(index);
        }
        return retStr;
    }

    /**
     * activate plugin component inside  the component
    */
    public void activateEditorPlugin(String packName, String pkgSet){
        logger.info("----------0  packName = " + packName + " pkgSet = " + pkgSet);
        /*
        List<String> params = new ArrayList<String>();
        params.add(EditorDistConst.EDITOR);
        params.add(packName);
        params.add(pkgSet);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.ACTIVATE_PLUGIN_CMD,
                                 QIWConstants.ARRAYLIST_TYPE, params);
        */
        
        Vector<DEditXMLWindow> windows = gui.getWindows();
        if(windows != null && windows.size() > 0){
        	int st = JOptionPane.showConfirmDialog(gui,"All the current XML windows will be closed before switching the package. You might lose some xml data. Are you sure you want to proceed?","Switch Package Confirmation",JOptionPane.YES_NO_OPTION);
            if(st == JOptionPane.NO_OPTION)
            	return;
            gui.closeAllXMLWindows();
        }
        prepare.makeNamedDir(pkgSet);
        PackageLoad pLoad = getPackageLoad(pkgSet, packName);
        pLoad.setKeyValue("fromParent", "true");
        gui.switchPackage(pLoad);
        ///PluginModel fModel = getPkgConfig();
        ///fModel.setActivePackage(packName);
        //gui = new XmlEditorGUI(this, pLoad);
        //ComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        //QiWorkbenchMsg msg = new QiWorkbenchMsg();
        //msg.setProducerCID(wbMgr.getCID());
        //msg.setCommand(QIWConstants.INVOKE_SELF_CMD);
        //messagingMgr.sendResponse(msg, XmlEditorGUI.class.getName(), gui);
    }


    public void savePkgConfigFile(boolean reset) {
        PluginModel obj = getPkgConfig();
        XStream xStream = new XStream(new DomDriver());

    // JE FIELD added remove all xstream security
    xStream.addPermission(AnyTypePermission.ANY);

        xStream.alias(DistConst.CONFIG_NAME, PluginModel.class);
        xStream.registerConverter(new PluginModelConverter());
        String xmlFile = prepare.getProperty(EditorDistribution.PKG_CONFIG);

        messagingMgr.saveToFile(xmlFile,xStream,obj);

    }

    public void saveMailConfigFile(boolean reset) {
        Smtp obj = getMailConfig();
        XStream xStream = new XStream(new DomDriver());

    // JE FIELD added remove all xstream security
    xStream.addPermission(AnyTypePermission.ANY);

        xStream.alias(DistConst.CONFIG_NAME, Smtp.class);
        xStream.registerConverter(new SmtpConverter());
        String xmlFile = prepare.getProperty(EditorDistribution.EMAIL_CONFIG);
        messagingMgr.saveToFile(xmlFile,xStream,obj);

    }

    public void savePkgObject(Object obj) {
        messagingMgr.saveState(prepare.getName(),
                               DistConst.CONFIG_NAME, obj);
    }

    public void saveMailObject(Object obj) {
        messagingMgr.saveState(EditorDistribution.EMAIL_CONFIG,
                               DistConst.CONFIG_NAME, obj);
    }

    public PluginModel getPkgConfig() {
        PluginModel cModel = (PluginModel)messagingMgr.getState(
                              prepare.getName(),
                              DistConst.CONFIG_NAME);

        if (cModel == null) {
             XStream xStream = new XStream(new DomDriver());

    // JE FIELD added remove all xstream security
    xStream.addPermission(AnyTypePermission.ANY);

             xStream.alias(DistConst.CONFIG_NAME, PluginModel.class);
             xStream.registerConverter(new PluginModelConverter());
             String xmlFile = prepare.getProperty(EditorDistribution.PKG_CONFIG);
             logger.info("------------getPkgConfig() = " + xmlFile);
             File aFile= new File(xmlFile);
             if (!aFile.exists()) {
                 cModel = new PluginModel();
                 cModel.defaultModel();
                 savePkgObject(cModel);
             } else {
                 cModel = (PluginModel)messagingMgr.loadFromFile(
                            prepare.getName(), DistConst.CONFIG_NAME , xmlFile, xStream);
             }
        }
        return cModel;
    }

    public Smtp getMailConfig() {
        Smtp model = (Smtp)messagingMgr.getState(EditorDistribution.EMAIL_CONFIG,
                                                  DistConst.CONFIG_NAME);

        if (model == null) {
             XStream xStream = new XStream(new DomDriver());

    // JE FIELD added remove all xstream security
    xStream.addPermission(AnyTypePermission.ANY);

             xStream.alias(DistConst.CONFIG_NAME, Smtp.class);
             xStream.registerConverter(new SmtpConverter());
             String xmlFile = prepare.getProperty(EditorDistribution.EMAIL_CONFIG);
             logger.info("------------file = " + xmlFile);
             File aFile= new File(xmlFile);
             if (!aFile.exists()) {
                 model = new Smtp();
                 model.defaultModel();
                 saveMailObject(model);
             } else {
                 model = (Smtp)messagingMgr.loadFromFile(
                         EditorDistribution.EMAIL_CONFIG, DistConst.CONFIG_NAME , xmlFile, xStream);
             }
        }
        return model;
    }

    public String getStrPkg() {
        XStream xStream = new XStream(new DomDriver());

    // JE FIELD added remove all xstream security
    xStream.addPermission(AnyTypePermission.ANY);

        xStream.alias(DistConst.CONFIG_NAME, PluginModel.class);
        xStream.registerConverter(new PluginModelConverter());
        return xStream.toXML(getPkgConfig());
    }

    public String getStrMail() {
        XStream xStream = new XStream(new DomDriver());

    // JE FIELD added remove all xstream security
    xStream.addPermission(AnyTypePermission.ANY);

        xStream.alias(DistConst.CONFIG_NAME, Smtp.class);
        xStream.registerConverter(new SmtpConverter());
        return xStream.toXML(getMailConfig());
    }

    public PackageLoad getPackageLoad() {
        PluginModel fModel = getPkgConfig();
        String activePack = fModel.getActivePackage();
        logger.info("---------------xml = " + fModel.getLastXml());
        logger.info("---------------activePack = " + activePack);
        PackageLoad load = fModel.getPackageLoad(prepare, activePack);
        load.setPkgSet(prepare.getName());
        return load;
    }

    private PackageLoad getInitialDefaultPackageLoad() {
        PluginModel fModel = getPkgConfig();
        logger.info("---------------xml = " + fModel.getLastXml());
        PackageLoad load = fModel.getPackageLoad(prepare, defaultPackage);
        load.setPkgSet(prepare.getName());
        return load;
    }
    
    public PackageLoad getPackageLoad(String pkgSet, String packName) {
        PluginModel fModel = getPkgConfig();
        PackageLoad pLoad = fModel.getPackageLoad(prepare, packName);
        pLoad.setPkgSet(pkgSet);
        return pLoad;
    }


    public IDistribution getPrepare() {
        return prepare;
    }

    public void setPrepare(IDistribution prepare) {
        this.prepare = prepare;
    }

    /** restore plugin and it's gui to previous condition
    *
    * @param node xml containing saved state variables
    */
   void restoreState(Node node){
       String preferredDisplayName = ((Element)node).getAttribute("preferredDisplayName");
       NodeList children = node.getChildNodes();
       for(int i = 0; i < children.getLength(); i++){
           Node child = children.item(i);
           if(child.getNodeType() == Node.ELEMENT_NODE){
               if(child.getNodeName().equals(XmlEditorGUI.class.getName())){
                   Element el = (Element) child;
                   String dist = el.getAttribute("distribution");
                   String pkg  = el.getAttribute("package");
                   //String freeform  = el.getAttribute("freeform");
                   //boolean free = false;
                   //if(freeform != null && freeform.equals("yes"))
                	//   free = true;
                   PackageLoad pload = getPackageLoad(dist, pkg);
                   prepare.makeNamedDir(dist);
                   gui = new XmlEditorGUI(this, pload);
                   gui.buildWindowsFromXMLNode(node);

                   String hStr = el.getAttribute("height");
                   int height = Integer.valueOf(hStr).intValue();
                   String wStr = el.getAttribute("width");
                   int width = Integer.valueOf(wStr).intValue();

                   String X = el.getAttribute("x");
                   int x = Integer.valueOf(X).intValue();
                   String Y = el.getAttribute("y");
                   int y = Integer.valueOf(Y).intValue();

                   if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
                       CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                       gui.renamePlugin(preferredDisplayName);
                   }
                   gui.setLocation(x,y);
                   gui.setSize(width,height);
                   String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
                   gui.resetTitle(projName);
               }
           }
       }
   }

   public List<NodePackage> getNewPkgLoadList() {
        Object pkgObj = messagingMgr.getState(EditorDistribution.PKG_CONFIG,
                                              XmlEditorConstants.ADD_PACKS);
        List<NodePackage> list = null;
        if (pkgObj == null)
            list = new ArrayList<NodePackage>();
        else
           list = (List<NodePackage>)pkgObj;

        return list;
    }

    public void setNewPkgLoadList(List<NodePackage>obj) {
        messagingMgr.saveState(EditorDistribution.PKG_CONFIG,
                               XmlEditorConstants.ADD_PACKS, obj);
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public QiProjectDescriptor getQiProjDesc() {
        return qiProjDesc;
    }

    public void setQiProjDesc(QiProjectDescriptor qiProjDesc) {
        this.qiProjDesc = qiProjDesc;
    }

}
