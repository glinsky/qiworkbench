/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;

import java.awt.event.*;
import com.bhpb.xmleditor.XmlEditorGUI;
/**
 * Title:        Exit Action <br><br>
 * Description:  exit internal frame.<br><br>
 *
 * Copyright:    Copyright (c) 2006 <br>
 * Company:      BHP Petroleum <br>
 * @author       Charlie Jiang
 * @version 1.0
 */

public class ExitAction extends AbstractAction {
    private XmlEditorGUI gui;

    public ExitAction(XmlEditorGUI _gui) {
        super("Exit", "Exit the program");
        gui = _gui;
    }

    public void actionPerformed(ActionEvent e) {
        //gui.closeAllInternalFrames();
        gui.getAgent().deactivateSelf();
        gui.setVisible(false);
        gui.dispose();
    }
}
