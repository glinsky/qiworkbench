/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JFileChooser;
import com.bhpb.xmleditor.swing.TabManager;
import com.bhpb.xmleditor.swing.model.EditorFileFilter;
/**
 * Title:        XmlEditor <br><br>
 * Description:  This action will load plugin with specified schema
 *               <br><br>
 * @version 1.0
 */
public class ImportAction implements ActionListener {
    private TabManager tManager;
    private String desc;
    JFileChooser fc;
    
    public  ImportAction(TabManager tManager, String desc) {
    	this.tManager = tManager;
    	this.desc     = desc;
    }
    
    public JFileChooser getFileChooser() {
    	return fc;
    }
    
    /**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
    public void actionPerformed(ActionEvent e){
    	fc = new JFileChooser();
    	EditorFileFilter filter = new EditorFileFilter(
    		    new String[] {"qid", "QID", "qic", "QIC"}, desc);
    	fc.addChoosableFileFilter(filter);
        fc.setFileFilter(filter);
        fc.setFileHidingEnabled(false);
        //fc.setControlButtonsAreShown(false);
		// show the filechooser
		//Frame parent = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, gui);
		
		int retVal = fc.showOpenDialog(new JFrame());
		if (retVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			String fname = file.getName();
			String pkgSet = fname.substring(0, fname.length()-4);
			tManager.getUI().getAgent().getPrepare().bhpUnZip(fname, file.getAbsolutePath());
			tManager.getUI().getAgent().activateEditorPlugin("", pkgSet);	
		}  
    }
          
}

