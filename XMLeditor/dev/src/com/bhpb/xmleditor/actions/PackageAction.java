/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;

import java.awt.event.*;
import com.bhpb.xmleditor.XmlEditorGUI;
/**
 * Title:        XmlEditor <br><br>
 * Description:  This action will load plugin with specified schema
 *               <br><br>
 * @version 1.0
 */
public class PackageAction implements ActionListener {
	private XmlEditorGUI gui;
	private String packageName;

    public PackageAction(XmlEditorGUI gui, String packageName) {
        this.gui = gui;
        this.packageName = packageName;
    }

    public void actionPerformed(ActionEvent e) {
    	gui.getAgent().activateEditorPlugin(packageName, gui.getAgent().getPrepare().getName());
    }
}
