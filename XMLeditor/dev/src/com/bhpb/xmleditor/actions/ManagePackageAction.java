/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;

import java.awt.event.*;

import javax.swing.JOptionPane;
import com.bhpb.xmleditor.swing.ErrorCode;
import com.bhpb.xmleditor.swing.TabManager;
/**
 * Title:        XmlEditor <br><br>
 * Description:  This action will load plugin with specified schema
 *               <br><br>
 * @version 1.0
 */
public class ManagePackageAction implements ActionListener {
	
	private String action;
	private TabManager tManager;
	
    public ManagePackageAction(TabManager pManager, String action) {
        this.action   = action;
        this.tManager = pManager;
    }

    public void actionPerformed(ActionEvent e) {
    	String name = tManager.getPanel().getNameEx(action);
    	if ("CHECK".equals(action)) return;
    	if ("OPEN_PACK".equals(action)) {
    		String packname = tManager.getNameEx(action);
    		tManager.getUI().getAgent().activateEditorPlugin(packname, tManager.getUI().getAgent().getPrepare().getName());
    		return;
    	}
    	
    	int err = tManager.err(action, name);
    	if ( err == 0) 
    		tManager.manageObject(action);
		else if (err == 1 || err == 2)
			JOptionPane.showMessageDialog(tManager.getPanel(), 
				"There is at least one package already associated with this name\n Please either rename package or delete old package first");
		else if (err == 3) {
			JOptionPane.showMessageDialog(tManager.getPanel(), 
			"Cannt delete builtin schema !!");
		} else if (err == ErrorCode.ERR_XML_NONE) {
			JOptionPane.showMessageDialog(tManager.getPanel(), 
			"XML file is required, please select one !");
		} else if (err == ErrorCode.ERR_PACK_NONE) {
			JOptionPane.showMessageDialog(tManager.getPanel(), 
			"Please specify an Unique Package name !");
		}
    }
}
