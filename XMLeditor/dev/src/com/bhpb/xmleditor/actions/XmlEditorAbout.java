/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006-2009 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.util.Properties;

import javax.swing.*;

import com.bhpb.xmleditor.XmlEditorGUI;

/**
 * Title:        XmlEditor <br><br>
 * Description:  This action will pop up a dialog showing basic
 *               information of the program. <br><br>
 * @version 1.0
 */

public class XmlEditorAbout extends JDialog {
    private ImageIcon icon;
    private Component parent;
    Properties props;
    public  XmlEditorAbout(Component parent, Properties props) {
        super(JOptionPane.getFrameForComponent(parent),"About xmlEditor");
        this.parent = parent;
        this.props = props;
        icon = new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public void createAndShowGUI(){
        String viewerVersion = "XmlEditor Workbench version 1.0";
        JEditorPane aboutTextEditorPane = new JEditorPane();
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");

        aboutTextEditorPane.setText(getAboutHtmlText());
        aboutTextEditorPane.setBorder(BorderFactory.createLineBorder(Color.black));

        //final JDialog dialog = new JDialog(JOptionPane.getRootFrame(), "About XmlEditor");
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);
        panel.add(new JLabel(icon), BorderLayout.CENTER);
        //dialog.getContentPane().add(new JLabel(_icon), BorderLayout.WEST);
        getContentPane().add(panel, BorderLayout.WEST);
        getContentPane().add(aboutTextEditorPane, BorderLayout.EAST);
        JButton okButton = new JButton("Close");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dispose();
            }
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(okButton);
        getContentPane().add(bpanel, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(parent);
        setVisible(true);
    }
    private String getAboutHtmlText(){

        StringBuffer sb = new StringBuffer();
        sb.append("<br><b>XmlEditor</b> - an editor for creating and editing XML files <br>");
        sb.append("based on an XML Schema, It includes Two Major Plugins: <br>");
        sb.append("Delivery and Script Plugins");
        sb.append("<br><br>");

        String versionProp = "";
        String buildProp = "";
        if(props != null){
            versionProp = props.getProperty("project.version");;
            buildProp = props.getProperty("project.build");
        }

        if(versionProp == null) {
             versionProp = "";
        }
        if(buildProp == null) {
             buildProp = "";
        }

        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");
        sb.append("Copyright (C) 2007-2009 BHP Billiton Petroleum; BHP Billiton Confidential<br>");
        sb.append("<br><br>");
        sb.append("</font>");
        return sb.toString();

    }

}
