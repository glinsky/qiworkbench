/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;

import javax.swing.*;

import java.awt.event.*;
import javax.help.CSH;

import com.bhpb.xmleditor.util.ResourceUtil;


/**
 * Title:        AbstractAction <br><br>
 * Description:  This is the base class for all the actions in the package.
 *               It provides methods that are common for all the actions.
 *               This abstrace class makes actions easy to be added to either
 *               a menu bar or a tool bar. Its subclasses need only
 *               to implement the method actionPerformed to carry on the actual
 *               task of the action. <br><br>
 *
 * @version 1.0
 */

public abstract class AbstractAction implements ActionListener {
    private final String _menuText;
    private final String _tooltip;
	private final String iconName;

    public AbstractAction(String menu, String tooltip) {
        this(menu,tooltip,"");
    }

    public AbstractAction(String menu, String tooltip, String iconName) {
        this._menuText = menu;
        this._tooltip = tooltip;
        this.iconName = iconName;
    }

    public abstract void actionPerformed(ActionEvent e);

    public final String getTooltip() { return _tooltip; }

    public final String getMenuText() { return _menuText; }

    public final String getIconName() { return iconName; }

    public JMenuItem addMenuItem(JMenu menu, ImageIcon icon) {
        JMenuItem mi = createMenuItem(icon);
        menu.add(mi);
        return mi;
    }

    public JMenuItem addMenuItem(JPopupMenu menu, ImageIcon icon) {
        JMenuItem mi = createMenuItem(icon);
        menu.add(mi);
        return mi;
    }

    public JCheckBoxMenuItem addCheckBoxMenuItem(JMenu menu, ImageIcon icon, boolean state) {
        JCheckBoxMenuItem mi = createCheckBoxMenuItem(icon, state);
        menu.add(mi);
        return mi;
    }

    public JCheckBoxMenuItem addCheckBoxMenuItem(JPopupMenu menu, ImageIcon icon, boolean state) {
        JCheckBoxMenuItem mi = createCheckBoxMenuItem(icon, state);
        menu.add(mi);
        return mi;
    }

    public JButton addToToolBar(JToolBar toolbar, ImageIcon icon) {
    	JButton btn=getButton(icon);
        toolbar.add(btn);
        return btn;
    }

    /**
	 * @param icon
	 * @return
	 */
	public JButton getButton(ImageIcon icon) {
		JButton btn = new JButton();
    	if (icon != null) {
	        btn.setIcon(icon);
    	}
        btn.setToolTipText(getTooltip());
        btn.addActionListener(this);
        CSH.setHelpIDString(btn,_menuText);
		return btn;
	}

	public JButton getButton() {
		JButton btn;
		if (iconName.length()>0) {
			btn = getButton(ResourceUtil.getInstance().getImageIcon(iconName));
		} else {
			btn = getButton(null);
		}
		return (btn);
	}

	private JMenuItem createMenuItem(ImageIcon icon) {
        JMenuItem mi = new JMenuItem(getMenuText());
        if (icon != null) {
            mi.setIcon(icon);
        }
        mi.setToolTipText(getTooltip());
        mi.addActionListener(this);
        return mi;
    }

	public JMenuItem getMenuItem (ImageIcon icon) {
		return createMenuItem(icon);
	}
	public JMenuItem getMenuItem () {
		return createMenuItem(null);
	}
	public JCheckBoxMenuItem getCheckBoxMenuItem () {
		return createCheckBoxMenuItem(null, true);
	}
	public JCheckBoxMenuItem getCheckBoxMenuItem (boolean state) {
		return createCheckBoxMenuItem(null, state);
	}
    private JCheckBoxMenuItem createCheckBoxMenuItem(ImageIcon icon, boolean state) {
        JCheckBoxMenuItem mi = new JCheckBoxMenuItem(getMenuText(), state);
        if (icon != null) {
            mi.setIcon(icon);
        }
        mi.setToolTipText(getTooltip());
        mi.addActionListener(this);
        return mi;
    }

}
