/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;
import com.bhpb.xmleditor.XmlEditorGUI;

/**
 * Title:        XmlEditor <br><br>
 * Description:  This action will pop up a dialog showing basic
 *               information of the program. <br><br>
 * @version 1.0
 */
public class XmlEditorConfig implements ActionListener {
    private ImageIcon icon;
    private XmlEditorGUI parent;
    public  XmlEditorConfig(XmlEditorGUI parent) {
    	this.parent = parent;
    	icon = new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));
    }

    /**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
    public void actionPerformed(ActionEvent e){
        String dataSet = "Data Set For This Window:";
		JEditorPane aboutTextEditorPane = new JEditorPane();
		aboutTextEditorPane.setEditable(false);
		aboutTextEditorPane.setContentType("text/html");

		StringBuffer sb = new StringBuffer();
		sb.append("<font face=\"Arial\" size=4>" + dataSet + "</font>");
		sb.append("<p><font face=\"Arial\" color=\"green\">Plugin Name: </font>");
		sb.append("<font face=\"Arial\">");
		sb.append(parent.getPluginClass());
		sb.append("</font>");
		sb.append("<br><font face=\"Arial\" color=\"green\">Config  File: </font>");
		sb.append("<font face=\"Arial\">");
		sb.append(parent.getConfigFile());
		sb.append("</font>");
		sb.append("<br><font face=\"Arial\" color=\"green\">Schema File: </font>");
		sb.append("<font face=\"Arial\">");
		sb.append(parent.getSchemaFile());
		sb.append("</font>");
		sb.append("<br>");
		aboutTextEditorPane.setText(sb.toString());

		final JDialog dialog = new JDialog(JOptionPane.getRootFrame(), "About XmlEditor Config");
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBackground(Color.WHITE);
		panel.add(new JLabel(icon), BorderLayout.CENTER);
		//dialog.getContentPane().add(new JLabel(_icon), BorderLayout.WEST);
		dialog.getContentPane().add(panel, BorderLayout.WEST);
		dialog.getContentPane().add(aboutTextEditorPane, BorderLayout.EAST);
		JButton okButton = new JButton("Close");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	dialog.dispose();
            }
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(okButton);
        dialog.getContentPane().add(bpanel, BorderLayout.SOUTH); 
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}

}
