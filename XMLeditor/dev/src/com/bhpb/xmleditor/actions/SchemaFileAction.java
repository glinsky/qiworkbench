/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.Icon;
import javax.swing.Action;
import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import com.bhpb.xmleditor.swing.BasePanel;
import com.bhpb.xmleditor.swing.TabManager;
import com.bhpb.xmleditor.swing.model.EditorFileFilter;
/**
 * Title:        XmlEditor <br><br>
 * Description:  This action will load plugin with specified schema
 *               <br><br>
 * @version 1.0
 */
public class SchemaFileAction implements ActionListener {
    private TabManager tManager;
    private String fileExt;
    private String desc;
    JDialog dialog;
    JFileChooser fc;
    JTextField dispName;
	JComboBox  pluginComboBox;
    
    public  SchemaFileAction(TabManager parent, String ext, String desc) {
    	this.tManager = parent;
    	this.fileExt  = ext;
    	this.desc     = desc;
    }

    public JFileChooser getFileChooser() {
    	return fc;
    }
    
    public JTextField getTextField() {
    	return dispName;
    }
    /**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
    public void actionPerformed(ActionEvent e){
    	fc = new JFileChooser();
    	EditorFileFilter filter = new EditorFileFilter(
    		    new String[] {fileExt.toLowerCase(), fileExt.toUpperCase()}, desc);
    	fc.addChoosableFileFilter(filter);
        fc.setFileFilter(filter);
        fc.setControlButtonsAreShown(false);
        fc.addPropertyChangeListener(createSelectionListener(this));
        //make custom controls
		JPanel custom = new JPanel();
		custom.setLayout(new BoxLayout(custom, BoxLayout.Y_AXIS));
		custom.add(Box.createRigidArea(BasePanel.VGAP10));
		custom.add(fc);
		//Action okAction = createOKAction();
		//fc.addActionListener(okAction);
		JLabel dispL1 = new JLabel("Name:");
        dispName = new JTextField("NONE", 5);
        
        JLabel dispL2 = new JLabel("Plugin:");
        pluginComboBox = new JComboBox() ;
        pluginComboBox.addItem("Script");
        pluginComboBox.addItem("Delivery");
        pluginComboBox.setSelectedIndex(0);
        
		JPanel buttons = new JPanel();
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		buttons.add(Box.createRigidArea(BasePanel.HGAP5));
		buttons.add(dispL1);
		buttons.add(dispName);
		buttons.add(Box.createRigidArea(BasePanel.HGAP10));
		//buttons.add(dispL2);
		//buttons.add(pluginComboBox);
		buttons.add(Box.createRigidArea(BasePanel.HGAP10));
		buttons.add(createButton(createSchemaAction()));
		buttons.add(Box.createRigidArea(BasePanel.HGAP10));
		buttons.add(createButton(createCancelAction()));
		buttons.add(Box.createRigidArea(BasePanel.HGAP10));
		buttons.add(createImageButton(createHelpAction()));
		buttons.add(Box.createRigidArea(BasePanel.HGAP10));
		custom.add(buttons);
		custom.add(Box.createRigidArea(BasePanel.VGAP10));
		
		// show the filechooser
		Frame parent = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, tManager.getPanel());
		dialog = new JDialog(parent, "Select Schema", true);
                dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		dialog.getContentPane().add(custom, BorderLayout.CENTER);
		dialog.pack();
		dialog.setLocationRelativeTo(tManager.getPanel());
		dialog.show();  
      
    }
    
    protected PropertyChangeListener createSelectionListener(SchemaFileAction sAction) {
    	return new ActionChangedListener(sAction);
    }
    
    private class ActionChangedListener implements PropertyChangeListener {
    	SchemaFileAction schemaAction;
    	ActionChangedListener(SchemaFileAction nameAction) {
    		super();
    		this.schemaAction = nameAction;
    	}
    	
    	public void propertyChange(PropertyChangeEvent e) {
    		File file = schemaAction.getFileChooser().getSelectedFile();
    		if (file != null) {
    			schemaAction.getTextField().setText(file.getName());
    		}
    	}
    }
    
    public Action createSchemaAction() {
    	return new AbstractAction("Ok") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
    		   File file = fc.getSelectedFile();
    		   String text;
    		   if(file == null) {
    		      text = "Please Select .xsd File First !";
    		      JOptionPane.showMessageDialog(tManager.getPanel(), text);
    		   } else {
    			  dialog.dispose();
    			  String dName = dispName.getText();
    			  if ("NONE".equals(dName) || "".equals(dName)) {
    				  dName = file.getName();
    			  }
    			  if ( tManager.err("ADD_SCHEMA", dName) == 0)
    			     //tManager.manageObject((String)pluginComboBox.getSelectedItem(), file.getAbsolutePath(), dName);
    			     tManager.manageObject("Script", file.getAbsolutePath(), dName);
    			  else 
    				  JOptionPane.showMessageDialog(tManager.getPanel(), "Scheme Name " + dName + " alreadt exists !");
    		   }
    		
    	    }
    	};
    }

     public Action createCancelAction() {
    	    return new AbstractAction("Cancel") {
			   private static final long serialVersionUID = 1L;
			   public void actionPerformed(ActionEvent e) {
    		    dialog.dispose();
    	       }
    	    };
    }
        
    public JButton createButton(Action a) {
        	JButton b = new JButton(a) {
				private static final long serialVersionUID = 1L;
				public Dimension getMaximumSize() {
        		int width = Short.MAX_VALUE;
        		int height = super.getMaximumSize().height;
        		return new Dimension(width, height);
        	    }
        	};
        	return b;
    }

    public JButton createImageButton(Action a) {
        	JButton b = new JButton(a);
        	b.setMargin(new Insets(0,0,0,0));
        	return b;
    }
        
    public Action createHelpAction() {
        	Icon icon = tManager.createImageIcon("images/help.gif", "Help");
        	return new AbstractAction("", icon) {
				private static final long serialVersionUID = 1L;
				public void actionPerformed(ActionEvent e) {
        		JOptionPane.showMessageDialog(tManager.getPanel(), "Each Schema Is Always Associated with a Plugin\n Please name an unique displayName\n If displayName is not specified\n the system will use the file name as its displayname");
        	    }
        	};
    }
            
}
