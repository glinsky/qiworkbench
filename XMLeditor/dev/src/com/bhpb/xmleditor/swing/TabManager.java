package com.bhpb.xmleditor.swing;

import java.util.List;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
//import java.util.logging.Logger;
import com.bhpb.xmleditor.XmlEditorGUI;
import com.bhpb.qiworkbench.compAPI.model.*;

/**
 * there is no array kept for the list data, rather it is generated
 * on the fly as only those elements are needed.
 *
 * @version 1.0
 */
public class TabManager implements ChangeListener {
	//private static Logger logger = Logger.getLogger(PluginManager.class.getName());
	private static final long serialVersionUID = 1L;
	
	JTabbedPane tab;
	int activePane = 0;
	
	XmlEditorGUI gui;
    BasePanel[] panels = new BasePanel[4];
    
    public TabManager(XmlEditorGUI gui) {
 	   this.gui = gui;
 	   tab = new JTabbedPane();
 	   tab.getModel().addChangeListener(this);
 	   
 	   panels[0] = new PackagePanel(gui.getResourceBundle() );
 	   panels[0].initPanel(this);
 	   
 	   panels[1] = new ViewXmlPanel(gui.getResourceBundle() );
 	   panels[1].initPanel(this);
 	   
 	   panels[2] = new DistributionPanel(gui.getResourceBundle() );
	   panels[2].initPanel(this);
 	   
 	   panels[3] = new GuidePanel(gui.getResourceBundle() );
	   panels[3].initPanel(this);
 	   
 	   tab.add("Pkg Manager",  panels[0].getBasePanel());
 	   tab.add("Pkg Defs",     panels[1].getBasePanel());
 	   tab.add("Pkg Dist",     panels[2].getBasePanel());
 	   tab.add("Pkg Guide",    panels[3].getBasePanel());
 	   this.gui.setInternalFrame(tab);
    }

    
    public void stateChanged(ChangeEvent e) {
 	   SingleSelectionModel model = (SingleSelectionModel) e.getSource();
 	   stopAll();
 	   activePane = model.getSelectedIndex();
 	   panels[activePane].doSwitch();
    }
    
   public void stopAll() {
	   panels[0].doStop();
	   panels[1].doStop();
	   panels[2].doStop();
   }
   
   public BasePanel getPanel() {
		return panels[activePane];
   }
    
   public XmlEditorGUI getUI() {
    	return gui;
   }
    
    public String getNameEx(String type) {
    	return panels[activePane].getNameEx(type);
    }
    
    public int getActivePane() {
    	 return activePane;
    }
     
    public void tableSingleSelection() {
    	 getPanel().tableRowSelected(); 
    }
     
    public void goModalDialog(String action) {
   	     getPanel().goModalDialog(action);
    }
    
    public void manageObject(String action) {
    	 getPanel().manageObject(action);
    }
    
    public void manageObject(String action, String path, String name) {
   	 getPanel().manageObject(action, path, name);
   }
     
    public void manageObject(String action, List<String> list) {
    	 getPanel().manageObject(action, list);
    }
     
    public PluginModel getPluginModel() {
    	return this.gui.getAgent().getPkgConfig();
    }
    
    public PackageLoad getPackageLoad(String packName) {
    	return this.gui.getAgent().getPkgConfig().getPackageLoad(gui.getAgent().getPrepare(),packName);
    }
    
    public ImageIcon createImageIcon(String filename, String description) {
 	   if(gui != null) {
 	       return gui.createImageIcon(filename, description);
 	   } else {
 	       String path = "/res/" + filename;
 	       return new ImageIcon(getClass().getResource(path), description); 
 	   }
    }	
    
    public int err(String type, String name) {
    	int ret = panels[activePane].err(type);
    	if (ret == 0)
    	   ret = this.gui.getAgent().getPkgConfig().err(type, name);
    	
    	return ret;
    }
}
