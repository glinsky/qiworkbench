/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.swing;
import java.awt.BorderLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.*;
/**
  * for modal dialog.
  */
public class DialogHolder {

    private JDialog dialog; 
    JInternalFrame frame; 
   
    /**
     * Constructor 
     */
   public DialogHolder() {
	  reset();
   }
    /**
      * Makes a JDialog with given parent JFrame, title, and initial value.
      * @param frame the parent JFrame for this ParamsDialog
      * @param title the String to go in the title of this dialog
      * @return the new value if "Accept" is chosen, null otherwise
      */
    public String showDialog(JInternalFrame frame, String title, JPanel pane) {
	    reset();
	    this.frame = frame;
	    dialog = new JDialog(JOptionPane.getFrameForComponent(frame), title, true);
	    dialog.getContentPane().add(pane);
	    dialog.pack();
	    dialog.setLocationRelativeTo(frame);
	    boolean setVal = showDialog();
	    if (setVal) {
	        //return value;
	    } else {
	        return null;
	    }
	    return null;
    }
    
    /**
     * Resets all fields for this class that need to be reset before
     * a new dialog can be created.
     */
    private void reset() {
	    dialog = null;
	    frame = null;
    }
    
    /**
      * Creates and returns a JPanel with border to be used as the main 
      * panel in a DEditLeafDialog.
      * @return a JPanel to be used as the main panel in a DEditLeafDialog
      */
    private JPanel setUpMainPanel() {
	    JPanel ret = new JPanel(new BorderLayout());
	    ret.setBorder(new EmptyBorder(5,5,5,5));
	    return ret;
    }
    
    /**
      * Shows the dialog and returns the value of set which in turn will 
      * determine whether or not edited values should be set in the xml tree
      * or discarded.
      * @return true if the accept action was chosen
      */
    private boolean showDialog() {
	    dialog.show();
	    return true;
    }

}
