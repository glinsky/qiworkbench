package com.bhpb.xmleditor.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.help.HelpBroker;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.bhpb.model.ui.NameValue;
import com.bhpb.xmleditor.XmlEditorGUI;
import com.bhpb.xmleditor.model.DEditDebug;
import com.bhpb.xmleditor.model.DEditXMLWindow;

public class ParameterDialog extends javax.swing.JDialog{
	private DefaultTableModel parameterTableModel;
	private javax.swing.JTable paramTable = new javax.swing.JTable();
	private JPopupMenu popupMenu;
	private Element parameterizedElement;
	
	public ParameterDialog(final XmlEditorGUI owner, String title, boolean modal, Element element){
		super(JOptionPane.getFrameForComponent(owner),title,modal);
		this.parameterizedElement = element;
		JPanel paramPanel = new JPanel(new BorderLayout());
		paramPanel.setBorder(new EmptyBorder(5,5,5,5));
		JPanel tablePanel = new JPanel(new BorderLayout());
		JLabel tableLabel = new JLabel("Parameters");
		tablePanel.add(tableLabel,BorderLayout.NORTH);
		//String[] names = new String[]{"Name","Value"};
		Vector<String> names = new Vector<String>();
		names.add("Name");
		names.add("Value");
		//Object[][] data = {{" "}};
		NamedNodeMap map = parameterizedElement.getAttributes();
		Vector list = new Vector();
		for(int i = 0; map != null && i < map.getLength(); i++){
			Node n = map.item(i);
			if(n.getNodeType() == Node.ATTRIBUTE_NODE){
				Vector v = new Vector();
				v.add(n.getNodeName());
				v.add(n.getNodeValue());
				list.add(v);
			}
		}
		
		parameterTableModel = new DefaultTableModel(list, names);
		//ParameterTableModel ptm = new ParameterTableModel(params,names,temp);
		//final JTable paramTable = new JTable(model);
		paramTable.setModel(parameterTableModel);
		//paramTable.setIntercellSpacing(new Dimension(5,5));
		final javax.swing.JDialog dialog = this;
		CellEditorListener cel = new CellEditorListener() {
			public void editingCanceled(ChangeEvent e) {
			    System.out.println("cancelled");
			}
			public void editingStopped(ChangeEvent e) {
				System.out.println("stopped");
			    //updateParameterInfo(temp,paramTable);
			} 
		};
		JMenuItem insertAboveMenuItem = new JMenuItem("Insert Above");
		JMenuItem insertBelowMenuItem = new JMenuItem("Insert Below");
		JMenuItem deleteMenuItem = new JMenuItem("Delete");
		insertAboveMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int row = paramTable.getSelectedRow();
				if(row == -1)
					return;
				DefaultTableModel model = (DefaultTableModel)paramTable.getModel();
				Object[] newRow = new Object[model.getColumnCount()];
				for(int i = 0; i < newRow.length; i++){
					newRow[i] = "";
				}
				model.insertRow(row, newRow);
			}
		});
		insertBelowMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int row = paramTable.getSelectedRow();
				if(row == -1)
					return;
				DefaultTableModel model = (DefaultTableModel)paramTable.getModel();
				Object[] newRow = new Object[model.getColumnCount()];
				for(int i = 0; i < newRow.length; i++){
					newRow[i] = "";
				}
				model.insertRow(row+1, newRow);
			}
		});
		deleteMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int row = paramTable.getSelectedRow();
				if(row == -1)
					return;
				DefaultTableModel model = (DefaultTableModel)paramTable.getModel();
				model.removeRow(row);
			}
		});
	    popupMenu = new JPopupMenu();
	    popupMenu.add(insertAboveMenuItem);
	    popupMenu.add(insertBelowMenuItem);
	    popupMenu.add(deleteMenuItem);

	    MouseListener popupListener = new PopupListener();
	    paramTable.addMouseListener(popupListener);
		
		JScrollPane jsp = new JScrollPane(paramTable);	
		jsp.setPreferredSize(new Dimension(350,300));
		tablePanel.add(jsp,BorderLayout.CENTER);
		paramPanel.add(tablePanel,BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel();
		
		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				DefaultTableModel model = (DefaultTableModel)paramTable.getModel();
				Object[] newRow = new Object[model.getColumnCount()];
				for(int i = 0; i < newRow.length; i++){
					newRow[i] = "";
				}
				model.addRow(newRow);
			}
		});
		buttonPanel.add(addButton);
		JButton setButton = new JButton("Accept");
		
		setButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			    //DEditDebug.println(this,4,"accept");
			    //updateParameterInfo(temp,paramTable);
				DefaultTableModel model = (DefaultTableModel)paramTable.getModel();
				int rowCount = model.getRowCount();
				int colCount = model.getColumnCount();
				Vector<NameValue> list = new Vector<NameValue>();
				
				for(int i = 0; i < rowCount; i++){
					NameValue nv = new NameValue();
					for(int j = 0; j < colCount; j++){
						String s = (String)model.getValueAt(i, j);
						if(s == null || s.trim().length() == 0){
							JOptionPane.showMessageDialog(owner,"Table cell must not be empty.","Invalid Data Entry",JOptionPane.WARNING_MESSAGE);
							return;
						}
						if(j == 0)
							nv.setDisplayName((String)model.getValueAt(i,j));
						else if(j == 1)
							nv.setValue((String)model.getValueAt(i,j));
					}
					list.add(nv);
				}
				//model.getDataVector();

				//TreePath path = tree.getSelectionPath();
				//DEditNode node = (DEditNode)path.getLastPathComponent();
				
				//Element el = (Element) node.getNode();
				for(int i = 0; i < list.size(); i++){
					NameValue nv = list.get(i);
					parameterizedElement.setAttribute(nv.getDisplayName().trim(), nv.getValue().trim());
				}
				
				DEditXMLWindow window = owner.getSelectedWindow();
				window.update(true);
			    dialog.dispose();
			}
		    });
		
		buttonPanel.add(setButton);
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			    //for (int i = 0; i < backup.length; i++) {
				//for (int j = 0; j < backup[i].length; j++) {
				//    paramTable.setValueAt(backup[i][j],i,j);
				//}
			    //}
			    //updateParameterInfo(temp,paramTable);
			    dialog.dispose();
			}
		    });
		buttonPanel.add(cancelButton);
		JButton clearValuesButton = new JButton("Clear Values");
		clearValuesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				DefaultTableModel model = (DefaultTableModel)paramTable.getModel();
				for(int i = 0; i < model.getRowCount(); i++)
					for(int j = 0; j < model.getColumnCount(); j++)
						model.setValueAt("", i, j);
			}
		    });
		buttonPanel.add(clearValuesButton);
		JButton helpButton = new JButton("Help");

		helpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			    HelpBroker hb = owner.getHelpBroker();
			    dialog.setModal(false);
			    if (hb != null) {
				try {
				    //hb.setCurrentID(PARAMETERS_ID);
				    hb.setDisplayed(true);
				} catch (Exception e) {
				    DEditDebug.println(this,6,"couldn't display help");
				}
			    }
			}
		    });
		buttonPanel.add(helpButton);
		paramPanel.add(buttonPanel,BorderLayout.SOUTH);
		this.getContentPane().add(paramPanel);
		this.pack();
		this.setLocationRelativeTo(owner);
		//this.show();
	}
	
	class PopupListener extends MouseAdapter {
	    public void mousePressed(MouseEvent e) {
	      showPopup(e);
	    }
	    public void mouseReleased(MouseEvent e) {
	      showPopup(e);
	    }
	    private void showPopup(MouseEvent e) {
	      if (e.isPopupTrigger()) {
	        popupMenu.show(e.getComponent(), e.getX(), e.getY());
	      }
	    }
	  }          

	}

