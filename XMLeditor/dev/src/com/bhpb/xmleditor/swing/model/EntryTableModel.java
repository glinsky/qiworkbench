package com.bhpb.xmleditor.swing.model;

import javax.swing.table.DefaultTableModel ;

public class EntryTableModel extends DefaultTableModel {
	private static final long serialVersionUID = 1L;

   
    /** Creates a new instance of EntryTableModel */
    public EntryTableModel(int index) {
    	super();
    	String[] strList = null;
    	if (index == 0) {
    		strList = ModelConsts.COLUMN_SCHEMA;
    	} else if (index == 1) {
		   strList = ModelConsts.COLUMN_FILES;
        } else {
           strList = ModelConsts.COLUMN_FILEDS;
        } 
        for (int i = 0; i < strList.length; i++) {
        	addColumn(strList[i]);
		}		
       
    }
    
    public boolean isCellEditable(int row, int col) {
    	return false;
    }
    
}
