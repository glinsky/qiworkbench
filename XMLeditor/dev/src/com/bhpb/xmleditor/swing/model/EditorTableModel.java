package com.bhpb.xmleditor.swing.model;

import javax.swing.table.AbstractTableModel;

public class EditorTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	
	String[] names = {"Plugin", "DisplayName", "Type", "Schema", "Editable", "XML File", "Help", "Action"};
	final Object[][] data = {
        	{"XmlEditor",  "XmlEditor",         "Build-in",  "NONE",                   "YES", "NONE", "ggg.hs", "Load 1"},
            {"Delivery",   "Delivery",          "Build-in",  "delivery.xsd",           "YES", "NONE", "ggg.hs", "Load 1"},
            {"Script",     "Script",            "Build-in",  "script.xsd",             "YES", "NONE", "ggg.hs", "Load 2"},
            {"Script",     "DelivMasasge",      "Build-in",  "deliveryMassager.xsd",   "YES", "NONE", "ggg.hs", "Load 1"},
            {"Script",     "WavletExtraction",  "Build-in",  "waveletExtraction.xsd",  "YES", "NONE", "ggg.hs", "Load 1"},
            {"Delivery",   "MeDelev1",  "Custom",    "dddddd.xsd",             "YES", "dddddxxx.xml", "ggg.hs", "Load 2"},
            {"Script",     "MeScript1", "Custom",    "sxsssss.xsd",            "YES", "dddddxxx.xml", "ggg.hs", "Load 2"},
            {"Delivery",   "MeDelev2",  "Custom",    "dddddd.xsd",             "YES", "dddddxxx.xml", "ggg.hs", "Load 2"},
            {"Script",     "MeScript5", "Custom",    "sxsssss.xsd",            "YES", "dddddxxx.xml", "ggg.hs", "Load 2"}
        };
	 public int getColumnCount() { return names.length; }
	 public int getRowCount() { return data.length;}
	 public Object getValueAt(int row, int col) {return data[row][col];}
	 public String getValue(int row, int col) {return (String)data[row][col];}
	 public String getColumnName(int column) {return names[column];}
	 public Class getColumnClass(int c) {return getValueAt(0, c).getClass();}
     public boolean isCellEditable(int row, int col) {return false;}
	 public void setValueAt(Object aValue, int row, int column) { data[row][column] = aValue; }
	 public TableRecord getRecord(int row) {
		 TableRecord record = new TableRecord();
		 record.setPlugin(getValue(row, 0));
		 record.setDispName(getValue(row, 1));
		 record.setType(getValue(row, 2));
		 record.setSchema(getValue(row, 3));
		 record.setXml(getValue(row, 5));
		 record.setHelp(getValue(row, 6));
		 return record;
	 }
}
