/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.swing.model;

import java.util.Vector;
import javax.swing.AbstractListModel;

public class FileListModel extends AbstractListModel {
	private static final long serialVersionUID = 1L;
    public Vector<String> names = new Vector<String>();


    public void addName(String s) {
       if(!names.contains(s)) {
	      names.addElement(s);  
	      update();
       }
    }

    public void removeName(String s) {
       names.removeElement(s);
       update();
    }


    public int getSize() {
       return names.size();
    }

    public Object getElementAt(int index) {
       return (String) names.elementAt(index);
    }
    
    public void clear() {
    	names.clear();
    }
    
    public void update() {
    	fireContentsChanged(this, 0, names.size());
    }
    
}