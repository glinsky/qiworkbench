/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.swing;

/**
 * Constants used within XmlEditor
 */
public final class ErrorCode {
    /**
     * Prevent object construction outside of this class.
     */
    private ErrorCode() {}

    public static final int ERR_PACK_DUP   = 10;
    public static final int ERR_PACK_NONE  = 11;
    public static final int ERR_XSD_DUP    = 12;
    public static final int ERR_XML_DUP    = 13;
    public static final int ERR_XML_NONE   = 14;
    public static final int ERR_HELP_DUP   = 15;
    public static final int ERR_BUILDIN    = 16;
      
}
