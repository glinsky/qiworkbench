/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.swing;

import java.util.List;
import java.util.Date;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.swing.border.BevelBorder;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.border.TitledBorder;
import com.bhpb.mail.Smtp;
import com.bhpb.mail.BhpMail;
import com.bhpb.mail.SmtpConst;
import com.bhpb.distribution.DistConst;
import com.bhpb.distribution.IDistribution;
import com.bhpb.xmleditor.actions.ModalDialogAction;
import com.bhpb.xmleditor.distribution.EditorDistribution;
import com.bhpb.xmleditor.actions.DistributionAction;
import com.bhpb.xmleditor.actions.ImportAction;

public class DistributionPanel extends BasePanel {
	private static Logger logger = Logger.getLogger(PackagePanel.class.getName());
	private static final long serialVersionUID = 1L;
	final int INITIAL_ROWHEIGHT = 20;
	static String MSG = "\n\nAttached is the XmlEditor\n" +
	                    "\n" +
	                    "Instructions\n\n"+
	                    "Package Distribution:\n" +
	                    "Attachment with .qid extension is the MetaData (package) distribution file\n" +
	                    "The file extension means QI workbench-related distribution file\n" +
	                    "Just download the file to your local drive, and go Import menu\n" +
	                    "to make the package active.\n" +
	                    "Import action will open the .qid file, create a subdirectory based file name\n" +
	                    "under .qiworkbench, and then activate the package\n" +
	                    "\n" +
	                    "\n" +
	                    
	                    "Component Distribution:\n" +
	                    "Attachement with .qic extension is the component distribution file\n" +
	                    "It is packaged similar to .qid file, but with binary (component) included\n" +
	                    "You have to restart application for the new component to take the effect\n" +
	                    "\n\n" +
	                    "Web Distribution:\n" +
	                    "We are going to make the qiWorkbench components web deployable\n" +
	                    "The same methodology will be used as email distribution, only difference is that\n " +
	                    "the user can find/install qi components from web sites automatically once he/she selected\n " +
	                    "which components they want to load";
	
	JLabel     destFile, attach;
	JTextArea  textArea;
	JButton    genDistB, genDist, uploadDist, sendTo, loadd, switchd, sendMail;
	JTextField newName, xsdDepend, sourceDir, destDir, from, to, cc, subject;
	JPanel north1, mailPane, switchPane;
	String selDist;
	String qidName = null;

	public DistributionPanel(ResourceBundle bundle) {
		super(bundle);
	}
	
	public void initPanel(TabManager manager) {
		super.initPanel(manager);
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
	
		north1 = new JPanel();
	    north1.setLayout(new BoxLayout(north1, BoxLayout.Y_AXIS));
		north1.add(createParamPanel());
		north1.add(Box.createRigidArea(VGAP10));
		mailPane   = createEmailPane();
		switchPane = createSwitchPane();
		//if (generated) north1.add(mailPane);
		northPanel.add(north1);
		northPanel.add(Box.createRigidArea(VGAP5));
		getBasePanel().add(northPanel,  BorderLayout.NORTH);
	}
	
	public String getNameEx(String action) {
		String nameEx = null;  
		if ("DIST_NAME".equals(action)) {
			 nameEx = newName.getText();
			 Date today = new Date();
			 SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
			 if (nameEx.length() == 0){
				nameEx = sdf.format(today);
				newName.setText(nameEx);   
		     }
			 
		} else if ("SEL_DIST".equals(action)) {
			nameEx = selDist;
		}
		return nameEx;
	}
	
	private JPanel createParamPanel() {
		JPanel retPanel = new JPanel();
        retPanel.setLayout(new BoxLayout(retPanel, BoxLayout.Y_AXIS));
        
		JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
        TitledBorder tb = new TitledBorder("Distribution Params:");
        tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
        tb.setTitleColor(Color.BLUE);
        controlPanel.setBorder(tb);
        
        JLabel noneL = new JLabel("");
        JPanel none = new JPanel(new GridLayout(3, 1));
        none.add(noneL);
        none.add(noneL);
        none.add(noneL);
        
        JPanel leftLabel = new JPanel(new GridLayout(3, 1));
        leftLabel.add(new JLabel("Distribution File Name:"));
        //leftLabel.add(new JLabel("XSD Dependency Directory:"));
        leftLabel.add(new JLabel("Current Config Directory:"));
        leftLabel.add(new JLabel("To Destination Directoty:"));
       
        
        IDistribution prepare= tManager.getUI().getAgent().getPrepare();
		JPanel leftParam = new JPanel(new GridLayout(3, 1));
		JPanel newNameP = new JPanel(new GridLayout(1, 3));
		newName   = new JTextField("", 12);
		destFile  = new JLabel("");
		attach    = new JLabel("");
		newNameP.add(newName);
		newNameP.add(Box.createRigidArea(HGAP15));
		newNameP.add(destFile);
	    //xsdDepend = new JTextField("", 40);
	    //xsdDepend.setText(prepare.getDirByName(PkgConst.DEPEND));
	    sourceDir = new JTextField("", 40);
	    sourceDir.setText(prepare.getDirByName(null));
	    sourceDir.setEditable(false);
	    destDir   = new JTextField("", 40);
	    destDir.setText(prepare.getDirByName(DistConst.OUTPUT));
	    destDir.setEditable(false);
	    
	    leftParam.add(newNameP);
	    //leftParam.add(xsdDepend);
	    leftParam.add(sourceDir);
	    leftParam.add(destDir);
	    
	  
	    JPanel aButtons = new JPanel();
	    genDistB = new JButton("Export with Binary");
	    genDistB.addActionListener(new DistributionAction(tManager, "GEN_DISTB"));
	    genDist = new JButton("Export");
	    //genDist.setIcon(tManager.createImageIcon("images/s_plus.gif", ""));
	    genDist.addActionListener(new DistributionAction(tManager, "GEN_DIST"));
	    uploadDist = new JButton("Push To Web");
	    uploadDist.setEnabled(false);
	    //xmlAdd.setIcon(tManager.createImageIcon("images/s_plus.gif", ""));
	    uploadDist.addActionListener(new DistributionAction(tManager, "DEPLOY_DIST"));
	    sendTo = new JButton("Email");
	    sendTo.setEnabled(false);
	    //helpAdd.setIcon(tManager.createImageIcon("images/s_plus.gif", ""));
	    sendTo.addActionListener(new DistributionAction(tManager, "SEND_MAILTO"));
	    aButtons.add(genDistB);
	    aButtons.add(genDist);
	    aButtons.add(uploadDist);
	    aButtons.add(sendTo);
	    
	    JPanel loadB = new JPanel();
	    switchd = new JButton("Switch");
	    switchd.addActionListener(new DistributionAction(tManager, "SWITCH"));
	    loadd = new JButton("Import");
	    loadd.addActionListener(new ImportAction(tManager,"Distribution"));
	    loadB.add(switchd);
	    loadB.add(loadd);
	    
	    controlPanel.add(Box.createRigidArea(HGAP5));
	    controlPanel.add(none);
		controlPanel.add(leftLabel);
		controlPanel.add(leftParam);
		controlPanel.add(Box.createRigidArea(HGAP5));
		
		retPanel.add(controlPanel);
		JPanel buttons = new JPanel();
		buttons.setLayout(new BorderLayout());
		buttons.add(loadB, BorderLayout.WEST);
		buttons.add(aButtons, BorderLayout.EAST);
		retPanel.add(buttons);
		return retPanel;
	}
	
	private JPanel createEmailPane() {
		JPanel emailPane = new JPanel();
		//emailPane.setBackground(Color.GREEN);
        emailPane.setLayout(new BoxLayout(emailPane, BoxLayout.Y_AXIS));
        TitledBorder tb = new TitledBorder("Send Email:");
        tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
        tb.setTitleColor(Color.BLUE);
        emailPane.setBorder(tb);
        JPanel paramPane = new JPanel();
        paramPane.setLayout(new BoxLayout(paramPane, BoxLayout.X_AXIS));
        
        JPanel leftLabel = new JPanel(new GridLayout(4, 1));
        leftLabel.add(new JLabel("From:"));
        JButton tos = new JButton("To ...");
        tos.addActionListener(new ModalDialogAction(tManager, SmtpConst.TOS));
        leftLabel.add(tos);
        JButton ccs = new JButton("Cc ...");
        ccs.addActionListener(new ModalDialogAction(tManager, SmtpConst.CCS));
        leftLabel.add(ccs);
        leftLabel.add(new JLabel("Subject:"));
        paramPane.add(leftLabel);
        
        JPanel leftParam    = new JPanel(new GridLayout(4, 1));
		from     = new JTextField("qiWorkbench@BHPBilliton.com", 45);
		from.setEditable(false);
		to       = new JTextField("", 45);
		cc       = new JTextField("Michael.E.Glinsky@BHPBilliton.com", 45);
		to.setEditable(false);
		cc.setEditable(false);
		subject  = new JTextField("Package Distribution Ready", 45);
		leftParam.add(from);
	    leftParam.add(to);
	    leftParam.add(cc);
	    leftParam.add(subject);
	    paramPane.add(leftParam);
	    
	    
	    JLabel label = new JLabel("");
	    JPanel noneL = new JPanel(new GridLayout(4, 1));
	    noneL.add(label);
	    noneL.add(label);
	    noneL.add(label);
	    noneL.add(label);
	    paramPane.add(noneL);
	    paramPane.add(label);
	    emailPane.add(paramPane);
	    emailPane.add(Box.createRigidArea(VGAP5));
	    emailPane.add(createSend());
	    emailPane.add(createTextPane()); 
        return emailPane;
	}
	
	private JPanel createSwitchPane() {
		
		JPanel switchPane = new JPanel();
		switchPane.setLayout(new BoxLayout(switchPane, BoxLayout.Y_AXIS));
		TitledBorder tb = new TitledBorder("Switch Distribution:");
        tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
        tb.setTitleColor(Color.BLUE);
        switchPane.setBorder(tb);
		
		JPanel distPane = new JPanel();
		distPane.setLayout(new BorderLayout());  
        distPane.add(createRadioButtons(), BorderLayout.WEST);
        
        JPanel bPane = new JPanel();
        JButton aButton = new JButton("Open Selected");
        DistributionAction action = new DistributionAction(tManager, "OPEN_DIST");
        aButton.addActionListener(action);
        bPane.add(aButton);
        
        aButton = new JButton("Set As Default");
        action = new DistributionAction(tManager, "SET_DIST");
        aButton.addActionListener(action);
        bPane.add(aButton);
        switchPane.add(distPane);
        switchPane.add(bPane);
        return switchPane;
	}
	
	public JPanel createRadioButtons() {
        JPanel distPanel = new JPanel();
        distPanel.setLayout(new BoxLayout(distPanel, BoxLayout.Y_AXIS));
        ButtonGroup group = new ButtonGroup();
	    String configDir = tManager.getUI().getAgent().getPrepare().getDirByName(DistConst.CONFIG);
        File aFile = new File(configDir);
        String[] dirs = aFile.list();
        for (int i = 0; i < dirs.length; i++) {
    	   JRadioButton radio = createRadioButton(dirs[i]);
    	   DistributionAction action = new DistributionAction(tManager, "SEL_DIST" + dirs[i]);
    	   radio.addActionListener(action);
   		   group.add(radio);
   		   distPanel.add(radio);
        } 
	    return distPanel;
    }
	
	private JPanel createSend() {
		 JPanel sendPane = new JPanel();
		 sendPane.setLayout(new BorderLayout()); 
		 JPanel msgBody = new JPanel();
		 msgBody.add(new JLabel("Message Body :"));
		 JLabel uclip = new JLabel("");
		 uclip.setIcon(tManager.createImageIcon("images/uclip.jpg", ""));
		 msgBody.add(uclip);
		 msgBody.add(attach);
		 sendPane.add(msgBody, BorderLayout.WEST);
		 sendMail = new JButton("Send");
		 DistributionAction action = new DistributionAction(tManager, "SEND");
		 sendMail.addActionListener(action);
		 sendPane.add(sendMail, BorderLayout.EAST);
 	     return sendPane;
    }
	
	private JScrollPane createTextPane() {
		textArea = new JTextArea();
		textArea.setText("XmlEditor Email Distribution with Attachmnt !!\n");
		textArea.setPreferredSize(new Dimension(600, 80));
    	JScrollPane scrollPane = new JScrollPane(textArea);  
 	    return scrollPane;
    }
	
	public int err(String action) {
		int ret = 0;
		if ("ADD_PACK".equals(action)) {
			if ("NONE".equals(newName.getText()) || "".equals(newName.getText()))
		    	ret = ErrorCode.ERR_PACK_NONE;
		}
		return ret;
	}
	
	public void goModalDialog(String action) {
		DialogHolder dialog = new DialogHolder();
		if (SmtpConst.TOS.equals(action) || SmtpConst.CCS.equals(action)) {
			DialogEmailList emailPane = new DialogEmailList(this.getBundle());
			emailPane.initPanel(tManager);
			dialog.showDialog(tManager.getUI(), "Email Management", emailPane.getBasePanel());
		}
		
	}
	public void manageObject(String action) {
		IDistribution prepare = tManager.getUI().getAgent().getPrepare();
		String zipName = getNameEx("DIST_NAME") ;
		try {
		if ("SEND_MAILTO".equals(action) ) {
			north1.remove(mailPane);
			north1.remove(switchPane);
			north1.add(mailPane);
			attach.setText(qidName);
			
		} else if ("GEN_DIST".equals(action) ) {
			if (!configExist()) {
				JOptionPane.showMessageDialog(this,"Nothing to Export !!");
				return;
			}
			north1.remove(mailPane);
			north1.remove(switchPane);
			qidName = zipName + ".qid";
			prepare.bhpZip(qidName, false);
			destFile.setText("<html><font color='blue'>" + qidName + "</html>");
			sendTo.setEnabled(true);
			sendMail.setEnabled(true);
		} else if ("GEN_DISTB".equals(action)) {
			if (!configExist()) {
				JOptionPane.showMessageDialog(this,"Nothing to Export !!");
				return;
			}
			north1.remove(mailPane);
			north1.remove(switchPane);
			qidName = zipName + ".qic";
			prepare.bhpZip(qidName, true);
			destFile.setText("<html><font color='blue'>" + qidName + "</html>");
			destFile.setIcon(tManager.createImageIcon("images/uclip.jpg", ""));	
			sendTo.setEnabled(true);
			sendMail.setEnabled(true);
		} else if ("SWITCH".equals(action)) {
			north1.remove(mailPane);
			north1.remove(switchPane);
			north1.add(switchPane);
		} else if (action.lastIndexOf("SEL_DIST") >= 0) {
			selDist = action.substring(8);
		} else if ("OPEN_DIST".equals(action)) {
			tManager.getUI().getAgent().activateEditorPlugin("", selDist);
		} else if ("SET_DIST".equals(action)) {
			tManager.getUI().getAgent().getPrepare().setProperty(EditorDistribution.LAST_DIST, selDist);
		}  else if ("SEND".equals(action)) {
			sendMail.setEnabled(false);
			tManager.getUI().getAgent().savePkgConfigFile(true);
			tManager.getUI().getAgent().saveMailConfigFile(true);
			Smtp model = tManager.getUI().getAgent().getMailConfig();
			String oDir = tManager.getUI().getAgent().getPrepare().getDirByName(DistConst.OUTPUT);
			String oname = oDir + File.separator + qidName;
			model.addAttach1(oname);
			String message = textArea.getText() + MSG;
			model.setContent(message);
			model.setSubject(subject.getText());
			BhpMail.send(model);
		}
		} catch(Exception e) {
	        e.printStackTrace();
	    }
		getBasePanel();
	}
	
	public void manageObject(String action, List<String> list) {
		if (SmtpConst.FROM.equals(action) ) {
			from.setText(list.get(0));
		} else if (SmtpConst.TOS.equals(action) ) {
			to.setText(list.get(0));
		} else if (SmtpConst.CCS.equals(action)) {
			cc.setText(list.get(0));
		} 
		getBasePanel();
	}	
	
	private boolean configExist() {
		String confFile = tManager.getUI().getAgent().getPrepare().getDefaultDir() 
		                  + File.separator + DistConst.CONFIG_FILE;
		File aFile = new File(confFile);
		return aFile.exists();
	}
}
