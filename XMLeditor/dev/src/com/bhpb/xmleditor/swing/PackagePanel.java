/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.swing;

import java.io.IOException;
import java.io.File;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.*;
import javax.swing.table.*;

import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.GridLayout;

import com.bhpb.distribution.DistConst;
import com.bhpb.distribution.IDistribution;
import com.bhpb.qiworkbench.compAPI.model.PluginModel;
import com.bhpb.qiworkbench.compAPI.model.NodeFile;
import com.bhpb.qiworkbench.compAPI.model.NodeSchema;
import com.bhpb.qiworkbench.compAPI.model.NodePackage;
import com.bhpb.qiworkbench.compAPI.model.PackageLoad;
import com.bhpb.xmleditor.distribution.EditorDistConst;
import com.bhpb.xmleditor.actions.NamedFileAction;
import com.bhpb.xmleditor.actions.SchemaFileAction;
import com.bhpb.xmleditor.actions.TableSelectionAction;
import com.bhpb.xmleditor.actions.ManagePackageAction;
import com.bhpb.xmleditor.swing.model.TableRecord;
import com.bhpb.xmleditor.swing.model.EntryTableModel;

public class PackagePanel extends BasePanel {
	private static Logger logger = Logger.getLogger(PackagePanel.class.getName());
	private static final long serialVersionUID = 1L;
	final int INITIAL_ROWHEIGHT = 20;
	
	JTable pTable;
	JTextField newName;
	
	JComboBox  schemaComboBox, xmlComboBox, helpComboBox;
	DefaultComboBoxModel modelSchema, modelXml, modelHelp;
	
	JButton schemaAdd,  xmlAdd,     helpAdd, dependAdd;
	JButton schemaDel,  xmlDel,     helpDel;
	JButton packageAdd, packageDel, packageUpd, packageOpen;
	
	JCheckBox editable;
	DefaultTableModel  pModel = null;

	public PackagePanel(ResourceBundle bundle) {
		super(bundle);
	}
	
	public void initPanel(TabManager manager) {
		super.initPanel(manager);
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
	
		JPanel north1 = new JPanel();
	    north1.setLayout(new BoxLayout(north1, BoxLayout.Y_AXIS));
	    north1.add(Box.createRigidArea(VGAP10));
		north1.add(createParamPanel());
		north1.add(Box.createRigidArea(VGAP25));
		north1.add(createPackageButtons());
		northPanel.add(north1);
		northPanel.add(Box.createRigidArea(VGAP15));
		getBasePanel().add(northPanel,  BorderLayout.NORTH);
		getBasePanel().add(createPackTable(), BorderLayout.CENTER);
	}
	
	public String[] getParams() {
		String[] strs = new String[6];
		strs[0] = newName.getText();
		String schemaName = (String)schemaComboBox.getSelectedItem();
		strs[1] = tManager.getPluginModel().getPluginName(schemaName);
		strs[2] = schemaName;
		strs[3] = (String)helpComboBox.getSelectedItem();
		strs[4] = (String)xmlComboBox.getSelectedItem();
		strs[5] = editable.isSelected() ? "YES" : "NO";
		return strs;
	}

	public String getNameEx(String action) {
		String nameEx = null;
		if ("ADD_PACK".equals(action))
		   nameEx = newName.getText();
		else if ("DEL_XSD".equals(action))
		   nameEx = (String)schemaComboBox.getSelectedItem();
		else if ("DEL_XML".equals(action))
			   nameEx = (String)xmlComboBox.getSelectedItem();
		else if ("DEL_HELP".equals(action))
			   nameEx = (String)helpComboBox.getSelectedItem();
		else if ("OPEN_PACK".equals(action)) {
			int index = pTable.getSelectionModel().getMinSelectionIndex();
			Vector record = (Vector)pModel.getDataVector().get(index);
			nameEx = (String)record.get(0);
		}
		
		return nameEx;
	}
	
	private JPanel createParamPanel() {
		JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
        
        JPanel leftLabel = new JPanel(new GridLayout(4, 1));
        leftLabel.add(new JLabel("Package Name:"));
        leftLabel.add(new JLabel("Schemas:"));
        leftLabel.add(new JLabel("xml:"));
        leftLabel.add(new JLabel("help:"));
        
		JPanel leftParam = new JPanel(new GridLayout(4, 1));
		
		newName = new JTextField("NONE", 15);
		leftParam.add(newName);
		
	    List<NodeSchema> schemas = tManager.getPluginModel().getSchemas();
	    int cnt = schemas.size();
	    String[] strSchema = new String[cnt] ;
	    for (int i = 0; i < cnt; i++) {
	    	NodeSchema node = schemas.get(i);
	    	strSchema[i] = node.getDisplayName();
	    }
	    schemaComboBox = new JComboBox() ;
	    modelSchema = new DefaultComboBoxModel(strSchema);
	    schemaComboBox.setModel(modelSchema);
	    schemaComboBox.setSelectedIndex(1);
	    leftParam.add(schemaComboBox);
	    
	    List<NodeFile> xmlFiles = tManager.getPluginModel().getXmlFiles();
	    cnt = xmlFiles.size();
	    String[] strXml = new String[cnt] ;
	    for (int i = 0; i < cnt; i++) {
	    	NodeFile node = xmlFiles.get(i);
	    	strXml[i] = node.getDisplayName();
	    }
	    xmlComboBox = new JComboBox() ;
	    modelXml = new DefaultComboBoxModel(strXml);
	    xmlComboBox.setModel(modelXml);
	    xmlComboBox.setSelectedIndex(0);
	    leftParam.add(xmlComboBox);
	    
	    List<NodeFile> helpFiles = tManager.getPluginModel().getHelpFiles();
	    cnt = helpFiles.size();
	    strXml = new String[cnt] ;
	    for (int i = 0; i < cnt; i++) {
	    	NodeFile node = helpFiles.get(i);
	    	strXml[i] = node.getDisplayName();
	    }
	    helpComboBox = new JComboBox() ;
	    modelHelp = new DefaultComboBoxModel(strXml);
	    helpComboBox.setModel(modelHelp);
	    helpComboBox.setSelectedIndex(0);
	    leftParam.add(helpComboBox);
		
	    JPanel addButtons = new JPanel(new GridLayout(4, 1));
	    addButtons.add(addCheckBox());
	    schemaAdd = new JButton("Import XSD");
	    schemaAdd.setIcon(tManager.createImageIcon("images/s_plus.gif", ""));
	    schemaAdd.addActionListener(new SchemaFileAction(tManager, "xsd", "Schema Files"));
	    xmlAdd = new JButton("Import XML");
	    xmlAdd.setIcon(tManager.createImageIcon("images/s_plus.gif", ""));
	    xmlAdd.addActionListener(new NamedFileAction(tManager, "ADD_XML", "xml", "XML Files"));
	    helpAdd = new JButton("Import Help");
	    helpAdd.setIcon(tManager.createImageIcon("images/s_plus.gif", ""));
	    helpAdd.addActionListener(new NamedFileAction(tManager, "ADD_HELP", "hs", "Help Files"));
	    addButtons.add(schemaAdd);
	    addButtons.add(xmlAdd);
	    addButtons.add(helpAdd);
	    
	    JPanel delButtons = new JPanel(new GridLayout(4, 1));
	    delButtons.add(Box.createRigidArea(VGAP5));
	    schemaDel = new JButton("Delete");
	    //schemaDel.setIcon(tManager.createImageIcon("images/s_minus.gif", ""));
	    schemaDel.addActionListener(new ManagePackageAction(tManager, "DEL_XSD"));
	    xmlDel = new JButton("Delete");
	    //xmlDel.setIcon(tManager.createImageIcon("images/s_minus.gif", ""));
	    xmlDel.addActionListener(new ManagePackageAction(tManager, "DEL_XML"));
	    helpDel = new JButton("Delete");
	    //helpDel.setIcon(tManager.createImageIcon("images/s_minus.gif", ""));
	    helpDel.addActionListener(new ManagePackageAction(tManager, "DEL_HELP"));
	    delButtons.add(schemaDel);
	    delButtons.add(xmlDel);
	    delButtons.add(helpDel);
	    controlPanel.add(Box.createRigidArea(HGAP30));
		controlPanel.add(leftLabel);
		controlPanel.add(leftParam);
		controlPanel.add(Box.createRigidArea(HGAP10));
		controlPanel.add(addButtons);
		controlPanel.add(Box.createRigidArea(HGAP5));
		controlPanel.add(delButtons);
		controlPanel.add(Box.createRigidArea(HGAP10));
		return controlPanel;
	}
	
	
	public JPanel addCheckBox() {
		JPanel pane = new JPanel();
		pane.setLayout(new BorderLayout());
		editable = new JCheckBox("editable(xml)", true);
    	editable.addActionListener(new ManagePackageAction(tManager, "CHECK"));
    	pane.add(editable, BorderLayout.WEST);
    	return pane;
	}
	public JPanel createPackTable() {
        JPanel packPanel = new JPanel();
        packPanel.setLayout(new BoxLayout(packPanel, BoxLayout.Y_AXIS));
        pTable = new JTable(getTableModel());
        pTable.setPreferredScrollableViewportSize(new Dimension(650, 150));
        pTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pTable.setRowHeight(INITIAL_ROWHEIGHT);
        
        TableSelectionAction tableAction = new TableSelectionAction(tManager, 2);
        ListSelectionModel rowSM = pTable.getSelectionModel();
        rowSM.addListSelectionListener(tableAction);  
        
        JScrollPane scrollpane = new JScrollPane(pTable);
        scrollpane.setBorder(new TitledBorder("Package List:"));
       
        packPanel.add(scrollpane);
        
        return packPanel;
	}
	
	public void tableRowSelected() {
		if (pTable.getSelectionModel().isSelectionEmpty()) {
			packageDel.setEnabled(false);
			packageUpd.setEnabled(false);
			packageOpen.setEnabled(false);
		} else {
			packageDel.setEnabled(true);
			packageUpd.setEnabled(true);
			packageOpen.setEnabled(true);
			int index = pTable.getSelectionModel().getMinSelectionIndex();
			Vector record = (Vector)pModel.getDataVector().get(index);
			newName.setText((String)record.get(0));
			schemaComboBox.setSelectedItem((String)record.get(2));
			xmlComboBox.setSelectedItem((String)record.get(3));
			helpComboBox.setSelectedItem((String)record.get(4));
			String editStr = (String)record.get(5);
			editable.setSelected("YES".equals(editStr) ? true : false);
		}
	}
	
	public int err(String action) {
		int ret = 0;
		if ("ADD_PACK".equals(action)) {
			if ("NONE".equals(newName.getText()) || "".equals(newName.getText())) {
		    	ret = ErrorCode.ERR_PACK_NONE;
		    } else if ("NONE".equals((String)xmlComboBox.getSelectedItem()) ) {
				ret = ErrorCode.ERR_XML_NONE;
		    }
		}
		return ret;
	}
	
	public void manageObject(String action) {
		int index = 0;
		PluginModel model = tManager.getPluginModel();
		List pkgList = tManager.getUI().getAgent().getNewPkgLoadList();
		if ("DEL_XSD".equals(action) ) {
			index = schemaComboBox.getSelectedIndex();
			if (index > 1) {
				String item = (String)schemaComboBox.getSelectedItem();
				tManager.getPluginModel().deleteSchema(item);
				modelSchema.removeElement(item);  
			}
		} else if ("DEL_XML".equals(action)) {
			index = xmlComboBox.getSelectedIndex();
			if (index > 0) {
			  tManager.getPluginModel().deleteXml((String)xmlComboBox.getSelectedItem());
			  modelXml.removeElementAt(index); 
			}
			
		} else if ("DEL_HELP".equals(action)) {
			index = helpComboBox.getSelectedIndex();
			if (index > 0) {
			  tManager.getPluginModel().deleteHelp((String)helpComboBox.getSelectedItem());
			  modelHelp.removeElementAt(index);
			}
			
		} else if ("ADD_PACK".equals(action)) {
			String[] strs = getParams();
			getModel().addRow(strs);
			
			NodePackage node = new NodePackage();
			node.setDisplayName(strs[0]);
			node.setSchema(strs[2]);
			node.setHelp(strs[3]);
			node.setXml(strs[4]);
			node.setEditable(editable.isSelected());
			model.addPackage(node);
			pkgList.add(node);
			tManager.getUI().getAgent().setNewPkgLoadList(pkgList);
			
		} else if ("DEL_PACK".equals(action)) {
			index = pTable.getSelectionModel().getMinSelectionIndex();
			
			Vector record = (Vector)pModel.getDataVector().get(index);
			model.deletePackage((String)record.get(0));
			getModel().removeRow(index);
			
		} else if ("UPD_PACK".equals(action)) {
			index = pTable.getSelectionModel().getMinSelectionIndex();
			Vector record = (Vector)pModel.getDataVector().get(index);
			String schemaName = (String)schemaComboBox.getSelectedItem();
			String xmlName = (String)xmlComboBox.getSelectedItem();
			String helpName = (String)helpComboBox.getSelectedItem();
			boolean edit = editable.isSelected();
			model.updatePackage((String)record.get(0), schemaName, xmlName, helpName, edit);
			getModel().removeRow(index);
			record.set(1, tManager.getPluginModel().getPluginName(schemaName));
			record.set(2, schemaName);
			record.set(3, helpName);
			record.set(4, xmlName);
			record.set(5, edit ? "YES" : "NO");
			getModel().addRow(record);
		}
		
		pTable.getSelectionModel().clearSelection();
		tableRowSelected();
	}
	
	
	public void manageObject(String action, String fullPath, String name) {
		
		if ("Script".equals(action) || "Delivery".equals(action)) {
			modelSchema.addElement(name);
			modelSchema.setSelectedItem(name);
			addSchema(name, action, fullPath);
		} else if ("ADD_XML".equals(action)) {
			modelXml.addElement(name);
			modelXml.setSelectedItem(name);
			addXml(name, fullPath);
		} else if ("ADD_HELP".equals(action)) {
			modelHelp.addElement(name);
			modelHelp.setSelectedItem(name);
			addHelp(name, fullPath);
		}
		pTable.getSelectionModel().clearSelection();
		tableRowSelected();
	}
	
	public JPanel createPackageButtons() {
		JPanel pane = new JPanel();
		pane.setLayout(new BorderLayout());
		JPanel ePanel = new JPanel();
		packageAdd    = new JButton("Add Package");
		packageAdd.setIcon(tManager.createImageIcon("images/plus.gif", ""));
		packageAdd.addActionListener(new ManagePackageAction(tManager, "ADD_PACK"));
		packageDel = new JButton("Delete");
		//packageDel.setIcon(tManager.createImageIcon("images/minus.gif", ""));
		packageDel.addActionListener(new ManagePackageAction(tManager, "DEL_PACK"));
		packageUpd = new JButton("Update");
		packageUpd.addActionListener(new ManagePackageAction(tManager, "UPD_PACK"));
		packageOpen = new JButton("Open");
		packageOpen.setIcon(tManager.createImageIcon("images/open.gif", ""));
		packageOpen.addActionListener(new ManagePackageAction(tManager, "OPEN_PACK"));
		packageAdd.setEnabled(true);
		packageDel.setEnabled(false);
		packageUpd.setEnabled(false);
		packageOpen.setEnabled(false);
    	ePanel.add(packageAdd);
    	ePanel.add(packageDel);
    	ePanel.add(packageUpd);
    	ePanel.add(Box.createRigidArea(HGAP25));
    	ePanel.add(packageOpen);
    	
    	//JPanel wPanel = new JPanel();
    	//dependAdd = new JButton("XSD Dependency");
    	//dependAdd.addActionListener(new SchemaFileAction(tManager, "xsd", "Schema Files"));
    	//wPanel.add(dependAdd);
    	//pane.add(wPanel, BorderLayout.WEST);
    	pane.add(ePanel, BorderLayout.EAST);
        return pane;
    }	
	
	private void addSchema(String displayName, String plugin, String path) {
		File sourceF = new File(path);
		String fName = sourceF.getName();
		IDistribution prepare= tManager.getUI().getAgent().getPrepare();
		String destDir = prepare.getDirByName(DistConst.SCHEMA);
		String destFile = destDir + File.separator + fName;
		try {
		   prepare.copyFile(sourceF, destFile);
		   String sDepend = sourceF.getParent() + File.separator + EditorDistConst.EDITOR_DEPEND;
		   String dDepend = null;
		   File aFile = new File(sDepend);
		   logger.info("--------------file = " + aFile.getAbsolutePath()) ;
		   if (aFile.exists()) {
			  dDepend = destDir+ File.separator + EditorDistConst.EDITOR_DEPEND;
		   }
		   prepare.copyFiles(sDepend, dDepend);
		} catch (IOException ex) {
			logger.info("Error at Copying File: ");
			ex.printStackTrace();
		}
		PluginModel model = tManager.getPluginModel();
		NodeSchema node = new NodeSchema();
		node.setDisplayName(displayName);
		node.setPlugin(plugin);
		node.setFilePath(fName);
		model.addSchema(node);
	}
	
	private void addXml(String displayName, String path) {
		File sourceF = new File(path);
		String fName = sourceF.getName();
		IDistribution prepare= tManager.getUI().getAgent().getPrepare();
		String destPath = prepare.getDirByName(DistConst.XML);
		destPath += File.separator + fName;
		try {
			prepare.copyFile(path, destPath);
	    } catch (IOException ex) {
		   logger.info("Error at Copying File: ");
		   ex.printStackTrace();
		}
		
		PluginModel model = tManager.getPluginModel();
		NodeFile node = new NodeFile();
		node.setDisplayName(displayName);
		node.setFilePath(fName);
		model.addXml(node);
	}
	
	private void addHelp(String displayName, String path) {
		File sourceF = new File(path);
		String fName = sourceF.getName();
		IDistribution prepare= tManager.getUI().getAgent().getPrepare();
		String destPath = prepare.getDirByName(DistConst.HELP);
		destPath += File.separator + fName;
		try {
			prepare.copyFile(sourceF, destPath);
	    } catch (IOException ex) {
		   logger.info("Error at Copying File: ");
		   ex.printStackTrace();
		}
	    
		PluginModel model = tManager.getPluginModel();
		NodeFile node = new NodeFile();
		node.setDisplayName(displayName);
		node.setFilePath(fName);
		model.addHelp(node);
	}
   
	private DefaultTableModel getTableModel() {
    	if (pModel == null) {
    		PluginModel model = tManager.getPluginModel();
        	List<NodePackage> sList = model.getCustomPackages();
        	pModel = new EntryTableModel(2);
        	IDistribution prepare= tManager.getUI().getAgent().getPrepare();
        	for (int i = 0; i < sList.size(); i++) {
        		  NodePackage info = sList.get(i);
        		  PackageLoad load = model.getPackageLoad(prepare, info.getDisplayName());
        	      String[] rowData = new String[6] ;
        		  rowData[0] = load.getPackageName();
        		  rowData[1] = load.getPluginName();
        		  rowData[2] = load.getSchemaName();
        		  rowData[3] = load.getHelpName();
        		  rowData[4] = load.getXmlName();
        		  rowData[5] = load.isEditable() ? "YES" : "NO";
        		  pModel.addRow(rowData);  
        	}	
        } 
        return pModel;
         
    }

	public DefaultTableModel getModel() {
		return pModel;
	}

	public void setModel(DefaultTableModel model) {
		this.pModel = model;
	}
	
	public void setValues(TableRecord record) {
		  editable.setSelected(record.isEditable());
		 
	}	
}
