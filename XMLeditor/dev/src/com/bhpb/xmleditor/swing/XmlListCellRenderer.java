/*
 * Copyright (c) 2005 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.bhpb.xmleditor.swing;

import javax.swing.*;

import java.awt.*;
import com.bhpb.qiworkbench.compAPI.model.NodeFile;
/**
 * @version 1.0
 */
class XmlListCellRenderer extends DefaultListCellRenderer {
	private static final long serialVersionUID = 1L;
	BasePanel basePanel;
	
	public XmlListCellRenderer(BasePanel basePanel) {
	       this.basePanel = basePanel;
	}
	
	public Component getListCellRendererComponent(JList list, Object value,
                      int index, boolean isSelected, boolean cellHasFocus) {
        Component retValue = 
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        int[] sels = list.getSelectedIndices();
        if (sels != null | sels.length > 0) {
        	basePanel.listSelected(sels);
        	//for (int i = 0; i < sels.length; i++)
        	//System.out.println("-------------- index = " + sels[i]);
        }
        /*	
        NodeFile fNode = getFileNode(index);
        if ("NONE".equals(fNode.getDisplayName())) {
        	setIcon(pManager.getImageIcon(0));
        } else if (1 == fNode.getStatus()){
        	setIcon(pManager.getImageIcon(3));
        } else if (0 == fNode.getStatus()) {
        	setIcon(pManager.getImageIcon(1));
        } else {
        	setIcon(pManager.getImageIcon(0));
        }
        if (isSelected) {
        	setIcon(pManager.getImageIcon(2));
        	list.setSelectedIndex(index);
        	
        	pManager.setSelection(listNum, index);
        	pManager.setListType();
        	
        }*/
        
        return retValue;
    }

}
