/*
 * Copyright (c) 2005 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.bhpb.xmleditor.swing;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
/**
 * A generic module based on swing example
 * @version 1.0
 */
public class BasePanel extends JApplet {
    static final long serialVersionUID = 1l;
    protected TabManager      tManager  = null;
    public static Insets      insets0   = new Insets(0,0,0,0);
    public static Insets      insets10  = new Insets(10,10,10,10);
    public static EmptyBorder border5   = new EmptyBorder(5,5,5,5);
    public static EmptyBorder border10  = new EmptyBorder(10,10,10,10);
    Border loweredBorder 
    = new CompoundBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED), border5);

    // Premade convenience dimensions, for use wherever you need 'em.
    public static Dimension HGAP2 = new Dimension(2,1);
    public static Dimension VGAP2 = new Dimension(1,2);

    public static Dimension HGAP5 = new Dimension(5,1);
    public static Dimension VGAP5 = new Dimension(1,5);
    
    public static Dimension HGAP10 = new Dimension(10,1);
    public static Dimension VGAP10 = new Dimension(1,10);

    public static Dimension HGAP15 = new Dimension(15,1);
    public static Dimension VGAP15 = new Dimension(1,15);
    
    public static Dimension HGAP20 = new Dimension(20,1);
    public static Dimension VGAP20 = new Dimension(1,20);

    public static Dimension HGAP25 = new Dimension(25,1);
    public static Dimension VGAP25 = new Dimension(1,25);

    public static Dimension HGAP30 = new Dimension(30,1);
    public static Dimension VGAP30 = new Dimension(1,30);
    
    private JPanel panel = null;
    // Resource bundle for internationalized and accessible text
    private ResourceBundle bundle = null;

    public BasePanel(ResourceBundle bundle) {
        UIManager.put("swing.boldMetal", Boolean.FALSE);
	    panel = new JPanel();
	    panel.setLayout(new BorderLayout());  
	    this.bundle = bundle;
    }

    public JPanel getBasePanel() {
    	panel.setSize(650, 550);
	    return panel;
    }

    public int  err(String type) {return 0;}
    public String getNameEx(String action) {return "";}
    public void doStop() {}
    public void doSwitch() {}
    public void listSelected(int[] sels) {}
    public void tableRowSelected() {}
    public void tableRowsSelected() {}
    public void goModalDialog(String action) {}
    public void manageObject(String action) {}
    public void manageObject(String action, List<String> list) {}
    public void manageObject(String action, String path, String name) {}
    
    public void initPanel(TabManager manager) {
    	this.tManager = manager;
    }
   
    
    public String getString(String key) {
	   String value = "nada";
	   if(bundle == null) return value;
	   try {
	       value = bundle.getString(key);
	   } catch (MissingResourceException e) {
	       System.out.println("MissingResourceException: Couldn't find value for: " + key);
	   }
	   return value;
    }

    public char getMnemonic(String key) {
	   return (getString(key)).charAt(0);
    }

    public JPanel createHorizontalPanel(boolean threeD) {
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
        p.setAlignmentY(TOP_ALIGNMENT);
        p.setAlignmentX(LEFT_ALIGNMENT);
        if(threeD) {
            p.setBorder(loweredBorder);
        }
        return p;
    }
    
    public JPanel createVerticalPanel(boolean threeD) {
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        p.setAlignmentY(TOP_ALIGNMENT);
        p.setAlignmentX(LEFT_ALIGNMENT);
        if(threeD) {
            p.setBorder(loweredBorder);
        }
        return p;
    }

    public JRadioButton createRadioButton(String name) {
    	JRadioButton 
    	radio = new JRadioButton(name, tManager.createImageIcon("images/rb.gif", ""));
		radio.setPressedIcon(tManager.createImageIcon("images/rbp.gif", ""));
		radio.setRolloverIcon(tManager.createImageIcon("images/rbr.gif", ""));
		radio.setRolloverSelectedIcon(tManager.createImageIcon("images/rbrs.gif", ""));
		radio.setSelectedIcon(tManager.createImageIcon("images/rbs.gif", ""));
		radio.setMargin(new Insets(0,0,0,0));
		return radio;
    }
    
    protected String getFileName(String fullPath) {
		int index = fullPath.lastIndexOf(File.separator);
		return fullPath.substring(index + 1);
	}
    
    public void init() {
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(getBasePanel(), BorderLayout.CENTER);
    }
    
    void updateDragEnabled(boolean dragEnabled) {}

	public ResourceBundle getBundle() {
		return bundle;
	}

	public void setBundle(ResourceBundle bundle) {
		this.bundle = bundle;
	}
    
}

