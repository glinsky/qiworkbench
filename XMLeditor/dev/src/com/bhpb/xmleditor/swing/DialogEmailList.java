/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.swing;

import java.util.List;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import com.bhpb.xmleditor.actions.ModalWindowAction;
import com.bhpb.xmleditor.swing.model.FileListModel;

import java.awt.Dimension;
import java.awt.BorderLayout;
import com.bhpb.mail.Smtp;
import com.bhpb.mail.SmtpConst;

public class DialogEmailList extends BasePanel {
	private static Logger logger = Logger.getLogger(PackagePanel.class.getName());
	private static final long serialVersionUID = 1L;
	final int INITIAL_ROWHEIGHT = 20;

	JLabel sName; 
	JTextField fromF, inputF;
	JList jList;
	int[] selEmails;
	Smtp smtp;
	FileListModel listModel;
	
	
	public DialogEmailList(ResourceBundle bundle) {
		super(bundle);
	}
	
	public void initPanel(TabManager manager) {
		super.initPanel(manager);
		Smtp smtp = tManager.getUI().getAgent().getMailConfig();
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
		
		JPanel servLabel = new JPanel();
		servLabel.setLayout(new BoxLayout(servLabel, BoxLayout.X_AXIS));
		JLabel server = new JLabel("Email Server:  ");
		sName = new JLabel(smtp.getMailHost());
		servLabel.add(server);
		servLabel.add(sName);
		
		JPanel from = new JPanel();
		from.setLayout(new BoxLayout(from, BoxLayout.X_AXIS));
		JButton frm = new JButton("FRM");
		frm.addActionListener(new ModalWindowAction(this, SmtpConst.FROM));
		from.add(frm);
		from.add(Box.createRigidArea(HGAP5));
		fromF = new JTextField(smtp.getFrom(), 18);
		from.add(fromF);
		
		JPanel emalList = new JPanel();
		emalList.setLayout(new BoxLayout(emalList, BoxLayout.X_AXIS));
		emalList.add(createToCc());
		emalList.add(createEmailsPanel());
		
		//north1.add(createPackageButtons());
		northPanel.add(servLabel);
		northPanel.add(from);
		northPanel.add(Box.createRigidArea(VGAP5));
		northPanel.add(emalList);
		northPanel.add(textField());
		northPanel.add(Box.createRigidArea(VGAP5));
		northPanel.add(controlButtons());
		getBasePanel().add(northPanel,  BorderLayout.NORTH);
		//getBasePanel().add(createPackTable(), BorderLayout.CENTER);
	}
	
	public JPanel createEmailsPanel() {
		JPanel selPanel = new JPanel();
		selPanel.add(createList());
		return selPanel;
    }
	
	public JPanel createToCc() {
		JPanel bPanel = new JPanel();
		bPanel.setLayout(new BorderLayout());
		
		JPanel buttons = new JPanel();
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));
		JButton tos = new JButton("TOs");
		tos.addActionListener(new ModalWindowAction(this, SmtpConst.TOS));
		buttons.add(tos);
		buttons.add(Box.createRigidArea(HGAP15));
		
		JButton ccs = new JButton("CCs");
		ccs.addActionListener(new ModalWindowAction(this, SmtpConst.CCS));
		buttons.add(ccs);
		bPanel.add(buttons, BorderLayout.NORTH);
		return  bPanel;
    }
	
	public JPanel textField() {
		JPanel bPanel = new JPanel();
		bPanel.setLayout(new BorderLayout());
		inputF = new JTextField("", 20);
		bPanel.add(inputF, BorderLayout.EAST);
		return  bPanel;
    }
	
	public JPanel controlButtons() {
		JPanel bPanel = new JPanel();
		bPanel.setLayout(new BorderLayout());
		
		JPanel buttons = new JPanel();
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		JButton server = new JButton("Reset Server");
		server.addActionListener(new ModalWindowAction(this, SmtpConst.MAIL_SERVER));
		buttons.add(server);
		
		buttons.add(Box.createRigidArea(HGAP15));
		JButton addemail = new JButton("Add Email");
		addemail.addActionListener(new ModalWindowAction(this, SmtpConst.MAIL_ADD));
		buttons.add(addemail);
		
		buttons.add(Box.createRigidArea(HGAP5));
		JButton delemail = new JButton("Del Email");
		delemail.addActionListener(new ModalWindowAction(this, SmtpConst.MAIL_DEL));
		buttons.add(delemail);
		bPanel.add(buttons, BorderLayout.EAST);
		return  bPanel;
    }
	
	private JScrollPane createList() {
        JScrollPane scrollPane = null;  
        jList = new JList();
        jList.setCellRenderer(new XmlListCellRenderer(this));
        jList.setModel(getListModel());
        jList.setVisibleRowCount(6);
  	    scrollPane = new JScrollPane(jList);  
  	    scrollPane.setPreferredSize(new Dimension(260, 125));  
        return scrollPane;
	}
	
	public void listSelected(int[] selEmails) {
		this.selEmails = selEmails;
	}
	
	public void manageObject(String action) {
		Object[] aList = null;
		String temp = null;
		ArrayList<String> list = new ArrayList<String>();
		if (SmtpConst.FROM.equals(action) ) {
			int sel = selEmails[0];
			aList = jList.getSelectedValues();
			temp = null;
			if (aList != null) {
				temp = (String)aList[0];
				list.add(temp);
			}
			fromF.setText(temp);
			tManager.getPanel().manageObject(action, list);
			
		} else if (SmtpConst.TOS.equals(action) || SmtpConst.CCS.equals(action) ) {
			aList = jList.getSelectedValues();
			StringBuffer aBuffer = new StringBuffer();
			for (int i = 0; i < aList.length; i++) {
				temp = (String)aList[i];
				smtp.addEmail(action, temp);
				aBuffer.append(temp);
				aBuffer.append(";");
			}
			list.add(aBuffer.toString());
			tManager.getPanel().manageObject(action, list);
		
		} else if (SmtpConst.MAIL_ADD.equals(action)) {
			temp = inputF.getText();
			if (temp.length() > 0 && temp.indexOf("@") > 0)
			   smtp.addEmail(SmtpConst.MAIL_LIST, temp);
			   listModel.addName(temp);
				
		} else if (SmtpConst.MAIL_DEL.equals(action)) {
			aList = jList.getSelectedValues();
			if (aList != null && aList.length > 0) {
				temp = (String)aList[0];
				smtp.delEmail(temp);
				listModel.removeName(temp);
			}
		} else if (SmtpConst.MAIL_SERVER.equals(action)) {
			temp = inputF.getText();
			if (temp.length() == 0) return;
			temp = temp.toLowerCase();
			if (temp.indexOf("mtp.") > 0)
			   smtp.setMailHost(temp);
			   sName.setText(temp);
		} 
	}
	
	public String[] getParams() {
		return null;
	}

	public String getNameEx(String action) {
		String nameEx = null;
		return nameEx;
	}
	
	
	public int err(String action) {
		int ret = 0;
		return ret;
	}
	
	private FileListModel getListModel() {     
		listModel = new FileListModel(); 
		smtp = tManager.getUI().getAgent().getMailConfig();
		List<String> aList = smtp.getEmails();
		for (int i = 0; i < aList.size(); i++) {
		   listModel.addName(aList.get(i));
	    }
        return listModel;
	}
	
}
