/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.util;

import java.lang.reflect.Method;
import java.util.Vector;
import javax.swing.JApplet;
import com.bhpb.xmleditor.XmlEditorGUI;
import com.bhpb.xmleditor.model.DEditDebug;
import com.bhpb.xmleditor.model.DEditPlugin;

public class MakeEditor {
	//test class
	public static XmlEditorGUI makeDEditor(String[] args, DEditPlugin plugin, JApplet applet) {
        String configFile = "configFiles/simpleconfig.xml";
        String schemaFilename = null;
        String xmlFilename = null;
        boolean allowLoadSchema = true;
        boolean allowOpenFile = true;
        boolean allowNewFile = true;
        boolean relaxed = false;
        String pluginName = null;
        Vector extraArgs = new Vector();
        for (int i = 0; i < args.length; i++) {
           if (args[i].startsWith("xsd=")) {
              schemaFilename = args[i].substring(4);
           } else if (args[i].startsWith("xml=")) {
              xmlFilename = args[i].substring(4);
           } else if (args[i].startsWith("cfg=")) {
              configFile = args[i].substring(4);
           } else if (args[i].startsWith("plugin=")) {
              pluginName = args[i].substring(7);
           } else if (args[i].equals("-S")) {
              allowLoadSchema = false;
           } else if (args[i].equals("-N")) {
              allowNewFile = false;
           } else if (args[i].equals("-X")) {
              allowOpenFile = false;
           } else if (args[i].equals("-V")) {
              relaxed = true;
           } else if (args[i].equals("-pl")) {
              int priority = 0;
              i++;
              if (i < args.length) {
                 priority = Integer.parseInt(args[i]);
              }
              DEditDebug.setPriority(priority);
           } else {
              extraArgs.add(args[i]);
           }
        }
        //String dirString = System.getProperty("user.dir");
        String dirString = "";
        if (pluginName != null) {
           try {
               String[] leftOvers = new String[extraArgs.size()];
               extraArgs.toArray(leftOvers);
               extraArgs.removeAllElements();
               Class c = Class.forName(pluginName);
               if (c != null) {
                  String[] temp = new String[0];
                  Method m=c.getDeclaredMethod("buildPlugin", new Class[]{temp.getClass()});
                  if (m != null) {
	                 plugin =(DEditPlugin)m.invoke(null, new Object[]{leftOvers});
                  }
                }
             } catch (Exception e) {
                System.err.println("load exception error: " + e.toString());
             }
        }
       // XmlEditorGUI dedit = new XmlEditorGUI(null, "", "", 
    	//	                  dirString, configFile, 
    	//	                  xmlFilename, schemaFilename, allowNewFile,
		 //                     allowOpenFile, allowLoadSchema, relaxed, plugin);
        return null;
      }   

}
