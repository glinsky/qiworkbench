/*
 * @(#)ImageCache.java	is based on java JDK examples
 * 
 * Copyright (c) 2004 Sun Microsystems, Inc. All Rights Reserved.
 * 
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.util;

import java.awt.*;
import java.util.Hashtable;
import java.net.URL;
import java.net.URLClassLoader;
/**
 * A cache of all images found in the images directory.
 */
public class ImageCache extends Component {
	private static final long serialVersionUID = 1L;

	private String[] names = { 
        "bhp-logo.gif", "well.gif"
    };
    
    private static Hashtable<String, Image> cache;


    public ImageCache() {
        cache = new Hashtable<String, Image>(names.length);
        for (int i = 0; i < names.length; i++) {
            cache.put(names[i], getImage(names[i], this));
        }
    }


    public static Image getImage(String name, Component cmp) {
        Image img = null;
        
        if (cache != null) {
            if ((img = cache.get(name)) != null) {
                return img;
            }
        }

	    URLClassLoader urlLoader = (URLClassLoader)cmp.getClass().getClassLoader();
	    URL fileLoc = urlLoader.findResource("res/images/" + name);
	    img = cmp.getToolkit().createImage(fileLoc);

        MediaTracker tracker = new MediaTracker(cmp);
        tracker.addImage(img, 0);
        
        try {
            tracker.waitForID(0);
            if (tracker.isErrorAny()) {
                System.out.println("Error loading image " + name);
            }
        } catch (Exception ex) { ex.printStackTrace(); }
        return img;
    }
}
