/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.xmleditor.util;

//import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Title:        ResourceUtil <br><br>
 * Description:  Class to load image files to create icon. <br><br>
 * Copyright:    Copyright (c) 2006 <br>
 * Company:      BHPB <br>
 * @version 1.0
 */

public class ResourceUtil {
    private static ResourceUtil _instance = null;

    protected ResourceUtil() {
    }

    public static ResourceUtil getInstance() {
        if (_instance == null) _instance = new ResourceUtil();
        return _instance;
    }

    public ImageIcon getImageIcon(String imageFile) {
        ImageIcon result = null;
        try {
            result = new ImageIcon(this.getClass().getResource(imageFile));
        }
        catch (Exception ex) {
            return null;
        }
        return result;
    }
}
