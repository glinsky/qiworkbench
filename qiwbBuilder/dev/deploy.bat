REM deploys dist to %CATALINA_HOME% (must be set)
%CATALINA_HOME}%\bin\shutdown

rmdir /s /q %CATALINA_HOME%\webapps\qiWorkbench
del %CATALINA_HOME%\webapps\qiWorkbench.war
del /q %CATALINA_HOME%\webapps\qiComponents\*
copy dist\dev\qiWorkbench.war %CATALINA_HOME%\webapps\
copy dist\dev\qiComponents\* %CATALINA_HOME%\webapps\qiComponents\

%CATALINA_HOME%\bin\startup
