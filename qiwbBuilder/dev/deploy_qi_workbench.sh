# deploy_qi_workbench.sh - last modified by Woody Folsom 2/19/2008
# Deploys qiWorkbench from ./dist to $CATALINA_HOME/webapps.
#
# Requires CATALINA_HOME to be set and $CATALINA_HOME/webapps to exist.
#
# Optionally, accepts a single command line argument that is the name of the branch or tag to deploy.
# If no arguments are given, defaults to 'dev'.
# If more than 1 argument is given, the second and subsequent arguments are ignored.
#
# Returns one of the following exit codes
# 0 normal termination
# 1 CATALINA_HOME not set, execution terminated
# 2 CATALINA_HOME/webapps does not exist, execution terminated
# 3 dist/$1 does not exist (or dist/dev if no arg $1 was given), execution terminated

[ `whoami` == "root" ] || {
  echo "this script must be run as root to copy into the tomcat webapps directory";
  exit 1;
}

if [ -z ${CATALINA_HOME} ]
then
echo "Cannot deploy - environment variable CATALINA_HOME is not set."
exit 1
fi

if [ ! -d ${CATALINA_HOME}/webapps ]
then
echo "Cannot deploy - ${CATALINA_HOME}/webapps is not a valid directory."
exit 2
fi

if [ -z $1 ]
then
tagname=dev
else
tagname=$1
fi

echo "Deploying distrubtion: ${tagname}..."

if [ ! -d dist/${tagname} ]
then
echo "Cannot deploy - dist/${tagname} does not exist."
exit 3
fi

[ -n $CATALINA_HOME ] || {
  echo "CATALINA_HOME not set. set to tomcat root.";
  exit 1;
}

#${CATALINA_HOME}/bin/shutdown.sh
systemctl stop tomcat.service

echo "Sleeping 3 seconds while Tomcat completes shutdown..."
sleep 3

webappdir=${CATALINA_HOME}/webapps/qiWorkbench
warfile=${CATALINA_HOME}/webapps/qiWorkbench.war
compcache=${CATALINA_HOME}/webapps/qiComponents
installdir=dist/${tagname}

echo "Deployment parameters:"
echo "Installation source directory: ${installdir}"
echo "Web App directory: ${webappdir}"
echo "War file: ${warfile}"
echo "Component cache: ${compcache}"

if [ ! -d ${webappdir} ]
then
echo "Cannot delete web app directory: ${webappdir}, it does not exist."
else
rm -rf ${webappdir}
fi

if [ ! -f ${warfile} ]
then
echo "Cannot delete war file: ${warfile}, it does not exist."
else
rm -f ${warfile}
fi

if [ ! -d ${compcache} ]
then
echo "Cannot clear component cache: ${compcache}, it does not exist."
else
rm -f ${compcache}/*
fi

echo "Creating directory ${compcache}"
[ -d ${compcache} ] || mkdir ${compcache}

cp ${installdir}/qiWorkbench.war ${CATALINA_HOME}/webapps/
cp ${installdir}/qiComponents/* ${compcache}

#${CATALINA_HOME}/bin/startup.sh
systemctl start tomcat.service

exit 0

