#!/bin/bash

echo "checking for XmlEditor to XMLeditor link from `pwd`  JE FIELD  3/17/2022"
cd ../../..
[ -d XmlEditor ] || ln -s XMLeditor XmlEditor || {
  echo "problem creating XmlEditor link to XMLeditor in `pwd`" ;
  exit 1 ;
}


