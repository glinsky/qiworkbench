package com.bhpb.qiDataConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HeaderConversionHelper {
	Map<String,String> headerPairMap; //keyed by targer header name b/c target header must be unique
	List<String> fromHeaders;
	List<String> toHeaders;
	public HeaderConversionHelper(List<String> from, List<String> to){
		if(from == null || to == null || from.size() != to.size())
			throw new IllegalArgumentException("Header list can not be null and source header list should have the same size as the target header list.");
		fromHeaders = from;
		toHeaders = to;
		headerPairMap = new HashMap<String,String>();
		for(int i = 0; i < from.size(); i++){
			headerPairMap.put(to.get(i),from.get(i));
		}
	}
	/*
	 * Get a list of target headers which can be converted directly from source to target without channeling
	 * through staging header storage
	 */
	public List<String>getNonConflictingTargetHeaders(){
		List<String> list = new ArrayList<String>();
		for(int i = 0; i < toHeaders.size(); i++){
			if(!fromHeaders.contains(toHeaders.get(i)) && !fromHeaders.get(i).equals(toHeaders.get(i))){
				list.add(toHeaders.get(i));
			}
		}
		return list;
	}
	
	/*
	 * Get a list of target headers which can not be converted directly from source to target and require
	 * the help of staging header storage currently empty.
	 */
	public List<String>getConflictingTargetHeaders(){
		List<String> list = new ArrayList<String>();
		for(int i = 0; i < toHeaders.size(); i++){
			if(fromHeaders.contains(toHeaders.get(i)) && !fromHeaders.get(i).equals(toHeaders.get(i))){
				list.add(toHeaders.get(i));
			}
		}
		return list;
	}
	
	public Map<String,String> getHeaderPairMap(){
		return headerPairMap;
	}
	
	
}
