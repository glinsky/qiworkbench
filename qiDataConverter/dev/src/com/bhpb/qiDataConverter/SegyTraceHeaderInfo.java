package com.bhpb.qiDataConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SegyTraceHeaderInfo{
	private String name = "";
	private String type = "";
	private int loc;
	protected  static final String [][] SEGY_H_INT_HEADERS_BYTE = {{"tracl","int","1"},
		{"tracr","int","5"},
		{"fldr","int","9"},
		{"tracf","int","13"},
		{"ep","int","17"},
		{"cdp","int","21"},
		{"cdpt","int","25"},
		{"trid","short","29"},
		{"nvs","short","31"},
		{"nhs","short","33"},
		{"duse","short","35"},
		{"offset","int","37"},
		{"gelev","int","41"},
		{"selev","int","45"},
		{"sdepth","int","49"},
		{"gdel","int","53"},
		{"sdel","int","57"},
		{"swdep","int","61"},
		{"gwdep","int","65"},
		{"scalel","short","69"},
		{"scalco","short","71"},
		{"sx","int","73"},
		{"sy","int","77"},
		{"gx","int","81"},
		{"gy","int","85"},
		{"counit","short","89"},
		{"wevel","short","91"},
		{"swevel","short","93"},
		{"sut","short","95"},
		{"gut","short","97"},
		{"sstat","short","99"},
		{"gstat","short","101"},
		{"tstat","short","103"},
		{"laga","short","105"},
		{"lagb","short","107"},
		{"delrt","short","109"},
		{"muts","short","111"},
		{"mute","short","113"},
		{"ns","unsigned short","115"},
		{"dt","unsigned short","117"},
		{"gain","short","119"},
		{"igc","short","121"},
		{"igi","short","123"},
		{"corr","short","125"},
		{"sfs","short","127"},
		{"sfe","short","129"},
		{"slen","short","131"},
		{"styp","short","133"},
		{"stas","short","135"},
		{"stae","short","137"},
		{"tatyp","short","139"},
		{"afilf","short","141"},
		{"afils","short","143"},
		{"nofilf","short","145"},
		{"nofils","short","147"},
		{"lcf","short","149"},
		{"hcf","short","151"},
		{"lcs","short","153"},
		{"hcs","short","155"},
		{"year","short","157"},
		{"day","short","159"},
		{"hour","short","161"},
		{"minute","short","163"},
		{"sec","short","165"},
		{"timbas","short","167"},
		{"trwf","short","169"},
		{"grnors","short","171"},
		{"grnofr","short","173"},
		{"grnlof","short","175"},
		{"gaps","short","177"},
		{"otrav","short","179"},
		{"mark","short","1"},
		{"mutb","short","1"},
		{"n2","short","1"},
		{"shortpad","short","1"},
		{"ntr","int","1"}
	};
	
	private static Map<String,SegyTraceHeaderInfo> map = new HashMap<String,SegyTraceHeaderInfo>();
	static{
    	try{
    	 //map = new HashMap<String,SegyTraceHeaderInfo>();
		    for(String [] ss : SEGY_H_INT_HEADERS_BYTE){
		    	SegyTraceHeaderInfo sh = new SegyTraceHeaderInfo(ss[0],ss[1],Integer.valueOf(ss[2]).intValue());
		    	map.put(ss[0],sh);
		    }
	    //segyHeaderInfoMap = map;
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
	public static final Map<String,SegyTraceHeaderInfo> segyHeaderInfoMap = map;
    /*
     * Get a list of header which are considered empty; those header filtered out by surange
     * @param nonEmptyHeaders   a list of header with integer valid values showing result of surange
     * @return      a list of empty headers
     */
    public static List<SegyTraceHeaderInfo> getEmptyIntegerHeaders(List<String> nonEmptyHeaders){
    	List<SegyTraceHeaderInfo> list = new ArrayList<SegyTraceHeaderInfo>();
    	for(String key : segyHeaderInfoMap.keySet()){
    		if(!nonEmptyHeaders.contains(key))
    			list.add(segyHeaderInfoMap.get(key));
    	}
    	return list;
    }
    
	public SegyTraceHeaderInfo(){}
	public SegyTraceHeaderInfo(String name, String type, int loc){
		this.name = name;
		this.type = type;
		this.loc = loc;
	}
	public void setLoc(int loc){
		this.loc = loc;
	}
	public int getLoc(){
		return loc;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setType(String type){
		this.type = type;
	}
	public String getType(){
		return type;
	}
	public String getAlternativeType(){
		if(type.equals("int"))
			return "l";
		else if(type.equals("short") || type.equals("unsigned short"))
			return "s";
		else if(type.equals("byte"))
    		return "b";
		else if(type.equals("float"))
			return "f";
		else
			return "";
	}
	public String toString(){
		return "name=" + name + ", type=" + type + ", byte starting location=" + loc;
	}
	

}
