/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiDataConverter;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.PlainDocument;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;


public class EditTextHeaderDialog extends javax.swing.JDialog {
    /** A return status code - returned if Cancel button has been pressed */
    public static final int RET_CANCEL = 0;
    /** A return status code - returned if OK button has been pressed */
    public static final int RET_OK = 1;    
    private QiDataConverterPlugin messagingAgent; 
    private String samplText = "C 1 CLIENT                        COMPANY                       CREW NO         C 2 LINE            AREA                        MAP ID                          C 3 REEL NO           DAY-START OF REEL     YEAR      OBSERVER                  C 4 INSTRUMENT: MFG            MODEL            SERIAL NO                       C 5 DATA TRACES/RECORD        AUXILIARY TRACES/RECORD         CDP FOLD          C 6 SAMPLE INTERNAL         SAMPLES/TRACE       BITS/IN      BYTES/SAMPLE       C 7 RECORDING FORMAT        FORMAT THIS REEL        MEASUREMENT SYSTEM          C 8 SAMPLE CODE: FLOATING PT     FIXED PT     FIXED PT-GAIN     CORRELATED      C 9 GAIN  TYPE: FIXED     BINARY     FLOATING POINT     OTHER                   C10 FILTERS: ALIAS     HZ  NOTCH     HZ  BAND    -     HZ  SLOPE    -    DB/OCT C11 SOURCE: TYPE            NUMBER/POINT        POINT INTERVAL                  C12     PATTERN:                           LENGTH        WIDTH                  C13 SWEEP: START     HZ  END     HZ  LENGTH      MS  CHANNEL NO     TYPE        C14 TAPER: START LENGTH       MS  END LENGTH       MS  TYPE                     C15 SPREAD: OFFSET        MAX DISTANCE        GROUP INTERVAL                    C16 GEOPHONES: PER GROUP     SPACING     FREQUENCY     MFG          MODEL       C17     PATTERN:                           LENGTH        WIDTH                  C18 TRACES SORTED BY: RECORD     CDP     OTHER                                  C19 AMPLITUDE RECOVEY: NONE      SPHERICAL DIV       AGC    OTHER               C20 MAP PROJECTION                      ZONE ID       COORDINATE UNITS          C21 PROCESSING:                                                                 C22 PROCESSING:                                                                 C23                                                                             C24                                                                             C25                                                                             C26                                                                             C27                                                                             C28                                                                             C29                                                                             C30                                                                             C31                                                                             C32                                                                             C33                                                                             C34                                                                             C35                                                                             C36                                                                             C37                                                                             C38                                                                             C39 SEG Y REV1                                                                  C40 END TEXTUAL HEADER                                                          ";
    private boolean headerEdited = false;
    public EditTextHeaderDialog(java.awt.Frame parent, boolean modal,QiDataConverterPlugin messagingAgent) {
    	super(parent, modal);
    	this.messagingAgent = messagingAgent;
    	setTitle("Edit EBCIDC Text Header");
        initComponents();
        ebcdicTextHeaderTextArea.setDocument(new MyDocument());
        StringBuffer sampleText1 = new StringBuffer();
        int row = samplText.length()/80;
        for(int i = 0; i < row; i++){
        	int ind = i*80;
        	sampleText1.append(samplText.substring(ind,ind+80).trim());
        	if(i < (row-1))
        		sampleText1.append("\n");
        }
        ebcdicTextHeaderTextArea.setText(sampleText1.toString());
    }
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">
    private void initComponents() {
        cancelButton = new javax.swing.JButton();
        ebcdicTextHeaderScrollPane = new javax.swing.JScrollPane();
        ebcdicTextHeaderTextArea = new javax.swing.JTextArea();
        okButton = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        ebcdicTextHeaderTextArea.setColumns(80);
        ebcdicTextHeaderTextArea.setRows(40);
        ebcdicTextHeaderScrollPane.setViewportView(ebcdicTextHeaderTextArea);

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap(228, Short.MAX_VALUE)
                .add(okButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 86, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cancelButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 86, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .add(ebcdicTextHeaderScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(ebcdicTextHeaderScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 450, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED,20,20)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cancelButton)
                    .add(okButton))
                .addContainerGap())
        );
        pack();
    }// </editor-fold>

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	String text = ebcdicTextHeaderTextArea.getText();
    	int index =  text.indexOf('\n');
    	StringBuffer outText = new StringBuffer();
    	while(index != -1){
    		String temp = text.substring(0, index);
    		StringBuffer tempBuffer = new StringBuffer(temp);
    		if(temp.length() < 80){
    			int diff = 80-temp.length();
    			for(int i =0; i < diff; i++)
    				tempBuffer.append(" ");
    			outText.append(tempBuffer.toString());
    		}else
    			outText.append(tempBuffer.toString());
    		
    		text = text.substring(index+1);
    		index = text.indexOf('\n');
    	}
    	StringBuffer tempBuffer = new StringBuffer(text);
    	if(text.length() < 80){
    		int diff = 80-text.length();
    		for(int i =0; i < diff; i++)
    			tempBuffer.append(" ");
    		outText.append(tempBuffer.toString());
    	}else
    		outText.append(tempBuffer.toString());

    	ArrayList params = new ArrayList();
    	MessagingManager manager = messagingAgent.getMessagingMgr();
        params.add(manager.getLocationPref());    // [0] IO preference
        String tempPath = QiProjectDescUtils.getTempPath(messagingAgent.getProjectDescriptor()) + "/" + QiDataConverterConstants.TEXT_HEADER_FILE_NAME; 
        params.add(tempPath);   // [1] file pathname
        params.add(QIWConstants.TEXT_FORMAT);   // [2] file's format
        params.add(outText);   // [3] binary data

        String msgID = messagingAgent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG,
                 QIWConstants.BINARY_FILE_WRITE_CMD,
                 QIWConstants.ARRAYLIST_TYPE, params,true);
        IQiWorkbenchMsg response = manager.getMatchingResponseWait(msgID,5000);
		if(response == null){
			JOptionPane.showMessageDialog(this, "Writing text header is timing out. Please try again or contact workbench support for assistance.", "Timed Out problem.", JOptionPane.WARNING_MESSAGE);
			return;
		}else if (MsgUtils.isResponseAbnormal(response)){
			System.out.println("IO error: " +(String)MsgUtils.getMsgContent(response));
			JOptionPane.showMessageDialog(this, "Writing text header is experiencing problem due to " + MsgUtils.getMsgContent(response) + ". Contact workbench support for assistance.", "Write Error.", JOptionPane.WARNING_MESSAGE);
		}else
			JOptionPane.showMessageDialog(this, "header.txt is successfully written to " + tempPath, "Write Successfully.", JOptionPane.INFORMATION_MESSAGE);
		headerEdited = true;
		doClose(RET_OK);
    }
    
    public boolean isHeaderEdited(){
    	return headerEdited;
    }
    
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        doClose(RET_CANCEL);
    }        
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {                             
        doClose(RET_CANCEL);
    }                            
    
    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }
   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditTextHeaderDialog(new javax.swing.JFrame(), true, null).setVisible(true);
            }
        });
    }
    
    private javax.swing.JButton okButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JScrollPane ebcdicTextHeaderScrollPane;
    private javax.swing.JTextArea ebcdicTextHeaderTextArea;
    // End of variables declaration
    
    private int returnStatus = RET_CANCEL;
    class MyDocument extends PlainDocument
    {
      public void insertString(int offset,String str, AttributeSet a)
        throws BadLocationException {

    	  char[] insertChars = str.toCharArray();       
    	  if((int)insertChars[0] == 10){
    		  getToolkit().beep();
    		  return;
    	  }

          try{
          int line = getLineOfOffset(offset);

          int endoffset1 = 0;
          if(line > 0)
          		endoffset1 = getLineEndOffset(line-1);

          int endoffset2 = getLineEndOffset(line);

          if(endoffset2 >= (endoffset1+81))
        	  getToolkit().beep();
          else
        	  super.insertString(offset, str, a);
          }catch(Exception e){
      	  	e.printStackTrace();
      	  }
      }
      
      private int getLineCount() {
          Element map = getDefaultRootElement();
          return map.getElementCount();
      }
      private int getLineStartOffset(int line) throws BadLocationException {
          int lineCount = getLineCount();
          if (line < 0) {
              throw new BadLocationException("Negative line", -1);
          } else if (line >= lineCount) {
              throw new BadLocationException("No such line", getLength()+1);
          } else {
              Element map = getDefaultRootElement();
              Element lineElem = map.getElement(line);
              return lineElem.getStartOffset();
          }
      }
      
      private int getLineEndOffset(int line) throws BadLocationException {
          int lineCount = getLineCount();
          if (line < 0) {
              throw new BadLocationException("Negative line", -1);
          } else if (line >= lineCount) {
              throw new BadLocationException("No such line", getLength()+1);
          } else {
              Element map = getDefaultRootElement();
              Element lineElem = map.getElement(line);
              int endOffset = lineElem.getEndOffset();
              // hide the implicit break at the end of the document
              return ((line == lineCount - 1) ? (endOffset - 1) : endOffset);
          }
      }
      
      private int getLineOfOffset(int offset) throws BadLocationException {
          if (offset < 0) {
              throw new BadLocationException("Can't translate offset to line", -1);
          } else if (offset > getLength()) {
              throw new BadLocationException("Can't translate offset to line", getLength()+1);
          } else {
              Element map = getDefaultRootElement();
              return map.getElementIndex(offset);
          }
      }
    }
    
    
}
