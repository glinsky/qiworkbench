/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiDataConverter;

public class QiDataConverterConstants {
	public static final String GET_SEGY_FILE_CMD = "getSEGYFile";
	public static final String GET_SU_FILE_CMD = "getSUFile";
	public static final String GET_HORIZON_XYZ_FILES_CMD = "getHorizonXYZFiles";
	public static final String GET_SEISMIC_DATASET_CMD = "getSeismicDataset";
	public static final String SEGY_TEXT_HEADER_FILE_NAME = "TextHeader.txt";
	public static final String xyz2BhpsuScriptPrefix = "xyz_to_bhpsu";
	public static final String lm3d2BhpsuScriptPrefix = "lm3d_to_bhpsu";
	public static final String bhpsu2Lm3dScriptPrefix = "bhpsu_to_lm3d";
	public static final String bhpsu2SEGYScriptPrefix = "bhpsu_to_segy";
	public static final String bhpsu2XYZScriptPrefix = "bhpsu_to_xyz";	
	public static final String lm3dHor2BhpsuScriptPrefix = "lm3d_hor_to_bhpsu";	
	public static final String segy2BhpsuScriptPrefix = "segy_to_bhpsu";
	public static final String su2BhpsuScriptPrefix = "su_to_bhpsu";
	public static final String LITTLE_ENDIAN = "little endian";
	public static final String BIG_ENDIAN = "big endian";
	public static final String LANDMARK_3D = "Landmark 3D";
	public static final String SEGY = "SEGY";
	public static final String SU = "SU";
	public static final String XYZ = "XYZ";
	public static final String SEGY_DATA_FORMAT_IBM_float = "IBM floating point";
	public static final String SEGY_DATA_FORMAT_32_bit_int = "32 bits Integer";
	public static final String SEGY_DATA_FORMAT_16_bit_int = "16 bits Integer";
	public static final String SEGY_DATA_FORMAT_IEEE_float = "IEEE floating point";
	public static final String SEGY_DATA_FORMAT_8_bit_int = "8 bits integer";	
	public static final String DATA_FORMAT_32_bit_float = "32 bits float bricked";
	public static final String DATA_FORMAT_16_bit_float = "16 bits float bricked";
	public static final String DATA_FORMAT_8_bit_float = "8 bits float bricked";
	public static final String DATA_FORMAT_16_bit_int = "16 bits int";
	public static final String DATA_FORMAT_8_bit_int = "8 bits int";
	public static final String TEXT_HEADER_FILE_NAME = "ebcdic.header";
	
	protected static final String [][] SEGY_H_INT_HEADERS = {
			{"tracl","int","1-4"},
			{"tracr","int","5-8"},
			{"fldr","int","9-12"},
			{"tracf","int","13-16"},
			{"ep","int","17-20"},
			{"cdp","int","21-24"},
			{"cdpt","int","25-28"},
			{"trid","short","29-30"},
			{"nvs","short","31-32"},
			{"nhs","short","33-34"},
			{"duse","short","35-36"},
			{"offset","int","37-40"},
			{"gelev","int","41-44"},
			{"selev","int","45-48"},
			{"sdepth","int","49-52"},
			{"gdel","int","53-56"},
			{"sdel","int","57-60"},
			{"swdep","int","61-64"},
			{"gwdep","int","65-68"},
			{"scalel","short","69-70"},
			{"scalco","short","71-72"},
			{"sx","int","73-76"},
			{"sy","int","77-80"},
			{"gx","int","81-84"},
			{"gy","int","85-88"},
			{"counit","short","89-90"},
			{"wevel","short","91-92"},
			{"swevel","short","93-94"},
			{"sut","short","95-96"},
			{"gut","short","97-98"},
			{"sstat","short","99-100"},
			{"gstat","short","101-102"},
			{"tstat","short","103-104"},
			{"laga","short","105-106"},
			{"lagb","short","107-108"},
			{"delrt","short","109-110"},
			{"muts","short","111-112"},
			{"mute","short","113-114"},
			{"ns","unsigned short","115-116"},
			{"dt","unsigned short","117-118"},
			{"gain","short","119-120"},
			{"igc","short","121-122"},
			{"igi","short","123-124"},
			{"corr","short","125-126"},
			{"sfs","short","127-128"},
			{"sfe","short","129-130"},
			{"slen","short","131-132"},
			{"styp","short","133-134"},
			{"stas","short","135-136"},
			{"stae","short","137-138"},
			{"tatyp","short","139-140"},
			{"afilf","short","141-142"},
			{"afils","short","143-144"},
			{"nofilf","short","145-146"},
			{"nofils","short","147-148"},
			{"lcf","short","149-150"},
			{"hcf","short","151-152"},
			{"lcs","short","153-154"},
			{"hcs","short","155-156"},
			{"year","short","157-158"},
			{"day","short","159-160"},
			{"hour","short","161-162"},
			{"minute","short","163-164"},
			{"sec","short","165-166"},
			{"timbas","short","167-168"},
			{"trwf","short","169-170"},
			{"grnors","short","171-172"},
			{"grnofr","short","173-174"},
			{"grnlof","short","175-176"},
			{"gaps","short","177-178"},
			{"otrav","short","179-180"},
			{"mark","short","181-182"},
			{"mutb","short","183-184"},
			{"n2","short","185-186"},
			{"shortpad","short","187-188"},
			{"ntr","int","189-192"}
	};
}
