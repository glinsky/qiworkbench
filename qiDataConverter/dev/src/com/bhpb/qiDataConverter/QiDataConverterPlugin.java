/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiDataConverter;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

/**
 * Wavelet Decomposition plugin, a BHP propritary qiComponent. It is
 * started as a thread by the Messenger Dispatcher that executes the
 * "Activate plugin" command.
 */
public class QiDataConverterPlugin extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
	private static Logger logger = Logger.getLogger(QiDataConverterPlugin.class.getName());

	// messaging mgr for this class only
	private MessagingManager messagingMgr;
	// gui component
	private QiDataConverterGUI gui;

	// my thread
	private static Thread pluginThread;

	/** CID for component instance. Generated before the thread is started and carried as the thread's name. */
	private String myCID = "";

	// boolean used to stop plugin thread
	private boolean stop = false;
    //This delegate exists to add debugging code to a proxy of this class
    private IqiMessageHandler processorDelegate;
	private static int saveAsCount = 0;
	protected boolean horizonExportDone = false;
	private List<Object> datasetInfoList;
	private final String [][] SEGY_H_INT_HEADERS = {{"tracl","int"},
			{"tracr","int"},
			{"fldr","int"},
			{"tracf","int"},
			{"ep","int"},
			{"cdp","int"},
			{"cdpt","int"},
			{"trid","short"},
			{"nvs","short"},
			{"nhs","short"},
			{"duse","short"},
			{"offset","int"},
			{"gelev","int"},
			{"selev","int"},
			{"sdepth","int"},
			{"gdel","int"},
			{"sdel","int"},
			{"swdep","int"},
			{"gwdep","int"},
			{"scalel","short"},
			{"scalco","short"},
			{"sx","int"},
			{"sy","int"},
			{"gx","int"},
			{"gy","int"},
			{"counit","short"},
			{"wevel","short"},
			{"swevel","short"},
			{"sut","short"},
			{"gut","short"},
			{"sstat","short"},
			{"gstat","short"},
			{"tstat","short"},
			{"laga","short"},
			{"lagb","short"},
			{"delrt","short"},
			{"muts","short"},
			{"mute","short"},
			{"ns","unsigned short"},
			{"dt","unsigned short"},
			{"gain","short"},
			{"igc","short"},
			{"igi","short"},
			{"corr","short"},
			{"sfs","short"},
			{"sfe","short"},
			{"slen","short"},
			{"styp","short"},
			{"stas","short"},
			{"stae","short"},
			{"tatyp","short"},
			{"afilf","short"},
			{"afils","short"},
			{"nofilf","short"},
			{"nofils","short"},
			{"lcf","short"},
			{"hcf","short"},
			{"lcs","short"},
			{"hcs","short"},
			{"year","short"},
			{"day","short"},
			{"hour","short"},
			{"minute","short"},
			{"sec","short"},
			{"timbas","short"},
			{"trwf","short"},
			{"grnors","short"},
			{"grnofr","short"},
			{"grnlof","short"},
			{"gaps","short"},
			{"otrav","short"},
			{"mark","short"},
			{"mutb","short"},
			{"n2","short"},
			{"shortpad","short"},
			{"ntr","int"}
	};
	protected String getCID() {
		return myCID;
	}


	public IComponentDescriptor getComponentDescriptor(){
		return messagingMgr.getMyComponentDesc();
	}

	public JInternalFrame getGUI(){
		return gui;
	}
	/** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
	private ComponentDescriptor projMgrDesc = null;

	/** Metadata of project associated with */
	private QiProjectDescriptor qiProjectDesc = new QiProjectDescriptor();

	public QiProjectDescriptor getQiProjectDescriptor() {
		return qiProjectDesc;
	}


	private void runBHPIOTask(String file, Component cmp){
		final Component comp = cmp;
		final String filePath = file;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering input dataset information";
				String text = "Please wait while system is gathering input dataset information....";

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				ProgressUtil.start(monitor,"");
				int status = getSeismicDatasetInfo(filePath);

				if(ProgressUtil.isCancel(monitor)){
					//if(monitor.isCancel()){
					//gui.setEntireDatasetButtonEnabled(false,comp);
					if(comp instanceof RangeSettable){
						RangeSettable rs = (RangeSettable)comp;
						rs.setEntireDatasetButtonEnabled(false);
					}

					return;
				}
				logger.info("status = " + status);
				//if(status == 0 || status == 1 || status == -1){
				if(status == 0){
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
					List<String> horizonList = (List<String>)datasetInfoList.get(1);
					if(comp instanceof Bhpsu2XYZPanel){
						if(horizonList != null && horizonList.size() == 0){
							JOptionPane.showMessageDialog(gui,"The selected dataset does not appear to be a HORIZON set. Please choose another dataset and try again.",
									"Wrong Dataset", JOptionPane.WARNING_MESSAGE);
							return;
						}

					}else{
						if(horizonList != null && horizonList.size() > 0){
							JOptionPane.showMessageDialog(gui,"The selected dataset does not appear to be a SEISMIC set. Please choose another dataset and try again.",
									"Wrong Dataset", JOptionPane.WARNING_MESSAGE);
							return;
						}

					}
					//monitor.setCurrent(null,monitor.getTotal());
					//if(status == 0){
					Runnable updateAComponent = new Runnable() {
						public void run() {
							//String datasetsDir = QiProjectDescUtils.getDatasetsPath(qiProjectDesc);
							//String dataset = filePath.substring(datasetsDir.length()+1);
							//gui.setSeismicVolumeName(dataset);
							//gui.setSeismicVolumeName(filePath,comp);

							//gui.setEntireDatasetButtonEnabled(true,comp);

							//gui.setLineTraceFieldEnabled(true,comp);
							int [][] range = new int[4][3];
							Map<String,DataSetHeaderKeyInfo> seismicDataset = (Map<String,DataSetHeaderKeyInfo>)datasetInfoList.get(0);
							Iterator i = seismicDataset.entrySet().iterator();
				            while (i.hasNext()) {
				                Map.Entry e = (Map.Entry) i.next();
				                String key = (String)e.getKey();
				                Object value = e.getValue();

							//for (String s : seismicDataset.keySet()) {
								if (key.equals("cdp")) {
									DataSetHeaderKeyInfo ds = (DataSetHeaderKeyInfo)value;
									if (ds != null) {
										range[1][2] = (int) ds.getIncr();
										range[1][0] = (int) ds.getMin();
										range[1][1] = (int) ds.getMax();
									}
								}
								if (key.equals("ep")) {
									DataSetHeaderKeyInfo ds = (DataSetHeaderKeyInfo)value;
									if (ds != null) {
										range[0][2] = (int) ds.getIncr();
										range[0][0] = (int) ds.getMin();
										range[0][1] = (int) ds.getMax();
									}
								}
								if (key.equals("tracl")) {
									DataSetHeaderKeyInfo ds = (DataSetHeaderKeyInfo)value;
									if (ds != null) {
										range[2][2] = (int) ds.getIncr();
										range[2][0] = (int) ds.getMin();
										range[2][1] = (int) ds.getMax();
									}
								}
								if (key.equals("cdpt")) {
									DataSetHeaderKeyInfo ds = (DataSetHeaderKeyInfo)value;
									if (ds != null) {
										range[3][2] = (int) ds.getIncr();
										range[3][0] = (int) ds.getMin();
										range[3][1] = (int) ds.getMax();
									}
								}
							}
							//gui.setSUDatasetDefaultValue(range, comp);
							if(comp instanceof RangeSettable){
								RangeSettable rs = (RangeSettable)comp;
								rs.setSeismicVolumeName(filePath);
								rs.setLineTraceFieldEnabled(true);
								rs.setDefaultRangeInfo(range);
								rs.setEntireDatasetButtonEnabled(true);
							}
							if(comp instanceof Bhpsu2XYZPanel){
								Bhpsu2XYZPanel panel = (Bhpsu2XYZPanel)comp;
								List<String> horizonList = (List<String>)datasetInfoList.get(1);
								Object[] array = horizonList.toArray();
								panel.setHorizonList(array);
							}
						}
					};
					SwingUtilities.invokeLater(updateAComponent);
					//}else{
					//	JOptionPane.showMessageDialog(gui,"Error in running SU script.",
					//			"Problem in running SU script",JOptionPane.WARNING_MESSAGE);
					//}
				}else
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
			}
		};
		new Thread(heavyRunnable).start();

	}



	private boolean segyReadFinished = false;
	public boolean isSegyReadFinished(){
		return segyReadFinished;
	}
	private boolean suReadFinished = false;
	public boolean isSUReadFinished(){
		return suReadFinished;
	}

	public void runSUREADTask(String file, final HeaderConvertablePanel cmp, final int endian){
		final String filePath = file;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering input SU information";
				String text = "Please wait while system is gathering input SU file information....";

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));
				Component comp = cmp;
				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				suReadFinished = false;
				ProgressUtil.start(monitor,"");
				//int endian = gui.getSelectedEndianess(comp);
				int status = getSEGYFileInfo(filePath,endian,cmp);
				//if(status == 0)
				//	gui.setSelectedSEGYFile(filePath);
				if(ProgressUtil.isCancel(monitor)){
					//if(monitor.isCancel()){
					//gui.setEntireDatasetButtonEnabled(false,comp);
					return;
				}
				logger.info("runSUREADTask status = " + status);
				//if(status == 0 || status == 1 || status == -1){
				if(status == 0){
					suReadFinished = true;
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				}else{
					JOptionPane.showMessageDialog(gui,"Problem in reading su. Please check the error log or consult workbench support.",
							"Error in running SU script.", JOptionPane.WARNING_MESSAGE);
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				}
			}
		};
		new Thread(heavyRunnable).start();

	}
	public void runSEGYREADTask(String file, final HeaderConvertablePanel cmp, final int endian){
		final Component comp = cmp;
		final String filePath = file;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering input SEGY information";
				String text = "Please wait while system is gathering input SEGY file information...";

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				segyReadFinished = false;
				ProgressUtil.start(monitor,"");
				//int endian = gui.getSelectedEndianess(comp);
				int status = getSEGYFileInfo(filePath,endian,cmp);
				//if(status == 0)
				//	gui.setSelectedSEGYFile(filePath);
				if(ProgressUtil.isCancel(monitor)){
					//if(monitor.isCancel()){
					//gui.setEntireDatasetButtonEnabled(false,comp);
					return;
				}
				logger.info("runSEGYREADTask status = " + status);
				//if(status == 0 || status == 1 || status == -1){
				if(status == 0){
					segyReadFinished = true;

					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));

				}else{
					JOptionPane.showMessageDialog(gui,"Problem in running segyread command. Please check the error log or consult workbench support.",
							"Error in running SU script.", JOptionPane.WARNING_MESSAGE);
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				}
			}
		};
		new Thread(heavyRunnable).start();

	}


	/** Invoke File Chooser Service
	 * @param list Passed through to fileChooser from caller
	 *
	 */
	public void callFileChooser(QiFileChooserDescriptor desc){
		if(desc == null)
			return;
		ComponentDescriptor fileChooser = null;
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			logger.warning("Respone to get file chooser service returning null due to timed out");
			return;
		}
		else if(MsgUtils.isResponseAbnormal(response)) {
			logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
			return;
		}
		else
			fileChooser = (ComponentDescriptor)MsgUtils.getMsgContent(response);

		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_QI_FILE_CHOOSER_CMD,fileChooser,QiFileChooserDescriptor.class.getName(),desc);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD,fileChooser);
		return;
	}
	/**
	 * Initialize the plugin component:
	 * <ul>
	 * <li>Create its messaging manager</li>
	 * <li>Register with the Message Dispatcher</li>
	 * </ul>
	 */
	public void init() {
		try {
			QIWConstants.SYNC_LOCK.lock();
			messagingMgr = new MessagingManager();

			myCID = Thread.currentThread().getName();

			// register self with the Message Dispatcher
			messagingMgr.registerComponent(QIWConstants.PLUGIN_AGENT_COMP, "qiDataConverter", myCID);
			QIWConstants.SYNC_LOCK.unlock();
		} catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in AmpExtPlugin.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
	}

	public List<String> getUnmadeQiProjDescDirs(){
		List params = new ArrayList();
		params.add(messagingMgr.getLocationPref());
		params.add(qiProjectDesc);
		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_UNMADE_QIPROJ_DESC_DIRS_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);

		if (resp == null){
			logger.info("Response returning null from command GET_UNMADE_QIPROJ_DESC_DIRS_CMD. Consult qiWorkbench technical support.");
			return null;
		}else if(resp.isAbnormalStatus()){
			logger.info("Abnormal response from GET_UNMADE_QIPROJ_DESC_DIRS_CMD command:  in getting unmade directories that do not yet exists to meet QiProject's meta data specification. " + (String)resp.getContent());
			JOptionPane.showMessageDialog(gui, "Error in getting directories that do not yet exist to meet QiProject's meta data specification." + " cause: " +  (String)resp.getContent(), "QI Workbench",
					JOptionPane.WARNING_MESSAGE);
			return null;
		} else{
			return (List<String>)(resp.getContent());
		}
	}

	public boolean makeQiProjDescDirs(List<String> list){
		if(list == null || list.size() == 0)
			return true;

		List params = new ArrayList();
		params.add(messagingMgr.getLocationPref());
		params.add(list);
		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CREATE_DIRECTORIES_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);

		if (resp == null){
			logger.info("Response returning null from command CREATE_DIRECTORIES_CMD. Consult qiWorkbench technical support.");
			return false;
		}else if(resp.isAbnormalStatus()){
			logger.info("Abnormal response from CREATE_DIRECTORIES_CMD command:  in getting unmade directories that do not yet exists to meet QiProject's meta data specification. " + (String)resp.getContent());
			JOptionPane.showMessageDialog(gui, "Error in getting directories that do not yet exist to meet QiProject's meta data specification." + " cause: " +  (String)resp.getContent(), "QI Workbench",
					JOptionPane.WARNING_MESSAGE);
			return false;
		} else{
			logger.info("Normal response from CREATE_DIRECTORIES_CMD command:  " + (String)resp.getContent());
			String result = (String)resp.getContent();
			if(result.equals("success"))
				return true;
			return false;
		}
	}

	/**
	 * Generate state information into xml string format
	 * @return  String
	 */
	public String genState(){
		StringBuffer content = new StringBuffer();
		IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
		//TODO WorkbenchStateManager will need to be changed to conform to the package change of WaveletDecompPlugin in ServletDispatcher
		content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
		content.append(gui.genState());
		content.append("</component>\n");
		return content.toString();
	}

	/**
	 * Generate state information into xml string format in repsonse to save as command
	 * @return  String
	 */
	public String genStateAsClone(){
		StringBuffer content = new StringBuffer();
		IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
		//TODO WorkbenchStateManager will need to be changed to conform to the package change of the plugin in ServletDispatcher
		String displayName = "";
		saveAsCount++;
		if(saveAsCount == 1)
			displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
		else
			displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
		content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
		content.append(gui.genState());
		content.append("</component>\n");

		return content.toString();
	}


	private void printClassLoaderName(String context,ClassLoader loader){
		if(loader != null){
			System.out.println("context " + context + " loader name " + loader.getClass().getName());
			printClassLoaderName(context,loader.getParent());
		}
		return;
	}
	public QiProjectDescriptor getProjectDescriptor(){
		return qiProjectDesc;
	}



	/** restore plugin and it's gui to previous condition
	 *
	 * @param node xml containing saved state variables
	 */
	void restoreState(Node node){
		String preferredDisplayName = ((Element)node).getAttribute("preferredDisplayName");
		NodeList children = node.getChildNodes();
		for(int i = 0; i < children.getLength(); i++){
			Node child = children.item(i);
			if(child.getNodeType() == Node.ELEMENT_NODE){
				if(child.getNodeName().equals(QiDataConverterGUI.class.getName())){
					qiProjectDesc = getProjectDesc(child);
					gui = new QiDataConverterGUI(this);
					gui.restoreState(child);

					if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
						CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
						gui.renamePlugin(preferredDisplayName);
					}
				}
			}
		}
	}




	/**
	 * Import state saved in a XML file. Use existing GUI. User not asked to save current
	 * state first, because after import, saving state will save imported state. The preferred
	 * display name is not changed. Therefore, the name in the GUI title and component tree node
	 * don't have to change.
	 *
	 * @param node XML containing saved state variables
	 */
	void importState(Node node) {
		NodeList children = node.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				if (child.getNodeName().equals(QiDataConverterGUI.class.getName())) {
					//get project descriptor from state since GUI needs it
					//keep the associated project
					gui.restoreState(child);

				}
			}
		}
	}

    	
	/** Initialize the plugin, then start processing messages its receives.
	 * The stop flag is set when user has terminated the GUI, and it is time to quit
	 * If a message has the skip flag set, it is to be handled by the GUI, not this class
	 */
	public void run() {
		// initialize the Wavelet Decomposition plugin
		init();

		//process any messages received from other components
		while(stop == false) {
			//Check if associated with a PM. If not (because was restored), discover the PM
			//with a matching PID. There will be one once its GUI comes up.
			String myPid = QiProjectDescUtils.getPid(qiProjectDesc);
			while (projMgrDesc == null && !myPid.equals("")) {
				//NOTE: Request will stay forwever on outstanding request list if no PM with matching PID.
				//      This can occur when restoring the workbench and the PM hasn't been restored
				//      or it is being restored but its GUI is not yet up.
				//Message Dispatcher will broadcast command to all PMs. Only PM with matching PID
				//will send back a response.
				messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
				try {
					//wait and then try again
					Thread.currentThread().sleep(1000);
					//if a message arrived, process it. It probably is the response from a PM.
					if (!messagingMgr.isMsgQueueEmpty()) break;
				} catch (InterruptedException ie) {
					continue;
				}
			}
			/*
			IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
			if(msg != null && !(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip())){
				msg = messagingMgr.getNextMsgWait();
				processMsg(msg);
			}
			*/
			IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            //if a data message with skip flag set to true, it will not be processed here
            //instead it will be claimed by calling getMatchingResponseWait
            if (msg != null) {
                // If it is a response with a timed-out request, dequeue it
                if (msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip()) {
                    if (messagingMgr.getMatchingOutstandingRequest(msg) == null) {
                        msg = messagingMgr.getNextMsg();
                        logger.warning("Consuming response message which lacks a matching outstanding request. Response msg: " + msg);
                    }
                } else { // it is a request or asynchronous response, process it
                msg = messagingMgr.getNextMsgWait();
                if (processorDelegate != null) {
                    logger.info("Processor delegate non-null, delegating processing through IqiMessageHandler interface " + processorDelegate);
                    processorDelegate.processMsg(msg);
                }
                else {
                    logger.info("Processor delegate is null, processing my own message");
                    try{
                    	processMsg(msg);
                    }catch(Exception e){
                    	e.printStackTrace();
                    	logger.warning(ExceptionMessageFormatter.getFormattedMessage(e));
                    }
                }
                }
            }
		}
	}


	/**
	 * Extract the project descriptor from the saved state. If the state
	 * does not contain a project descriptor, form one from the implicit
	 * project so backward compatible.
	 *
	 * @return Project descriptor for project associated with the bhpViewer.
	 */
	private QiProjectDescriptor getProjectDesc(Node node) {
		QiProjectDescriptor projDesc = null;
		NodeList children = node.getChildNodes();
		String nodeName;
		Node child;
		for (int i = 0; i < children.getLength(); i++) {
			child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				nodeName = child.getNodeName();
				if (nodeName.equals("QiProjectDescriptor")) {
					projDesc = (QiProjectDescriptor)XmlUtils.XmlToObject(child);
					break;
				}
			}
		}

		//Check if saved state has a project descriptor. If not, create
		//one using the implicit project so backward compatible.
		if (projDesc == null) {
			projDesc = new QiProjectDescriptor();
			//get path of implicit project from Message Dispatcher
			String projPath = messagingMgr.getProject();
			QiProjectDescUtils.setQiSpace(projDesc, projPath);
			//leave relative location of project as "/"
			int idx = projPath.lastIndexOf("\\");
			if (idx == -1) idx = projPath.lastIndexOf("/");
			String projectName = projPath.substring(idx+1);
			QiProjectDescUtils.setQiProjectName(projDesc, projectName);
		}

		return projDesc;
	}

	/**
	 * Find the request matching the response and process the response based on the request.
	 */
	public void processMsg(IQiWorkbenchMsg msg) {
		/** Request that matches the response */
		IQiWorkbenchMsg request = null;

		//log message traffic
		logger.fine("msg="+msg.toString());

		// Check if a response. If so, process and consume response
		if(messagingMgr.isResponseMsg(msg)) {
			request = messagingMgr.checkForMatchingRequest(msg);
			String cmd = MsgUtils.getMsgCommand(request);
			// check if from the message dispatcher
			if(messagingMgr.isResponseFromMsgDispatcher(msg)) {
				if(cmd.equals(QIWConstants.NULL_CMD))
					return;
			}
			else if(cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
				String jobID = (String)msg.getContent();
				logger.info("JOB ID=" + jobID);
			}
			else if(cmd.equals(QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD)) {
				QiFileChooserDescriptor desc = (QiFileChooserDescriptor)msg.getContent();
				if(desc.getReturnCode()  == JFileChooser.APPROVE_OPTION){
					List<String> list = desc.getSelectedFileNameList();
					if(!desc.isMultiSelectionEnabled()){ //single file selection mode
						String filesep = messagingMgr.getServerOSFileSeparator();
						String filePath = desc.getSelectedFilePathBase() + filesep + list.get(0);
						String command = desc.getMessageCommand();
						//HeaderConvertablePanel comp = (HeaderConvertablePanel)desc.getParentGUI();
						Component comp = (Component)desc.getParentGUI();
						if(command.equals(QiDataConverterConstants.GET_SEISMIC_DATASET_CMD)){
							runBHPIOTask(filePath,comp);
						}else if(command.equals(QiDataConverterConstants.GET_SEGY_FILE_CMD)){
							//int endian = gui.getSelectedEndianess(comp);
							HeaderConvertablePanel comp1 = (HeaderConvertablePanel)comp;
							int endian = comp1.getSelectedEndianess();
							runSEGYREADTask(filePath, comp1, endian);
						}else if(command.equals(QiDataConverterConstants.GET_SU_FILE_CMD)){
							//int endian = gui.getSelectedEndianess(comp);
							HeaderConvertablePanel comp1 = (HeaderConvertablePanel)comp;
							int endian = comp1.getSelectedEndianess();
							runSUREADTask(filePath, comp1, endian);
						}

					}else if(desc.getMessageCommand().equals(QiDataConverterConstants.GET_HORIZON_XYZ_FILES_CMD)){ //select multiple file
						Component comp = desc.getParentGUI();
						if(comp instanceof XYZ2BhpsuPanel){
							XYZ2BhpsuPanel panel = (XYZ2BhpsuPanel)comp;
							panel.setSelectedHorizonList(list);
						}
					}
				}
			}
			// route the result from file chooser service back to its caller
			else if(cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
				ArrayList list = (ArrayList)msg.getContent();
				final String selectionPath = (String)list.get(1);

				if (((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
					final String filePath = (String)list.get(1);
					String action = (String)list.get(3);
					if (action.equals(QIWConstants.EXPORT_COMP_STATE)) {
                    	//Write XML state to the specified file and location
                    	String pmState = genState();
                    	String xmlHeader = "<?xml version=\"1.0\" ?>\n";
                    	//TODO: handle remote case - use an IO service
                    	try {
                    		ComponentStateUtils.writeState(xmlHeader+pmState, selectionPath,messagingMgr);
                    	} catch (QiwIOException qioe) {
                    		//TODO notify user cannot write state file
                    		logger.finest(qioe.getMessage());
                    	}
                    }
                    else if (action.equals(QIWConstants.IMPORT_COMP_STATE)) {
                    	//TODO: handle remote case - use an IO service
                    	//check if file exists
                    	if (ComponentStateUtils.stateFileExists(selectionPath,messagingMgr)) {
                    		try {
                    			String pmState = ComponentStateUtils.readState(selectionPath,messagingMgr);
                    			Node compNode = ComponentStateUtils.getComponentNode(pmState);
                    			if (compNode == null) {
                    				JOptionPane.showMessageDialog(gui, "Can't convert XML state to XML object", "Internal Processing Error", JOptionPane.ERROR_MESSAGE);
                    			} else {
                    				//check for matching component type
                    				String compType = ((Element)compNode).getAttribute("componentType");
                    				String expectedCompType = messagingMgr.getRegisteredComponentDisplayNameByDescriptor(messagingMgr.getMyComponentDesc());
                    				if (!compType.equals(expectedCompType)) {
                    					JOptionPane.showMessageDialog(gui, "State file not for this qiComponent type. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                    				} else {
                    					//Note: don't ask if want to save state before importing because
                    					//      for future saves, the imported state will be saved.

                    					//import state
                    					importState(compNode);
                    				}
                    			}
                    		} catch (QiwIOException qioe) {
                    			JOptionPane.showMessageDialog(gui, "Can't read state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                    			logger.finest(qioe.getMessage());
                    		}
                    	} else {
                    		JOptionPane.showMessageDialog(gui, "State file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                    	}
                    }
				}
			}
			else if (cmd.equals(QIWConstants.GET_PROJ_INFO_CMD)) {
				ArrayList projInfo = (ArrayList)msg.getContent();
				qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);

				//reset window title (if GUI up)
				if (gui != null) {
					String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
					gui.resetTitle(projName);
				}
			}
			else if (cmd.equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
				ArrayList projInfo = (ArrayList)msg.getContent();
				projMgrDesc = (ComponentDescriptor)projInfo.get(0);
				qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);
				String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
				gui.resetTitle(projName);
			}
			// TODO other possible responses...
			else
				logger.warning("WaveletDecompPlugin: Response to " + cmd + " command not processed " + msg.toString());
			return;
		}
		//Check if a request. If so, process and send back a response
		else if(messagingMgr.isRequestMsg(msg)) {
			String cmd = msg.getCommand();
			// deactivate plugin came from user, tell gui to quit and stop run method
			if(cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) || cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
				try{
					if(gui != null && gui.isVisible())
						gui.closeGUI();
					// unregister plugin
					messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
				}
				catch (Exception e) {
					IQiWorkbenchMsg res = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Unknown exception occurred with " + myCID);
					messagingMgr.routeMsg(res);
				}
				stop = true;
				messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,myCID + " is successfully deactivated.");
			} else
				if(cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
					if(gui != null)
						gui.setVisible(true);
				} else
					if(cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
						if(gui != null){
		                	messagingMgr.sendResponse(msg,Component.class.getName(),(Component)gui);
							gui.closeGUI();
						}
					} else
						if(cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
							String preferredDisplayName = (String)msg.getContent();
							if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
								CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
								gui.renamePlugin(preferredDisplayName);
							}
							messagingMgr.sendResponse(msg,QIWConstants.COMP_DESC_TYPE,messagingMgr.getMyComponentDesc());
						}
//			save state
						else if(cmd.equals(QIWConstants.SAVE_COMP_CMD))
							messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genState());
//			save component state as clone
						else if(cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
							messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genStateAsClone());
							// restore state
						} else if(cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
							Node node = (Node)msg.getContent();
							restoreState(node);
							messagingMgr.sendResponse(msg,QiDataConverterGUI.class.getName(),gui);
						}
//			invoke is done after activate
						else if(cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
							//Do nothing if don't have a GUI
							// Create the plugin's GUI, but don't make it visible. That is
							// up to the Workbench Manager who requested the plugin be
							// activated. Pass GUI the CID of its parent.
							//check to see if content incudes xml information which means invoke self will
							// do the restoration
							ArrayList msgList = (ArrayList)msg.getContent();
							String invokeType = (String)msgList.get(0);
							if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)){
								Node node = ((Node)((ArrayList)msg.getContent()).get(2));
								restoreState(node);
							} else if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)){
//								qiProjectDesc = ((QiProjectDescriptor)((ArrayList)msg.getContent()).get(2));
								gui = new QiDataConverterGUI(this);
								java.awt.Point p = (java.awt.Point)msgList.get(1);
				                if(p != null)
				                	gui.setLocation(p);
							}

							//Send a normal response back with the plugin's JInternalFrame and
							//let the Workbench Manager add it to the desktop and make
							//it visible.
							messagingMgr.sendResponse(msg,QiDataConverterGUI.class.getName(),gui);
						}
//			user selected rename plugin menu item
						else if(cmd.equals(QIWConstants.RENAME_COMPONENT)) {
							ArrayList<String> names = new ArrayList<String>(2);
							names = (ArrayList)msg.getContent();
							gui.renamePlugin(names.get(1));
						}
						else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
							//Just a notification. No response required or expected.
							projMgrDesc = (ComponentDescriptor)msg.getContent();
							//Note: Cannot get info about PM's project because GUI may not be up yet.
							if (projMgrDesc != null)
								messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
						} else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
							ArrayList info = (ArrayList)msg.getContent();
							ComponentDescriptor pmDesc = (ComponentDescriptor)info.get(0);
							QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
							//ignore message if not from associated PM
							if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
								qiProjectDesc = projDesc;
//								TODO: update PID [get from projDesc?]
								//update window title
								String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
								gui.resetTitle(projName);
							}
						}
						else
							logger.warning("QiDataConverterPlugin: Request not processed, requeued: " + msg.toString());
			return;
		}
	}



	/** Send a message to the Workbench Manager to remove this instance of WaveletDecomp from the component tree and send a message to it to deactivate itself.
	 */
	public void deactivateSelf(){
		//ask Workbench Manager to remove WaveletDecomp from workbench GUI and then send it back to self to deactivate self
		IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.QUIT_COMPONENT_CMD,wbMgr,QIWConstants.STRING_TYPE,
				messagingMgr.getMyComponentDesc());
	}


	/**
	 * Get the seismic dataset information by calling SU script bhpio given the data input, then retrieve the output,
	 * parse the output and form the data structure
	 * @param filePath file path of the seismic dataset input
	 * @return status indicating normal 0 or abnormal non 0
	 */
	public int getSeismicDatasetInfo(String filePath) {
		if(filePath == null || filePath.trim().length() == 0)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		params.add(messagingMgr.getLocationPref());
		params.add("bash");
		params.add("-c");
		//String fileDir = filePath.substring(0,filePath.lastIndexOf(File.separator));
		String filesep = messagingMgr.getServerOSFileSeparator();
		String fileDir = filePath.substring(0,filePath.lastIndexOf(filesep));
		//String fileName = filePath.substring(filePath.lastIndexOf(File.separator)+1);
		String fileName = filePath.substring(filePath.lastIndexOf(filesep)+1);
		fileName = fileName.substring(0,fileName.lastIndexOf(".dat"));
		params.add(" source ~/.bashrc; cd " + fileDir + "; bhpio filename=" +fileName);
		params.add("true");
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD errorContent = " + errorContent);
				JOptionPane.showMessageDialog(gui,"Error in running SU script.",
						"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}
			//return status = -1;
			return errorStatus;
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
			}
			//return status = -1;
			return errorStatus;
		}
		else{
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			datasetInfoList = parseBHPIOOutput(stdOut);
			logger.info("eismicDataset header info = " + datasetInfoList.get(0));
			logger.info("shorizon info = " + datasetInfoList.get(1));
		}

		//get stderr
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
				JOptionPane.showMessageDialog(gui,"Error in running SU script.",
						"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
				return status;
			}
			//return status = -1;
			return status;
		}
		else{
			ArrayList stdErr = (ArrayList)MsgUtils.getMsgContent(response);

			for(int i=0; i<stdErr.size(); i++)
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));

			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
			if(stdErr.size() > 0){
				JOptionPane.showMessageDialog(gui,"Error in running SU script:" + stdErr,
						"Problem in running SU script", JOptionPane.WARNING_MESSAGE);
			}
		}

		//Wait until the command has finished
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
			}
			//return status = -1;
			return status;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		// get exitValue
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}
	}

	/**
	 * Parse the surange output (a list of Strings) and return a list of
	 * DataSetHeaderKeyInfo
	 * @param filePath   the file path of the target segy file
	 * @param endian   the value of endianess
	 * @param comp     the UI component where the requested is made
	 * @return status of the method
	 */
	private int getSEGYFileInfo(String filePath, int endian,HeaderConvertablePanel comp) {
		if(filePath == null || filePath.trim().length() == 0)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		params.add(messagingMgr.getLocationPref());
		params.add("bash");
		params.add("-c");
		String loc = QiProjectDescUtils.getTempPath(qiProjectDesc);
        if(loc != null && checkDirExist(loc).equals("no")){
            List<String> lst = new ArrayList<String>();
            lst.add(loc);
            makeQiProjDescDirs(lst);
        }
		String command = "";
		String filesep = messagingMgr.getServerOSFileSeparator();
		String segyFilePrefix = Utils.getFilePrefix(filePath, filesep);
		if(comp instanceof SEGY2BhpsuPanel){
			command = " source ~/.bashrc; segyread tape=" + filePath;
			if(endian == 0 || endian == 1){
				command += " endian=" + endian;
			}
			command += " hfile=" + loc + "/" + segyFilePrefix + "_" + QiDataConverterConstants.SEGY_TEXT_HEADER_FILE_NAME;
		}else if(comp instanceof SU2BhpsuPanel){
			command = " source ~/.bashrc; cat " + filePath;
			if(endian == 0 || endian == 1){
				command += " | suswapbytes format=" + endian;
			}
		}

		if(comp instanceof SEGY2BhpsuPanel)
			command += " | segyclean | surange";
		else if(comp instanceof SU2BhpsuPanel)
			command += " | surange";
		params.add(command);
		params.add("true");
		
		logger.info("command= " + params.get(3));
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		int WAIT_TIME = 10000;
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID, WAIT_TIME, true); 

		if(response == null){
			errorContent = "Your request has timed out.";
			logger.info("SUBMIT_JOB_CMD message content = " + errorContent);
			comp.setStdOutErrorMessage(errorContent);
			return -1;
		}
		
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
				//gui.setStdOutErrorMessage(errorContent,comp);
				comp.setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "GET_JOB_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				//gui.setStdOutErrorMessage(errorContent,comp);
				comp.setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			logger.info("stdOut.size() of GET_JOB_OUTPUT_CMD begin =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			logger.info("stdOut.size() of GET_JOB_OUTPUT_CMD end =" + stdOut.size());

			List<DataSetHeaderKeyInfo> list = parseSEGYREADOutput(stdOut,comp);
			//segyRangeList = list;
			//gui.setSURangeList(list,comp);
			comp.setSURangeList(list);
			//gui.setSelectedInputFile(filePath,comp);
			comp.setSelectedInputFile(filePath);
			String [] columnNames = {"Header Name","Minimum","Maximum"};
			int size = 0;
			if(list != null)
				size = list.size();
			String [][] columnData = new String[size][3];
			String [] headers = new String[size+1];
			headers[0] = "Select one";
			for(int i = 0; i < size; i++){
				DataSetHeaderKeyInfo ds = list.get(i);
				columnData[i][0] = ds.getName();
				if(isIntegerTypeHeader(ds.getName()))
					headers[i+1] = ds.getName();
				columnData[i][1] = String.valueOf((int)ds.getMin());
				columnData[i][2] = String.valueOf((int)ds.getMax());
			}

			//ComboBoxModel listModelEp = new DefaultComboBoxModel(headers);
			//ComboBoxModel listModelCdp = new DefaultComboBoxModel(headers);
			//gui.setHeaderNameList(listModelEp,listModelCdp, comp);
			comp.setHeaderNameList(headers);
			TableModel tableModel = new DefaultTableModel(columnData,columnNames){
				Class[] types = new Class [] {
						java.lang.String.class, java.lang.String.class, java.lang.String.class
				};
				boolean[] canEdit = new boolean [] {
						false, false, false
				};

				public boolean isCellEditable(int rowIndex, int columnIndex) {
					return canEdit [columnIndex];
				}
			};
			//gui.setHeaderRangeTableModel(tableModel,comp);
			comp.setHeaderRangeTableModel(tableModel);
		}
		//		get stderr
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
				//JOptionPane.showMessageDialog(gui,"Error in running SU script.",
				//		"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}else{
				errorContent = "GET_JOB_ERROR_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				//gui.setStdOutErrorMessage(errorContent,comp);
				comp.setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			 ArrayList stdErr = (ArrayList)MsgUtils.getMsgContent(response);

			StringBuffer errorContentB = new StringBuffer();
			for(int i=0; i<stdErr.size(); i++){
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));
				errorContentB.append(stdErr.get(i) + "\n");
			}
			//gui.setStdOutErrorMessage(errorContentB.toString(),comp);
			comp.setStdOutErrorMessage(errorContentB.toString());
			status = MsgUtils.getMsgStatusCode(response);
			logger.warning("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
			//if(stdErr.size() > 0){
			//	JOptionPane.showMessageDialog(gui,"Error in running SU script:" + stdErr,
			//		"Problem in running SU script", JOptionPane.WARNING_MESSAGE);
			//}
		}

		//Wait until the command has finished
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
				//gui.setStdOutErrorMessage(errorContent,comp);
				comp.setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "WAIT_FOR_JOB_EXIT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				//gui.setStdOutErrorMessage(errorContent,comp);
				comp.setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		// get exitValue
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}
	}


	private boolean isIntegerTypeHeader(String header){
		if(header == null)
			return false;
		for(int i = 0; i < SEGY_H_INT_HEADERS.length; i++){
			if(header.equals(SEGY_H_INT_HEADERS[i][0]))
				return true;

		}
		return false;
	}

	public String[][] getSegyIntHeader(){
		return SEGY_H_INT_HEADERS.clone();
	}

	/**
	 * Parse the surange output (a list of Strings) and return a list of
	 * DataSetHeaderKeyInfo
	 * @param list   the list of string from surange output
	 * @param comp   the UI component where the action is requested.
	 * @return a list of DataSetHeaderKeyInfo objects
	 */
	private synchronized List<DataSetHeaderKeyInfo> parseSEGYREADOutput(List<String> list, HeaderConvertablePanel comp){
		if(list == null || list.size() == 0)
			return null;
		String first = list.get(0);
		String[] array = first.split(" +");
		if(!array[1].startsWith("traces")){
			String error = "Unexpected abnormal output from segyread or reading su task, contact component support.";
			//gui.setStdOutErrorMessage(error,comp);
			comp.setStdOutErrorMessage(error);
			return null;
		}
		//gui.setNumberOfTraces(array[0],comp);
		comp.setNumberOfTraces(array[0]);
		List<DataSetHeaderKeyInfo> headerList = new ArrayList<DataSetHeaderKeyInfo>();
		for(int i = 1; i < list.size(); i++){
			String arr [] = list.get(i).split(" +");
			if(arr.length > 2){ //in the form of "tracl    353221 356271 (356266 - 353226)"
				DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo(arr[0],Double.valueOf(arr[1]).doubleValue(),Double.valueOf(arr[2]).doubleValue(),1);
				headerList.add(ds);
			}else{ // in the form of "ns       626"
				DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo(arr[0],Double.valueOf(arr[1]).doubleValue(),Double.valueOf(arr[1]).doubleValue(),1);
				headerList.add(ds);
			}
		}
		return headerList;
	}

	/**
	 * Parse the bhpio output (a list of Strings) and return a list of
	 * DataSetHeaderKeyInfo
	 * @param list   the list of string from surange output
	 * @return a list of objects; the first element is an instance of Map<String,DataSetHeaderKeyInfo>
	 * 							  the second element is a list of String if the dataset is a horizon set
	 */
	private List<Object> parseBHPIOOutput(List<String> list){
		if(list == null || list.size() == 0)
			return null;

		boolean foundHorizon = false;
		List<String> list2 = new ArrayList<String>();
		boolean foundHeaderKey = false;
		String horizonLine = null;
		for(String s : list){
			if(foundHeaderKey && s.trim().startsWith("Name:"))
				list2.add(s);
			if(foundHorizon && s.trim().startsWith("HORIZONS:"))
				horizonLine = s;
			if(s.startsWith("Number of Header Keys:")){
				foundHeaderKey = true;
			}
			if(s.contains("is HORIZON data")){
				foundHorizon = true;
			}
		}
		Map<String,DataSetHeaderKeyInfo> map = new HashMap<String,DataSetHeaderKeyInfo>();
		for(String s : list2){
			String [] s2 = s.split(",");
			//String tag = "Name: ";
			String name = s2[0].substring(s2[0].indexOf(":")+1).trim();
			// tag = "Min: ";
			String min = s2[1].substring(s2[1].indexOf(":")+1).trim();
			//tag = "Max: ";
			String max = s2[2].substring(s2[2].indexOf(":")+1).trim();
			//tag = "Incr: ";
			String incr = s2[3].substring(s2[3].indexOf(":")+1).trim();
			DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo(name,Double.valueOf(min).doubleValue(),Double.valueOf(max).doubleValue(),Double.valueOf(incr).doubleValue());
			map.put(name,ds);
		}

		List<String> horizonList = new ArrayList<String>();
		if(horizonLine != null){
			int ind = horizonLine.indexOf("HORIZONS:");
			String [] horizons = null;
			if(ind != -1){
				horizonLine = horizonLine.substring("HORIZONS:".length());
				horizons = horizonLine.trim().split(" +");
			}
			if(horizons != null){
				for(String s : horizons)
					horizonList.add(s);
			}
		}
		List<Object> infoList = new ArrayList<Object>();
		infoList.add(map);
		infoList.add(horizonList);
		return infoList;
	}

	/** Get Messaging Manager of WaveletDecomp component.
	 *  @return  MessagingManager
	 */
	public MessagingManager getMessagingMgr(){
		return messagingMgr;
	}

	/** Invoke File Chooser Service
	 * @param list Passed through to fileChooser from caller
	 *
	 */
	public void callFileChooser(ArrayList list){
		if(list == null)
			return;
		IComponentDescriptor fileChooser = null;
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			logger.warning("Respone to get file chooser service returning null due to timed out");
			return;
		}
		else if(MsgUtils.isResponseAbnormal(response)) {
			logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
			return;
		}
		else
			fileChooser = (ComponentDescriptor)MsgUtils.getMsgContent(response);

		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,fileChooser,QIWConstants.ARRAYLIST_TYPE,list);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,fileChooser);
		return;
	}



	/**
	 * Create a directory of a given path
	 * @param filePath as directory name
	 * @return yes, no or error
	 */
	public String mkDir(String filePath){
		ArrayList params = new ArrayList();
		params.add(messagingMgr.getLocationPref());
		params.add(filePath);
		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CREATE_DIRECTORY_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId,1000);
		if(response != null && !MsgUtils.isResponseAbnormal(response))
			return (String)MsgUtils.getMsgContent(response);
		else if(response == null){
			JOptionPane.showMessageDialog(gui,"Timed out problem in running CREATE_DIRECTORY_CMD.",
					"IO Error",JOptionPane.WARNING_MESSAGE);
			return "error";
		}else{
			JOptionPane.showMessageDialog(gui,"Error in running CREATE_DIRECTORY_CMD. " + (String)MsgUtils.getMsgContent(response),
					"IO Error",JOptionPane.WARNING_MESSAGE);
			return "error";
		}
	}


	/**
	 * Check to see if a given directory exists in the file system
	 * @param filePath file path to be checked
	 * @return yes, no, or error
	 */
	public String checkDirExist(String filePath){
		ArrayList params = new ArrayList();
		params.add(messagingMgr.getLocationPref());
		params.add(filePath);
		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CHECK_FILE_EXIST_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
		String temp = "";
		if(resp != null && !MsgUtils.isResponseAbnormal(resp)){
			temp = (String)resp.getContent();
		}else if(resp == null){
			JOptionPane.showMessageDialog(gui,"Timed out problem in running CHECK_FILE_EXIST_CMD.",
					"IO Error",JOptionPane.WARNING_MESSAGE);
			temp = "error";
		}else{
			JOptionPane.showMessageDialog(gui,"Error in running CHECK_FILE_EXIST_CMD. " + (String)MsgUtils.getMsgContent(resp),
					"IO Error",JOptionPane.WARNING_MESSAGE);
			temp = "error";
		}
		return temp;
	}


	/**
	 * call the state manager to store the state
	 */
	public void saveState(){
		IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,
				QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
	}

    /**
     * call the state manager to store the state
     */
    public void saveStateAsClone(){
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

	/**
	 * call the state manager to store the state then quit the component
	 */
	public void saveStateThenQuit(){
            IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_THEN_QUIT_CMD,stMgrDesc,
				QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
	}

	/** Launch the Wavelet Decomposition plugin:
	 *  <ul>
	 *  <li>Start up the plugin thread which will initialize the plugin.</li>
	 *  </ul>
	 * <p>
	 * NOTE: Each thread's init() must finish before the next thread is
	 * started. This is accomplished by monitoring the SYNC_LOCK object.
	 */
	public static void main(String[] args) {
		QiDataConverterPlugin pluginInstance = new QiDataConverterPlugin();
		//CID has to be passed in since no way to get it back out
		String cid = args[0];
		// use the CID as the name of the thread
		pluginThread = new Thread(pluginInstance, cid);
		pluginThread.start();
		long threadId = pluginThread.getId();
		logger.info("qiDataConverter Thread-"+Long.toString(threadId)+" started");
		// When the plugin's init() is finished, it will release the lock

		// wait until the plugin's init() has finished
		QIWConstants.SYNC_LOCK.lock();
		QIWConstants.SYNC_LOCK.unlock();
		logger.info("Qi Data Conversion main finished");
	}


}


