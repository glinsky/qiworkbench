/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiDataConverter;

import javax.swing.JButton;
import javax.swing.JTextArea;

import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.JobMonitor;

public class StatusThread implements Runnable{
	private JobManager manager;
	private JobMonitor monitor;
	private JButton run;
	private JButton stop;
	private JTextArea status;
	private boolean continueCheck = true;
	public StatusThread (JobManager manager, JobMonitor monitor, JButton run, JButton stop, JTextArea status){
		this.monitor = monitor;
		this.manager = manager;
		this.run = run;
		this.stop = stop;
		this.status = status;
	}
	
	public void setContinueCheck(boolean check){
		continueCheck = check;
	}
	
	public void run(){
		int time = 1000*10;
		if(monitor == null){
			return;
		}
		while(continueCheck && monitor.isJobRunning()){
			updateStatus(monitor,manager,run,stop,status);
			try{
				Thread.sleep(time);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		return;
	}
	
	private void updateStatus(final JobMonitor monitor, JobManager manager, final javax.swing.JButton run,final javax.swing.JButton stop, final javax.swing.JTextArea area){
		if(monitor == null || manager == null)
			return;
		monitor.updateStatus2(manager);
		Runnable updateGUI = new Runnable() {
			public void run() {
				if (monitor.isJobEnded()){
					run.setEnabled(true);
					stop.setEnabled(false);
				}else{
					run.setEnabled(false);
					stop.setEnabled(true);
				}

				area.setText(monitor.fetchStdoutText());
			}
		};
		javax.swing.SwingUtilities.invokeLater(updateGUI);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
