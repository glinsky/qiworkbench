/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiDataConverter;

/**
 * Performs reading and writing on a given byte array. Designed
 *  for conversions to and from various formats.
 */
public class EbcdicTranslater{
   private static /*   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F */
           char[] e={'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\r','\0','\0'
          /* 1x */  ,'\0','\0','\0','\0','\0','\n','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'
          /* 2x */  ,'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'
          /* 3x */  ,'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'
          /* 4x */  , ' ','\0','\0','\0','\0','\0','\0','\0','\0','\0', '?', '.', '<', '(', '+', '|'
          /* 5x */  , '&','\0','\0','\0','\0','\0','\0','\0','\0','\0', '!', '$', '*', ')', ';', '~'
          /* 6x */  , '-', '/','\0','\0','\0','\0','\0','\0','\0','\0','\0', ',', '%', '_', '>', '?'
          /* 7x */  ,'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0', ':', '#', '@','\'', '=', '"'
          /* 8x */  ,'\0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i','\0','\0','\0','\0','\0','\0'
          /* 9x */  ,'\0', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r','\0','\0','\0','\0','\0','\0'
          /* Ax */  ,'\0','\0', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z','\0','\0','\0','\0','\0','\0'
          /* Bx */  ,'\0','\0','\0','\0','\0','\0','\0','\0','\0', '`','\0','\0','\0','\0','\0','\0'
          /* Cx */  ,'\0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I','\0','\0','\0','\0','\0','\0'
          /* Dx */  ,'\0', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R','\0','\0','\0','\0','\0','\0'
          /* Ex */  ,'\\','\0', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','\0','\0','\0','\0','\0','\0'
          /* Fx */  , '0', '1', '2', '3', '4', '5', '6', '7', '8', '9','\0','\0','\0','\0','\0','\0'};


/**
 * Translates EBCDIC byte <code>ebcdic</code> to unicode character.
 **/
   public static char ebcdicToChar(byte ebcdic){
      int i = (int)ebcdic;
      if (i<0) i+=256;
      return e[i];
   }

/**
 * Translates EBCDIC byte array <code>ebcdic</code> to <code>String</code>.
 **/
   public static String ebcdicToString(byte[] ebcdic){
      char[] c = new char[ebcdic.length];
      for(int j=0;j<ebcdic.length;j++)c[j] = ebcdicToChar(ebcdic[j]);
      return new String(c);
   }

/**Translates unicode character <code>c</code> to EBCDIC byte.
 * Unsupported characters become <code>null</code>.
 **/
   public static byte charToEbcdic(char c){
      int b=0;
      for(b=0;b<256;b++) if(c == e[b]) return (byte)b;
      return 0;
   }

/**
 * Translates <code>s</code> to EBCDIC byte array.
 *  Unsupported characters become <code>null</code>.
 **/
   public static byte[] stringToEbcdic(String s){
      byte[] ebcdic = new byte[s.length()];
      for(int i=0;i<s.length();i++){
         int b=0;
         char c = s.charAt(i);
         ebcdic[i] = 0x6f;
         for(b=0;b<256;b++){
            if(c == e[b]){
               ebcdic[i] = (byte)b;
               break;
            }
         }
      }
      return ebcdic;
   }

/**Testing only.*/
   public static void main(String[] args){
      for(int i=0;i<256;i++){
         byte b;
         if(i<128) b= (byte)i; else b = (byte)(i-256);
         char c = EbcdicTranslater.ebcdicToChar(b);
         if(c=='\0'){
            System.out.println(i+": "+b+": Invalid EBCDIC");
         }else{
            System.out.print(i+": "+b+":"+c);
            byte bn = EbcdicTranslater.charToEbcdic(c);
            if(bn!=b) {
               System.out.println("Invalid back translation");
            }else{
               System.out.println();
            }
         }
      }
   }
}