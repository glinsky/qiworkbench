/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/


package com.bhpb.qiDataConverter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/*
 * HorizonSelectDialog.java
 *
 * Created on September 12, 2006, 5:13 PM
 */

/**
 *
 * @author  l
 */
public class HorizonSelectDialog extends java.awt.Dialog {
	private static Logger logger = Logger.getLogger(HorizonSelectDialog.class
			.getName());
	private java.awt.Component parent;
	//private LandmarkProjectHandler projectHandler;
	private String [] horizons;
	//private int projectType = 3;
    /** Creates new form ProjectSelectDialog */
    public HorizonSelectDialog(java.awt.Component parent, boolean modal, String [] horizons) {
    	super(JOptionPane.getFrameForComponent(parent), modal);
    	this.parent = parent;
    	this.setTitle("Select A Horizon Dataset");
    	//projectHandler = new  LandmarkProjectHandler();
        //by default get all projects
        //String sPrjType = parent.getLandmarkProjectType();

        //if(sPrjType.equals("2D"))
        //	projectType = 2;
        //else if(sPrjType.equals("3D"))
        //	projectType = 3;

        //horizons = projectHandler.getHorizonList(parent.getSelectedLandmarkProject(), projectType);
        this.horizons = horizons.clone();
        initComponents();
        this.setLocationRelativeTo(parent);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">
    private void initComponents() {
        jScrollPane1 = new javax.swing.JScrollPane();
        horizonList = new javax.swing.JList();
        horizonListLabel = new javax.swing.JLabel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 204));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        horizonList.setBackground(new java.awt.Color(204, 255, 255));
        horizonList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        horizonList.setModel(new javax.swing.AbstractListModel() {
            public int getSize() { return horizons.length; }
            public Object getElementAt(int i) { return horizons[i]; }
        });

        horizonList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                okButton.setEnabled(true);
            }
    	});
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
            	if (e.getClickCount() == 2) {
            		okButton.doClick();
            	}
            }
        };
        horizonList.addMouseListener(mouseListener);
        horizonList.addKeyListener(new ListSearcher(horizonList,okButton));
        jScrollPane1.setViewportView(horizonList);

        horizonListLabel.setBackground(new java.awt.Color(255, 255, 255));
        horizonListLabel.setText("Horizon List:");

        okButton.setText("OK");
        okButton.setEnabled(false);
        okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] selectedHorizons = horizonList.getSelectedValues();
				if(selectedHorizons == null || selectedHorizons.length == 0)
					return;
				if(parent instanceof Lm3dHor2BhpsuPanel)
					((Lm3dHor2BhpsuPanel)parent).setSelectedHorizons(selectedHorizons);
				closeDialog(e);
			}
		});
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
			.add(layout.createSequentialGroup()
				.addContainerGap()
				.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
					.add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
					.add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
						.add(okButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(cancelButton))
					.add(horizonListLabel))
				.addContainerGap())
		);

		layout.linkSize(new java.awt.Component[] {cancelButton, okButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

		layout.setVerticalGroup(
			layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
			.add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
				.addContainerGap()
				.add(horizonListLabel)
				.add(9, 9, 9)
				.add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 15, Short.MAX_VALUE)
				.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
					.add(cancelButton)
					.add(okButton))
				.addContainerGap())
		);
		pack();
    }// </editor-fold>

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	closeDialog(evt);
    }

    private void closeDialog(java.awt.event.ActionEvent evt) {
        setVisible(false);
        dispose();
    }
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {
        setVisible(false);
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	String[] strings = {"item 1","item 2"};
                new HorizonSelectDialog(null, true,strings).setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify
    private javax.swing.JButton cancelButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton okButton;
    private javax.swing.JList horizonList;
    private javax.swing.JLabel horizonListLabel;
    // End of variables declaration

}
