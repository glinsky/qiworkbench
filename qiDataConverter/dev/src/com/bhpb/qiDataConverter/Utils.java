/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/


package com.bhpb.qiDataConverter;

import java.awt.Component;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.JobMonitor;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.services.landmark.LandmarkServices;

public class Utils {
	private static Logger logger = Logger.getLogger(Utils.class
            .getName());
	/**
	 * Validate Component UI line and trace text fields (from,to,inc) based on the default range information
	 * @param comp base UI component
	 * @param from a text field containing minimum range value 
	 * @param to a text field containing maximum range value
	 * @param inc a text field containing increment value
	 * @param def[] an array of integer containing default (min, max,inc) range value
	 * @return true of false
	 */
    public static boolean validateLineTraceTextField(Component comp, JTextField from, JTextField to, JTextField inc, int[] def) {
        String str = from.getText().trim();

		if(str.length() == 0){
			JOptionPane.showMessageDialog(comp, "The \"from\" field value is empty.",
					"Invalid data entry", JOptionPane.WARNING_MESSAGE);
			from.requestFocus();
			return false;
		}
        int fromVal = -1;
        try {
            fromVal = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "The \"from\" field value must be an integer value. Reset to default \"from\" value (" + def[0] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);

            from.requestFocus();
            from.setText(String.valueOf(def[0]));
            return false;
        }

        str = to.getText().trim();
		if(str.length() == 0){
			JOptionPane.showMessageDialog(comp, "The \"to\" field value is empty.",
					"Invalid data entry", JOptionPane.WARNING_MESSAGE);
			from.requestFocus();
			return false;
		}

        int toVal = -1;
        try {
            toVal = Integer.valueOf(str).intValue();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "The \"to\" field value must be an integer value. Reset to default \"to\" value (" + def[1] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);

            to.requestFocus();
            to.setText(String.valueOf(def[1]));
            return false;
        }

        str = inc.getText().trim();
		if(str.length() == 0){
			JOptionPane.showMessageDialog(comp, "The \"increment\" field value is empty.",
					"Invalid data entry", JOptionPane.WARNING_MESSAGE);
			from.requestFocus();
			return false;
		}
        int by = -1;
        try {
            by = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(comp, "The \"increment\" field value must be an integer value. Reset to default increment value (" + def[2] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);

            inc.requestFocus();
            inc.setText(String.valueOf(def[2]));
            return false;
        }
        if(by == 0){
        	JOptionPane.showMessageDialog(comp, "The \"increment\" field value must not be 0. Reset to the default value (" + def[2] + ").",
                    "Invalid data entry", JOptionPane.WARNING_MESSAGE);

            inc.requestFocus();
            inc.setText(String.valueOf(def[2]));
            return false;
        }

        if(def[2] > 0){
            if(by < def[2]){
                JOptionPane.showMessageDialog(comp, "The \"increment\" field value must not less than the defaul value (" + def[2] + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);

                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        } else if(def[2] < 0){
            if(by > def[2]){
                JOptionPane.showMessageDialog(comp, "The \"increment\" field value must not greater than the defaul value (" + def[2] + "). Reset to default.",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);

                inc.requestFocus();
                inc.setText(String.valueOf(def[2]));
                return false;
            }
        }

        if(def[2] != 0){
            int rem = by % def[2];
            if(rem != 0){
                JOptionPane.showMessageDialog(comp, "The \"increment\" field value must be divisible by the default value (" + def[2] +  "). Reset to a valid one",
                        "Invalid data entry", JOptionPane.WARNING_MESSAGE);

                by = by - rem;
                inc.requestFocus();
                inc.setText(String.valueOf(by));
                return false;
            }
        }
        if(def[2] > 0){
            if (!(fromVal >= def[0])) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value must not be less than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }

            int rem = (fromVal-def[0]) % def[2];
        	if(rem != 0){
               	
               	JOptionPane.showMessageDialog(comp,
                      "The difference of " + fromVal + " (from) and " + def[0] + " (default \"from\" value) must be divisible by "
                           + def[2] + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);

                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
        	}
            if (toVal > def[1]) {
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" field value must not be greater than the default \"to\" value ("
                                + def[1] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            if (!(toVal >= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value (" + fromVal + ") must not be greater than the value of \"to\" field. Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }
            rem = (toVal - fromVal) % by;
            if(rem != 0){
               	JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                toVal = toVal - rem;
                to.setText(String.valueOf(toVal));
                return false;
            }
        } else {
            if (fromVal > def[0]) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value must not be greater than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }
           	int rem = (fromVal - def[0]) % def[2];
           	if(rem != 0){
           		
           		JOptionPane.showMessageDialog(comp,
                  "The difference of \"from\" field value(" + fromVal + ") and default \"from\" field value(" + def[0] + ") must be divisible by the default increment ("
                       + def[2] + "). Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);

           		fromVal = fromVal - rem;
           		from.requestFocus();
           		from.setText(String.valueOf(fromVal));
           		return false;
           	}

            if (!(toVal <= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The\"to\" field value " +  toVal + " must be less than or equal to the value of \"from\" field " + fromVal + ". Reset to default \"to\" field."
                         + fromVal, "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }

            rem = (toVal - fromVal) % by;
            if(rem != 0){
               	
               	JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by the increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);

                toVal = toVal - rem;
                to.requestFocus();
                to.setText(String.valueOf(toVal));
                return false;
            }

            if(!(toVal >= def[1])){
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" field value must be greater than or equals to the value of \"from\" field. Reset to default \"to\" value ("
                          + def[1] + ")", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
        }

        if(def[2] != 0){
        	int rem = (fromVal-def[0]) % def[2];
        	if(rem != 0){
               	
               	JOptionPane.showMessageDialog(comp,
                      "The difference of " + fromVal + "(\"from\" field) and " + def[0] + "(default \"from\" field) must be divisible by "
                           + def[2] + "(default \"increment\" value)" , "Invalid data entry",JOptionPane.WARNING_MESSAGE);

                fromVal = fromVal - rem;
                from.requestFocus();
                from.setText(String.valueOf(fromVal));
                return false;
        	}
        }

        if(by != 0){
            int rem = (toVal - fromVal) % by;
            if(rem != 0){
             JOptionPane.showMessageDialog(comp,"The difference of " + fromVal + "(\"from\" field) and " + toVal + "(\"to\" field) must be divisible by "
                           + by + "(\"increment\" field)",
                     "Invalid data entry",JOptionPane.WARNING_MESSAGE);

             to.requestFocus();
             if(rem > 0){
                 int next = toVal - rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }else{
                 int next = toVal + rem;
                 if(next == fromVal)
                     next += by;
                 to.setText(String.valueOf(next));
             }
             return false;
            }
        }

        return true;
    }

    /*
     * Validate the range fields. This method function similiar to the function validateLineTraceTextField in the Utils.java
     * execpt that it is less restrictive as it does not validate increment fields. Those values are given as suggested values 
     * to the users based on searching and estimation.
     * @param comp   the parent GUI where this function is acted upon
     * @param from   a text field holding range minimum 
     * @param to   a text field holding range maximum
     * @param inc   a text field holding increment value
     * @param def   an array of values holding default values for the from, to, and inc
     * @return  boolean success or failure
     */
    public static boolean validateLineTraceTextField2(Component comp, JTextField from, JTextField to, JTextField inc, int[] def) {
    	String str = from.getText().trim();
	
    	if(str.length() == 0){
    		JOptionPane.showMessageDialog(comp, "The \"from\" field value is empty.",
				"Invalid data entry", JOptionPane.WARNING_MESSAGE);
    		from.requestFocus();
    		return false;
    	}
    	int fromVal = -1;
    	try {
    		fromVal = Integer.parseInt(str);
    	} catch (NumberFormatException ex) {
    		JOptionPane.showMessageDialog(comp, "The \"from\" field value must be an integer value. Reset to default \"from\" value (" + def[0] + ").",
	             "Invalid data entry", JOptionPane.WARNING_MESSAGE);
	
    		from.requestFocus();
    		from.setText(String.valueOf(def[0]));
    		return false;
    	}
	
    	str = to.getText().trim();
    	if(str.length() == 0){
    		JOptionPane.showMessageDialog(comp, "The \"to\" field value is empty.",
				"Invalid data entry", JOptionPane.WARNING_MESSAGE);
    		from.requestFocus();
    		return false;
    	}
	
    	int toVal = -1;
    	try {
    		toVal = Integer.valueOf(str).intValue();
    	} catch (NumberFormatException ex) {
    		JOptionPane.showMessageDialog(comp, "The \"to\" field value must be an integer value. Reset to default \"to\" value (" + def[1] + ").",
	             "Invalid data entry", JOptionPane.WARNING_MESSAGE);
	
    		to.requestFocus();
    		to.setText(String.valueOf(def[1]));
    		return false;
    	}
	 
    	str = inc.getText().trim();
    	if(str.length() == 0){
    		JOptionPane.showMessageDialog(comp, "The \"increment\" field value is empty.",
				"Invalid data entry", JOptionPane.WARNING_MESSAGE);
    		from.requestFocus();
    		return false;
    	}
    	int by = -1;
    	try {
    		by = Integer.parseInt(str);
    	} catch (NumberFormatException ex) {
    		JOptionPane.showMessageDialog(comp, "The \"increment\" field value must be an integer value. Reset to default increment value (" + def[2] + ").",
	             "Invalid data entry", JOptionPane.WARNING_MESSAGE);
	
    		inc.requestFocus();
    		inc.setText(String.valueOf(def[2]));
    		return false;
    	}
    	if(by == 0){
    		JOptionPane.showMessageDialog(comp, "The \"increment\" field value must not be 0. Reset to the default value (" + def[2] + ").",
	             "Invalid data entry", JOptionPane.WARNING_MESSAGE);
	
    		inc.requestFocus();
    		inc.setText(String.valueOf(def[2]));
    		return false;
    	}
    	
    	if(def[2] > 0){
            if (!(fromVal >= def[0])) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value must not be less than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }

            if (toVal > def[1]) {
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" field value must not be greater than the default \"to\" value ("
                                + def[1] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
            if (toVal < fromVal) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value (" + fromVal + ") must not be greater than the value of \"to\" field. Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);
                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }
            
            int rem = (toVal - fromVal) % by;
            if(rem != 0){
               	JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                toVal = toVal - rem;
                to.setText(String.valueOf(toVal));
                return false;
            }
        } else {
            if (fromVal > def[0]) {
                JOptionPane.showMessageDialog(comp,
                        "The \"from\" field value must not be greater than the default \"from\" value ("
                         + def[0] + "). Reset to default.", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                from.requestFocus();
                from.setText(String.valueOf(def[0]));
                return false;
            }
           	
            if (!(toVal <= fromVal)) {
                JOptionPane.showMessageDialog(comp,
                        "The\"to\" field value " +  toVal + " must be less than or equal to the value of \"from\" field " + fromVal + ". Reset to default \"to\" field."
                         + fromVal, "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }

            int rem = (toVal - fromVal) % by;
            if(rem != 0){
               	
               	JOptionPane.showMessageDialog(comp,
                      "The difference of " + toVal + " (\"to\" field) and " + fromVal + " (\"from\" field) must be divisible by the increment "
                           + by + ". Reset to a valid one.", "Invalid data entry",JOptionPane.WARNING_MESSAGE);

                toVal = toVal - rem;
                to.requestFocus();
                to.setText(String.valueOf(toVal));
                return false;
            }

            if(!(toVal >= def[1])){
                JOptionPane.showMessageDialog(comp,
                        "The \"to\" field value must be greater than or equals to the value of \"from\" field. Reset to default \"to\" value ("
                          + def[1] + ")", "Invalid data entry",
                        JOptionPane.WARNING_MESSAGE);

                to.requestFocus();
                to.setText(String.valueOf(def[1]));
                return false;
            }
        }

    	return true;
    }
	/**
	 * Create the base name of the script file.
	 * @param rootRelPath Relative path of an output file the end of which is the base name.
	 * @return Base name of the script file
	 */
	public static String createBaseName(String rootRelPath, String filesep) {
		String baseName = rootRelPath;

		int ind = baseName.lastIndexOf(filesep);
		if (ind != -1) {
			baseName = baseName.substring(ind+1);
		}

		return baseName;
	}

	/**
	 * Create a unique name for the generated script from a base name.
	 */
	public static String createScriptPrefix(String baseName) {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy_MM_dd_HH_mm_ss_SSS");
		String scriptPrefix = baseName + "_" + formatter.format(ts);

		return scriptPrefix;
	}   

	private static boolean validateLandmarkEnvironment(Component comp){
		String os = System.getProperty("os.name");
		if(os.startsWith("Windows")){
			JOptionPane.showMessageDialog(comp, "Landmark services are not currently supported in Windows environment.",
					"Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		Map envmap = System.getenv();

		if(!envmap.containsKey("OWHOME")){
			JOptionPane.showMessageDialog(comp, "Environment variable OWHOME must be set prior to using Landmark services.",
					"Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		if(!envmap.containsKey("ORACLE_HOME")){
			JOptionPane.showMessageDialog(comp, "Environment variable ORACLE_HOME must be set prior to using Landmark services.",
					"Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		if(!envmap.containsKey("OW_PMPATH")){
			JOptionPane.showMessageDialog(comp, "Environment variable OW_PMPATH must be set prior to using Landmark services.",
					"Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		if(!envmap.containsKey("LM_LICENSE_FILE")){
			JOptionPane.showMessageDialog(comp, "Environment variable LM_LICENSE_FILE must be set prior to using Landmark services.",
					"Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		if(!envmap.containsKey("LD_LIBRARY_PATH")){
			JOptionPane.showMessageDialog(comp, "Environment variable LD_LIBRARY_PATH must be set prior to using Landmark services.",
					"Environment Variable Not Set", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	public static LandmarkServices getLandmarkServices(Component comp){
		if(!validateLandmarkEnvironment(comp))
			return null;
		try{
			return LandmarkServices.getInstance();
		}catch(UnsatisfiedLinkError e){
			JOptionPane.showMessageDialog(comp, "Unexpected error occurred while getting Landmark services: " + e.getMessage(), "Unexpected Error.", JOptionPane.WARNING_MESSAGE);
			return null;
		}catch(NoClassDefFoundError e){
			JOptionPane.showMessageDialog(comp, "Unexpected error occurred while getting Landmark services: No class definition found.", "Unexpected Error.", JOptionPane.WARNING_MESSAGE);
			return null;
		}catch(Exception e){
			JOptionPane.showMessageDialog(comp, "Unexpected error occurred while getting Landmark services: " + e.getMessage(), "Unexpected Error.", JOptionPane.WARNING_MESSAGE);
			return null;
		}
	}
	/**
	 * saveScriptToFile saves a script to a selected file
	 * @param manager MessagingManager	    
	 * @param script ArrayList containing script
	 * @param path is /path/file to write.
	 *   It is inserted as the first element of the ArrayList for IO Services
	 * @return true if file written; otherwise, false.
	 */
	public static String saveScriptToFile(MessagingManager manager, List<String> script, String path) {
		//Add the path of the file want to write to to the head of the list
		script.add(0, path);
		//Add the location of the IO to the head of the list
		script.add(0,manager.getLocationPref());
		String msgID = manager.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_WRITE_CMD,
				QIWConstants.ARRAYLIST_TYPE,script,true);
		// wait for the response. Note: There can only be 1 response since there
		// is a separate IO service performing the IO which is not performing
		// parallel IO.
		IQiWorkbenchMsg response = manager.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return "Messaging timed out.";
		}else if (MsgUtils.isResponseAbnormal(response)){
			System.out.println("IO error: " +(String)MsgUtils.getMsgContent(response));
			return (String)MsgUtils.getMsgContent(response);
		}else
			return "";
	}

	public static void runScript(String scriptNamePrefix, final JobMonitor monitor, final JobManager manager, final JButton run, final JButton stop, final JTextArea status, JTextArea summary) {
		String logFile = scriptNamePrefix + ".out";
		String scriptName = scriptNamePrefix + ".sh";
		ArrayList params = new ArrayList();
		params.add("cd " + monitor.getScriptDir() + "; chmod ug+x "
				+ scriptName + "; ./" + scriptName + " > " + logFile + " 2>&1");
		//submit the script as a possibly long running job
		params.add("false");
		manager.submitJob(params);
		monitor.setJobStatusToRunning();
		status.setText("");

		String text = scriptName + "\n";
		text += logFile + "\n";
		text += "have been created in " + monitor.getScriptDir();
		summary.setText(text);
		run.setEnabled(false);
		stop.setEnabled(true);

		//TODO will be used upon request based on menu input from user.
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				int time = 1000*10;
				while(monitor.isJobRunning()){
					if(!updateStatus(monitor,manager,run,stop,status))
						return;
					try{
						Thread.sleep(time);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
				}
				return;
			}
		};
		new Thread(heavyRunnable).start();

	}	  

	public static void runScript2(String scriptNamePrefix, final JobMonitor monitor, final JobManager manager, final JButton run, final JButton stop, final JTextArea status, JTextArea summary) {
		String logFile = scriptNamePrefix + ".out";
		String scriptName = scriptNamePrefix + ".sh";
		ArrayList params = new ArrayList();
		params.add("cd " + monitor.getScriptDir() + "; chmod u+x "
				+ scriptName + "; ./" + scriptName + " > " + logFile + " 2>&1");
		params.add("false");
		//submit the script as a possibly long running job
		manager.submitJob(params);
		monitor.setJobStatusToRunning();
		status.setText("");

		String text = scriptName + "\n";
		text += logFile + "\n";
		text += "have been created in " + monitor.getScriptDir();
		summary.setText(text);
		run.setEnabled(false);
		stop.setEnabled(true);
	}	  
	
	
	public static String getFilePrefix(String filePath, String filesep){
		if(filePath == null || filePath.trim().length() == 0)
			return "";
		String filePrefix = filePath.substring(filePath.lastIndexOf(filesep) + 1);
		int index = filePrefix.lastIndexOf(".");
		if(index != -1)
			filePrefix = filePrefix.substring(0,index);
		return filePrefix;
	}

	private static boolean updateStatus(final JobMonitor monitor, JobManager manager, final javax.swing.JButton run,final javax.swing.JButton stop, final javax.swing.JTextArea area){
		if(monitor == null || manager == null)
			return false;
		String status = monitor.updateStatus2(manager);
		if(status.equals(JobMonitor.JOB_UNKNOWN_STATUS)){
			JOptionPane.showMessageDialog(null, "Unexpected error in getting process ids.", "Unexpected Error.", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		Runnable updateGUI = new Runnable() {
			public void run() {
				if (monitor.isJobEnded()){
					run.setEnabled(true);
					stop.setEnabled(false);
				}else{
					run.setEnabled(false);
					stop.setEnabled(true);
				}

				area.setText(monitor.fetchStdoutText());
			}
		};
		javax.swing.SwingUtilities.invokeLater(updateGUI);
		return true;
	}

	/** 
	 * Generate common header script code such as QISPACE, QIPROJECT .. etc.
	 */
	public static List<String> genHeadScripts(QiProjectDescriptor qiProjectDesc, String filesep){
		List<String> script = new ArrayList<String>();
		if(qiProjectDesc == null)
			return script; 
		// source ~/.bashrc
		// make sure the binary of SU and BHP-SU on PATH
		String command = "source ~/.bashrc";
		script.add(command);

		// alias rm
		// silently remove files so script can run standalone
		command = "alias rm=\"rm -f\"";
		script.add(command);

		// alias cp
		command = "alias cp=cp";
		script.add(command);
		script.add("#set -x");
		script.add("");
		script.add("# Parameters specified in the qiProject");

		// export QISPACE
		String qiSpace = QiProjectDescUtils.getQiSpace(qiProjectDesc);
		script.add("# qiSpace containing one or more projects");
		command = "export QISPACE=\"" + qiSpace + "\"";
		script.add(command);

		// export QIPROJECT
		String qiProject = QiProjectDescUtils.getQiProjectReloc(qiProjectDesc);
		script.add("# location of qiProject relative to $QISPACE");
		command = "export QIPROJECT=\"" + qiProject + "\"";
		script.add(command);

		// export QIDATASETS
		String qiDatasets = QiProjectDescUtils.getDatasetsReloc(qiProjectDesc);
		script.add("# location of datasets relative to $QISPACE/$QIPROJECT");
		command = "export QIDATASETS=\"" + qiDatasets + "\"";
		script.add(command);
		// export QISEISMICS
		ArrayList<String> seismicDirs = QiProjectDescUtils.getSeismicDirs(qiProjectDesc);
		StringBuffer qiSeismics = new StringBuffer();
		//Note: there is always at least 1 seismic directory
		String projectPath = QiProjectDescUtils.getProjectPath(qiProjectDesc);
		for(int i = 0; seismicDirs != null && i < seismicDirs.size(); i++){
			qiSeismics.append(projectPath + filesep + seismicDirs.get(i));
			if(i < seismicDirs.size() -1){
				qiSeismics.append("\n");
			}
		}
		script.add("# list of seismic directories separated by a line separator (\\n)");
		command = "export QISEISMICS=\"" +  qiSeismics.toString() + "\"";

		script.add(command);
		return script;
	}

	/**
	 * Validate Component UI time text fields (from,to) based on the default range information
	 * @param comp base UI component
	 * @param from a text field containing minimum range value 
	 * @param to a text field containing maximum range value
	 * @param inc a text field containing increment value
	 * @param def[] an array of integer containing default (min, max,inc) range value
	 * @return true of false
	 */
	public static boolean validateTimeTextField(Component comp,JTextField from, JTextField to, int[] def) {
		String str = from.getText().trim();
		if(str.length() == 0){
			JOptionPane.showMessageDialog(comp, "The \"from\" field value of the time range is empty.",
					"Invalid data entry", JOptionPane.WARNING_MESSAGE);
			from.requestFocus();
			return false;
		}
		int fromVal = -1;
		try {
			fromVal = Integer.parseInt(str);
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(comp, "The \"from\" field value of the time range must be an integer value. Reset to default value " + def[0],
					"Invalid data entry", JOptionPane.WARNING_MESSAGE);
			from.requestFocus();
			from.setText(String.valueOf(def[0]));
			return false;
		}
		//if(fromVal < 0){
		//	JOptionPane.showMessageDialog(comp, "The \"from\" field value of the time range must not be a negative numeric value.",
		//			"Invalid data entry", JOptionPane.WARNING_MESSAGE);
		//	from.requestFocus();
		//	from.setText(String.valueOf(def[0]));
		//	return false;
		//}
		
		str = to.getText().trim();
		if(str.length() == 0){
			JOptionPane.showMessageDialog(comp, "The \"to\" field value of the time range is empty.",
					"Invalid data entry", JOptionPane.WARNING_MESSAGE);
			from.requestFocus();
			return false;
		}

		
		int toVal = -1;
		try {
			toVal = Integer.valueOf(str).intValue();
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(comp, "The \"to\" field value of the time range must be an integer value. Reset to default value " + def[1],
					"Invalid data entry", JOptionPane.WARNING_MESSAGE);
			to.requestFocus();
			to.setText(String.valueOf(def[1]));
			return false;
		}
		//if(toVal < 0){
		//	JOptionPane.showMessageDialog(comp, "The \"to\" field value of the time range must not be a negative numeric value.",
		//			"Invalid data entry", JOptionPane.WARNING_MESSAGE);
		//	to.requestFocus();
		//	to.setText(String.valueOf(def[1]));
		//	return false;
		//}
		if (!(fromVal >= def[0])) {
			JOptionPane.showMessageDialog(comp,
					"The \"from\" field value of the time range must not be less than the default minimum time value "
					+ def[0] + ". Reset to default value.", "Invalid data entry",
					JOptionPane.WARNING_MESSAGE);
			from.requestFocus();
			from.setText(String.valueOf(def[0]));
			return false;
		}
		if (toVal > def[1]) {
			JOptionPane.showMessageDialog(comp,
					"The \"to\" time field value " + toVal + " of the time range must not be greater than the default maximum time value " + def[1] + ". Reset to default.", "Invalid data entry",
					JOptionPane.WARNING_MESSAGE);
			to.requestFocus();
			to.setText(String.valueOf(def[1]));
			return false;
		}
		if (!(toVal >= fromVal)) {
			JOptionPane.showMessageDialog(comp,
					"The \"to\" field value (" + toVal + ") of the time range must be greater than or equal to the \"from\" field value ("
					+ fromVal + ").", "Invalid data entry",
					JOptionPane.WARNING_MESSAGE);
			to.requestFocus();
			to.setText(String.valueOf(def[1]));
			return false;
		}

		if(def[2] != 0 ){
			int rem = (fromVal-def[0]) % def[2];
			if(rem != 0){
				JOptionPane.showMessageDialog(comp,
						"The difference of the \"from\" field value " + fromVal + " and the default \"from\" value " + def[0] + " of the time range must be divisible by the sample rate " + def[2] + ". Reset to a valid one."
						, "Invalid data entry",
						JOptionPane.WARNING_MESSAGE);
				fromVal = fromVal - rem;
				from.requestFocus();
				from.setText(String.valueOf(fromVal));
				return false;
			}

			rem = (toVal - fromVal) % def[2];
			if(rem != 0){
				JOptionPane.showMessageDialog(comp,"The difference of the \"to\" field value " + toVal + " and the \"from\" field value " + fromVal + " of the time range must be divisible by the sample rate " + def[2] + ". Reset to a valid one." ,
						"Invalid data entry",JOptionPane.WARNING_MESSAGE);
				to.requestFocus();
				int next = toVal - rem;
				to.setText(String.valueOf(next));
				return false;
			}
		}
		return true;
	}
	
	
	
	private static boolean pass = false;
    public static boolean ensureProjectSetup(final QiDataConverterPlugin messagingAgent){
    	if(messagingAgent == null)
    		return false;
    	pass = false;
    	List<String> list = messagingAgent.getUnmadeQiProjDescDirs();
        String projectRoot = QiProjectDescUtils.getProjectPath(messagingAgent.getProjectDescriptor());

        if(list != null && list.size() > 0){
            int returnStatus = QiProjectDescUtils.showQiProjectMetaDataSetupDialog(
                    JOptionPane.getFrameForComponent(messagingAgent.getGUI()), true, list, projectRoot);
            if(returnStatus == JOptionPane.CANCEL_OPTION)
                return false;
            if(list.size() > 0){
                final List<String> flist = list;
                Runnable heavyRunnable = new Runnable(){
                    public void run(){
                        if(messagingAgent.makeQiProjDescDirs(flist))
                            pass = true;
                        else
                            pass = false;
                    }
                };
                Thread thread = new Thread(heavyRunnable);
                thread.start();
                try{
                    thread.join();
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                if(!pass){
                    logger.info("Creating QiProject directory structure not passed.");
                    return false;
                }
                logger.info("Creating QiProject directory structure succeeds.");
            }
        }
        return true;
    }
    
    
    /*
     * valiate the header selection against number of total traces
     * @param toEp   header name  (the values intended to be converted to header ep)
     * @param toCdp  header name  (the values intended to be converted to header cdp)
     * @param totalTraces  the total number of traces keyed by toEp and toCdp
     */
    public synchronized static boolean validateHeaderSelection(Map<String,DataSetHeaderKeyInfo> segyRangeMap, String toEp, String toCdp, int totalTraces){
    	DataSetHeaderKeyInfo ds = segyRangeMap.get(toEp);
    	int incep = -1, inccdp = -1;
    	int epRange = (int)(ds.getMax() - ds.getMin());
    	ds = segyRangeMap.get(toCdp);
    	int cdpRange = (int)(ds.getMax() - ds.getMin());
    	if(epRange == 0)
    		incep = 1;
    	if(cdpRange == 0)
    		inccdp = 1;
    	if(incep == 1 && inccdp == 1)
    		return true;
    	else if(incep == 1 && inccdp == -1){
    		int rem = cdpRange % (totalTraces-1);
    		if(rem == 0)
    			return true;
    		else
    			return false;
    		
    	}else if(inccdp == 1 && incep == -1){
    		int rem = epRange % (totalTraces-1);
    		if(rem == 0)
    			return true;
    		else
    			return false;
    		
    	}else{
    		int min = Math.min(epRange, cdpRange);
    		for(int i = 1; i <= min; i++){
    			if(min == epRange){
    				int d1 = epRange/i + 1;
    				int d2 = totalTraces/d1 - 1;
    				if(d2 == 0)
    					return false;
    				int rem = cdpRange % d2;
    				if(rem == 0)
    					return true;
    			}else if(min == cdpRange){
    				int d1 = cdpRange/i + 1;
    				int d2 = totalTraces/d1 - 1;
    				if(d2 == 0)
    					return false;
    				int rem = epRange % d2;
    				if(rem == 0)
    					return true;
    			}
    		}
    	}
    	return false;
    }    
    /*
     * valiate the header selection against number of total traces
     * @param toEp   header name  (the values intended to be converted to header ep)
     * @param toCdp  header name  (the values intended to be converted to header cdp)
     * @param toCdpt  header name  (the values intended to be converted to header cdpt)
     * @param totalTraces  the total number of traces keyed by toEp,toCdp,and toCdpt
     */    
    public synchronized  static boolean validateHeaderSelection(Map<String,DataSetHeaderKeyInfo> segyRangeMap,String toEp, String toCdp, String toCdpt, int totalTraces){
    	int incep = 0, inccdp = 0, inccdpt = 0; //not valid for now
    	int epRange = -1; //not a valid value for now
    	int cdpRange = -1;//not a valid value for now
    	int cdptRange = -1;//not a valid value for now
    	if(segyRangeMap.containsKey(toEp)){
    		DataSetHeaderKeyInfo ds = segyRangeMap.get(toEp);
    		epRange = (int)(ds.getMax() - ds.getMin());
    	}
    	
    	if(segyRangeMap.containsKey(toCdp)){
    		DataSetHeaderKeyInfo ds = segyRangeMap.get(toCdp);
    		cdpRange = (int)(ds.getMax() - ds.getMin());
    	}
    	
    	if(toCdpt != null && segyRangeMap.containsKey(toCdpt)){
    		DataSetHeaderKeyInfo ds = segyRangeMap.get(toCdpt);
    		cdptRange = (int)(ds.getMax() - ds.getMin());
    	}
    	
    	if(epRange == 0)
    		incep = 1;
    	if(cdpRange == 0)
    		inccdp = 1;
    	if(cdptRange == 0)
    		inccdpt = 1;
    	if(incep == 1 && inccdp == 1 && inccdpt == 1)
    		return true;
    	else if(incep == 1 && inccdp == 0 && inccdpt == 1){
    		int rem = cdpRange % (totalTraces-1);
    		if(rem == 0)
    			return true;
    		else
    			return false;
    		
    	}else if(inccdp == 1 && incep == 0 && inccdpt == 1){
    		int rem = epRange % (totalTraces-1);
    		if(rem == 0)
    			return true;
    		else
    			return false;
    	}else if(inccdp == 1 && incep == 1 && inccdpt == 0){
    		int rem = cdptRange % (totalTraces-1);
    		if(rem == 0)
    			return true;
    		else
    			return false;
    	}else if(inccdp == 0 && incep == 0 && inccdpt == 1){
    		return validateTwoHeaderSelection(totalTraces,epRange,cdpRange);
    	}else if(inccdp == 1 && incep == 0 && inccdpt == 0){
    		return validateTwoHeaderSelection(totalTraces,epRange,cdptRange);
    	}else if(inccdp == 0 && incep == 1 && inccdpt == 0){
    		return validateTwoHeaderSelection(totalTraces,cdpRange,cdptRange);
    		/*int min = Math.min(epRange, cdpRange);
    		for(int i = 1; i <= min; i++){
    			if(min == epRange){
    				int d1 = epRange/i + 1;
    				int d2 = totalTraces/d1 - 1;
    				if(d2 == 0)
    					return false;
    				int rem = cdpRange % d2;
    				if(rem == 0)
    					return true;
    			}else if(min == cdpRange){
    				int d1 = cdpRange/i + 1;
    				int d2 = totalTraces/d1 - 1;
    				if(d2 == 0)
    					return false;
    				int rem = epRange % d2;
    				if(rem == 0)
    					return true;
    			}
    		}
    		*/
    		
    	}else{
    		return validateThreeHeaderSelection(totalTraces, epRange, cdpRange, cdptRange);
    	}
    }
    
    /*
     * validate two given valid header value ranges against number of total traces
     * Nep=Rep/Incep+1, Ncdp=Rcdp/Inccdp + 1, then totalTraces is divisible by either by Nep or Ncdp
     * try every possibe increment value from the lower header range value, compute its possible number of instances (range/increment+1),
     * and get the possible number of header instances for the larger range value
     * if the total traces is divisible by the possible number of header instances then the combination of ranges considered valid.
     */
    private static boolean validateTwoHeaderSelection(int totalTraces, int range1, int range2){
    	if(range1 == -1 || range2 == -1)
    		return false;
    	int min = Math.min(range1, range2);
		for(int i = 1; i <= min; i++){
			if(min == range1){
				int numOfHeader1 = range1/i + 1;
				int numOfHeader2 = totalTraces/numOfHeader1 - 1;
				if(numOfHeader2 == 0)
					return false;
				int rem = range2 % numOfHeader2;
				if(rem == 0)
					return true;
			}else if(min == range2){
				int numOfHeader2 = range2/i + 1;
				int numOfHeader1 = totalTraces/numOfHeader2 - 1;
				if(numOfHeader1 == 0)
					return false;
				int rem = range1 % numOfHeader1;
				if(rem == 0)
					return true;
			}
		}
		return false;
    }

    /*
     * validate three given valid header value ranges against number of total traces
     * Nep=Rep/Incep+1, Ncdp=Rcdp/Inccdp+1, Ncdpt=Rcdpt/Inccdpt+1 then totalTraces should be divisible by either by Nep or Ncdp or Ncdpt
     * try every possibe increment value from the lower header range value, compute its possible number of instances (range/increment+1),
     * and get the possible number of header instances for the larger range value
     * if the total traces is divisible by the possible number of header instances then the combination of ranges considered valid.
     */
    private static boolean validateThreeHeaderSelection(int totalTraces, int range1, int range2, int range3){
    	if(range1 == -1 || range2 == -1 || range3 == -1)
    		return false;
    	int min = Math.min(range1, range2);
    	min = Math.min(min, range3);
		for(int i = 1; i <= min; i++){
			if(min == range1){
				int numOfHeader1 = range1/i + 1;
				int totalTracesWithoutHeader1 = totalTraces/numOfHeader1;
				if(totalTracesWithoutHeader1 == 0)
					return false;
				return validateTwoHeaderSelection(totalTracesWithoutHeader1,range2,range3);
			}else if(min == range2){
				int numOfHeader2 = range2/i + 1;
				int totalTracesWithoutHeader2 = totalTraces/numOfHeader2;
				if(totalTracesWithoutHeader2 == 0)
					return false;
				return validateTwoHeaderSelection(totalTracesWithoutHeader2,range1,range3);
			}else if(min == range3){
				int numOfHeader3 = range3/i + 1;
				int totalTracesWithoutHeader3 = totalTraces/numOfHeader3;
				if(totalTracesWithoutHeader3 == 0)
					return false;
				return validateTwoHeaderSelection(totalTracesWithoutHeader3,range1,range2);				
			}
		}
		return false;
    }    

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
