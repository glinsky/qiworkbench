/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiDataConverter;

public class DataSetHeaderKeyInfo {
	private String name;
	private double min;
	private double max;
	private double incr;
	
	public DataSetHeaderKeyInfo(){
		name = "";
		min = 0;
		max = 0;
		incr = 0;
	}
	
	public DataSetHeaderKeyInfo(String name, double min, double max, double incr){
		this.name = name;
		this.min = min;
		this.max = max;
		this.incr = incr;
	}
	public String getName(){
		return name;
	}
	public double getMax(){
		return max;
	}
	public double getMin(){
		return min;
	}
	public double getIncr(){
		return incr;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setMin(double min){
		this.min = min;
	}
	
	public void setMax(double max){
		this.max = max;
	}

	public void setIncr(double incr){
		this.incr = incr;
	}

	public String toString(){
		return "Name:" + name + " Min:" + min + " Max:" + max + " Incr:" + incr;
	}
}
