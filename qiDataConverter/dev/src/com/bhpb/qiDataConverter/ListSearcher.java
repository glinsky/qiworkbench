/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiDataConverter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListModel;


public	class ListSearcher extends KeyAdapter {
	protected JList m_list;
	protected ListModel m_model;
	protected String m_key = "";
	protected long m_time = 0;
	private JButton ok;
	public static final int CHAR_DELTA = 1000;

	public ListSearcher(JList list,JButton ok)
	{
		m_list = list;
		m_model = m_list.getModel();
		this.ok = ok;
	}

	public void keyTyped(KeyEvent e)
	{
		char ch = e.getKeyChar();
		if (!Character.isLetterOrDigit(ch)){
			if ((int)ch == KeyEvent.VK_ENTER){
				ok.doClick();
			}
			return;
		}
		if (m_time+CHAR_DELTA < System.currentTimeMillis())
			m_key = "";
		m_time = System.currentTimeMillis();

		m_key += Character.toLowerCase(ch);
		for (int k = 0; k < m_model.getSize(); k++)
		{
			String str = ((String)m_model.getElementAt(k)).toLowerCase();
			if (str.startsWith(m_key))
			{
				m_list.setSelectedIndex(k);
				m_list.ensureIndexIsVisible(k);
				break;
			}
		}
	}

}
