/*
###########################################################################
# QiDataConverter - Convert seismic/horizon data between BHP-SU and other data
# stores such as SEGY, Landmark, SU.
#
# This program module Copyright (C) 2007 BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiDataConverter;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.TableModel;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

public abstract class HeaderConvertablePanel extends JPanel {
	private static Logger logger = Logger.getLogger(HeaderConvertablePanel.class.getName());
	protected QiDataConverterPlugin messagingAgent;
    /**
     * Contains header key and its range information; when user select a SEGY or SU file
     * if successfully processed, the app will populate this map with all header range (min, max)
     * information with header name as key
     */
	protected Map<String,DataSetHeaderKeyInfo> segyRangeMap = new HashMap<String,DataSetHeaderKeyInfo>();
	protected List<DataSetHeaderKeyInfo> segyRangeList;
    /**
     * Contains selected source header key and its increment information; populated when user
     * call for "Get Range" to get increment information for the chosen keys
     * for conversion (could choose the same key i.e. ep to ep)
     */
	protected Map<String,String>segyRangeIncrementMap = new HashMap<String,String>();
	protected int[][] defaultSEGYRange = {{Integer.MIN_VALUE,Integer.MIN_VALUE,1},
    {Integer.MIN_VALUE,Integer.MIN_VALUE,1},
    {Integer.MIN_VALUE,Integer.MIN_VALUE,1},{Integer.MIN_VALUE,Integer.MIN_VALUE,0}};
	public HeaderConvertablePanel(QiDataConverterPlugin agent){
		messagingAgent = agent;
	}
	
	public abstract void setSEGYRangeInfo(int [][] range);
	//public abstract void setHeaderNameList(ComboBoxModel modelEp, ComboBoxModel modelCdp);
	public abstract void setHeaderNameList(String [] headers);
	public abstract void setSURangeList(List<DataSetHeaderKeyInfo> list);
	public abstract void setStdOutErrorMessage(String message);
	public abstract void setNumberOfTraces(String text);
	public abstract void setHeaderRangeTableModel(TableModel model);
	public abstract void setSelectedInputFile(String path);
	public abstract int getSelectedEndianess();

    protected void initDefaultSEGYRange(){
        for(int i = 0; i < defaultSEGYRange.length; i++){
            defaultSEGYRange[i][0] = Integer.MIN_VALUE;
            defaultSEGYRange[i][1] = Integer.MIN_VALUE;
            if(i == 3)//tracl
                defaultSEGYRange[i][2] = 0;
            else
                defaultSEGYRange[i][2] = 1;
        }
    }
    /*
     * Initiate a thread with progress monitoring capability to get increment information from two non-unique headers
     * @param comp   the UI where the progress bar is based  
	 * @param toEp   the header name used to converse its value to the first header key
	 * @param toCdp   the header name used to converse its value to the second header key
	 * @param totalTraces  number of total traces
	 * @param command  the initial portion of the scripts to run this job
	 * @return
     */   
	protected void runGetHeaderIncrementTask(final Component comp, final String toEp, final String toCdp, final int totalTraces, final String command){
    	segyRangeIncrementMap.clear();

		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "";
				String text = "";
				if(comp instanceof SU2BhpsuPanel){
					title = "Gathering su headers increment information";
					text = "Please wait while system is gathering input su headers (" + toEp + "," + toCdp + ") increment information....";
				}else if(comp instanceof SEGY2BhpsuPanel){
					title = "Gathering segy headers increment information";
					text = "Please wait while system is gathering input SEGY Headers (" + toEp + "," + toCdp + ") increment information....";
				}

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				ProgressUtil.start(monitor,"");
				boolean uniqueEp = false;
				boolean uniqueCdp = false;
				if(segyRangeMap.containsKey(toEp)){
					DataSetHeaderKeyInfo ds = segyRangeMap.get(toEp);
					if(ds.getMin() == ds.getMax()){
						uniqueEp = true;
						segyRangeIncrementMap.put(toEp, String.valueOf("1"));
					}
				}
				if(segyRangeMap.containsKey(toCdp)){
					DataSetHeaderKeyInfo ds = segyRangeMap.get(toCdp);
					if(ds.getMin() == ds.getMax()){
						uniqueCdp = true;
						segyRangeIncrementMap.put(toCdp, String.valueOf("1"));
					}
				}
				int status = -1;
				if(uniqueEp && uniqueCdp)
					status = 0;
				else if(uniqueEp && !uniqueCdp){
					status = getSEGYHeaderIncrementInfo1(toCdp, totalTraces);
				}else if(!uniqueEp && uniqueCdp){
					status = getSEGYHeaderIncrementInfo1(toEp, totalTraces);
				}else
					status = getSEGYHeaderIncrementInfo2(toEp,toCdp,totalTraces,command);

				if(status == 0){
				int lineTraceRange[][] = {{Integer.MIN_VALUE,Integer.MIN_VALUE,1},{Integer.MIN_VALUE,Integer.MIN_VALUE,1},{Integer.MIN_VALUE,Integer.MIN_VALUE,1},{Integer.MIN_VALUE,Integer.MIN_VALUE,0}}; //{ep,{min,max,inc}},{cdp,{min,max,inc},{time,{min,max,inc}}
				int delrt=0, ns=0, dt=0;
				for(int i = 0; segyRangeList != null && i < segyRangeList.size(); i++){
					DataSetHeaderKeyInfo ds = segyRangeList.get(i);
					for(String s : segyRangeIncrementMap.keySet()){
						if(ds.getName().equals(s)){
							int inc = Integer.valueOf(segyRangeIncrementMap.get(s)).intValue();
							if(s.equals(toEp)){
								lineTraceRange[0][0] = (int)ds.getMin();
								lineTraceRange[0][1] = (int)ds.getMax();
								lineTraceRange[0][2] = inc;
							}else if(s.equals(toCdp)){
								lineTraceRange[1][0] = (int)ds.getMin();
								lineTraceRange[1][1] = (int)ds.getMax();
								lineTraceRange[1][2] = inc;
							}
						}
					}
					
					if(ds.getName().equals("delrt"))
						delrt = (int)ds.getMin();
					if(ds.getName().equals("ns"))
						ns = (int)ds.getMin();
					if(ds.getName().equals("dt"))
						dt = (int)ds.getMin();
				}
				lineTraceRange[3][0] = delrt;
				lineTraceRange[3][1] = delrt + (ns-1) * (dt/1000);
				lineTraceRange[3][2] = dt/1000;
				setSEGYRangeInfo(lineTraceRange);
				}
				if(ProgressUtil.isCancel(monitor)){
					//if(monitor.isCancel()){
					return;
				}
				logger.info("status = " + status);
				//if(status == 0 || status == 1 || status == -1){
				if(status == 0){
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				}else{
					String message = "";
					if(comp instanceof SU2BhpsuPanel)
						message = "Problem in running su read command (cat). Please check the error message or consult workbench support.";
					else if(comp instanceof SEGY2BhpsuPanel)
						message = "Problem in running segyread command. Please check the error message or consult workbench support.";
					JOptionPane.showMessageDialog(comp,message,
							"Error in running SU script.", JOptionPane.WARNING_MESSAGE);
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				}
			}
		};
		new Thread(heavyRunnable).start();
    }

    /*
     * Initiate a thread with progress monitoring capability to get increment information from three non-unique headers
     * @param comp   the UI where the progress bar is based  
	 * @param toEp   the header name used to converse its value to the first header key
	 * @param toCdp   the header name used to converse its value to the second header key
	 * @param toCdpt   the header name used to converse its value to the third header key
	 * @param totalTraces  number of total traces
	 * @param command  the initial portion of the scripts to run this job
	 * @return
     */   
    protected void runGetHeaderIncrementTask(final Component comp, final String toEp, final String toCdp, final String toCdpt, final int totalTraces, final String command){
    	segyRangeIncrementMap.clear();
		
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "";
				String text = "";
				if(comp instanceof SU2BhpsuPanel){
					title = "Gathering su headers increment information";
					text = "Please wait while system is gathering input su headers (" + toEp + "," + toCdp + "," + toCdpt + ") increment information....";
				}else if(comp instanceof SEGY2BhpsuPanel){
					title = "Gathering segy headers increment information";
					text = "Please wait while system is gathering input segy headers (" + toEp + "," + toCdp + "," + toCdpt + ") increment information....";
				}

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,true,500,title,text,icon);
				//monitor.start("");
				ProgressUtil.start(monitor,"");
				boolean uniqueEp = false;
				boolean uniqueCdp = false;
				boolean uniqueCdpt = false;
                List<String> headerList = new ArrayList<String>();
				if(segyRangeMap.containsKey(toEp)){
					DataSetHeaderKeyInfo ds = segyRangeMap.get(toEp);
					if(ds.getMin() == ds.getMax()){
						uniqueEp = true;
						segyRangeIncrementMap.put(toEp, String.valueOf("1"));
					}
                    headerList.add(toEp);
				}
				if(segyRangeMap.containsKey(toCdp)){
					DataSetHeaderKeyInfo ds = segyRangeMap.get(toCdp);
					if(ds.getMin() == ds.getMax()){
						uniqueCdp = true;
						segyRangeIncrementMap.put(toCdp, String.valueOf("1"));
					}
                    headerList.add(toCdp);
				}
				if(segyRangeMap.containsKey(toCdpt)){
					DataSetHeaderKeyInfo ds = segyRangeMap.get(toCdpt);
					if(ds.getMin() == ds.getMax()){
						uniqueCdpt = true;
						segyRangeIncrementMap.put(toCdpt, String.valueOf("1"));
					}
                    headerList.add(toCdpt);
				}
				int status = -1;
				if(uniqueEp && uniqueCdp && uniqueCdpt)
					return;
				else if(uniqueEp && uniqueCdpt && !uniqueCdp){
					status = getSEGYHeaderIncrementInfo1(toCdp, totalTraces);
				}else if(!uniqueEp && uniqueCdp && uniqueCdpt){
					status = getSEGYHeaderIncrementInfo1(toEp, totalTraces);
				}else if(uniqueEp && uniqueCdp && !uniqueCdpt){
					status = getSEGYHeaderIncrementInfo1(toCdpt, totalTraces);					
				}else if(uniqueEp && !uniqueCdpt && !uniqueCdp){
					status = getSEGYHeaderIncrementInfo2(toCdpt,toCdp,totalTraces,command);
				}else if(!uniqueEp && !uniqueCdpt && uniqueCdp){
					status = getSEGYHeaderIncrementInfo2(toEp,toCdpt,totalTraces,command);
				}else if(!uniqueEp && uniqueCdpt && !uniqueCdp){
					status = getSEGYHeaderIncrementInfo2(toEp,toCdp,totalTraces,command);
				}else{
					List<String>  list = new ArrayList<String>();
					list.add(toEp);
					list.add(toCdp);
					list.add(toCdpt);
					status = getSEGYHeaderIncrementInfo3(list,totalTraces,2,command);
				}
				if(status == 0){
				int lineTraceRange[][] = {{Integer.MIN_VALUE,Integer.MIN_VALUE,1},{Integer.MIN_VALUE,Integer.MIN_VALUE,1},{Integer.MIN_VALUE,Integer.MIN_VALUE,1},{Integer.MIN_VALUE,Integer.MIN_VALUE,0}}; //{ep,{min,max,inc}},{cdp,{min,max,inc},{cdpt,{min,max,inc},{time,{min,max,inc}}
				int delrt=0, ns=0, dt=0;
                for(int i = 0; i < headerList.size(); i++){
                    String s = headerList.get(i);
                    if(segyRangeIncrementMap.containsKey(s) && segyRangeMap.containsKey(s)){
				//for(int i = 0; segyRangeList != null && i < segyRangeList.size(); i++){
					//DataSetHeaderKeyInfo ds = segyRangeList.get(i);
                        DataSetHeaderKeyInfo ds = segyRangeMap.get(s);
					//for(String s : segyRangeIncrementMap.keySet()){
                    //for(String s : headerList){
                        //if(segyRangeIncrementMap.containsKey(s)){
						//if(ds.getName().equals(s)){
							int inc = Integer.valueOf(segyRangeIncrementMap.get(s)).intValue();
							//if(s.equals(toEp)){
                            //if(i == 0){
								lineTraceRange[i][0] = (int)ds.getMin();
								lineTraceRange[i][1] = (int)ds.getMax();
								lineTraceRange[i][2] = inc;
							//}else if(s.equals(toCdp)){
							//	lineTraceRange[1][0] = (int)ds.getMin();
							//	lineTraceRange[1][1] = (int)ds.getMax();
							//	lineTraceRange[1][2] = inc;
							//}else if(s.equals(toCdpt)){
							//	lineTraceRange[2][0] = (int)ds.getMin();
							//	lineTraceRange[2][1] = (int)ds.getMax();
							//	lineTraceRange[2][2] = inc;
							//}
						//}
					}
					}
					if(segyRangeMap.containsKey("delrt")){
                        DataSetHeaderKeyInfo ds = segyRangeMap.get("delrt");
                        delrt = (int)ds.getMin();
                    }

                    if(segyRangeMap.containsKey("ns")){
                        DataSetHeaderKeyInfo ds = segyRangeMap.get("ns");
                        ns = (int)ds.getMin();
                    }

                    if(segyRangeMap.containsKey("dt")){
                        DataSetHeaderKeyInfo ds = segyRangeMap.get("dt");
                        dt = (int)ds.getMin();
                    }
					//if(ds.getName().equals("ns"))
						
					//if(ds.getName().equals("dt"))
					//	dt = (int)ds.getMin();
				//}
				lineTraceRange[3][0] = delrt;
				lineTraceRange[3][1] = delrt + (ns-1) * (dt/1000);
				lineTraceRange[3][2] = dt/1000;
				setSEGYRangeInfo(lineTraceRange);
				//}
				if(ProgressUtil.isCancel(monitor)){
					//if(monitor.isCancel()){
					return;
				}
				logger.info("status = " + status);
				//if(status == 0 || status == 1 || status == -1){
				if(status == 0){
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				}else{
					
					String message = "";
					if(comp instanceof SU2BhpsuPanel)
						message = "Problem in running su read command (cat). Please check the error message or consult workbench support.";
					else if(comp instanceof SEGY2BhpsuPanel)
						message = "Problem in running segyread command. Please check the error message or consult workbench support.";
					JOptionPane.showMessageDialog(comp,message,
							"Error in running SU script.", JOptionPane.WARNING_MESSAGE);
					ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				}
			}
            }
		};
		new Thread(heavyRunnable).start();
    }
    
    
    private int getSEGYHeaderIncrementInfo1(String headerName, int totalTraces){
    	if(headerName == null || headerName.trim().length() == 0 || totalTraces < 1)
			return 1;
    	if(segyRangeMap.containsKey(headerName)){
    		DataSetHeaderKeyInfo ds = segyRangeMap.get(headerName);
    		int range = (int)(ds.getMax()-ds.getMin());
    		int inc = range /(totalTraces-1);
    		segyRangeIncrementMap.put(headerName, String.valueOf(inc));
    		return 0;
    	}
    	return 1;
    }
    
    /*
     * Get increment information from two non-unique headers
     * @param filePath   the file path of the target segy file  
	 * @param endian   the value of endianess
	 * @param toEp     header name
	 * @param toCdp     header name
	 * @param totalTraces total number of traces with two non-unique header keys
     */
    private int getSEGYHeaderIncrementInfo2(String toEp, String toCdp, int totalTraces, String command){
    	if(messagingAgent == null)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		MessagingManager messagingMgr = messagingAgent.getMessagingMgr();
		params.add(messagingMgr.getLocationPref());
		params.add("bash");
		params.add("-c");
		//String command = " source ~/.bashrc; cat " + filePath;
		//if(endian == 0 || endian == 1){
		//	command += " | suswapbytes format=" + endian;
		//}
		//command += " | segyclean | "
		String cmd = command + " suwind count=2 | sugethw key=" + toEp + "," + toCdp;
		params.add(cmd);
		params.add("true");
		logger.info("command= " + params.get(3));
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD message content = " + errorContent);
				setStdOutErrorMessage(errorContent);
				//JOptionPane.showMessageDialog(gui,"Error in running SU script.",
				//		"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}else{
				errorContent = "SUBMIT_JOB_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "GET_JOB_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		} else {
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			parseSEGYHeaderIncrementOutput2(stdOut,toEp, toCdp, totalTraces);
		}
		
		/*String header;
		if(segyRangeIncrementMap.containsKey(toEp))
			header = toCdp;
		else
			header = toEp;
		int sta = getSEGYHeaderSecondIncrementInfo(filePath, endian, header, range);
		*/
		
//		get stderr
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
				//JOptionPane.showMessageDialog(gui,"Error in running SU script.",
				//		"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}else{
				errorContent = "GET_JOB_ERROR_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			ArrayList stdErr = (ArrayList)MsgUtils.getMsgContent(response);
			StringBuffer errorContentB = new StringBuffer();
			for(int i=0; i<stdErr.size(); i++){
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));
				errorContentB.append(stdErr.get(i) + "\n");
			}
			setStdOutErrorMessage(errorContentB.toString());
			status = MsgUtils.getMsgStatusCode(response);
			logger.warning("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
			//if(stdErr.size() > 0){
			//	JOptionPane.showMessageDialog(gui,"Error in running SU script:" + stdErr,
			//		"Problem in running SU script", JOptionPane.WARNING_MESSAGE);
			//}
		}

		//Wait until the command has finished
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "WAIT_FOR_JOB_EXIT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		// get exitValue
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}
    }

    /*
     * Get increment information from three non-unique headers
     * @param filePath   the file path of the target segy file  
	 * @param endian   the value of endianess
	 * @param headers     a list containing three non-unique header names
	 * @return status of the method
     */
    private int getSEGYHeaderIncrementInfo3(List<String> headers, int totalTraces, int count, String command){
    	if(messagingAgent == null)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		MessagingManager messagingMgr = messagingAgent.getMessagingMgr();
		params.add(messagingMgr.getLocationPref());
		params.add("sh");
		params.add("-c");
		//String command = " source ~/.bashrc; cat " + filePath;
		//if(endian == 0 || endian == 1){
		//	command += " | suswapbytes format=" + endian;
		//}
		//command += " | segyclean | ";
		String cmd = command + " suwind count=" + count + " | sugethw key=" + headers.get(0) + "," + headers.get(1);
		params.add(cmd);
		params.add("true");
		logger.info("command= " + params.get(3));
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD message content = " + errorContent);
				setStdOutErrorMessage(errorContent);
				//JOptionPane.showMessageDialog(gui,"Error in running SU script.",
				//		"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}else{
				errorContent = "SUBMIT_JOB_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "GET_JOB_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		} else {
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			parseSEGYHeaderIncrementOutput3(stdOut, headers, totalTraces,command);
		}
		
		/*String header;
		if(segyRangeIncrementMap.containsKey(toEp))
			header = toCdp;
		else
			header = toEp;
		int sta = getSEGYHeaderSecondIncrementInfo(filePath, endian, header, range);
		*/
		
//		get stderr
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
				//JOptionPane.showMessageDialog(gui,"Error in running SU script.",
				//		"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}else{
				errorContent = "GET_JOB_ERROR_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			ArrayList stdErr = (ArrayList)MsgUtils.getMsgContent(response);
			StringBuffer errorContentB = new StringBuffer();
			for(int i=0; i<stdErr.size(); i++){
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));
				errorContentB.append(stdErr.get(i) + "\n");
			}
			setStdOutErrorMessage(errorContentB.toString());
			status = MsgUtils.getMsgStatusCode(response);
			logger.warning("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
			//if(stdErr.size() > 0){
			//	JOptionPane.showMessageDialog(gui,"Error in running SU script:" + stdErr,
			//		"Problem in running SU script", JOptionPane.WARNING_MESSAGE);
			//}
		}

		//Wait until the command has finished
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "WAIT_FOR_JOB_EXIT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		// get exitValue
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}
    }
    
    
    private int getSEGYHeaderSecondIncrementInfo(String filePath, int endian, String header, int j, String command){
    	if(filePath == null || filePath.trim().length() == 0 || messagingAgent == null)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		MessagingManager messagingMgr = messagingAgent.getMessagingMgr();
		params.add(messagingMgr.getLocationPref());
		params.add("sh");
		params.add("-c");
		//String command = " source ~/.bashrc; segyread tape=" + filePath;
		//if(endian == 0 || endian == 1)
		//	command += " endian=" + endian;
		//command += " | segyclean | "
		String cmd = command + " suwind j=" + j + " count=2 | sugethw key=" + header;
		params.add(cmd);
		params.add("true");
		logger.info("command= " + params.get(3));
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD message content = " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "SUBMIT_JOB_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "GET_JOB_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		} else {
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			parseSEGYSecondHeaderIncrementOutput(stdOut);
		}
		
//		get stderr
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
			}else{
				errorContent = "GET_JOB_ERROR_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			ArrayList stdErr = (ArrayList)MsgUtils.getMsgContent(response);
			StringBuffer errorContentB = new StringBuffer();
			for(int i=0; i<stdErr.size(); i++){
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));
				errorContentB.append(stdErr.get(i) + "\n");
			}
			setStdOutErrorMessage(errorContentB.toString());
			status = MsgUtils.getMsgStatusCode(response);
			logger.warning("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
		}

		//Wait until the command has finished
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "WAIT_FOR_JOB_EXIT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}
    }    
    
    /*
     * Parsing the output of sugethw and get increment from one header, then perform calculation to get increment information
     * for the other one
	 * @param list     a list of String containing output of sugethw  
	 * @param toEp     header name
	 * @param toCdp     header name
	 * @param totalTraces     total number of traces containing three non-unique header keys
     */
    private void parseSEGYHeaderIncrementOutput2(List<String> list,String toEp, String toCdp, int totalTraces){
    	if(list == null || list.size() == 0)
            return;
    	for(int i = 0; i < list.size(); i++){
    		String line = list.get(i);
    		if(line.trim().length() == 0){
    			list.remove(i);
    		}
    	}
    	
    	NameValues nv1 = new NameValues();
    	NameValues nv2 = new NameValues();
    	if(list.size() == 2){
    		String firstLine = list.get(0);

    		String [] arr1 = firstLine.trim().split(" +");
    		if(arr1.length == 2){
    			String [] detail1 = arr1[0].trim().split("=");
    			nv1.setName(detail1[0].trim());
    			nv1.setFirst(Integer.valueOf(detail1[1].trim()).intValue());
    			String [] detail2 = arr1[1].trim().split("=");
    			nv2.setName(detail2[0].trim());
    			nv2.setFirst(Integer.valueOf(detail2[1].trim()).intValue());
    		}

    		String secondLine = list.get(1);

    		String [] arr2 = secondLine.trim().split(" +");
    		if(arr2.length == 2){
    			String [] detail1 = arr2[0].trim().split("=");
    			nv1.setSecond(Integer.valueOf(detail1[1].trim()).intValue());
    			String [] detail2 = arr2[1].trim().split("=");
    			nv2.setSecond(Integer.valueOf(detail2[1].trim()).intValue());
    		}
    		
    		if(nv1.getIncrement() > 0){
    			segyRangeIncrementMap.put(nv1.getName(), String.valueOf(nv1.getIncrement()));
    			DataSetHeaderKeyInfo dsNv1 = segyRangeMap.get(nv1.getName());
    			int numberOfNv1 = (int)(dsNv1.getMax() - dsNv1.getMin()) / nv1.getIncrement() + 1;
    			DataSetHeaderKeyInfo dsNv2 = segyRangeMap.get(nv2.getName());
    			int incrementNv2 = (int)(dsNv2.getMax()-dsNv2.getMin()) / (totalTraces/numberOfNv1 - 1);
    			segyRangeIncrementMap.put(nv2.getName(), String.valueOf(incrementNv2));
    		}
    		if(nv2.getIncrement() > 0){
    			segyRangeIncrementMap.put(nv2.getName(), String.valueOf(nv2.getIncrement()));
    			DataSetHeaderKeyInfo dsNv2 = segyRangeMap.get(nv2.getName());
    			int numberOfNv2 = ((int)(dsNv2.getMax() - dsNv2.getMin()) / nv2.getIncrement()) + 1;
    			DataSetHeaderKeyInfo dsNv1 = segyRangeMap.get(nv1.getName());
    			int incrementNv1 = ((int)(dsNv1.getMax()-dsNv1.getMin())) / (totalTraces/numberOfNv2 - 1);
    			segyRangeIncrementMap.put(nv1.getName(), String.valueOf(incrementNv1));
    		}
    	}
    	return;
    }

    /*
     * Parsing the output of sugethw and get increment from one header, then call getSEGYHeaderIncrementInfo2 to get increment information
     * from the other tow headers
     * @param filePath   the file path of the target segy file  
	 * @param endian   the value of endianess
	 * @param list     a list of String containing output of sugethw  
	 * @param headers     a list containing three non-unique header names
	 * @param totalTraces     total number of traces containing three non-unique header keys
     */
    private void parseSEGYHeaderIncrementOutput3(List<String> list,List<String> headers, int totalTraces, String command){
    	if(list == null || list.size() == 0)
            return;
    	for(int i = 0; i < list.size(); i++){
    		String line = list.get(i);
    		if(line.trim().length() == 0){
    			list.remove(i);
    		}
    	}
    	
    	int size = list.size();
    	
    	List<String> newlist = new ArrayList();
    	if(size > 2){
    		newlist.add(list.get(size-2));
    		newlist.add(list.get(size-1));
    		list = newlist;
    	}
    	
    	NameValues nv1 = new NameValues();
    	NameValues nv2 = new NameValues();
    	if(list.size() == 2){
    		String firstLine = list.get(0);

    		String [] arr1 = firstLine.trim().split(" +");
    		if(arr1.length == 2){
    			String [] detail1 = arr1[0].trim().split("=");
    			nv1.setName(detail1[0].trim());
    			nv1.setFirst(Integer.valueOf(detail1[1].trim()).intValue());
    			String [] detail2 = arr1[1].trim().split("=");
    			nv2.setName(detail2[0].trim());
    			nv2.setFirst(Integer.valueOf(detail2[1].trim()).intValue());
    		}

    		String secondLine = list.get(1);

    		String [] arr2 = secondLine.trim().split(" +");
    		if(arr2.length == 2){
    			String [] detail1 = arr2[0].trim().split("=");
    			nv1.setSecond(Integer.valueOf(detail1[1].trim()).intValue());
    			String [] detail2 = arr2[1].trim().split("=");
    			nv2.setSecond(Integer.valueOf(detail2[1].trim()).intValue());
    		}
    		
    		if(nv1.getIncrement() > 0){
    			segyRangeIncrementMap.put(nv1.getName(), String.valueOf(nv1.getIncrement()));
    			DataSetHeaderKeyInfo dsNv1 = segyRangeMap.get(nv1.getName());
    			int numberOfNv1 = (int)(dsNv1.getMax() - dsNv1.getMin()) / nv1.getIncrement() + 1;
    			//int totalTracesWithoutNv1 = totalTraces / numberOfNv1;
    			headers.remove(nv1.getName());
    			headers.add(nv1.getName());
    			//int count = 0;
    			
    			//for(String s : headers){
    			//	if(segyRangeIncrementMap.containsKey(s))
    			//		count++;
    			//}
    			
    			//if(count < 2)
    			if(segyRangeIncrementMap.keySet().size() < 2)
    			//DataSetHeaderKeyInfo dsNv2 = segyRangeMap.get(nv2.getName());
    			//int incrementNv2 = (int)(dsNv2.getMax()-dsNv2.getMin()) / (totalTraces/numberOfNv1 - 1);
    			//segyRangeIncrementMap.put(nv2.getName(), String.valueOf(incrementNv2));
    			//if(headers.size() == 2)
    				getSEGYHeaderIncrementInfo3(headers, totalTraces,numberOfNv1+1,command);
    			else{
    				int total = totalTraces;
    				String unresolve = null;
    				for(String s : headers){
        				if(!segyRangeIncrementMap.containsKey(s))
        					unresolve = s;
        				else{
        					DataSetHeaderKeyInfo ds = segyRangeMap.get(s);
        					String str = segyRangeIncrementMap.get(s);
        					int inc = Integer.valueOf(str).intValue();
        					if(inc != 0)
        						total = total /((int)(ds.getMax() - ds.getMin()) / inc + 1);
        				}
        			}
    				DataSetHeaderKeyInfo ds = segyRangeMap.get(unresolve);
    				int range = (int)(ds.getMax() - ds.getMin());
    				if(total > 1){
    					int incr = range / (total-1);
    					segyRangeIncrementMap.put(unresolve, String.valueOf(incr));
    				}
    			}
    		}else
    		if(nv2.getIncrement() > 0){
    			segyRangeIncrementMap.put(nv2.getName(), String.valueOf(nv2.getIncrement()));
    			DataSetHeaderKeyInfo dsNv2 = segyRangeMap.get(nv2.getName());
    			int numberOfNv2 = ((int)(dsNv2.getMax() - dsNv2.getMin()) / nv2.getIncrement()) + 1;
    			//int totalTracesWithoutNv2 = totalTraces / numberOfNv2;
    			headers.remove(nv2.getName());
    			headers.add(nv2.getName());
    			
    			//int count = 0;
    			//for(String s : headers){
    			//	if(segyRangeIncrementMap.containsKey(s))
    			//		count++;
    			//}
    			
    			//if(count < 2)
    			if(segyRangeIncrementMap.keySet().size() < 2)
    			getSEGYHeaderIncrementInfo3(headers, totalTraces,numberOfNv2+1,command);
    			//DataSetHeaderKeyInfo dsNv1 = segyRangeMap.get(nv1.getName());
    			//int incrementNv1 = ((int)(dsNv1.getMax()-dsNv1.getMin())) / (totalTraces/numberOfNv2 - 1);
    			//segyRangeIncrementMap.put(nv1.getName(), String.valueOf(incrementNv1));
    			//if(headers.size() == 2)
    				//getSEGYHeaderIncrementInfo2(filePath, endian, headers.get(0), headers.get(1), totalTracesWithoutNv2);
    			else{
    				int total = totalTraces;
    				String unresolve = null;
    				for(String s : headers){
        				if(!segyRangeIncrementMap.containsKey(s)){
        					unresolve = s;
        				}else{
        					DataSetHeaderKeyInfo ds = segyRangeMap.get(s);
        					String str = segyRangeIncrementMap.get(s);
        					int inc = Integer.valueOf(str).intValue();
        					if(inc != 0)
        						total = total /((int)(ds.getMax() - ds.getMin()) / inc + 1);
        				}
        			}
    				DataSetHeaderKeyInfo ds = segyRangeMap.get(unresolve);
    				int range = (int)(ds.getMax() - ds.getMin());
    				if(total > 1){
    					int incr = range / (total-1);
    					segyRangeIncrementMap.put(unresolve, String.valueOf(incr));
    				}
    			}
    		}else{ //all identical because the number breaks first with the other key
    			String name = headers.remove(2);
    			headers.add(0, name);
    			
    			//if(!segyRangeIncrementMap.containsKey(name))
    				getSEGYHeaderIncrementInfo3(headers,totalTraces,2,command);
    			//else{
    			//	String incs = segyRangeIncrementMap.get(name);
    			//	int inc = Integer.valueOf(incs).intValue();
    			//	DataSetHeaderKeyInfo ds = segyRangeMap.get(name);
        		//	int range = (int)(ds.getMax() - ds.getMin());
        		//	if(inc != 0){
        		//		range = range / inc + 1;
        		//	}
    			//	getSEGYHeaderIncrementInfo3(filePath,endian,headers,totalTraces,range+1);
    			//}
    		}
    	}
    	return;
    }
    
    private int getSEGYHeaderFirstIncrementInfo(String filePath, int endian, String toEp, String toCdp, String command){
    	if(filePath == null || filePath.trim().length() == 0 || messagingAgent == null)
			return 1;
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		MessagingManager messagingMgr = messagingAgent.getMessagingMgr();
		params.add(messagingMgr.getLocationPref());
		params.add("sh");
		params.add("-c");
		//String command = " source ~/.bashrc; segyread tape=" + filePath;
		//if(endian == 0 || endian == 1)
		//	command += " endian=" + endian;
		//command += " | segyclean | "
		String cmd = command + " suwind count=2 | sugethw key=" + toEp + "," + toCdp;
		params.add(cmd);
		params.add("true");
		logger.info("command= " + params.get(3));
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		//QiWorkbenchMsg response = messagingMgr.getNextMsgWait();
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD message content = " + errorContent);
				setStdOutErrorMessage(errorContent);
				//JOptionPane.showMessageDialog(gui,"Error in running SU script.",
				//		"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}else{
				errorContent = "SUBMIT_JOB_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "GET_JOB_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		} else {
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			status = MsgUtils.getMsgStatusCode(response);
			logger.info("status of GET_JOB_OUTPUT_CMD normal response " + status);
			System.out.println("stdOut.size() of GET_JOB_OUTPUT_CMD =" + stdOut.size());
			for(int i=0; i<stdOut.size(); i++)
				System.out.println(stdOut.get(i));
			int range = parseSEGYFirstHeaderIncrementOutput(stdOut,toEp, toCdp);
			String header;
			if(segyRangeIncrementMap.containsKey(toEp))
				header = toCdp;
			else
				header = toEp;
			int sta = getSEGYHeaderSecondIncrementInfo(filePath, endian, header, range,command);
			int lineTraceRange[][] = {{0,0,1},{0,0,1},{0,0,1},{0,0,0}}; //{ep,{min,max,inc}},{cdp,{min,max,inc},{time,{min,max,inc}}
			if(sta == 0){
				if(segyRangeIncrementMap.isEmpty()){
					segyRangeIncrementMap.put(toEp, String.valueOf(1));
					segyRangeIncrementMap.put(toCdp, String.valueOf(1));
				}

				int delrt=0, ns=0, dt=0;
				for(int i = 0; segyRangeList != null && i < segyRangeList.size(); i++){
					DataSetHeaderKeyInfo ds = segyRangeList.get(i);
					for(String s : segyRangeIncrementMap.keySet()){
						if(ds.getName().equals(s)){
							range = (int)(ds.getMax() - ds.getMin());
							int inc = Integer.valueOf(segyRangeIncrementMap.get(s)).intValue();
							
							if(s.equals(toEp)){
								lineTraceRange[0][0] = (int)ds.getMin();
								lineTraceRange[0][1] = (int)ds.getMax();
								lineTraceRange[0][2] = inc;
							}else if(s.equals(toCdp)){
								lineTraceRange[1][0] = (int)ds.getMin();
								lineTraceRange[1][1] = (int)ds.getMax();
								lineTraceRange[1][2] = inc;
							}
						}
	    			}
					
					if(ds.getName().equals("delrt"))
						delrt = (int)ds.getMin();
					if(ds.getName().equals("ns"))
						ns = (int)ds.getMin();
					if(ds.getName().equals("dt"))
						dt = (int)ds.getMin();
	    		}
				lineTraceRange[3][0] = delrt;
				lineTraceRange[3][1] = delrt + (ns-1) * (dt/1000);
				lineTraceRange[3][2] = dt;
				setSEGYRangeInfo(lineTraceRange);
	    	}
		}
		
//		get stderr
		params.clear();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_ERROR_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_ERROR_OUTPUT_CMD errorContent = " + errorContent);
				//JOptionPane.showMessageDialog(gui,"Error in running SU script.",
				//		"Problem in running SU script: " + errorContent, JOptionPane.WARNING_MESSAGE);
			}else{
				errorContent = "GET_JOB_ERROR_OUTPUT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			ArrayList stdErr = (ArrayList)MsgUtils.getMsgContent(response);
			StringBuffer errorContentB = new StringBuffer();
			for(int i=0; i<stdErr.size(); i++){
				logger.info("GET_JOB_ERROR_OUTPUT_CMD " + stdErr.get(i));
				errorContentB.append(stdErr.get(i) + "\n");
			}
			setStdOutErrorMessage(errorContentB.toString());
			status = MsgUtils.getMsgStatusCode(response);
			logger.warning("status of GET_JOB_ERROR_OUTPUT_CMD normal response " + status);
			//if(stdErr.size() > 0){
			//	JOptionPane.showMessageDialog(gui,"Error in running SU script:" + stdErr,
			//		"Problem in running SU script", JOptionPane.WARNING_MESSAGE);
			//}
		}

		//Wait until the command has finished
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.WAIT_FOR_JOB_EXIT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("WAIT_FOR_JOB_EXIT_CMD errorContent = " + errorContent);
				setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "WAIT_FOR_JOB_EXIT_CMD request message for segyread job timed out.";
				logger.warning(errorContent);
				setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return errorStatus;
		}
		else{
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of WAIT_FOR_JOB_EXIT_CMD normal response " + status);
		}
		// get exitValue
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_STATUS_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			return -1;
		}else{
			//return status = (Integer)MsgUtils.getMsgContent(response);
			status = (Integer)MsgUtils.getMsgContent(response);
			logger.info("status of GET_JOB_STATUS_CMD normal job status " + status);
			return status;

		}
    }

    
    private int parseSEGYFirstHeaderIncrementOutput(List<String> list,String toEp, String toCdp){
    	if(list == null || list.size() == 0)
            return 0;
    	for(int i = 0; i < list.size(); i++){
    		String line = list.get(i);
    		if(line.trim().length() == 0){
    			list.remove(i);
    		}
    	}
    	
    	
    	if(list.size() <= 2){
    		String firstLine = list.get(0);
    		String [] array1 = firstLine.trim().split(" +");
    		for(int i = 0; i < array1.length; i++){
    			String [] detail = array1[i].split("=");
    			segyRangeIncrementMap.put(detail[0].trim(), detail[1].trim());
    		}
    		String [] array2 = list.get(1).trim().split(" +");
    		for(String s : array2){
    			String [] detail = s.trim().split("=");
    			if(segyRangeIncrementMap.containsKey(detail[0].trim())){
    				if(segyRangeIncrementMap.get(detail[0].trim()).equals(detail[1].trim())){
    					segyRangeIncrementMap.remove(detail[0].trim());
    				}else{ //increment found and settled
    					String val1 = segyRangeIncrementMap.get(detail[0].trim());
    					int ival1 = Integer.valueOf(val1).intValue();
    					int ival2 = Integer.valueOf(detail[1].trim()).intValue();
    					segyRangeIncrementMap.put(detail[0].trim(), String.valueOf(ival2-ival1));
    				}
    			}else{
    				segyRangeIncrementMap.put(detail[0].trim(), detail[1].trim());
    			}
    		}
    	}
    	int range = 0;
    	for(int i = 0; segyRangeList != null && i < segyRangeList.size(); i++){
    		DataSetHeaderKeyInfo ds = segyRangeList.get(i);
    		for(String s : segyRangeIncrementMap.keySet()){
    			if(ds.getName().equals(s)){
    				range = (int)(ds.getMax() - ds.getMin());
    				int inc = Integer.valueOf(segyRangeIncrementMap.get(s)).intValue();
    				if(inc != 0)
    					range = range / inc;
    			}
    		}
    	}
    	return range;
    }
    
    private Map<String,String> parseSEGYSecondHeaderIncrementOutput(List<String> list){
    	if(list == null || list.size() == 0)
            return null;
    	for(int i = 0; i < list.size(); i++){
    		String line = list.get(i);
    		if(line.trim().length() == 0){
    			list.remove(i);
    		}
    	}
    	
    	if(list.size() <= 2){
    		String first = list.get(0);
    		String [] array1 = first.trim().split("=");
    		String [] array2 = list.get(1).trim().split("=");
    		int ival1 = Integer.valueOf(array1[1]).intValue();
    		int ival2 = Integer.valueOf(array2[1]).intValue();
    		if(ival1 == ival2)
    			segyRangeIncrementMap.put(array1[0], String.valueOf(1));
    		else
    			segyRangeIncrementMap.put(array1[0], String.valueOf(ival2-ival1));
    	}
    	return segyRangeIncrementMap;
    }    

    protected static List<DataSetHeaderKeyInfo> genSURangeListFromNode(Node node){
    	Element el = (Element)node;
    	String attr;
    	List<DataSetHeaderKeyInfo> rangeList = new ArrayList<DataSetHeaderKeyInfo>();
    	NodeList childs = el.getChildNodes();
    	for (int j = 0; childs != null && j < childs.getLength(); j++) {
    		String nodeName;
    		Node chi = childs.item(j);
    		if (chi.getNodeType() == Node.ELEMENT_NODE) {
    			nodeName = chi.getNodeName();
                if (nodeName.equals("suRangeInfo")) {
                	NodeList children = chi.getChildNodes();
		    		for (int i = 0; children != null && i < children.getLength(); i++) {
			            Node child = children.item(i);
			            if (child.getNodeType() == Node.ELEMENT_NODE) {
			                nodeName = child.getNodeName();
			                if (nodeName.equals("key")) {
			                	attr = ((Element)child).getAttribute("name");
			                	DataSetHeaderKeyInfo ds = new DataSetHeaderKeyInfo();
			                	ds.setName(attr);
			                	attr = ((Element)child).getAttribute("min");
			                	ds.setMin(Double.valueOf(attr));
			                	attr = ((Element)child).getAttribute("max");
			                	ds.setMax(Double.valueOf(attr));
			                	attr = ((Element)child).getAttribute("incr");
			                	ds.setIncr(Double.valueOf(attr));
			                	rangeList.add(ds);
			                }
			            }
		    		}
                }
    		}
        }
    	return rangeList;
    }
}
