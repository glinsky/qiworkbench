/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer;

import com.bhpb.qiviewer.ui.desktop.ViewerDesktop;
import java.awt.Dimension;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.fest.swing.core.BasicRobot;
import org.fest.swing.core.Robot;
import org.fest.swing.core.Settings;
import org.fest.swing.fixture.ContainerFixture;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JMenuItemFixture;
import org.junit.After;
import org.junit.Before;

import static org.junit.Assert.*;

public abstract class QiViewerSwingTestBase extends QiViewerTestBase {
    private static final Logger logger = Logger.getLogger(QiViewerSwingTestBase.class.toString());
    
    private FrameFixture window;
    private ViewerDesktop desktop;
    private Thread agentThread;
    private Robot robot;
    
    @Before
    @Override
    public void setUp() {
        super.setUp();
        robot = BasicRobot.robotWithNewAwtHierarchy();
        new Settings().delayBetweenEvents(500);
        initViewer();
        super.workbenchMgr.associateProjMgr(agent.getComponentDescriptor());
    }
    
    @After
    @Override
    public void tearDown() {
        super.tearDown(); // fix for FindBugs IJU_TEARDOWN_NO_SUPER
        window.close();
        window.cleanUp();
    }
    
    private void initViewer() {
        assertNotNull("Error - ViewerAgent is null", agent);
        agentThread = new Thread(agent);
        agentThread.start();
        
        int elapsedTime = 0;
        
        while ((agent.isInitFinished() == false) && (elapsedTime < 10000)){
            try {
                elapsedTime += 1000; // this is added BEFORE sleep in case it is repeatedly interrupted
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                logger.warning("Caught InterruptedException");
            }
            elapsedTime += 1000;
        }
        
        if (agent.isInitFinished() == false) {
            fail("Component init did not finish after 5 seconds.");
        }
        
        if (agent.isInitSuccessful()) {
            // do nothing, this is the successful case
        } else {
            fail("Component initialization failed");
        }
        
        JFrame frame = new JFrame();
         
        agent.getMessageProcessor().invokeNewComponent();
        desktop = (ViewerDesktop)agent.getViewerDesktop();
        assertNotNull(desktop);
        
        frame.getContentPane().add(desktop);
        desktop.setVisible(true);
        desktop.setSize(800,600);
       
        window = new FrameFixture(robot,(JFrame)frame);
        assertNotNull(window);

        window.show(new Dimension(800,600));
    }
    
    protected ViewerDesktop getDesktop() {
        return desktop;
    }
    
    protected FrameFixture getFrameFixture() {
        return window;
    }
    
    /**
     * Renamed to show that this method closes any open MessageBox by clicking 'Ok'.
     */
    protected void closeMessageBoxWithOk() {
        FestUtils.getOptionPane(robot).okButton().click();
    }
    
    /**
     * Closes the visible messageBox by clicking "OK".
     * Currently duplicates the functionality of closeUnimplementedCodeWarning
     * but may later exercise more complicated JOptionPane GUIs.
     */
    protected void closeMessageBoxWithOK() {
        FestUtils.getOptionPane(robot).okButton().click();
    }
    
    /**
     * Select the MenuItem with the specified path in the ContainerFixture.
     */
    protected void selectMenuItem(ContainerFixture containerFixture, String ... menuItemPath) {
        JMenuItemFixture menuItem = containerFixture.menuItemWithPath(menuItemPath);
        assertNotNull(menuItem);
        logger.info("Got JMenuItemFixture for menuItem: " + getPathString(menuItemPath));
        logger.info("Selecting...");
        menuItem.click();
        logger.info("Done.");
    }
    
    private String getPathString(String ... menuItemPath) {
        String pathString = "";
        for (String pathElement : menuItemPath) {
            //If this is not the first element o the menuItemPath, append an arrow to the previous element
            if (!"".equals(pathString)) {
                pathString += "-->";
            }
            pathString += pathElement;
        }
        return pathString;
    }
}