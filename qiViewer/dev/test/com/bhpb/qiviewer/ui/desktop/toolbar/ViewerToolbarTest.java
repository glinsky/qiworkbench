/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.ui.desktop.toolbar;

import com.bhpb.qiviewer.QiViewerSwingTestBase;
import java.util.logging.Logger;
import org.fest.swing.fixture.JToolBarFixture;
import org.junit.After;
import org.junit.Before;

import org.junit.Ignore;
import org.junit.Test;

public class ViewerToolbarTest extends QiViewerSwingTestBase {
    private static final Logger logger = Logger.getLogger(ViewerToolbarTest.class.toString());
    
    @Before
    @Override
    public void setUp() {
        super.setUp();
    }
    
    @After
    @Override
    public void tearDown() {
        super.tearDown();
    }
    
    @Test
    @Ignore("This test case is disabled pending information on JToolBar changes in Fest 1.01a")
    public void test_Viewer_FileMenuItems() {
        JToolBarFixture toolbar = new JToolBarFixture(getFrameFixture().robot,"toolbar");

        /*
        //viewAll button test
        toolbar.button("viewAll").click();
        super.closeUnimplementedCodeWarning();
        //zoomRubber button test
        toolbar.button("zoomRubber").click();
        super.closeUnimplementedCodeWarning();
        
        //zoomIn button test
        toolbar.button("zoomIn").click();
        super.closeUnimplementedCodeWarning();

        //zoomOut button test
        toolbar.button("zoomOut").click();
        super.closeUnimplementedCodeWarning();

        //stepPlus button test
        toolbar.button("stepPlus").click();
        super.closeUnimplementedCodeWarning();

        //stepMinus button test
        toolbar.button("stepMinus").click();
        super.closeUnimplementedCodeWarning();

        //showLayer button test
        toolbar.button("showLayer").click();
        super.closeUnimplementedCodeWarning();

        //hideLayer button test
        toolbar.button("hideLayer").click();
        super.closeUnimplementedCodeWarning();

        //pickPBP button test
        toolbar.button("pickPBP").click();
        super.closeUnimplementedCodeWarning();

        //snapShot button test
        toolbar.button("snapShot").click();
        super.closeUnimplementedCodeWarning();

        //help button test
        toolbar.button("help").click();
        super.closeUnimplementedCodeWarning();
        */
    }
}