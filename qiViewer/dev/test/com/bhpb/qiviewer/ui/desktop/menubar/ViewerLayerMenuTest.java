/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.desktop.menubar;

import com.bhpb.qiviewer.QiViewerSwingTestBase;
import com.bhpb.qiviewer.ui.layer.CreateHorizonsDialog;
import java.util.logging.Logger;
import org.fest.swing.finder.WindowFinder;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.JInternalFrameFixture;
import org.fest.swing.fixture.JPanelFixture;
import org.junit.Test;

import static org.junit.Assert.*;

public class ViewerLayerMenuTest extends QiViewerSwingTestBase {

    static final transient Logger logger =
            Logger.getLogger(ViewerLayerMenuTest.class.getName());

    @Test
    public void test_Viewer_LayerMenuItems() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ie) {
            fail("While waiting for QiViewer Desktop to appear, caught InterruptedException: " + ie.getMessage());
        }

        JInternalFrameFixture viewerDesktopFixture = new JInternalFrameFixture(getFrameFixture().robot, "QiViewerDesktop");

        selectMenuItem(viewerDesktopFixture, "Layer", "Property ...");
        closeMessageBoxWithOk();

        selectMenuItem(viewerDesktopFixture, "Layer", "Create New Horizon");

        DialogFixture horizonDlg =
                WindowFinder.findDialog(CreateHorizonsDialog.class).using(viewerDesktopFixture.robot);
        JPanelFixture panelFixture = new JPanelFixture(viewerDesktopFixture.robot,
                ((CreateHorizonsDialog) horizonDlg.component()).getFileSelectorPanel());
        assertNotNull(panelFixture);
        panelFixture.button("Cancel").targetCastedTo(javax.swing.JButton.class).doClick();

        selectMenuItem(viewerDesktopFixture, "Layer", "Increment +");

        selectMenuItem(viewerDesktopFixture, "Layer", "Increment -");

        selectMenuItem(viewerDesktopFixture, "Layer", "Ensemble +");
        closeMessageBoxWithOk();

        selectMenuItem(viewerDesktopFixture, "Layer", "Hide Layers");
        closeMessageBoxWithOk();

        selectMenuItem(viewerDesktopFixture, "Layer", "Show Layers");
        closeMessageBoxWithOk();

        selectMenuItem(viewerDesktopFixture, "Layer", "Delete Layers");
        closeMessageBoxWithOk();

        selectMenuItem(viewerDesktopFixture, "Layer", "Move Layers To Top");
        closeMessageBoxWithOk();

        selectMenuItem(viewerDesktopFixture, "Layer", "Move Layers To Bottom");
        closeMessageBoxWithOk();
    }
}