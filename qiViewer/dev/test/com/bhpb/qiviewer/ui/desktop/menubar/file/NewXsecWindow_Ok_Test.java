/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.ui.desktop.menubar.file;

import com.bhpb.qiviewer.FestUtils;
import com.bhpb.qiviewer.QiViewerSwingTestBase;
import java.util.logging.Logger;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.JInternalFrameFixture;
import org.fest.swing.fixture.JMenuItemFixture;

import org.junit.After;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.*;

public class NewXsecWindow_Ok_Test extends QiViewerSwingTestBase {
    private static final Logger logger = Logger.getLogger(MenuTest.class.toString());
    private static final String TEST_BHPSU_FILE_NAME = "gathers.dat";
    
    @Before
    @Override
    public void setUp() {
        super.setUp();
    }
    
    @After
    @Override
    public void tearDown() {
        super.tearDown();
    }
    
    @Test
    public void test_Viewer_FileMenuItems() {
        logger.info("Getting viewerDesktopFixture...");
        
        JInternalFrameFixture viewerDesktopFixture = new JInternalFrameFixture(getFrameFixture().robot, "QiViewerDesktop");
        
        JMenuItemFixture menuItem = viewerDesktopFixture.menuItemWithPath("File","New X-Section Window", "BHP-SU");
        assertNotNull(menuItem);
        
        logger.info("Got JMenuItemFixture for 'File-->New X-Section Windows-->BHP-SU'");
        menuItem.click();
        logger.info("Clicked!");
        
        DialogFixture dialog = FestUtils.getDialog(getFrameFixture().robot);
        assertNotNull(dialog);
        dialog.requireVisible();
        
        logger.info("Waiting 2 seconds for FileChooserDialog to appear...");
        sleepFor(2000); // wait 2 seconds, if you start typing immediately, the FileChooserDialog will be cleared after the first few letters
        
        logger.info("Simulating typing into fileNameField: " + TEST_BHPSU_FILE_NAME);
        FestUtils.simulateTyping(dialog.textBox("fileNameField"), TEST_BHPSU_FILE_NAME);
        
        logger.info("Clicking openFileButton");
        dialog.button("openFileButton").targetCastedTo(javax.swing.JButton.class).doClick();
        
        int nSecs = 20;
        logger.info("Waiting " + nSecs + " seconds for DatasetProperties editor dialog to appear...");
        sleepFor(nSecs * 1000); //wait 5 seconds - if test proceeds before this dialog is properly created and visible, misleading errors occur
        
        logger.info("Getting dialog.");
        //getFrameFixture().robot.printer().printComponents(System.out);
        dialog = FestUtils.getDialog(getFrameFixture().robot,"layerPropertiesDialog");
        assertNotNull(dialog);
        dialog.requireVisible();
        logger.info("Dialog is not null and is visible.");
        
        logger.info("Waiting 2 seconds for model to update...");
        sleepFor(2000);
        
        //logger.info("Limiting the first two header keys to 3015 and 1350, respectively");
        //ristricted range removed from test pending resolution of possible FEST-Swing bug
        //dialog.panel("contentPanel").panel("seismicDatasetPropertiesPanel").panel("headerKeys").table().cell(row(2).column(0));
        
        logger.info("Clicking 'Ok' (load file)...");
        dialog.panel("contentPanel").panel("btnPanel").button("Ok").click();
    }
}