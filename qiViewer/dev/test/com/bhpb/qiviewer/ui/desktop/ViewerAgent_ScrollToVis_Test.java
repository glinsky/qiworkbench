/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.qiviewer.adapter.geoio.GeoIODirector;
import com.bhpb.qiviewer.adapter.geoio.GeoIOTransactionResult;
import com.bhpb.qiviewer.ui.desktop.menubar.file.*;
import com.bhpb.qiviewer.QiViewerSwingTestBase;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.*;

public class ViewerAgent_ScrollToVis_Test extends QiViewerSwingTestBase {
    private static final Logger logger = Logger.getLogger(MenuTest.class.toString());
    private static final String TEST_BHPSU_FILE_NAME = "/scratch/qiProjects/demo/datasets/gathers.dat";
    
    @Before
    @Override
    public void setUp() {
        super.setUp();
    }
    
    @After
    @Override
    public void tearDown() {
        super.tearDown();
    }
    
    @Test
    public void test_ScrollToVisiblePoint() {
        logger.info("Creating geoIOdirector.");
        GeoIODirector geoIODirector = new GeoIODirector(agent);
        LayerProperties result1 = geoIODirector.readLayerProperties(TEST_BHPSU_FILE_NAME);
        GeoIOTransactionResult result = geoIODirector.readData(result1, QIWConstants.OPEN_WINDOW, false, true);
        if (result.isErrorCondition()) {
            fail("Result of issueGeoIOread was failure: " + result.getErrorMsg());
        }
        String winID = result.getWindowID();
        assertNotNull(winID);
        String layerID = result.getLayerID();
        assertNotNull(layerID);
        
        //agent.setLayerListWidth(winID, 200);
        agent.scrollToVisiblePoint(winID, 2500);
        sleepFor(10000);
    }
}