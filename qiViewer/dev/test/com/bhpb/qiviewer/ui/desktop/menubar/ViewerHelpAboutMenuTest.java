/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.ui.desktop.menubar;

import com.bhpb.qiviewer.QiViewerSwingTestBase;
import com.bhpb.qiviewer.ui.about.HelpAbout;
import java.util.logging.Logger;
import org.fest.swing.finder.WindowFinder;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.JInternalFrameFixture;
import org.fest.swing.fixture.JMenuItemFixture;
import org.junit.After;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.*;

public class ViewerHelpAboutMenuTest extends QiViewerSwingTestBase {
    private static final Logger logger = Logger.getLogger(ViewerHelpAboutMenuTest.class.toString());
    
    @Before
    @Override
    public void setUp() {
        super.setUp();
    }
    
    @After
    @Override
    public void tearDown() {
        super.tearDown();
    }
    
    @Test
    public void test_Viewer_HelpMenuItems() {
        logger.info("Getting viewerDesktopFixture...");
        JInternalFrameFixture viewerDesktopFixture = new JInternalFrameFixture(getFrameFixture().robot, "QiViewerDesktop");
        
        JMenuItemFixture helpAboutFixture = viewerDesktopFixture.menuItemWithPath("Help","About ...");
        assertNotNull(helpAboutFixture);
        logger.info("Got JMenuItemFixture for 'Help-->About'");
        
        logger.info("Clicking 'Help->About'");
        helpAboutFixture.click();
        logger.info("Clicked!");
        
        DialogFixture helpAboutDlgFixture = 
                WindowFinder.findDialog(HelpAbout.class).using(viewerDesktopFixture.robot);
        assertNotNull(helpAboutDlgFixture);
        helpAboutDlgFixture.requireVisible();
        
        logger.info("Clicking License");
        helpAboutDlgFixture.button("License").targetCastedTo(javax.swing.JButton.class).doClick();
        
        logger.info("Clicking Close");
        helpAboutDlgFixture.button("Close").targetCastedTo(javax.swing.JButton.class).doClick();

        helpAboutDlgFixture.requireNotVisible();
        
        selectMenuItem(viewerDesktopFixture, "Help", "Open Help ...");
        closeMessageBoxWithOk();
    }
}