/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.qiviewer.adapter.geoio.GeoIODirector;
import com.bhpb.qiviewer.adapter.geoio.GeoIOTransactionResult;
import com.bhpb.qiviewer.ui.desktop.menubar.file.*;
import com.bhpb.qiviewer.QiViewerSwingTestBase;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.awt.Dimension;
import java.awt.Point;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.*;

public class SetWindowPropertiesTest extends QiViewerSwingTestBase {

    private static final Logger logger = Logger.getLogger(MenuTest.class.toString());
    private static final String TEST_BHPSU_FILE_NAME = "/scratch/qiProjects/demo/datasets/gathers.dat";

    @Before
    @Override
    public void setUp() {
        super.setUp();
    }

    @After
    @Override
    public void tearDown() {
        super.tearDown();
    }

    private GeoIOTransactionResult openWindow(GeoIODirector geoIODirector, String fileName) {
        return addLayer(geoIODirector, fileName, QIWConstants.OPEN_WINDOW);
    }

    private GeoIOTransactionResult addLayer(GeoIODirector geoIODirector, String fileName) {
        return addLayer(geoIODirector, fileName, QIWConstants.ADD_LAYER);
    }

    private GeoIOTransactionResult addLayer(GeoIODirector geoIODirector, String fileName, String ACTION) {
        LayerProperties layerProps = geoIODirector.readLayerProperties(TEST_BHPSU_FILE_NAME);
        GeoIOTransactionResult result = geoIODirector.readData(layerProps, ACTION, false, true);
        if (result.isErrorCondition()) {
            fail("Result of issueGeoIOread was failure: " + result.getErrorMsg());
        }
        return result;
    }

    @Test
    public void test_Setting_Viewer_And_Window_Properties() {
        logger.info("Creating geoIOdirector.");
        GeoIODirector geoIODirector = new GeoIODirector(agent);

        //load first layer
        GeoIOTransactionResult result = openWindow(geoIODirector, TEST_BHPSU_FILE_NAME);
        String winID = result.getWindowID();
        assertNotNull(winID);
        String layerID1 = result.getLayerID();
        assertNotNull(layerID1);

        agent.updateQiViewerLocation(50, 75);
        sleepFor(2000); // wait for event dispatch and GUI update

        Point desktopLoc = agent.getViewerDesktop().getLocation();

        assertEquals(desktopLoc.getX(), 50.0, 0.0);
        assertEquals(desktopLoc.getY(), 75.0, 0.0);

        agent.updateQiViewerSize(456, 123);
        sleepFor(2000); // wait for event dispatch and GUI update

        Dimension desktopSize = ((ViewerDesktop) agent.getViewerDesktop()).getDesktopPaneSize();

        assertEquals(456.0, desktopSize.getWidth(), 0.0);
        assertEquals(123.0, desktopSize.getHeight(), 0.0);

        agent.updateWindowLocation(winID, 10, 20);

        agent.updateWindowSize(winID, 256, 128);
        ViewerWindow window = agent.getValidWindow(winID);
        assertEquals("Window height: ", 128.0, window.getSize().getHeight(), 0.0);
        assertEquals("Window width: ", 256.0, window.getSize().getWidth(), 0.0);
    }
}
