/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.qiviewer.QiViewerSwingTestBase;
import java.awt.Dimension;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import org.junit.After;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.*;

public class ViewerDesktopStatePersistanceTest extends QiViewerSwingTestBase {
    private static final Logger logger 
            = Logger.getLogger(ViewerDesktopStatePersistanceTest.class.toString());
    
    @Before
    @Override
    public void setUp() {
        super.setUp();
    }
    
    @After
    @Override
    public void tearDown() {
        super.tearDown();
    }
    
    @Test
    public void test_Viewer_setVisible() throws Exception {
        if (agent.isInitFinished() == false) {
            fail("Component init did not finish after 5 seconds.");
        }
        
        if (agent.isInitSuccessful()) {
            int[] initialGeometry = new int[4];
            initialGeometry[0] = getDesktop().getX();
            initialGeometry[1] = getDesktop().getY();
            initialGeometry[2] = getDesktop().getHeight();
            initialGeometry[3] = getDesktop().getWidth();
            
            //persist the state
            String state = getDesktop().saveState();
            
            //modify the desktop's state and assert that the geometry has changed
            getDesktop().setLocation(initialGeometry[0]+1,initialGeometry[1]+1);
            getDesktop().setSize(new Dimension(initialGeometry[2]+1,initialGeometry[3]+1));
            assertTrue(getDesktop().getX() == initialGeometry[0]+1);
            assertTrue(getDesktop().getY() == initialGeometry[1]+1);
            assertTrue(getDesktop().getSize().getWidth() == initialGeometry[2]+1);
            assertTrue(getDesktop().getSize().getHeight() == initialGeometry[3]+1);

            assertTrue(state != null);
            
            InputStream inputStream = null;
            try {
                inputStream = new ByteArrayInputStream(state.getBytes("UTF-8"));         
            } catch (Exception ex) {
                fail("Cannot convert state String to inputStream because of: " + ex.getMessage());
            }
            
            assertTrue(inputStream != null);
                
            try {
                Element rootNode = getXMLTree(inputStream);
                getDesktop().importQiViewerState(rootNode);
            } catch (Exception ex) {
                fail("Cannot convert state String to w3c DOM Element because of: " + ex.getMessage());
            }
            
            int[] finalGeometry = new int[4];
            finalGeometry[0] = getDesktop().getX();
            finalGeometry[1] = getDesktop().getY();
            finalGeometry[2] = getDesktop().getHeight();
            finalGeometry[3] = getDesktop().getWidth();
            
            assertEquals(initialGeometry[0], finalGeometry[0]);
            assertEquals(initialGeometry[1], finalGeometry[1]);
            assertEquals(initialGeometry[2], finalGeometry[2]);
            assertEquals(initialGeometry[3], finalGeometry[3]);
        }
    }
    
   /**
     * Builds an XML tree from the InputStream and returns the root Element,
     * without using validation.
     *
     * @param ins InputStream containing the XML document
     * @return root element of the XML document
     * @throws FactoryConfigurationError
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private static Element getXMLTree(InputStream ins)
            throws FactoryConfigurationError, ParserConfigurationException,
            SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        DocumentBuilder dbuilder = factory.newDocumentBuilder();
        Document doc = dbuilder.parse(ins);
        Element root = doc.getDocumentElement();
        root.normalize();
        return root;
    }
}