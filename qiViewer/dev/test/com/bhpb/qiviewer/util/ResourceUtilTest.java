/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Logger;
import junit.framework.TestCase;

public class ResourceUtilTest extends TestCase {
    static final String STUB_COPYRIGHT_TEXT = "This is a stub copyright notice file for testing the ResourceUtil class.\n";
    static final String baseDir = System.getProperty("basedir");
    static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;
    static final String validFileName = baseTestFilePath + "CopyrightNotice.html";
    private static final Logger logger = Logger.getLogger(ResourceUtilTest.class.toString());
    public ResourceUtilTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
    }
    
    @Override
    protected void tearDown() throws Exception {
    }
    
    public void testConstructor() {
        ResourceUtil util = new ResourceUtil();
        assertNotNull(util);
    }
    
    public void testGetInputStreamAsString() throws FileNotFoundException, IOException {
        //test with valid file
        String copyrightNoticeHTML;
        InputStream inputStream = new FileInputStream(new File(validFileName));
        assertNotNull("InputStream for file " + validFileName + " was null after instantiation.");
        copyrightNoticeHTML = ResourceUtil.getInputStreamAsString(inputStream);
        assertNotNull("ResourceUtil.getInputStreamAsString(InputStream) returned a null string",copyrightNoticeHTML);
        assertTrue("getInputStreamAsString(InputStream) returned a non-null string that did not match the expected content",
                STUB_COPYRIGHT_TEXT.compareTo(copyrightNoticeHTML) == 0);
        inputStream.close();
        
        //now test with invalid inputStream
        inputStream = null;
        try {
            //eliminated dodgy warning from findbugs
            /*copyrightNoticeHTML =*/ ResourceUtil.getInputStreamAsString(inputStream);
            fail("getInputStreamAsString(InputStream) did not throw an IllegalArgumentException when InputStream was null");
        } catch (IllegalArgumentException iae) {
            //do nothing, this is the passing case
        }
    }
    
    public void testGetResourceAsString() throws Exception {
        URL resourceURL = new URL("file://" + baseTestFilePath);
        System.out.println("URL: " + resourceURL.toString());
        URLClassLoader urlClassLoader = URLClassLoader.newInstance(new URL[] {resourceURL}, ResourceUtil.class.getClassLoader());
        logger.info("classLoader: " + urlClassLoader.toString());
        logger.info("resource: " + resourceURL.toString());
        ResourceUtil.getResourceAsString(urlClassLoader, "CopyrightNotice.html");

        //try with invalid resource
        try {
            ResourceUtil.getResourceAsString(urlClassLoader, "resource-which-does-not-exist");
            fail("getResourceAsString did not throw an IllegalArgumentException when loading of a non-existant file as a resource was attempted");
        } catch (IllegalArgumentException iae) {
            // do nothing, this is the passing case
        } catch (ClassCastException cce) {
            fail("getResourceAsString threw ClassCastException: expected URLClassLoader but got: " + ResourceUtil.class.getClassLoader().getClass().getName());
        }
    }
}