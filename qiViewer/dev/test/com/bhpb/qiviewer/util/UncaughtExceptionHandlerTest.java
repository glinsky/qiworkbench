/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.util;

import junit.framework.TestCase;
import org.fest.swing.core.BasicRobot;
import org.fest.swing.core.Robot;
import org.fest.swing.fixture.JOptionPaneFixture;

public class UncaughtExceptionHandlerTest extends TestCase {
    private UncaughtExceptionHandler handlerWithPrint = null;
    private UncaughtExceptionHandler handlerWithoutPrint = null;
    private Robot robot = null; 
    private static final String TEST_EXCEPTION_MSG = "This is a test Exception for the UncaughtExceptionHandler.";
    
    public UncaughtExceptionHandlerTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        handlerWithPrint = new UncaughtExceptionHandler(true);
        handlerWithoutPrint = new UncaughtExceptionHandler(false);
    }

    @Override
    protected void tearDown() throws Exception {
    }

    public void testHandleException() {
        handlerWithPrint.handleException(new RuntimeException(TEST_EXCEPTION_MSG), false);
        handlerWithoutPrint.handleException(new RuntimeException(TEST_EXCEPTION_MSG), false);
        
        //generate messagebox from another Thread so that the TestCase's thread, which
        //needs to invoke FEST to close the MessageBox, is not blocked
        robot = BasicRobot.robotWithNewAwtHierarchy();
        new Thread(new Runnable(){
            public void run() {
                Thread.setDefaultUncaughtExceptionHandler(handlerWithPrint);
                //test by throwing a RuntimeException, which invokes uncaughtException()
                //which will have the side effect of also calling handleException(ex, true)
                throw new RuntimeException(TEST_EXCEPTION_MSG);
                //handlerWithPrint.handleException(new RuntimeException(TEST_EXCEPTION_MSG), true);
            }
        }).start();

        //wait for the messagebox to appear before attempting to locate and Cancel it
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            fail("Interrupted while waiting 2 seconds for UncaughtExceptionHandler MessageBox: " + ie.getMessage());
        }
        
        //JOptionPaneFixture optionPane = JOptionPaneFinder.findOptionPane().withTimeout(10000).using(robot);
        
        JOptionPaneFixture optionPane = new JOptionPaneFixture(robot);
        assertNotNull(optionPane);
        optionPane.cancelButton();

        //workaround for FEST bug - get a new robot before trying to get a second JOptionPane fixture
        robot.cleanUp();
        robot = BasicRobot.robotWithNewAwtHierarchy();
        new Thread(new Runnable(){
            public void run() {
                handlerWithoutPrint.handleException(new RuntimeException(TEST_EXCEPTION_MSG), true);
            }
        }).start();

        //wait for the messagebox to appear before attempting to locate and Cancel it
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            fail("Interrupted while waiting 2 seconds for UncaughtExceptionHandler MessageBox: " + ie.getMessage());
        }
        
        //optionPane = JOptionPaneFinder.findOptionPane().withTimeout(10000).using(robot);
        optionPane = new JOptionPaneFixture(robot);
        optionPane.cancelButton();
        robot.cleanUp();
        
        //assert that the length of the document listing exceptions is the same as the number of exceptions handled
        int exceptionDocLength = handlerWithPrint.getExceptionDocument().getLength();
        assertTrue(exceptionDocLength != 0);
        
        int nExceptionsHandled = handlerWithPrint.getNumHandledExceptions();
        assertTrue(2 == nExceptionsHandled);
        
        //assert that the length of the document listing exceptions is the same as the number of exceptions handled
        assertTrue(handlerWithoutPrint.getExceptionDocument().getLength() != 0);
        assertTrue(2 == handlerWithoutPrint.getNumHandledExceptions());
    } 
}