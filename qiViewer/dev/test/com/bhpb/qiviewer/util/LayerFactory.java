/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.util;

import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.layer.sync.SeismicXsecSyncProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetPropertiesFactory;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParametersFactory;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.util.GeoIOAdapter;

public class LayerFactory {
    /**
     * Creates a minimal SeismicLayer which belongs to its own unique LayerSyncGroup.
     * 
     * @param orientation
     * @return
     */
    public static Layer createSeismicLayer(GeoDataOrder orientation) {
        
        DatasetProperties dsProperties = DatasetPropertiesFactory.createTestSeismicProperties(orientation);
        SeismicXsecDisplayParameters layerDisplayParams = SeismicXsecDisplayParametersFactory.
                createLayerDisplayParameters(dsProperties);
        LocalSubsetProperties subsetProps = LocalSubsetPropertiesFactory.createLocalSubsetPropertiesTestData(0);
        LayerSyncProperties syncProps = new SeismicXsecSyncProperties();
        DataObject[] wiggles = new DataObject[1];
        wiggles[0] = DataObjectFactory.createDataObject();
        GeoIOAdapter ioAdapter = null;
        WindowModel winModel = new WindowModel("qiViewer Test", dsProperties.getMetadata().getGeoDataOrder());
        
        return new SeismicLayer(dsProperties,
            layerDisplayParams,
            subsetProps,
            syncProps,
            wiggles,
            ioAdapter,
            orientation == GeoDataOrder.MAP_VIEW_ORDER,
            new LayerSyncGroup(),
            winModel);
    }
}