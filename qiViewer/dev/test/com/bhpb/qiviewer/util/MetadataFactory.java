/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.util;

import com.bhpb.geoio.datasystems.DataSystemConstants.GeoKeyType;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Factor for creation of various MetadataFactory instances used to test the
 * associated controller and view functionality.
 */
public class MetadataFactory {
    
    public MetadataFactory() {
    }
    
    public static SeismicFileMetadata createTestSeismicMetadata(GeoDataOrder orientation) {
        String geoFilePath = "/scratch/qiProjects/demo///datasets/gathers.dat";
        String geoFileName = "gathers";
        FileSystemConstants.GeoFileType geoFileType = FileSystemConstants.GeoFileType.SEISMIC_GEOFILE;
        
        ArrayList<String> headerKeys = new ArrayList<String>();
        headerKeys.add("ep");
        headerKeys.add("cdp");
        headerKeys.add("offset");
        headerKeys.add("tracl");
        
        HashMap<String, DiscreteKeyRange> keyRangeMap = new HashMap<String, DiscreteKeyRange>();
        keyRangeMap.put("ep", new DiscreteKeyRange(3015.0, 3016.0, 1.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("cdp", new DiscreteKeyRange(1350.0, 1450.0, 1.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("offset", new DiscreteKeyRange(50.0, 3950.0, 100.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("tracl", new DiscreteKeyRange(0, 8000.0, 4.0, GeoKeyType.REGULAR_TYPE));
                
        SeismicFileMetadata metadata = new SeismicFileMetadata(geoFilePath, 
                geoFileName, 
                geoFileType, 
                orientation,
                headerKeys, 
                keyRangeMap,
                "dat",
                new ArrayList<String>());
        
        return metadata;
    }
    
    public static SeismicFileMetadata createTestSeismicMetadataWithHorizons() {
        String geoFilePath = "/scratch/qiProjects/demo///datasets/gathers.dat";
        String geoFileName = "gathers";
        FileSystemConstants.GeoFileType geoFileType = FileSystemConstants.GeoFileType.SEISMIC_GEOFILE;
        FileSystemConstants.GeoDataOrder geoDataOrder = FileSystemConstants.GeoDataOrder.CROSS_SECTION_ORDER;
        
        ArrayList<String> headerKeys = new ArrayList<String>();
        headerKeys.add("ep");
        headerKeys.add("cdp");
        headerKeys.add("offset");
        headerKeys.add("tracl");
        
        HashMap<String, DiscreteKeyRange> keyRangeMap = new HashMap<String, DiscreteKeyRange>();
        keyRangeMap.put("ep", new DiscreteKeyRange(3015.0, 3016.0, 1.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("cdp", new DiscreteKeyRange(1350.0, 1450.0, 1.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("offset", new DiscreteKeyRange(50.0, 3950.0, 100.0, GeoKeyType.REGULAR_TYPE));
        keyRangeMap.put("tracl", new DiscreteKeyRange(0, 8000.0, 4.0, GeoKeyType.REGULAR_TYPE));
                
        SeismicFileMetadata metadata = new SeismicFileMetadata(geoFilePath, 
                geoFileName, 
                geoFileType, 
                geoDataOrder,
                headerKeys, 
                keyRangeMap,
                "dat",
                createTestHorizons(1));
        
        return metadata;
    }
    
    private static ArrayList<String> createTestHorizons(int nHorizons) {
        ArrayList<String> hlist = new ArrayList<String>(nHorizons);
        for (int i=0;i<nHorizons;i++) {
            hlist.add("STUB_HORIZON_"+Integer.valueOf(i).toString());
        }
        return hlist;
    }
}