/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer;

import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiSpaceDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.messageFramework.MessageDispatcher;
import com.bhpb.qiworkbench.workbench.WorkbenchManager;
import java.io.File;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.*;

public abstract class QiViewerTestBase {

    private static final Logger logger = Logger.getLogger(QiViewerTestBase.class.toString());
    protected static final String baseDir = System.getProperty("basedir");
    protected static final String baseTestFilePath = baseDir + File.separator + "test" + File.separator + "data" + File.separator;
    protected boolean setUpComplete = false;
    private IComponentDescriptor agentCompDesc;
    protected ViewerAgent agent;
    protected WorkbenchManager workbenchMgr;
    private static final String headlessKey = "java.awt.headless";
    private boolean launchWorkbench = true;

    @Before
    public void setUp() {
        if (!setUpComplete) {
            setUpMessageDispatcher();
            try {
                workbenchMgr = initWbMgr();
                agent = new ViewerAgent(true);
            } catch (Throwable ex) {
                ex.printStackTrace();
                fail("Caught exception while initializing component: " + ex.getMessage());
            }
        }
    }

    @After
    public void tearDown() { // fix for FindBugs IJU_TEARDOWN_NO_SUPER
    }

    private WorkbenchManager initWbMgr() {
        //select the test project 'demo' so that the filechooserservice can be used
        String qispaceDir = "/scratch/qiProjects/demo";
        String selectedServer = "http://localhost:8080";

        boolean use_local_io = true;
        String tomcatLoc = use_local_io ? QIWConstants.LOCAL_PREF : QIWConstants.REMOTE_PREF;
        MessageDispatcher.getInstance().setTomcatURL(selectedServer);
        MessageDispatcher.getInstance().setLocationPref(tomcatLoc);

        //Add qiSpace to preferences. Do before set default qiSpace in prefs
        //Note:QibwPreferences.addQispace() will check if already on the list
        String qispaceKind = QiSpaceDescriptor.PROJECT_KIND;

        WorkbenchManager wbMgr = WorkbenchManager.getInstance();
        int firstAttemptNum = 1;
        int maxAttempts = 10;
        assertNotNull(wbMgr);
        for (int attempt = firstAttemptNum; (attempt < maxAttempts) && (wbMgr.isInitFinished() == false); attempt++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                logger.warning("Interrupted while waiting for wbMgr to init, skipping wait attempt # " + attempt + " of " + maxAttempts);
            }
        }

        assertTrue("wbMgr failed to init after " + maxAttempts + " seconds", wbMgr.isInitFinished());
        assertTrue("wbMgr finished init but was not successful", wbMgr.isInitSuccessful());

        logger.info("Adding qiSpaceDir, qispaceKind and selectedServer to wbMgr: " + qispaceDir + ", " + qispaceKind + ", " + selectedServer);
        wbMgr.addQispace(qispaceDir, qispaceKind, selectedServer);
        wbMgr.setQispace(qispaceDir, qispaceKind, selectedServer);

        if (launchWorkbench) {
            wbMgr.launchWorkbench();
        }

        return wbMgr;
    }

    private void setUpMessageDispatcher() {
        System.setProperty("deployServerURL", "http://localhost:8080");
        System.setProperty("install", "false");

        MessageDispatcher.mainNoGUI();

        MessageDispatcher dispatcher = MessageDispatcher.getInstance();
        waitForQiComponentInit(dispatcher, 10000, 1000, false); // wait up to 5 seconds for the MD to start up, checking in 1-second intervals for completion       
    }

    /**
     * Class constructor.
     *
     * @param testName name of the test method being invoked - used for formatting of JUnit reports
     * @param launchWorkbench if true, the workbench GUI will be displayed when setUp() is invoked
     * by some functionality such as the FileChooserService (although it probably should not be).
     */
    /*
    protected QiViewerTestBase(String testName, boolean launchWorkbench) {
    super(testName);
    this.launchWorkbench = launchWorkbench;
    }
     */
    /**
     * Wait for the specified amount of time for the component to initialize, checking
     * waiting for the given time increment between calls to comp.isInitFinished().
     * Returns if the component successfully finishes initialization within
     * the time limit, otherwise calls TestCase.fail() with a message indicating
     * whether the failure was due to timeout or unsuccessful init, with a
     * message indicating the cause if possible in the latter case.
     *
     * Causes the TestCase to fail() if parameter qiComponent does not implement getCID
     * (this is a workaround pending compAPI update)
     *
     * @param comp the component for which a new worker Thread will be started
     * @param the maximum amount of time to wait for component init to complete, in ms
     * @param increment the amount of time to Thread.sleep() between attempts to check for init completion
     * @param startComponent determines whether to create and start a new worker Thread for the component.  Generally false for singletons such as MessageDispatcher.
     *
     */
    private IComponentDescriptor waitForQiComponentInit(IqiWorkbenchComponent qiComponent, int timeout, int increment, boolean startComponent) {
        assertNotNull("Error - component" + qiComponent.toString() + " is null", qiComponent);
        assertTrue("timeout must be a positive value in millesconds", timeout > 0);

        if (startComponent) {
            new Thread((Runnable) qiComponent).start();
        }

        int elapsedTime = 0;

        while ((qiComponent.isInitFinished() == false) && (elapsedTime < timeout)) {
            try {
                elapsedTime += increment; // this is added BEFORE sleep in case it is repeatedly interrupted
                Thread.sleep(increment);
            } catch (InterruptedException ie) {
                logger.warning("Caught InterruptedException while waiting for " + qiComponent.toString() + " to initialize");
            }
            elapsedTime += increment;
        }

        if (elapsedTime >= timeout) {
            logger.warning("elapsedTime >= timeout, qiComponent has not finished initializing");
        }

        if (qiComponent.isInitFinished() == false) {
            fail("Component " + qiComponent.toString() + "init did not finish after 5 seconds.");
        }

        if (qiComponent.isInitSuccessful()) {
            // do nothing, this is the successful case
        } else {
            List exceptions = qiComponent.getInitExceptions();
            if (exceptions.size() > 0) {
                logger.info("Component " + qiComponent.toString() + " initialization failed, exceptions follow: ");
                for (int i = 0; i < exceptions.size(); i++) {
                    Exception ex = (Exception) exceptions.get(i);
                    logger.info(i + ") " + ex.getMessage());
                }
                fail("Component " + qiComponent.toString() + " initialization failed");
            } else {
                fail("Component " + qiComponent.toString() + " initialization failed but no exceptions were caught in init(): ");
            }
        }

        Class<?> clazz = qiComponent.getClass();
        try {
            Method mthd = clazz.getMethod("getCID", new Class[0]);

            //component descriptor is never used explicitly but must be retrieved or MessageDispatcher cannot route messages to it.
            String qiCompCID = (String) mthd.invoke(qiComponent);
            IComponentDescriptor compDesc = MessageDispatcher.getInstance().getComponentDesc(qiCompCID);
            assertNotNull("Error - " + qiComponent.toString() + " component descriptor is null", compDesc);
            return compDesc;
        } catch (Exception ex) {
            logger.warning("Unexpected exception " + ex + " caught attempting to invoke getCID() using reflection.");
            return null;
        }
    }

    /** Creates a new instance of ViewerAgentInitTest */
    public void initializeAgent() {
        assertNotNull("Error - ViewerAgent is null", agent);
        logger.info("ViewerAgent non-null, calling waitForQiComponentInit(agent, 5000, 1000, true)...");
        agentCompDesc = waitForQiComponentInit(agent, 5000, 1000, true);

        assertNotNull("Error - ViewerAgent component descriptor is null", agentCompDesc);
    }

    public void initializeWbMgr() {
        assertNotNull("Error - WbMgr is null", workbenchMgr);
        logger.info("WbMgr non-null, calling waitForQiComponentInit(WbMgr, 5000, 1000, false)...");
        waitForQiComponentInit(workbenchMgr, 5000, 1000, false);
    }

    protected void sleepFor(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ie) {
            fail("Test case was interrupted while waiting in qiViewerTestBase.sleepFor(" + ms + ").");
        }
    }
}