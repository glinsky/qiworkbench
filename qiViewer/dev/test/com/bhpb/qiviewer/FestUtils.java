/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer;

import javax.swing.JDialog;
import org.fest.swing.core.Robot;
import org.fest.swing.core.Settings;
import org.fest.swing.finder.JOptionPaneFinder;
import org.fest.swing.finder.WindowFinder;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.JOptionPaneFixture;
import org.fest.swing.fixture.JTextComponentFixture;

public final class FestUtils {
    private static Settings settings = new Settings();
    
    public static DialogFixture getDialog(Robot robot) {
        return WindowFinder.findDialog(JDialog.class).using(robot);
    }
    
    public static DialogFixture getDialog(Robot robot, String dlgName) {
        return WindowFinder.findDialog(dlgName).using(robot);
    }
    
    public static JOptionPaneFixture getOptionPane(Robot robot) {
        return JOptionPaneFinder.findOptionPane().using(robot);
    }
    
    /**
     * Simulate entering text into the textbox at 10 characters per second, then
     * reset the robotAutoDelay to its previous value.
     * @param textbox JTextComponentFixture which receives the keyboard input
     * @param text String which is entered into the textbox as if typed
     */
    public static synchronized void simulateTyping(JTextComponentFixture textbox, String text) {
        int robotDelay = settings.delayBetweenEvents();
        settings.delayBetweenEvents(100);
        textbox.enterText(text);
        settings.delayBetweenEvents(robotDelay);
    }
}