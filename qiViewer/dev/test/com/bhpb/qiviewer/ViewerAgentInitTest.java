/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer;

import java.util.logging.Logger;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.*;

public class ViewerAgentInitTest extends QiViewerTestBase {
    private static final Logger logger = Logger.getLogger(ViewerAgentInitTest.class.toString());
    
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Test
    public void test_ViewerAgent_Init() {
        assertNotNull("Error - ViewerAgent is null", agent);
        new Thread(agent).start();
        int elapsedTime = 0;
        
        while ((agent.isInitFinished() == false) && (elapsedTime < 10000)){
            try {
                elapsedTime += 1000; // this is added BEFORE sleep in case it is repeatedly interrupted
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                logger.warning("Caught InterruptedException");
            }
            elapsedTime += 1000;
        }
        
        if (agent.isInitFinished() == false) {
            fail("Component init did not finish after 5 seconds.");
        }
        
        if (agent.isInitSuccessful()) {
            // do nothing, this is the successful case
        } else {
            fail("Component initialization failed");
        }
        
        Thread.UncaughtExceptionHandler handler = agent.getUncaughtExceptionHandler();
        assertNotNull(handler);
    }
}