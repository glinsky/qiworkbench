/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer;

import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.testutils.MockQiComponent;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import static org.junit.Assert.*;

public class Agent_Save_And_Restore_State_Test extends QiViewerSwingTestBase {

    private static final Logger logger = Logger.getLogger(Agent_Save_And_Restore_State_Test.class.toString());
    private MockQiComponent cmdSender;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        cmdSender = setUpMockQiComponent();

        cmdSender.startComponent("MOCK1");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            logger.info(ie.getMessage());
        }

        //this must be done or else messages cannot be routed to/from mock
        cmdSender.getCID();
    }

    @After
    @Override
    public void tearDown() {
        super.tearDown();
    }

    @Test
    public void testRoundtrip() throws FileNotFoundException, IOException {
        if (agent.isInitFinished() == false) {
            fail("Component init did not finish after 5 seconds.");
        }

        if (agent.isInitSuccessful()) {
            int[] initialGeometry = new int[4];

            //Save the initial desktop geometry
            initialGeometry[0] = getDesktop().getX();
            initialGeometry[1] = getDesktop().getY();
            initialGeometry[2] = getDesktop().getHeight();
            initialGeometry[3] = getDesktop().getWidth();

            String initialState = sendSaveCompCmd();

            //Finally, restore the desktop geometry and verify that it matches the inital state.
            sendRestoreCompCmd(initialGeometry, initialState);
        } else {
            fail("Test failed - agent init was not successful");
        }
    }

    /**
     * Builds an XML tree from the InputStream and returns the root Element,
     * without using validation.
     *
     * @param ins InputStream containing the XML document
     * @return root element of the XML document
     * @throws FactoryConfigurationError
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private static Element getXMLTree(InputStream ins)
            throws FactoryConfigurationError, ParserConfigurationException,
            SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        DocumentBuilder dbuilder = factory.newDocumentBuilder();
        Document doc = dbuilder.parse(ins);
        Element root = doc.getDocumentElement();
        root.normalize();
        return root;
    }

    private QiWorkbenchMsg createSaveCompCmd() {
        String mockCID = cmdSender.getCID();
        String msgID = MsgUtils.genMsgID();
        return new QiWorkbenchMsg(mockCID, agent.getComponentDescriptor().getCID(), QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_CMD, msgID, QIWConstants.STRING_TYPE, "");
    }

    private QiWorkbenchMsg createRestoreCompCmd(Element rootNode) {
        String mockCID = cmdSender.getCID();
        String msgID = MsgUtils.genMsgID();
        //does this command even take a data type?
        return new QiWorkbenchMsg(mockCID, agent.getComponentDescriptor().getCID(), QIWConstants.CMD_MSG, QIWConstants.RESTORE_COMP_CMD, msgID, "", rootNode);
    }

    private MockQiComponent setUpMockQiComponent() {
        MockQiComponent mock = new MockQiComponent();
        assertNotNull(mock);
        mock.isInitFinished(); // test mock functionality

        return mock;
    }

    /**
     * Validate that the component desktop restored from the state String matches 
     * the specified initial geometry
     */
    private void sendRestoreCompCmd(int[] initialGeometry, String stateXML) throws FileNotFoundException, IOException {
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream(stateXML.getBytes("UTF-8"));
        } catch (Exception ex) {
            fail("Cannot convert state String to inputStream because of: " + ex.getMessage());
        }

        assertTrue(inputStream != null);

        try {
            Element rootNode = getXMLTree(inputStream);
            agent.processMsg(createRestoreCompCmd(rootNode));
        } catch (Exception ex) {
            fail("Cannot convert state String to w3c DOM Element because of: " + ex.getMessage());
        }

        //wait for message to be processed
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            logger.info(ie.getMessage());
        }

        int[] finalGeometry = new int[4];
        finalGeometry[0] = agent.getViewerDesktop().getX();
        finalGeometry[1] = agent.getViewerDesktop().getY();
        finalGeometry[2] = agent.getViewerDesktop().getHeight();
        finalGeometry[3] = agent.getViewerDesktop().getWidth();

        assertEquals("initialGeometry X was " + initialGeometry[0] + " but finalGeometry was " + finalGeometry[0],
                initialGeometry[0],
                finalGeometry[0]);
        assertEquals("initialGeometry Y was " + initialGeometry[1] + " but finalGeometry was " + finalGeometry[1],
                initialGeometry[1],
                finalGeometry[1]);
        assertEquals("initialGeometry height was " + initialGeometry[2] + " but finalGeometry was " + finalGeometry[2],
                initialGeometry[2],
                finalGeometry[2]);
        assertEquals("initialGeometry width was " + initialGeometry[3] + " but finalGeometry was " + finalGeometry[3],
                initialGeometry[3],
                finalGeometry[3]);
    }

    private String sendSaveCompCmd() throws FileNotFoundException, IOException {
        agent.processMsg(createSaveCompCmd());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            logger.info(ie.getMessage());
        }

        Object content = cmdSender.getResponse(QIWConstants.SAVE_COMP_CMD);

        assertNotNull(content);

        return content.toString();
    }
}