/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.testutils;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiMessageHandler;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 *
 * @author folsw9
 */
public class MockQiComponent  extends QiComponentBase implements IqiMessageHandler, Runnable {
    private static final Logger logger = Logger.getLogger(MockQiComponent.class.toString());
    
    // Messaging Manager for this adaptor
    private MessagingManager messagingMgr;
    // flag to tell run method to exit
    private boolean stopThread = false;
    
    //This delegate exists to add debugging code to a proxy of this class
    private IqiMessageHandler processorDelegate;
    
    /** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
    private ComponentDescriptor projMgrDesc = null;
    
    /** Metadata of project associated with */
    private QiProjectDescriptor qiProjDesc = new QiProjectDescriptor();
    
    private HashMap<String, Object> responseMap = new HashMap<String, Object>();
    
    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjDesc;
    }
    
    /** Unique, system wide ID (CID) */
    private String myCID = "";
    private static int saveAsCount = 0;
    
    public String getCID() {
        return myCID;
    }
    
    /** Creates a new instance of MockQiComponent */
    public MockQiComponent() {
    }
    
    public MessagingManager getMessagingMgr() {
        return messagingMgr;
    }
    
    public void init() {
        
        logger.info("Starting init...");
        try {
            QIWConstants.SYNC_LOCK.lock();
            
            messagingMgr = new MessagingManager();
            
            myCID = Thread.currentThread().getName();
            
            logger.info("myCID: " + myCID);
            // register self with the Message Dispatcher
            // This Mock is pretending to be a bhpViewer agent
            messagingMgr.registerComponent(QIWConstants.VIEWER_AGENT_COMP, QIWConstants.BHP_VIEWER_NAME, myCID);
            
            // notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in Bhp2DviewerAgent.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
        logger.info("Init ended.");
    }
    
    /**
     * Find the request matching the response and process the response based on the request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        /** Request that matches the response */
        IQiWorkbenchMsg request = null;
        
        // Check if a response. If so, process and consume response
        if (messagingMgr.isResponseMsg(msg)) {
            logger.info("Mock component '" + getCID() + "' received a response: " + msg.getCommand());
            request = messagingMgr.checkForMatchingRequest(msg);
            if (request == null)
                logger.info("Mock component response did not match a non-null request");
            else {
                logger.info("Mock component matched a non-null request.");
                responseMap.put(msg.getCommand(), msg.getContent());
            }
        } else {
            if (msg.getCommand().compareTo("SEND_TEST_RESPONSE") == 0) {
                logger.info("Request command matched SEND_TEST_REPONSE, sending test response...");
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, "TEST RESPONSE");
            } else {
                logger.info("No action for msg.getCommand() == " + msg.getCommand());
            }
        }
    }
    
    /** Initialize the 2D viewer, then start processing messages it receives.
     */
    public void run() {
        logger.info("Starting run...");
        
        // initialize the 2D viewer
        init();
        
        // process any messages received from other components
        while (stopThread == false) {
            
            //Check if associated with a PM. If not (because was restored), discover the PM
            //with a matching PID. There will be one once its GUI comes up.
            String myPid = QiProjectDescUtils.getPid(qiProjDesc);
            while (projMgrDesc == null && !myPid.equals("")) {
                //NOTE: Request will stay forwever on outstanding request list if no PM with matching PID.
                //      This can occur when restoring the workbench and the PM hasn't been restored
                //      or it is being restored but its GUI is not yet up.
                //Message Dispatcher will broadcast command to all PMs. Only PM with matching PID
                //will send back a response.
                messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
                try {
                    //wait and then try again
                    Thread.sleep(1000);
                    //if a message arrived, process it. It probably is the response from a PM.
                    if (!messagingMgr.isMsgQueueEmpty()) break;
                } catch (InterruptedException ie) {
                    continue;
                }
            }
            
            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            //if a data message with skip flag set to true, it will not be processed here
            //instead it will be claimed by calling getMatchingResponseWait
            if (msg != null) {
                // If it is a response with a timed-out request, dequeue it
                if (msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip()) {
                    if (messagingMgr.getMatchingOutstandingRequest(msg) == null) {
                        msg = messagingMgr.getNextMsg();
                        logger.warning("Consuming response message which lacks a matching outstanding request. Response msg: " + msg);
                    }
                } else { // it is a request or asynchronous response, process it
                msg = messagingMgr.getNextMsgWait();
                if (processorDelegate != null) {
                    logger.info("Processor delegate non-null, delegating processing through IqiMessageHandler interface " + processorDelegate);
                    processorDelegate.processMsg(msg);
                }
                else {
                    logger.info("Processor delegate is null, processing my own message");
                    processMsg(msg);
                }
                }
            }
        }
        
        logger.info("Run ended.");
    }
    
    /** Launch the mock component:
     *  <ul>
     *  <li>Start up the viewer's thread which will initialize the viewer.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before another thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public void startComponent(String cid) {
        logger.info("Starting new thread with cid: " + cid);
        Thread viewerThread = new Thread(this, cid);
        viewerThread.start();
        long threadId = viewerThread.getId();
        logger.info("MockQiComponent Thread-"+Long.toString(threadId)+" started");
        
        // When the viewer's init() is finished, it will release the lock
        // wait until the viewers's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
        logger.info("MockQiComponent init finished");
    }
    
    public void setProcessorDelegate(IqiMessageHandler msgHandler) {
        this.processorDelegate = msgHandler;
    }
    
    /**
     * Return the latest response to cmd which was received by this MockQiComponent.
     */
    
    public Object getResponse(String cmd) {
        return responseMap.get(cmd);
    }
}