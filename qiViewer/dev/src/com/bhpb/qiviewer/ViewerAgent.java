/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer;

import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.window.WindowSyncGroup;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.qiviewer.adapter.geoio.GeoIODirector;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import com.bhpb.qiviewer.ui.desktop.ViewerDesktop;
import com.bhpb.qiviewer.ui.movie.MovieController;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiviewer.util.UncaughtExceptionHandler;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
// JE FIELD can't seem to find this
//import com.sun.corba.se.impl.presentation.rmi.ExceptionHandler;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * qiWorkbench seismic viewer agent. The viewer is started as a thread by the
 * Messenger Dispatcher when it executes the "Activate viewer agent"
 * command.
 *
 */
public class ViewerAgent extends QiComponentBase implements IqiWorkbenchComponent, Runnable {

    private static final Logger logger = Logger.getLogger(ViewerAgent.class.getName());
    private static int saveAsCount = 0;
    private boolean stopThread = false;
    /** Component descriptor of associated qiProject Manager. null means no association with a PM has been made. */
    private ComponentDescriptor projMgrDesc = null;
    private GeoIODirector fileChooserDirector;
    private IMessagingManager messagingMgr;
    private Map<Layer, ViewerWindow> layerWindowMap = new HashMap<Layer, ViewerWindow>();
    private Map<String, Layer> layerIDmap = new HashMap<String, Layer>();
    private Map<String, ViewerWindow> windowIDmap = new HashMap<String, ViewerWindow>();
    private MessageProcessor messageProcessor;
    private MovieController movieController;
    private QiProjectDescriptor qiProjDesc = new QiProjectDescriptor();
    private String myCID = "";
    private UncaughtExceptionHandler uncaughtExceptionHandler;
    private ViewerDesktop viewerDesktop;
    private WindowSyncGroup defaultWindowSyncGroup = new WindowSyncGroup();
    private LayerSyncGroup defaultLayerSyncGroup = new LayerSyncGroup();

    /**
     * Class constructor which initializes an {@link UncaughtExceptionHandler} as the {@link ExceptionHandler}.
     *
     * @param printStackTraces is passed to the constructor of the <code>ExceptionHandler</code>
     * used by this <code>ViewerAgent</code>
     */
    public ViewerAgent(boolean printStackTraces) {
        uncaughtExceptionHandler = new UncaughtExceptionHandler(printStackTraces);
    }

    public void addViewerWindow(ViewerWindow viewerWindow) {
        windowIDmap.put(viewerWindow.getWinID(), viewerWindow);
    }

    /**
     * Opens a FileChooser for selecting data to be added as a new Layer to the current
     * active ViewerWindow.
     */
    public void addBhpSuLayer() {
        chooseXsectionFile(QIWConstants.ADD_LAYER);
    }

    /** Send a message to the Workbench Manager to remove this instance of qiViewer from the component tree and send a message to it to deactivate itself.
     */
    public void deactivateSelf() {
        //ask Workbench Manager to remove qiViewer from workbench GUI and then send it back to self to deactivate self
        IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.QUIT_COMPONENT_CMD, wbMgr, QIWConstants.STRING_TYPE,
                messagingMgr.getMyComponentDesc());
    }

    /**
     * Delegator method for invoking the FileChooserService, for instance, by the
     * CreateHorizonsDialog.
     * 
     * @param list
     * @param requesterType
     * @param requesterInstance
     */
    public void invokeFileChooser(ArrayList list, String requesterType, Object requesterInstance) {
        fileChooserDirector.showFileChooser(list, requesterType, requesterInstance);
    }

    /**
     * Opens a FileChooser, allowing selection of a File to be added in a new ViewerWindow
     * or as a new Layer of an existing ViewerWindow, as specified by the 'command' parameter.
     * 
     * @param command String indicating the action to be taken if a File is selected: QIWConstants.OPEN_WINDOW
     * or QIWConstants.ADD_LAYER.
     */
    public void chooseXsectionFile(String command) {
        if (QIWConstants.OPEN_WINDOW.equals(command)) {
            logger.fine("Choosing crossSection file - command is: " + QIWConstants.OPEN_WINDOW);
        } else if (QIWConstants.ADD_LAYER.equals(command)) {
            logger.fine("Choosing crossSection file - command is: " + QIWConstants.ADD_LAYER);
        } else {
            String errorMsg = "Unable to choose cross-section file due to unknown command: " + command;
            JOptionPane.showMessageDialog(viewerDesktop, errorMsg);
        }
        //logger.info("Getting qiProjectDescriptor from ViewerAgent");
        QiProjectDescriptor projDesc = getQiProjectDescriptor();
        if (projDesc == null) {
            logger.warning("projDesc == null");
        }

        String dsPath = QiProjectDescUtils.getDatasetsPath(projDesc);
        if (dsPath == null) {
            logger.warning("QiProjectDescriptor's dataset path is null");
        }

        fileChooserDirector.callFileChooser(getViewerDesktop(),
                dsPath,
                command,
                "dat",
                "BHP-SU data file",
                "Select bhpsu Data");
    }

    public void closeGUI() {
        if (viewerDesktop == null) {
            throw new UnsupportedOperationException("setGUI() must be called before closeGUI()");
        } else {
            java.awt.EventQueue.invokeLater(new Runnable() {

                public void run() {
                    viewerDesktop.dispose();
                }
            });
        }
    }

    /**
     * Gets the qiViewer's serialized state as xml
     *
     * @return xml representation of QiViewer's state, without header
     * 
     * @throws Exception if an error occurs while getting or serializing the ViewerAgent's state
     */
    public String genState() throws Exception {
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();

        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");

        //save the project descriptor
        String pdXml = XmlUtils.objectToXml(getQiProjectDescriptor());
        content.append(pdXml);

        try {
            content.append(viewerDesktop.saveState());
        } catch (Exception ex) {
            logger.warning("Unable to save ViewerDesktop state due to: " + ex.getMessage());
            throw ex;
        }

        content.append("</component>\n");

        // now pretty-print the XML String
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document doc = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(content.toString())));

        return XmlUtils.toFormattedXMlString(doc);
    }

    /**
     * Have the State Manager save the state of this qiComponent.
     */
    public void saveState() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_CMD, stMgrDesc,
                QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

    /**
     * Have the State Manager save the state of this qiComponent.
     */
    public void saveStateAsClone() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_AS_CLONE_CMD, stMgrDesc,
                QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

    /**
     * Have the State Manager save the state of this qiComponent and then quit the component.
     */
    public void saveStateThenQuit() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.SAVE_COMP_THEN_QUIT_CMD, stMgrDesc,
                QIWConstants.STRING_TYPE, messagingMgr.getMyComponentDesc());
    }

    /**
     * Generate state information into xml string format in repsonse to save as command
     * @return  String
     */
    public String genStateAsClone() throws Exception {
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        String displayName = "";
        saveAsCount++;
        if (saveAsCount == 1) {
            displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
        } else {
            displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
        }
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");

        //save the project descriptor
        String pdXml = XmlUtils.objectToXml(getQiProjectDescriptor());
        content.append(pdXml);

        try {
            content.append(viewerDesktop.saveState());
        } catch (Exception ex) {
            logger.warning("While saving ViewerDesktop state, caught: " + ex.getMessage());
            throw ex;
        }
        content.append("</component>\n");
        return content.toString();
    }

    public ViewerWindow getActiveWindow() {
        return viewerDesktop.getActiveWindow();
    }

    String getCID() {
        return myCID;
    }

    /**
     * Gets the {@link ComponentDescriptor} of this <code>ViewerAgent</code>.
     */
    public IComponentDescriptor getComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    public CursorSettings getCursorSettings() {
        return viewerDesktop.getCursorSettings();
    }

    public LayerSyncGroup getDefaultLayerSyncGroup() {
        return defaultLayerSyncGroup;
    }

    public WindowSyncGroup getDefaultWindowSyncGroup() {
        return defaultWindowSyncGroup;
    }

    MessageProcessor getMessageProcessor() {
        return messageProcessor;
    }

    /**
     * Gets the {@link MessagingManager} of this <code>ViewerAgent</code>
     */
    public IMessagingManager getMessagingManager() {
        return messagingMgr;
    }

    public MovieController getMovieController() {
        if (movieController == null) {
            movieController = new MovieController(viewerDesktop, this);
        }

        return movieController;
    }

    public ComponentDescriptor getProjMgrDesc() {
        return projMgrDesc;
    }

    /**
     * Returns the QiProjectDescriptor associated with this <code>ViewerAgent</code>.
     */
    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjDesc;
    }

    /**
     * Extract the project descriptor from the saved state. If the state
     * does not contain a project descriptor, form one from the implicit
     * project so backward compatible.
     *
     * @param node XML Node containing the serialized QiProjectDescriptor
     */
    public void loadQiProjectDescriptor(Node node) {
        QiProjectDescriptor projDesc = null;
        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
            child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                nodeName = child.getNodeName();
                if (nodeName.equals("QiProjectDescriptor")) {
                    projDesc = (QiProjectDescriptor) XmlUtils.XmlToObject(child);
                    break;
                }
            }
        }

        //Check if saved state has a project descriptor. If not, create
        //one using the implicit project so backward compatible.
        if (projDesc == null) {
            projDesc = new QiProjectDescriptor();
            //get path of implicit project from Message Dispatcher
            String projPath = getMessagingManager().getProject();
            QiProjectDescUtils.setQiSpace(projDesc, projPath);
            //leave relative location of project as "/"
            int idx = projPath.lastIndexOf("\\");
            if (idx == -1) {
                idx = projPath.lastIndexOf("/");
            }
            String projectName = projPath.substring(idx + 1);
            QiProjectDescUtils.setQiProjectName(projDesc, projectName);
        }
        qiProjDesc = projDesc;
    }

    public UncaughtExceptionHandler getUncaughtExceptionHandler() {
        return uncaughtExceptionHandler;
    }

    public String[] getValidLayerIDs() {
        return layerIDmap.keySet().toArray(new String[0]);
    }

    public String[] getValidWindowIDs() {
        return windowIDmap.keySet().toArray(new String[0]);
    }

    public Layer getValidLayer(String layerID) {
        Layer layer = layerIDmap.get(layerID);

        if (layer == null) {
            logger.info("No match found for layerID: " + layerID);
        }

        return layer;
    }

    public ViewerWindow getValidWindow(String windowID) {
        ViewerWindow window = windowIDmap.get(windowID);
        if (window == null) {
            throw new IllegalArgumentException("The window ID: " + windowID + " is not associated with a non-null ViewerWindow.");
        } else {
            return window;
        }

    }

    public ViewerDesktop getViewerDesktop() {
        return viewerDesktop;
    }

    /**
     * Import state saved in a XML file. Use existing viewer window. User not 
     * asked to save current state first, because after import, saving state 
     * will also save imported state. The preferred display name is not changed. 
     * Therefore, the name in the viewer window's title and component tree node
     * don't have to change.
     *
     * @param node XML containing saved state variables
     */
    void importState(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(ViewerDesktop.getXmlAlias())) {
                    //keep the associated project
                    viewerDesktop.importQiViewerState(child);
                //Not necessary to reset the title of the window, for the project didn't change
                }
            }
        }
    }

    /**
     * Initialize the viewer adaptor component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register it with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        logger.fine("Entering ViewerAgent.init...");
        logger.fine("Setting UncaughtExceptionHandler");
        Thread.currentThread().setUncaughtExceptionHandler(uncaughtExceptionHandler);
        logger.fine("Setting SYNC_LOCK");
        QIWConstants.SYNC_LOCK.lock();
        //logger.info("Constructing MessagingMgr");
        messagingMgr = new MessagingManager();
        //This cannot be done until the messagingMgr is instantiated
        fileChooserDirector = new GeoIODirector(this);
        //logger.info("Getting thread name...");
        myCID = Thread.currentThread().getName();
        logger.fine("Thread name: " + myCID);
        // register self with the Message Dispatcher
        logger.fine("Registering Component");
        messagingMgr.registerComponent(QIWConstants.VIEWER_AGENT_COMP, QIWConstants.QI_VIEWER_NAME, myCID);
        // notify the startup method initialization finished
        logger.fine("Clearing SYNC_LOCK");
        QIWConstants.SYNC_LOCK.unlock();
        logger.fine("Exiting ViewerAgent.init.");

        messageProcessor = new MessageProcessor(messagingMgr,
                fileChooserDirector,
                this);

        super.setInitSuccessful(true);
        super.setInitFinished(true);
    }

    /**
     * Open a new instance of a viewerDesktop and make it this agent's viewerDesktop.
     * If GUI is already open for this agent, an UnsupportedOperationException is thrown.
     *
     */
    public ViewerDesktop invokeNewDesktop() {
        if (viewerDesktop != null) {
            throw new UnsupportedOperationException("Unable to invokeNewDesktop() because this agent already has a non-null viewerDesktop.");
        }
        viewerDesktop = new ViewerDesktop(this);
        return viewerDesktop;
    }

    /** Launch the qiViewer:
     * <ul>
     *   <li>Start up the viewer's thread which will initialize the viewer.</li>
     * </ul>
     * <p>
     *
     * NOTE: Each thread's init() must finish before another thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        //ViewerAgent viewer2Dinstance = new ViewerAgent(false);
        ViewerAgent viewer2Dinstance = new ViewerAgent(true);

        //CID has to be passed in since no way to get it back out
        String cid = args[0];

        // use the CID as the name of the thread
        Thread viewerThread = new Thread(viewer2Dinstance, cid);
        viewerThread.start();

        // When the viewer's init() is finished, it will release the lock
        // wait until the viewers's init() has finished
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
    }

    public synchronized void showUnimplementedCodeWarning() {
        if (viewerDesktop == null) {
            throw new UnsupportedOperationException("setGUI() must be called before showUnimplementedCodeWarningGUI()");
        } else {
            java.awt.EventQueue.invokeLater(new Runnable() {

                public void run() {
                    JOptionPane.showMessageDialog(viewerDesktop, "The selected functionality is not yet available.", "Warning", JOptionPane.OK_OPTION);
                }
            });
        }
    }

    public void showAboutDialog() {
        if (viewerDesktop == null) {
            throw new UnsupportedOperationException("setGUI() must be called before showAboutDialog()");
        } else {
            String compName = messagingMgr.getRegisteredComponentDisplayNameByDescriptor(messagingMgr.getMyComponentDesc());
            Properties props = ComponentUtils.getComponentVersionProperies(compName);
            viewerDesktop.showAboutDialog(props);
        }
    }

    public void setGUI(ViewerDesktop desktopComponent) {
        this.viewerDesktop = desktopComponent;
        messageProcessor.setGUI(desktopComponent);
    }

    public void stopThread(Thread t) {
        stopThread = true;
        t.interrupt();
    }

    public GeoIODirector getFileChooserDirector() {
        return fileChooserDirector;
    }

    /**
     * @return true if data is currently being loaded for any layer associated
     * with this ViewerAgent
     */
    public boolean isLoadingData() {
        for (Entry<String, ViewerWindow> entry : windowIDmap.entrySet()) {
            if (entry.getValue().isLoadingData()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return true if rendering is currently being performed for any layer associated
     * with this ViewerAgent
     */
    public boolean isRendering() {
        for (Entry<String, ViewerWindow> entry : windowIDmap.entrySet()) {
            if (entry.getValue().isRendering()) {
                return true;
            }
        }
        return false;
    }

    public void setStop(boolean value) {
        stopThread = value;
    }

    public ViewerWindow openNewViewerWindow(DatasetProperties dsProperties, DataObject[] traces,
            LayerDisplayProperties layerProps) {
        return viewerDesktop.openNewViewerWindow(dsProperties,
                traces, layerProps);
    }

    public ViewerWindow openNewViewerWindow(DatasetProperties dsProperties, BhpSuHorizonDataObject[] horizons,
            LayerDisplayProperties layerProps) {
        return viewerDesktop.openNewViewerWindow(dsProperties,
                horizons, layerProps);
    }

    public void processMsg(IQiWorkbenchMsg message) {
        messageProcessor.processMsg(message);
    }

    /**
     *  Initializes this <code>ViewerAgent</code> and begins to poll the {@link MessagingManager}
     *  for incoming {@link IQiWorkbenchMsg}s.
     */
    public void run() {
        init();

        String myPid = QiProjectDescUtils.getPid(qiProjDesc);
        if (projMgrDesc == null && !"".equals(myPid)) {
            logger.info("ProjMgrDesc is null, requesting associated PROJMGR for this component with Pid: " + myPid);
            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
        }

        do {
            if (Thread.interrupted() == true) {
                stopThread = true;
            } else {
                IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
                //if a data message with skip flag set to true, it will not be processed here
                //instead it will be claimed by calling getMatchingResponseWait
                if (msg != null && !(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip())) {
                    msg = messagingMgr.getNextMsgWait();
                    messageProcessor.processMsg(msg);
                } else {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException ie) {
                        logger.warning("Interruptepd while sleeping 250ms after null/skippable msg: " + ie.getMessage());
                    }
                }
            }
        } while (stopThread == false);
    }

    public void setActiveWindow(ViewerWindow window) {
        viewerDesktop.setActiveWindow(window);
    }

    public void setLayerID(Layer layer, String id) {
        layerIDmap.put(id, layer);
    }

    public void setLayerWindopMap(Layer layer, ViewerWindow window) {
        layerWindowMap.put(layer, window);
    }

    void setProjMgrDesc(ComponentDescriptor projMgrDesc) {
        this.projMgrDesc = projMgrDesc;
    }

    /**
     * Returns the QiProjectDescriptor associated with this <code>ViewerAgent</code>.
     */
    void setQiProjectDescriptor(QiProjectDescriptor qiProjDesc) {
        this.qiProjDesc = qiProjDesc;
    }

    public void setWindowID(ViewerWindow window, String id) {
        windowIDmap.put(id, window);
    }

    public void updateWindowLocation(String windowID, final int x, final int y) {
        ViewerWindow window = getValidWindow(windowID);
        window.updateLocation(x, y);
    }

    public void updateWindowSize(String windowID, final int width, final int height) {
        ViewerWindow window = getValidWindow(windowID);
        window.updateSize(width, height);
    }

    public void updateQiViewerLocation(final int x, final int y) {
        viewerDesktop.updateLocation(x, y);
    }

    public void updateQiViewerSize(final int width, final int height) {
        viewerDesktop.updateSize(width, height);
    }

    public void setHidden(String layerID, boolean hidden) {
        Layer layer = layerIDmap.get(layerID);

        if (layer == null) {
            throw new IllegalArgumentException(layerID + " cannot be made hidden because it is not associated with a non-null Layer.");
        }

        ViewerWindow window = layerWindowMap.get(layer);
        window.setHidden(layer, hidden);
    }

    public void moveLayer(String layerID, int posChange) {
        Layer layer = getValidLayer(layerID);

        ViewerWindow window = layerWindowMap.get(layer);
        window.moveLayer(layer, posChange);
    }

    /**
     * Attemtps to reposition the specified window's vertical scroll bar to
     * placel the requested point at the uppermost visible horizontal line of the
     * GeoPlot.  If the window configuration is such that the maximum scroll
     * is not sufficient to place the requested timeOrDepth sample at the top of the
     * GeoPlot, then the resulting position of the scrollbar will be the maximum
     * scroll position.
     * 
     * @param windowID The String ID of the window to be scrolled.
     * @param timeOrDepth The time or depth (y-axis) value that is requested
     * to fall within the window's visible area.
     */
    public void scrollToVisiblePoint(String windowID, int timeOrDepth) {
        ViewerWindow window = getValidWindow(windowID);
        window.scrollToVisiblePoint(timeOrDepth);
    }

    /**
     * Writes the horizon associated with the active horizon xsec layer.
     * If the selected layer is not a cross-section horizon, or if there is no
     * active layer, then no action occurs.
     */
    public void writeActiveHorizon() {
        ViewerWindow activeWindow = getActiveWindow();
        if (activeWindow == null) {
            return;
        }

        Layer activeLayer = activeWindow.getActiveLayer();
        if (activeLayer == null) {
            return;
        }

        if (activeLayer.isHorizonLayer() && activeLayer.getDatasetProperties().getMetadata().getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER) {
            fileChooserDirector.writeHorizon((HorizonLayer) activeLayer);
        } else {
            logger.warning("Unable to invoke writeHorizon - active layer is non-horizon or non-xsec.  WriteHorizon() should not have been called.");
        }
    }

    public void incrementFrame() {
        ViewerWindow activeWindow = getActiveWindow();
        if (activeWindow == null) {
            return;
        }
        activeWindow.incrementFrame();
    }

    public void decrementFrame() {
        ViewerWindow activeWindow = getActiveWindow();
        if (activeWindow == null) {
            return;
        }
        activeWindow.decrementFrame();
    }

    public void delete(ViewerWindow viewerWindow) {
        ViewerWindow activeWindow = getActiveWindow();
        if (activeWindow == viewerWindow) {
            activeWindow = null;
        }
        viewerDesktop.delete(viewerWindow);

        for (Entry<Layer, ViewerWindow> entry : layerWindowMap.entrySet()) {
            if (entry.getValue() == viewerWindow) {
                layerWindowMap.remove(entry.getKey());
            }
        }

        for (String layerID : viewerWindow.getLayerIDs()) {
            layerIDmap.remove(layerID);
        }

        for (Entry<String, ViewerWindow> entry : windowIDmap.entrySet()) {
            if (entry.getValue() == viewerWindow) {
                layerWindowMap.remove(entry.getKey());
            }
        }

        defaultWindowSyncGroup.removeListener(viewerWindow);
    }
}
