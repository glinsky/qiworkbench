/**
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.qiviewer.controller.util;

import com.bhpb.qiviewer.ViewerAgent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <code>UnimplementedCodeActionListener</code> receives ActionEvents from the UI and invokes the QiWorkbenchComponent's
 * showUnimplementedCodeWarning() method, which displays a modal MessageBox for the bhp2Dviewer.
 *
 * It is recommended to use this class instead of creating anonymous inner classes for each unimplemented
 * component which has an ActionListener without implemented functionality.
 *
 * @author Woody Folsom
 * @version bhp2Dviewer release 2
 *
 */
public class UnimplementedCodeActionListener implements ActionListener {
    private ViewerAgent agent;
    
    public UnimplementedCodeActionListener(ViewerAgent agent) {
        this.agent = agent;
    }
    
    /**
     * Invokes the showUnimplementedCodeWarning method of this <code>UnimplementedCodeActionListener</code>'s
     * {@link ViewerAgent} when an {@link ActionEvent} is received.
     *
     * @param ae ActionEvent received by this <code>UnimplementedCodeActionListener</code>
     */
    public void actionPerformed(ActionEvent ae) {
        agent.showUnimplementedCodeWarning();
    }
}