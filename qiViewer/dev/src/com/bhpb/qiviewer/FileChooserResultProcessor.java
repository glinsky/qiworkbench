/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer;

import com.bhpb.qiviewer.adapter.geoio.GeoIODirector;
import com.bhpb.qiviewer.adapter.geoio.GeoIOTransactionResult;
import com.bhpb.qiviewer.ui.desktop.ViewerDesktop;
import com.bhpb.qiviewer.ui.layer.CreateHorizonsDialog;
import com.bhpb.qiworkbench.FileChooserDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.FileChooserDescUtils;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class FileChooserResultProcessor {

    private static final Logger logger =
            Logger.getLogger(FileChooserResultProcessor.class.toString());
    private GeoIODirector fileChooserDirector;
    private ViewerAgent viewerAgent;
    private ViewerDesktop viewerDesktop;

    public FileChooserResultProcessor(GeoIODirector fileChooserDirector,
            ViewerDesktop viewerDesktop,
            ViewerAgent viewerAgent) {
        this.fileChooserDirector = fileChooserDirector;
        this.viewerAgent = viewerAgent;
        this.viewerDesktop = viewerDesktop;
    }

    public void processMsg(IQiWorkbenchMsg response) {
        ArrayList list = (ArrayList) MsgUtils.getMsgContent(response);
        final String selectionPath = (String) list.get(1);

        if (((Integer) list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
            String filePath = (String) list.get(1);
            String action = (String) list.get(3);

            if (action.equals(QIWConstants.OPEN_SESSION_ACTION_BY_2D_VIEWER)) {
                //Import the state of a bhpViewer, i.e., state that was exported
                //in the bhpViewer.
                if (ComponentStateUtils.stateFileExists(selectionPath, viewerAgent.getMessagingManager())) {
                    try {
                        String pmState = ComponentStateUtils.readState(selectionPath, viewerAgent.getMessagingManager());
                        Node compNode = ComponentStateUtils.getComponentNode(pmState);
                        if (compNode == null) {
                            JOptionPane.showMessageDialog(viewerDesktop, "Can't convert XML state to XML object", "Internal Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            //check for matching component type
                            String compType = ((Element) compNode).getAttribute("componentType");
                            if (!compType.equals("bhpViewer")) {
                                JOptionPane.showMessageDialog(viewerDesktop, "State file not for a bhpViewer. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                            } else {
                                viewerDesktop.importBhpViewerState(compNode);
                            }
                        }
                    } catch (QiwIOException qioe) {
                        JOptionPane.showMessageDialog(viewerDesktop, "Can't read bhpViewer state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                        logger.finest(qioe.getMessage());
                    }
                } else {
                    JOptionPane.showMessageDialog(viewerDesktop, "bhpViewer state file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                }
            } else if (action.equals(QIWConstants.SAVE_MOVIE_VIDEO_ACTION_BY_2D_VIEWER)) {
                logger.warning("Cannot save QuickTime file: " + filePath + ". This functionality is not implemented.");
            } else if (action.equals(QIWConstants.SAVE_MOVIE_IMAGE_ACTION_BY_2D_VIEWER)) {
                try {
                    viewerAgent.getMovieController().saveImageFiles(filePath);
                    final String msgStr = "Save multiple JPEG files " + filePath;
                    Runnable updateAComponent = new Runnable(){
                        public void run(){
                            viewerDesktop.setStatus(msgStr);
                        }
                    };
                    SwingUtilities.invokeLater(updateAComponent);
                } catch (Exception ex) {
                    final String msgStr = "Failed to save multiple JPEG files " + filePath;
                    Runnable updateAComponent = new Runnable(){
                        public void run(){
                            viewerDesktop.setStatus(msgStr);
                        }
                    };
                    SwingUtilities.invokeLater(updateAComponent);
                    logger.warning("Failed to save JPEG file " + filePath);
                    logger.warning("    " + ex.toString());
                }
            } else if (action.equals(QIWConstants.OPEN_WINDOW)) {
                logger.info("Invoking action: " + action + " on file: " + filePath);
                GeoIOTransactionResult result = fileChooserDirector.issueGeoIOread(response, filePath, action, true, true);
                if (result.isAccepted()) {
                    logger.info("issueGeoIOread() successful and the user clicked 'Ok'");
                } else if (result.isCancelled()) {
                    logger.warning("issueGeoIOread() successful but the user clicked 'Cancel'");
                } else if (result.isErrorCondition()) {
                    logger.warning("An error occurred: " + result.getErrorMsg());
                    if (result.isInteractive()) {
                        JOptionPane.showMessageDialog(viewerDesktop, result.getErrorMsg());
                    }
                } else {
                    logger.warning("An unknown error occurred while issuing a GeoIOread() or editing the dataset properties.");
                }
            } else if (action.equals(QIWConstants.ADD_LAYER)) {
                logger.info("Invoking action: " + action + " on file: " + filePath);
                GeoIOTransactionResult result = fileChooserDirector.issueGeoIOread(response, filePath, action, true, true);
                if (result.isAccepted()) {
                    logger.info("issueGeoIOread() successful and the user clicked 'Ok'");
                } else if (result.isCancelled()) {
                    logger.warning("issueGeoIOread() successful but the user clicked 'Cancel'");
                } else if (result.isErrorCondition()) {
                    logger.warning("An error occurred: " + result.getErrorMsg());
                    if (result.isInteractive()) {
                        JOptionPane.showMessageDialog(viewerDesktop, result.getErrorMsg());
                    }
                } else {
                    logger.warning("An unknown error occurred while issuing a GeoIOread() or editing the dataset properties.");
                }
            } else if (action.equals(QIWConstants.EXPORT_COMP_STATE)) {
                //Write XML state to the specified file and location
                try {
                String pmState = viewerAgent.genState();
                String xmlHeader = "<?xml version=\"1.0\" ?>\n";
                try {
                    ComponentStateUtils.writeState(xmlHeader + pmState, selectionPath);
                } catch (QiwIOException qioe) {
                    logger.finest(qioe.getMessage());
                }
                } catch (Exception ex) {
                    logger.warning("Unable to save ViewerAgent state due to: " + ex.getMessage());
                }
            } else if (action.equals(QIWConstants.IMPORT_COMP_STATE)) {
                if (ComponentStateUtils.stateFileExists(selectionPath)) {
                    try {
                        String pmState = ComponentStateUtils.readState(selectionPath);
                        Node compNode = ComponentStateUtils.getComponentNode(pmState);
                        if (compNode == null) {
                            JOptionPane.showMessageDialog(viewerDesktop, "Can't convert XML state to XML object", "Internal Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            //check for matching component type
                            String compType = ((Element) compNode).getAttribute("componentType");
                            String expectedCompType = viewerAgent.getMessagingManager().getRegisteredComponentDisplayNameByDescriptor(
                                    viewerAgent.getMessagingManager().getMyComponentDesc());
                            if (!compType.equals(expectedCompType)) {
                                JOptionPane.showMessageDialog(viewerDesktop, "State file not for this qiComponent type. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                            } else {
                                //Note: don't ask if want to save state before importing because
                                //      for future saves, the imported state will be saved.
                                viewerAgent.importState(compNode);
                            }
                        }
                    } catch (QiwIOException qioe) {
                        JOptionPane.showMessageDialog(viewerDesktop, "Can't read state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                        logger.finest(qioe.getMessage());
                    }
                } else {
                    JOptionPane.showMessageDialog(viewerDesktop, "State file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                }
            } else if (action.equals(CreateHorizonsDialog.SELECT_SEISMIC_CUBE)) {
                //Note: message ID of response same as request's
                String fileName = (String) list.get(1);

                String msgID = MsgUtils.getMsgID(response);
                //make sure file chooser request registered
                if (fileChooserDirector.isFileChooserRequestRegistered(msgID, 3)) {
                    FileChooserDescriptor desc = fileChooserDirector.getFileChooserDesc(msgID);
                    CreateHorizonsDialog chooser = (CreateHorizonsDialog) FileChooserDescUtils.getRequesterInstance(desc);
                    //unregister file chooser request
                    fileChooserDirector.unregisterFileChooserRequest(desc);
                    //process the selected Segy file
                    chooser.setSelectedDataset(fileName);
                } else {
                    logger.severe("INTERNAL ERROR: cannot find registered file chooser request. msgID=" + msgID);
                }
            } else { // added explicit warning message for fallthrough case

                logger.warning("Unknown file-related action not processed by the ViewerAgent: " + action);
            }
        } else {
            logger.info("File Chooser canceled by the user");
        }
    }
}