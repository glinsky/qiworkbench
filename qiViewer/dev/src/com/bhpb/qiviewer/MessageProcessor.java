/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer;

import com.bhpb.qiviewer.adapter.geoio.GeoIODirector;
import com.bhpb.qiviewer.ui.desktop.ViewerDesktop;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;

import com.bhpb.qiviewer.controlManager.ControlSessionManager;

import java.awt.Component;
import java.awt.Point;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Delegate for processing of QiWorkbenchMsgs sent to the ViewerAgent.
 * 
 */
public class MessageProcessor {

    public enum EXECUTION_MODE {

        INTERACTIVE, CONTROL
    }
    private static final Logger logger =
            Logger.getLogger(MessageProcessor.class.toString());
    private final GeoIODirector geoIODirector;
    private EXECUTION_MODE executionMode = EXECUTION_MODE.INTERACTIVE;
    private ControlSessionManager controlSessionMgr = null;
    private FileChooserResultProcessor fileChooserResultProcessor;
    private IMessagingManager messagingMgr;
    private String controlSessionID = "";
    private String myCID = "";
    private ViewerAgent viewerAgent;
    private ViewerDesktop viewerDesktop;

    /**
     * Constructs a MessageProcessor which uses the given IMessagingManager,
     * FileChooserDirector, ViewerAgent and CID.
     * 
     * @param messagingMgr IMessagingManager used to relay requests and send responses.
     * @param geoIODirector GeoIODirector through which GeoIO functionality is available.
     * @param viewerAgent ViewerAgent associated with this MessagingManager, used invoke GUI methods.
     * 
     */
    public MessageProcessor(IMessagingManager messagingMgr,
            GeoIODirector geoIODirector,
            ViewerAgent viewerAgent) {
        this.messagingMgr = messagingMgr;
        this.geoIODirector = geoIODirector;
        this.viewerAgent = viewerAgent;
        this.myCID = viewerAgent.getCID();
    }

    /**
     * This method is invoked to instantiate the ViewerAgent's GUI.  Alternatively,
     * the viewerAgent component may be send an INVOKE_SELF_CMD as documented
     * in QIWConstants.  Note that this method does NOT add the created ViewerDesktop component
     * to any Swing hierarchy - this must be don by the WorkbenchManager or JUnit test
     * using agent.getViewerDesktop().
     */
    void invokeNewComponent() {
        viewerDesktop = viewerAgent.invokeNewDesktop();
        fileChooserResultProcessor = new FileChooserResultProcessor(geoIODirector,
                viewerDesktop, viewerAgent);
    }

    /**
     * Checks whether the MessageProcessor is configured to allow interactive
     * viewing of GUI elements associated with commands from the ControlSessionManager.
     * 
     * @return true if and only if the execution mode is CONTROL.
     */
    public boolean isControlMode() {
        return executionMode == EXECUTION_MODE.CONTROL;
    }

    /**
     * Checks whether the MessageProcessor is configured to allow interactive
     * viewing of GUI elements associated with commands from the ControlSessionManager.
     * 
     * @return true if and only if the execution mode is INTERACTIVE.
     */
    public boolean isInteractiveMode() {
        return executionMode == EXECUTION_MODE.INTERACTIVE;
    }

    /**
     * Process the given QiWorkbenchMsg, which may be either a request or a response.
     * 
     * @param msg The QiWorkbenchMsg to be processed.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        logger.finest("ViewerAgent::processMsg: msg=" + msg.toString());
        try {
            // Check if a response. If so, process and consume response
            if (messagingMgr.isResponseMsg(msg)) {
                processResponse(msg);
            } else if (messagingMgr.isRequestMsg(msg)) { //Check if a request. If so, process and send back a response
                processRequest(msg);
            }
        //This catch block explicitly handles RuntimeExceptions in order to return
        //an abnormal response in the event of catastrophic component failure to handle message.
        } catch (RuntimeException ex) {
            String errorMessage = "Caught exception in ViewerAgent.ProcessMsg() : " + ex.getMessage();
            logger.warning(errorMessage);

            //return an abnormal response indicating that an exception has occurred
            IQiWorkbenchMsg abnormalResponse = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, errorMessage);
            IComponentDescriptor targetDesc = messagingMgr.getComponentDesc(msg.getProducerCID());
            messagingMgr.addMessageTo(targetDesc, abnormalResponse);

            //Explicitly invoke UncaughtExceptionHandler
            //Otherwise, rethrowing it would allow the UncaughtExceptionBrowser to display
            //the exception via the browser, but also terminates the ViewerAgent's run()
            //method, effectively disabling the ViewerAgent.
            viewerAgent.getUncaughtExceptionHandler().handleException(ex, true);
        }
    }

    private void processRequest(IQiWorkbenchMsg msg) {
        String cmd = MsgUtils.getMsgCommand(msg);
        logger.fine("ViewerAgent received " + cmd + " command");
        if (cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
            if (viewerDesktop != null) {
                viewerDesktop.setVisible(true);
            }
        } else if (cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
            if (viewerDesktop != null && viewerDesktop.isVisible()) {
                messagingMgr.sendResponse(msg, Component.class.getName(), (Component) viewerDesktop);
                logger.fine("Closing qiViewer GUI...");
                viewerAgent.closeGUI();
                logger.fine("qiViewer GUI closed.");
            } else {
                logger.warning("Could not close qiViewer GUI - the desktop is null or not visible");
            }
        } else if (cmd.equals(QIWConstants.DEACTIVATE_VIEWER_AGENT_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD)) {
            logger.fine("Unregistering qiViewer component agent...");
            messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));

            if (viewerDesktop != null && viewerDesktop.isVisible()) {
                logger.fine("Closing qiViewer GUI...");
                viewerAgent.closeGUI();
                logger.fine("qiViewer GUI closed.");
            } else {
                logger.warning("Could not close qiViewer GUI - the desktop is null or not visible.");
            }

            // tell plugin thread to quit
            viewerAgent.setStop(true);
            messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, myCID + "is successfully deactivated");
        } else if (cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
            String preferredDisplayName = (String) MsgUtils.getMsgContent(msg);
            if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                renameViewer(preferredDisplayName);
            }
            messagingMgr.sendResponse(msg, QIWConstants.COMP_DESC_TYPE, messagingMgr.getMyComponentDesc());
        } else if (cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
            // Create the viewer's GUI, but don't make it visible. That is
            // up to the Workbench Manager who requested the viewer be
            // activated. Pass GUI the CID of its parent.
            // Check to see if content incudes XML information which means invoke
            // self will do the restoration
            ArrayList msgList = (ArrayList) msg.getContent();
            String invokeType = (String) msgList.get(0);
            if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)) {
                Node node = ((Node) ((ArrayList) MsgUtils.getMsgContent(msg)).get(2));
                //Note: restoreState() will restore the project descriptor needed by buildGUI
                fileChooserResultProcessor = new FileChooserResultProcessor(geoIODirector,
                        viewerDesktop, viewerAgent);
                restoreState(node);
            } else if (invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)) {
                invokeNewComponent();
                Point compLoc = (Point) msgList.get(1);
                if (compLoc != null) {
                    viewerDesktop.setLocation(compLoc);
                }
            }
            //Send a normal response back with the plugin's JInternalFrame and
            //let the Workbench Manager add it to the desktop and make
            //it visible.
            messagingMgr.sendResponse(msg, ViewerDesktop.class.getName(), viewerDesktop);
        } else if (cmd.equals(QIWConstants.SAVE_COMP_CMD)) {
            try {
                String xmlState = viewerAgent.genState();
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, xmlState);
            } catch (Exception ex) {
                logger.warning("Unable to save ViewerAgent's state due to: " + ex.getMessage());
            }
        } else if (cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
            try {
                String xmlState = viewerAgent.genStateAsClone();
                messagingMgr.sendResponse(msg, QIWConstants.STRING_TYPE, xmlState);
            } catch (Exception ex) {
                logger.warning("Unable to save ViewerAgent's state as a clone due to: " + ex.getMessage());
            }
        } else if (cmd.equals(QIWConstants.RESTORE_COMP_CMD)) { // restore state
            restoreState((Node) msg.getContent());
            messagingMgr.sendResponse(msg, ViewerDesktop.class.getName(), viewerDesktop);
        } else if (cmd.equals(QIWConstants.GET_VIEWER_GUI_CMD)) {
            messagingMgr.sendResponse(msg, ViewerDesktop.class.getName(), viewerDesktop);
        } else if (cmd.equals(QIWConstants.RENAME_COMPONENT)) {
            ArrayList names = (ArrayList) MsgUtils.getMsgContent(msg);
            logger.fine("Rename myself from " + names.get(0) + " to " + names.get(1));
            renameViewer((String) names.get(1));
        } else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
            //Just a notification. No response required or expected.
            ComponentDescriptor projMgrDesc = (ComponentDescriptor) msg.getContent();
            viewerAgent.setProjMgrDesc(projMgrDesc);
            //Note: Cannot get info about PM's project because GUI may not be up yet.
            if (projMgrDesc != null) {
                messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
            }
        } else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
            ArrayList info = (ArrayList) msg.getContent();
            ComponentDescriptor pmDesc = (ComponentDescriptor) info.get(0);
            QiProjectDescriptor projDesc = (QiProjectDescriptor) info.get(1);

            //ignore message if not from associated PM
            if (viewerAgent.getProjMgrDesc() != null && pmDesc.getCID().equals(viewerAgent.getProjMgrDesc().getCID())) {
                QiProjectDescriptor qiProjDesc = projDesc;
                viewerAgent.setQiProjectDescriptor(qiProjDesc);
                String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
                viewerDesktop.resetTitle(projName);
            }
        } else if (cmd.equals(QIWConstants.CONTROL_QIVIEWER_CMD)) {
            ArrayList actions = (ArrayList) msg.getContent();
            String action = (String) actions.get(0);
            ArrayList actionParams = (ArrayList) actions.get(1);

            if (action.indexOf(ControlConstants.START_CONTROL_SESSION_ACTION) != -1) {
                executionMode = EXECUTION_MODE.CONTROL;
                //Note: There is a CSM if control session started once,
                //so reuse it.
                if (controlSessionMgr == null) {
                    controlSessionMgr = new ControlSessionManager(viewerAgent);
                //return the session ID and the qiViewer component descriptor
                //NOTE: The qiViewer was launched by a qiComponent and the request contains
                //      its component descriptor in the action parameters.
                }
                this.controlSessionID = ControlSessionManager.genControlSessionID();
                actionParams.add(0, this.controlSessionID);
                messagingMgr.sendResponse(msg, QIWConstants.ARRAYLIST_TYPE, actionParams);
            } else if (action.indexOf(ControlConstants.END_CONTROL_SESSION_ACTION) != -1) {
                String sessionID = (String) actionParams.get(0);
                if (sessionID.equals(this.controlSessionID)) {
                    executionMode = EXECUTION_MODE.INTERACTIVE;
                }
                //Let the CSM cleanup its state for another invocation
                //and the response sent to the FSM so it can cleanup
                //its state
                controlSessionMgr.processReq(msg);
            } else {
                controlSessionMgr.processReq(msg);
            }
        } else {
            logger.warning("ViewerAgent: Request message not processed: " + msg.toString());
        }
    }

    private void processResponse(IQiWorkbenchMsg msg) {
        messagingMgr.checkForMatchingRequest(msg);
        // check if from the message dispatcher
        if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
            logger.warning("An unexpected response was received from the MessageDispatcher: " + msg.toString());
        } else if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
            fileChooserResultProcessor.processMsg(msg);
        } else if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_PROJ_INFO_CMD)) {
            ArrayList projInfo = (ArrayList) msg.getContent();
            QiProjectDescriptor qiProjDesc = (QiProjectDescriptor) projInfo.get(1);
            viewerAgent.setQiProjectDescriptor(qiProjDesc);

            //reset window title (if GUI up)
            if (viewerDesktop != null) {
                String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
                viewerDesktop.resetTitle(projName);
            }
        } else if (MsgUtils.getMsgCommand(msg).equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
            ArrayList projInfo = (ArrayList) msg.getContent();
            ComponentDescriptor projMgrDesc = (ComponentDescriptor) projInfo.get(0);
            viewerAgent.setProjMgrDesc(projMgrDesc);
            QiProjectDescriptor qiProjDesc = (QiProjectDescriptor) projInfo.get(1);
            viewerAgent.setQiProjectDescriptor(qiProjDesc);
            String projName = QiProjectDescUtils.getQiProjectName(qiProjDesc);
            viewerDesktop.resetTitle(projName);
        } else {
            logger.warning("ViewerAgent: Response message not processed: " + msg.toString());
        }
    }

    private void renameViewer(String preferredDisplayName) {
        CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
        viewerDesktop.resetTitle(viewerAgent.getQiProjectDescriptor().getQiProjectName());
    }

    /** 
     * Restores plugin and GUI to previous condition.
     *
     * @param node xml containing saved state variables
     */
    private void restoreState(Node node) {
        String preferredDisplayName = ((Element) node).getAttribute("preferredDisplayName");
        NodeList children = node.getChildNodes();

        //QiProjectDescriptor element was moved from within ViewerDesktop (GUI) element
        //into the component element
        viewerAgent.loadQiProjectDescriptor(node);

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if ((child.getNodeType() == Node.ELEMENT_NODE) && child.getNodeName().equals(ViewerDesktop.getXmlAlias())) {
                if (viewerDesktop == null) {
                    viewerDesktop = new ViewerDesktop(viewerAgent);
                    viewerAgent.setGUI(viewerDesktop);
                }
                viewerDesktop.importQiViewerState(child);
                if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                    renameViewer(preferredDisplayName);
                }
                break;
            }
        }
    }

    /**
     * This package-protected method is used to set the GUI field, which is used
     * by the genState and restoreState methods, after the ViewerAgent and this
     * MessageProcesor are created.  
     * <p>
     * Initially, the viewerDesktop was a parameter
     * of the MessageProcessor constructor which was a bug. 
     * This parameter could never have been non-null, since the MessageProcessor itself
     * is responsible for initiating the call to ViewerAgent.invokeNewDesktop, which
     * creates the ViewerDesktop.
     */
    void setGUI(ViewerDesktop viewerDesktop) {
        this.viewerDesktop = viewerDesktop;
    }
}