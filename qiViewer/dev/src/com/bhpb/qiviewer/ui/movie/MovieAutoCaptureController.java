/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.movie;


import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.qiworkbench.util.StringValidator;
import com.bhpb.qiworkbench.util.StringValidator.TYPE;
import com.bhpb.qiworkbench.util.ValidatorResult;
import javax.swing.JDialog;


public class MovieAutoCaptureController extends AbstractEditorPanelController<MovieAutoCaptureSettings, AutoCapturePanel> {

    private MovieController movieController;
    private JDialog parentDialog; //the parent dialog that holds the view panel

    public MovieAutoCaptureController(MovieAutoCaptureSettings model, AutoCapturePanel view, MovieController movieController) {
        super(model, view);
        view.setController(this);
        this.movieController = movieController;
    }

    public void run() {
        view.setMaxStepNumber(model.getMaxStepNumber());
    }
    
    
    public void captureImage(){
        if(movieController == null)
            return;
        movieController.captureImage();
        
    }
    
    public void autoCaptureImage(){
        if(movieController == null)
            return;
        movieController.startAutoCapture();
    }
       
    public void shutdownMovieDialog(){
        parentDialog.setVisible(false);
        movieController.shutdownMovieDialog();
    }
    
    public void setParentDialog(JDialog dialog){
        parentDialog = dialog;
    }
    
    public void setMaxStepNumber(String num) {
        ValidatorResult validatorResult = StringValidator.validate(num, TYPE.INTEGER);
        if (validatorResult.isValid()) {
            model.setMaxStepNumber(Integer.valueOf(num));
        } else {
            view.showValidationFailure(validatorResult.getReason());
        }
    }
    
     public int getMaxStepNumber() {
        return model.getMaxStepNumber();
    }

}