/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

/*
 * MovieDialog.java
 *
 * Created on October 29, 2008, 11:10 AM
 */
package com.bhpb.qiviewer.ui.movie;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.controller.MoviePreferenceSettingsController;
import com.bhpb.geographics.model.MoviePreferenceSettings;
import com.bhpb.geographics.ui.MoviePreferencePanel;
import com.bhpb.geographics.ui.layer.EditorDialog;
import com.bhpb.geographics.util.OkCancelDialog;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JPanel;

import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class MovieDialog extends javax.swing.JFrame {

    private static final transient Logger logger =
            Logger.getLogger(MovieDialog.class.getName());
    private transient MovieController movieController;
    private PLAY_DIRECTION playDirection = PLAY_DIRECTION.FORWARD;
    private Timer moviePlayTimer = null;
    //private ViewerDesktop parentGUI;
    private int currentFrameIndex = 0;

    /** Creates new form MovieDialog */
    public enum PLAY_DIRECTION {

        FORWARD, BACKWARD
    };

    public MovieDialog(MovieController movieController) {
        super("Movie");

        this.movieController = movieController;
        this.movieController.setMovieDialog(this);

        initComponents();

        setupIconButtons();
        loadImages();
        setSize(800, 600);
        setupTimer();
    }

    public synchronized void setSelectedImageIndex(int index) {
        if (moviePlayTimer.isRunning()) {
            stopMovie();
        }
        if (index == currentFrameIndex) {
            return;
        }

        currentFrameIndex = index;
        this.repaint();
    }

    private synchronized void setupTimer() {
        currentFrameIndex = 0;
        playDirection = PLAY_DIRECTION.FORWARD;
        ActionListener actionPerformer = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                updateImageFrame();
            }
        };
        if (moviePlayTimer == null) {
            moviePlayTimer = new Timer(movieController.getMoviePreferenceSettings().getFrameInterval(), actionPerformer);
        } else {
            moviePlayTimer.setDelay(movieController.getMoviePreferenceSettings().getFrameInterval());
            if (moviePlayTimer.isRunning()) {
                moviePlayTimer.restart();
            }
        }
    }

    private void loadImages() {
        JPanel panel = new MovieDisplayPanel();
        displayPanel.setLayout(new BorderLayout());
        displayPanel.add(panel, BorderLayout.CENTER);
    }

    private synchronized void playMovie(PLAY_DIRECTION direction) {
        playDirection = direction;
        logger.info("PlayDirection set to: " + playDirection);
        if (moviePlayTimer == null) {
            logger.warning("Unable to play movie: moviePlayTimer is null");
        } else {
            moviePlayTimer.setDelay(movieController.getMoviePreferenceSettings().getFrameInterval());
            if (moviePlayTimer.isRunning()) {
                moviePlayTimer.restart();
            } else {
                moviePlayTimer.start();
            }
        }
    }

    private synchronized void stopMovie() {
        if (moviePlayTimer == null) {
            logger.warning("Unable to stop move: moviePlayTimer is null");
        } else {
            if (moviePlayTimer.isRunning()) {
                moviePlayTimer.stop();
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        toolbarPanel = new javax.swing.JPanel();
        movieToolBar = new javax.swing.JToolBar();
        rewindFrameButton = new javax.swing.JButton();
        rewindButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        forwardButton = new javax.swing.JButton();
        forwardFrameButton = new javax.swing.JButton();
        separator = new javax.swing.JToolBar.Separator();
        helpButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        contentScrollPane = new javax.swing.JScrollPane();
        displayPanel = new javax.swing.JPanel();
        movieMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exportJPEGMenuItem = new javax.swing.JMenuItem();
        exportAVIMenuItem = new javax.swing.JMenuItem();
        closeMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        framesMenuItem = new javax.swing.JMenuItem();
        preferencesMenuItem = new javax.swing.JMenuItem();
        autoCaptureMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Movie");

        toolbarPanel.setLayout(new java.awt.BorderLayout());

        movieToolBar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        movieToolBar.setRollover(true);

        rewindFrameButton.setToolTipText("Go to the previous frame");
        rewindFrameButton.setFocusable(false);
        rewindFrameButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rewindFrameButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        rewindFrameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rewindFrameButtonActionPerformed(evt);
            }
        });
        movieToolBar.add(rewindFrameButton);

        rewindButton.setToolTipText("Play movie backward");
        rewindButton.setFocusable(false);
        rewindButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rewindButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        rewindButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rewindButtonActionPerformed(evt);
            }
        });
        movieToolBar.add(rewindButton);

        stopButton.setToolTipText("Stop auto play");
        stopButton.setFocusable(false);
        stopButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stopButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });
        movieToolBar.add(stopButton);

        forwardButton.setToolTipText("Play movie forward");
        forwardButton.setFocusable(false);
        forwardButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        forwardButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        forwardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forwardButtonActionPerformed(evt);
            }
        });
        movieToolBar.add(forwardButton);

        forwardFrameButton.setToolTipText("Go to the next frame");
        forwardFrameButton.setFocusable(false);
        forwardFrameButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        forwardFrameButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        forwardFrameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forwardFrameButtonActionPerformed(evt);
            }
        });
        movieToolBar.add(forwardFrameButton);
        movieToolBar.add(separator);

        helpButton.setToolTipText("Help information");
        helpButton.setFocusable(false);
        helpButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        helpButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        helpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpButtonActionPerformed(evt);
            }
        });
        movieToolBar.add(helpButton);

        closeButton.setToolTipText("Close the movie window");
        closeButton.setFocusable(false);
        closeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        closeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        movieToolBar.add(closeButton);

        toolbarPanel.add(movieToolBar, java.awt.BorderLayout.NORTH);
        movieToolBar.getAccessibleContext().setAccessibleParent(toolbarPanel);

        getContentPane().add(toolbarPanel, java.awt.BorderLayout.NORTH);
        toolbarPanel.getAccessibleContext().setAccessibleParent(this);

        displayPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        contentScrollPane.setViewportView(displayPanel);

        getContentPane().add(contentScrollPane, java.awt.BorderLayout.CENTER);

        fileMenu.setText("File");

        exportJPEGMenuItem.setText("Save As Multiple JPEG Files ...");
        exportJPEGMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportJPEGMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exportJPEGMenuItem);

        exportAVIMenuItem.setText("Save As AVI File ...");
        exportAVIMenuItem.setEnabled(false);
        fileMenu.add(exportAVIMenuItem);

        closeMenuItem.setText("Close");
        closeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(closeMenuItem);

        movieMenuBar.add(fileMenu);

        editMenu.setText("Edit");

        framesMenuItem.setText("Frames ...");
        framesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                framesMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(framesMenuItem);

        preferencesMenuItem.setText("Preferences ...");
        preferencesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preferencesMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(preferencesMenuItem);

        autoCaptureMenuItem.setText("Automatic Capture ...");
        autoCaptureMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoCaptureMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(autoCaptureMenuItem);

        movieMenuBar.add(editMenu);

        setJMenuBar(movieMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void setupIconButtons() {
        java.net.URL imgURL = MovieDialog.class.getResource("/icons/Rewind2.gif");
        if (imgURL != null) {
            rewindFrameButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }

        imgURL = MovieDialog.class.getResource("/icons/Rewind.gif");
        if (imgURL != null) {
            rewindButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }
        imgURL = MovieDialog.class.getResource("/icons/Stop.gif");
        if (imgURL != null) {
            stopButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }

        imgURL = MovieDialog.class.getResource("/icons/Forward.gif");
        if (imgURL != null) {
            forwardButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }

        imgURL = MovieDialog.class.getResource("/icons/Forward2.gif");
        if (imgURL != null) {
            forwardFrameButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }

        imgURL = MovieDialog.class.getResource("/icons/HelpSM.gif");
        if (imgURL != null) {
            helpButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }

        imgURL = MovieDialog.class.getResource("/icons/Close.gif");
        if (imgURL != null) {
            closeButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }
        //movieToolBar.removeAll();
        movieToolBar.add(rewindFrameButton);



        movieToolBar.add(rewindButton);

        movieToolBar.add(stopButton);

        movieToolBar.add(forwardButton);

        movieToolBar.add(forwardFrameButton);
        movieToolBar.add(separator);

        movieToolBar.add(helpButton);
        movieToolBar.add(Box.createHorizontalGlue());
        movieToolBar.add(closeButton);

    }

	private void closeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeMenuItemActionPerformed
        this.setVisible(false);
	}//GEN-LAST:event_closeMenuItemActionPerformed

	private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        closeMenuItemActionPerformed(evt);
	}//GEN-LAST:event_closeButtonActionPerformed

    private synchronized Image getFrameImage() {
        int imageNumber = movieController.getNumFrames();
        if (currentFrameIndex >= 0 && currentFrameIndex < imageNumber) {
            return (Image) (movieController.getImageByIndex(currentFrameIndex));
        }
        return null;
    }

    private synchronized void updateImageFrame() {
        int numFrames = movieController.getNumFrames();
        if (numFrames == 0) {
            return;
        }
        int index = 1;
        if (playDirection == PLAY_DIRECTION.FORWARD) {
            index = 1;
        } else if (playDirection == PLAY_DIRECTION.BACKWARD) {
            index = -1;
        }
        int newIndex = currentFrameIndex + index;
        if (index > 0) {
            newIndex = newIndex % numFrames;
        } else {
            if (newIndex < 0) {
                newIndex = numFrames - 1;
            }
        }
        currentFrameIndex = newIndex;
        this.repaint();
    }

private void framesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_framesMenuItemActionPerformed
    //MovieFramesEditPanel view = new MovieFramesEditPanel();
    //MovieFramesEditModel framesEditModel = movieController.getMovieFramesEditModel();
    //MovieFramesEditController controller = new MovieFramesEditController(framesEditModel, view);
    //movieController.setFrameEditController(MovieFramesEditController);
    JDialog dlg = new EditorDialog((JFrame) SwingUtilities.getRoot(this), movieController.getFrameEditController().getView(), movieController.getFrameEditController());
    dlg.setVisible(true);
    dlg.pack();
}//GEN-LAST:event_framesMenuItemActionPerformed

private void preferencesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preferencesMenuItemActionPerformed
    MoviePreferencePanel view = new MoviePreferencePanel();
    MoviePreferenceSettings preferenceSettings = movieController.getMoviePreferenceSettings();
    AbstractEditorPanelController controller = new MoviePreferenceSettingsController(preferenceSettings, view);
    JDialog dlg = new EditorDialog((JFrame) SwingUtilities.getRoot(this), view, controller);
    dlg.setVisible(true);
    dlg.pack();
}//GEN-LAST:event_preferencesMenuItemActionPerformed
    private MovieAutoCaptureController autoCaptureController = null;
private void autoCaptureMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoCaptureMenuItemActionPerformed
    AutoCapturePanel view = new AutoCapturePanel();
    MovieAutoCaptureSettings autoCaptureSettings = movieController.getMovieAutoCaptureSettings();
    MovieAutoCaptureController controller = new MovieAutoCaptureController(autoCaptureSettings, view, movieController);
    JDialog dlg = new OkCancelDialog((JFrame) SwingUtilities.getRoot(this), view);
    controller.setParentDialog(dlg);
    dlg.setVisible(true);
    dlg.pack();
}//GEN-LAST:event_autoCaptureMenuItemActionPerformed

private void forwardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forwardButtonActionPerformed
    playMovie(PLAY_DIRECTION.FORWARD);
}//GEN-LAST:event_forwardButtonActionPerformed

private void rewindFrameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rewindFrameButtonActionPerformed
    synchronized (this) {
        playDirection = PLAY_DIRECTION.BACKWARD;
        updateImageFrame();
    }
}//GEN-LAST:event_rewindFrameButtonActionPerformed

private void rewindButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rewindButtonActionPerformed
    playMovie(PLAY_DIRECTION.BACKWARD);
}//GEN-LAST:event_rewindButtonActionPerformed

private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopButtonActionPerformed
    stopMovie();
}//GEN-LAST:event_stopButtonActionPerformed

private void forwardFrameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forwardFrameButtonActionPerformed
    synchronized (this) {
        playDirection = PLAY_DIRECTION.FORWARD;
        updateImageFrame();
    }
}//GEN-LAST:event_forwardFrameButtonActionPerformed

private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpButtonActionPerformed
}//GEN-LAST:event_helpButtonActionPerformed

private void exportJPEGMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportJPEGMenuItemActionPerformed
    movieController.saveImagesAsJPEGs();
}//GEN-LAST:event_exportJPEGMenuItemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem autoCaptureMenuItem;
    private javax.swing.JButton closeButton;
    private javax.swing.JMenuItem closeMenuItem;
    private javax.swing.JScrollPane contentScrollPane;
    private javax.swing.JPanel displayPanel;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem exportAVIMenuItem;
    private javax.swing.JMenuItem exportJPEGMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JButton forwardButton;
    private javax.swing.JButton forwardFrameButton;
    private javax.swing.JMenuItem framesMenuItem;
    private javax.swing.JButton helpButton;
    private javax.swing.JMenuBar movieMenuBar;
    private javax.swing.JToolBar movieToolBar;
    private javax.swing.JMenuItem preferencesMenuItem;
    private javax.swing.JButton rewindButton;
    private javax.swing.JButton rewindFrameButton;
    private javax.swing.JToolBar.Separator separator;
    private javax.swing.JButton stopButton;
    private javax.swing.JPanel toolbarPanel;
    // End of variables declaration//GEN-END:variables

    private class MovieDisplayPanel extends JPanel {

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);    // paint background
            Image image = getFrameImage();
            if (image != null) {
                g.drawImage(image, 0, 0, this);
            }
        }
    }
}