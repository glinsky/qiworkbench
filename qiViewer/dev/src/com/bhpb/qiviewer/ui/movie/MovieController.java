/*
###########################################################################
# This program module Copyright (C) 2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.movie;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.MoviePreferenceSettings;
import com.bhpb.geographics.model.MoviePreferenceSettings.CAPTURE_OPTION;
import com.bhpb.geographics.model.MoviePreferenceSettings.SAVE_OPTION;
import com.bhpb.geographics.ui.event.GeoPlotEvent;
import com.bhpb.geographics.ui.event.GeoPlotEventListener;
import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.ui.desktop.ViewerDesktop;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

public class MovieController implements GeoPlotEventListener {
    // setup logging

    private static Logger logger = Logger.getLogger(MovieController.class.getName());
    private static final int MAX_ALLOWED = 50;
    public static final int MINUTE_WAIT_TIME = 300000;
    public static final int MAX_WAIT_TIME = 300000; //5 minutes
    private List<Image> images = new ArrayList<Image>();
    private ViewerDesktop viewerDesktop = null;
    private ViewerAgent viewerAgent = null;
    private MovieFramesEditModel framesEditModel = new MovieFramesEditModel();
    private MoviePreferenceSettings preferenceSettings = new MoviePreferenceSettings();
    private MovieAutoCaptureSettings autoCaptureSettings = new MovieAutoCaptureSettings();
    private NumberFormat format;
    private MovieFramesEditPanel movieFramesEditView;
    private MovieDialog movieDialog;
    int index = 0;
    private MovieFramesEditController framesEditController;

    public MovieController(ViewerDesktop viewerDesktop, ViewerAgent viewerAgent) {
        this.viewerDesktop = viewerDesktop;
        this.viewerAgent = viewerAgent;
        format = NumberFormat.getIntegerInstance();
        format.setMinimumIntegerDigits(4);
        format.setGroupingUsed(false);
        movieFramesEditView = new MovieFramesEditPanel();
        framesEditController = new MovieFramesEditController(framesEditModel, movieFramesEditView);
        framesEditController.setParentController(this);
    }

    public MovieFramesEditController getFrameEditController() {
        return framesEditController;
    }

    private void setFrameList() {
        if (framesEditController == null) {
            return;
        }
        //List<Image> images = framesEditController.getImages();
        int numberFrame = images.size();
        List<Object> frameList = new ArrayList<Object>();
        for (int i = 0; i < numberFrame; i++) {
            frameList.add("Frame " + i);
        }

        framesEditController.setFrameList(frameList, false);
    }

    public void saveImagesAsJPEGs() {
        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(movieDialog);
        //2nd element is the dialog title
        list.add("Save as multiple JPEG files");
        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(viewerAgent.getMessagingManager().getProject());
        lst.add("yes");
        list.add(lst);

        //4th element is the file filter
        String[] extensions = {".jpg", ".JPG"};
        list.add(new GenericFileFilter(extensions, "JPEG file (*.jpg)"));
        //5th element is the navigation flag
        list.add(true);
        //6th element is the producer component descriptor
        list.add(viewerAgent.getMessagingManager().getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(QIWConstants.FILE_CHOOSER_TYPE_SAVE);
        //8th element is the message command
        list.add(QIWConstants.SAVE_MOVIE_IMAGE_ACTION_BY_2D_VIEWER);
        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("");
        //11th element is the target tomcat url where the file chooser chooses
        list.add(viewerAgent.getMessagingManager().getTomcatURL());
        viewerAgent.invokeFileChooser(list, MovieController.class.getName(), this);
    }

    /**
     * Save images (in memory) to a file.
     *
     * @param fname Full path of the file to write images to
     */
    public synchronized void saveImageFiles(String fname) throws Exception {
        int idx = fname.lastIndexOf(".jpg");
        if (idx == -1) {
            idx = fname.lastIndexOf(".JPG");
        }

        if (idx > -1) {
            fname = fname.substring(0, idx);
        }
        //List<Image> images = framesEditController.getImages();
        for (int i = 0; i < images.size(); i++) {
            String pathname = fname + "_" + format.format(i) + ".jpg";

            BufferedImage bfImage = (BufferedImage) images.get(i);
            // form parameters for binary IO command
            String locPref = viewerAgent.getMessagingManager().getLocationPref();
            //TEST: force to be remote
            //locPref = QIWConstants.REMOTE_PREF;
            ArrayList params = new ArrayList();
            params.add(locPref);    // [0] IO preference
            params.add(pathname);   // [1] file pathname
            params.add(QIWConstants.JPEG_FORMAT);   // [2] file's format
            params.add(bfImage);   // [3] binary data

            String msgID = viewerAgent.getMessagingManager().sendRequest(QIWConstants.CMD_MSG,
                    QIWConstants.BINARY_FILE_WRITE_CMD,
                    QIWConstants.ARRAYLIST_TYPE, params, true);
            // wait for the response.
            IQiWorkbenchMsg response = null;
            int ii = 0;
            while (response == null) {
                response = viewerAgent.getMessagingManager().getMatchingResponseWait(msgID, MINUTE_WAIT_TIME);
                ii++;
                if (ii >= 4) {
                    break;
                }
            }

            try {
                if (MsgUtils.isResponseAbnormal(response)) {
                    logger.finest("Binary file write error:" + (String) MsgUtils.getMsgContent(response));
                    return;
                }
            } catch (NullPointerException npe) {
                logger.severe("SYSTEM ERROR: Timed out waiting for response to binary file write");
                npe.getStackTrace();
                return;
            }
        }
    }

    public MovieFramesEditModel getMovieFramesEditModel() {
        return framesEditModel;
    }

    public MovieAutoCaptureSettings getMovieAutoCaptureSettings() {
        return autoCaptureSettings;
    }

    public MoviePreferenceSettings getMoviePreferenceSettings() {
        return preferenceSettings;
    }

    public List<Image> getImages() {
        //return framesEditController.getImages();
        return images;
    }

    public void handleEvent(GeoPlotEvent event) {
        if (event == GeoPlotEvent.RENDERING_COMPLETE) {
        } else if (event == GeoPlotEvent.RENDERING_STARTED) {
        } else if (event.isError()) {
        }
    }

    public void startAutoCapture() {
        if (viewerDesktop.getActiveWindow() == null) {
            JOptionPane.showMessageDialog(viewerDesktop, "Please select a layer first.");
            return;
        }
        Runnable worker = new Runnable() {

            public void run() {
                ViewerWindow win = viewerDesktop.getActiveWindow();
                int maxStep = autoCaptureSettings.getMaxStepNumber();
                int totalWaitTime = 0;
                boolean pass = true;
                for (int i = 0; i < maxStep + 1; i++) {
                    win.incrementFrame();
                    while (viewerAgent.isLoadingData() || viewerAgent.isRendering()) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        totalWaitTime += 1000;
                        if (totalWaitTime >= MAX_WAIT_TIME) {
                            int action = JOptionPane.showConfirmDialog(viewerDesktop, "The total wait has exceeded the max wait time (5 mins). Do you want to continue to wait (Yes) or ignore this snapshot (No)?",
                                    "Action Confirmation", JOptionPane.YES_NO_OPTION);
                            if (action == JOptionPane.NO_OPTION) {
                                pass = false;
                                break;
                            } else {
                                totalWaitTime = 0;
                            }
                        }
                    }
                    if (pass) {
                        captureImage();
                    }
                    totalWaitTime = 0;
                    pass = true;
                }

            }
        };

        Thread thread = new Thread(worker);
        thread.start();
    }

    public void captureImage() {
        if (viewerDesktop == null) {
            return;
        }
        CursorSettings.CURSOR_STYLE currentCursorStyle = viewerDesktop.getCursorSettings().getCursorStyle();
        viewerDesktop.getCursorSettings().setCursorStyle(CursorSettings.CURSOR_STYLE.NO_CURSOR);
        //java.awt.Container image = null;
        Image image = null;
        if (preferenceSettings.getCaptureOption() == CAPTURE_OPTION.ENTIRE_DESKTOP) {
            image = viewerDesktop.getImage();
        } else if (preferenceSettings.getCaptureOption() == CAPTURE_OPTION.SELECTED_FRAME) {
            image = viewerDesktop.getActiveWindow().getImage();
        }
        viewerDesktop.getCursorSettings().setCursorStyle(currentCursorStyle);
        if (image == null) {
            JOptionPane.showMessageDialog(movieDialog, "Please select a layer first.");
            return;
        }
        if (preferenceSettings.getSaveOption() == SAVE_OPTION.SAVE_IN_MEMORY) {
            images.add(image);
            setFrameList();
        } else if (preferenceSettings.getSaveOption() == SAVE_OPTION.SAVE_AS_JPEG) {
            String tmpFileName = preferenceSettings.getTemplateFileName();

            if (tmpFileName != null && tmpFileName.length() > 0) {
                String indexString = format.format(index);
                String pathname = tmpFileName + "." + indexString + ".jpg";
                index++;

                // form parameters for binary IO command
                String locPref = viewerAgent.getMessagingManager().getLocationPref();
                //TEST: force to be remote
                //locPref = QIWConstants.REMOTE_PREF;
                ArrayList params = new ArrayList();
                params.add(locPref);    // [0] IO preference

                params.add(pathname);   // [1] file pathname

                params.add(QIWConstants.JPEG_FORMAT);   // [2] file's format

                params.add(image);   // [3] binary data

                String msgID = viewerAgent.getMessagingManager().sendRequest(QIWConstants.CMD_MSG, QIWConstants.BINARY_FILE_WRITE_CMD,
                        QIWConstants.ARRAYLIST_TYPE, params, true);
                // wait for the response.
                IQiWorkbenchMsg response = null;
                int i = 0;
                while (response == null) {
                    response = viewerAgent.getMessagingManager().getMatchingResponseWait(msgID, MINUTE_WAIT_TIME);
                    i++;
                    if (i >= 4) {
                        break;
                    }
                }

                try {
                    if (MsgUtils.isResponseAbnormal(response)) {
                        logger.finest("Binary file write error:" + (String) MsgUtils.getMsgContent(response));
                        return;
                    }
                } catch (NullPointerException npe) {
                    logger.severe("SYSTEM ERROR: Timed out waiting for response to binary file write");
                    npe.getStackTrace();
                    return;
                }
            }
        }
    }

    public void setMovieDialog(MovieDialog dialog) {
        this.movieDialog = dialog;
    }

    public void shutdownMovieDialog() {
        if (movieDialog != null && movieDialog.isVisible()) {
            movieDialog.setVisible(false);
        }
    }

    public void setSelectedFrameIndex(int index) {
        movieDialog.setSelectedImageIndex(index);
    }

    public synchronized Image getImageByIndex(int idx) {
        //List<Image> images = framesEditController.getImages();
        if (images == null || images.size() == 0) {
            return null;
        }
        if (idx < 0) {
            idx = 0;
        }
        if (idx >= images.size()) {
            idx = images.size() - 1;
        }
        return (Image) (images.get(idx));
    }

    public void resetImages(int[] order, boolean deleteMode) {
        if (order == null) {
            return;
        }
        List<Image> newImages = new ArrayList<Image>();
        for (int i = 0; i < order.length; i++) {
            if (deleteMode) {
                newImages.add(images.get(i));
            } else {
                newImages.add(images.get(order[i]));
            }
        }
        images.clear();
        images = newImages;
    }

    public int getNumFrames() {
        //if(viewerDesktop.getActiveWindow() == null) {
        //    return 0;
        //}
        //if(framesEditController == null)
        //	return 0;
        //List<Image> images = framesEditController.getImages();
        return images.size();
    //return viewerDesktop.getActiveWindow().getFrameCount();
    }
}