/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.controller.EditorResultProcessor;
import com.bhpb.geographics.controller.EditorWorker;
import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.geographics.controller.layer.LayerSyncGroup;
import com.bhpb.geographics.controller.window.CrossSectionAnnotationPropertiesController;
import com.bhpb.geographics.controller.window.MapAnnotationPropertiesController;
import com.bhpb.geographics.controller.window.ScalePropertiesController;
import com.bhpb.geographics.controller.window.SyncPropertiesController;
import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.GeoGraphicsPropertyGroup;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.LayerFactory;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.ViewTarget;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties;
import com.bhpb.geographics.model.window.LayerLoader;
import com.bhpb.geographics.model.window.MapAnnotationProperties;
import com.bhpb.geographics.model.window.WindowAnnotationProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowModel.LAYER_TARGET;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowPropertyGroup;
import com.bhpb.geographics.model.window.WindowSyncBroadcaster;
import com.bhpb.geographics.model.window.WindowSyncGroup;
import com.bhpb.geographics.model.window.WindowSyncListener;
import com.bhpb.geographics.model.window.WindowSyncProperties;
import com.bhpb.geographics.model.window.WindowSyncProperties.SYNC_FLAG;
import com.bhpb.geographics.ui.AbstractGeoPlot.ACTION_MODE;
import com.bhpb.geographics.ui.LayerPanel;
import com.bhpb.geographics.ui.event.GeoPlotEvent;
import com.bhpb.geographics.ui.event.GeoPlotEventListener;
import com.bhpb.geographics.ui.event.WindowCursorEvent;
import com.bhpb.geographics.ui.event.WindowScaleEvent;
import com.bhpb.geographics.ui.event.WindowScrollEvent;
import com.bhpb.geographics.ui.layer.EditorDialog;
import com.bhpb.geographics.ui.layerlist.LayerListPanel;
import com.bhpb.geographics.ui.window.AbstractAnnotationPropertiesPanel;
import com.bhpb.geographics.ui.window.CrossSectionAnnotationPropertiesPanel;
import com.bhpb.geographics.ui.window.MapAnnotationPropertiesPanel;
import com.bhpb.geographics.ui.window.ScalePropertiesPanel;
import com.bhpb.geographics.ui.window.SyncPropertiesPanel;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.adapter.geoio.GeoIODirector;
import com.bhpb.qiviewer.adapter.geoio.GeoIOTransactionResult;
import com.bhpb.qiviewer.util.ProgressDialog;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A ViewerWindow is a JInternalFrame which provides the top-level view for the
 * WindowModel.  As such it implements one of the WindowModel's three ListDataListeners.
 * It presents an appropriate title depending on whether the WindowModel contains
 * a list of cross-section layers, map layers or is empty.  The ViewerWindow contains
 * a split pane with an ordered list view on the left and a map, wiggle or density
 * plot on the right.
 *
 * Each ViewerWindow posesses a unique identifier String and has accessors for
 * zooming, frame increment/decrement, capturing the plot image and interrogating
 * the type, orientation and position of the layers contained in the underlying WindowModel.
 *
 * The ViewerWindow allows all JInternalFrame features: resizable, moveable,
 * closeable, maximizable and iconifiable.
 */
public class ViewerWindow extends JInternalFrame implements EditorResultProcessor<GeoGraphicsPropertyGroup>,
        WindowSyncListener, WindowSyncBroadcaster, GeoPlotEventListener, ListDataListener,
        LayerLoader {

    private static final long serialVersionUID = -509488749286244560L;
    private static final Logger logger = Logger.getLogger(ViewerWindow.class.toString());
    private static final String EMPTY_WINDOW_TITLE = "Empty Window";
    private static Dimension DEFAULT_INITIAL_SIZE = new Dimension(600, 560);
    private GeoDataOrder orientation = GeoDataOrder.UNKNOWN_ORDER;
    private JSplitPane splitPane = null;
    private LayerListPanel layerListView = null;
    private LayerPanel layerPanel = null;
    private Map<GeoPlotEvent, Long> geoPlotEventTimes = new HashMap<GeoPlotEvent, Long>();
    private ViewerAgent agent;
    private WindowModel windowModel;
    private transient Point2D viewPos = null;

    ViewerWindow(String windowTitle, WindowModel initialWindowModel, ViewerAgent agent) {
        //ViewerWindow is now configurable, maximizable and closeable as in the bhpViewer
        super("", true, true, true, true);
        setName(windowTitle);

        this.windowModel = initialWindowModel;
        windowModel.addListDataListener(this);
        windowModel.setLayerLoader(this);

        WindowSyncGroup windowSyncGroup = agent.getDefaultWindowSyncGroup();

        this.agent = agent;

        initGUI();

        setSize(DEFAULT_INITIAL_SIZE);

        addInternalFrameListener(new ViewerWindowListener(layerPanel, agent, this));
        windowSyncGroup.addListener(this);
    }

    /**
     * Constructs a ViewerWindow.  Package-protected to enforce factory use.
     *
     * @param dsProperties
     * @param dataobjects
     * @param layerProps
     * @param agent
     * @param initialWindowModel
     */
    ViewerWindow(DatasetProperties dsProperties, DataObject[] dataobjects,
            LayerDisplayProperties layerProps, ViewerAgent agent,
            WindowModel initialWindowModel) {
        //ViewerWindow is now configurable, maximizable and closeable as in the bhpViewer
        super("", true, true, true, true);
        setName(initialWindowModel.getWindowID());

        this.windowModel = initialWindowModel;
        windowModel.addListDataListener(this);
        windowModel.setLayerLoader(this);

        WindowSyncGroup windowSyncGroup = agent.getDefaultWindowSyncGroup();

        this.agent = agent;

        logger.info("Validating ViewerWindow parameters...");
        validateParams(dsProperties, dataobjects, layerProps.getLayerDisplayParameters());

        addLayer(dsProperties, dataobjects, layerProps);

        initGUI();

        setSize(DEFAULT_INITIAL_SIZE);

        addInternalFrameListener(new ViewerWindowListener(layerPanel, agent, this));
        windowSyncGroup.addListener(this);
    }

    /**
     * Create a qiViewer's internal window restored with the state of a 
     * qiViewer's internal window. Package-protected to enforce factory use.
     *
     * @param node
     * @param agent
     * @param windowSyncGroup
     * @param progressDialog
     * @param initialWindowModel
     */
    ViewerWindow(Node node, ViewerAgent agent, WindowSyncGroup windowSyncGroup,
            ProgressDialog progressDialog, WindowModel initialWindowModel) {
        //ViewerWindow is now iconifiable, maximizable and closeable as in the bhpViewer
        super("", true, true, true, true);
        setName(initialWindowModel.getWindowID());

        this.windowModel = initialWindowModel;
        windowModel.addListDataListener(this);
        windowModel.setLayerLoader(this);

        //This attribute is necessary in case all layers were removed before the window was saved,
        //in order to know which type of Layers may be added after restoring, given the type of WindowProperties.
        orientation = GeoDataOrder.valueOf(ElementAttributeReader.getString(node, "orientation"));

        logger.info("Deserializing ViewerWindow node...");
        progressDialog.setWinType(orientation.toString());
        progressDialog.incrWinCnt();

        int height = ElementAttributeReader.getInt(node, "height");
        int width = ElementAttributeReader.getInt(node, "width");
        //do not invoke update size; the component is not yet valid
        //so no need to wait on the GeoPlot to be resized and rerasterized before returning
        setSize(width, height);

        int x = ElementAttributeReader.getInt(node, "x");
        int y = ElementAttributeReader.getInt(node, "y");
        updateLocation(x, y);

        logger.info("Deserializing layer nodes...");
        NodeList children = node.getChildNodes();
        //Search for the windowProperties node and deserialize it first as all layers need it
        Node winPropsNode = null;

        //Note: This must iterate rather than asuming the first child will be "windowProperties".
        //This reflects a format change which occurred when formatting was added - now, the first node
        //is named "#text" and is followed by "windowProperties."  Also, elements may not be reported
        //in document order, although they almost certainly will be.
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeName().equals(WindowProperties.getXmlAlias())) {
                winPropsNode = child;
            }
        }

        if (winPropsNode == null) {
            String errMsg = "Unable to restore ViewerWindow - WindowProperties node not found.";
            logger.warning(errMsg);
            throw new RuntimeException("errMsg");
        }

        WindowProperties windowProperties = new WindowProperties(winPropsNode);
        initialWindowModel.setWindowProperties(windowProperties);
        this.agent = agent;
        LayerFactory layerFactory = new LayerFactory(agent.getMessagingManager());

        initGUI();

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            //skip any (whitespace) text elements
            if ("#text".equals(childName)) {
                continue;
            } else if (WindowProperties.getXmlAlias().equals(childName)) {
                //skip windowProperties; it was alread parsed
                continue;
            }
            String layerName = ((Element) child).getAttribute("layerName");
            if (layerName == null) {
                layerName = "Unknown Layer Name";
            }

            if (progressDialog != null) {
                if (progressDialog.isCancelled()) {
                    break;
                }
                progressDialog.setLayerName(layerName);
            }

            addLayer(layerFactory.restoreLayer(child, windowProperties, agent.getViewerDesktop().getPickingSettings(),
                    agent.getViewerDesktop().getHorizonHighlightSettings(), agent.getDefaultLayerSyncGroup(), windowModel));
            if (progressDialog != null) {
                if (progressDialog.isCancelled()) {
                    break;
                }
                progressDialog.incrLayerCnt();
            }

        }

        addInternalFrameListener(new ViewerWindowListener(layerPanel, agent, this));
        windowSyncGroup.addListener(this);

        int scrollPosH = 0;
        try {
            scrollPosH = ElementAttributeReader.getInt(node, "scrollPosH");
        } catch (Exception ex) {
            logger.info("Unable to deserialize horiz. scrollbar position because attribute 'scrollPosH' of ViewerWindow '" + getWinID() + "' is missing or not a valid Integer.");
        }

        int scrollPosV = 0;
        try {
            scrollPosV = ElementAttributeReader.getInt(node, "scrollPosV");
        } catch (Exception ex) {
            logger.info("Unable to deserialize horiz. scrollbar position because attribute 'scrollPosV' of ViewerWindow '" + getWinID() + "' is missing or not a valid Integer.");
        }

        layerPanel.scrollPlotToVisiblePoint(scrollPosH, scrollPosV);
    }

    /**
     * Adds a GeoPlotEventListener to this Window.  Used to communicate with
     * processes which require completion or termination of rendering before proceeding,
     * such as image and movie capture.
     *
     * @param listener GeoPlotEventListener to add to this ViewerWindow.
     */
    public void addGeoPlotEventListener(GeoPlotEventListener listener) {
        layerPanel.addGeoPlotEventListener(listener);
    }

    /**
     * Adds a layer to this ViewerWindow.  If the Viewer already contains the Layer
     * then no action occurs
     * @param layer the new Layer to be added to this ViewerWindow
     */
    public void addLayer(final Layer layer) {
        if (layer == null) {
            throw new IllegalArgumentException("Cannot add null layer to ViewerWindow.layers.");
        }

        if (!windowModel.getLayers().contains(layer)) {
            windowModel.add(0, layer);

            //if a save set is being restored, the scaleProperties does not yet contain a reference to the annotated layer
            if (getWindowProperties().getAnnotationProperties().getAnnotatedLayerName().equals(layer.getLayerName())) {
                getWindowProperties().getScaleProperties().setAnnotatedLayer(layer);
            }

            agent.getDefaultLayerSyncGroup().addListener(layer);
        } else {
            logger.warning("Unable to add layer: " + layer.getLayerID() + ", it is already contained in this ViewerWindow.");
        }
    }

    /**
     * Converts a DataObject[] to BhpSuHorizonDataObject[] if all elements of the array
     * are BhpSuHorizonDataObjects.  Otherwise, an IllegalArgumentException is thrown.
     * This is a utility method used when adding (xsec) HorizonLayers.
     * 
     * @param dataObjects
     * 
     * @return dataObjects as a BhpSuHorizonDataObject[]
     */
    private BhpSuHorizonDataObject[] asHorizonDataObjects(DataObject[] dataObjects) {
        BhpSuHorizonDataObject[] horizonDataObjects = new BhpSuHorizonDataObject[dataObjects.length];
        for (int i = 0; i < dataObjects.length; i++) {
            if (dataObjects[i] instanceof BhpSuHorizonDataObject) {
                horizonDataObjects[i] = (BhpSuHorizonDataObject) dataObjects[i];
            } else {
                throw new IllegalArgumentException("Element " + i + " of dataObjects is not a BhpSuHorizonDataObject.");
            }
        }
        return horizonDataObjects;
    }

    /**
     * Creates a new Layer from the DatasetProperties, DataObject array and LayerDisplayProperties.
     * The layer is then added to this ViewerWindow's WindowModel and the ViewerAgent's LayerSyncGroup.
     *
     * @param dsProperties DatasetProperties
     * @param dataobjects DataObject array
     * @param layerProps LayerDisplayProperties
     *
     * @return reference to the newly created and added Layer
     */
    public Layer addLayer(DatasetProperties dsProperties, DataObject[] dataobjects, LayerDisplayProperties layerProps) {
        Layer layer;
        GeoIOAdapter geoIOadapter = new GeoIOAdapter(agent.getMessagingManager());
        LayerSyncGroup layerSyncGroup = agent.getDefaultLayerSyncGroup();
        GeoDataOrder dataOrder = dsProperties.getMetadata().getGeoDataOrder();
        if (dsProperties.isSeismic()) {
            layer = new SeismicLayer(dsProperties,
                    layerProps.getLayerDisplayParameters(),
                    layerProps.getLocalSubsetProperties(),
                    layerProps.getLayerSyncProperties(),
                    dataobjects,
                    geoIOadapter,
                    dataOrder == GeoDataOrder.MAP_VIEW_ORDER,
                    layerSyncGroup,
                    windowModel);
        } else if (dsProperties.isHorizon()) {
            BhpSuHorizonDataObject[] horizons = asHorizonDataObjects(dataobjects);
            switch (dataOrder) {
                case CROSS_SECTION_ORDER:
                    if (horizons.length > 1) {
                        logger.warning("Multiple horizons were read in cross-section order.  Confirm that a single horizon was selected and that the data is not in transpose order.");
                    }
                    layer = new HorizonLayer(dsProperties,
                            layerProps.getLayerDisplayParameters(),
                            layerProps.getLocalSubsetProperties(),
                            layerProps.getLayerSyncProperties(),
                            horizons,
                            geoIOadapter,
                            agent.getViewerDesktop().getPickingSettings(),
                            agent.getViewerDesktop().getHorizonHighlightSettings(),
                            false,
                            layerSyncGroup,
                            windowModel);
                    break;
                case MAP_VIEW_ORDER:
                    layer = new HorizonLayer(dsProperties,
                            layerProps.getLayerDisplayParameters(),
                            layerProps.getLocalSubsetProperties(),
                            layerProps.getLayerSyncProperties(),
                            horizons,
                            geoIOadapter,
                            agent.getViewerDesktop().getPickingSettings(),
                            agent.getViewerDesktop().getHorizonHighlightSettings(),
                            true,
                            layerSyncGroup,
                            windowModel);
                    break;
                default:
                    throw new IllegalArgumentException("Unable to add horizons layers because the GeoDataOrder is neither map nor cross-section.  It is: " + dataOrder);
            }
        } else if (dsProperties.isModel()) {
            layer = new ModelLayer(dsProperties,
                    layerProps.getLayerDisplayParameters(),
                    layerProps.getLocalSubsetProperties(),
                    layerProps.getLayerSyncProperties(),
                    dataobjects,
                    new GeoIOAdapter(agent.getMessagingManager()),
                    dataOrder == GeoDataOrder.MAP_VIEW_ORDER,
                    layerSyncGroup,
                    windowModel);
        } else {
            throw new IllegalArgumentException("Unable to create ViewerWindow using datasetProperties of unknown type: " + dsProperties.getClass().getSimpleName());
        }

        layerSyncGroup.addListener(layer);
        addLayer(layer);

        return layer;
    }

    /**
     * Broadcasts the requested ViewTarget to other members of the LayerSyncGroup.
     * @param targetProps
     */
    public void broadcastCursorPos(ViewTarget targetProps) {
        agent.getDefaultWindowSyncGroup().broadcast(new WindowCursorEvent(this, targetProps));
    }

    /**
     * Broadcasts the WindowScaleProperties to other members of the LayerSyncGroup.
     * @param windowScaleProperties
     */
    public void broadcastScale(WindowScaleProperties windowScaleProperties) {
        agent.getDefaultWindowSyncGroup().broadcast(new WindowScaleEvent(this, windowScaleProperties));
    }

    /**
     * Broadcasts the requested ViewTarget to other members of the LayerSyncGroup
     * @param newOrigin
     */
    public void broadcastScrollPos(ViewTarget newOrigin) {
        if (!isVisible()) {
            return;
        }
        WindowScrollEvent wse = new WindowScrollEvent(this, newOrigin);
        agent.getDefaultWindowSyncGroup().broadcast(wse);
    }

    /**
     * Determines whether this Window is currently capable of broadcasting any events.
     * @return true if the SYNC_FLAG.BROADCAST flag is set to true for this ViewerWindow's
     * SyncProperties and this ViewerWindow is currently selected.  ViewerWindows which are not selected
     * can only receive, not broadcast, events.
     */
    public boolean canBroadcast() {
        return getWindowProperties().getSyncProperties().isEnabled(SYNC_FLAG.BROADCAST) &&
                isSelected();
    }

    private void initGUI() {
        layerPanel = new LayerPanel(windowModel.getLayers(), windowModel.getWindowProperties(), agent.getCursorSettings(), this, windowModel);
        layerPanel.setName("layerPanel");

        layerListView = new LayerListPanel(windowModel, layerPanel);
        layerListView.setName("layerListView");

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(layerListView), layerPanel);

        splitPane.setDividerLocation(100);

        getContentPane().add(splitPane, BorderLayout.CENTER);
    }

    /**
     * Determines whether this is a cross-section ViewerWindow
     *
     * @return true if the GeoDataOrder of all Layers contained in this ViewerWindow's
     * WindowModel are GeoDataOrder.CROSS_SECTION_ORDER
     */
    public boolean isCrossSectionWindow() {
        return orientation == GeoDataOrder.CROSS_SECTION_ORDER;
    }

    /**
     * Determines whether this is a cross-section ViewerWindow
     *
     * @return true if the GeoDataOrder of all Layers contained in this ViewerWindow's
     * WindowModel are GeoDataOrder.MAP_VIEW_ORDER
     */
    public boolean isMapWindow() {
        return orientation == GeoDataOrder.MAP_VIEW_ORDER;
    }

    /**
     * Programatically load a new layer using the specified file path and the default
     * properties for the relevant dataset type.  The layer is inserted at the index
     * specified.  If any error occurs while loading or adding the layer, an info
     * level message is logged.
     *
     * @param filePath
     * @param index
     *
     * @return true if the file was read and layer loaded successfully, otherwise false
     */
    public boolean loadLayer(String filePath, int index) {
        logger.info("Loaded layer: " + filePath + " at index: " + index);
        GeoIODirector director = agent.getFileChooserDirector();

        LayerProperties dsPropsEditorResult = director.readLayerProperties(filePath, true);
        if (dsPropsEditorResult.isCancelled()) {
            logger.info("Unable to complete layer loading: user cancelled operation.");
            return false;
        } else if (dsPropsEditorResult.isErrorCondition()) {
            logger.info("Unable to complete layer loading: " + dsPropsEditorResult.getErrorMsg());
            return false;
        } else if (dsPropsEditorResult.isApplied()) {
            String errMsg = "The user should not be able to click Apply when initially editing dataset properties.";
            logger.info(errMsg);
            return false;
        } else {
            agent.setActiveWindow(this);
            GeoIOTransactionResult result = director.readData(dsPropsEditorResult, QIWConstants.ADD_LAYER, true, true);
            if (result.isErrorCondition()) {
                logger.info("An error occurred while loading a layer: " + result.getErrorMsg());
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Programatically move the requested layer by the given number of positions.
     * A positive posChange increases the Layer's position in the underlying List
     * (decreases Z-Order).
     *
     * @param layer the Layer to be moved
     * @param posChange int relative position change - negative value moves the layer up the Z axis
     */
    public void moveLayer(Layer layer, int posChange) {
        windowModel.moveLayer(layer, posChange);
    }

    /**
     * Programatically move the requested layer to the bottom of the WindowModel's List
     * (Z-Order 0).
     *
     * @param layer the Layer to be moved
     */
    public void moveLayerToBottom(Layer layer) {
        windowModel.moveLayer(layer, LAYER_TARGET.LIST_BOTTOM);
    }

    /**
     * Programatically move the requested layer to the bottom of the WindowModel's List
     * (Z-Order equal to number of Layers in the WindowModel - 1).
     *
     * @param layer the Layer to be moved
     */
    public void moveLayerToTop(Layer layer) {
        windowModel.moveLayer(layer, LAYER_TARGET.LIST_TOP);
    }

    /**
     * Programatically move the currently selected Layers, if any, to the bottom
     * of the WindowModel's List (Z-Order 0).
     *
     */
    public void moveSelectedLayersToBottom() {
        layerListView.moveSelectedLayers(LAYER_TARGET.LIST_BOTTOM);
    }

    /**
     * Programatically move the currently selected Layers, if any, to the top
     * of the WindowModel's List (Z-Order equal to number of Layers in the WindowModel - 1).
     *
     */
    public void moveSelectedLayersToTop() {
        layerListView.moveSelectedLayers(LAYER_TARGET.LIST_TOP);
    }

    public Layer getActiveLayer() {
        return windowModel.getActiveLayer();
    }

    /**
     * Gets the entire contents of this Component as an Image, including the LayerListPanel,
     * splitpane separator and GeoPlot.
     *
     * @return Image of this ViewerWindow component
     */
    public Image getImage() {
        BufferedImage bufferedImage = new BufferedImage(getWidth(), getHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics g = bufferedImage.getGraphics();
        paint(g);
        g.dispose();

        return bufferedImage;
    }

    /**
     * Gets a List of the Layers currently selected in this ViewerWindow's
     * LayerListPanel, in list order/reverse z-order.
     *
     * @return List of selected layers
     */
    public List<Layer> getSelectedLayers() {
        return windowModel.getSelectedLayers();
    }

    /**
     * Sets the 'hidden' field of each layer in the given List as requested.
     *
     * @param layers List of layers
     * @param isHidden if true the Layer will not be shown in the GeoPlot and will be
     * visibly 'hidden' in the LayerListPanel
     */
    public void setLayersHidden(List<Layer> layers, boolean isHidden) {
        for (Layer layer : layers) {
            layer.setHidden(isHidden);
        }
        layerListView.repaint();
        //redraw must be requested here because the primary layer listmodel has not changed, only the layer properties
        layerPanel.redraw(true, false);
    }

    /**
     * Opens the LayerPropertiesDialog for the current active (topmost selected)
     * layer.
     */
    public void showLayerPropertiesDialog() {
        layerListView.showPropertiesDialog(getActiveLayer());
    }

    /**
     * Gets the layer identifiers for all Layers in this ViewerWindow in List order.
     * 
     * return array of String IDs of all layers contained in this ViewerWindow
     */
    public String[] getLayerIDs() {
        String[] layerIDs = new String[windowModel.getLayers().size()];
        for (int i = 0; i < layerIDs.length; i++) {
            layerIDs[i] = windowModel.getLayers().get(i).getLayerID();
        }
        return layerIDs;
    }

    /**
     * Gets the position of the Layer in the WindowModel's List.
     *
     * @param layer
     *
     * @return 0-based Layer position or -1 if the Layer is not in the WindowModel
     */
    public int getLayerPosition(Layer layer) {
        return windowModel.getLayers().indexOf(layer);
    }

    /**
     * Gets the GeoDataOrder of all Layers in this ViewerWindow
     *
     * @return GeoDataOrder of all Layers or GeoDataOrder.UNKNOWN if the ViewerWindow
     * is empty.
     */
    public GeoDataOrder getOrientation() {
        return orientation;
    }

    /**
     * Invokes the LayerPanel's zoomIn() method.
     */
    public void zoomIn() {
        layerPanel.zoomIn();
    }

    /**
     * Invokes the LayerPanel's zoomOut() method.
     */
    public void zoomOut() {
        layerPanel.zoomOut();
    }

    /**
     * Invokes the LayerPanel's zoomAl() method.
     */
    public void zoomAll() {
        layerPanel.zoomAll();
    }

    /**
     * Updates the WindowProperties of this ViewerWindow if obj is one of the 
     * following types: ScaleProperties, AnnotationProperties, SyncProperties.
     *
     * A warning is logged if the GeoGraphicsPropertyGroup is not one of these types.
     *
     * @param properties any GeoGraphicsPropertyGroup
     */
    public void processEditorResult(GeoGraphicsPropertyGroup properties) {
        if (properties instanceof WindowPropertyGroup) {
            updateWindowPropertyGroup((WindowPropertyGroup) properties, false);
        } else if (properties instanceof HorizonHighlightSettings) {
            layerPanel.updateHorizonHighlightSettings((HorizonHighlightSettings) properties);
        } else if (properties instanceof CursorSettings) {
            layerPanel.updateCursorSettings((CursorSettings) properties);
        } else {
            logger.warning("Unable to process unknown type of EditorResult: " + properties.getClass().getName());
        }
    }

    /**
     * Updates the requested WindowPropertyGroup.  Has effect only for WindowPropertyGroups
     * (or portions of WindowPropertyGroups) that are distinct from the current value
     * of the WindowPropertyGroup for this ViewerWindow.
     *
     * To force the properties parameter to take effect, possibly causing redundant side effects,
     * use setWindowPropertyGroup().
     *
     * @param properties the requested WindowPropertyGroup to set
     */
    public void updateWindowPropertyGroup(WindowPropertyGroup properties, boolean requireDisinct) {
        if (properties instanceof WindowScaleProperties) {
            update((WindowScaleProperties) properties);
        } else if (properties instanceof WindowAnnotationProperties) {
            update((WindowAnnotationProperties) properties, requireDisinct);
        } else if (properties instanceof WindowSyncProperties) {
            update((WindowSyncProperties) properties);
        } else if (properties instanceof LayerSyncProperties) {
            update((LayerSyncProperties) properties);
        } else {
            throw new IllegalArgumentException("Cannot proces object of type: " + properties.getClass().getName());
        }
    }

    /**
     * Updates the ScaleProperties of this ViewerWindow.
     * 
     * @param updatedProperties ScaleProperties which will update this ViewerWindow
     */
    private void update(WindowScaleProperties updatedProperties) {
        layerPanel.updateWindowScaleProperties(updatedProperties);
    }

    /**
     * Updates the AnnotationProperties of this ViewerWindow.
     * 
     * @param updatedProperties AnnotationProperties which will update this ViewerWindow
     * @param requireDistinct if true, side effects will only occur if the AnnotationProperties
     * are different than the current value for this ViewerWindow
     */
    private void update(WindowAnnotationProperties updatedProperties, boolean requireDistinct) {
        if (updatedProperties instanceof CrossSectionAnnotationProperties && !isCrossSectionWindow()) {
            logger.warning("Unable to update CrossSectionAnnotationProperties: window '" + windowModel.getWindowID() + "' is a not a cross-section Window.");
            return;
        }
        if (updatedProperties instanceof MapAnnotationProperties && !isMapWindow()) {
            logger.warning("Unable to update CrossSectionAnnotationProperties: window '" + windowModel.getWindowID() + "' is a not a map Window.");
            return;
        }

        logger.info("Updating window annotation properties");

        //if requireDistinct is true but the new and current properties are equivalent, return
        if (requireDistinct && getWindowProperties().getAnnotationProperties().equals(updatedProperties)) {
            return;
        }

        if (updatedProperties instanceof CrossSectionAnnotationProperties) {
            getWindowProperties().setAnnotationProperties(new CrossSectionAnnotationProperties((CrossSectionAnnotationProperties) updatedProperties));
        } else if (updatedProperties instanceof MapAnnotationProperties) {
            getWindowProperties().setAnnotationProperties(new MapAnnotationProperties((MapAnnotationProperties) updatedProperties));
        }
        layerListView.repaint();
        layerPanel.updateWindowAnnotationProperties(updatedProperties);
        layerPanel.updateAnnotations();
    }

    /**
     * Updates the SyncProperties of this ViewerWindow.
     * 
     * @param updatedProperties SyncProperties which will update this ViewerWindow
     */
    private void update(LayerSyncProperties updatedProperties) {
        logger.warning("This method is not implemented but does not throw UnsupportedOperationException to allow CSM testing.");
    }

    /**
     * Updates the SyncProperties of this ViewerWindow.
     * 
     * @param updatedProperties SyncProperties which will update this ViewerWindow
     */
    private void update(WindowSyncProperties updatedProperties) {
        logger.fine("update(WindowSyncProperties) invoked - no action necessary");
    }

    /**
     * Immediately redraws the LayerListPanel and LayerPanel, interrupting any rasterization currently in progress.
     */
    public void redraw() {
        layerListView.repaint();
        layerPanel.redraw(true, false);
    }

    /**
     * Serializes the state of this <code>ViewerDesktop</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() throws Exception {
        String QUOTE_AND_SPACE = "\" ";
        StringBuffer content = new StringBuffer();
        content.append("<" + getXmlAlias() + " ");

        //save position in workbench canvas
        try {
            content.append("title=\"" + this.getTitle() + QUOTE_AND_SPACE);
            content.append("height=\"" + this.getHeight() + QUOTE_AND_SPACE);
            content.append("width=\"" + this.getWidth() + QUOTE_AND_SPACE);
            content.append("x=\"" + this.getX() + QUOTE_AND_SPACE);
            content.append("y=\"" + this.getY() + QUOTE_AND_SPACE);
            content.append("activeLayer=\"" + this.getActiveLayer().getLayerName() + QUOTE_AND_SPACE);
            content.append("orientation=\"" + this.orientation.toString() + QUOTE_AND_SPACE);
            content.append("dividerLoc=\"" + this.splitPane.getDividerLocation() + QUOTE_AND_SPACE);
            content.append("scrollPosH=\"" + this.layerPanel.getHorizontalScrollPos() + QUOTE_AND_SPACE);
            content.append("scrollPosV=\"" + this.layerPanel.getVerticalScrollPos() + QUOTE_AND_SPACE);
            content.append(">\n");
        } catch (Exception ex) {
            logger.warning("Unable to save ViewerWindow field due to: " + ex.getMessage());
            throw ex;
        }

        try {
            content.append(getWindowProperties().saveState());
        } catch (Exception ex) {
            logger.warning("Unable to save WindowProperties due to: " + ex.getMessage());
            throw ex;
        }

        int nLayers = windowModel.getLayers().size();
        logger.info("ViewerWindow has " + nLayers + " layers.");

        //Write the layers out in the order in which they were created
        //i.e. from the bottom of the list
        for (int i = nLayers - 1; i >= 0; i--) {
            Layer layer = windowModel.getLayers().get(i);
            try {
                String stateString = layer.saveState();
                content.append(stateString);
            } catch (Exception ex) {
                logger.warning("Unable to save layer #" + i + " due to: " + ex.getMessage());
                throw ex;
            }
        }

        content.append("</" + getXmlAlias() + ">\n");

        return content.toString();
    }

    public String getWinID() {
        return windowModel.getWindowID();
    }

    public WindowProperties getWindowProperties() {
        return windowModel.getWindowProperties();
    }

    public static String getXmlAlias() {
        return "viewerWindow";
    }

    /**
     * Sets the ViewerWindow's splitpane divider location to the requested pixel
     * position.
     *
     * @param loc new divider location; value < 0 will cause the splitpane to attempt
     * to honor the preferred size of the LayerPanel on the right then the LayerListPanel
     * on the left.
     */
    public void setDividerLocation(final int loc) {
        if (SwingUtilities.isEventDispatchThread()) {
            splitPane.setDividerLocation(loc);
        } else {
            SwingUtilities.invokeLater(
                    new Runnable() {

                        public void run() {
                            splitPane.setDividerLocation(loc);
                        }
                    });
        }
    }

    public void setActionMode(ACTION_MODE actionMode) {
        layerPanel.setActionMode(actionMode);
        final ACTION_MODE newActionMode = layerPanel.getActionMode();

        //request the current action mode again in case the requested mode
        //was overridden by LayerPanel behavior
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                agent.getViewerDesktop().setStatus("Action mode set: " + newActionMode);
            }
        });
    }

    public void setHidden(Layer layer, boolean hidden) {
        layer.setHidden(hidden);
        layerPanel.repaint();
    }

    public void toggleZoomSelectionMode() {
        ACTION_MODE currentActionMode = layerPanel.getActionMode();
        ACTION_MODE newActionMode;

        if (currentActionMode != ACTION_MODE.ZOOM_REGION) {
            newActionMode = ACTION_MODE.ZOOM_REGION;
        } else {
            newActionMode = ACTION_MODE.NONE;
        }

        setActionMode(newActionMode);
    }

    public void updateLocation(final int x, final int y) {
        Runnable locationSetter = new Runnable() {

            public void run() {
                setLocation(new Point(x, y));
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            setLocation(new Point(x, y));
        } else {
            SwingUtilities.invokeLater(locationSetter);
        }
    }

    /**
     * Invokes setSize(width,height) on the Event Dispatch thread using
     * invokeAndWait.  If the currentThread is the EDT, setSize() is called immediately.
     *
     * @param width the new width of theViewerWindow in pixels
     * @param height the new height of the ViewerWindow in pixels
     */
    public void updateSize(final int width, final int height) {
        if (SwingUtilities.isEventDispatchThread()) {
            setSize(new Dimension(width, height));
        } else {
            SwingUtilities.invokeLater(
                    new Runnable() {

                        public void run() {
                            setSize(new Dimension(width, height));
                        }
                    });
        }
        if (isVisible()) {
            waitForGeoPlotEvent(10000, GeoPlotEvent.GEOPLOT_RESIZED);
        }
    }

    /**
     * Blocks until a GeoPlotEvent is received, then log it.  A warning is logged
     * if no GeoPlotEvent is received before the specified timeOut has elapsed.
     *
     * @param timeOut maximum number of milleseconds to wait
     * @param geoPlotEvent one of GeoPlotEvent.RASTERIZATION_COMPLETE or RASTERIZATION_START
     */
    public void waitForGeoPlotEvent(long timeOut, GeoPlotEvent geoPlotEvent) {
        if (geoPlotEvent == null) {
            throw new IllegalArgumentException("Cannot listen for null GeoPlotEvent.");
        }
        long startTime = System.currentTimeMillis();
        Long matchingEventTime;

        do {
            //wait until a matching event (or any event is evType == null) is detected
            matchingEventTime = geoPlotEventTimes.get(geoPlotEvent);
            if (matchingEventTime != null && (matchingEventTime.longValue() > startTime)) {
                logger.info("Received GeoPlotEvent: " + geoPlotEvent.toString() + " after waiting " +
                        (matchingEventTime - startTime) + " ms.");
                return;
            }
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                logger.warning(ie.getMessage());
            }
        } while (System.currentTimeMillis() - startTime < timeOut);

        logger.warning("Did not receive GeoPlotEvent: " + geoPlotEvent.toString() + " within " + timeOut + "ms");
    }

    private void validateParams(DatasetProperties dsProperties, DataObject[] traces, LayerDisplayParameters layerDisplayParams) {
        if (dsProperties == null) {
            throw new IllegalArgumentException("Parameter: DatasetProperties was null in ViewerWindow constructor.");
        }
        if (dsProperties.getMetadata() == null) {
            throw new IllegalArgumentException("DatasetProperties' metadata was null in ViewerWindow constructor");
        }
        if (dsProperties.getMetadata().getGeoFileName() == null) {
            throw new IllegalArgumentException("DatasetProperties' metadata's GeoFileName was null in ViewerWindow constructor");
        }
        if (traces == null) {
            throw new IllegalArgumentException("Parameter: DataObject array was null in ViewerWindow constructor.");
        }
        if (traces.length == 0) {
            throw new IllegalArgumentException("Parameter: DataObject array was length 0 in ViewerWindow constructor.");
        }
        if (layerDisplayParams == null) {
            throw new IllegalArgumentException("Parameter: LayerDisplayParams was null in ViewerWindow constructor.");
        }
    }

    /**
     * Uses the JPanel implementation to reposition the JScrollPane associated
     * with this ViewerWindow so that the requested depth is visible.  If possible given
     * the scale and dimensions of the GeoPlot, this point will be positioned at the
     * topmost row of pixels, but this is not guaranteed.
     * 
     * @param timeOrDepth time or depth value to be made visible
     */
    public void scrollToVisiblePoint(double timeOrDepth) {
        try {
            layerPanel.scrollToVisiblePoint(timeOrDepth);
        } catch (IllegalArgumentException iae) {
            String errorMsg = "Unable to scroll to visible time or depth: " +
                    timeOrDepth + ".\nIt is not valid for ViewerWindow '" + windowModel.getWindowID() + "' due to:\n" + iae.getMessage();
            logger.warning(errorMsg);
            JOptionPane.showMessageDialog(this, errorMsg);
        }
    }

    public void editScaleProperties() {
        ScalePropertiesPanel scalePropertiesPanel = new ScalePropertiesPanel();
        ScalePropertiesController controller = new ScalePropertiesController(getWindowProperties().getScaleProperties(), scalePropertiesPanel);
        EditorDialog dlg = new EditorDialog(
                (JFrame) SwingUtilities.getRoot(this),
                scalePropertiesPanel, controller);

        EditorWorker<WindowScaleProperties> worker = new EditorWorker<WindowScaleProperties>(
                (EditorResultProcessor) this,
                controller,
                dlg);

        new Thread(worker).start();
    }

    public void editSyncProperties() {
        SyncPropertiesPanel syncPropertiesPanel = new SyncPropertiesPanel();
        SyncPropertiesController controller = new SyncPropertiesController(getWindowProperties().getSyncProperties(), syncPropertiesPanel);
        EditorDialog dlg = new EditorDialog(
                (JFrame) SwingUtilities.getRoot(this),
                syncPropertiesPanel, controller);

        EditorWorker<WindowSyncProperties> worker = new EditorWorker<WindowSyncProperties>(
                (EditorResultProcessor) this,
                controller,
                dlg);

        new Thread(worker).start();
    }

    public void editAnnotationProperties() {
        AbstractAnnotationPropertiesPanel annotationPropertiesPanel;
        AbstractEditorPanelController controller;

        if (isCrossSectionWindow()) {
            annotationPropertiesPanel = new CrossSectionAnnotationPropertiesPanel();

            controller = new CrossSectionAnnotationPropertiesController(
                    //edit a _copy_ of the CrossSectionAnnotationProperties
                    new CrossSectionAnnotationProperties((CrossSectionAnnotationProperties) getWindowProperties().getAnnotationProperties()),
                    (CrossSectionAnnotationPropertiesPanel) annotationPropertiesPanel);
        } else if (isMapWindow()) {
            annotationPropertiesPanel = new MapAnnotationPropertiesPanel();
            controller = new MapAnnotationPropertiesController(
                    //edit a _copy_ of the CrossSectionAnnotationProperties
                    new MapAnnotationProperties((MapAnnotationProperties) getWindowProperties().getAnnotationProperties()),
                    (MapAnnotationPropertiesPanel) annotationPropertiesPanel);
        } else {
            throw new UnsupportedOperationException("Cannot edit annotation properties for unknown window orientation: " + orientation);
        }

        EditorDialog dlg = new EditorDialog(
                (JFrame) SwingUtilities.getRoot(this),
                annotationPropertiesPanel, controller);

        EditorWorker<CrossSectionAnnotationProperties> worker = new EditorWorker<CrossSectionAnnotationProperties>(
                (EditorResultProcessor) this,
                controller,
                dlg);

        new Thread(worker).start();
    }

    /**
     * Synchronizes the cursor position of the active layer of this ViewerWindow
     * to the WindowCursorEvent's ViewerTarget.
     *
     * If this ViewerWindow has no current active layer, then this method has no effect,
     * otherwise setCursorTarget() is invoked on this ViewerWindow's LayerPanel.
     *
     * @param cursorEvt
     */
    public void handleCursorPosEvent(WindowCursorEvent cursorEvt) {
        ViewTarget evtTarget = cursorEvt.getTarget();
        Layer activeLayer = windowModel.getActiveLayer();

        //If there is an active layer in this window, synchronize the cursor, otherwise,
        //do nothing.
        if (activeLayer != null) {
            ViewTarget localTarget = ViewTarget.copyTargetProperties(evtTarget, activeLayer);
            layerPanel.setCursorTarget(localTarget);
        }
    }

    public void handleScaleEvent(WindowScaleEvent scaleEvt) {
        logger.info("Window " + getWinID() + " received WindowScaleEvent: " + scaleEvt.toString());
        WindowScaleProperties evtScaleProps = scaleEvt.getProperties();
        WindowScaleProperties myScaleProps = getWindowProperties().getScaleProperties();

        WindowScaleProperties newScaleProps = new WindowScaleProperties(myScaleProps);

        WindowSyncProperties mySyncProps = getWindowProperties().getSyncProperties();

        boolean scaleChanged = false;
        boolean listenToHscale = mySyncProps.isEnabled(SYNC_FLAG.H_SCALE_LISTENING);
        boolean listenToVscale = mySyncProps.isEnabled(SYNC_FLAG.V_SCALE_LISTENING);

        if (listenToHscale &&
                myScaleProps.getHorizontalScaleUnits().equals(evtScaleProps.getHorizontalScaleUnits())) {
            newScaleProps.setHorizontalScale(evtScaleProps.getHorizontalScale());
            scaleChanged = true;
        }

        if (listenToVscale &&
                myScaleProps.getVerticalScaleUnits().equals(evtScaleProps.getVerticalScaleUnits())) {
            newScaleProps.setVerticalScale(evtScaleProps.getVerticalScale());
            scaleChanged = true;
        }

        if (scaleChanged) {
            updateWindowPropertyGroup(newScaleProps, true);
        } else {
            logger.info("Scale change not processed by window #" + windowModel.getWindowID() + "; H_SCALE_LISTENING=" + listenToHscale + ", V_SCALE_LISTENING=" + listenToVscale);
        }
    }

    public void handleScrollEvent(WindowScrollEvent scrollEvt) {
        WindowSyncProperties syncProps = getWindowProperties().getSyncProperties();

        if (!syncProps.isEnabled(SYNC_FLAG.SCROLL_POS_LISTENING)) {
            logger.fine("Scroll pos listening disabled, returning from handleScrollEvent");
            return;
        }

        layerPanel.scrollToOrigin(scrollEvt.getOrigin());
    }

    public void incrementFrame() {
        getActiveLayer().shiftFrame(1);
    }

    public boolean isLoadingData() {
        for (Layer layer : windowModel.getLayers()) {
            if (layer.isLoadingData()) {
                return true;
            }
        }
        return false;
    }

    public boolean isRendering() {
        return layerPanel.isRendering();
    }

    public void decrementFrame() {
        getActiveLayer().shiftFrame(-1);
    }

    public void deleteLayer(Layer layer) {
        windowModel.remove(layer);
    }

    public int getCurrentFrame() {
        return 0;
    }

    public int getFrameCount() {
        return 1;
    }

    public void handleEvent(GeoPlotEvent evt) {
        geoPlotEventTimes.put(evt, System.currentTimeMillis());
    }

    public void setViewPosition(Point2D viewPos) {
        this.viewPos = viewPos;
    }

    /**
     * Allows the ViewerWindow to be scrolled (for instance after restoring from bhpViewer state)
     * or re-scrolled (for instance after adjusting the splitpane divider) to a specific point
     * configured using the setViewPosX and setViewPosY methods.
     */
    public void scrollToViewPosition() {
        WindowSyncProperties winSyncProps = getWindowProperties().getSyncProperties();
        boolean enableBroadcastAfterScrolling = false;

        if (winSyncProps.isEnabled(SYNC_FLAG.BROADCAST)) {
            enableBroadcastAfterScrolling = true;
            winSyncProps.setEnabled(SYNC_FLAG.BROADCAST, false);
        }

        //finally, enable broadcast is applicable after all layers have been restored
        layerPanel.scrollPlotToVisiblePoint((int) viewPos.getX(), (int) viewPos.getY());

        winSyncProps.setEnabled(SYNC_FLAG.BROADCAST, enableBroadcastAfterScrolling);
    }

    private void validateOrientation() {
        int nLayers = windowModel.getSize();

        final String newTitle;

        if (nLayers == 0) {
            newTitle = EMPTY_WINDOW_TITLE;
            orientation = GeoDataOrder.UNKNOWN_ORDER;
        } else {
            if (orientation != GeoDataOrder.UNKNOWN_ORDER) {
                //orientation is already set and windowModel is not empty
                return;
            } else {
                Layer firstLayer = windowModel.getLayer(0);
                orientation = firstLayer.getDatasetProperties().getMetadata().getGeoDataOrder();
                newTitle = getTitle(orientation);
            }
        }
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                setTitle(newTitle);
            }
        });
    }

    private String getTitle(GeoDataOrder orientation) {
        switch (orientation) {
            case UNKNOWN_ORDER:
                throw new IllegalArgumentException("Cannot construct ViewerWindow using datasetProperties with UNKNOWN_ORDER.");
            case CROSS_SECTION_ORDER:
                return "XSection Window";
            case MAP_VIEW_ORDER:
                return "Map Window";
            default:
                throw new IllegalArgumentException("Cannot construct ViewerWindow using unknown orientation: " + orientation);
        }
    }

    public void intervalRemoved(ListDataEvent lde) {
        validateOrientation();
    }

    public void intervalAdded(ListDataEvent lde) {
        validateOrientation();
    }

    public void contentsChanged(ListDataEvent lde) {
        validateOrientation();
    }
}