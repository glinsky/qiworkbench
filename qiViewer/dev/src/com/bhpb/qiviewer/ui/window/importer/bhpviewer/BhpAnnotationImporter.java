/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geoio.util.ElementAttributeReader;
import java.util.logging.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Factory which creates qiViewer annotation-related objects from bhpViewer
 * XML state elements.
 *
 * @author folsw9
 */
public class BhpAnnotationImporter {

    private static final Logger logger =
            Logger.getLogger(BhpAnnotationImporter.class.getName());

    /**
     * Deserializes the "AnnoTraceField" element and adds the resulting
     * Annotation to the Layer.
     *
     * @param emt the "AnnoTracField element of bhpViewer state XML
     */
    public static void addAnnotationTrace(Element emt) {
        double step = 5.0;
        boolean line;
        double gap = 40.0;
        double angle = 0.0;
        double limit = 0.0;
        int flag = 0;

        line = ElementAttributeReader.getBoolean(emt, "line");
        step = ElementAttributeReader.getDouble(emt, "step", step);
        gap = ElementAttributeReader.getDouble(emt, "gap", gap);
        angle = ElementAttributeReader.getDouble(emt, "angle", angle);
        limit = ElementAttributeReader.getDouble(emt, "limit", limit);
        flag = ElementAttributeReader.getInt(emt, "flag", flag);
    }

    private static void restoreAnnoGeneral(Node node) {
        int hloc = 0;
        int vloc = 0;
        float hmajor = 0;
        float hminor = 0;
        float vmajor = 0;
        float vminor = 0;

        hloc = ElementAttributeReader.getInt(node, "hlocation", hloc);
        vloc = ElementAttributeReader.getInt(node, "vlocation", vloc);
        hmajor = ElementAttributeReader.getFloat(node, "hmajorStep", hmajor);
        hminor = ElementAttributeReader.getFloat(node, "hminorStep", hminor);
        vmajor = ElementAttributeReader.getFloat(node, "vmajorStep", vmajor);
        vminor = ElementAttributeReader.getFloat(node, "vminorStep", vminor);
    }

    private static void restoreAnnoMisc(Node node) {
        try {
            int tloc = 0;
            tloc = ElementAttributeReader.getInt(node, "titleLocation", tloc);

            String tt = ElementAttributeReader.getString(node, "titleText");

            int cloc = 0;
            cloc = ElementAttributeReader.getInt(node, "colorbarLocation", cloc);

            String leftLabel = ElementAttributeReader.getString(node, "leftLabel");
            String rightLabel = ElementAttributeReader.getString(node, "rightLabel");
            String topLabel = ElementAttributeReader.getString(node, "topLabel");
            String bottomLabel = ElementAttributeReader.getString(node, "bottomLabel");
        } catch (Exception e) {
            logger.warning("Unable to restore a miscellaneous annotation from its bhpViewer XML state");
        }
    }

    private static void restoreAnnoSample(Node node) {
        try {
            int loc = 0;
            float minor = 1.0f;
            float major = 10.0f;

            boolean grid = ElementAttributeReader.getBoolean(node, "showGrid");
            boolean auto = ElementAttributeReader.getBoolean(node, "isAutomatic");
            loc = ElementAttributeReader.getInt(node, "location", loc);
            major = ElementAttributeReader.getFloat(node, "majorStep", major);
            minor = ElementAttributeReader.getFloat(node, "minorStep", minor);
        } catch (Exception e) {
            logger.warning("Unable to restore a sample annotation from its bhpViewer XML state");
        }
    }

    private static void restoreAnnoTrace(Node node) {
        try {
            int loc = 0;
            loc = ElementAttributeReader.getInt(node, "location", loc);

            String hSyncField = ElementAttributeReader.getString(node, "hSyncField");

            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child.getNodeType() == Node.ELEMENT_NODE) {
                    if (child.getNodeName().equals("AnnoTraceField")) {
                        BhpAnnotationImporter.addAnnotationTrace((Element) child);
                    }
                }
            }
        } catch (Exception e) {
            logger.warning("Unable to restore a trace annotation from its bhpViewer XML state");
        }
    }

    /**
     * Restore annotation setting from its bhpViewer XML state
     * @param viewer qiViewer
     * @param node <Annotation> XML node in <BhpWindow>'s XML state
     */
    static void restoreAnnotation(Node node) {
        boolean traceIni = false;
        boolean sampleIni = false;
        boolean miscIni = false;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals("AnnoGeneral")) {
                    restoreAnnoGeneral(child);
                    traceIni = true;
                    sampleIni = true;
                } else if (child.getNodeName().equals("AnnoTrace")) {
                    restoreAnnoTrace(child);
                    traceIni = true;
                } else if (child.getNodeName().equals("AnnoSample")) {
                    restoreAnnoSample(child);
                    sampleIni = true;
                } else if (child.getNodeName().equals("AnnoMisc")) {
                    restoreAnnoMisc(child);
                    miscIni = true;
                }
            }
            if (traceIni && sampleIni && miscIni) {
                break;
            }
        }
    }
}