/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.geographics.model.KeyRangeParseResult;
import com.bhpb.geographics.model.KeyRangeSerializer;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.shared.AutoGainControl;
import com.bhpb.geographics.model.layer.shared.ColorAttributes;
import com.bhpb.geographics.model.layer.shared.ColorMapModel;
import com.bhpb.geographics.model.layer.shared.Culling;
import com.bhpb.geographics.model.layer.shared.Normalization;
import com.bhpb.geographics.model.layer.shared.Normalization.NORMALIZATION_TYPE;
import com.bhpb.geographics.model.layer.shared.RasterizationTypeModel;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.ArbTravKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.ui.window.ViewerWindowFactory.Pipeline;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Deserializes BhpViewer Layer-related XML state elements and builds
 * geoIO/geoGraphics objects.
 */
public class BhpLayerImporter {

    private static final Logger logger =
            Logger.getLogger(BhpLayerImporter.class.getName());
    private static final int NO_SECONDARY_KEY = -2;

    /**
     * Method copied from GeoIODirector
     * @param metadata
     * @return
     */
    private static DatasetProperties createDatasetProperties(GeoFileMetadata metadata) {
        if (metadata.isSeismicFile()) {
            ArrayList horizons = ((SeismicFileMetadata) metadata).getHorizons();

            if (horizons != null && horizons.size() > 0) {
                return new HorizonProperties((SeismicFileMetadata) metadata);
            } else {
                return new SeismicProperties((SeismicFileMetadata) metadata);
            }
        } else if (metadata.isModelFile()) {
            return new ModelProperties((ModelFileMetadata) metadata);
        } else {
            throw new IllegalArgumentException("metadata is not one of: seismic, model, horizon");
        }
    }

    /**
     * Parses the content String and returns a table, a Vector of Vectors,
     * where each row Vector contains a series of Objects as described in
     * {@link parseContentRow}.
     *
     * @param content String of '|' delimited dataset property table row substrings
     * @param isSeismic true if the associated dataset is BHP-SU Seismic
     *
     * @return dataset properties table in Vector<Vector> format
     */
    private static Vector<Vector> getTable(String content, boolean isSeismic) {
        String[] _COLUMNNAMES = {
            "Key", "FullRange", "ChosenRange", "Order", "Increment",
            "Offset", "Synchronize", "Interpolation", "Binsize"};

        int expectedRowCount;

        if (isSeismic) {
            expectedRowCount = _COLUMNNAMES.length;
        } else {
            expectedRowCount = _COLUMNNAMES.length - 1; // only seismic layers have 'binSize' field
        }

        StringTokenizer stk = new StringTokenizer(content, "|");
        Vector<Vector> table = new Vector<Vector>();
        while (stk.hasMoreTokens()) {
            Vector row = parseContentRow(expectedRowCount, stk);
            table.add(row);
        }
        return table;
    }

    /**
     * Initializes local subset properties.
     * Has no effect - see issue http://www.qiworkbench.org:8080/jira/browse/QIVIEWER-80
     *
     * @param node
     * @param pipeline
     */
    private static void initPlotDataSelector(Node node, Pipeline pipeline, LocalSubsetProperties lsProps) {
        try {
            int pkey = ElementAttributeReader.getInt(node, "primaryKey");
            double pstart = ElementAttributeReader.getDouble(node, "primaryKeyStart");
            double pend = ElementAttributeReader.getDouble(node, "primaryKeyEnd");
            double pstep = ElementAttributeReader.getDouble(node, "primaryKeyStep");
            double sampleValueS = ElementAttributeReader.getDouble(node, "sampleValueStart");
            double sampleValueE = ElementAttributeReader.getDouble(node, "sampleValueEnd");
            String useNewBhpio = ElementAttributeReader.getString(node, "useNewBhpio");
            if (useNewBhpio.length() == 0) {
                if (!(pipeline == Pipeline.HORIZON_PIPELINE)) {
                    sampleValueS = sampleValueS * 1000;
                    sampleValueE = sampleValueE * 1000;
                }
            }

            boolean applyGaps = ElementAttributeReader.getBoolean(node, "applyGaps");
            int gapSize = ElementAttributeReader.getInt(node, "gapSize");

            lsProps.setPrimaryKey(
                    Integer.valueOf(pkey).toString(),
                    BigDecimal.valueOf(pstart).intValue(),
                    BigDecimal.valueOf(pend).intValue(),
                    BigDecimal.valueOf(pstep).intValue(),
                    applyGaps,
                    gapSize);

            int skey = ElementAttributeReader.getInt(node, "secondaryKey");
            if (skey != NO_SECONDARY_KEY) {
                double sstart = ElementAttributeReader.getDouble(node, "secondaryKeyStart");
                double send = ElementAttributeReader.getDouble(node, "secondaryKeyEnd");
                double sstep = ElementAttributeReader.getDouble(node, "secondaryKeyStep");

                lsProps.setSecondaryKey(
                        Integer.valueOf(skey).toString(),
                        BigDecimal.valueOf(sstart).intValue(),
                        BigDecimal.valueOf(send).intValue(),
                        BigDecimal.valueOf(sstep).intValue());
            }
        } catch (Exception e) {
            logger.warning("Unable to parse 'pipeline' from its bhpViewer XML state for a plot data selector");
        }
    }

    static void initColorAttribs(Node node, ColorAttributes colorAttribs) {
        double cs = -1;
        double ce = 1;

        cs = ElementAttributeReader.getDouble(node, "colorInterpStart", cs);
        ce = ElementAttributeReader.getDouble(node, "colorInterpEnd", ce);

        colorAttribs.setMin(cs);
        colorAttribs.setMax(cs);
    }

    static void initCullingParams(Node node, Culling culling) {
        float clip = ElementAttributeReader.getFloat(node, "clippingValue");
        double wiggleDecimation = 1.0;
        wiggleDecimation = ElementAttributeReader.getDouble(node, "wiggleDecimation",
                wiggleDecimation);

        logger.warning("Conversion of clipping factor from float to int - possible loss of precision.");
        culling.setClippingFactor((int) clip);

        logger.warning("Conversion of wiggle decimation from double to int - possible loss of precision.");
        culling.setDecimationSpacing((int) wiggleDecimation);
    }

    static void initColorMap(Node node,
            LayerDisplayParameters ldParams) {

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals("Colormap")) {
                    ColorMapModel colorMapModel = BhpColorMapImporter.getColorMapModel(ldParams);
                    colorMapModel.setColorMap(BhpColorMapImporter.getColormap(child));
                    break;
                }
            }
        }
    }

    private static void initPlotScaling(Node node, Pipeline pipeline, LayerDisplayParameters ldParams) {
        final NORMALIZATION_TYPE[] normTypes = {NORMALIZATION_TYPE.MAXIMUM,
            NORMALIZATION_TYPE.TRACE_MAXIMUM,
            NORMALIZATION_TYPE.AVERAGE,
            NORMALIZATION_TYPE.TRACE_AVERAGE,
            NORMALIZATION_TYPE.RMS,
            NORMALIZATION_TYPE.TRACE_RMS,
            NORMALIZATION_TYPE.LIMITS
        };
        final String errMsg = "Unable to initialize normalization";

        Normalization normalization = null;
        if (ldParams instanceof SeismicXsecDisplayParameters) {
            normalization = ((SeismicXsecDisplayParameters) ldParams).getNormalizationProperties();
        } else if (ldParams instanceof ModelXsecDisplayParameters) {
            normalization = ((ModelXsecDisplayParameters) ldParams).getNormalizationProperties();
        } else if (ldParams instanceof HorizonXsecDisplayParameters) {
            logger.warning(errMsg + " because qiViewer HorizonXsecDisplayParameters class does not support configurable normalization.");
            return;
        } else if (ldParams instanceof MapDisplayParameters) {
            logger.warning(errMsg + " because qiViewer MapDisplayParameters class does not support configurable normalization.");
            return;
        } else {
            logger.warning("Unknown type of LayerDisplayParameters: " + ldParams.getClass().getName() + ".  " + errMsg + ".");
            return;
        }
        try {
            int ntype = ElementAttributeReader.getInt(node, "normType");

            if (ntype < 0 || ntype > 6) {
                logger.warning("Invalid normalization type:" + ntype + ".  " + errMsg);
                return;
            } else {
                NORMALIZATION_TYPE normType = normTypes[ntype];
                normalization.setNormalizationType(normType);

                float min = ElementAttributeReader.getFloat(node, "normLimitMin");
                normalization.setNormalizationMinValue(min);

                float max = ElementAttributeReader.getFloat(node, "normLimitMax");
                normalization.setNormalizationMaxValue(max);

                float nscale = ElementAttributeReader.getFloat(node, "normScale");
                normalization.setNormalizationScale(nscale);

                logger.info("Skipping interpolationType attribute if present - bhpViewer plots always use linear interpolation.");
            }
        } catch (Exception e) {
            logger.warning("Unable to parse 'pipeline' from its bhpViewer XML state for setting the plotting scale");
        }
    }

    private static void initLayerSyncFlags(DatasetProperties dsProperties, LayerSyncProperties syncProps, int syncField,
            boolean broadcastEnabled) {
        syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.BROADCAST, broadcastEnabled);
        //syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.VISIBILITY, visible);

        GeoDataOrder order = dsProperties.getMetadata().getGeoDataOrder();
        switch (order) {
            case CROSS_SECTION_ORDER: //HEX value of syncField when all configurable seismic xsec flags are set to true is 200ECDC
                //These properties apply to all cross-sections
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.VISIBILITY, (syncField & 0x0000010) != 0);
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.OPACITY, (syncField & 0x0000040) != 0);
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.TIME_DEPTH_RANGE, (syncField & 0x0002000) != 0);

                //These properties are specific to certain dataset types
                if (dsProperties.isHorizon()) {
                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.EVENT_ATTRIBUTE, (syncField & 0x0020000) != 0);
                } else if (!(dsProperties.isSeismic() || dsProperties.isModel())) {
                    logger.warning("Unable to configure additional dataset-specific sync flags for unknown dsProperties type.");
                    return;
                } else {
                    //0x1000000 is WINDOW_VIEWPOS_SYNC, which is a WINDOW sync flag
                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.AGC, (syncField & 0x0008000) != 0);
                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.NORM_TYPE, (syncField & 0x0004000) != 0);
                    //0x0001000 is SELECTOR_FLAG, a mystery
                    //syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.PLOT_TYPE, (syncField & 0x0001000) != 0);

                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.INTERPOLATION, (syncField & 0x0000800) != 0);
                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.POLARITY, (syncField & 0x0000400) != 0);
                    //0x0000200 is SCALEV_FLAG, which is a WINDOW sync flag
                    //0x0000100 is SCALEH_FLAG, which is a WINDOW sync flag

                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.NORM_SCALE, (syncField & 0x0000080) != 0);
                    //0x0000020 ORDER_FLAG unknown significance for xsec layers

                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.PLOT_TYPE, (syncField & 0x0000008) != 0);
                    syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.COLORMAP, (syncField & 0x0000004) != 0);
                    //0x0000002 is CURSOR_FLAG, a WINDOW sync property
                    //0x0000001 is not used
                    if (dsProperties.isModel()) {
                        syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.COLOR_INTERP, (syncField & 0x0010000) != 0);
                    } else if (dsProperties.isSeismic()) {
                        syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.WIGGLE_DECIMATION, (syncField & 0x2000000) != 0);
                    }
                }
                break;
            case MAP_VIEW_ORDER: //HEX value of syncField when all map flags are set to true is 10454
                //0x4000000 EVENT_VALUE_FLAG... a mystery
                //0x2000000 WIGGLE_DECIMATION does not apply to map layer sync flags
                //0x1000000 is WINDOW_VIEWPOS_SYNC, which is a WINDOW sync flag

                //0x0080000 MAPSETTING_FLAG... a mystery
                //0x0040000 EVENTNAME_FLAG... how is a NAME a bit?
                //0x0020000 EVENTATT_FLAG... a mystery
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.COLOR_INTERP, (syncField & 0x0010000) != 0);

                //0x0000800 is INTERPOLATION_FLAG, does not apply to maps
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.POLARITY, (syncField & 0x0000400) != 0);
                //0x0000200 is SCALEV_FLAG, which is a WINDOW sync flag
                //0x0000100 is MAP_ASPECTRATIO_FLAG, which is a window scale property but not a configurable sync flag

                //0x0000080 unknown
                //referred to as MAY_Y_DIR_FLAG but is actually opacity
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.OPACITY, (syncField & 0x0000040) != 0);
                //0x0000020 unknown
                //0x0000010 is variously referred to in bhpViewer code asCOLORBAR_FLAG or MAP_Y_END, but is actually visibility for a map
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.VISIBILITY, (syncField & 0x0000010) != 0);

                //0x0000008 is MAP_Y_START
                //0x0000004 is referred to as MAP_X_END in bhpViewer code but is COLORMAP in this context
                syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.COLORMAP, (syncField & 0x0000004) != 0);
                //0x0000002 is MAP_X_START
                //0x0000001 is not used

                break;
            default:
                logger.warning("Unable to set layer sync flags because dataset order is not recognized: " + order);
        }
    }

    private static void initPlotAGC(Node node, LayerDisplayParameters ldParams) {
        AutoGainControl agc = null;
        if (ldParams instanceof SeismicXsecDisplayParameters) {
            agc = ((SeismicXsecDisplayParameters) ldParams).getAutoGainControl();
        }
        if (agc == null) {
            logger.warning("ldParams.autoGainControl is null or ldParams is not SeismicXsecDisplayParameters, initPlotAGC has no effect.");
            return;
        }
        try {
            boolean agcApply = ElementAttributeReader.getBoolean(node, "applyAGC");
            agc.setEnabled(agcApply);
            int agcUnits = ElementAttributeReader.getInt(node, "measureUnit");
            logger.warning("AGC units not yet implemented: cannot set units to " + agcUnits);
            double agcLen = ElementAttributeReader.getDouble(node, "windowLength");
            agc.setWindowLength((int) agcLen);
        } catch (Exception e) {
            logger.warning("Unable to parse 'pipeline' from its bhpViewer XML state for setting the auto gain controller's settings: " + e.getMessage());
        }
    }

    static void initPropertiesFromNode(DatasetProperties datasetProperties,
            Node parameterNode, boolean isSeismic) {
        String content = ElementAttributeReader.getString(parameterNode, "content");
        Vector<Vector> table = getTable(content, isSeismic);

        GeoFileMetadata metadata = datasetProperties.getMetadata();
        for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
            Vector rowVector = table.get(rowIndex);
            String keyName = (String) rowVector.get(0);
            String chosenRangeStr = (String) rowVector.get(2);
            DiscreteKeyRange fullKeyRange = metadata.getKeyRange(keyName);
            KeyRangeParseResult result = KeyRangeSerializer.parseRange(chosenRangeStr, fullKeyRange);
            if (result.hasErrors()) {
                throw new RuntimeException("Unable to parse chosen range: " + chosenRangeStr + " due to " + result.getErrorMessage());
            } else {
                AbstractKeyRange chosenRange = result.getKeyRange();
                if (chosenRange instanceof ArbTravKeyRange) {
                    datasetProperties.setKeyChosenArbTrav(keyName, (ArbTravKeyRange) chosenRange);
                } else {
                    datasetProperties.setKeyChosenRange(keyName, chosenRange);
                }
            }
        }
    }

    /**
     * Initializes the RasterizationTypeModel based on the plotType attribute of the PlotRasterizer node.
     * This attribute is a bitfield with bits
     * <ul>
     *   <li>1 : wiggle trace</li>
     *   <li>2 : positive fill</li>
     *   <li>4 : negative fill</li>
     *   <li>8 : variable density</li>
     *   <li>16 : interpolated density</li>
     * </ul>
     * @param node
     * @param rasterTypeModel
     */
    static void initRasterTypes(Node node, RasterizationTypeModel rasterTypeModel) {
        int plotTypeField = ElementAttributeReader.getInt(node, "plotType");

        rasterTypeModel.setRasterizeWiggleTraces((plotTypeField & 1) != 0);
        rasterTypeModel.setRasterizePositiveFill((plotTypeField & 2) != 0);
        rasterTypeModel.setRasterizeNegativeFill((plotTypeField & 4) != 0);
        rasterTypeModel.setRasterizeVariableDensity((plotTypeField & 8) != 0);
        rasterTypeModel.setRasterizeInterpolatedDensity((plotTypeField & 16) != 0);
    }

    /**
     * Check if a bhpViewer XML node is a layer node.
     * @param nodeName Name of the layer node
     * @return true if node is a layer node; otherwise, false
     */
    static boolean isDataLayer(String nodeName) {
        if (nodeName.equals("BhpModelLayer") || nodeName.equals("BhpSeismicLayer") || nodeName.equals("BhpEventLayer") || nodeName.equals("BhpMapLayer") || nodeName.equals("BhpLogLayer")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Based on BhpWindow::makeBhpLayerWithXML(), this is the essential method
     * of ViewerWindowFactory which gathers layer and rasterizer attributes and constructs
     * a set of LayerProperties from which a Layer may be built and then added to
     * the ViewerWindow.
     * @param node w3c.dom.Node named BhpLayer
     * @param agent ViewerAgent
     * @param geoIOadapter geoIOadapter used to read DatasetProperties, GeoFileMetadata and DataObjects
     *
     * @return LayerProperties, which will in turn return isError() == true if the node
     * does not have a child named BhpLayer, or this child node does not in turn
     * have children named Paramter and Pipeline, or
     * if some other error occurs during deserialization.
     */
    static LayerProperties loadLayerProperties(Node node, ViewerAgent agent, GeoIOAdapter geoIOadapter) {
        String nodeName = node.getNodeName();

        if (!isDataLayer(nodeName)) {
            return LayerProperties.createErrorResult("Unable to load a layer from its bhpViewer XML state. Invalid node name: " + nodeName);
        }

        Node bhpLayerNode = XmlUtils.getChild(node, "BhpLayer");
        if (bhpLayerNode == null) {
            return LayerProperties.createErrorResult("Unable to load a layer from its bhpViewer XML state. Cannot find <BhpLayer> node");
        }

        Node parameterNode = XmlUtils.getChild(bhpLayerNode, "Parameter");
        Node pipelineNode = XmlUtils.getChild(bhpLayerNode, "Pipeline");

        if (parameterNode == null || pipelineNode == null) {
            return LayerProperties.createErrorResult("Unable to load a layer from its bhpViewer XML state. Every <BhpLayer> node must have children named <Parameter> and <Pipeline>");
        }

        //basic datset-related attributes
        int id = ElementAttributeReader.getInt(bhpLayerNode, "idNumber");
        String name = ElementAttributeReader.getString(bhpLayerNode, "name");
        String dataName = ElementAttributeReader.getString(bhpLayerNode, "dataName");
        String pathlist = ElementAttributeReader.getString(bhpLayerNode, "pathlist");
        String dstypeString = ElementAttributeReader.getString(bhpLayerNode, "dsType");

        //additional layer-specific attributes
        boolean enableTalk = ElementAttributeReader.getBoolean(bhpLayerNode, "enableTalk");
        int synFlag = ElementAttributeReader.getInt(bhpLayerNode, "synFlag");
        int transparent = ElementAttributeReader.getInt(bhpLayerNode, "transparent");
        String property = ElementAttributeReader.getString(bhpLayerNode, "property");
        boolean propertySyn = ElementAttributeReader.getBoolean(bhpLayerNode, "propertySync");
        boolean reversedPolarity = ElementAttributeReader.getBoolean(bhpLayerNode, "reversedPolarity");
        boolean visible = ElementAttributeReader.getBoolean(bhpLayerNode, "visible", true);
        boolean reversedSampleOrder = ElementAttributeReader.getBoolean(bhpLayerNode, "reversedSampleOrder");

        // layer vertical and horizontal scale
        double hScale = ElementAttributeReader.getDouble(bhpLayerNode, "scaleFactorH");
        double vScale = ElementAttributeReader.getDouble(bhpLayerNode, "scaleFactorV");

        GeoFileMetadata metadata = geoIOadapter.readMetadata(pathlist);
        DatasetProperties dsProperties = createDatasetProperties(metadata);

        LayerDisplayProperties ldProperties;

        if (nodeName.equals("BhpSeismicLayer")) {
            ldProperties = BhpSeismicLayerImporter.initSeismicLayer(parameterNode, hScale, vScale,
                    (SeismicProperties) dsProperties, geoIOadapter);
        } else if (nodeName.equals("BhpModelLayer")) {
            ldProperties = BhpModelLayerImporter.initModelLayer(parameterNode, hScale, vScale,
                    (ModelProperties) dsProperties, geoIOadapter, property);
        } else if (nodeName.equals("BhpEventLayer")) {
            ldProperties = BhpHorizonLayerImporter.initHorizonLayer(node, parameterNode, (HorizonProperties) dsProperties, geoIOadapter, property);
        } else if (nodeName.equals("BhpMapLayer")) {
            ldProperties = BhpMapLayerImporter.initMapLayer(node, parameterNode, hScale, vScale,
                    dsProperties, geoIOadapter);
        } else {
            throw new RuntimeException("Unknown layer type: " + nodeName);
        }

        // If this is a horizon xsec layer, the Pipeline node is to be parsed as
        // a HORIZON_PIPELINE, otherwise it is a DEFAULT_PIPELINE
        if (metadata.getGeoFileType() == GeoFileType.EVENT_GEOFILE &&
                metadata.getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER) {
            restoreDefaultPipeline(pipelineNode, Pipeline.DEFAULT_PIPELINE, ldProperties);
        } else {
            restoreDefaultPipeline(pipelineNode, Pipeline.HORIZON_PIPELINE, ldProperties);
        }

        LayerDisplayParameters ldParams = ldProperties.getLayerDisplayParameters();
        ldParams.setLayerName(name);
        ldParams.setHidden(!visible);

        initLayerSyncFlags(dsProperties, ldProperties.getLayerSyncProperties(), synFlag,
                enableTalk);

        return LayerProperties.createUserAcceptedResult(dsProperties, ldProperties);
    }

    private static Vector parseContentRow(int expectedRowCount, StringTokenizer stk) {
        Vector row = new Vector();
        row.add(stk.nextToken());
        row.add(stk.nextToken());
        row.add(stk.nextToken());
        row.add(Integer.valueOf(stk.nextToken()));
        row.add(Double.valueOf(stk.nextToken()));
        row.add(Double.valueOf(stk.nextToken()));
        row.add(Boolean.valueOf(stk.nextToken()));
        if (expectedRowCount > 7) {
            row.add(Boolean.valueOf(stk.nextToken()));
        } else {
            row.add(Boolean.valueOf(true));
        }
        if (expectedRowCount > 8) {
            row.add(Integer.valueOf(stk.nextToken()));
        } else {
            row.add(Integer.valueOf(0)); //binSize is not part of the bhpViewer xml format
        }

        return row;
    }

    private static void restoreDefaultPipeline(Node node, Pipeline pipeline, LayerDisplayProperties ldProps) {
        boolean selectorIni = false;
        boolean scalingIni = false;
        boolean agcIni = false;
        boolean rasterIni = false;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);

            if (child.getNodeType() == Node.ELEMENT_NODE) {
                String childNodeName = child.getNodeName();
                LayerDisplayParameters ldParams = ldProps.getLayerDisplayParameters();
                if (childNodeName.equals("PlotDataSlector")) {
                    LocalSubsetProperties lsProps = ldProps.getLocalSubsetProperties();
                    initPlotDataSelector(child, pipeline, lsProps);
                    selectorIni = true;
                } else if (childNodeName.equals("PlotScaling")) {
                    initPlotScaling(child, pipeline, ldParams);
                    scalingIni = true;
                } else if (childNodeName.equals("PlotAGC")) {
                    initPlotAGC(child, ldParams);
                    agcIni = true;
                } else {
                    boolean isModel;
                    if (childNodeName.equals("PlotRasterizer")) {
                        isModel = false;
                    } else if (childNodeName.equals("ModelRasterizer")) {
                        isModel = true;
                    } else {
                        logger.info("Skipping unrecognized pipeline child node: " + childNodeName);
                        return;
                    }
                    if (ldParams instanceof SeismicXsecDisplayParameters) {
                        BhpSeismicLayerImporter.initSeismicXsecDisplayParameters(child, (SeismicXsecDisplayParameters) ldParams);
                    } else if (ldParams instanceof ModelXsecDisplayParameters) {
                        BhpModelLayerImporter.initModelXsecDisplayParameters(child, (ModelXsecDisplayParameters) ldParams);
                    } else if (ldParams instanceof HorizonXsecDisplayParameters) {
                        BhpHorizonLayerImporter.initHorizonXsecDisplayParameters(child, (HorizonXsecDisplayParameters) ldParams);
                    } else if (ldParams instanceof MapDisplayParameters) {
                        BhpMapLayerImporter.initMapDisplayParameters(child, isModel, (MapDisplayParameters) ldParams);
                    } else {
                        throw new RuntimeException("Unable to restore default Pipeline for unknown type of LayerDisplayParameters: " + ldParams.getClass().getName());
                    }
                    rasterIni = true;
                }
            }
            if (selectorIni && scalingIni && rasterIni && agcIni) {
                break;
            }
        }
    }

    /**
     * Restore the layer from its bhpViewer XML state.
     * @param child Layer node
     */
    static void restoreLayer(Node child, ViewerAgent agent) {
        String nodeName = child.getNodeName();
        if (nodeName.equals("Annotation")) {
            BhpAnnotationImporter.restoreAnnotation(child);
        } else if (nodeName.equals("HorizonGraph")) {
            HorizonGraphImporter.restoreHorizonGraph(child);
        } else {
            throw new RuntimeException("Unrecognized element: " + nodeName);
        }
    }
}