/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window;

import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.window.MapAnnotationProperties;
import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowSyncGroup;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoFileType;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.ui.window.importer.bhpviewer.BhpWindowImporter;
import com.bhpb.qiviewer.util.ProgressDialog;
import java.math.BigDecimal;
import org.w3c.dom.Node;

/**
 * Creates a ViewerWindow from a dataset, layer, qiViewer state XML or bhpViewer
 * state XML.
 */
public class ViewerWindowFactory {

    public enum Pipeline {

        DEFAULT_PIPELINE, HORIZON_PIPELINE
    }

    public enum XML_FORMAT {

        BHPVIEWER, QIVIEWER
    }

    /**
     * Creates a ViewerWindow.  Used by qiViewer New XSec/Map action handlers.
     *
     * @param dsProperties DatasetProperties
     * @param dataobjects DataObject array
     * @param layerProps LayerDisplayProperties
     * @param agent ViewerAgent
     *
     * @return ViewerWindow
     */
    public static ViewerWindow createViewerWindow(DatasetProperties dsProperties, DataObject[] dataobjects,
            LayerDisplayProperties layerProps, ViewerAgent agent, WindowModel winModel) {
        ViewerWindow viewerWindow = new ViewerWindow(dsProperties, dataobjects, layerProps, agent, winModel);
        viewerWindow.addGeoPlotEventListener(viewerWindow);
        return viewerWindow;
    }

    public static ViewerWindow createViewerWindow(String windowTitle, WindowModel initialWindowModel, ViewerAgent agent) {
        ViewerWindow viewerWindow = new ViewerWindow(windowTitle, initialWindowModel, agent);
        viewerWindow.addGeoPlotEventListener(viewerWindow);
        return viewerWindow;
    }

    /**
     * Creates a ViewerWindow with default WindowProperties based on the dsProperties parameter.
     * Used by the QiViewer GUI to construct a new ViewerWindow.
     *
     * @param dsProperties DatasetProperties
     * @param dataobjects DataObject array
     * @param layerProps LayerDisplayProperties
     * @param agent ViewerAgent
     *
     * @return ViewerWindow
     */
    public static ViewerWindow createViewerWindow(DatasetProperties dsProperties, DataObject[] dataobjects,
            LayerDisplayProperties layerProps, ViewerAgent agent) {
        WindowModel winModel = createDefaultWindowModel(dsProperties);
        ViewerWindow viewerWindow =
                new ViewerWindow(dsProperties, dataobjects, layerProps, agent, winModel);
        viewerWindow.addGeoPlotEventListener(viewerWindow);
        return viewerWindow;
    }

    private static WindowModel createDefaultWindowModel(DatasetProperties dsProperties) {
        WindowModel winModel = new WindowModel("qiViewer", dsProperties.getMetadata().getGeoDataOrder());
        GeoFileMetadata metadata = dsProperties.getMetadata();
        //if this is a map, initialize the caption labels
        if (metadata.getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER) {
            MapAnnotationProperties annoProps = (MapAnnotationProperties) winModel.getWindowProperties().getAnnotationProperties();
            GeoFileType fileType = metadata.getGeoFileType();
            String[] keys = metadata.getKeys().toArray(new String[0]);
            switch (fileType) {
                case EVENT_GEOFILE:
                    annoProps.setTitleLocation(LABEL_LOCATION.TOP);
                    annoProps.setTitle(((HorizonProperties) dsProperties).getSelectedHorizon());
                    break;
                case MODEL_GEOFILE:
                    annoProps.setTitleLocation(LABEL_LOCATION.TOP);
                    annoProps.setTitle(((ModelProperties) dsProperties).getSelectedProperties());
                    break;
                case SEISMIC_GEOFILE:
                    annoProps.setTitleLocation(LABEL_LOCATION.TOP);
                    AbstractKeyRange keyrange = dsProperties.getKeyChosenRange(keys[0]);
                    if (keyrange.isDiscrete()) {
                        int traclInt = new BigDecimal(((DiscreteKeyRange) keyrange).getMin()).intValue();
                        annoProps.setTitle("Time/Depth: " + traclInt);
                    }
                    break;
                default:
                //do nothing
            }
            //keys.length must be == 3 or this is not a valid map file
            annoProps.setLabel(LABEL_LOCATION.TOP, keys[1]);
            annoProps.setLabel(LABEL_LOCATION.LEFT, keys[2]);
        }
        return winModel;
    }

    /**
     * Deserializes a ViewerWindow from qiViewer or bhpViewer state xml.
     *
     * @param format XML_FORMAT
     * @param node Node
     * @param agent ViewerAgent
     * @param windowSyncGroup the default WindowSyncGroup for the ViewerAgent
     * @param progressDialog ProgressDialog with isCancellable() == false
     *
     * @return ViewerWindow
     */
    public static ViewerWindow restoreFromXml(XML_FORMAT format, Node node,
            ViewerAgent agent, WindowSyncGroup windowSyncGroup,
            ProgressDialog progressDialog) {
        if (progressDialog.isCancelable()) {
            throw new IllegalArgumentException("ProgressDialog must not be cancellable for use by restoreFromXml()");
        }
        switch (format) {
            case BHPVIEWER:
                return BhpWindowImporter.restoreFromBhpViewerStateXml(agent, node, windowSyncGroup,
                        new GeoIOAdapter(agent.getMessagingManager()),
                        progressDialog);
            case QIVIEWER:
                String orientation = ElementAttributeReader.getString(node, "orientation");

                WindowModel windowModel = new WindowModel("qiViewer", GeoDataOrder.valueOf(orientation));
                return new ViewerWindow(node, agent, windowSyncGroup,
                        progressDialog, windowModel);
            default:
                throw new IllegalArgumentException("Uknown XML state format: " + format);
        }
    }
}