/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.shared.BasicLayerProperties;
import com.bhpb.geographics.model.layer.shared.DisplayAttributes;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import org.w3c.dom.Node;

public class BhpMapLayerImporter {

    static void initMapDisplayParameters(Node node, boolean isModel,
            MapDisplayParameters ldParams) {
        BhpLayerImporter.initColorMap(node, ldParams);
    }

    static LayerDisplayProperties initMapLayer(Node mapLayerNode, Node parameterNode, double hScale,
            double vScale, DatasetProperties dsProperties, GeoIOAdapter geoIOadapter) {
        int mapDataType = ElementAttributeReader.getInt(mapLayerNode, "dataType");

        boolean isSeismic = (mapDataType == BhpImporterConstants.BHP_DATA_TYPE_SEISMIC) ? true : false;

        int mapSynFlag = ElementAttributeReader.getInt(mapLayerNode, "mapSynFlag");
        boolean modelLayerSync = ElementAttributeReader.getBoolean(mapLayerNode, "modelLayerSync");
        boolean lockAspectRatio = ElementAttributeReader.getBoolean(mapLayerNode, "lockAspectRatio");
        boolean transposeImage = ElementAttributeReader.getBoolean(mapLayerNode, "transposeImage");
        boolean reverseh = ElementAttributeReader.getBoolean(mapLayerNode, "reversedHDirection");

        parseMapProperties(parameterNode, isSeismic, dsProperties, geoIOadapter);

        LayerDisplayProperties ldProperties = LayerDisplayProperties.createDefaultLayerProperties(dsProperties);

        MapDisplayParameters ldParams = (MapDisplayParameters) ldProperties.getLayerDisplayParameters();

        BasicLayerProperties basicProps = ldParams.getBasicLayerProperties();
        basicProps.setHorizontalScale(hScale);
        basicProps.setVerticalScale(vScale);

        DisplayAttributes displayAttribs = ldParams.getDisplayAttributes();
        displayAttribs.setCdpReversed(reverseh);
        displayAttribs.setTransposed(transposeImage);

        initMapLayerSyncFlags(ldProperties.getLayerSyncProperties(), mapSynFlag);

        return ldProperties;
    }

    private static void initMapLayerSyncFlags(LayerSyncProperties syncProps, int syncField) {
        syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.TRANSPOSE, (syncField & 0x80) != 0);
        syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.CDP_AXIS_DIRECTION, (syncField & 0x40) != 0);
        syncProps.setEnabled(LayerSyncProperties.SYNC_FLAG.EP_AXIS_DIRECTION, (syncField & 0x20) != 0);
        //0x10 unused

        //0x08 unused
        //0x04 unused
        //0x02 unused
        //0x01 unused
    }

    /**
     * Updates the horizonProperties with a table of values read from the content
     * of the paramterNode and calls geoIOadapter.readSummary(horizonProperties).
     *
     * @param parameterNode the <Parameter> node of a BhpSeismicLayer's BhpLayer
     * @param horizonProperties
     * @param geoIOadapter
     */
    private static void parseMapProperties(Node parameterNode,
            boolean isSeismic, DatasetProperties dsProperties, GeoIOAdapter geoIOadapter) {
        if (parameterNode == null || parameterNode.getNodeType() != Node.ELEMENT_NODE) {
            throw new IllegalArgumentException("ParameterNode is null or not an Element");
        }

        BhpLayerImporter.initPropertiesFromNode(dsProperties, parameterNode, isSeismic);

        geoIOadapter.readSummary(dsProperties);
    }
}