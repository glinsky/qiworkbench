/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.MapDisplayParameters;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geographics.model.layer.shared.ColorMapModel;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.util.ElementAttributeReader;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Parses BhpViewer state XML related to ColorMaps.
 */
public class BhpColorMapImporter {

    /**
     * Parses the BhpViewer XML state Node and returns a GeoGraphics ColorMap.
     * The foreground, background, positive, negative and highlight color indices
     * are discarded as the ColorMap class is not intended for indexed color rendering.
     *
     * @param node the "Colormap" node of BhpViewer state XML.
     *
     * @return ColorMap deserialized from the XML node
     */
    static ColorMap getColormap(Node node) {
        Color backgroundColor = ElementAttributeReader.getColor(node, "backgroundColor");
        Color foregroundColor = ElementAttributeReader.getColor(node, "foregroundColor");
        Color positiveColor = ElementAttributeReader.getColor(node, "positiveColor");
        Color negativeColor = ElementAttributeReader.getColor(node, "negativeColor");
        Color hilightColor = ElementAttributeReader.getColor(node, "hilightColor");

        NodeList children = node.getChildNodes();

        List<Color> colors = new ArrayList<Color>();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                Color clr = ElementAttributeReader.getColor(child, "color");
                colors.add(clr);
            }
        }

        Map<String, List<Color>> colorNameListMap = new HashMap<String, List<Color>>();

        List fgList = new ArrayList<Color>();
        fgList.add(foregroundColor);

        List bgList = new ArrayList<Color>();
        bgList.add(backgroundColor);

        List posList = new ArrayList<Color>();
        posList.add(positiveColor);

        List negList = new ArrayList<Color>();
        negList.add(negativeColor);

        List hilightList = new ArrayList<Color>();
        hilightList.add(hilightColor);

        colorNameListMap.put(ColorMap.FOREGROUND, fgList);
        colorNameListMap.put(ColorMap.BACKGROUND, bgList);
        colorNameListMap.put(ColorMap.POSFILL, posList);
        colorNameListMap.put(ColorMap.NEGFILL, negList);
        colorNameListMap.put(ColorMap.RAMP, colors);

        return new ColorMap(colorNameListMap);
    }

    /**
     * Returns the ColorMapModel associated with given LayerDisplayParameters.
     * This method is needed because the ColorMapModel is a direct field of
     * the cross-section LayerDisplayParameters classes, but for the Map classes,
     * it is a sub-field of the ColorAttributes field.
     *
     * @param ldParams LayerDisplayParameters for which the ColorMapModel will be returned
     *
     * @return ColorMapModel associated with ldParams
     */
    static ColorMapModel getColorMapModel(LayerDisplayParameters ldParams) {
        if (ldParams instanceof SeismicXsecDisplayParameters) {
            return ((SeismicXsecDisplayParameters) ldParams).getColorMapModel();
        } else if (ldParams instanceof ModelXsecDisplayParameters) {
            return ((ModelXsecDisplayParameters) ldParams).getColorMapModel();
        } else if (ldParams instanceof HorizonXsecDisplayParameters) {
            return null; // Horizon xsec layers have no color maps
        } else if (ldParams instanceof MapDisplayParameters) {
            return ((MapDisplayParameters) ldParams).getColorAttributes().getColorMapModel();
        } else {
            throw new RuntimeException("Unknown type of LayerDisplayParameters: " + ldParams.getClass().getName());
        }
    }
}