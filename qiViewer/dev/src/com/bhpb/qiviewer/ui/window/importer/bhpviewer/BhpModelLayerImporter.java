/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.ModelXsecDisplayParameters;
import com.bhpb.geographics.model.layer.shared.ModelVerticalRange;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import org.w3c.dom.Node;

public class BhpModelLayerImporter {

    static void initModelXsecDisplayParameters(Node node,
            ModelXsecDisplayParameters ldParams) {
        BhpLayerImporter.initColorMap(node, ldParams);
        BhpLayerImporter.initCullingParams(node, ldParams.getCulling());
        initModelVerticalRange(node, ldParams.getModelVerticalRange());
    }

    static LayerDisplayProperties initModelLayer(Node parameterNode, double hScale,
            double vScale, ModelProperties modelProperties, GeoIOAdapter geoIOadapter,
            String property) {
        parseModelProperties(parameterNode, modelProperties, geoIOadapter,
                property);

        LayerDisplayProperties ldProperties = LayerDisplayProperties.createDefaultLayerProperties(modelProperties);

        ModelXsecDisplayParameters ldParams = (ModelXsecDisplayParameters) ldProperties.getLayerDisplayParameters();
        ldParams.getBasicLayerProperties().setHorizontalScale(hScale);
        ldParams.getBasicLayerProperties().setVerticalScale(vScale);

        return ldProperties;
    }

    private static void initModelVerticalRange(Node node,
            ModelVerticalRange verticalRange) {
        boolean structuralInterp = ElementAttributeReader.getBoolean(node, "structuralInterp",
                true);

        float startDepth = ElementAttributeReader.getFloat(node, "startDepth");
        float endDepth = ElementAttributeReader.getFloat(node, "endDepth");

        // If start and end depth in XML are both zero, use values from reader summary.
        verticalRange.setStartTime(startDepth);
        verticalRange.setEndTime(endDepth);
        verticalRange.setStructuralInterpolationEnabled(structuralInterp);
    }

    /**
     * Updates the horizonProperties with a table of values read from the content
     * of the parameterNode, sets the selectedProperty and calls
     * geoIOadapter.readSummary(horizonProperties).
     *
     * @param parameterNode the <Parameter> node of a BhpSeismicLayer's BhpLayer
     * @param horizonProperties
     * @param geoIOadapter
     */
    private static void parseModelProperties(Node parameterNode,
            ModelProperties modelProperties, GeoIOAdapter geoIOadapter,
            String property) {
        if (parameterNode == null || parameterNode.getNodeType() != Node.ELEMENT_NODE) {
            throw new IllegalArgumentException("ParameterNode is null or not an Element");
        }

        BhpLayerImporter.initPropertiesFromNode(modelProperties, parameterNode, false);
        modelProperties.setSelectedProperty(property);

        geoIOadapter.readSummary(modelProperties);
    }
}