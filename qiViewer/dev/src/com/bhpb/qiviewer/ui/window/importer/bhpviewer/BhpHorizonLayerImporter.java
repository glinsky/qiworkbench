/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import java.awt.Color;
import org.w3c.dom.Node;

public class BhpHorizonLayerImporter {

    static void initHorizonXsecDisplayParameters(Node node,
            HorizonXsecDisplayParameters ldParams) {
        BhpLayerImporter.initColorMap(node, ldParams);
    }

    static LayerDisplayProperties initHorizonLayer(Node horizonLayerNode, Node parameterNode,
            HorizonProperties horizonProperties,
            GeoIOAdapter geoIOadapter,
            String property) {
        int refSeismicLayerId = -1;
        refSeismicLayerId = ElementAttributeReader.getInt(horizonLayerNode, "refSeismicLayerId", refSeismicLayerId);
        int eventLS = ElementAttributeReader.getInt(horizonLayerNode, "eventLineStyle");
        float eventLW = ElementAttributeReader.getFloat(horizonLayerNode, "eventLineWidth");
        Color eventLC = ElementAttributeReader.getColor(horizonLayerNode, "eventLineColor");

        parseHorizonProperties(parameterNode, horizonProperties, geoIOadapter, property);

        LayerDisplayProperties ldProperties = LayerDisplayProperties.createDefaultLayerProperties(horizonProperties);

        HorizonXsecDisplayParameters ldParams = (HorizonXsecDisplayParameters) ldProperties.getLayerDisplayParameters();

        HorizonXsecDisplayParameters.LINE_STYLE lineStyle;
        switch (eventLS) {
            default:
                lineStyle = HorizonXsecDisplayParameters.LINE_STYLE.SOLID;
        }

        ldParams.setLineStyle(lineStyle);
        ldParams.setLineWidth((int) eventLW);
        ldParams.setLineColor(eventLC);

        return ldProperties;
    }

    /**
     * Updates the horizonProperties with a table of values read from the content
     * of the paramterNode and calls geoIOadapter.readSummary(horizonProperties).
     *
     * @param parameterNode the <Parameter> node of a BhpSeismicLayer's BhpLayer
     * @param horizonProperties
     * @param geoIOadapter
     */
    private static void parseHorizonProperties(Node parameterNode,
            HorizonProperties horizonProperties, GeoIOAdapter geoIOadapter,
            String property) {
        if (parameterNode == null || parameterNode.getNodeType() != Node.ELEMENT_NODE) {
            throw new IllegalArgumentException("ParameterNode is null or not an Element");
        }

        BhpLayerImporter.initPropertiesFromNode(horizonProperties, parameterNode, false);
        horizonProperties.setSelectedHorizon(property);

        geoIOadapter.readSummary(horizonProperties);
    }
}