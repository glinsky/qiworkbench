/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.window.WindowModel;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.model.window.WindowSyncGroup;
import com.bhpb.geographics.model.window.WindowSyncProperties;
import com.bhpb.geographics.model.window.WindowSyncProperties.SYNC_FLAG;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiviewer.ui.window.ViewerWindowFactory;
import com.bhpb.qiviewer.util.ProgressDialog;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Parses attributes and children of the BhpWindow element of the bhpViewer's
 * state XML.
 */
public class BhpWindowImporter {

    private static final Logger logger =
            Logger.getLogger(BhpWindowImporter.class.getName());
    private static final double DEFAULT_XSEC_HSCALE = 2.0;
    private static final double DEFAULT_XSEC_VSCALE = 1.0;

    private static Layer createLayer(DatasetProperties dsProperties,
            LayerDisplayProperties ldProps, ViewerAgent agent,
            GeoIOAdapter geoIOadapter,
            WindowModel windowModel) {
        boolean allowArbTrav = dsProperties.getMetadata().getGeoDataOrder() == GeoDataOrder.MAP_VIEW_ORDER;

        if (dsProperties.isHorizon()) {
            BhpSuHorizonDataObject[] dataObjects =
                    geoIOadapter.getHorizons((HorizonProperties) dsProperties);

            return new HorizonLayer(
                    dsProperties,
                    ldProps.getLayerDisplayParameters(),
                    ldProps.getLocalSubsetProperties(),
                    ldProps.getLayerSyncProperties(),
                    dataObjects,
                    geoIOadapter,
                    agent.getViewerDesktop().getPickingSettings(),
                    agent.getViewerDesktop().getHorizonHighlightSettings(),
                    allowArbTrav,
                    agent.getDefaultLayerSyncGroup(),
                    windowModel);
        } else if (dsProperties.isModel()) {
            DataObject[] dataObjects =
                    geoIOadapter.getTraces(dsProperties);
            return new ModelLayer(dsProperties,
                    ldProps.getLayerDisplayParameters(),
                    ldProps.getLocalSubsetProperties(),
                    ldProps.getLayerSyncProperties(),
                    dataObjects,
                    geoIOadapter,
                    allowArbTrav,
                    agent.getDefaultLayerSyncGroup(),
                    windowModel);
        } else if (dsProperties.isSeismic()) {
            DataObject[] dataObjects =
                    geoIOadapter.getTraces(dsProperties);
            return new SeismicLayer(dsProperties,
                    ldProps.getLayerDisplayParameters(),
                    ldProps.getLocalSubsetProperties(),
                    ldProps.getLayerSyncProperties(),
                    dataObjects,
                    geoIOadapter,
                    allowArbTrav,
                    agent.getDefaultLayerSyncGroup(),
                    windowModel);
        } else {
            throw new RuntimeException("Unknown type of datasetProperties: not seismic, model or horizon");
        }
    }

    /**
     * Count the number of layer nodes in a <BhpWindow> node
     * @param children List of <BhpWindow>'s children nodes
     * @return The number of layer nodes.
     */
    private static int getLayerCount(NodeList children) {
        int nl = 0;
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (BhpLayerImporter.isDataLayer(child.getNodeName())) {
                    nl++;
                }
            }
        }
        return nl;
    }

    private static void initPlotSyncFlags(WindowSyncProperties syncProps, int plotSyncField, boolean plotTalk,
            boolean syncHscroll, boolean syncVscroll) {
        syncProps.setEnabled(SYNC_FLAG.BROADCAST, plotTalk);

        syncProps.setEnabled(SYNC_FLAG.CURSOR_POS_LISTENING, (plotSyncField & 0x0000002) != 0);

        syncProps.setEnabled(SYNC_FLAG.H_SCALE_LISTENING, (plotSyncField & 0x0000100) != 0);
        syncProps.setEnabled(SYNC_FLAG.V_SCALE_LISTENING, (plotSyncField & 0x0000200) != 0);

        syncProps.setEnabled(SYNC_FLAG.SCROLL_POS_LISTENING, (plotSyncField & 0x1000000) != 0);
        syncProps.setEnabled(SYNC_FLAG.H_SCROLL_POS_SYNC, syncHscroll);
        syncProps.setEnabled(SYNC_FLAG.V_SCROLL_POS_SYNC, syncVscroll);

        if ((plotSyncField & 0x0001000) != 0) {
            logger.warning("SELECTOR_FLAG is set but is not used by qiViewer.");
        }

        if ((plotSyncField & 0x0000400) != 0) {
            logger.warning("POLARITY_FLAG is set but is not used by qiViewer.");
        }
    }

    /**
     * Create a qiViewer's internal window restored with the state of a
     * bhpViewer's internal window.  Importing a BhpViewer state xml file
     * is _not_ a cancellable process, due to the potential to leave 
     * the QiViewer in an invalid state after only a portion of the XML
     * file has been parsed.
     *
     * @param agent qiViewer agent
     * @param node Node (BhpWindow) of the state for a bhpViewer internal window.
     * @param progressDlg ProgressDialog with isCancellable() == false
     *
     * @return ViewerWindow deserialized from bhpViewer state.
     */
    public static ViewerWindow restoreFromBhpViewerStateXml(ViewerAgent agent,
            Node node,
            WindowSyncGroup windowSyncGroup,
            GeoIOAdapter geoIOadapter,
            ProgressDialog progressDlg) {
        if (progressDlg.isCancelable()) {
            throw new IllegalArgumentException("ProgressDialog must not be cancellable for use by restoreFromBhpViewerStateXml()");
        }
        double ploths = DEFAULT_XSEC_HSCALE;
        double plotvs = DEFAULT_XSEC_VSCALE;

        plotvs = ElementAttributeReader.getDouble(node, "plotVScale", plotvs);
        ploths = ElementAttributeReader.getDouble(node, "plotHScale", ploths);

        int x = ElementAttributeReader.getInt(node, "positionX");
        int y = ElementAttributeReader.getInt(node, "positionY");
        int width = ElementAttributeReader.getInt(node, "width");
        int height = ElementAttributeReader.getInt(node, "height");

        //START of code from BhpWindow::initWithXML()
        int axisAssociateLayer = 0;
        int layerCounter = 0;
        int viewPositionX = -999;
        int viewPositionY = -999;
        int plotSynFlag = 0;
        int dividerLocation = -999;     //FIXME globalise default values

        String title = ElementAttributeReader.getString(node, "title");
        if (title == null) {
            title = "Missing Window Title";
        }

        progressDlg.setWinType(title);
        progressDlg.incrWinCnt();

        axisAssociateLayer = ElementAttributeReader.getInt(node, "axisAssociate", axisAssociateLayer);
        layerCounter = ElementAttributeReader.getInt(node, "layerCounter", layerCounter);
        viewPositionX = ElementAttributeReader.getInt(node, "viewPositionX", viewPositionX);
        viewPositionY = ElementAttributeReader.getInt(node, "viewPositionY", viewPositionY);
        plotSynFlag = ElementAttributeReader.getInt(node, "plotSynFlag", plotSynFlag);
        dividerLocation = ElementAttributeReader.getInt(node, "dividerLocation", dividerLocation);

        //boolean mouseTracking = ElementAttributeReader.getBoolean(node, "mouseTracking");
        boolean plotTalkEnabled = ElementAttributeReader.getBoolean(node, "enablePlotTalk");
        //boolean eventGraphPlotVisible = ElementAttributeReader.getBoolean(node, "showHorizonGraph");
        boolean lockAspectRatio = ElementAttributeReader.getBoolean(node, "plotLockRatio");
        boolean syncHScroll = ElementAttributeReader.getBoolean(node, "syncHScroll");
        boolean syncVScroll = ElementAttributeReader.getBoolean(node, "syncVScroll");
        //windowType is used to create the correct type of windowModel if it contains no Layers from which to infer data order
        int windowType = ElementAttributeReader.getInt(node, "windowType");

        NodeList children = node.getChildNodes();

        int nl = getLayerCount(children);
        logger.info("Restoring " + nl + " layers...");

        //Process children nodes of <BhpWindow>
        List<LayerProperties> layerPropsList = new ArrayList<LayerProperties>();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                String nodeName = child.getNodeName();
                if (nodeName.equals("Annotation") || nodeName.equals("HorizonGraph")) {
                    BhpLayerImporter.restoreLayer(child, agent);
                } else {
                    layerPropsList.add(BhpLayerImporter.loadLayerProperties(child, agent, geoIOadapter));
                }
            }
        }

        //Now, convert the List of LayerProperties to a List of Layers
        List<Layer> layers = new ArrayList<Layer>();

        int zOrder = 0;

        WindowModel winModel = null;
        for (LayerProperties layerProps : layerPropsList) {
            //use the first imported layer to initialize the associated WindowProperties
            DatasetProperties dsProps = layerProps.getDatasetProperties();
            if (winModel == null) {
                winModel = new WindowModel("qiViewer", dsProps.getMetadata().getGeoDataOrder());
                WindowScaleProperties scaleProps = winModel.getWindowProperties().getScaleProperties();
                
                scaleProps.setHorizontalPixelIncr(ploths);
                scaleProps.setVerticalPixelIncr(plotvs);
                scaleProps.setAspectRatioLocked(lockAspectRatio);
                WindowSyncProperties winSyncProps = winModel.getWindowProperties().getSyncProperties();
                initPlotSyncFlags(winSyncProps, plotSynFlag, plotTalkEnabled, syncHScroll, syncVScroll);
            }

            layers.add(createLayer(dsProps, layerProps.getLayerDisplayProperties(),
                    agent, geoIOadapter, winModel));
            zOrder++;
        }

        ViewerWindow viewerWindow = null;

        for (Layer layer : layers) {
            //Abort loading layers if progress dialog was cancelled
            if (viewerWindow == null) {
                viewerWindow = ViewerWindowFactory.createViewerWindow(layer.getDatasetProperties(),
                        layer.getDataObjects(), layer.getDisplayProperties(), agent, winModel);
            } else {
                viewerWindow.addLayer(layer);
            }
            progressDlg.incrLayerCnt();
        }

        //sanity check - if the viewer window is null, either an error has occurred
        //or the state file contains an empty window
        if (viewerWindow == null) {
            if (layerPropsList.size() == 0 && layers.size() == 0) {
                switch(windowType) {
                    case 1 :
                        winModel = new WindowModel("qiViewer", GeoDataOrder.CROSS_SECTION_ORDER);
                        break;
                    case 2 :
                        winModel = new WindowModel("qiViewer", GeoDataOrder.MAP_VIEW_ORDER);
                        break;
                    default :
                        logger.warning("Unable to create WindowModel for unknown bhpViewer windowType: " + windowType);
                }
                if (winModel != null) {
                    viewerWindow = ViewerWindowFactory.createViewerWindow(title, winModel, agent);
                }
            } else {
                logger.warning("Error: restored ViewerWindow is null but the state file contains "
                        + layerPropsList.size() + " layers.");
            }
        }

        if (viewerWindow != null) {
            viewerWindow.updateSize(width, height);
            viewerWindow.updateLocation(x, y);
            if (dividerLocation > 0) {
                viewerWindow.setDividerLocation(dividerLocation);
            }

            windowSyncGroup.addListener(viewerWindow);

            viewerWindow.setViewPosition(new Point2D.Double(viewPositionX, viewPositionY));
        }

        return viewerWindow;
    }
}