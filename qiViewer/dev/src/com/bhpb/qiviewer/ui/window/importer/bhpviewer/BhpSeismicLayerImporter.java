/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.window.importer.bhpviewer;

import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import com.bhpb.geoio.filesystems.FileSystemConstants.MissingTraceOption;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.geoio.util.GeoIOAdapter;
import org.w3c.dom.Node;

public class BhpSeismicLayerImporter {

    static void initSeismicXsecDisplayParameters(Node node,
            SeismicXsecDisplayParameters ldParams) {
        BhpLayerImporter.initColorMap(node, ldParams);
        BhpLayerImporter.initCullingParams(node, ldParams.getCulling());
        BhpLayerImporter.initRasterTypes(node, ldParams.getRasterizationTypeModel());
    }

    /**
     * Initializes the seismicProperties with the content of the parameterNode,
     * reads the dataset summary using the geoIOadapter and then sets the display
     * parameters' vertical and horizontal scale before returning a newly created
     * LayerDisplayProperties.
     *
     * @param parameterNode
     * @param hScale
     * @param vScale
     * @param seismicProperties
     * @param geoIOadapter
     *
     * @return
     */
     static LayerDisplayProperties initSeismicLayer(Node parameterNode, double hScale,
            double vScale, SeismicProperties seismicProperties, GeoIOAdapter geoIOadapter) {
        parseSeismicProperties(parameterNode, seismicProperties, geoIOadapter);

        LayerDisplayProperties ldProperties = LayerDisplayProperties.createDefaultLayerProperties(seismicProperties);

        SeismicXsecDisplayParameters ldParams = (SeismicXsecDisplayParameters) ldProperties.getLayerDisplayParameters();
        ldParams.getBasicLayerProperties().setHorizontalScale(hScale);
        ldParams.getBasicLayerProperties().setVerticalScale(vScale);

        return ldProperties;
    }

    /**
     * Updates the horizonProperties with a table of values read from the content
     * of the paramterNode and calls geoIOadapter.readSummary(horizonProperties).
     *
     * @param parameterNode the <Parameter> node of a BhpSeismicLayer's BhpLayer
     * @param horizonProperties
     * @param geoIOadapter
     */
    private static void parseSeismicProperties(Node parameterNode,
            SeismicProperties seismicProperties, GeoIOAdapter geoIOadapter) {
        if (parameterNode == null || parameterNode.getNodeType() != Node.ELEMENT_NODE) {
            throw new IllegalArgumentException("ParameterNode is null or not an Element");
        }

        BhpLayerImporter.initPropertiesFromNode(seismicProperties, parameterNode, true);

        //missingData selection - 0(ignore) or 1(fill)
        String md = ElementAttributeReader.getString(parameterNode, "missingData");
        if ("0".equals(md)) {
            seismicProperties.setMissingTraceOption(MissingTraceOption.IGNORE_OPTION);
        } else if ("1".equals(md)) {
            seismicProperties.setMissingTraceOption(MissingTraceOption.FILL_OPTION);
        } else {
            throw new RuntimeException("Unable to parse invalid missingData value: " + md);
        }

        geoIOadapter.readSummary(seismicProperties);
    }
}