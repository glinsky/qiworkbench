/*
###########################################################################
# bhpViewer - a 2D seismic viewer
# This program module Copyright (C) 2008  BHP Billiton Petroleum
#
# This program module is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program module is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.layer;

import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.ui.desktop.ViewerDesktop;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import javax.swing.JPanel;

/**
 * A dialog for creating empty horizons in a BHP-SU event dataset..
 * @author  Gil Hansen
 * @version 1.0
 */
public class CreateHorizonsDialog extends javax.swing.JDialog {

    private static Logger logger = Logger.getLogger(CreateHorizonsDialog.class.getName());
    private static final long serialVersionUID = 6943584775062631512L;
    public static final String SELECT_SEISMIC_CUBE = "Select Seismic_Cube";
    private static final int NUMBER_HORIZONS_DEFAULT = 5;
    private static final String NUM_HORIZONS_DEFAULT = "5";
    private static final int MAX_NUMBER_HORIZONS = 30;
    private static final String HORIZON_PREFIX_DEFAULT = "null";
    private static final String EVENT_SUFFIX_DEFAULT = "_events";
    private static final String DATASET_SUFFIX = ".dat";
    ViewerDesktop viewerDesktop;
    ViewerAgent viewerAgent = null;
    IMessagingManager messagingMgr = null;
    /** The names of the horizons. */
    private List<javax.swing.JTextField> eventNameList;
    /** The number of horizons. */
    private int numHorizons = NUMBER_HORIZONS_DEFAULT;
    private int prevNumHorizons = numHorizons;
    /** Server file separator */
    String filesep = "/";
    /** Selected Seismic File */
    String seismicFile = "";
    /** Selected Event File */
    String eventFile = "";

    /**
     * 
     * @param viewerAgent
     * @param viewerDesktop
     * @param modal
     */
    public CreateHorizonsDialog(ViewerAgent viewerAgent, ViewerDesktop viewerDesktop, boolean modal) {
        super(JOptionPane.getFrameForComponent(viewerDesktop), modal);
        this.viewerAgent = viewerAgent;
        this.viewerDesktop = viewerDesktop;
        this.messagingMgr = viewerAgent.getMessagingManager();
        this.filesep = messagingMgr.getServerOSFileSeparator();

        initComponents();

        this.setTitle("Create Horizon(s)");

        //set the default number of horizons
        numHorizonsTextField.setText(NUM_HORIZONS_DEFAULT);

        //set the default horizon prefix
        prefixTextField.setText(HORIZON_PREFIX_DEFAULT);

        //disable setting of the event file until seisic file is selected
        eventFileTextField.setEnabled(false);

        //capture the horizon text field list
        eventNameList = new ArrayList<javax.swing.JTextField>(MAX_NUMBER_HORIZONS);
        eventNameList.add(0, event1TextField);
        eventNameList.add(1, event2TextField);
        eventNameList.add(2, event3TextField);
        eventNameList.add(3, event4TextField);
        eventNameList.add(4, event5TextField);
        eventNameList.add(5, event6TextField);
        eventNameList.add(6, event7TextField);
        eventNameList.add(7, event8TextField);
        eventNameList.add(8, event9TextField);
        eventNameList.add(9, event10TextField);
        eventNameList.add(10, event11TextField);
        eventNameList.add(11, event12TextField);
        eventNameList.add(12, event13TextField);
        eventNameList.add(13, event14TextField);
        eventNameList.add(14, event15TextField);
        eventNameList.add(15, event16TextField);
        eventNameList.add(16, event17TextField);
        eventNameList.add(17, event18TextField);
        eventNameList.add(18, event19TextField);
        eventNameList.add(19, event20TextField);
        eventNameList.add(20, event21TextField);
        eventNameList.add(21, event22TextField);
        eventNameList.add(22, event23TextField);
        eventNameList.add(23, event24TextField);
        eventNameList.add(24, event25TextField);
        eventNameList.add(25, event26TextField);
        eventNameList.add(26, event27TextField);
        eventNameList.add(27, event28TextField);
        eventNameList.add(28, event29TextField);
        eventNameList.add(29, event30TextField);

        //set the horizon names
        setHorizonNames(numHorizons);
    }

    /**
     * Accessor for unit testing.
     * 
     * @return the JPanel which contains the file selection textfield and related JButtons
     */
    public JPanel getFileSelectorPanel() {
        return fileSelectorPanel;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {
        dialogScrollPane = new javax.swing.JScrollPane();
        dialogPanel = new javax.swing.JPanel();
        horizonSelectorPanel = new javax.swing.JPanel();
        col1Panel = new javax.swing.JPanel();
        event1TextField = new javax.swing.JTextField();
        event2TextField = new javax.swing.JTextField();
        event3TextField = new javax.swing.JTextField();
        event4TextField = new javax.swing.JTextField();
        event5TextField = new javax.swing.JTextField();
        event6TextField = new javax.swing.JTextField();
        event7TextField = new javax.swing.JTextField();
        event8TextField = new javax.swing.JTextField();
        event9TextField = new javax.swing.JTextField();
        event10TextField = new javax.swing.JTextField();
        col2Panel = new javax.swing.JPanel();
        event11TextField = new javax.swing.JTextField();
        event12TextField = new javax.swing.JTextField();
        event13TextField = new javax.swing.JTextField();
        event14TextField = new javax.swing.JTextField();
        event15TextField = new javax.swing.JTextField();
        event16TextField = new javax.swing.JTextField();
        event17TextField = new javax.swing.JTextField();
        event18TextField = new javax.swing.JTextField();
        event19TextField = new javax.swing.JTextField();
        event20TextField = new javax.swing.JTextField();
        col3Panel = new javax.swing.JPanel();
        event21TextField = new javax.swing.JTextField();
        event22TextField = new javax.swing.JTextField();
        event23TextField = new javax.swing.JTextField();
        event24TextField = new javax.swing.JTextField();
        event25TextField = new javax.swing.JTextField();
        event26TextField = new javax.swing.JTextField();
        event27TextField = new javax.swing.JTextField();
        event28TextField = new javax.swing.JTextField();
        event29TextField = new javax.swing.JTextField();
        event30TextField = new javax.swing.JTextField();
        fileSelectorPanel = new javax.swing.JPanel();
        filesPanel = new javax.swing.JPanel();
        seismicFileLabel = new javax.swing.JLabel();
        seismicFileTextField = new javax.swing.JTextField();
        eventFileLabel = new javax.swing.JLabel();
        eventFileTextField = new javax.swing.JTextField();
        seismicFileBrowseButton = new javax.swing.JButton();
        OkButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        numHorizonsTextField = new javax.swing.JTextField();
        numHorizonsLabel = new javax.swing.JLabel();
        prefixTextField = new javax.swing.JTextField();
        prefixLabel = new javax.swing.JLabel();
        horizonNamesLabel = new javax.swing.JLabel();

        setModal(true);
        horizonSelectorPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        event1TextField.setText("null1");

        event2TextField.setText("null2");

        event3TextField.setText("null3");

        event4TextField.setText("null4");

        event5TextField.setText("null5");

        event6TextField.setText("null6");

        event7TextField.setText("null7");

        event8TextField.setText("null8");

        event9TextField.setText("null9");

        event10TextField.setText("null10");

        org.jdesktop.layout.GroupLayout col1PanelLayout = new org.jdesktop.layout.GroupLayout(col1Panel);
        col1Panel.setLayout(col1PanelLayout);
        col1PanelLayout.setHorizontalGroup(
                col1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, col1PanelLayout.createSequentialGroup().addContainerGap().add(col1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING).add(event6TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event10TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event9TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event8TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(org.jdesktop.layout.GroupLayout.LEADING, event7TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event1TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event2TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event5TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event4TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(org.jdesktop.layout.GroupLayout.LEADING, event3TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)).addContainerGap()));
        col1PanelLayout.setVerticalGroup(
                col1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(col1PanelLayout.createSequentialGroup().addContainerGap().add(event1TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event2TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event3TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event4TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event5TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event6TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event7TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event8TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event9TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event10TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        event11TextField.setText("null11");

        event12TextField.setText("null12");

        event13TextField.setText("null13");

        event14TextField.setText("null14");

        event15TextField.setText("null15");

        event16TextField.setText("null16");

        event17TextField.setText("null17");

        event18TextField.setText("null18");

        event19TextField.setText("null19");

        event20TextField.setText("null20");

        org.jdesktop.layout.GroupLayout col2PanelLayout = new org.jdesktop.layout.GroupLayout(col2Panel);
        col2Panel.setLayout(col2PanelLayout);
        col2PanelLayout.setHorizontalGroup(
                col2PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(col2PanelLayout.createSequentialGroup().addContainerGap().add(col2PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(event12TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event13TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event14TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event15TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event16TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event17TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event18TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event19TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(event20TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE).add(org.jdesktop.layout.GroupLayout.TRAILING, event11TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)).addContainerGap()));
        col2PanelLayout.setVerticalGroup(
                col2PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, col2PanelLayout.createSequentialGroup().addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(event11TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event12TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event13TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event14TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event15TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event16TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event17TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event18TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event19TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event20TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)));

        event21TextField.setText("null21");

        event22TextField.setText("null22");

        event23TextField.setText("null23");

        event24TextField.setText("null24");

        event25TextField.setText("null25");

        event26TextField.setText("null26");

        event27TextField.setText("null27");

        event28TextField.setText("null28");

        event29TextField.setText("null29");

        event30TextField.setText("null30");

        org.jdesktop.layout.GroupLayout col3PanelLayout = new org.jdesktop.layout.GroupLayout(col3Panel);
        col3Panel.setLayout(col3PanelLayout);
        col3PanelLayout.setHorizontalGroup(
                col3PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(col3PanelLayout.createSequentialGroup().add(col3PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, event30TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event21TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event22TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event23TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event24TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event25TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event26TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event27TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event28TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE).add(event29TextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)).addContainerGap()));
        col3PanelLayout.setVerticalGroup(
                col3PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(col3PanelLayout.createSequentialGroup().addContainerGap().add(event21TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event22TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event23TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event24TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event25TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event26TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event27TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event28TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event29TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(event30TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        org.jdesktop.layout.GroupLayout horizonSelectorPanelLayout = new org.jdesktop.layout.GroupLayout(horizonSelectorPanel);
        horizonSelectorPanel.setLayout(horizonSelectorPanelLayout);
        horizonSelectorPanelLayout.setHorizontalGroup(
                horizonSelectorPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, horizonSelectorPanelLayout.createSequentialGroup().add(col1Panel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(col2Panel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(15, 15, 15).add(col3Panel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)));
        horizonSelectorPanelLayout.setVerticalGroup(
                horizonSelectorPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(horizonSelectorPanelLayout.createSequentialGroup().add(horizonSelectorPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false).add(col2Panel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(col3Panel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(col1Panel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)).add(33, 33, 33)));

        fileSelectorPanel.setPreferredSize(new java.awt.Dimension(0, 0));
        seismicFileLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
        seismicFileLabel.setText("Seismic File:");

        seismicFileTextField.addFocusListener(new java.awt.event.FocusListener() {

            public void focusLost(java.awt.event.FocusEvent evt) {
                seismicFileTextFieldActionPerformed(evt);
            }

            public void focusGained(java.awt.event.FocusEvent evt) {
            }
        });
        seismicFileTextField.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seismicFileTextFieldActionPerformed(evt);
            }
        });

        eventFileLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
        eventFileLabel.setText("Event File:");

        eventFileTextField.addFocusListener(new java.awt.event.FocusListener() {

            public void focusLost(java.awt.event.FocusEvent evt) {
                eventFileTextFieldActionPerformed(evt);
            }

            public void focusGained(java.awt.event.FocusEvent evt) {
            }
        });
        eventFileTextField.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eventFileTextFieldActionPerformed(evt);
            }
        });

        seismicFileBrowseButton.setText("Browse");
        seismicFileBrowseButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seismicFileBrowseButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout filesPanelLayout = new org.jdesktop.layout.GroupLayout(filesPanel);
        filesPanel.setLayout(filesPanelLayout);
        filesPanelLayout.setHorizontalGroup(
                filesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(filesPanelLayout.createSequentialGroup().add(filesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(seismicFileLabel).add(eventFileLabel)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(filesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false).add(eventFileTextField).add(seismicFileTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(seismicFileBrowseButton).addContainerGap()));
        filesPanelLayout.setVerticalGroup(
                filesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(filesPanelLayout.createSequentialGroup().add(14, 14, 14).add(filesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(seismicFileLabel).add(seismicFileTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(seismicFileBrowseButton)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(filesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(eventFileLabel).add(eventFileTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        OkButton.setText("OK");
        OkButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OkButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.setName("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout fileSelectorPanelLayout = new org.jdesktop.layout.GroupLayout(fileSelectorPanel);
        fileSelectorPanel.setLayout(fileSelectorPanelLayout);
        fileSelectorPanelLayout.setHorizontalGroup(
                fileSelectorPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, fileSelectorPanelLayout.createSequentialGroup().addContainerGap(405, Short.MAX_VALUE).add(cancelButton).add(16, 16, 16).add(OkButton)).add(filesPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE));
        fileSelectorPanelLayout.setVerticalGroup(
                fileSelectorPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, fileSelectorPanelLayout.createSequentialGroup().add(filesPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(fileSelectorPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(cancelButton).add(OkButton)).addContainerGap()));

        numHorizonsTextField.setText("20");
        numHorizonsTextField.addFocusListener(new java.awt.event.FocusListener() {

            public void focusLost(java.awt.event.FocusEvent evt) {
                numHorizonsTextFieldActionPerformed(evt);
            }

            public void focusGained(java.awt.event.FocusEvent evt) {
            }
        });
        numHorizonsTextField.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numHorizonsTextFieldActionPerformed(evt);
            }
        });

        numHorizonsLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
        numHorizonsLabel.setText("Number of Horizons");

        prefixTextField.setText(null);
        prefixTextField.addFocusListener(new java.awt.event.FocusListener() {

            public void focusLost(java.awt.event.FocusEvent evt) {
                prefixTextFieldActionPerformed(evt);
            }

            public void focusGained(java.awt.event.FocusEvent evt) {
            }
        });
        prefixTextField.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prefixTextFieldActionPerformed(evt);
            }
        });

        prefixLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
        prefixLabel.setText("Horizon Prefix");

        horizonNamesLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
        horizonNamesLabel.setText("Horizon Names:");

        org.jdesktop.layout.GroupLayout dialogPanelLayout = new org.jdesktop.layout.GroupLayout(dialogPanel);
        dialogPanel.setLayout(dialogPanelLayout);
        dialogPanelLayout.setHorizontalGroup(
                dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(dialogPanelLayout.createSequentialGroup().addContainerGap().add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(dialogPanelLayout.createSequentialGroup().add(horizonSelectorPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()).add(dialogPanelLayout.createSequentialGroup().add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(dialogPanelLayout.createSequentialGroup().add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false).add(prefixTextField).add(numHorizonsTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)).add(16, 16, 16).add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(prefixLabel).add(numHorizonsLabel))).add(horizonNamesLabel).add(fileSelectorPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 578, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).add(20, 20, 20)))));
        dialogPanelLayout.setVerticalGroup(
                dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(dialogPanelLayout.createSequentialGroup().add(20, 20, 20).add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(numHorizonsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(numHorizonsLabel)).add(14, 14, 14).add(dialogPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(prefixTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(prefixLabel)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(horizonNamesLabel).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(horizonSelectorPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(fileSelectorPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE).add(33, 33, 33)));
        dialogScrollPane.setViewportView(dialogPanel);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(dialogScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 622, Short.MAX_VALUE));
        layout.setVerticalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(dialogScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 531, Short.MAX_VALUE));
        pack();
    }// </editor-fold>                        

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void eventFileTextFieldActionPerformed(java.awt.AWTEvent evt) {//GEN-FIRST:event_eventFileTextFieldActionPerformed
        String valMsg = validateEventFilename();
        if (!valMsg.equals("")) {
            JOptionPane.showMessageDialog(this, valMsg, "Validation Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_eventFileTextFieldActionPerformed

    private void seismicFileBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seismicFileBrowseButtonActionPerformed
        //Get the most recent project descriptor
        QiProjectDescriptor projDesc = viewerAgent.getQiProjectDescriptor();

        ArrayList list = new ArrayList();
        //1st element the parent GUI object
        list.add(this.viewerDesktop);
        //2nd element is the dialog title
        list.add("Select BHP-SU Seismic Cube");
        // 3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if a already remembered directory
        //should be used instead
        ArrayList lst = new ArrayList();
        lst.add(QiProjectDescUtils.getDatasetsPath(projDesc));
        lst.add("no");
        list.add(lst);
        //4th element is the file filter
        list.add(getFileFilter(new String[]{".dat", ".DAT"}, "Data File (*.dat)"));
        //5th element is the navigation flag
        list.add(true);
        //6th element is the producer component descriptor
        list.add(messagingMgr.getMyComponentDesc());
        //7th element is the type of file chooser either Open or Save
        list.add(QIWConstants.FILE_CHOOSER_TYPE_OPEN);
        //8th element is the message command
        list.add(SELECT_SEISMIC_CUBE);
        //9th element is the default prefix for the file name if user fails to prefix it. If no prefix is required then use empty String
        list.add("");
        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        list.add("dat");
        //11th element is the target Tomcat url where the file chooser chooses
        list.add(messagingMgr.getTomcatURL());

        viewerAgent.invokeFileChooser(list, CreateHorizonsDialog.class.getName(), this);

    }//GEN-LAST:event_seismicFileBrowseButtonActionPerformed

    private void seismicFileTextFieldActionPerformed(java.awt.AWTEvent evt) {//GEN-FIRST:event_seismicFileTextFieldActionPerformed
        String valMsg = validateSeismicFilename();
        if (!valMsg.equals("")) {
            JOptionPane.showMessageDialog(this, valMsg, "Validation Error", JOptionPane.ERROR_MESSAGE);
            eventFileTextField.setText("");
            eventFileTextField.setEnabled(false);
        } else {
            String name = seismicFileTextField.getText();
            eventFileTextField.setEnabled(true);
            if (!name.equals(seismicFile)) {
                seismicFile = name;
                eventFile = name + EVENT_SUFFIX_DEFAULT;
                eventFileTextField.setText(eventFile);
            }
        }
    }//GEN-LAST:event_seismicFileTextFieldActionPerformed

    private void prefixTextFieldActionPerformed(java.awt.AWTEvent evt) {//GEN-FIRST:event_prefixTextFieldActionPerformed
        String valMsg = validatePrefix();
        if (!valMsg.equals("")) {
            JOptionPane.showMessageDialog(this, valMsg, "Validation Error", JOptionPane.ERROR_MESSAGE);
        } else {
            setHorizonNames(numHorizons);
        }
    }//GEN-LAST:event_prefixTextFieldActionPerformed

    private void numHorizonsTextFieldActionPerformed(java.awt.AWTEvent evt) {//GEN-FIRST:event_numHorizonsTextFieldActionPerformed
        String valMsg = validateNumHorizons();
        if (!valMsg.equals("")) {
            JOptionPane.showMessageDialog(this, valMsg, "Validation Error", JOptionPane.ERROR_MESSAGE);
        } else //do nothing of the entered value is the same as before
        if (numHorizons != prevNumHorizons) {
            prevNumHorizons = numHorizons;
            setHorizonNames(numHorizons);
        }
    }//GEN-LAST:event_numHorizonsTextFieldActionPerformed

    private void OkButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OkButtonActionPerformed
        //validate script input parameters
        String valMsg = validateParms();
        if (!valMsg.equals("")) {
            JOptionPane.showMessageDialog(this, valMsg, "Validation Error(s)", JOptionPane.ERROR_MESSAGE);
        } else {
            //generate the BHP-SU script to make the events
            String script = genBhpsuScript();

            //execute the generated BHP-SU script
            String genMsg = execBhpsuScript(script);

            if (!genMsg.equals("")) {
                logger.warning("Script execution warning: " + genMsg);
                logger.warning("Script: " + script);
                JOptionPane.showMessageDialog(this, genMsg, "Script Execution Warning", JOptionPane.WARNING_MESSAGE);
            }

            this.setVisible(false);
            this.dispose();
        }
    }//GEN-LAST:event_OkButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton OkButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel col1Panel;
    private javax.swing.JPanel col2Panel;
    private javax.swing.JPanel col3Panel;
    private javax.swing.JPanel dialogPanel;
    private javax.swing.JScrollPane dialogScrollPane;
    private javax.swing.JTextField event10TextField;
    private javax.swing.JTextField event11TextField;
    private javax.swing.JTextField event12TextField;
    private javax.swing.JTextField event13TextField;
    private javax.swing.JTextField event14TextField;
    private javax.swing.JTextField event15TextField;
    private javax.swing.JTextField event16TextField;
    private javax.swing.JTextField event17TextField;
    private javax.swing.JTextField event18TextField;
    private javax.swing.JTextField event19TextField;
    private javax.swing.JTextField event1TextField;
    private javax.swing.JTextField event20TextField;
    private javax.swing.JTextField event21TextField;
    private javax.swing.JTextField event22TextField;
    private javax.swing.JTextField event23TextField;
    private javax.swing.JTextField event24TextField;
    private javax.swing.JTextField event25TextField;
    private javax.swing.JTextField event26TextField;
    private javax.swing.JTextField event27TextField;
    private javax.swing.JTextField event28TextField;
    private javax.swing.JTextField event29TextField;
    private javax.swing.JTextField event2TextField;
    private javax.swing.JTextField event30TextField;
    private javax.swing.JTextField event3TextField;
    private javax.swing.JTextField event4TextField;
    private javax.swing.JTextField event5TextField;
    private javax.swing.JTextField event6TextField;
    private javax.swing.JTextField event7TextField;
    private javax.swing.JTextField event8TextField;
    private javax.swing.JTextField event9TextField;
    private javax.swing.JLabel eventFileLabel;
    private javax.swing.JTextField eventFileTextField;
    private javax.swing.JPanel fileSelectorPanel;
    private javax.swing.JPanel filesPanel;
    private javax.swing.JLabel horizonNamesLabel;
    private javax.swing.JPanel horizonSelectorPanel;
    private javax.swing.JLabel numHorizonsLabel;
    private javax.swing.JTextField numHorizonsTextField;
    private javax.swing.JLabel prefixLabel;
    private javax.swing.JTextField prefixTextField;
    private javax.swing.JButton seismicFileBrowseButton;
    private javax.swing.JLabel seismicFileLabel;
    private javax.swing.JTextField seismicFileTextField;
    // End of variables declaration//GEN-END:variables

    /** Generate file filter for use by file chooser service
     * @param filterList extensions to use
     * @return filter
     */
    private FileFilter getFileFilter(String[] filterList, String fileType) {
        FileFilter filter = (FileFilter) new GenericFileFilter(filterList, fileType);
        return filter;
    }

    /**
     * Set the seismic textfield with the selected seismic dataset file.
     * @param fileName Name of the selected .dat file
     */
    public void setSelectedDataset(String fileName) {
        String name = fileName.substring(fileName.lastIndexOf(filesep) + 1, fileName.lastIndexOf("."));
        seismicFile = name;
        seismicFileTextField.setText(name);
        eventFileTextField.setEnabled(true);
        eventFile = name + EVENT_SUFFIX_DEFAULT;
        eventFileTextField.setText(eventFile);
    }

    /**
     * Validate the number of horizons.
     * @return The empty string if a valid number; otherwise, a string
     * containing the error message.
     */
    private String validateNumHorizons() {
        String errMsg = "";

        //validate the number of horizons
        String snum = numHorizonsTextField.getText();
        int num = 0;
        try {
            num = Integer.parseInt(snum);
            if (num <= 0) {
                errMsg += "Number of horizons must be > 0\n";
            } else if (num > MAX_NUMBER_HORIZONS) {
                errMsg += "Number of horizons must be <= 30\n";
            } else {
                //remember the number of horizons specified
                numHorizons = num;
            }
        } catch (NumberFormatException nfe) {
            errMsg += "Number of horizons not a number\n";
        }

        return errMsg;
    }

    /**
     * Validate the horizon prefix, i.e., it cannot be empty or contain
     * invalid characters.
     * @return The empty string if a valid prefix; otherwise, a string
     * containing the error message.
     */
    private String validatePrefix() {
        String errMsg = "";

        String prefix = prefixTextField.getText();
        if (prefix == null || "".equals(prefix)) {
            errMsg += "Horizon prefix cannot be empty\n";
        }

        return errMsg;
    }

    /**
     * Validate the seismic file name, i.e., it cannot be empty or contain
     * invalid characters.
     * @return The empty string if a valid file name; otherwise, a string
     * containing the error message.
     */
    private String validateSeismicFilename() {

        String filename = seismicFileTextField.getText();
        if (filename == null || "".equals(filename)) {
            return "Seismic file name cannot be empty\n";
        }

        //if file name ends in .dat, remove it
        int idx = filename.indexOf(DATASET_SUFFIX);
        if (idx != -1) {
            filename = filename.substring(0, idx);
            seismicFileTextField.setText(filename);
        }

        //check if file exists (it may have been typed in)
        QiProjectDescriptor projDesc = viewerAgent.getQiProjectDescriptor();
        String filepath = QiProjectDescUtils.getDatasetsPath(projDesc) + filesep + filename + DATASET_SUFFIX;
        String valMsg = checkFileExist(filepath);

        if (valMsg.equals("no")) {
            return "Seismic file " + filename + " does not exist";
        } else if (valMsg.equals("yes")) {
            return ""; // the success case response expected by the caller of this method
        } else {
            return "Unexpected return value of checkFileExist: " + valMsg;
        }
    }

    /**
     * Validate the event file name, i.e., it cannot be empty or contain
     * invalid characters.
     * @return The empty string if a valid file name; otherwise, a string
     * containing the error message.
     */
    private String validateEventFilename() {
        String errMsg = "";

        String name = eventFileTextField.getText();
        if (name == null || "".equals(name)) {
            errMsg += "Event file name cannot be empty\n";
        }

        return errMsg;
    }

    /**
     * Validate the input parameters to the BHP-SU script. All inputs
     * are validated. If there are multiple errors, all are reported.
     * 
     * @return The empty string if all inputs validate; otherwise, a
     * string containing all the error messages.
     */
    private String validateParms() {
        String errMsg = validateNumHorizons();

        //validate horizon prefix
        errMsg += validatePrefix();

        //validate horizon names
        int num = errMsg.equals("") ? numHorizons : prevNumHorizons;
        logger.warning("validateParms generated unused local variable 'num' :" + num);

        //validate seismic file
        errMsg += validateSeismicFilename();

        //validate event file
        errMsg += validateEventFilename();
        eventFile = eventFileTextField.getText();

        return errMsg;
    }

    /**
     * Set the text fields for the number of horizons. That is,
     * create the text fields and populate with numbered hrozion
     * prefix names.
     * @param num The number of horizon text field to create.
     */
    private void setHorizonNames(int num) {
        javax.swing.JTextField eventTextField;

        //populate used names with the horizon prefix, make editable
        String prefix = prefixTextField.getText();
        if (prefix == null || "".equals(prefix)) {
            prefix = HORIZON_PREFIX_DEFAULT;
            prefixTextField.setText(prefix);
        }
        for (int i = 0; i < num; i++) {
            eventTextField = eventNameList.get(i);
            eventTextField.setEditable(true);
            eventTextField.setText(prefix + (i + 1));
        }

        //clear the unused names, make uneditable
        for (int i = num; i < MAX_NUMBER_HORIZONS; i++) {
            eventTextField = eventNameList.get(i);
            eventTextField.setText("");
            eventTextField.setEditable(false);
        }
    }

    /**
     * Generate the BHP-SU script to make the events; represented as a semicolon
     * separated string of commands..
     * Note: The input parameters must have already been validated.
     * @return The generated script to be executed by a local/remote JobService
     * represented as a semicolon separated string of commands.
     */
    private String genBhpsuScript() {
        //Get the most recent project descriptor
        QiProjectDescriptor qiProjectDesc = viewerAgent.getQiProjectDescriptor();

        String script = "";
        String command = "";

        // source ~/.bashrc
        // make sure the binary of SU and BHP-SU on PATH
        command = "source ~/.bashrc";
        script += command + "; ";

        // export QISPACE
        String qiSpace = QiProjectDescUtils.getQiSpace(qiProjectDesc);
        command = "export QISPACE=\"" + qiSpace + "\"";
        script += command + "; ";

        // export QIPROJECT
        String qiProject = QiProjectDescUtils.getQiProjectReloc(qiProjectDesc);
        command = "export QIPROJECT=\"" + qiProject + "\"";
        script += command + "; ";

        // export QIDATASETS
        String qiDatasets = QiProjectDescUtils.getDatasetsReloc(qiProjectDesc);
        command = "export QIDATASETS=\"" + qiDatasets + "\"";
        script += command + "; ";

        // number of horizons
        command = "numberHorizons=" + numHorizons;
        script += command + "; ";

        // name of input seismic file
        command = "inputDatafile=" + seismicFileTextField.getText();
        script += command + "; ";

        // horizon prefix
        String prefix = prefixTextField.getText();
        if (!prefix.equals(HORIZON_PREFIX_DEFAULT)) {
            command = "eventPrefix=" + prefix;
            script += command + "; ";
        }

        // horizons
        boolean nameChanged = false;
        for (int i = 0; i < numHorizons; i++) {
            if (!eventNameList.get(i).getText().equals(prefix + (i + 1))) {
                nameChanged = true;
                break;
            }
        }
        if (nameChanged) {
            command = "eventNames=";
            for (int i = 0; i < numHorizons; i++) {
                command += i == (numHorizons - 1) ? eventNameList.get(i).getText() : eventNameList.get(i).getText() + ",";
            }
            script += command + "; ";
        }

        // name of output event data file
        command = "eventDatafile=" + eventFileTextField.getText();
        script += command + "; ";

        // bhpmakeevent command
        command = "bhpmakeevent ";
        command += "num=${numberHorizons} ";
        command += "filename=${inputDatafile} ";
        command += "pathlist=${QISPACE}/${QIPROJECT}/${QIDATASETS}/${inputDatafile}.dat ";
        if (!prefix.equals(HORIZON_PREFIX_DEFAULT)) {
            command += "ename=${eventPrefix} ";
        }
        if (nameChanged) {
            command += "horizons=${eventNames} ";
        }
        command += "output=${eventDatafile}";
        script += command + "; ";

        return script;
    }

    /**
     * Execute the generate BHP-SU Script (represented as semicolon separated
     * string of commands).
     * @param script The generated BHP-SU script.
     * @return The empty string if the script executed successfully; otherwise,
     * stderr or a message if the script failed to execute properly.
     */
    private String execBhpsuScript(String script) {
        StringBuilder errMsg = new StringBuilder("");
        ArrayList<String> params = new ArrayList<String>();

        /** Job manager for the generated script executing as a job */
        JobManager jobManager = new JobManager(messagingMgr, this);

        params.add(messagingMgr.getLocationPref());
        params.add("sh");
        params.add("-c");
        params.add(script);
        int status = jobManager.submitJobWait(params);
        ArrayList<String> stderr = jobManager.getStdErr();
        if (status != 0) {
            if (!stderr.isEmpty()) {
                for (int i = 0; i < stderr.size(); i++) {
                    String line = stderr.get(i);
                    if (!line.equals("")) {
                        errMsg.append(line);
                        errMsg.append("\n");
                    }
                }
            } else {
                errMsg.append("Generated script failed to execute:\n");
                errMsg.append(jobManager.getErrorContent());
            }
        }

        return errMsg.toString();
    }

    /**
     * Check to see if a given file exists in the project
     * @param filePath file path to be checked
     * @return yes if file exists in the project; no if it does not; error message if check command failed.
     */
    private String checkFileExist(String filePath) {
        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(filePath);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CHECK_FILE_EXIST_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId);

        if (resp != null && !MsgUtils.isResponseAbnormal(resp)) {
            return (String) resp.getContent();   // yes or no
        } else if (resp == null) {
            return "Timed out checking if seismic file exists.";
        } else {
            return "Error in checking if seismic file exists:\n" + (String) MsgUtils.getMsgContent(resp);
        }
    }
}