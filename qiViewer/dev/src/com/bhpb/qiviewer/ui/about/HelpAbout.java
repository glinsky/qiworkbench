/*
###########################################################################
# This program module Copyright (C) 2008-2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.about;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;



public class HelpAbout extends JDialog {
    private ImageIcon _icon;
    private Component parent;
    private JTextArea textArea = new JTextArea(40,40);
    private JScrollPane scrollPane = new JScrollPane();
    private JEditorPane aboutTextEditorPane = new JEditorPane();
    private JButton licenseAbout;
    private Properties props;
    public HelpAbout(Component parent, Properties props) {
        this.props = props;
        this.parent = parent;
        _icon = new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
          public void run() {
              createAndShowGUI();
          }
        });
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public void createAndShowGUI(){
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");

        aboutTextEditorPane.setAutoscrolls(true);
        textArea.setEnabled(false);
        textArea.setFont(new java.awt.Font("Arial", 0, 14));
        textArea.setRows(20);

        scrollPane.setViewportView(aboutTextEditorPane);
        textArea.setLayout(new FlowLayout(FlowLayout.CENTER));
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setEditable(false);
        aboutTextEditorPane.setText(getAboutHtmlText());
        textArea.setCaretPosition(0);
        final JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);
        panel.add(new JLabel(_icon), BorderLayout.CENTER);
        getContentPane().add(panel, BorderLayout.WEST);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        licenseAbout = new JButton("License");
        licenseAbout.setName("License"); //required by FEST-Swing unit test
        licenseAbout.setActionCommand("viewLicense");
        licenseAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if(licenseAbout.getActionCommand().equals("viewLicense")){
                    String license = getLicenseHtmlText();
                    aboutTextEditorPane.setText(license);
                    aboutTextEditorPane.scrollToReference(license.substring(0,10));
                    licenseAbout = new JButton("About");
                    licenseAbout.setActionCommand("viewAbout");
                }else if(licenseAbout.getActionCommand().equals("viewAbout")){
                    aboutTextEditorPane.setText(getAboutHtmlText());
                    licenseAbout = new JButton("License");
                    licenseAbout.setActionCommand("viewLicense");
                }

            }
        });
        JButton okButton = new JButton("Close");
        okButton.setName("Close"); //required by FEST-Swing unit test
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dispose();
            }
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(licenseAbout);
        bpanel.add(okButton);
        getContentPane().add(bpanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(parent);
        setVisible(true);
    }



    private String getAboutHtmlText(){
        String viewerVersion = "qiViewer - a 2D seismic viewer";
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3><b>");
        sb.append(viewerVersion+"</b><br><br>");


        String versionProp = "";
        String buildProp = "";

        if(props != null){
            versionProp = props.getProperty("project.version");
            buildProp = props.getProperty("project.build");
        }

        if(versionProp == null) {
             versionProp = "";
        }

        if(buildProp == null) {
            buildProp = "";
        }

        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");

        sb.append("This program module Copyright (C) 2008-2009 ,<br>");
        sb.append("BHP Billiton Petroleum and is licensed under<br>");
        sb.append("the BSD License.");
        sb.append("<br><br>");
        sb.append("qiViewer comes with ABSOLUTELY NO WARRANTY. ");
        sb.append("<br><br>");
        sb.append("Click on the <b>License</b> button for more details. ");
        sb.append("<br><br>");
        sb.append("<br><br>");
        sb.append("</font>");
        return sb.toString();

    }

    private String getLicenseHtmlText(){
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3>");
        sb.append("<b>qiViewer</b> - a 2D seismic viewer<br><br>");
        sb.append("This program module Copyright (C) 2008-2009, BHP Billiton Petroleum<br>");
        sb.append("and is licensed under the following BSD License.<br>");
        sb.append("<br>");
        sb.append("Redistribution and use in source and binary forms, with or <br>");
        sb.append("without modification, are permitted provided that the <br>");
        sb.append("following conditions are met:<br>");
        sb.append("<br>");
        sb.append("<ul>");
        sb.append("<li>Redistributions of source code must retain the <br>");
        sb.append("above copyright notice, this list of conditions and <br>");
        sb.append("the following disclaimer.<br>");
        sb.append("</li>");
        sb.append("<li>Redistributions in binary form must reproduce <br>");
        sb.append("the above copyright notice, this list of conditions <br>");
        sb.append("and the following disclaimer in the documentation <br>");
        sb.append("and/or other materials provided with the distribution.");
        sb.append("</li>");
        sb.append("<li>Neither the name of BHP Billiton Petroleum <br>");
        sb.append("nor the names of its contributors may be used to <br>");
        sb.append("endorse or promote products derived from this <br>");
        sb.append("software without specific prior written permission.");
        sb.append("</li>");
        sb.append("</ul>");
        sb.append("<br>");
        sb.append("THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS <br>");
        sb.append("AND CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR <br>");
        sb.append("IMPLIED WARRANTIES, INCLUDING, BUT NOTLIMITED TO, <br>");
        sb.append("THE IMPLIED WARRANTIES OF MERCHANTABILITY AND <br>");
        sb.append("FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. <br>");
        sb.append("IN NO EVENT SHALL THE COPYRIGHT OWNER OR <br>");
        sb.append("CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, <br>");
        sb.append("INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL <br>");
        sb.append("DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT <br>");
        sb.append("OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, <br>");
        sb.append("OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER <br>");
        sb.append("CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN <br>");
        sb.append("CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  <br>");
        sb.append("NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT <br>");
        sb.append("OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF <br>");
        sb.append("THE POSSIBILITY OF SUCH DAMAGE.");
        sb.append("</font>");
        return sb.toString();
    }
}
