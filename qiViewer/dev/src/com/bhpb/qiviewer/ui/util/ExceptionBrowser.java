/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiviewer.ui.util;

import com.bhpb.qiviewer.util.UncaughtExceptionHandler;
import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * <code>ExceptionBrowser</code> displays a list of Messages from uncaught Exceptions.
 *
 * @author <a href="mailto:woody.folsom@bhpbilliton.com">woody.folsom@bhpbilliton.com</a>
 * @version qiViewer release 4 for qiWorkbench 1.3
 *
 */
public class ExceptionBrowser extends JDialog {
    
    public ExceptionBrowser(UncaughtExceptionHandler exceptionHandler) {
        super();
        
        setTitle("Exception Browser");
        
        setMinimumSize(new Dimension(300,300));
        setPreferredSize(new Dimension(800,600));
        
        JTextArea exceptionTextArea = new JTextArea(exceptionHandler.getExceptionDocument());
        exceptionTextArea.setEditable(false);
        
        add(new JScrollPane(exceptionTextArea));
        
        pack();
    }   
}