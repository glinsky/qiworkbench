/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.geographics.controller.AbstractEditorPanelController;
import com.bhpb.geographics.controller.PickingSettingsController;
import com.bhpb.geographics.controller.CursorSettingsController;
import com.bhpb.geographics.controller.EditorResultProcessor;
import com.bhpb.geographics.controller.EditorWorker;
import com.bhpb.geographics.controller.HorizonHighlightSettingsController;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.GeoGraphicsPropertyGroup;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.PickingSettings;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.ui.AbstractGeoPlot.ACTION_MODE;
import com.bhpb.geographics.ui.PickingSettingsPanel;
import com.bhpb.geographics.ui.CursorSettingsPanel;
import com.bhpb.geographics.ui.HorizonHighlightPanel;
import com.bhpb.geographics.ui.event.GeoPlotEvent;
import com.bhpb.geographics.ui.layer.EditorDialog;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.controller.desktop.NewXsecWindowActionListener;
import com.bhpb.qiviewer.ui.about.HelpAbout;
import com.bhpb.qiviewer.ui.layer.CreateHorizonsDialog;
import com.bhpb.qiviewer.ui.movie.MovieDialog;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiviewer.ui.window.ViewerWindowFactory;
import com.bhpb.qiviewer.ui.window.ViewerWindowFactory.XML_FORMAT;
import com.bhpb.qiviewer.util.ProgressDialog;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.MDIDesktopPane;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import org.w3c.dom.Node;

/**
 * <code>ViewerDesktop</code> is a component which resides on the QiWorkbench desktop (it is a JInternalFrame)
 * and encloses its Viewers (JInternalFrames) within its JDesktopPane.
 *
 */
public class ViewerDesktop extends JInternalFrame implements EditorResultProcessor<GeoGraphicsPropertyGroup> {
    //package-protected for use by ViewerDesktopImporters

    MDIDesktopPane viewerDesktopPane;
    ViewerWindow lastActiveViewerWindow;
    PickingSettings pickingSettings = new PickingSettings();
    CursorSettings cursorSettings = new CursorSettings();
    HorizonHighlightSettings horizonHighlightSettings = new HorizonHighlightSettings();
    transient ViewerAgent agent = null;
    private transient ViewerDesktopImporter bhpViewerDesktopImporter;
    private transient ViewerDesktopExporter qiViewerDesktopExporter;
    private transient ViewerDesktopImporter qiViewerDesktopImporter;
    private static final long serialVersionUID = 378861174098306549L;
    private static final Logger logger = Logger.getLogger(ViewerDesktop.class.toString());
    private static final String NO_WINDOW_SELECTED_ERROR_MSG = "Please select a window";
    private static final Dimension DEFAULT_SIZE = new Dimension(800, 500);
    private JButton helpButton;
    private JButton hideLayerButton;
    private JButton pickPBPbutton;
    private JButton showLayerButton;
    private JButton snapShotButton;
    private JButton stepMinusButton;
    private JButton stepPlusButton;
    private JButton zoomInButton;
    private JButton zoomRubberButton;
    private JButton viewAllButton;
    private JButton zoomOutButton;
    private JCheckBoxMenuItem horizonGraphMenuItem;
    private JCheckBoxMenuItem statusBarCheckBoxMenuItem;
    private JCheckBoxMenuItem toolBarCheckBoxMenuItem;
    private JDialog aboutDialog = null;
    private JLabel qiViewerLabel;
    private JLabel infoLabel;
    private JLabel jLabel3;
    private JLabel statusLabel;
    private JMenu addLayerMenu;
    private JMenu fileMenu;
    private JMenu helpMenu;
    private JMenu layerMenu;
    private JMenu newMapWindowMenu;
    private JMenu newXsectionWindowMenu;
    private JMenu preferencesMenu;
    private JMenu toolsMenu;
    private JMenu windowMenu;
    private JMenuBar viewerDesktopMenuBar;
    private JMenuItem aboutMenuItem;
    private JMenuItem addBhpSuMenuItem;
    private JMenuItem annotationMenuItem;
    private JMenuItem cascadeAllMenuItem;
    private JMenuItem createNewHorizonMenuItem;
    private JMenuItem deleteLayersMenuItem;
    private JMenuItem ensemblePlusMenuItem;
    private JMenuItem exportMenuItem;
    private JMenuItem horizonHighlightMenuItem;
    private JMenu importMenu;
    private JMenuItem incrementMinusMenuItem;
    private JMenuItem incrementPlusMenuItem;
    private JMenuItem hideLayersMenuItem;
    private JMenuItem moveLayersToBottomMenuItem;
    private JMenuItem moveLayersToTopMenuItem;
    private JMenuItem movieMenuItem;
    private JMenuItem newBHPSUmenuItem;
    private JMenuItem newBHPSUmenuItem2;
    private JMenuItem openHelpMenuItem;
    private JMenuItem openOldSessionMenuItem;
    private JMenuItem pickingMenuItem;
    private JMenuItem bhpViewerFormatMenuItem;
    private JMenuItem propertyMenuItem;
    private JMenuItem qiwbFormatMenuItem;
    private JMenuItem quitMenuItem;
    private JMenuItem saveHorizonMenuItem;
    private JMenuItem saveMenuItem;
    private JMenuItem saveAsMenuItem;
    private JMenuItem saveQuitMenuItem;
    private JMenuItem scaleMenuItem;
    private JMenuItem stopLoadingDataMenuItem;
    private JMenuItem synchronizationMenuItem;
    private JMenuItem showLayersMenuItem;
    private JMenuItem snapshotMenuItem;
    private JMenuItem tileAllMenuItem;
    private JMenuItem trackingCursorMenuItem;
    private JMenuItem viewAllMenuItem;
    private JMenuItem zoomOutMenuItem;
    private JMenuItem zoomResetMenuItem;
    private JMenuItem zoomRubberMenuItem;
    private JMenuItem zoomInMenuItem;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel statusPanel;
    private JScrollPane viewerDesktopScrollPane;
    private JSeparator fileMenuSeparator1;
    private JSeparator fileMenuSeparator2;
    private JSeparator layerMenuSeparator1;
    private JSeparator layerMenuSeparator2;
    private JSeparator windowMenuSeparator1;
    private JSeparator windowMenuSeparator2;
    private JSeparator preferencesMenuSeparator;
    private JToolBar toolbar;

    /**
     * Constructs a resizable, closeable, maximizable, iconifiable ViewerDesktop.
     * It is named 'QiViewerDesktop'.
     * 
     * @param agent ViewerAgent controller associated with this view.
     */
    public ViewerDesktop(ViewerAgent agent) {
        super(agent.getComponentDescriptor().getCID(),
                true, //resizable
                false, //closeable
                true, //maximizable
                true); //iconifiable

        super.setName("QiViewerDesktop");

        this.agent = agent;
        initComponents();

        //this is done after initComponents due to pack()
        super.setSize(DEFAULT_SIZE);

        bhpViewerDesktopImporter = new BhpViewerXmlImporter(this);
        qiViewerDesktopExporter = new QiViewerDesktopExporter(this);
        qiViewerDesktopImporter = new QiViewerXmlImporter(this);

        resetTitle(agent.getQiProjectDescriptor().getQiProjectName());
    }

    private void initComponents() {
        jPanel1 = new JPanel();
        qiViewerLabel = new JLabel();
        infoLabel = new JLabel();

        toolbar = new JToolBar();
        // because it is not floatable in the bhpViewer, it is not floatable here
        toolbar.setFloatable(false);

        toolbar.setName("toolbar");

        viewAllButton = new JButton();
        zoomRubberButton = new JButton();
        zoomInButton = new JButton();
        zoomOutButton = new JButton();
        stepPlusButton = new JButton();
        stepMinusButton = new JButton();
        showLayerButton = new JButton();
        hideLayerButton = new JButton();
        pickPBPbutton = new JButton();
        snapShotButton = new JButton();
        helpButton = new JButton();
        jPanel2 = new JPanel();
        statusPanel = new JPanel();
        jLabel3 = new JLabel();
        statusLabel = new JLabel();
        viewerDesktopMenuBar = new JMenuBar();
        fileMenu = new JMenu();
        newXsectionWindowMenu = new JMenu();

        newBHPSUmenuItem = new JMenuItem("BHP-SU");
        newBHPSUmenuItem.addActionListener(new NewXsecWindowActionListener(agent));
        newXsectionWindowMenu.add(newBHPSUmenuItem);

        newMapWindowMenu = new JMenu();
        newBHPSUmenuItem2 = new JMenuItem("BHP-SU");
        newBHPSUmenuItem2.addActionListener(new NewXsecWindowActionListener(agent));
        newMapWindowMenu.add(newBHPSUmenuItem2);

        fileMenuSeparator1 = new JSeparator();

        quitMenuItem = new JMenuItem();
        quitMenuItem.setName("Quit");

        saveMenuItem = new JMenuItem();
        saveMenuItem.setName("Save");

        saveAsMenuItem = new JMenuItem();
        saveAsMenuItem.setName("SaveAs");

        saveQuitMenuItem = new JMenuItem();
        saveQuitMenuItem.setName("SaveQuit");

        openOldSessionMenuItem = new JMenuItem();
        openOldSessionMenuItem.setName("OpenSession");

        fileMenuSeparator2 = new JSeparator();
        importMenu = new JMenu();
        importMenu.setName("Import");
        qiwbFormatMenuItem = new JMenuItem();
        qiwbFormatMenuItem.setName("ImportQiwbFormat");
        bhpViewerFormatMenuItem = new JMenuItem();
        bhpViewerFormatMenuItem.setName("ImportPreQiwbFormat");

        exportMenuItem = new JMenuItem();
        exportMenuItem.setName("Export");
        stopLoadingDataMenuItem = new JMenuItem();

        windowMenu = new JMenu();
        scaleMenuItem = new JMenuItem();
        annotationMenuItem = new JMenuItem();
        synchronizationMenuItem = new JMenuItem();
        horizonGraphMenuItem = new JCheckBoxMenuItem();
        windowMenuSeparator1 = new JSeparator();
        zoomInMenuItem = new JMenuItem();
        zoomOutMenuItem = new JMenuItem();
        zoomResetMenuItem = new JMenuItem();
        zoomRubberMenuItem = new JMenuItem();
        viewAllMenuItem = new JMenuItem();
        windowMenuSeparator2 = new JSeparator();
        cascadeAllMenuItem = new JMenuItem();
        tileAllMenuItem = new JMenuItem();
        addBhpSuMenuItem = new JMenuItem();
        layerMenu = new JMenu();
        propertyMenuItem = new JMenuItem();
        addLayerMenu = new JMenu();
        createNewHorizonMenuItem = new JMenuItem();
        saveHorizonMenuItem = new JMenuItem();
        layerMenuSeparator1 = new JSeparator();
        incrementPlusMenuItem = new JMenuItem();
        incrementMinusMenuItem = new JMenuItem();
        ensemblePlusMenuItem = new JMenuItem();
        layerMenuSeparator2 = new JSeparator();
        hideLayersMenuItem = new JMenuItem();
        showLayersMenuItem = new JMenuItem();
        deleteLayersMenuItem = new JMenuItem();
        moveLayersToTopMenuItem = new JMenuItem();
        moveLayersToBottomMenuItem = new JMenuItem();
        toolsMenu = new JMenu();
        movieMenuItem = new JMenuItem();
        snapshotMenuItem = new JMenuItem();
        preferencesMenu = new JMenu();
        toolBarCheckBoxMenuItem = new JCheckBoxMenuItem();
        statusBarCheckBoxMenuItem = new JCheckBoxMenuItem();
        preferencesMenuSeparator = new JSeparator();
        pickingMenuItem = new JMenuItem();
        trackingCursorMenuItem = new JMenuItem();
        horizonHighlightMenuItem = new JMenuItem();

        helpMenu = new JMenu();
        helpMenu.setName("Help");

        aboutMenuItem = new JMenuItem();
        aboutMenuItem.setName("About");

        openHelpMenuItem = new JMenuItem();

        jPanel1.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        qiViewerLabel.setText("qiViewer");

        infoLabel.setIcon(new ImageIcon(getClass().getResource("/icons/Info.gif")));

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1Layout.createSequentialGroup().add(infoLabel).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(qiViewerLabel).addContainerGap(670, Short.MAX_VALUE)));
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(infoLabel).add(qiViewerLabel)));

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("qiViewer");

        viewAllButton.setName("viewAll");
        viewAllButton.setIcon(new ImageIcon(getClass().getResource("/icons/ViewAll.gif")));
        viewAllButton.setToolTipText("View the whole image");
        viewAllButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewAllButtonActionPerformed(evt);
            }
        });

        toolbar.add(viewAllButton);

        zoomRubberButton.setName("zoomRubber");
        zoomRubberButton.setIcon(new ImageIcon(getClass().getResource("/icons/ZoomRubber.gif")));
        zoomRubberButton.setToolTipText("To do rubber band zooming");
        zoomRubberButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomRubberbandButtonActionPerformed(evt);
            }
        });

        toolbar.add(zoomRubberButton);

        zoomInButton.setName("zoomIn");
        zoomInButton.setToolTipText("Zoom in the display by 2");
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonActionPerformed(evt);
            }
        });
        zoomInButton.setIcon(new ImageIcon(getClass().getResource("/icons/ZoomIn.gif")));
        toolbar.add(zoomInButton);

        zoomOutButton.setName("zoomOut");
        zoomOutButton.setIcon(new ImageIcon(getClass().getResource("/icons/ZoomOut.gif")));
        zoomOutButton.setToolTipText("Zoom out the display by 2");
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonActionPerformed(evt);
            }
        });

        toolbar.add(zoomOutButton);

        stepPlusButton.setName("stepPlus");
        stepPlusButton.setIcon(new ImageIcon(getClass().getResource("/icons/StepPlus.gif")));
        stepPlusButton.setToolTipText("Forward increment the data");
        stepPlusButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepPlusButtonActionPerformed(evt);
            }
        });

        toolbar.add(stepPlusButton);

        stepMinusButton.setName("stepMinus");
        stepMinusButton.setIcon(new ImageIcon(getClass().getResource("/icons/StepMinus.gif")));
        stepMinusButton.setToolTipText("Backward increment the data");
        stepMinusButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepMinusButtonActionPerformed(evt);
            }
        });

        toolbar.add(stepMinusButton);

        showLayerButton.setName("showLayer");
        showLayerButton.setIcon(new ImageIcon(getClass().getResource("/icons/ShowLayer.gif")));
        showLayerButton.setToolTipText("Show all the selected layers");
        showLayerButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showLayersActionPerformed(evt);
            }
        });

        toolbar.add(showLayerButton);

        hideLayerButton.setName("hideLayer");
        hideLayerButton.setIcon(new ImageIcon(getClass().getResource("/icons/HideLayer.gif")));
        hideLayerButton.setToolTipText("Hide the selected layers");
        hideLayerButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hideLayersActionPerformed(evt);
            }
        });

        toolbar.add(hideLayerButton);

        pickPBPbutton.setName("pickPBP");
        pickPBPbutton.setIcon(new ImageIcon(getClass().getResource("/icons/PickPBP.gif")));
        pickPBPbutton.setToolTipText("Show the picking settings");
        pickPBPbutton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pickPBPbuttonActionPerformed(evt);
            }
        });

        toolbar.add(pickPBPbutton);

        snapShotButton.setName("snapShot");
        snapShotButton.setIcon(new ImageIcon(getClass().getResource("/icons/Snapshot.gif")));
        snapShotButton.setToolTipText("Create a snapshot of the current display");
        snapShotButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                snapShotButtonActionPerformed(evt);
            }
        });

        toolbar.add(snapShotButton);

        helpButton.setName("help");
        helpButton.setIcon(new ImageIcon(getClass().getResource("/icons/Help.gif")));
        helpButton.setToolTipText("Get context-sensitive help");
        helpButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpButtonActionPerformed(evt);
            }
        });

        toolbar.add(helpButton);

        jPanel2.setLayout(new java.awt.BorderLayout());
        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        //The viewerDesktopPane contains all of the ViewerDesktop's JInternalFrames
        viewerDesktopPane = new MDIDesktopPane();
        viewerDesktopScrollPane = new JScrollPane(viewerDesktopPane);
        jPanel2.add(viewerDesktopScrollPane, BorderLayout.CENTER);

        statusPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        statusPanel.setBorder(BorderFactory.createEtchedBorder());
        statusPanel.setPreferredSize(new java.awt.Dimension(100, 25));
        jLabel3.setIcon(new ImageIcon(getClass().getResource("/icons/Info.gif")));
        statusPanel.add(jLabel3);

        statusLabel.setText("qiViewer");
        statusPanel.add(statusLabel);

        jPanel2.add(statusPanel, java.awt.BorderLayout.SOUTH);

        fileMenu.setText("File");
        fileMenu.setName("File");

        newXsectionWindowMenu.setText("New X-Section Window");
        fileMenu.add(newXsectionWindowMenu);

        newMapWindowMenu.setText("New Map Window");
        fileMenu.add(newMapWindowMenu);

        stopLoadingDataMenuItem.setText("Stop Loading Data");
        stopLoadingDataMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopLoadingDataMenuItemActionPerformed(evt);
            }
        });

        fileMenu.add(stopLoadingDataMenuItem);

        fileMenu.add(fileMenuSeparator1);

        importMenu.setMnemonic('I');
        importMenu.setText("Import...");
        importMenu.setToolTipText("Restore a qiViewer's state saved in a file");

        qiwbFormatMenuItem.setText("qiWorkbench format");
        qiwbFormatMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importQiwbFormatMenuItemActionPerformed(evt);
            }
        });
        importMenu.add(qiwbFormatMenuItem);

        bhpViewerFormatMenuItem.setText("bhpViewer format");
        bhpViewerFormatMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importbhpViewerFormatMenuItemActionPerformed(evt);
            }
        });
        importMenu.add(bhpViewerFormatMenuItem);
        fileMenu.add(importMenu);

        exportMenuItem.setMnemonic('E');
        exportMenuItem.setText("Export...");
        exportMenuItem.setToolTipText("Save qiViewer's state to a file");
        exportMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exportMenuItem);

        fileMenu.add(fileMenuSeparator2);

        quitMenuItem.setMnemonic('Q');
        quitMenuItem.setText("Quit");
        quitMenuItem.setToolTipText("Quit without saving state");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(quitMenuItem);

        saveMenuItem.setMnemonic('S');
        saveMenuItem.setText("Save");
        saveMenuItem.setToolTipText("Save qiViewer's state in session state");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setMnemonic('A');
        saveAsMenuItem.setText("Save As");
        saveAsMenuItem.setToolTipText("Save qiViewer's state as a clone");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        saveQuitMenuItem.setMnemonic('U');
        saveQuitMenuItem.setText("Save, Quit");
        saveQuitMenuItem.setToolTipText("Save qiViewer's state in<br> session state, then quit");
        saveQuitMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveQuitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveQuitMenuItem);

        viewerDesktopMenuBar.add(fileMenu);

        windowMenu.setText("Window");
        scaleMenuItem.setText("Scale ...");
        scaleMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scaleMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(scaleMenuItem);

        annotationMenuItem.setText("Annotation ...");
        annotationMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annotationMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(annotationMenuItem);

        synchronizationMenuItem.setText("Synchronization ...");
        synchronizationMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                synchronizationMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(synchronizationMenuItem);

        horizonGraphMenuItem.setText("Horizon Graph");
        horizonGraphMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horizonGraphMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(horizonGraphMenuItem);

        windowMenu.add(windowMenuSeparator1);

        zoomInMenuItem.setText("Zoom In");
        zoomInMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(zoomInMenuItem);

        zoomOutMenuItem.setText("Zoom Out");
        zoomOutMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(zoomOutMenuItem);

        zoomResetMenuItem.setText("Zoom Reset");
        zoomResetMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomResetMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(zoomResetMenuItem);

        zoomRubberMenuItem.setText("Zoom Rubber");
        zoomRubberMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomRubberbandMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(zoomRubberMenuItem);

        viewAllMenuItem.setText("View All");
        viewAllMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewAllMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(viewAllMenuItem);

        windowMenu.add(windowMenuSeparator2);

        cascadeAllMenuItem.setText("Cascade All");
        cascadeAllMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cascadeAllMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(cascadeAllMenuItem);

        tileAllMenuItem.setText("Tile All");
        tileAllMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tileAllMenuItemActionPerformed(evt);
            }
        });

        windowMenu.add(tileAllMenuItem);

        viewerDesktopMenuBar.add(windowMenu);

        layerMenu.setText("Layer");
        layerMenu.addMenuListener(new MenuListener() {

            public void menuSelected(MenuEvent ae) {
                logger.info("Layer menu action performed");

                ViewerWindow activeWindow = getActiveWindow();
                if (activeWindow == null) {
                    saveHorizonMenuItem.setEnabled(false);
                    return;
                }

                Layer activeLayer = activeWindow.getActiveLayer();
                if (activeLayer == null) {
                    saveHorizonMenuItem.setEnabled(false);
                    return;
                }

                saveHorizonMenuItem.setEnabled(activeLayer.getDatasetProperties().isHorizon());
            }

            public void menuCanceled(MenuEvent e) {
                //do nothing
            }

            public void menuDeselected(MenuEvent e) {
                //do nothing
            }
        });

        propertyMenuItem.setText("Property ...");
        propertyMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                propertyMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(propertyMenuItem);

        addLayerMenu.setText("Add Layer");

        addBhpSuMenuItem.setText("BHP-SU ...");
        addBhpSuMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBhpSuMenuItemActionPerformed(evt);
            }
        });
        addLayerMenu.add(addBhpSuMenuItem);


        layerMenu.add(addLayerMenu);

        createNewHorizonMenuItem.setText("Create New Horizon");
        createNewHorizonMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createNewHorizonMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(createNewHorizonMenuItem);

        saveHorizonMenuItem.setText("Save Horizon ...");
        saveHorizonMenuItem.setEnabled(false);
        saveHorizonMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agent.writeActiveHorizon();
            }
        });

        layerMenu.add(saveHorizonMenuItem);

        layerMenu.add(layerMenuSeparator1);

        incrementPlusMenuItem.setText("Increment +");
        incrementPlusMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incrementPlusMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(incrementPlusMenuItem);

        incrementMinusMenuItem.setText("Increment -");
        incrementMinusMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incrementMinusMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(incrementMinusMenuItem);

        ensemblePlusMenuItem.setText("Ensemble +");
        ensemblePlusMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ensemblePlusMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(ensemblePlusMenuItem);

        layerMenu.add(layerMenuSeparator2);

        hideLayersMenuItem.setText("Hide Layers");
        hideLayersMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hideLayersActionPerformed(evt);
            }
        });

        layerMenu.add(hideLayersMenuItem);

        showLayersMenuItem.setText("Show Layers");
        showLayersMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showLayersActionPerformed(evt);
            }
        });

        layerMenu.add(showLayersMenuItem);

        deleteLayersMenuItem.setText("Delete Layers");
        deleteLayersMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLayersMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(deleteLayersMenuItem);

        moveLayersToTopMenuItem.setText("Move Layers To Top");
        moveLayersToTopMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveLayersToTopMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(moveLayersToTopMenuItem);

        moveLayersToBottomMenuItem.setText("Move Layers To Bottom");
        moveLayersToBottomMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveLayersToBottomMenuItemActionPerformed(evt);
            }
        });

        layerMenu.add(moveLayersToBottomMenuItem);

        viewerDesktopMenuBar.add(layerMenu);

        toolsMenu.setText("Tools");
        movieMenuItem.setText("Movie ...");
        movieMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                movieMenuItemActionPerformed(evt);
            }
        });

        toolsMenu.add(movieMenuItem);

        snapshotMenuItem.setText("Snapshot");
        snapshotMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                snapshotMenuItemActionPerformed(evt);
            }
        });

        toolsMenu.add(snapshotMenuItem);

        viewerDesktopMenuBar.add(toolsMenu);

        preferencesMenu.setText("Preferences");
        preferencesMenu.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preferencesMenuActionPerformed(evt);
            }
        });

        toolBarCheckBoxMenuItem.setSelected(true);
        toolBarCheckBoxMenuItem.setText("ToolBar");
        toolBarCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolBarCheckBoxMenuItemActionPerformed(evt);
            }
        });

        preferencesMenu.add(toolBarCheckBoxMenuItem);

        statusBarCheckBoxMenuItem.setSelected(true);
        statusBarCheckBoxMenuItem.setText("StatusBar");
        statusBarCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statusBarCheckBoxMenuItemActionPerformed(evt);
            }
        });

        preferencesMenu.add(statusBarCheckBoxMenuItem);

        preferencesMenu.add(preferencesMenuSeparator);

        pickingMenuItem.setText("Picking ...");
        pickingMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pickingMenuItemActionPerformed(evt);
            }
        });

        preferencesMenu.add(pickingMenuItem);

        trackingCursorMenuItem.setText("Tracking Cursor ...");
        trackingCursorMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trackingCursorMenuItemActionPerformed(evt);
            }
        });

        preferencesMenu.add(trackingCursorMenuItem);

        horizonHighlightMenuItem.setText("Horizon Highlight ...");
        horizonHighlightMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horizonHighlightMenuItemActionPerformed(evt);
            }
        });

        preferencesMenu.add(horizonHighlightMenuItem);

        viewerDesktopMenuBar.add(preferencesMenu);

        helpMenu.setText("Help");
        aboutMenuItem.setText("About ...");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });

        helpMenu.add(aboutMenuItem);

        openHelpMenuItem.setText("Open Help ...");
        openHelpMenuItem.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openHelpMenuItemActionPerformed(evt);
            }
        });

        helpMenu.add(openHelpMenuItem);

        viewerDesktopMenuBar.add(helpMenu);

        setJMenuBar(viewerDesktopMenuBar);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(toolbar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE).add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE));
        layout.setVerticalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout.createSequentialGroup().add(toolbar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)));
        pack();
    }

    private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void snapShotButtonActionPerformed(java.awt.event.ActionEvent evt) {
        agent.getMovieController().captureImage();
    }

    private void pickPBPbuttonActionPerformed(java.awt.event.ActionEvent evt) {
        agent.getActiveWindow().setActionMode(ACTION_MODE.PICK);
        showPickingMenu();
    }

    private void stepMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {
        agent.decrementFrame();
    }

    private void stepPlusButtonActionPerformed(java.awt.event.ActionEvent evt) {
        agent.incrementFrame();
    }

    private void zoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {
        zoomIn();
    }

    private void zoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {
        zoomOut();
    }

    private void zoomRubberband() {
        if (lastActiveViewerWindow == null) {
            logger.info("Unable to toggle zoom mode: rubberband - no viewer window is active");
            showSelectWindowFirstMessage();
        } else {
            lastActiveViewerWindow.toggleZoomSelectionMode();
        }
    }

    private void zoomRubberbandButtonActionPerformed(java.awt.event.ActionEvent evt) {
        zoomRubberband();
    }

    private void viewAllButtonActionPerformed(java.awt.event.ActionEvent evt) {
        zoomAll();
    }

    private void openHelpMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showAboutDialog();
    }

    private void trackingCursorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        CursorSettingsPanel cursorSettingsPanel = new CursorSettingsPanel();
        AbstractEditorPanelController controller =
                new CursorSettingsController(new CursorSettings(cursorSettings), cursorSettingsPanel);
        EditorDialog dlg = new EditorDialog(
                (JFrame) SwingUtilities.getRoot(this),
                cursorSettingsPanel, controller);

        EditorWorker<CursorSettings> worker = new EditorWorker<CursorSettings>(
                (EditorResultProcessor) this,
                controller,
                dlg);

        new Thread(worker).start();
    }

    private void horizonHighlightMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        HorizonHighlightPanel horizonHighlightPanel = new HorizonHighlightPanel();
        AbstractEditorPanelController controller =
                new HorizonHighlightSettingsController(
                new HorizonHighlightSettings(horizonHighlightSettings), horizonHighlightPanel);
        EditorDialog dlg = new EditorDialog(
                (JFrame) SwingUtilities.getRoot(this),
                horizonHighlightPanel, controller);

        EditorWorker<HorizonHighlightSettings> worker = new EditorWorker<HorizonHighlightSettings>(
                (EditorResultProcessor) this,
                controller,
                dlg);

        new Thread(worker).start();
    }

    private void pickingMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        showPickingMenu();
    }

    private void statusBarCheckBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void toolBarCheckBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void movieMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        MovieDialog dialog = new MovieDialog(agent.getMovieController());

        if (dialog.isVisible()) {
            dialog.setVisible(false);
        }

        dialog.setVisible(true);
    }

    private void snapshotMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.getMovieController().captureImage();
    }

    private void propertyMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        JInternalFrame[] frames = viewerDesktopPane.getAllFrames();
        if (frames != null && frames.length == 0) {
            JOptionPane.showMessageDialog(this, "Viewer windows must be present to edit layer properties.");
            return;
        }

        ViewerWindow win = getActiveWindow();
        if (win == null) {
            JOptionPane.showMessageDialog(this, "Please select a viewer window first.");
            return;
        }

        if (win.getActiveLayer() == null) {
            JOptionPane.showMessageDialog(this, "Please select a layer first.");
            return;
        }

        win.showLayerPropertiesDialog();

    }

    private void addBhpSuMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.addBhpSuLayer();
    }

    private void createNewHorizonMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        JDialog dlg = new CreateHorizonsDialog(agent, this, true);
        dlg.setVisible(true);
    }

    private void incrementPlusMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.incrementFrame();
    }

    private void incrementMinusMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.decrementFrame();
    }

    private void ensemblePlusMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void hideLayersActionPerformed(java.awt.event.ActionEvent evt) {
        setSelectedLayersHidden(true);
    }

    private void setSelectedLayersHidden(boolean isHidden) {
        ViewerWindow window = getActiveWindow();
        if (window == null) {
            JOptionPane.showMessageDialog(this, "A ViewerWindow must be selected before a layer can be shown or hidden.");
        } else {
            List<Layer> layers = window.getSelectedLayers();
            window.setLayersHidden(layers, isHidden);
        }
    }

    private void showLayersActionPerformed(java.awt.event.ActionEvent evt) {
        setSelectedLayersHidden(false);
    }

    private void showPickingMenu() {
        PickingSettingsPanel view = new PickingSettingsPanel();
        AbstractEditorPanelController controller = new PickingSettingsController(pickingSettings, view);
        JDialog dlg = new EditorDialog((JFrame) SwingUtilities.getRoot(this), view, controller);
        dlg.setVisible(true);
        dlg.pack();
    }

    private void deleteLayersMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        ViewerWindow viewerWindow = getActiveWindow();
        if (viewerWindow == null) {
            showSelectWindowFirstMessage();
        } else {
            viewerWindow.deleteLayer(viewerWindow.getActiveLayer());
        }
    }

    private void moveLayersToTopMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        ViewerWindow viewerWindow = getActiveWindow();
        if (viewerWindow == null) {
            showSelectWindowFirstMessage();
        } else {
            viewerWindow.moveSelectedLayersToTop();
        }
    }

    private void moveLayersToBottomMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        ViewerWindow viewerWindow = getActiveWindow();
        if (viewerWindow == null) {
            showSelectWindowFirstMessage();
        } else {
            viewerWindow.moveSelectedLayersToBottom();
        }
    }

    private void tileAllMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        viewerDesktopPane.tileFrames();
    }

    private void cascadeAllMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        viewerDesktopPane.cascadeFrames();
    }

    public void closeAllViewerWindows() {
        viewerDesktopPane.disposeAll();
    }

    private void viewAllMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        zoomAll();
    }

    private void zoomRubberbandMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        zoomRubberband();
    }

    private void zoomResetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void zoomOutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        zoomOut();
    }

    private void zoomInMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        zoomIn();
    }

    private void horizonGraphMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void synchronizationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        if (lastActiveViewerWindow == null) {
            JOptionPane.showMessageDialog(this, NO_WINDOW_SELECTED_ERROR_MSG);
        } else {
            logger.info("Opening sync properties dialog for Window: " + lastActiveViewerWindow.getWinID());
            lastActiveViewerWindow.editSyncProperties();
        }
    }

    private void annotationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        if (lastActiveViewerWindow == null) {
            JOptionPane.showMessageDialog(this, NO_WINDOW_SELECTED_ERROR_MSG);
        } else {
            logger.info("Opening annotation properties dialog for Window: " + lastActiveViewerWindow.getWinID());
            lastActiveViewerWindow.editAnnotationProperties();
        }
    }

    private void scaleMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        if (lastActiveViewerWindow == null) {
            JOptionPane.showMessageDialog(this, NO_WINDOW_SELECTED_ERROR_MSG);
        } else {
            logger.info("Opening scale properties dialog for Window: " + lastActiveViewerWindow.getWinID());
            lastActiveViewerWindow.editScaleProperties();
        }
    }

    private void stopLoadingDataMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    private void exportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        //Use a file chooser to specify the path of the file where the qiComponent's state will be saved.
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.EXPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingManager());
    }

    private void importQiwbFormatMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        //Use a file chooser to select the qiViewer state file
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.IMPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingManager());
    }

    private void importbhpViewerFormatMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        //Use a file chooser to select the bhpViewer state file
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.OPEN_SESSION_ACTION_BY_2D_VIEWER, agent.getComponentDescriptor(), agent.getMessagingManager());
    }

    public void importBhpViewerState(Node node) {
        bhpViewerDesktopImporter.importState(node);
    }

    public void importQiViewerState(Node node) {
        qiViewerDesktopImporter.importState(node);
    }

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.deactivateSelf();
    }

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.saveState();
    }

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.saveStateAsClone();
    }

    private void saveQuitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        agent.saveStateThenQuit();
    }

    private void preferencesMenuActionPerformed(java.awt.event.ActionEvent evt) {
        agent.showUnimplementedCodeWarning();
    }

    public void showAboutDialog(final Properties props) {
        final Component comp = this;
        EventQueue.invokeLater(new Runnable() {

            public void run() {
                if (aboutDialog != null) {
                    aboutDialog.setVisible(true);
                } else {
                    aboutDialog = new HelpAbout(comp, props);
                }
            }
        });
    }

    /**
     * Reset the GUI's title based on the component's display name, preferred display name
     * and the given project name.
     * 
     * The title is formulated as follows:
     * 
     * "<component display name>: <preferred display name> Project: <projName>"
     *
     * @param projName Name of the associated project.
     */
    public void resetTitle(String projName) {
        String compName = "";
        IComponentDescriptor agentDesc = agent.getComponentDescriptor();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0) {
            compName = pdn.trim();
        } else {
            compName = screenName;
        }
        int idx = screenName.indexOf("#");
        if (idx != -1) {
            screenName = screenName.substring(0, idx);
        }
        this.setTitle(screenName + ": " + compName + "  Project: " + projName);
    }

    /**
     * Returns the ViewerWindow which currently has or last had focus.
     * 
     * @return the active ViewerWindow
     */
    public ViewerWindow getActiveWindow() {
        return lastActiveViewerWindow;
    }

    public CursorSettings getCursorSettings() {
        return cursorSettings;
    }

    public HorizonHighlightSettings getHorizonHighlightSettings() {
        return horizonHighlightSettings;
    }

    public PickingSettings getPickingSettings() {
        return pickingSettings;
    }

    /**
     * Get the size of the JScrollPane which contains the main qiViewer MDIDesktopPane.
     * 
     * @return the size of the 
     */
    public Dimension getDesktopPaneSize() {
        if (viewerDesktopScrollPane != null) {
            return viewerDesktopScrollPane.getSize();
        } else {
            return new Dimension(0, 0);
        }
    }

    Frame getFrame() {
        return Frame.getFrames()[0];
    }

    public Image getImage() {
        BufferedImage bufferedImage = new BufferedImage(getWidth(), getHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics g = bufferedImage.getGraphics();
        paint(g);
        g.dispose();

        return bufferedImage;
    }

    public static String getXmlAlias() {
        return "viewerDesktop";
    }

    /**
     * Serializes the state of this <code>ViewerDesktop</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() throws Exception {
        return qiViewerDesktopExporter.exportState();
    }

    /**
     *
     * @param node
     * @param progressDialog progressDialog ProgressDialog with isCancellable() == false
     * @return
     */
    ViewerWindow openNewBhpViewerWindow(Node node, ProgressDialog progressDialog) {
        ViewerWindow viewerWindow =
                ViewerWindowFactory.restoreFromXml(XML_FORMAT.BHPVIEWER,
                node, agent, agent.getDefaultWindowSyncGroup(),
                progressDialog);
        if (progressDialog.isCancelable()) {
            throw new IllegalArgumentException("ProgressDialog must not be cancellable for use by restoreFromXml()");
        }

        agent.addViewerWindow(viewerWindow);
        //Agent cannot invoke desktopPane.add() because the ViewerDesktop is still being instantiated
        //if state if being restored.
        viewerDesktopPane.add(viewerWindow);

        setStatus("ViewerWindow loaded: " + viewerWindow.getTitle());
        viewerWindow.setVisible(true);

        logger.info("Waiting 2 seconds for ViewerWindow to become visible before scrolling to initial position...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            logger.info("Interrupted while waiting 2 seconds for ViewerWindow to become visbile, initial scroll position may not be set properly.");
        }

        viewerWindow.scrollToViewPosition();

        lastActiveViewerWindow = (ViewerWindow) viewerWindow;
        return viewerWindow;
    }

    /**
     *
     * @param node
     * @param progressDialog progressDialog ProgressDialog with isCancellable() == false
     * @return
     */
    ViewerWindow openNewQiViewerWindow(Node node, ProgressDialog progressDialog) {
        ViewerWindow viewerWindow =
                ViewerWindowFactory.restoreFromXml(XML_FORMAT.QIVIEWER, node, agent, agent.getDefaultWindowSyncGroup(),
                progressDialog);
        if (progressDialog.isCancelable()) {
            throw new IllegalArgumentException("ProgressDialog must not be cancellable for use by restoreFromXml()");
        }

        agent.addViewerWindow(viewerWindow);
        //Agent cannot invoke desktopPane.add() because the ViewerDesktop is still being instantiated
        //if state if being restored.
        viewerDesktopPane.add(viewerWindow);

        setStatus("ViewerWindow loaded: " + viewerWindow.getTitle());
        viewerWindow.setVisible(true);
        lastActiveViewerWindow = (ViewerWindow) viewerWindow;

        return (ViewerWindow) viewerWindow;
    }

    /**
     * Opens a new ViewerWindow, including a single layer built using the given
     * DatasetProperties, DataObject array and layerProperties.
     * 
     * @param dsProperties the GeoIO properties of the first layer
     * @param traces the DataObject array used to generate the layer plot
     * @param layerProps the graphical, synchronization and subset properties of the layer
     * 
     * @return the new ViewerWindow
     */
    public ViewerWindow openNewViewerWindow(DatasetProperties dsProperties, DataObject[] traces,
            LayerDisplayProperties layerProps) {
        ViewerWindow viewerWindow = ViewerWindowFactory.createViewerWindow(dsProperties, traces, layerProps, agent);
        viewerDesktopPane.add(viewerWindow);
        setStatus("1 out of 1 seismic layers loaded");
        viewerWindow.setVisible(true);
        lastActiveViewerWindow = (ViewerWindow) viewerWindow;

        viewerWindow.waitForGeoPlotEvent(10000, GeoPlotEvent.RENDERING_COMPLETE);

        return (ViewerWindow) viewerWindow;
    }

    /**
     * Opens a new ViewerWindow, including a single layer built using the given
     * DatasetProperties, BhpSuHorizonDataObject array and layerProperties.
     * 
     * @param dsProperties the GeoIO properties of the first layer
     * @param horizons the BhpSuHorizonDataObject array used to generate the layer plot
     * @param layerProps the graphical, synchronization and subset properties of the layer
     * 
     * @return the new ViewerWindow
     */
    public ViewerWindow openNewViewerWindow(DatasetProperties dsProperties, BhpSuHorizonDataObject[] horizons,
            LayerDisplayProperties layerProps) {
        ViewerWindow viewerWindow = ViewerWindowFactory.createViewerWindow(dsProperties, horizons, layerProps, agent);
        viewerDesktopPane.add(viewerWindow);
        setStatus("1 out of 1 horizon layers loaded");
        viewerWindow.setVisible(true);
        lastActiveViewerWindow = (ViewerWindow) viewerWindow;

        viewerWindow.waitForGeoPlotEvent(10000, GeoPlotEvent.RENDERING_COMPLETE);

        return (ViewerWindow) viewerWindow;
    }

    public void setStatus(String status) {
        statusLabel.setText(status);
    }

    public void updateLocation(final int x, final int y) {
        Runnable locationSetter = new Runnable() {

            public void run() {
                setLocation(new Point(x, y));
            }
        };
        SwingUtilities.invokeLater(locationSetter);
    }

    /**
     * Update the preferred size of the JScrollPane containing the viewer's MDIDesktopPane.
     * The minimum size is reset to 0,0 and the maximu size to Integer.MAX_VALUE for both axes.
     * 
     * @param scrollPaneWidth The preferred width of the central MDIDesktopPane
     * @param scrollPaneHeight The preferred height of the central MDIDesktopPane
     */
    public void updateSize(final int scrollPaneWidth, final int scrollPaneHeight) {
        Runnable sizeSetter = new Runnable() {

            public void run() {
                int widthPadding = 0;
                int heightPadding = 0;
                if (viewerDesktopScrollPane != null) {
                    widthPadding = getSize().width - viewerDesktopScrollPane.getSize().width;
                    heightPadding = getSize().height - viewerDesktopScrollPane.getSize().height;
                }
                int desktopWidth = scrollPaneWidth + widthPadding;
                int desktopHeight = scrollPaneHeight + heightPadding;
                Dimension d = new Dimension(desktopWidth, desktopHeight);
                setSize(d);
            }
        };
        SwingUtilities.invokeLater(sizeSetter);
    }

    public void setActiveWindow(ViewerWindow window) {
        lastActiveViewerWindow = window;
    }

    private void showSelectWindowFirstMessage() {
        JOptionPane.showMessageDialog(this, "Please select a qiViewer Map or X-Sec window first.");
    }

    private void zoomIn() {
        if (lastActiveViewerWindow == null) {
            logger.info("Unable to zoom in - no viewer window is active");
            showSelectWindowFirstMessage();
        } else {
            lastActiveViewerWindow.zoomIn();
        }
    }

    private void zoomOut() {
        if (lastActiveViewerWindow == null) {
            logger.info("Unable to zoom out - no viewer window is active");
            showSelectWindowFirstMessage();
        } else {
            lastActiveViewerWindow.zoomOut();
        }
    }

    private void zoomAll() {
        if (lastActiveViewerWindow == null) {
            logger.info("Unable to zoom to entire image - no viewer window is active");
            showSelectWindowFirstMessage();
        } else {
            lastActiveViewerWindow.zoomAll();
        }
    }

    public void delete(ViewerWindow viewerWindow) {
        if (lastActiveViewerWindow == viewerWindow) {
            viewerWindow = null;
        }
    }

    public void processEditorResult(GeoGraphicsPropertyGroup geoGraphicsPropertyGroup) {
        if (geoGraphicsPropertyGroup instanceof HorizonHighlightSettings) {
            horizonHighlightSettings = new HorizonHighlightSettings((HorizonHighlightSettings) geoGraphicsPropertyGroup);
        } else if (geoGraphicsPropertyGroup instanceof CursorSettings) {
            cursorSettings = new CursorSettings((CursorSettings) geoGraphicsPropertyGroup);
        }
        for (String winID : agent.getValidWindowIDs()) {
            ViewerWindow viewerWindow = agent.getValidWindow(winID);
            viewerWindow.processEditorResult(geoGraphicsPropertyGroup);
        }
    }
}