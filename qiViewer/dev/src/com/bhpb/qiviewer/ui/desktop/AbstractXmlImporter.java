/*
###########################################################################
# This program module Copyright (C) 2008-2009 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.qiviewer.util.ProgressDialog;
import java.awt.Dimension;

public abstract class AbstractXmlImporter {
    private static final Dimension DEFAULT_PROGRESS_DLG_SIZE = new Dimension(500, 170);
    private static final int DEFAULT_PROGRSES_DLG_X_LOC = 200;
    private static final int DEFAULT_PROGRESS_DLG_Y_LOC = 100;

    protected ProgressDialog initProgressDialog(String title, int nWindows, int[] nLayers) {
        ProgressDialog dialog = new ProgressDialog(title, false);
        //dialog.initProgressDialog(numWins, numLayers);
        dialog.initProgressDialog(nWindows, nLayers);
        dialog.setSize(DEFAULT_PROGRESS_DLG_SIZE);
        dialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
        dialog.setLocation(DEFAULT_PROGRSES_DLG_X_LOC, DEFAULT_PROGRESS_DLG_Y_LOC);
        dialog.setVisible(true);

        return dialog;
    }
}
