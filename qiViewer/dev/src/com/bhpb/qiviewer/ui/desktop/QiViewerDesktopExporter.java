/**
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.qiviewer.ui.window.ViewerWindow;
import java.awt.Component;
import java.util.logging.Logger;

/**
 * Implementation ViewerDesktopExporter for QiWorkbench XML format.
 * @author folsw9
 */
public class QiViewerDesktopExporter implements ViewerDesktopExporter {

    private ViewerDesktop viewerDesktop;
    private static final Logger logger =
            Logger.getLogger(QiViewerDesktopExporter.class.getName());

    /**
     * Constructs a QiViewerXMLExporter for the ViewerDesktop.
     *
     * @param viewerDesktop ViewerDesktop for which state XML will be returned
     * in String form by invocation of exportState().
     */
    public QiViewerDesktopExporter(ViewerDesktop viewerDesktop) {
        this.viewerDesktop = viewerDesktop;
    }

    /**
     * Restores the state of the ViewerDesktop
     * beginning at the com...ViewerDesktop node.
     *
     * The first child element is a QiProjectDescriptor.  As this is not
     * actually part of the ViewerDesktop GUI's state, this is likely to
     * change.
     *
     */
    public String exportState() throws Exception {
        String QUOTE_AND_SPACE = "\" ";
        StringBuffer content = new StringBuffer();
        String xmlAlias = ViewerDesktop.getXmlAlias();
        content.append("<" + xmlAlias + " ");

        //save position in workbench canvas
        content.append("height=\"" + viewerDesktop.getHeight() + QUOTE_AND_SPACE);
        content.append("width=\"" + viewerDesktop.getWidth() + QUOTE_AND_SPACE);
        content.append("x=\"" + viewerDesktop.getX() + QUOTE_AND_SPACE);
        content.append("y=\"" + viewerDesktop.getY() + QUOTE_AND_SPACE);
        content.append(">\n");

        //save the instance-wide settings: picking etc.
        try {
            content.append(viewerDesktop.pickingSettings.saveState());
        } catch (Exception ex) {
            logger.warning("Unable to save PickingSettings due to: " + ex.getMessage());
            throw ex;
        }

        try {
            content.append(viewerDesktop.cursorSettings.saveState());
        } catch (Exception ex) {
            logger.warning("Unable to save CursorSettings due to: " + ex.getMessage());
            throw ex;
        }

        try {
            content.append(viewerDesktop.horizonHighlightSettings.saveState());
        } catch (Exception ex) {
            logger.warning("Unable to save HorizonHighlightSettings due to: " + ex.getMessage());
            throw ex;
        }

        //save the ViewerWindows
        int nComponents = viewerDesktop.viewerDesktopPane.getComponentCount();
        logger.info("ViewerDesktop has " + nComponents + "ViewerWindows");
        for (int i = 0; i < nComponents; i++) {
            Component comp = viewerDesktop.viewerDesktopPane.getComponent(i);
            if (comp instanceof ViewerWindow) {
                logger.info("Component #" + i + " is a ViewerWindow, saving state...");
                ViewerWindow asWindow = null;
                try {
                    asWindow = ((ViewerWindow) comp);
                    String stateString = asWindow.saveState();
                    content.append(stateString);
                } catch (Exception ex) {
                    String winID;
                    if (asWindow == null) {
                        winID = "NULL ViewerWindow";
                    } else {
                        winID = asWindow.getWinID();
                    }
                    logger.warning("Unable to save state for viewerWindow '" + winID + "' due to: " + ex.getMessage());
                    throw ex;
                }
            } else {
                logger.info("ViewerDesktopPane child Component is not a ViewerWindow, skipping: " + comp.getClass().toString());
            }
        }

        content.append("</" + xmlAlias + ">\n");

        return content.toString();
    }
}