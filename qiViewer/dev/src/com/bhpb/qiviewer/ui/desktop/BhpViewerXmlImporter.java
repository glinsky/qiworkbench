/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.CursorSettings.CURSOR_STYLE;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.layer.display.HorizonXsecDisplayParameters.LINE_STYLE;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.qiviewer.util.ProgressDialog;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Implementation ViewerDesktopImporter for BhpViewer XML format.
 * @author folsw9
 */
public class BhpViewerXmlImporter extends AbstractXmlImporter implements ViewerDesktopImporter {

    private static final Logger logger =
            Logger.getLogger(BhpViewerXmlImporter.class.getName());
    private ViewerDesktop viewerDesktop;

    /**
     * Constructs a BhpViewerXMLImporter for the ViewerDesktop.
     *
     * @param viewerDesktop ViewerDesktop for which state will be set by
     * call to importState().
     */
    public BhpViewerXmlImporter(ViewerDesktop viewerDesktop) {
        this.viewerDesktop = viewerDesktop;
    }

    /**
     * Import bhpViewer state saved in a XML file. Use existing viewer window.
     * The preferred display name is not changed. Therefore, the name in the
     * viewer window's title and component tree node don't have to change.
     * Before creating new viewer windows, all the old ones will be closed and
     * removed.
     *
     * @param viewerAgentNode XML Node containing saved bhpViewer state variables. node is a
     * {@literal <component>} node.
     */
    public void importState(Node viewerAgentNode) {
        //Close all the current internal windows first
        viewerDesktop.closeAllViewerWindows();

        //This is dangerous but is taken directly from the bhpViewer code.
        //There is only 1 child node, namely, the BhpViewerBase node
        Node bhpViewerBaseNode = viewerAgentNode.getChildNodes().item(0);

        //START of code from BhpViewerBase::initWithXML()
        int width = ElementAttributeReader.getInt(bhpViewerBaseNode, "width");
        int height = ElementAttributeReader.getInt(bhpViewerBaseNode, "height");
        int hlLineStyle = ElementAttributeReader.getInt(bhpViewerBaseNode, "highlightLS");
        float hlLineWidth = ElementAttributeReader.getFloat(bhpViewerBaseNode, "highlightLW");

        //ElementAttributeReader.getColor() is not used to allow more granular error reporting while parsing String
        //also, the value of having the above method silently return Color.orange in the case of an Exception is dubious.
        String hlLineColor = ElementAttributeReader.getString(bhpViewerBaseNode, "highlightLC");
        int csLineStyle = ElementAttributeReader.getInt(bhpViewerBaseNode, "cursorLS");

        //ElementAttributeReader.getColor() is not used to allow more granular error reporting while parsing String
        //also, the value of having the above method silently return Color.orange in the case of an Exception is dubious.
        String csLineColor = ElementAttributeReader.getString(bhpViewerBaseNode, "cursorLC");
        float csLineWidth = ElementAttributeReader.getFloat(bhpViewerBaseNode, "cursorLW");
        double csWidth = ElementAttributeReader.getDouble(bhpViewerBaseNode, "cursorW");
        double csHeight = ElementAttributeReader.getDouble(bhpViewerBaseNode, "cursorH");

        try {
            viewerDesktop.setSize(width, height);
            viewerDesktop.validate();
        } catch (Exception ex) {
            logger.warning("Unable to resize qiViewer window due to: " + ex.getMessage() + ". Existing size retained");
        }

        initHorizonHighlightSettings(hlLineStyle, hlLineWidth, hlLineColor);
        initCursorSettings(csLineStyle, csLineColor, csLineWidth, csWidth, csHeight);

        List<Node> windowNodes = parseWindowNodes(bhpViewerBaseNode);

        int nWindows = windowNodes.size();
        int[] nLayers = getNumLayersByWindow(windowNodes, nWindows);

        ProgressDialog progressDlg = initProgressDialog("Restoring qiViewer from bhpViewer XML", nWindows, nLayers);

        for (Node windowNode : windowNodes) {
            if (progressDlg.isCancelled()) {
                break;
            }
            viewerDesktop.openNewBhpViewerWindow(windowNode, progressDlg);
        }
        progressDlg.dispose();
    }

    private void initCursorSettings(int csLineStyle, String csLineColorRGBAString, float csLineWidth, double csWidth, double csHeight) {
        CursorSettings cursorSettings = viewerDesktop.getCursorSettings();

        logger.info("Ignoring unused unconfigurable attribute csLineStyle: " + csLineStyle);
        logger.info("Ignoring unused unconfigurable attribute csLineWidth: " + csLineWidth);

        if (csWidth == csHeight) {
            if (csWidth == 10.0) {
                cursorSettings.setCursorStyle(CURSOR_STYLE.SM_CROSS);
            } else if (csWidth == 20.0) {
                cursorSettings.setCursorStyle(CURSOR_STYLE.LG_CROSS);
            } else if (csWidth == 10000.0) {
                cursorSettings.setCursorStyle(CURSOR_STYLE.CROSSHAIR);
            } else if (csWidth == 0.0) {
                cursorSettings.setCursorStyle(CURSOR_STYLE.NO_CURSOR);
            } else {
                logger.warning("Invalid equal values of csWidth, csHeight (" +
                        csWidth + ") - cursor style will default to: " + cursorSettings.getCursorStyle());
            }
        } else {
            logger.warning("Invalid unequal values of csWidth, csHeight - cursor style will default to: " + cursorSettings.getCursorStyle());
        }

        try {
            String[] rgba = csLineColorRGBAString.trim().split("\\s");
            if (rgba.length != 4) {
                logger.warning("Cannot parse '" + csLineColorRGBAString + "' as RGB color - it must contain 4 integers separated by whitespace.");
                return;
            }
            int r = Integer.parseInt(rgba[0]);
            int g = Integer.parseInt(rgba[1]);
            int b = Integer.parseInt(rgba[2]);
            Color lineColor = new Color(r, g, b);
            cursorSettings.setColor(lineColor);
        } catch (Exception ex) {
            logger.warning("Cursor default color unchanged; caught exception while parsing '" + csLineColorRGBAString + "' as RGB Color: " + ex.getMessage());
        }

    }

    private void initHorizonHighlightSettings(int lineStyle, float lineWidth, String lineColorRGBAstring) {
        HorizonHighlightSettings horizonHighlightSettings = viewerDesktop.getHorizonHighlightSettings();

        final LINE_STYLE[] LINE_STYLES = {
            LINE_STYLE.SOLID,
            LINE_STYLE.EMPTY,
            LINE_STYLE.DASH,
            LINE_STYLE.DOT,
            LINE_STYLE.DASH_DOT,
            LINE_STYLE.DASH_DOT_DOT
        };

        if (lineStyle < 1 || lineStyle > 6) {
            logger.warning("Invalid horizon highlight line style #" + lineStyle + "; Using default style SOLID.");
        } else {
            horizonHighlightSettings.setLineStyle(LINE_STYLES[lineStyle - 1]);
        }
        horizonHighlightSettings.setLineWidth((int) lineWidth);

        //trim leading/trailing whitespace then split on whitespace
        if (lineColorRGBAstring == null) {
            logger.warning("Cannot set horizon highlight line color - lineColorRGBAstring attribute is null.");
            return;
        }
        try {
            String[] rgba = lineColorRGBAstring.trim().split("\\s");
            if (rgba.length != 4) {
                logger.warning("Cannot parse '" + lineColorRGBAstring + "' as RGBA color - it must contain 4 integers separated by whitespace.");
                return;
            }
            int r = Integer.parseInt(rgba[0]);
            int g = Integer.parseInt(rgba[1]);
            int b = Integer.parseInt(rgba[2]);
            int a = Integer.parseInt(rgba[3]);
            Color lineColor = new Color(r, g, b, a);
            horizonHighlightSettings.setLineColor(lineColor);
            horizonHighlightSettings.setColorOpacityPercent((int) Math.round(a * 100.0f / 255.0f));
        } catch (Exception ex) {
            logger.warning("HorizonHighlight default color & opacity unchanged; caught exception while parsing '" + lineColorRGBAstring + "' as RGBA Color: " + ex.getMessage());
        }
    }

    private int[] getNumLayersByWindow(List<Node> windowNodes, int nWindows) {
        if (nWindows != windowNodes.size()) {
            throw new IllegalArgumentException("Number of requested layer counts != number of window nodes");
        }

        int[] nLayersByWindow = new int[nWindows];

        int windowIndex = 0;
        for (Node windowNode : windowNodes) {
            int nLayers = 0;
            NodeList children = windowNode.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                String childName = children.item(i).getNodeName();
                if (childName.startsWith("Bhp") && childName.endsWith("Layer")) {
                    nLayers++;
                }
            }
            nLayersByWindow[windowIndex] = nLayers;
            windowIndex++;
        }

        return nLayersByWindow;
    }

    private List<Node> parseWindowNodes(Node bhpViewerBaseNode) {
        NodeList children = bhpViewerBaseNode.getChildNodes();
        List<Node> windowNodes = new ArrayList<Node>();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                String nodeName = child.getNodeName();
                if (nodeName.equals("MenuState")) {
                    boolean showToolbar = ElementAttributeReader.getBoolean(child, "ShowToolbar", true);
                    boolean showStatusbar = ElementAttributeReader.getBoolean(child, "ShowStatusbar", true);
                } else if (nodeName.equals("BhpWindow")) {
                    //Create/open an internal window restored with the state of
                    //a bhpViewer internal window
                    windowNodes.add(child);
                }
            }
        }
        return windowNodes;
    }
}