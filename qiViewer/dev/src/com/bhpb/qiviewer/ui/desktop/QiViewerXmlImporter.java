/**
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.ui.desktop;

import com.bhpb.geographics.model.CursorSettings;
import com.bhpb.geographics.model.HorizonHighlightSettings;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.PickingSettings;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiviewer.util.ProgressDialog;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.bhpb.qiworkbench.compAPI.XmlUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation ViewerDesktopImporter for QiWorkbench XML format.
 * @author folsw9
 */
public class QiViewerXmlImporter extends AbstractXmlImporter implements ViewerDesktopImporter {

    private static final Logger logger =
            Logger.getLogger(QiViewerXmlImporter.class.getName());

    private ViewerDesktop viewerDesktop;

    /**
     * Constructs a QiViewerXMLImporter for the ViewerDesktop.
     *
     * @param viewerDesktop ViewerDesktop for which state will be set by
     * call to importState().
     */
    public QiViewerXmlImporter(ViewerDesktop viewerDesktop) {
        this.viewerDesktop = viewerDesktop;
    }

    /**
     * Restores the state of the ViewerDesktop
     * beginning at the com...ViewerDesktop node.
     *
     * The first child element is a QiProjectDescriptor.  As this is not
     * actually part of the ViewerDesktop GUI's state, this is likely to
     * change.
     *
     * @param node XML tree node denoting the GUI element.
     */
    public void importState(Node node) {
        int height = ElementAttributeReader.getInt(node, "height");
        int width = ElementAttributeReader.getInt(node, "width");
        int x = ElementAttributeReader.getInt(node, "x");
        int y = ElementAttributeReader.getInt(node, "y");

        //restore the instance-wide settings: picking etc.
        Node pickingNode = XmlUtils.getChild(node, PickingSettings.getXmlAlias());
        if (pickingNode != null) {
            viewerDesktop.pickingSettings = PickingSettings.restoreState(pickingNode);
        } else {
            logger.warning("Unable to find node with name matching: " + PickingSettings.getXmlAlias() + "; PickingSettings will remain at defaults.");
        }

        //restore the instance-wide settings: picking etc.
        Node cursorNode = XmlUtils.getChild(node, CursorSettings.getXmlAlias());
        if (cursorNode != null) {
            viewerDesktop.cursorSettings = CursorSettings.restoreState(cursorNode);
        } else {
            logger.warning("Unable to find node with name matching: " + CursorSettings.getXmlAlias() + "; CursorSettings will remain at defaults.");
        }

        Node HorizonHighlightNode = XmlUtils.getChild(node, HorizonHighlightSettings.getXmlAlias());
        if (HorizonHighlightNode != null) {
            viewerDesktop.horizonHighlightSettings = HorizonHighlightSettings.restoreState(HorizonHighlightNode);
        } else {
            logger.warning("Unable to find node with name matching: " + HorizonHighlightSettings.getXmlAlias() + "; HorizonHighlightSettings will remain at defaults.");
        }

        logger.info("ViewerDesktop.restoreState width: " + width + ", height: " + height);
        viewerDesktop.setSize(width, height);
        viewerDesktop.setLocation(x, y);
        logger.info("ViewerDesktop attributes set, deserializing any ViewerWindows...");
        NodeList children = node.getChildNodes();
        logger.info("Node: " + node.getNodeName() + " has " + children.getLength() + " child nodes");

        List<Node> windowNodes = new ArrayList<Node>();

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            String childName = child.getNodeName();
            //valid ViewerWindow names include the class name and windowID
            if (childName.equals(ViewerWindow.getXmlAlias())) {
                windowNodes.add(child);
            } else if (!"#text".equals(childName)) {
                logger.info("Child node not recognized: " + childName);
            }
        }

        //Currently, the windowChildren must be parsed here to determine the total number of layers,
        //which must be set initially for the ProgressDialog.
        //Ideally, the progressDialog should consist of two separate bars - # of Windows loaded and # of layers within the current Window

        int nWindows = windowNodes.size();

        int[] nLayersByWindow = new int[nWindows];
        int currentWindowIndex = 0;
        for (Node windowNode : windowNodes) {
            NodeList windowChildren = windowNode.getChildNodes();
            int nLayers = 0;
            for (int i = 0; i < windowChildren.getLength(); i++) {
            Node potentialLayerNode = windowChildren.item(i);
            String nodeName = potentialLayerNode.getNodeName();
            //valid ViewerWindow names include the class name and windowID
            if ( nodeName.equals(HorizonLayer.getXmlAlias()) ||
                 nodeName.equals(ModelLayer.getXmlAlias()) ||
                 nodeName.equals(SeismicLayer.getXmlAlias())    ) {
                nLayers++;
            }
            }
            nLayersByWindow[currentWindowIndex] = nLayers;
            currentWindowIndex++;
        }

        ProgressDialog progressDlg = initProgressDialog("Restoring qiViewer", nWindows, nLayersByWindow);

        for (Node windowNode : windowNodes) {
            if (progressDlg.isCancelled()) {
                break;
            }
            viewerDesktop.openNewQiViewerWindow(windowNode, progressDlg);
        }
        progressDlg.dispose();
    }
}