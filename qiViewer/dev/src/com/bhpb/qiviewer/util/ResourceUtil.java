/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */

package com.bhpb.qiviewer.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Logger;

/**
 * Convenience class for getting the contents of a resource file as a String.
 */
public class ResourceUtil {
    private static Logger logger = Logger.getLogger(ResourceUtil.class.toString());
    private static final int RESOURCE_SLEEP_TIME = 100; // if resource IO is blocked, wait 1/10 second
    
    /**
     * Return the contents of the resource as a String using the platform's default
     * character set.  If an IOException occurs while reading the resource, then
     * the exception message is returned instead.  If the Thread is interrupted while waiting on blocked
     * InputStream to have available bytes, an error message is returned.
     *
     * Warning: This implementation may block indefinitely when calling read().
     *
     * @param classLoader Class used to get the Resource
     * @param resource the name of the resource to read
     * @throws IllegalArgumentException if resource does not exist
     *
     * @return String containing the contents of the resource using the platform's default character set
     */
    public static String getResourceAsString(ClassLoader classLoader, String resource) {
        logger.info("classLoader: " + classLoader.toString());
        logger.info("resource: " + resource);
        InputStream copyrightInStream = classLoader.getResourceAsStream(resource);
        
        if (copyrightInStream == null) {
            URL[] urls = ((URLClassLoader) classLoader).getURLs();
            logger.finest("Failed to create an InputStream for " + resource + " at any of the following URLs:");
            for (URL url : urls) {
                logger.finest(url.toString());
            }
            throw new IllegalArgumentException("Cannot load specified resource, it does not exist: " + resource);
        }
        
        String resourceString = null;
        try {
            resourceString = getInputStreamAsString(copyrightInStream);
        } catch (IOException ioe) {
            resourceString = "<html><body>IOException occurred while getting " + resource + ": " + ioe.getMessage() + "</body></html>";
        } finally {
            try {
                copyrightInStream.close();
            } catch (IOException ioe) {
               logger.warning("IOException occurred while closing InputStream for " + resource+ ": " + ioe.getMessage());
            }
        }
        return resourceString;
    }
    
    static String getInputStreamAsString(InputStream inputStream) throws IOException {
        
        if (inputStream == null) {
            throw new IllegalArgumentException("Cannot read from null inputStream");
        }
                
        StringBuffer stringBuffer = new StringBuffer();
        
        int bytesRead = 0;
        byte[] byteBuffer;
        do {
            int availableBytes = inputStream.available();
            if (availableBytes > 0) {
                byteBuffer = new byte[availableBytes];
                bytesRead = inputStream.read(byteBuffer);
                logger.fine("Read " + bytesRead + " bytes from InputStream");
                String byteBufferString = new String(byteBuffer);
                stringBuffer.append(byteBufferString);
            } else {
                logger.info("InputStream.available() returned 0, sleeping for "  + RESOURCE_SLEEP_TIME + " ms...");
                
                try {
                    Thread.sleep(RESOURCE_SLEEP_TIME);
                } catch (InterruptedException ie) {
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Interrupted while waiting on available bytes from InputStream.");
                    break;
                }
                
                int nextByte = inputStream.read();
                
                if (nextByte == -1) // expected case, no bytes were available because the end of file was reached
                {
                    logger.info("End of resource file reached.");
                    bytesRead = -1;
                } else { //end of file was reached
                    logger.info("Read one byte from resource");
                    byteBuffer = new byte[1];
                    
                    //This integer was returned from InputStream.read() and is not -1, there is no loss of precision
                    byteBuffer[0] = (byte) nextByte;
                    stringBuffer.append(new String(byteBuffer));
                }
            }
        } while (bytesRead != -1); // until the end of file is reached
        return stringBuffer.toString();
    }
}