/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.util;

import com.bhpb.qiviewer.ui.util.ExceptionBrowser;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.awt.Component;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;

/**
 * ErrorDialog displays Exception properties for an individual Exception.
 * Used by ErrorBrowser. 
 *
 * @author folsw9
 * @version bhp2Dviewer release 2
 */
public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static final Logger logger = Logger.getLogger(UncaughtExceptionHandler.class.toString());
    
    private boolean printStackTraces=false;
    private Document exceptionDocument = new DefaultStyledDocument();
    private ExceptionBrowser exceptionBrowser = new ExceptionBrowser(this);
    private List<Exception> allExceptions = new ArrayList<Exception>();
    
    public UncaughtExceptionHandler(boolean printStackTraces) {
        this.printStackTraces = printStackTraces;
        logger.fine("UncaughtExceptionHandler created with printStackTraces=" + printStackTraces);
    }

    /**
     * Adds the {@link Exception} to the {@link ExceptionBrowser}, optionally
     * prompting the user to view the browser to close the warning.
     * 
     * @param exception Exception which was not caught within the ViewerAgent's
     * <code>Thread</code> or child <code>Thread</code>.
     *
     * @param promptUser If true, a JDialog will be displayed prompting the user to choose
     * whether to view the <code>ExceptionBrowser</code>.
     */
    public void handleException(Exception exception, boolean promptUser) {
        logger.info("An uncaught exception was handled without a specified dialogParent: " + exception.getMessage());
        
        boolean showExceptionBrowser = true;
        
        if (printStackTraces) {
            exception.printStackTrace();
        }
        
        addException(exception);
        
        if (promptUser) {
            showExceptionBrowser = getBooleanInput("An exception has occurred! Click OK to view the details or Cancel to continue.");
        }
        
        if (showExceptionBrowser) {
            exceptionBrowser.setVisible(true);
        }
    }
    
    /**
     * Adds the exception's Message to this handler's Document.  If an exception
     * occurs while adding to the Document, it is logged to prevent the possibility
     * of an infinite loop due to uncaught Exceptions.
     *
     * @param exception The <code>Exception</code> to be added
     */
    private void addException(Exception exception) {
        allExceptions.add(exception);
        try {
            AttributeSet attribs = new SimpleAttributeSet();
            exceptionDocument.insertString(exceptionDocument.getLength(), 
                    "Exception #" 
                    + allExceptions.size() 
                    + " : " + exception.getClass().toString() 
                    + "\n", attribs);
            
            exceptionDocument.insertString(exceptionDocument.getLength(), 
                    "Message: " 
                    + ExceptionMessageFormatter.getFormattedMessage(exception) 
                    + "\n", attribs);
            
            exceptionDocument.insertString(exceptionDocument.getLength(), 
                    "=========================================================\n", 
                    attribs);
        } catch (BadLocationException ble) {
            logger.warning("Cannot create exception text document in UncaughtExceptionHandler.addExcetpion: " + ble.getMessage());
        }
    }
    
    private boolean getBooleanInput(String dialogMessage) {
        final String[] options = {"OK", "Cancel"};
        
        int selection = JOptionPane.showOptionDialog((Component)null, dialogMessage, "Warning", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        
        if (selection == JOptionPane.OK_OPTION) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Handles a {@link Throwable} which was not caught within the scope of the {@link Thread}
     * with which this {@link UncaughtExceptionHandler} is associated.
     *
     * @param t Thread from <code>uncaughtThrowable</code> was thrown
     * @param uncaughtThrowable <code>Throwable</code> which was not caught within the <run>
     * method of <code>t</code>.  If an instance of Exception, it is handled by this <code>UncaughtExceptionHandler</code>,
     * otherwise, a message is logged indicating that an uncaught Throwable (Error) has occurred.
     */
    public void uncaughtException(Thread t, Throwable uncaughtThrowable) {
        if (uncaughtThrowable instanceof Exception)
            handleException((Exception)uncaughtThrowable, true);
        else {
            if (printStackTraces) {
                uncaughtThrowable.printStackTrace();
            }
            logger.severe("Skipping uncaught Throwable (Error): " + uncaughtThrowable.getMessage());
        }
    }
    
    public Document getExceptionDocument() {
        return exceptionDocument;
    }
    
    public int getNumHandledExceptions() {
        return allExceptions.size();
    }
}