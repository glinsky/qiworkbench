/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.adapter.geoio;

import com.bhpb.geographics.controller.AbstractEditorPanelController.SELECTION;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

/**
 * The result of a user's interaction with the TransactionalEditor dialog.
 * Provides methods to determine whether the user acceptd or cancelled the
 * interaction and the final state of the underlying model, in this case,
 * DatasetProperties;
 */
public class GeoIOTransactionResult {

    private boolean interactive = false;
    private final SELECTION selection;
    private DatasetProperties dsProperties = null;
    private LayerDisplayProperties layerDisplayProperties = null;
    private String errorMsg = "";
    private final DataObject[] dataObjects;
    private String winID = "";
    private String layerID = "";

    /**
     * Factory method for creation of TransactionResult when the user
     * elected to cancel an operation.
     * 
     * @return GeoIOTransactionResult for which isCancelled() returns true.
     */
    public static GeoIOTransactionResult createUserCancelledResult() {
        return new GeoIOTransactionResult(SELECTION.CANCEL);
    }

    /*
     * Factory method for creation of successful transaction result.
     *
     * These changes would allow this code to be reused for non-geoIO
     * transactions, including those which do not allow user input.
     * Currently the Successful condition is equated with the selection
     * value OK, even if there is no UI.
     */
    /**
     * Create a GeoIOTransactionResult, for which isAccepted() returns true,
     * containg the given DatasetProperties, LayerDisplayProperties, DataObject,
     * GUI window ID and GUI layer ID.
     * 
     * @param dsProperties DatasetProperties containing the parameters of the geoIO read
     * @param layerProperties LayerDisplayProperties containing the parameters of the GUI layer
     * @param dataObjects DataObjects returned by geoIO
     * @param winID unique ID of the GUI window containing the new layer
     * @param layerID unique ID of the GUI layer displaying the dataObjects
     * 
     * @return GeoIOTransactionResult encapsulating all parameters of this method
     */
    public static GeoIOTransactionResult createSuccessfulResult(DatasetProperties dsProperties,
            LayerDisplayProperties layerProperties, DataObject[] dataObjects, String winID,
            String layerID) {
        return new GeoIOTransactionResult(SELECTION.OK, dsProperties, layerProperties, dataObjects, winID, layerID);
    }

    /**
     * Factory method for creation of TransactionResult when an error has occured.
     * Note that this will result in a SELECTION of NONE, regardless of whether
     * the user actually selected OK prior to the error.
     *
     * @param errorMsg description of the error encountered during the transaction.
     * If this parameter is null, then getErrorMessage() will return the String
     * 'null' when invoked on the GeoIOTransactionResult created by this method.
     * 
     * @return GeoIOTransaction encapsulating the errorMsg and for which
     * isError() returns true.
     */
    public static GeoIOTransactionResult createErrorResult(String errorMsg, boolean showMsgDlg) {
        if (errorMsg == null) {
            return new GeoIOTransactionResult("null", new DataObject[0], showMsgDlg);
        } else {
            return new GeoIOTransactionResult(errorMsg, new DataObject[0], showMsgDlg);
        }
    }

    private GeoIOTransactionResult(SELECTION selection) {
        this.selection = selection;
        this.dataObjects = new DataObject[0];
        this.winID = "-1";
        this.errorMsg = "";
        this.layerID = "-1";
    }

    private GeoIOTransactionResult(String errorMsg, DataObject[] dataObjects, boolean interactive) {
        this.errorMsg = errorMsg;
        this.dataObjects = dataObjects;
        this.winID = "-1";
        this.selection = SELECTION.NONE;
        this.layerID = "-1";
        this.interactive = interactive;
    }

    private GeoIOTransactionResult(SELECTION selection,
            DatasetProperties dsProperties, LayerDisplayProperties layerProperties,
            DataObject[] dataObjects, String winID,
            String layerID) {
        this.selection = selection;
        this.dataObjects = dataObjects;
        this.dsProperties = dsProperties;
        this.layerDisplayProperties = layerProperties;
        this.winID = winID;
        this.errorMsg = "";
        this.layerID = layerID;
    }

    public DatasetProperties getDatasetProperties() {
        return dsProperties;
    }

    public LayerDisplayParameters getLayerDisplayParameters() {
        return layerDisplayProperties.getLayerDisplayParameters();
    }

    public DataObject[] getDataObjects() {
        return dataObjects.clone();
    }

    /**
     * Return a non-null, non-empty error Message if and only if isErrorCondition()
     * is true.
     * 
     * @return A String describing the error.
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    public String getLayerID() {
        return layerID;
    }

    public String getWindowID() {
        return winID;
    }

    public LayerDisplayProperties getLayerDisplayProperties() {
        return layerDisplayProperties;
    }

    public boolean isAccepted() {
        return selection.equals(SELECTION.OK);
    }

    public boolean isCancelled() {
        return selection.equals(SELECTION.CANCEL);
    }

    public boolean isInteractive() {
        return interactive;
    }

    /**
     * Return true if this GeoIOTransactionResult represents an error condition.
     * 
     * @return true if and only if this GeoIOTransactionResult's errroMsg is non-null
     * and not the empty String.
     */
    public boolean isErrorCondition() {
        return errorMsg != null && !errorMsg.equals("");
    }
}