/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.adapter.geoio;

import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.dataset.HorizonPropertiesInheritor;
import com.bhpb.geographics.model.dataset.ModelPropertiesInheritor;
import com.bhpb.geographics.model.dataset.SeismicPropertiesInheritor;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.ui.layer.LayerPropertiesDialog;
import com.bhpb.geographics.ui.layer.PropertiesEditorFactory;
import com.bhpb.geoio.datasystems.DataObject;
import com.bhpb.geoio.datasystems.BhpSuHorizonDataObject;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.controller.desktop.NewXsecWindowActionListener;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.FileChooserDescriptor;
import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import java.awt.Component;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;
import com.bhpb.geoio.util.GeoIOAdapter;
import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Handles creation and display of a FileChooser, initialized in such a way that
 * user interaction with the resulting FileChooser GUI may cause a request, 
 * e.g. OPEN_WINDOW to be send from the FileChooserService to the 
 * QiComponent associated with this FileChooserDirector.  
 * 
 * Modeled after the Mediator design pattern [GoF].
 */
public class GeoIODirector {

    private static final Logger logger = Logger.getLogger(GeoIODirector.class.getName());
    private final GeoIOAdapter geoIOadapter;
    private final ViewerAgent agent;
    /** File chooser requester who registered themselves. Keyed on ID of message 
     * containing the user's selection. */
    private DatasetProperties datasetProperties = null;
    private HashMap<String, FileChooserDescriptor> fileChooserRegistry = new HashMap<String, FileChooserDescriptor>();

    public GeoIODirector(ViewerAgent agent) {
        this.agent = agent;
        this.geoIOadapter = new GeoIOAdapter(agent.getMessagingManager());
    }

    /**
     * Invoke a file chooser for reading BHP-SU.  Adapted from <code>ComponentStateUtils.callFileChooser()</code>.
     * This method returns nothing, rather, the FileChooserService it invokes may
     * send an OPEN_WINDOW command to the ViewerAgent, depending on the
     * the user's interaction with the FileChooser GUI.
     *
     * @param parent qiComponent GUI taking the import/export action.
     * @param dir Directory to start browsing from.
     * @param action Action to take, namely, import or export.
     * @param extension Only files with matching extensions will be displayed.
     * @param description Description of the extension (format) of visible files.
     */
    public void callFileChooser(Component parent,
            String dir,
            String action,
            String extension,
            String description,
            String fileChooserTitle) {
        ArrayList<Object> list = new ArrayList<Object>();
        //true is Export, false if Import.
        boolean isExport = action.equals(QIWConstants.EXPORT_COMP_STATE) ? true : false;

        //1st element the parent GUI object
        list.add(parent);

        //2nd element is the dialog title
        list.add(fileChooserTitle);

        //3rd element is a list that contains current directory to start with
        //and a flag (yes or no) indicating if an already remembered directory
        //should be used instead
        ArrayList<String> lst = new ArrayList<String>();
        lst.add(dir);
        lst.add("no");
        list.add(lst);

        //4th element is the file filter
        list.add(createFileFilter(extension, description));

        //5th element is the navigation flag
        list.add(false);

        //6th element is the producer's/requester's component descriptor
        list.add(agent.getComponentDescriptor());

        //7th element is the type of file chooser: either Open or Save
        String chooserType = isExport ? QIWConstants.FILE_CHOOSER_TYPE_SAVE : QIWConstants.FILE_CHOOSER_TYPE_OPEN;
        list.add(chooserType);

        //8th element is the action request for this service: either Import or Export
        list.add(action);

        //9th element is the default pretext for the file name if user fails to pretext it. If no pretext is required then use empty String
        String userName = ""; //System.getProperty("user.name");

        list.add(userName);

        //10th element is the default extension for the file name if user fails to append. If no extension is required then use empty String
        if ((extension != null) && (!("".equals(extension)))) {
            list.add(extension);
        } else {
            list.add("");
        }

        //11th element is the target Tomcat URL where the file chooser chooses
        list.add(agent.getMessagingManager().getTomcatURL());

        showFileChooser(list, NewXsecWindowActionListener.class.getName(), this);
    }

    /**
     * Create the default DatasetProperties for the given metadata, using
     * inherited properties from the current active layer where appropriate as
     * definied in the Inheritor classes.
     *
     * @param metadata
     *
     * @return
     */
    private DatasetProperties createDatasetProperties(GeoFileMetadata metadata) {
        ViewerWindow activeWindow = agent.getActiveWindow();
        Layer activeLayer = null;
        if (activeWindow != null) {
            activeLayer = activeWindow.getActiveLayer();
        }
        DatasetProperties activeDatasetProperties = null;
        if (activeLayer != null) {
            activeDatasetProperties = activeLayer.getDatasetProperties();
        }

        if (metadata.isSeismicFile()) {
            ArrayList horizons = ((SeismicFileMetadata) metadata).getHorizons();

            if (horizons != null && horizons.size() > 0) {
                return new HorizonPropertiesInheritor((SeismicFileMetadata) metadata).inheritFrom(activeDatasetProperties);
            } else {
                return new SeismicPropertiesInheritor((SeismicFileMetadata) metadata).inheritFrom(activeDatasetProperties);
            }
        } else if (metadata.isModelFile()) {
            return new ModelPropertiesInheritor((ModelFileMetadata) metadata).inheritFrom(activeDatasetProperties);
        } else {
            throw new IllegalArgumentException("metadata is not one of: seismic, model, horizon");
        }
    }

    static private FileFilter createFileFilter(String extension, String description) {
        String[] xmlExtensions = new String[]{extension.toLowerCase(), extension.toUpperCase()};
        FileFilter xmlFilter = (FileFilter) new GenericFileFilter(xmlExtensions, description + " (*." + xmlExtensions[0] + ", " + xmlExtensions[1] + ")");
        return xmlFilter;
    }

    private GeoIOTransactionResult createHorizonLayer(BhpSuHorizonDataObject[] horizons, DatasetProperties dsProps,
            LayerDisplayProperties layerProps, String action, boolean interactive) {
        if (horizons != null && horizons.length > 0) {
            ViewerWindow window = null;
            Layer layer = null;
            String errorMsg = "No error has occured";
            try {
                if (QIWConstants.OPEN_WINDOW.equals(action)) {
                    window = agent.openNewViewerWindow(dsProps,
                            horizons, layerProps);
                } else if (QIWConstants.ADD_LAYER.equals(action)) {
                    window = agent.getActiveWindow();
                    if (window == null) {
                        errorMsg = "Unable to add seismic layer because current ViewerWindow could not be determined.";
                    }
                } else {
                    GeoIOTransactionResult.createErrorResult("Unable to add seismic layer because action: " + action + " is unknown.", interactive);
                }
            } catch (Exception ex) {
                errorMsg = ex.getMessage();
            }

            String winID;

            if (window != null) {
                winID = window.getWinID();
                agent.setWindowID(window, winID);
            } else {
                return GeoIOTransactionResult.createErrorResult("Unable to create viewer window due to: " +
                        errorMsg, interactive);
            }

            try {
                if (QIWConstants.OPEN_WINDOW.equals(action)) {
                    layer = window.getActiveLayer();
                } else if (QIWConstants.ADD_LAYER.equals(action)) {
                    layer = window.addLayer(dsProps, horizons, layerProps);
                } else {
                    GeoIOTransactionResult.createErrorResult("Unable to instantiate layer for unknown action: " + action, interactive);
                }
            } catch (Exception ex) {
                errorMsg = ex.getMessage();
                agent.getUncaughtExceptionHandler().handleException(ex, true);
            }

            String layerID;
            if (layer != null) {
                layerID = layer.getLayerID();
                agent.setLayerID(layer, layerID);
                agent.setLayerWindopMap(layer, window);
            } else {
                return GeoIOTransactionResult.createErrorResult("Unable to create viewer layer due to: " +
                        errorMsg, interactive);
            }
            return GeoIOTransactionResult.createSuccessfulResult(dsProps, layerProps, horizons, winID, layerID);
        } else {
            logger.info("Data loading was initiated but no horizons were read.  Returning transaction error result.");
            return GeoIOTransactionResult.createErrorResult("No horizons were read - either data loading was cancelled or an error occured.", interactive);
        }
    }

    private GeoIOTransactionResult createSeismicLayer(DataObject[] traces, DatasetProperties dsProps,
            LayerDisplayProperties layerProps, String action, boolean interactive) {
        if (traces != null && traces.length > 0) {
            ViewerWindow window = null;
            Layer layer = null;
            String errorMsg = "No error occurred.";
            try {
                if (QIWConstants.OPEN_WINDOW.equals(action)) {
                    window = agent.openNewViewerWindow(dsProps,
                            traces, layerProps);
                } else if (QIWConstants.ADD_LAYER.equals(action)) {
                    window = agent.getActiveWindow();
                    if (window == null) {
                        errorMsg = "Unable to add seismic layer because current ViewerWindow could not be determined.";
                    }
                } else {
                    GeoIOTransactionResult.createErrorResult("Unable to add seismic layer because action: " + action + " is unknown.", interactive);
                }
            } catch (Exception ex) {
                errorMsg = ex.getMessage();
            }

            String winID;

            if (window != null) {
                winID = window.getWinID();
                agent.setWindowID(window, winID);
            } else {
                return GeoIOTransactionResult.createErrorResult(errorMsg, interactive);
            }

            try {
                if (QIWConstants.OPEN_WINDOW.equals(action)) {
                    layer = window.getActiveLayer();
                } else if (QIWConstants.ADD_LAYER.equals(action)) {
                    layer = window.addLayer(dsProps, traces, layerProps);
                } else {
                    GeoIOTransactionResult.createErrorResult("Unable to instantiate layer for unknown action: " + action, interactive);
                }
            } catch (Exception ex) {
                errorMsg = ex.getMessage();
                agent.getUncaughtExceptionHandler().handleException(ex, true);
            }

            String layerID;
            if (layer != null) {
                layerID = layer.getLayerID();
                agent.setLayerID(layer, layerID);
                agent.setLayerWindopMap(layer, window);
            } else {
                return GeoIOTransactionResult.createErrorResult("Unable to create viewer layer due to: " +
                        errorMsg, interactive);
            }

            return GeoIOTransactionResult.createSuccessfulResult(dsProps, layerProps, traces, winID, layerID);
        } else {
            logger.info("Data loading was initiated but no traces were read.  Returning transaction error result.");
            return GeoIOTransactionResult.createErrorResult("No traces were read - either data loading was cancelled or an error occured.", interactive);
        }
    }

    /**
     * @return DatasetProperties associated with the most recent issueGeoIORead command, if any,
     * or null otherwise.
     */
    public DatasetProperties getDatasetProperties() {
        return datasetProperties;
    }

    /**
     * Get the descriptor for a registered file chooser request based on its message ID.
     *
     * @param msgID ID of the file chooser response (same as the request)
     * @return File chooser descriptor if request registered; otherwise, null.
     */
    public FileChooserDescriptor getFileChooserDesc(String msgID) {
        if (!fileChooserRegistry.containsKey(msgID)) {
            return null;
        }

        return fileChooserRegistry.get(msgID);
    }

    public GeoIOAdapter getGeoIOAdapter() {
        return geoIOadapter;
    }

    /**
     * Check if file chooser request registered. Need to make this check since
     * registration occurs after the request is sent to the file chooser.
     * Note: Sending the request returns the message ID needed for registration.
     *
     * @param msgID ID of response (same as request)
     * @param tries The number of times to try.
     * @return true if registered; otherwise, false
     */
    public boolean isFileChooserRequestRegistered(String msgID, int tries) {
        while (tries > 0) {
            if (fileChooserRegistry.containsKey(msgID)) {
                return true;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ie) {
            }
            tries--;
        }

        return false;
    }

    /**
     * Wrapper method (possibly unnecessary) to issueGeoIOread(filePath)
     * which checks to ensure that the response received originated
     * from a registered fileChooser request.
     * 
     * @param responseToGetFileChooserResult QiWorkbenchMsg containing the file
     * chosen by the user if it is a Normal response.
     * @param filePath The name of the file selected by the user.
     * @param action The action requested by the user: open or add a cross-section
     * or map.
     * @param showMessages if false, MesageBoxes will not be displayed for error conditions
     * @param readSummary if false, the summary will not be read.  Used by ControlSessionManager.
     *
     * @return GeoIOTransactionResult encapsulating the user's final choice, as
     * well as associated data if accepted.
     */
    public GeoIOTransactionResult issueGeoIOread(IQiWorkbenchMsg responseToGetFileChooserResult,
            String filePath, String action, boolean showMessages, boolean readSummary) {
        //Note: message ID of response same as request's
        String msgID = MsgUtils.getMsgID(responseToGetFileChooserResult);
        //make sure file chooser request registered

        if (!isFileChooserRequestRegistered(msgID, 3)) {
            String errorMsg = "Cannot find registered file chooser request. msgID=" + msgID;
            logger.severe("INTERNAL ERROR: " + errorMsg);
            return GeoIOTransactionResult.createErrorResult(errorMsg, showMessages);
        }

        LayerProperties dsPropsEditorResult = readLayerProperties(filePath, true);
        if (dsPropsEditorResult.isCancelled()) {
            return GeoIOTransactionResult.createUserCancelledResult();
        } else if (dsPropsEditorResult.isErrorCondition()) {
            return GeoIOTransactionResult.createErrorResult(dsPropsEditorResult.getErrorMsg(), showMessages);
        } else if (dsPropsEditorResult.isApplied()) {
            String errMsg = "The user should not be able to click Apply when initially editing dataset properties.";
            return GeoIOTransactionResult.createErrorResult(errMsg, showMessages);
        }
        return readData(dsPropsEditorResult, action, showMessages, readSummary);
    }

    /**
     * Issue a geoIO read command based on the LayerProperties and then invoke
     * the ViewerAgent GUI using the associated action String.
     *
     * @param layerPropsEditorResult The result of the getLayerProperties() invocation,
     * which contains the DatasetProperties unless the user cancelled editing or an error
     * occurred.
     *
     * @param action The action passed to the GUI which determines how the traces are
     * displayed: OPEN_WINDOW or ADD_LAYER.
     * @param showMessages if false, MesageBoxes will not be displayed for error conditions
     * @param readSummary if false, the summary will not be read.  Used by ControlSessionManager.
     *
     * @return GeoIOTransactionResult containing a unique layer ID if successful,
     * or a String describing the error condition if a problem occurred in either
     * issuing the geoIO read command or displaying the resulting data.
     */
    public GeoIOTransactionResult readData(LayerProperties layerPropsEditorResult,
            String action, boolean showMessages, boolean readSummary) {

        DatasetProperties dsProps = layerPropsEditorResult.getDatasetProperties();

        //Horizon data is a special case because multiple layers may be read
        if (dsProps.isHorizon()) {
            return readHorizonData(layerPropsEditorResult, action, showMessages, readSummary);
        }

        LayerDisplayProperties layerProps = layerPropsEditorResult.getLayerDisplayProperties();
        if (layerPropsEditorResult.isAccepted()) {

            JDialog dlg = null;
            if (showMessages) {
                dlg = startConcurrentDlg(agent.getViewerDesktop());
            }

            if (readSummary) {
                String summaryRequestError = geoIOadapter.readSummary(dsProps);
                if ("".equals(summaryRequestError) == false) {
                    return GeoIOTransactionResult.createErrorResult(summaryRequestError, showMessages);
                }
            }

            if (showMessages && dlg != null) {
                dlg.dispose();
            }

            //if this is the initial read, then layer properties defaults cannot be
            //determined until the summary is read, which cannot be done until the user has accepted the
            //editor transaction
            if (layerProps == LayerDisplayProperties.NULL) {
                logger.info("Initial edit, creating default LayerProperties.");
                ViewerWindow activeWindow = agent.getActiveWindow();
                Layer activeLayer = null;
                if (activeWindow != null) {
                    activeLayer = activeWindow.getActiveLayer();
                }
                layerProps = LayerDisplayProperties.createDefaultLayerProperties(dsProps, activeLayer);
            } else {
                logger.info("Editing existing datasetProperties; creation of new default LayerDisplayProperties skipped.");
            }

            if (dsProps.isSeismic()) {
                DataObject[] traces = geoIOadapter.getTraces(dsProps);
                logger.info("Read: " + traces.length + " traces");
                return createSeismicLayer(traces, dsProps, layerProps, action, showMessages);
            } else if (dsProps.isModel()) {
                DataObject[] modelTraces = geoIOadapter.getTraces(dsProps);
                logger.info("Read: " + modelTraces.length + " traces");
                return createSeismicLayer(modelTraces, dsProps, layerProps, action, showMessages);
            } else {
                return GeoIOTransactionResult.createErrorResult("Unable to process properties of type: " + dsProps.getClass().getName(), showMessages);
            }
        } else if (layerPropsEditorResult.isCancelled()) {
            return GeoIOTransactionResult.createUserCancelledResult();
        //bug fix for misleading error message when resposne to READ_TRACE_SUMMARY command is abnormal,
        //return the error message. The next else {} block after this most likely indicates an
        //error in the LayerPropsEditorResult itself
        } else if (layerPropsEditorResult.isErrorCondition()) {
            return GeoIOTransactionResult.createErrorResult(layerPropsEditorResult.getErrorMsg(), showMessages);
        } else {
            return GeoIOTransactionResult.createErrorResult("No user selection was available after waitForSelection.  The controller was interrupted while waiting or an error occurred.", showMessages);
        }
    }

    /**
     * Reading horizon data is a special case - multiple layers, and hence multiple summaries and sets of LayerDisplayProperties
     * are created if more than one horizon is selected from the DatasetPropertiesPanel.
     *
     * If the input layerPropsEditorResult contains exactly 1 selected horizon, then the
     * summary field of the datasetProperties which it contains will be modified.  This is a kludge
     * to fix a NPE which occurs because the ControlSessionManager depends on this parameter being modified,
     * and does not utilize the copy returned in this method's GeoIOTransactionResult.
     *
     * @param layerPropsEditorResult
     * @param action
     * @param showMessages
     * @param readSummary
     * @return
     */
    private GeoIOTransactionResult readHorizonData(LayerProperties layerPropsEditorResult,
            String action, boolean showMessages, boolean readSummary) {
        DatasetProperties dsProps = layerPropsEditorResult.getDatasetProperties();
        LayerDisplayProperties layerProps = layerPropsEditorResult.getLayerDisplayProperties();
        if (layerPropsEditorResult.isAccepted()) {
            String selectedHorizonPackedString = ((HorizonProperties) dsProps).getSelectedHorizon();
            String[] selectedHorizons = selectedHorizonPackedString.split("[;]");

            GeoIOTransactionResult layerCreationResult = GeoIOTransactionResult.createErrorResult(
                    "No valid horizon names could be parsed from selected horizons: '" + selectedHorizonPackedString + ".", showMessages);

            StringBuilder layerIDs = new StringBuilder();

            String currentHorizonAction = action;
            boolean initialLayer = true;
            for (String selectedHorizon : selectedHorizons) {
                logger.info("Loading layer for selected horizon: " + selectedHorizon);
                HorizonProperties dsPropsCopy = new HorizonProperties((HorizonProperties) dsProps);
                dsPropsCopy.setSelectedHorizon(selectedHorizon);

                JDialog dlg = null;
                if (showMessages) {
                    dlg = startConcurrentDlg(agent.getViewerDesktop());
                }

                if (readSummary) {
                    String summaryRequestError = geoIOadapter.readSummary(dsPropsCopy);
                    if (selectedHorizons.length == 1) {
                        dsProps.setSummary(dsPropsCopy.getSummary());
                    }
                    if ("".equals(summaryRequestError) == false) {
                        return GeoIOTransactionResult.createErrorResult(summaryRequestError, showMessages);
                    }
                }

                if (showMessages && dlg != null) {
                    dlg.dispose();
                }

                //if this is the initial read, then layer properties defaults cannot be
                //determined until the summary is read, which cannot be done until the user has accepted the
                //editor transaction
                if (layerProps == LayerDisplayProperties.NULL) {
                    logger.info("Initial edit, creating default LayerProperties.");
                    ViewerWindow activeWindow = agent.getActiveWindow();
                    Layer activeLayer = null;
                    if (activeWindow != null) {
                        activeLayer = activeWindow.getActiveLayer();
                    }
                    layerProps = LayerDisplayProperties.createDefaultLayerProperties(dsPropsCopy, activeLayer);
                } else {
                    logger.info("Editing existing datasetProperties; creation of new default LayerDisplayProperties skipped.");
                }

                BhpSuHorizonDataObject[] horizons = geoIOadapter.getHorizons(dsPropsCopy);
                logger.info("Read: " + horizons.length + "horizons (some may be empty if GEO_READ errors occured).");

                layerCreationResult = createHorizonLayer(horizons, dsPropsCopy, layerProps, currentHorizonAction, showMessages);
                //after the first horizon has been loaded, the action is changed from OPEN_WINDOW to ADD_LAYER
                if (initialLayer) {
                    currentHorizonAction = QIWConstants.ADD_LAYER;
                    initialLayer = false;
                }
                if (layerCreationResult.isErrorCondition()) {
                    return layerCreationResult;
                } else {
                    layerIDs.append(layerCreationResult.getLayerID());
                    layerIDs.append(" ");
                }
            }

            String formattedLayerIDs = layerIDs.toString().trim().replace(' ', ';');
            return GeoIOTransactionResult.createSuccessfulResult(
                    layerCreationResult.getDatasetProperties(),
                    layerCreationResult.getLayerDisplayProperties(),
                    layerCreationResult.getDataObjects(),
                    layerCreationResult.getWindowID(),
                    formattedLayerIDs);

        } else if (layerPropsEditorResult.isCancelled()) {
            return GeoIOTransactionResult.createUserCancelledResult();
        //bug fix for misleading error message when resposne to READ_TRACE_SUMMARY command is abnormal,
        //return the error message. The next else {} block after this most likely indicates an
        //error in the LayerPropsEditorResult itself
        } else if (layerPropsEditorResult.isErrorCondition()) {
            return GeoIOTransactionResult.createErrorResult(layerPropsEditorResult.getErrorMsg(), showMessages);
        } else {
            return GeoIOTransactionResult.createErrorResult("No user selection was available after waitForSelection.  The controller was interrupted while waiting or an error occurred.", showMessages);
        }
    }

    /**
     * Issue a geoIOread command, displaying a transactional editor for the
     * resulting metadata and then opening a qiViewer window if the user clicks
     * OK in the datasetProperties editor dialog. Update - this method now does
     * what the documentation claims and actually opens the ViewerWindow before
     * returning.  This was needed in order to have a window ID to return in the
     * case of programmatic invocation.
     *
     * <ol>
     * <li>Get the metadata using geoIO.</li>
     * <li>Display the datasetproperties editor dialog.</li>
     * <li>Based on the resulting interaction with the edtior, display a qiViewer
     * or simply return.</li>
     * </ol>
     *
     * @param filePath Path to file which will be read using GeoIO.
     *
     * @return GeoIOTransactionResult representing the user's selection as well
     * as associated data, if accepted.
     */
    public LayerProperties readLayerProperties(String filePath) {
        return readLayerProperties(filePath, false);
    }

    /**
     * Issue a geoIOread command, displaying a transactional editor for the
     * resulting metadata and then opening a qiViewer window if the user clicks
     * OK in the datasetProperties editor dialog. Update - this method now does
     * what the documentation claims and actually opens the ViewerWindow before
     * returning.  This was needed in order to have a window ID to return in the
     * case of programmatic invocation.
     *
     * <ol>
     * <li>Get the metadata using geoIO.</li>
     * <li>Display the datasetproperties editor dialog.</li>
     * <li>Based on the resulting interaction with the edtior, display a qiViewer
     * or simply return.</li>
     * </ol>
     *
     * @param filePath Path to file which will be read using GeoIO.
     * @param interactive If true, the DatasetPropertiesDialog is dispalyed, otherwise
     * the resulting LayerProperties will contain the default DatasetProperties and
     * its isAccepted() method will return true.
     *
     * @return GeoIOTransactionResult representing the user's selection as well
     * as associated data, if accepted.
     */
    public LayerProperties readLayerProperties(String filePath, boolean interactive) {
        GeoFileMetadata metadata = geoIOadapter.readMetadata(filePath);

        if (metadata != null) {
            logger.fine("METADATA for " + filePath);
            logger.fine(metadata.toString());

            datasetProperties = createDatasetProperties(metadata);

            logger.info("Skipping initial geoIOadapter.readSummary()");

            LayerDisplayProperties layerProps = LayerDisplayProperties.NULL;
            LayerProperties result;
            LayerPropertiesDialog layerPropertiesDlg = null;

            //Cast metadata and create properties, properties editor dialog for one of 3 types:
            //seismic, horizon or model .dat file.
            if (metadata.isSeismicFile()) {
                ArrayList horizons = ((SeismicFileMetadata) metadata).getHorizons();
                if (horizons != null && horizons.size() > 0) {
                    //datasetProperties = new HorizonPropertiesInheritor((SeismicFileMetadata) metadata);
                    if (interactive) {
                        //no mutable horizon exists yet, mutableHorizon parameter set to null
                        //horizon value table will be empty
                        layerPropertiesDlg = PropertiesEditorFactory.createHorizonPropertiesDialog(
                                (JFrame) SwingUtilities.getRoot(agent.getViewerDesktop()),
                                (HorizonProperties) datasetProperties, layerProps, geoIOadapter, null);
                    }
                } else {
                    //datasetProperties = new SeismicPropertiesInheritor((SeismicFileMetadata) metadata);
                    if (interactive) {
                        layerPropertiesDlg = PropertiesEditorFactory.createSeismicPropertiesDialog(
                                (JFrame) SwingUtilities.getRoot(agent.getViewerDesktop()),
                                (SeismicProperties) datasetProperties, layerProps);
                    }
                }
            } else if (metadata.isModelFile()) {
                if (interactive) {
                    layerPropertiesDlg = PropertiesEditorFactory.createModelPropertiesDialog(
                            (JFrame) SwingUtilities.getRoot(agent.getViewerDesktop()),
                            (ModelProperties) datasetProperties, layerProps);
                }
            } else {
                return LayerProperties.createErrorResult("Unknown metadata type: " + metadata.getClass().getName());
            }

            if (interactive) {
                layerPropertiesDlg.setVisible(true);
                //bug fix - apply should be disabled when initially opening LayerPropertiesDialog
                layerPropertiesDlg.setApplyEnabled(false);
                layerPropertiesDlg.pack();
                result = layerPropertiesDlg.getTransactionResult();
            } else {
                //mock a succsesful UI result with the initial dataset and layer properties
                result = LayerProperties.createUserAcceptedResult(
                        datasetProperties,
                        layerProps);
            }
            return result;
        } else {
            String errMsg = "Unable to get metadata for: " + filePath;
            logger.warning(errMsg);
            return LayerProperties.createErrorResult(errMsg);
        }
    }

    /**
     * Register the request for a file chooser's result so the agent can invoke
     * the file chooser action. Usually performed when the request is sent.
     * <p>
     * The file chooser's descriptor is kept in a hashmap keyed on the ID of the
     * response.
     *
     * @param desc File chooser descriptor for the requester.
     * @return Empty string if successfully registerd or null if the file chooser
     * request is already registered.
     */
    synchronized public String registerFileChooserRequest(FileChooserDescriptor desc) {
        // check if response is already registered
        if (fileChooserRegistry.containsKey(desc.getMsgID())) {
            return null;
        }

        fileChooserRegistry.put(desc.getMsgID(), desc);

        logger.config("registered:" + desc);

        return "";
    }

    /*
     * Get a file chooser service, invoke a file chooser, request the user's
     * selection and register the request so the selector action is processed
     * by the requester.
     * @param list File chooser input parameters
     * @param requesterType Data type of the requester
     * @param requesterInstance Instance of the requester
     */
    public void showFileChooser(ArrayList paramList, String requesterType, Object requesterInstance) {
        IMessagingManager msgMgr = agent.getMessagingManager();

        String msgID = msgMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD, true);
        IQiWorkbenchMsg response = msgMgr.getMatchingResponseWait(msgID, 10000);

        if (response == null) {
            logger.warning("Internal error occurring in getting file chooser service returning null");
        } else if (response.isAbnormalStatus()) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + response.getContent());
        } else {
            ComponentDescriptor cd = (ComponentDescriptor) response.getContent();

            msgMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD, cd, QIWConstants.ARRAYLIST_TYPE, paramList);

            msgID = msgMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD, cd);

            FileChooserDescriptor desc = new FileChooserDescriptor(msgID, requesterType, requesterInstance);
            registerFileChooserRequest(desc);
        }
    }

    private JDialog startConcurrentDlg(JComponent parent) {
        JPanel jPanel = new JPanel();
        jPanel.add(new JLabel("Loading dataset summary, please wait"));
        JFrame rootFrame = (JFrame) SwingUtilities.getRoot(parent);
        final JDialog dlg = new JDialog(rootFrame, "Loading", true);

        Point frameLoc = rootFrame.getLocation();
        int frameWidth = rootFrame.getWidth();
        int frameHeight = rootFrame.getHeight();

        dlg.add(jPanel);
        dlg.pack();
        dlg.setLocation(new Point(
                frameLoc.x + frameWidth / 2 - dlg.getWidth() / 2,
                frameLoc.y + frameHeight / 2 - dlg.getHeight() / 2));

        Thread dlgWorker = new Thread(new Runnable() {

            public void run() {
                dlg.setVisible(true);
            }
        });
        dlgWorker.start();
        return dlg;
    }

    /**
     * Unregister a registered file chooser request. Usually performed when the file chooser action is invoked.
     *
     * @param desc Descriptor of the registered file chooser request.
     */
    synchronized public void unregisterFileChooserRequest(FileChooserDescriptor desc) {
        logger.config("unregistering:" + desc);
        fileChooserRegistry.remove(desc.getMsgID());
    }

    public void writeHorizon(HorizonLayer horizonLayer) {
        if (horizonLayer.getDatasetProperties().getMetadata().getGeoDataOrder() == GeoDataOrder.CROSS_SECTION_ORDER) {
            HorizonProperties horizDatasetProps = (HorizonProperties) horizonLayer.getDatasetProperties();
            String errMsg = geoIOadapter.writeXsecHorizon(horizDatasetProps.getSelectedHorizon(),
                    horizDatasetProps, horizonLayer.getMutableHorizon());
            if (!"".equals(errMsg)) {
                JOptionPane.showMessageDialog(agent.getViewerDesktop(), errMsg);
            }
        } else {
            logger.info("Only cross-section horizons may be written.  writeHorizon call ignored.");
        }
    }
}