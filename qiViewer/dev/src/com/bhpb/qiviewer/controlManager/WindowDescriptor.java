/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiviewer.controlManager;

import java.util.logging.Logger;

/**
 * Descriptor of a qiViewer's internal window, e.g., XSection, map
 */
public class WindowDescriptor {
    private static final Logger logger = Logger.getLogger(WindowDescriptor.class.toString());

    /** Window ID */
    String winid = "";

    /** Window type */
    String winType = "";

    /** Window's position within the qiViewer */
    int xcord = 0, ycord = 0;

    /** Window's size */
    int width = 0, height = 0;

    public WindowDescriptor(String winid, String winType) {
        this.winid = winid;
        this.winType = winType;
    }

    //GETTERS
    public String getWinID() {
        return winid;
    }

    public String getType() {
        return winType;
    }

    public int getXcord() {
        return xcord;
    }
    public int getYcord() {
        return ycord;
    }

    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }

    //SETTERS
    public void setWinID(String winid) {
        this.winid = winid;
    }

    public void setType(String winType) {
        this.winType = winType;
    }

    public void setPosition(int xcord, int ycord) {
        this.xcord = xcord;
        this.ycord = ycord;
    }
    public void setXcord(int xcord) {
        this.xcord = xcord;
    }
    public void setYcord(int ycord) {
        this.ycord = ycord;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    /** Display attributes of an internal window */
    @Override
    public String toString() {
        StringBuffer buf  = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n Window ID=");
        buf.append(winid);
        buf.append(", Window type=");
        buf.append(winType);
        buf.append(", position=(");
        buf.append(xcord+",");
        buf.append(ycord+")");
        buf.append(", size=");
        buf.append(width+"x");
        buf.append(height);

        return buf.toString();
    }
}
