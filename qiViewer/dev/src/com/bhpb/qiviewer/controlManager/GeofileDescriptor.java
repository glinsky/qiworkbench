/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.controlManager;

import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import java.util.logging.Logger;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;

/**
 * Descriptor of a qiViewer's geoFile, e.g., seismic, horizon
 */
public class GeofileDescriptor {

    private static final Logger logger = Logger.getLogger(GeofileDescriptor.class.toString());
    /** geoFile ID */
    String geofileID = "";
    /** geoFile's format */
    String geoFormat = "";
    /** Full path of geoFile */
    String pathname = "";
    /** geoFile's data properties */
    DatasetProperties geofileProperties;
    /** layer properties */
    SeismicXsecDisplayParameters layerProperties;

    public GeofileDescriptor(String geofileID, String geoFormat, String pathname) {
        this.geofileID = geofileID;
        this.geoFormat = geoFormat;
        this.pathname = pathname;
    }

    //GETTERS
    public String getGeofileID() {
        return geofileID;
    }

    public String getFormat() {
        return geoFormat;
    }

    public String getPath() {
        return pathname;
    }

    public SeismicXsecDisplayParameters getLayerProperties() {
        return layerProperties;
    }

    public DatasetProperties getGeofileProperties() {
        return geofileProperties;
    }

    //SETTERS
    public void setGeofileID(String geofileID) {
        this.geofileID = geofileID;
    }

    public void setFormat(String geoFormat) {
        this.geoFormat = geoFormat;
    }

    public void setPath(String pathname) {
        this.pathname = pathname;
    }

    public void setLayerProperties(SeismicXsecDisplayParameters layerProperties) {
        this.layerProperties = layerProperties;
    }

    public void setGeofileProperties(DatasetProperties geofileProperties) {
        this.geofileProperties = geofileProperties;
    }

    /** Display attributes of a geoFile */
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n geoFile ID=");
        buf.append(geofileID);
        buf.append(", geoFormat=");
        buf.append(geoFormat);
        buf.append(", file path=");
        buf.append(pathname);

        return buf.toString();
    }
}
