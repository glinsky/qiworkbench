/*
###########################################################################
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.controlManager;

import com.bhpb.geographics.model.layer.display.SeismicXsecDisplayParameters;
import java.util.logging.Logger;


/**
 * Descriptor of a qiViewer's layer
 */
public class LayerDescriptor {

    private static final Logger logger = Logger.getLogger(LayerDescriptor.class.toString());
    /** ID of window containing layer */
    String winID;
    /** layer ID */
    String layerID = "";
    /** layer's format */
    String geoFormat = "";
    /** layer properties */
    SeismicXsecDisplayParameters layerProperties;

    public LayerDescriptor(String layerID, String geoFormat, String winID) {
        this.layerID = layerID;
        this.geoFormat = geoFormat;
        this.winID = winID;
    }

    //GETTERS
    public String getWinID() {
        return winID;
    }

    public String getLayerID() {
        return layerID;
    }

    public String getFormat() {
        return geoFormat;
    }

    public SeismicXsecDisplayParameters getLayerProperties() {
        return layerProperties;
    }

    //SETTERS
    public void setWinID(String winID) {
        this.winID = winID;
    }

    public void setLayerID(String layerID) {
        this.layerID = layerID;
    }

    public void setFormat(String geoFormat) {
        this.geoFormat = geoFormat;
    }

    public void setLayerProperties(SeismicXsecDisplayParameters layerProperties) {
        this.layerProperties = layerProperties;
    }

    /** Display attributes of a geoFile */
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append(this.getClass().toString());
        buf.append(": \n layer ID=");
        buf.append(layerID);
        buf.append(", geoFormat=");
        buf.append(geoFormat);
        buf.append(", window ID=");
        buf.append(winID);

        return buf.toString();
    }
}