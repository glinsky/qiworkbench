/*
###########################################################################
# qiViewer - a 2D seismic viewer
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/

package com.bhpb.qiviewer.controlManager;

/**
 * Constants used by the Control Manager. Constants used in the qualified names
 * of layer and window properties and preferences.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class CMConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private CMConstants() {}

    public static final String QUALIFIED_DELIMITER = "[.]";

    // SEISMIC LAYER, DATASET FIELDS
    public static final String DATASET_BINSIZE_FIELD = "Binsize";
    public static final String DATASET_CHOSEN_RANGE_FIELD = "ChosenRange";
    public static final String DATASET_FULL_RANGE_FIELD = "FullRange";
	public static final String DATASET_INCR_FIELD = "Increment";
	public static final String DATASET_INTERP_FIELD = "Interpolation";
	public static final String DATASET_KEY_FIELD = "Key";
	public static final String DATASET_OFFSET_FIELD = "Offset";
	public static final String DATASET_ORDER_FIELD = "Order";
	public static final String DATASET_SYNC_FIELD = "Synchronize";
	// HORIZON LAYER, DATASET FIELD
    public static final String DATASET_HORIZON_SETTING_FIELD = "HorizonSetting";
    // HORIZON SETTING SUBFIELDS
    public static final String DISPLAY_HORIZON_SETTING_EVENT_FIELD = "Event";
    public static final String DISPLAY_HORIZON_SETTING_SYNC_FIELD = "Synchronize";

    // SEISMIC LAYER, DISPLAY FIELDS
    public static final String DISPLAY_AUTO_GAIN_CONTROL_FIELD = "AutoGainControl";
	public static final String DISPLAY_CLIPPING_FACTOR_FIELD = "Clipping_Factor";
	public static final String DISPLAY_COLOR_MAP_FIELD = "Color_Map";
	public static final String DISPLAY_DECIMATION_SPACING_FIELD = "Decimation_Spacing";
	public static final String DISPLAY_DISPLAY_ATTRIBUTE = "DisplayAttribute";
    public static final String DISPLAY_NORMALIZATION_FIELD = "Normalization";
	public static final String DISPLAY_OPACITY_FIELD = "Opacity";
	public static final String DISPLAY_RASTERIZING_TYPE_FIELD = "RasterizingType";
    public static final String DISPLAY_SCALE_FIELD = "Scale";
    //    AUTO GAIN CONTROL SUBFIELDS
    public static final String DISPLAY_AGC_APPLY_FIELD = "Apply";
    public static final String DISPLAY_AGC_WINDOW_LEN_FIELD = "WindowLength";

    //    NORMALIZATION SUBFIELDS
	public static final String DISPLAY_NORM_MAX_FIELD = "Max_Value";
	public static final String DISPLAY_NORM_MIN_FIELD = "Min_Value";
    public static final String DISPLAY_NORM_SCALE_FIELD = "Scale";
    public static final String DISPLAY_NORM_TYPE_FIELD = "Type";
    //    RASTERIZING SUBFIELDS
	public static final String DISPLAY_RAST_INTERP_DENSITY_FIELD = "Interpolated_Density";
	public static final String DISPLAY_RAST_NEGATIVE_FILL_FIELD = "Negative_Fill";
	public static final String DISPLAY_RAST_POSITIVE_FILL_FIELD = "Positive_Fill";
	public static final String DISPLAY_RAST_VAR_DENSITY_FIELD = "Variable_Density";
	public static final String DISPLAY_RAST_WIGGLE_TRACE_FIELD = "Wiggle_Trace";
    //    SCALE SUBFIELDS
    public static final String DISPLAY_HORIZ_SCALE_MUL_FIELD = "HorizontalScaleMultiplier";
    public static final String DISPLAY_VERT_SCALE_MUL_FIELD = "VerticalScaleMultiplier";
    
    // HORIZON LAYER, DISPLAY FIELDS, CROSS SECTION VIEW ORDER
    public static final String DISPLAY_LAYER_FIELD = "Layer";
    public static final String DISPLAY_LAYER_NAME_FIELD = "LayerName";
    public static final String DISPLAY_LINE_SETTINGS_FIELD = "LineSettings";
    public static final String DISPLAY_LINE_SETTINGS_DRAW_LINE_FIELD = "DrawLine";
    public static final String DISPLAY_LINE_SETTINGS_LINE_STYLE_FIELD = "LineStyle";
    public static final String DISPLAY_LINE_SETTINGS_LINE_WIDTH_FIELD = "LineWidth";
    public static final String DISPLAY_LINE_SETTINGS_LINE_COLOR_FIELD = "LineColor";
    public static final String DISPLAY_LINE_SETTINGS_DRAW_SYMBOL_FIELD = "DrawSymbol";
    public static final String DISPLAY_LINE_SETTINGS_SOLID_FILL_FIELD = "SolidFill";
    public static final String DISPLAY_SYMBOL_SETTINGS_FIELD = "SymbolSettings";
    public static final String DISPLAY_SYMBOL_SETTINGS_SYMBOL_STYLE_FIELD = "SymbolStyle";
    public static final String DISPLAY_SYMBOL_SETTINGS_SYMBOL_WIDTH_FIELD = "SymbolWidth";
    public static final String DISPLAY_SYMBOL_SETTINGS_SYMBOL_HEIGHT_FIELD = "SymbolHeight";
    public static final String DISPLAY_SYMBOL_SETTINGS_SYMBOL_COLOR_FIELD = "SymbolColor";
    
    // HORIZON LAYER, DISPLAY FIELDS, MAP VIEW ORDER
	//    DISPLAY ATTRIBUTE SUBFIELDS
    public static final String DISPLAY_DISPLAY_ATTR_FIELD = "DisplayAttribute";
	public static final String DISPLAY_DISPLAY_ATTR_EP_AXIS_DIR_FIELD = "epAxisDirection";
	public static final String DISPLAY_DISPLAY_ATTR_EP_AXIS_DIR_REVERSED_FIELD = "Reversed";
	public static final String DISPLAY_DISPLAY_ATTR_CDP_AXIS_DIR_FIELD = "cdpAxisDirection";
	public static final String DISPLAY_DISPLAY_ATTR_CDP_AXIS_DIR_REVERSED_FIELD = "Reversed";
	public static final String DISPLAY_DISPLAY_ATTR_TRANSPOSE_FIELD = "Transpose";
    
    public static final String DISPLAY_COLOR_ATTR_FIELD = "ColorAttribute";
    public static final String DISPLAY_COLOR_ATTR_COLORMAP_MIN_FIELD = "ColormapMin";
    public static final String DISPLAY_COLOR_ATTR_COLORMAP_MAX_FIELD = "ColormapMax";
    public static final String DISPLAY_COLOR_ATTR_COLORMAP_FIELD = "Colormap";
	
	//MODEL LAYER, DATASET FIELDS, CROSS SECTION VIEW ORDER
	//    SELECTED PROPERTY SUBFIELDS
	public static final String DATASET_SELECTED_PROPERTY_FIELD = "SelectedProperty";
	public static final String DATASET_SELECTED_PROPERTY_PROPERTY_FIELD = "Property";
	public static final String DATASET_SELECTED_PROPERTY_SYNCHRONIZE_FIELD = "Synchronize";
    
    // WINDOW, SCALE FIELDS
    public static final String WIN_SCALE_HORIZ_SCALE_FIELD = "HorizontalScale";
	public static final String WIN_SCALE_LOCK_ASPECT_RATIO_FIELD = "LockAspectRatio";
    public static final String WIN_SCALE_VERT_SCALE_FIELD = "VerticalScale";

    // WINDOW, SYNCHRONIZATION FIELDS
    public static final String WIN_SYNC_BROADCAST_FIELD = "Broadcast";
	public static final String WIN_SYNC_LISTEN_CURSOR_FIELD = "ListenToCursorPosition";
    public static final String WIN_SYNC_LISTEN_HORIZ_SCALE_FIELD = "ListenToHorizontalScale";
	public static final String WIN_SYNC_LISTEN_SCROLL_FIELD = "ListenToScrollPosition";
    public static final String WIN_SYNC_LISTEN_VERT_SCALE_FIELD = "ListenToVerticalScale";
    public static final String WIN_SYNC_SYNC_HORIZ_SCROLLING_FIELD = "SynchronizeHorizontalScrolling";
    public static final String WIN_SYNC_SYNC_VERT_SCROLLING_FIELD = "SynchronizeVerticalScrolling";

    // WINDOW, ANNOTATION FIELDS
    public static final String WIN_ANNO_ANNO_LAYER_FIELD = "AnnotatedLayer";
    public static final String WIN_ANNO_MISC_TAB = "Misc";
    public static final String WIN_ANNO_MISC_TITLE_FIELD = "Title";
    public static final String WIN_ANNO_MISC_LABELS_FIELD = "Labels";
    public static final String WIN_ANNO_MISC_COLORBAR_FIELD = "ColorBar";
    public static final String WIN_ANNO_HORIZ_TAB = "Horizontal";
    public static final String WIN_ANNO_HORIZ_LOC_FIELD = "Location";
    public static final String WIN_ANNO_HORIZ_SELECTED_KEYS_FIELD = "SelectedKeys";
    public static final String WIN_ANNO_HORIZ_SYNC_KEY_FIELD = "SynchronizationKey";
    public static final String WIN_ANNO_VERTICAL_TAB = "Vertical";
    public static final String WIN_ANNO_VERTICAL_STEPTYPE_FIELD = "StepType";
    public static final String WIN_ANNO_VERTICAL_APPEAR_FIELD = "Appearance";
	public static final String WIN_ANNO_AXES_TAB = "Axes";
	public static final String WIN_ANNO_AXES_HORIZ_AXIS_LOC_FIELD = "HorizontalAxisLocation";
	public static final String WIN_ANNO_AXES_HORIZ_MAJOR_STEP_FIELD = "HorizontalMajorStep";
	public static final String WIN_ANNO_AXES_HORIZ_MINOR_STEP_FIELD = "HorizontalMinorStep";
	public static final String WIN_ANNO_AXES_VERTICAL_AXIS_LOC_FIELD = "VerticalAxisLocation";
	public static final String WIN_ANNO_AXES_VERTICAL_MAJOR_STEP_FIELD = "VerticalMajorStep";
	public static final String WIN_ANNO_AXES_VERTICAL_MINOR_STEP_FIELD = "VerticalMinorStep";
    //    MISC TITLE SUBFIELDS
    public static final String WIN_ANNO_MISC_TITLE_LOC_FIELD = "Location";
    public static final String WIN_ANNO_MISC_TITLE_TEXT_FIELD = "Text";
    //    MISC LABELS SUBFIELDS
    public static final String WIN_ANNO_MISC_LABELS_LEFT_FIELD = "Left";
    public static final String WIN_ANNO_MISC_LABELS_RIGHT_FIELD = "Right";
    public static final String WIN_ANNO_MISC_LABELS_TOP_FIELD = "Top";
    public static final String WIN_ANNO_MISC_LABELS_BOTTOM_FIELD = "Bottom";
    //    VERTICAL STEP TYPE
    public static final String WIN_ANNO_VERTICAL_STEPTYPE_AUTO_FIELD = "Automatic";
    public static final String WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_FIELD = "UserDefined";
    public static final String WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_MAJOR_FIELD = "MajorStep";
    public static final String WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_MINOR_FIELD = "MinorStep";
    //    VERTICAL APPEARANCE
    public static final String WIN_ANNO_VERTICAL_APPEAR_LOC_FIELD = "Location";
}
