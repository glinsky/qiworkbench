/*
###########################################################################
# qiViewer - a 2D seismic viewer
# This program module Copyright (C) 2008 BHP Billiton Petroleum
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# PLEASE NOTE:  One or more BHP Billiton patents or patent applications may
# cover this software or its use. BHP Billiton hereby grants a limited,
# personal, irrevocable, perpetual, royalty-free license under such patents
# but only for things you are allowed to do under the terms of the General
# Public License.  All other rights are expressly reserved.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
 */
package com.bhpb.qiviewer.controlManager;

import java.awt.Color;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import com.bhpb.qiworkbench.api.IMessagingManager;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.ControlConstants;

import com.bhpb.qiviewer.ViewerAgent;
import com.bhpb.qiviewer.adapter.geoio.GeoIODirector;
import com.bhpb.qiviewer.adapter.geoio.GeoIOTransactionResult;
import com.bhpb.qiviewer.ui.window.ViewerWindow;
import com.bhpb.qiviewer.util.ProgressDialog;

import com.bhpb.geographics.controller.LayerProperties;
import com.bhpb.geographics.model.HorizonLayer;
import com.bhpb.geographics.model.Layer;
import com.bhpb.geographics.model.ModelLayer;
import com.bhpb.geographics.model.SeismicLayer;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geographics.colorbar.ColorbarUtil;

import com.bhpb.geographics.accessor.PropertyAccessor;
import com.bhpb.geographics.accessor.PropertyAccessorException;
import com.bhpb.geographics.model.layer.display.LayerDisplayParameters;
import com.bhpb.geographics.model.layer.display.LayerDisplayProperties;
import com.bhpb.geographics.model.layer.display.LocalSubsetProperties;
import com.bhpb.geographics.model.layer.sync.LayerSyncProperties;
import com.bhpb.geographics.model.window.WindowAnnotationProperties;
import com.bhpb.geographics.model.window.WindowAnnotationProperties.LABEL_LOCATION;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties;
import com.bhpb.geographics.model.window.CrossSectionAnnotationProperties.STEP_TYPE;
import com.bhpb.geographics.model.window.MapAnnotationProperties;
import com.bhpb.geographics.model.window.WindowScaleProperties;
import com.bhpb.geographics.model.window.WindowProperties;
import com.bhpb.geographics.model.window.WindowSyncProperties;
import com.bhpb.geoio.datasystems.AbstractKeyRange;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.datasystems.UnlimitedKeyRange;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoDataOrder;
import static com.bhpb.geoio.datasystems.DataSystemConstants.*;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.HorizonProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import java.io.IOException;

/**
 * Manage a control session, i.e., when another qiComponent is specifying
 * the actions to be taken instead of the user.
 *
 * @author Gil Hansen
 * @version 1.0
 */
public class ControlSessionManager {

    private static final Logger logger = Logger.getLogger(ControlSessionManager.class.toString());
    /** Viewer agent */
    ViewerAgent agent;
    /** Viewer Agent's messaging manager. */
    IMessagingManager messagingMgr;
    /** Map of internal window IDs to its window descriptor */
    HashMap<String, WindowDescriptor> intWins;
    /** Map of geofile IDs to its geoFile descriptor */
    HashMap<String, GeofileDescriptor> geoFiles;
    /** Map of layer IDs to its layer descriptor */
    HashMap<String, LayerDescriptor> layers;
    /** Map of layer properties IDs or layer IDs to the layer properties */
    HashMap<String, LayerProperties> layerProperties;
    GeoIODirector geoioDirector;
    ArrayList<String> ids = new ArrayList<String>();
    String winID = "", layerID = "";
    IQiWorkbenchMsg abResp;
    String errmsg = "";
    //Progress dialog
    ProgressDialog dialog;

    //Layers
    SeismicLayer seismicLayer = null;
    ModelLayer modelLayer = null;
    HorizonLayer horizonLayer = null;

    //Layer properties
    DatasetProperties _layerDatasetProps = null;
    LayerDisplayParameters _layerDisplayProps = null;
    LayerSyncProperties _layerSyncProps = null;
    LocalSubsetProperties _layerSubsetProps = null;
    //geoFile summary information
    GeoFileDataSummary _geofileDataSummary = null;

    /** Map of internal window IDs to the geofiles loaded in the window */
    /**
     * Constructor. Initialize the control manager
     * @param agent core qiViewer component.
     */
    public ControlSessionManager(ViewerAgent agent) {
        this.agent = agent;
        this.messagingMgr = agent.getMessagingManager();
        intWins = new HashMap<String, WindowDescriptor>();
        geoFiles = new HashMap<String, GeofileDescriptor>();
        layers = new HashMap<String, LayerDescriptor>();
        layerProperties = new HashMap<String, LayerProperties>();

        geoioDirector = new GeoIODirector(agent);
    }

    /**
     * Process the CONTROL_QIVIEWER request and return the response. The request
     * may contain one or more (action, params) pairs. When there are multiple
     * actions in a request, none of the intermediate actions return a response.
     * Only the last action returns a response; normal if no error while performing
     * any of the actions; otherwise, an abormal response containing the error(s).
     * @param req The request message.
     */
    public void processReq(IQiWorkbenchMsg req) {
        System.out.println("CSManager::");
        logger.info("qiViewer::processReq: request=" + req);
        ArrayList<String> nameValuePairs = new ArrayList<String>();

        ArrayList actions = (ArrayList) req.getContent();

        boolean multiactions = actions.size() > 2 ? true : false;
        for (int k = 0; k < actions.size(); k += 2) {
            String action = (String) actions.get(k);
            ArrayList actionParams = (ArrayList) actions.get(k + 1);
            nameValuePairs = parseNameValuePairs(actionParams);

            //Note: Cannot occur in a multiaction command.
            //Get the properties of a layer that has not been loaded and save
            //the properties in a hashmap keyed by the layer properties ID
            if (action.indexOf(ControlConstants.GET_LAYER_PROPS_ACTION) != -1) {
                String format = "", filepath = "";

                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("format")) {
                        format = value;
                    } else if (name.equals("file")) {
                        filepath = value;
                    }
                }

                LayerProperties properties = geoioDirector.readLayerProperties(filepath);

                if (properties.isErrorCondition()) {
                    errmsg = "geoIO ERROR: " + properties.getErrorMsg();
                    routeErrMsg(req);
                } else {
                    //return the layer's property ID
                    String layerPropsID = genLayerPropsID();
                    layerProperties.put(layerPropsID, properties);
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, layerPropsID);
                }

                return;
            }

            //Note: Cannot occur in a multiaction command.
            //Open a new window and load a layer. If the layer's properties are
            //specified, enforce them when loading the layer; otherwise, get the
            //layer's properties after loading it and save them in a hashmap
            //keyed on the layer ID.
            if (action.indexOf(ControlConstants.OPEN_WINDOW_ACTION) != -1) {
                String type = "", format = "", filepath = "", layerPropsID = "";
                String ltype = "", horizon = "";

                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("format")) {
                        format = value;
                    } else if (name.equals("file")) {
                        filepath = value;
                    } else if (name.equals("layerPropsID")) {
                        layerPropsID = value;
                    } else if (name.equals("ltype")) {
                        ltype = value;
                    } else if (name.equals("horizon")) {
                        horizon = value;
                    }
                }

                GeoIOTransactionResult result;
                LayerProperties properties = null;
                if (type.equals("XSection")) {
                    properties = layerPropsID.equals("") ? geoioDirector.readLayerProperties(filepath) : layerProperties.get(layerPropsID);

                    if (ltype.equals(ControlConstants.HORIZON_LAYER)) {
                        //set the selected horizon
                        HorizonProperties hprops = (HorizonProperties) properties.getDatasetProperties();
                        hprops.setSelectedHorizon(horizon);
                    }

                    //UPDATE PROGRESS: set the type of the window being created
                    dialog.setWinType("XSection");
                    if (dialog.isCancelled()) {
                        terminateControlScript(req);
                    }

                    boolean isSummary = properties.getDatasetProperties().getSummary() == null ? false : true;
                    result = geoioDirector.readData(properties, QIWConstants.OPEN_WINDOW, false, !isSummary);
                } else if (type.equals("Map")) {
                    properties = layerPropsID.equals("") ? geoioDirector.readLayerProperties(filepath) : layerProperties.get(layerPropsID);

                    if (ltype.equals(ControlConstants.HORIZON_LAYER)) {
                        //set the selected horizon
                        HorizonProperties hprops = (HorizonProperties) properties.getDatasetProperties();
                        hprops.setSelectedHorizon(horizon);
                    }

                    //UPDATE PROGRESS: set the type of the window being created
                    dialog.setWinType("Map");
                    if (dialog.isCancelled()) {
                        terminateControlScript(req);
                    }

                    boolean isSummary = properties.getDatasetProperties().getSummary() == null ? false : true;
                    result = geoioDirector.readData(properties, QIWConstants.OPEN_WINDOW, false, !isSummary);
                } else {
                    result = GeoIOTransactionResult.createErrorResult("Unable to process unknown type: " + type, false);
                }

                if (result.isErrorCondition()) {
                    errmsg = "geoIO ERROR: " + result.getErrorMsg();
                    routeErrMsg(req);
                } else {
                    ids.clear();
                    winID = result.getWindowID();
                    ids.add(winID);
                    layerID = result.getLayerID();
                    ids.add(layerID);

//                    if (layerPropsID.equals("")) {
                    layerProperties.put(layerID, properties);
//                    }

                    //save info about the created window
                    WindowDescriptor winDesc = new WindowDescriptor(winID, type);
                    intWins.put(winID, winDesc);

                    //save info about the loaded layer
                    LayerDescriptor layerDesc = new LayerDescriptor(layerID, format, winID);
                    layers.put(layerID, layerDesc);

                    //UPDATE PROGRESS: set the name of the layer being processed
                    String layerName = agent.getValidLayer(layerID).getLayerName();
                    //Indicate a new window is being created and a layer being loaded
                    dialog.incrWinCnt();
                    dialog.setLayerName(layerName);
                    if (dialog.isCancelled()) {
                        terminateControlScript(req);
                    }

                    //return the ID of the opened XSection window and layer loaded
                    messagingMgr.sendResponse(req, QIWConstants.ARRAYLIST_TYPE, ids);
                }

                return;
            } else //Note: Cannot occur in a multiaction command.
            //Get a layer property.
            if (action.indexOf(ControlConstants.GET_LAYER_PROP_ACTION) != -1) {
                String localLayerID = "", type = "", props = "", field = "", layerPropsID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("layerID")) {
                        localLayerID = value;
                    } else if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("props")) {
                        props = value;
                    } else if (name.equals("field")) {
                        field = value;
                    } else if (name.equals("layerPropsID")) {
                        layerPropsID = value;
                    }
                }

                //execute action
                String fieldValue = "";
                //rip apart the qualified field reference
                String[] tokens = field.split(CMConstants.QUALIFIED_DELIMITER);

                LayerProperties properties = layerProperties.get(layerPropsID.equals("") ? localLayerID : layerPropsID);

                if (type.equals(ControlConstants.SEISMIC_LAYER)) {
                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        _layerDatasetProps = properties.getDatasetProperties();

                        fieldValue = getDatasetProp(type, field, _layerDatasetProps);
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();

                        fieldValue = "3587 -3425 [2]";
                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();

                        fieldValue = getDisplayProp(type, field, _layerDisplayProps);
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();

                        fieldValue = "3587 -3425 [2]";
                    }
                } else if (type.equals(ControlConstants.MODEL_LAYER)) {
                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        _layerDatasetProps = properties.getDatasetProperties();

                        fieldValue = getDatasetProp(type, field, _layerDatasetProps);
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();

                        fieldValue = "3587 -3425 [2]";
                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();

                        fieldValue = getDisplayProp(type, field, _layerDisplayProps);
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();

                        fieldValue = "3587 -3425 [2]";
                    }
                } else if (type.equals(ControlConstants.HORIZON_LAYER)) {
                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        _layerDatasetProps = properties.getDatasetProperties();

                        fieldValue = getDatasetProp(type, field, _layerDatasetProps);
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();

                        fieldValue = "3587 -3425 [2]";
                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();

                        fieldValue = getDisplayProp(type, field, _layerDisplayProps);
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();

                        fieldValue = "3587 -3425 [2]";
                    }
                }

                //return the value of the specified field
                messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, fieldValue);
                return;
            } else //Set a layer property
            if (action.indexOf(ControlConstants.SET_LAYER_PROP_ACTION) != -1) {
                String localLayerID = "", type = "", props = "", field = "", value = "", layerPropsID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String nvalue = nameValuePairs.get(j + 1);
                    if (name.equals("layerID")) {
                        localLayerID = nvalue;
                    } else if (name.equals("type")) {
                        type = nvalue;
                    } else if (name.equals("props")) {
                        props = nvalue;
                    } else if (name.equals("field")) {
                        field = nvalue;
                    } else if (name.equals("value")) {
                        value = nvalue;
                    } else if (name.equals("layerPropsID")) {
                        layerPropsID = nvalue;
                    }
                }

                LayerProperties properties = layerProperties.get(layerPropsID.equals("") ? localLayerID : layerPropsID);

                //execute action
                if (type.equals(ControlConstants.SEISMIC_LAYER)) {
                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        _layerDatasetProps = properties.getDatasetProperties();

                        setDatasetProp(type, field, value, _layerDatasetProps);
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();

                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();

                        setDisplayProp(type, field, value, _layerDisplayProps);
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();

                    }
                } else if (type.equals(ControlConstants.MODEL_LAYER)) {
                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        _layerDatasetProps = properties.getDatasetProperties();

                        setDatasetProp(type, field, value, _layerDatasetProps);
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();
                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();

                        setDisplayProp(type, field, value, _layerDisplayProps);
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();
                    }

                } else if (type.equals(ControlConstants.HORIZON_LAYER)) {
                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        _layerDatasetProps = properties.getDatasetProperties();

                        setDatasetProp(type, field, value, _layerDatasetProps);
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();

                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();

                        setDisplayProp(type, field, value, _layerDisplayProps);
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();
                    }
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.GET_DATASET_SUMMARY_ACTION) != -1) {
                String layerPropsID = "";

                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("layerPropsID")) {
                        layerPropsID = value;
                    }
                }

                LayerProperties properties = layerProperties.get(layerPropsID);

                //execute action
                String status = geoioDirector.getGeoIOAdapter().readSummary(properties.getDatasetProperties());
                if (!status.equals("")) {
                    errmsg = "geoIO ERROR: Cannot read dataset summary. " + status;
                    routeErrMsg(req);
                    return;
                }
                //NOTE: dataset properties now contains a summary
                ViewerWindow activeWindow = agent.getActiveWindow();
                Layer activeLayer = null;
                if (activeWindow != null) {
                    activeLayer = activeWindow.getActiveLayer();
                }
                LayerDisplayProperties layerDisplayProps = LayerDisplayProperties.createDefaultLayerProperties(properties.getDatasetProperties(),
                        activeLayer);
                properties.setLayerDisplayProperties(layerDisplayProps);
                layerProperties.put(layerPropsID, properties);

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.OK_LAYER_PROPS_ACTION) != -1) {
                String localLayerID = "", type = "", props = "", layerPropsID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("layerID")) {
                        localLayerID = value;
                    } else if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("props")) {
                        props = value;
                    } else if (name.equals("layerPropsID")) {
                        layerPropsID = value;
                    }
                }

                LayerProperties properties = layerProperties.get(layerPropsID.equals("") ? localLayerID : layerPropsID);

                //execute action
                if (type.equals(ControlConstants.SEISMIC_LAYER)) {
                    if (!localLayerID.equals("")) {
                        seismicLayer = (SeismicLayer) agent.getValidLayer(localLayerID);
                        if (!localLayerID.equals("") && seismicLayer == null) {
                            errmsg = "internal ERROR: no seismic layer associated with layerID " + localLayerID;
                            routeErrMsg(req);
                            return;
                        }
                    }

                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerDatasetProps = properties.getDatasetProperties();
                            seismicLayer.setDatasetProperties(_layerDatasetProps, true, true);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();
                            seismicLayer.setLocalSubsetProperties(_layerSubsetProps);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();
                            seismicLayer.setDisplayParameters(_layerDisplayProps);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();
                            seismicLayer.setLayerSyncProperties(_layerSyncProps);
                        } else {
                        }
                    }

                } else if (type.equals(ControlConstants.MODEL_LAYER)) {
                    if (!localLayerID.equals("")) {
                        modelLayer = (ModelLayer) agent.getValidLayer(localLayerID);
                        if (!localLayerID.equals("") && modelLayer == null) {
                            errmsg = "internal ERROR: no model layer associated with layerID " + localLayerID;
                            routeErrMsg(req);
                            return;
                        }
                    }

                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerDatasetProps = properties.getDatasetProperties();
                            seismicLayer.setDatasetProperties(_layerDatasetProps, true, true);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();
                            seismicLayer.setLocalSubsetProperties(_layerSubsetProps);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();
                            seismicLayer.setDisplayParameters(_layerDisplayProps);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();
                            seismicLayer.setLayerSyncProperties(_layerSyncProps);
                        } else {
                        }
                    }

                } else if (type.equals(ControlConstants.HORIZON_LAYER)) {
                    if (!localLayerID.equals("")) {
                        horizonLayer = (HorizonLayer) agent.getValidLayer(localLayerID);
                        if (!localLayerID.equals("") && horizonLayer == null) {
                            errmsg = "internal ERROR: no horizon layer associated with layerID " + localLayerID;
                            routeErrMsg(req);
                            return;
                        }
                    }

                    if (props.equals(ControlConstants.LAYER_DATASET_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerDatasetProps = properties.getDatasetProperties();
                            horizonLayer.setDatasetProperties(_layerDatasetProps, true, true);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_LOCAL_SUBSET_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerSubsetProps = properties.getLayerDisplayProperties().getLocalSubsetProperties();
                            horizonLayer.setLocalSubsetProperties(_layerSubsetProps);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_DISPLAY_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerDisplayProps = properties.getLayerDisplayProperties().getLayerDisplayParameters();
                            horizonLayer.setDisplayParameters(_layerDisplayProps);
                        } else {
                        }
                    } else if (props.equals(ControlConstants.LAYER_SYNC_PROPERTIES)) {
                        //have changes take affect
                        if (!localLayerID.equals("")) {
                            _layerSyncProps = properties.getLayerDisplayProperties().getLayerSyncProperties();
                            horizonLayer.setLayerSyncProperties(_layerSyncProps);
                        } else {
                        }
                    }
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.RESIZE_WINDOW_ACTION) != -1) {
                String localWinID = "", width = "", height = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        localWinID = value;
                    } else if (name.equals("width")) {
                        width = value;
                    } else if (name.equals("height")) {
                        height = value;
                    }
                }

                int w = 0, h = 0;
                try {
                    w = Integer.parseInt(width);
                    h = Integer.parseInt(height);
                } catch (NumberFormatException nfe) {
                }
                if (!localWinID.equals("-1")) {
                    agent.updateWindowSize(localWinID, w, h);
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.VIEW_ALL_ACTION) != -1) {
                String localWinID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        localWinID = value;
                    }
                }

                ViewerWindow viewerWin = agent.getValidWindow(localWinID);
                viewerWin.zoomAll();

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else //Add layer to a specified window. If the layer properties are
            //speicifed, enforce them when loading the layer; otherwise, get the
            //layer's properties after loading it and save them in a hashmap
            //keyed on the layer ID.
            if (action.indexOf(ControlConstants.ADD_LAYER_ACTION) != -1) {
                //Note: Cannot occur in a multiaction command.
                String localWinID = "", format = "", filepath = "", type = "", horizon = "", layerPropsID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        localWinID = value;
                    } else if (name.equals("format")) {
                        format = value;
                    } else if (name.equals("file")) {
                        filepath = value;
                    } else if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("horizon")) {
                        horizon = value;
                    } else if (name.equals("layerPropsID")) {
                        layerPropsID = value;
                    }
                }

                GeoIOTransactionResult result;
                LayerProperties properties = null;
                //NOTE: Layer added to the selected window
                if (type.equals(ControlConstants.SEISMIC_LAYER)) {
                    //Note: it is not necessary to pass the winID for the viewer will use the selected window
                    properties = layerPropsID.equals("") ? geoioDirector.readLayerProperties(filepath) : layerProperties.get(layerPropsID);

                    result = geoioDirector.readData(properties, QIWConstants.ADD_LAYER, false, true);
                } else if (type.equals(ControlConstants.MAP_LAYER)) {
                    properties = layerPropsID.equals("") ? geoioDirector.readLayerProperties(filepath) : layerProperties.get(layerPropsID);
                    result = geoioDirector.readData(properties, QIWConstants.ADD_LAYER, false, true);
                } else if (type.equals(ControlConstants.HORIZON_LAYER)) {
                    properties = layerPropsID.equals("") ? geoioDirector.readLayerProperties(filepath) : layerProperties.get(layerPropsID);
                    //set the selected horizon
                    HorizonProperties hprops = (HorizonProperties) properties.getDatasetProperties();
                    hprops.setSelectedHorizon(horizon);
                    result = geoioDirector.readData(properties, QIWConstants.ADD_LAYER, false, true);
                } else if (type.equals(ControlConstants.MODEL_LAYER)) {
                    properties = layerPropsID.equals("") ? geoioDirector.readLayerProperties(filepath) : layerProperties.get(layerPropsID);
                    result = geoioDirector.readData(properties, QIWConstants.ADD_LAYER, false, true);
                } else {
                    result = GeoIOTransactionResult.createErrorResult("Unable to complete ADD_LAYER_ACTION for unknown layer type: " + type, false);
                }

                if (result.isErrorCondition()) {
                    errmsg = "geoIO ERROR: " + result.getErrorMsg();
                    routeErrMsg(req);
                } else {
                    ids.clear();
                    localWinID = result.getWindowID();
                    ids.add(localWinID);
                    layerID = result.getLayerID();
                    ids.add(layerID);

                    //if layer loaded without properties specified, save the
                    //layer's properties
                    //NOTE: geoioDirector.readData() will create default LayerDisplayProperties if there are none.
                    if (layerPropsID.equals("")) {
                        properties.setLayerDisplayProperties(result.getLayerDisplayProperties());
                        layerProperties.put(layerID, properties);
                    }

                    //save info about the loaded layer
                    LayerDescriptor layerDesc = new LayerDescriptor(layerID, format, localWinID);
                    layers.put(layerID, layerDesc);

                    //UPDATE PROGRESS: set the name of the layer being processed
                    String layerName = agent.getValidLayer(layerID).getLayerName();
                    //Indicate a new window is being created and a layer being loaded
                    dialog.incrLayerCnt();
                    dialog.setLayerName(layerName);
                    if (dialog.isCancelled()) {
                        terminateControlScript(req);
                    }

                    //return the ID of the opened XSection window and layer loaded
                    messagingMgr.sendResponse(req, QIWConstants.ARRAYLIST_TYPE, ids);
                }

                return;
            } else if (action.indexOf(ControlConstants.POSITION_WINDOW_ACTION) != -1) {
                String localWinID = "", x = "", y = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        localWinID = value;
                    } else if (name.equals("x")) {
                        x = value;
                    } else if (name.equals("y")) {
                        y = value;
                    }
                }

                int xcord = 0, ycord = 0;
                try {
                    xcord = Integer.parseInt(x);
                    ycord = Integer.parseInt(y);
                } catch (NumberFormatException nfe) {
                }
                if (!localWinID.equals("-1")) {
                    agent.updateWindowLocation(localWinID, xcord, ycord);
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else //Note: Cannot occur in a multiaction command.
            if (action.indexOf(ControlConstants.GET_WIN_PROP_ACTION) != -1) {
                String localWinID = "", props = "", field = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        localWinID = value;
                    } else if (name.equals("props")) {
                        props = value;
                    } else if (name.equals("field")) {
                        field = value;
                    }
                }

                //execute action
                String fieldValue = "";
                ViewerWindow viewerWin = agent.getValidWindow(localWinID);
                WindowProperties winProps = viewerWin.getWindowProperties();

                if (props.equals(ControlConstants.WIN_SCALE_PROPERTIES)) {
                    fieldValue = getWinScaleProp(field, winProps.getScaleProperties());
                } else if (props.equals(ControlConstants.WIN_ANNO_PROPERTIES)) {
                    fieldValue = getWinAnnoProp(field, winProps.getAnnotationProperties(), viewerWin.getOrientation());
                } else if (props.equals(ControlConstants.WIN_SYNC_PROPERTIES)) {
                    fieldValue = getWinSyncProp(field, winProps.getSyncProperties());
                }

                //return the value of the specified field
                messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, fieldValue);
                return;
            } else if (action.indexOf(ControlConstants.SET_WIN_PROP_ACTION) != -1) {
                String localWinID = "", props = "", field = "", value = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String nvalue = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        localWinID = nvalue;
                    } else if (name.equals("props")) {
                        props = nvalue;
                    } else if (name.equals("field")) {
                        field = nvalue;
                    } else if (name.equals("value")) {
                        value = nvalue;
                    }
                }

                //execute action
                String fieldValue = "";
                ViewerWindow viewerWin = agent.getValidWindow(localWinID);
                WindowProperties winProps = viewerWin.getWindowProperties();

                if (props.equals(ControlConstants.WIN_SCALE_PROPERTIES)) {
                    setWinScaleProp(field, value, winProps.getScaleProperties());
                } else if (props.equals(ControlConstants.WIN_ANNO_PROPERTIES)) {
                    setWinAnnoProp(req, field, value, winProps.getAnnotationProperties(),
                            viewerWin.getOrientation(), viewerWin);
                } else if (props.equals(ControlConstants.WIN_SYNC_PROPERTIES)) {
                    setWinSyncProp(field, value, winProps.getSyncProperties());
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.OK_WIN_PROPS_ACTION) != -1) {
                String localWinID = "", props = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        localWinID = value;
                    } else if (name.equals("props")) {
                        props = value;
                    }
                }

                //execute OK action
                ViewerWindow viewerWin = agent.getValidWindow(localWinID);
                WindowProperties winProps = viewerWin.getWindowProperties();

                if (props.equals(ControlConstants.WIN_SCALE_PROPERTIES)) {
                    viewerWin.updateWindowPropertyGroup(winProps.getScaleProperties(), false);
                } else if (props.equals(ControlConstants.WIN_ANNO_PROPERTIES)) {
                    viewerWin.updateWindowPropertyGroup(winProps.getAnnotationProperties(), false);
                } else if (props.equals(ControlConstants.WIN_SYNC_PROPERTIES)) {
                    viewerWin.updateWindowPropertyGroup(winProps.getSyncProperties(), false);
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.RESIZE_VIEWER_ACTION) != -1) {
                String width = "", height = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("width")) {
                        width = value;
                    } else if (name.equals("height")) {
                        height = value;
                    }
                }

                int w = 0, h = 0;
                try {
                    w = Integer.parseInt(width);
                    h = Integer.parseInt(height);
                } catch (NumberFormatException nfe) {
                }
                agent.updateQiViewerSize(w, h);

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.GET_TRACE_RANGE_ACTION) != -1) {
                String layerID = "", type = "", layerPropsID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("layerID")) {
                        layerID = value;
                    } else if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("layerPropsID")) {
                        layerPropsID = value;
                    }
                }

                LayerProperties properties = layerProperties.get(layerPropsID.equals("") ? layerID : layerPropsID);
                /* The geoFile Summary is read after the layer dataset properties are set.
                //Note: The geoFile summary has not been read when we have
                //      the layer's properties, but has if we have the layerID
                if (!layerPropsID.equals("")) {
                geoioDirector.getGeoIOAdapter().readSummary(layerProperties.get(layerPropsID).getDatasetProperties());
                _layerDatasetProps = layerProperties.get(layerPropsID).getDatasetProperties();
                } else {
                _layerDatasetProps = properties.getDatasetProperties();
                }
                 */
                _layerDatasetProps = properties.getDatasetProperties();

                //get min and max range of trace
                ArrayList<String> minMaxTraceVals = new ArrayList<String>();
                _geofileDataSummary = _layerDatasetProps.getSummary();
                minMaxTraceVals.add(Double.toString(_geofileDataSummary.getMinAmplitude()));
                minMaxTraceVals.add(Double.toString(_geofileDataSummary.getMaxAmplitude()));

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.ARRAYLIST_TYPE, minMaxTraceVals);
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.SCROLL_ACTION) != -1) {
                String winID = "", scrollbar = "", position = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        winID = value;
                    } else if (name.equals("scrollbar")) {
                        scrollbar = value;
                    } else if (name.equals("position")) {
                        position = value;
                    }
                }

                ViewerWindow viewerWin = agent.getValidWindow(winID);
                double pos = 0;
                try {
                    pos = Double.parseDouble(position);
                } catch (NumberFormatException nfe) {
                }
                if (scrollbar.equals(ControlConstants.VERTICAL_SCROLLBAR)) {
                    viewerWin.scrollToVisiblePoint(pos);
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.SET_DIVIDER_ACTION) != -1) {
                String winID = "", splitPane = "", dividerLoc = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        winID = value;
                    } else if (name.equals("splitPane")) {
                        splitPane = value;
                    } else if (name.equals("dividerLoc")) {
                        dividerLoc = value;
                    }
                }

                ViewerWindow viewerWin = agent.getValidWindow(winID);
                int width = 0;
                try {
                    width = Integer.parseInt(dividerLoc);
                } catch (NumberFormatException nfe) {
                }
                if (splitPane.equals(ControlConstants.LAYER_EXPLORER)) {
                    viewerWin.setDividerLocation(width);
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.END_CONTROL_SESSION_ACTION) != -1) {
                intWins.clear();
                geoFiles.clear();
                layers.clear();
                layerProperties.clear();

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.NOP_ACTION) != -1) {
                messagingMgr.sendResponse(req, "", "");
                return;
            } else if (action.indexOf(ControlConstants.SELECT_COLOR_MAP_ACTION) != -1) {
                String layerID = "", type = "", layerPropsID = "", colormap = "", props = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("layerID")) {
                        layerID = value;
                    } else if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("layerPropsID")) {
                        layerPropsID = value;
                    } else if (name.equals("colormap")) {
                        colormap = value;
                    } else if (name.equals("props")) {
                        props = value;
                    }
                }

                try {
                    ColorMap colorMap = ColorbarUtil.getColorMapByResourceName(colormap);
                    LayerProperties properties = layerProperties.get(layerPropsID.equals("") ? layerID : layerPropsID);
                    if (properties != null) {
                        LayerDisplayParameters ldParams = properties.getLayerDisplayParameters();
                        if (ldParams != null) {
                            ldParams.setColorMap(colorMap);
                            logger.info("Set colorMap: " + colormap + " for layer with layerPropsID: " + layerPropsID);
                        } else {
                            logger.warning("Unable to set ColorMap - ldParams are null for layerPropsID: " + layerPropsID);
                        }
                    } else {
                        logger.warning("Unable to set ColorMap - LayerProperties are null for layerPropsID: " + layerPropsID);
                    }
                } catch (IllegalArgumentException iae) {
                    logger.warning("Unable to load colorMap for resource named: " + colormap +
                            ".  Default ColorMap was not changed.");
                } catch (IOException ioe) {
                    logger.warning("Unable to load colorMap for resource named: " + colormap +
                            " because " + ioe.getMessage() +
                            ".  Default ColorMap was not changed.");
                } catch (UnsupportedOperationException uoe) {
                    logger.warning("While attempting to set colorMap '" + colormap +
                            "' for layPropsID '" + layerPropsID + "', caught UnsupportedOperationException: " + uoe.getMessage());
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.HIDE_LAYER_ACTION) != -1) {
                String layerID = "", type = "", winID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("layerID")) {
                        layerID = value;
                    } else if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("winID")) {
                        winID = value;
                    }
                }

                ViewerWindow viewerWin = agent.getValidWindow(winID);
                viewerWin.setHidden(agent.getValidLayer(layerID), true);

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.REDRAW_WINDOW_ACTION) != -1) {
                String winID = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        winID = value;
                    }
                }

                ViewerWindow viewerWin = agent.getValidWindow(winID);
                viewerWin.redraw();

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.ZOOM_ACTION) != -1) {
                String winID = "", direction = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        winID = value;
                    } else if (name.equals("direction")) {
                        direction = value;
                    }
                }

                ViewerWindow viewerWin = agent.getValidWindow(winID);
                if (direction.equals("All")) {
                    viewerWin.zoomAll();
                } else if (direction.equals("In")) {
                    viewerWin.zoomIn();
                } else if (direction.equals("Out")) {
                    viewerWin.zoomOut();
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.INIT_PROGRESS_DIALOG_ACTION) != -1) {
                String title = "", wins = "", layers = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("windows")) {
                        wins = value;
                    } else if (name.equals("layers")) {
                        layers = value;
                    } else if (name.equals("title")) {
                        title = value;
                    }
                }

                dialog = new ProgressDialog(title);

                //get number of windows to be created
                int numWins = 0;
                try {
                    numWins = Integer.parseInt(wins);
                } catch (NumberFormatException nfe) {
                }

                //get number of layers to be add per window
                String[] nums = layers.split(",");
                if (numWins == 0) {
                    numWins = nums.length;
                }
                int[] numLayers = new int[nums.length];
                for (int i = 0; i < nums.length; i++) {
                    try {
                        numLayers[i] = Integer.parseInt(nums[i]);
                    } catch (NumberFormatException nfe) {
                        numLayers[i] = 1;
                    }
                }

                //init the dialog
                dialog.initProgressDialog(numWins, numLayers);

                dialog.setSize(500, 170);
                dialog.setDefaultCloseOperation(javax.swing.JDialog.DISPOSE_ON_CLOSE);
                dialog.setLocation(200, 100);
                dialog.setVisible(true);

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.END_PROGRESS_DIALOG_ACTION) != -1) {
                dialog.setVisible(false);
                dialog = null;

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.MOVE_LAYER_ACTION) != -1) {
                String winID = "", layerID = "", type = "", direction = "", position = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("winID")) {
                        winID = value;
                    } else if (name.equals("layerID")) {
                        layerID = value;
                    } else if (name.equals("type")) {
                        type = value;
                    } else if (name.equals("direction")) {
                        direction = value;
                    } else if (name.equals("value")) {
                        position = value;
                    }
                }

                ViewerWindow viewerWin = agent.getValidWindow(winID);
                Layer winLayer = agent.getValidLayer(layerID);

                //Note: If position is a number, it is the index of the layer
                //in the layer list starting at zero.
                if (direction.equals("Up")) {
                    if (position.equals("Top")) {
                        viewerWin.moveLayerToTop(winLayer);
                    }
                } else if (direction.equals("Down")) {
                    if (position.equals("Bottom")) {
                        viewerWin.moveLayerToBottom(winLayer);
                    }
                }

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else if (action.indexOf(ControlConstants.SET_LAYER_COLOR_ACTION) != -1) {
                String layerID = "", property = "", color = "";
                //get the action parameters
                for (int j = 0; j < nameValuePairs.size(); j += 2) {
                    String name = nameValuePairs.get(j);
                    String value = nameValuePairs.get(j + 1);
                    if (name.equals("layerID")) {
                        layerID = value;
                    } else if (name.equals("property")) {
                        property = value;
                    } else if (name.equals("color")) {
                        color = value;
                    }
                }

                String colorProp = ColorMap.FOREGROUND;
                //Determine color property
                if (property.equals("Foreground")) {
                    colorProp = ColorMap.FOREGROUND;
                } else if (property.equals("Background")) {
                    colorProp = ColorMap.BACKGROUND;
                } else if (property.equals("NegativeMF")) {
                    colorProp = ColorMap.NEGFILL;
                } else if (property.equals("PositiveMF")) {
                    colorProp = ColorMap.POSFILL;
                }

                //Determine color
                Color jcolor = Color.BLACK;
                if (color.equals("Green")) {
                    jcolor = Color.GREEN;
                } else if (color.equals("Blue")) {
                    jcolor = Color.BLUE;
                } else if (color.equals("Red")) {
                    jcolor = Color.RED;
                }
                System.out.println("SET_LAYER_COLOR: layerID=" + layerID + ", property=" + property + ", color=" + color);

                Layer layer = agent.getValidLayer(layerID);
                layer.getColorMap().setColors(colorProp, Arrays.asList(new Color[]{jcolor}));

                if (!multiactions) {
                    messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
                    return;
                }
                continue;
            } else //unrecognized command which is a bug
            {
                errmsg = "Internal ERROR: Unrecognized action: " + action + "; Unable to process control request: " + req;
            }
            routeErrMsg(req);
            return;
        }
        //The last action in a multiaction request sends back the response
        if (multiactions) {
            messagingMgr.sendResponse(req, QIWConstants.STRING_TYPE, "");
        }
    }

    /**
     * Notify the display transducer the user cancelled displaying results. The
     * transducer will send control commands to close the progress dialog and
     * end the control session.
     * @param req Request (control command) being processed.
     */
    private void terminateControlScript(IQiWorkbenchMsg req) {
        errmsg = "ACTION CANCELLED: Displaying results cancelled.";
        routeErrMsg(req);
    }

    /**
     * Route an abnormal response back for the control request
     * @param req Control request message
     * Implicit params: errmsg - error message;
     */
    private void routeErrMsg(IQiWorkbenchMsg req) {
        logger.severe(errmsg);
        abResp = MsgUtils.genAbnormalMsg(req, MsgStatus.SC_EXCEPTION, errmsg);
        messagingMgr.routeMsg(abResp);
    }
    static long lastControlSessionID = 0;

    /**
     * Generate a unique control session ID (sessionID). The sessionID is the current time
     * and date. However, if the new sessionID is less than or equal to the last one,
     * the last one is incremented by 1 to make it unique.
     *
     * @return Unique control session ID
     */
    static public String genControlSessionID() {
        long sessionID = System.currentTimeMillis();
        if (sessionID <= lastControlSessionID) {
            sessionID = ++lastControlSessionID;
        }
        lastControlSessionID = sessionID;
        return Long.toString(sessionID);
    }
    static long lastGeofileID = 0;

    /**
     * Generate a unique geofile ID for data loaded in an internal window. The geofileID
     * is the  current time and date. However, if the new geofileID is less than or equal
     * to the last one, the last one, incremented by 1, is used to make it unique.
     *
     * @return Unique geofile ID
     */
    static public String genGeofileID() {
        long geofileID = System.currentTimeMillis();
        if (geofileID <= lastGeofileID) {
            geofileID = ++lastGeofileID;
        }
        lastGeofileID = geofileID;
        return Long.toString(geofileID);
    }
    static long lastPropsID = 0;

    /**
     * Generate a unique layer properties ID (propsID). The propsID is the current time
     * and date. However, if the new propsID is less than or equal to the last one,
     * the last one, incremented by 1, is used to make it unique.
     *
     * @return Unique properties ID
     */
    static public String genLayerPropsID() {
        long propsID = System.currentTimeMillis();
        if (propsID <= lastPropsID) {
            propsID = ++lastPropsID;
        }
        lastPropsID = propsID;
        return Long.toString(propsID);
    }

    /**
     * Parse the (name, value) pairs into a linear list.
     * @param actionParams List of name=value action parameters
     */
    private ArrayList<String> parseNameValuePairs(ArrayList<String> actionParams) {
        ArrayList<String> nameValuePairs = new ArrayList<String>();
        for (int i = 0; i < actionParams.size(); i++) {
            String nameValue = actionParams.get(i);
            int idx = nameValue.indexOf("=");
            nameValuePairs.add(nameValue.substring(0, idx));
            nameValuePairs.add(nameValue.substring(idx + 1));
        }
        return nameValuePairs;
    }

    /**
     * Get value of layer dataset property.
     * @param layer Type of layer
     * @param qualitifedField Qualified field reference
     * @param dataset properties
     * @return Value of the qualified field
     */
    private String getDatasetProp(String layerType, String qualifiedField, DatasetProperties layerDatasetProps) {
        //rip apart the qualified field reference
        String[] tokens = qualifiedField.split(CMConstants.QUALIFIED_DELIMITER);

        String fieldValue = "";

        switch (tokens[0].charAt(0)) {
            case 'B':
                //qualified field: Binsize.key
                if (tokens[0].equals(CMConstants.DATASET_BINSIZE_FIELD)) {
                    return Integer.toString(layerDatasetProps.getKeyBinsize(tokens[1]));
                }
                break;

            case 'C':
                //qualified field: ChosenRange.key
                if (tokens[0].equals(CMConstants.DATASET_CHOSEN_RANGE_FIELD)) {
                    fieldValue = layerDatasetProps.getKeyChosenRange(tokens[1]).toScriptString();

                    return fieldValue;
                }
                break;

            case 'F':
                //qualified field: FullRange.key
                if (tokens[0].equals(CMConstants.DATASET_FULL_RANGE_FIELD)) {
                    fieldValue = "1-1[1]";
                    GeoFileMetadata metadata = layerDatasetProps.getMetadata();
                    DiscreteKeyRange keyRange = metadata.getKeyRange(tokens[1]);
                    if (keyRange != null) {
                        fieldValue = keyRange.toScriptString();
                    }

                    return fieldValue;
                }
                break;

            case 'H':
                //qualified field: HorizonSetting
                if (tokens[0].equals(CMConstants.DATASET_HORIZON_SETTING_FIELD)) {
                    return ((HorizonProperties) layerDatasetProps).getSelectedHorizon();
                }
                break;

            case 'I':
                //qualified field: Increment.key
                if (tokens[0].equals(CMConstants.DATASET_INCR_FIELD)) {
                    return Double.toString(layerDatasetProps.getKeyIncrement(tokens[1]));
                } else //qualified field: Interpolation.key
                if (tokens[0].equals(CMConstants.DATASET_INTERP_FIELD)) {
                    return layerDatasetProps.getKeyNearest(tokens[1]) ? "on" : "off";
                }
                break;

            case 'K':
                //qualified field: Key
                if (tokens[0].equals(CMConstants.DATASET_INTERP_FIELD)) {
                    int order = 0;
                    try {
                        order = Integer.parseInt(fieldValue);
                    } catch (NumberFormatException nfe) {
                    }
                    return layerDatasetProps.getOrderedKey(order);
                }
                break;

            case 'O':
                //qualified field: Offset.key
                if (tokens[0].equals(CMConstants.DATASET_OFFSET_FIELD)) {
                    return Integer.toString(layerDatasetProps.getKeyOrder(tokens[1]));
                } else //qualified field: Order.key
                if (tokens[0].equals(CMConstants.DATASET_ORDER_FIELD)) {
                    return Integer.toString(layerDatasetProps.getKeyOffset(tokens[1]));
                }
                break;

            case 'S':
                //qualfied field: Synchronize.key
                if (tokens[0].equals(CMConstants.DATASET_SYNC_FIELD)) {
                    return layerDatasetProps.getKeySync(tokens[1]) ? "on" : "off";
                }
                break;

            default:
                System.out.println("Unable to get dataset property " + tokens[0]);
                break;
        }

        return fieldValue;
    }

    /**
     * Set value of layer dataset property.
     * @param layer Type of layer
     * @param qualitifedField Qualified field reference
     * @param Value of the qualified field
     * @param dataset properties
     */
    private void setDatasetProp(String layerType, String qualifiedField, String fieldValue, DatasetProperties layerDatasetProps) {
        //rip apart the qualified field reference
        String[] tokens = qualifiedField.split(CMConstants.QUALIFIED_DELIMITER);

        if (layerType.equals(ControlConstants.MODEL_LAYER)) {
            logger.info("CSM setting model dataset property '" + qualifiedField + "' to: " + fieldValue);
            switch (tokens[0].charAt(0)) {
                //qualified field: SelectedProperty.[Property | Synchronization]
                case 'S':
                    if (tokens[0].equals(CMConstants.DATASET_SELECTED_PROPERTY_FIELD)) {
                        if (tokens[1].equals(CMConstants.DATASET_SELECTED_PROPERTY_PROPERTY_FIELD)) {
                            ((ModelProperties) layerDatasetProps).setSelectedProperty(fieldValue);
                        } else if (tokens[1].equals(CMConstants.DATASET_SELECTED_PROPERTY_SYNCHRONIZE_FIELD)) {
                            ((ModelProperties) layerDatasetProps).setSynchronizeProperty(fieldValue.equals("on") ? true : false);
                        } else {
                            System.out.println("Unable to set model dataset property Selected Property." + tokens[1]);
                        }

                    }
                    break;

                default:
                    System.out.println("Unable to set model dataset property " + tokens[0]);
                    break;
            }
        }

        logger.info("CSM setting dataset property '" + qualifiedField + "' to: " + fieldValue);
        switch (tokens[0].charAt(0)) {
            case 'B':
                //qualified field: Binsize.key
                if (tokens[0].equals(CMConstants.DATASET_BINSIZE_FIELD)) {
                    int val = 0;
                    try {
                        val = Integer.parseInt(fieldValue);
                    } catch (NumberFormatException nfe) {
                    }
                    layerDatasetProps.setKeyBinsize(tokens[1], val);
                }
                break;

            case 'C':
                //qualified field: ChosenRange.key
                if (tokens[0].equals(CMConstants.DATASET_CHOSEN_RANGE_FIELD)) {
                    AbstractKeyRange keyRange = (AbstractKeyRange) layerDatasetProps.getKeyChosenRange(tokens[1]);
                    if (keyRange != null && !keyRange.isArbTrav()) {
                        if (fieldValue.equals("*")) {
                            //NOTE: Assumes chosen range initialized to full 
                            //range, so nothing really to do. Relevant only if
                            //chosen range changed, and want to reset to full
                            //range.
                            if (!keyRange.isUnlimited()) {
                                UnlimitedKeyRange uKeyRange = new UnlimitedKeyRange(keyRange.getFullKeyRange());
                                uKeyRange.setKey(tokens[1]);
                                layerDatasetProps.setKeyChosenRange(tokens[1], uKeyRange);
                            }
                        } else if (!fieldValue.contains("-")) {
                            //single value
                            double val = 0;
                            try {
                                val = Double.parseDouble(fieldValue);
                                if (keyRange.isDiscrete()) {
                                    ((DiscreteKeyRange) keyRange).setRange(val, val, ((DiscreteKeyRange) keyRange).getIncr(), ((DiscreteKeyRange) keyRange).getKeyType());
                                } else {
                                    DiscreteKeyRange fullRange = keyRange.getFullKeyRange();
                                    DiscreteKeyRange dKeyRange = new DiscreteKeyRange(val, val, fullRange.getIncr(), GeoKeyType.REGULAR_TYPE, keyRange.getFullKeyRange());
                                    dKeyRange.setKey(tokens[1]);
                                    layerDatasetProps.setKeyChosenRange(tokens[1], dKeyRange);
                                }
                            } catch (NumberFormatException nfe) {
                            }
                        } else {
                            //full range: min-max[incr]
                            String[] fr1 = fieldValue.split("-");
                            String[] fr2 = fr1[1].split("\\[");
                            String[] fr3 = fr2[1].split("\\]");
                            double min = 0, max = 0, incr = 1;
                            try {
                                min = Double.parseDouble(fr1[0]);
                                max = Double.parseDouble(fr2[0]);
                                incr = Double.parseDouble(fr3[0]);
                            } catch (NumberFormatException nfe) {
                            }
                            if (keyRange.isDiscrete()) {
                                ((DiscreteKeyRange) keyRange).setRange(min, max, incr, ((DiscreteKeyRange) keyRange).getKeyType());
                            } else {
                                DiscreteKeyRange dKeyRange = new DiscreteKeyRange(min, max, incr, GeoKeyType.REGULAR_TYPE);
                                dKeyRange.setKey(tokens[1]);
                                layerDatasetProps.setKeyChosenRange(tokens[1], dKeyRange);
                            }
                        }
                    }
                }
                break;

            case 'F':
                //qualified field: FullRange.key
                if (tokens[0].equals(CMConstants.DATASET_FULL_RANGE_FIELD)) {
                    System.out.println("Cannot set the dataset FullRange for a key");
                }
                break;

            case 'H':
                //qualified field: HorizonSetting
                if (tokens[0].equals(CMConstants.DATASET_HORIZON_SETTING_FIELD)) {
                    ((HorizonProperties) layerDatasetProps).setSelectedHorizon(fieldValue);
                }
                break;

            case 'I':
                //qualified field: Increment.key
                if (tokens[0].equals(CMConstants.DATASET_INCR_FIELD)) {
                    double val = 0.0;
                    try {
                        val = Double.parseDouble(fieldValue);
                    } catch (NumberFormatException nfe) {
                    }
                    layerDatasetProps.setKeyIncrement(tokens[1], val);
                } else //qualified field: Interpolation.key
                if (tokens[0].equals(CMConstants.DATASET_INTERP_FIELD)) {
                    layerDatasetProps.setKeyNearest(tokens[1], fieldValue.equals("on") ? true : false);
                }
                break;

            case 'K':
                //qualfied field: Key
                if (tokens[0].equals(CMConstants.DATASET_INTERP_FIELD)) {
                    System.out.println("Cannot set a dataset Key");
                }
                break;

            case 'O':
                //qualified field: Offset.key
                if (tokens[0].equals(CMConstants.DATASET_OFFSET_FIELD)) {
                    int val = 0;
                    try {
                        val = Integer.parseInt(fieldValue);
                    } catch (NumberFormatException nfe) {
                    }
                    layerDatasetProps.setKeyOffset(tokens[1], val);
                } else //qualified field: Order.key
                if (tokens[0].equals(CMConstants.DATASET_ORDER_FIELD)) {
                    int val = 0;
                    try {
                        val = Integer.parseInt(fieldValue);
                    } catch (NumberFormatException nfe) {
                    }
                    layerDatasetProps.setKeyOrder(tokens[1], val);
                }
                break;

            case 'S':
                //qualified field: Synchronize.key
                if (tokens[0].equals(CMConstants.DATASET_SYNC_FIELD)) {
                    layerDatasetProps.setKeySync(tokens[1], fieldValue.equals("on") ? true : false);
                }
                break;

            default:
                System.out.println("Unable to set common dataset property " + tokens[0]);
                break;
        }
    }

    /**
     * Get value of layer display property.
     * @param layer Type of layer
     * @param qualitifedField Qualified field reference
     * @param display properties
     * @return Value of the qualified field
     */
    private String getDisplayProp(String layerType, String qualifiedField, PropertyAccessor layerDisplayProps) {
        String fieldValue = "";
        try {
            fieldValue = layerDisplayProps.getProperty(qualifiedField);
        } catch (PropertyAccessorException pae) {
            logger.warning("Unable to getDisplayProp due to: " + pae.getMessage());
        }

        return fieldValue;
    }

    /**
     * Set value of layer display property.
     * @param layer Type of layer
     * @param qualitifedField Qualified field reference
     * @param Value of the qualified field
     * @param display properties
     */
    private void setDisplayProp(String layerType, String qualifiedField, String fieldValue, PropertyAccessor layerDisplayProps) {
        try {
            logger.info("CSM setting display property '" + qualifiedField + "' to: " + fieldValue);
            layerDisplayProps.setProperty(qualifiedField, fieldValue);
        } catch (PropertyAccessorException pae) {
            logger.warning("Unable to getDisplayProp due to: " + pae.getMessage());
        }
    }

    /**
     * Get value of window synchronization property.
     * @param qualitifedField Qualified field reference
     * @param window sync properties
     * @return Value of the qualified field
     */
    private String getWinSyncProp(String qualifiedField, WindowSyncProperties syncProps) {
        String fieldValue = "";

        try {
            syncProps.getProperty(qualifiedField);
        } catch (PropertyAccessorException pae) {
            //May not be a valid qualified field
            logger.warning("Cannot get window sync property. CAUSE: " + pae.getMessage());
        }

        return fieldValue;
    }

    /**
     * Set value of window synchronization property.
     * @param qualitifedField Qualified field reference
     * @param Value of the qualified field
     * @param display properties
     */
    private void setWinSyncProp(String qualifiedField, String fieldValue, WindowSyncProperties syncProps) {
        //rip apart the qualified field reference
        String[] tokens = qualifiedField.split(CMConstants.QUALIFIED_DELIMITER);

        switch (tokens[0].charAt(0)) {
            case 'B':
                //qualified field: Broadcast
                if (tokens[0].equals(CMConstants.WIN_SYNC_BROADCAST_FIELD)) {
                    syncProps.setEnabled(WindowSyncProperties.SYNC_FLAG.BROADCAST, fieldValue.equals("on") ? true : false);
                }
                break;

            case 'L':
                //qualified field: ListToCursorPosition | ListenToHorizontalScale | ListToScrollPostion | ListenToVerticalScale
                if (tokens[0].equals(CMConstants.WIN_SYNC_LISTEN_CURSOR_FIELD)) {
                    syncProps.setEnabled(WindowSyncProperties.SYNC_FLAG.CURSOR_POS_LISTENING, fieldValue.equals("on") ? true : false);
                } else if (tokens[0].equals(CMConstants.WIN_SYNC_LISTEN_HORIZ_SCALE_FIELD)) {
                    syncProps.setEnabled(WindowSyncProperties.SYNC_FLAG.H_SCALE_LISTENING, fieldValue.equals("on") ? true : false);
                } else if (tokens[0].equals(CMConstants.WIN_SYNC_LISTEN_SCROLL_FIELD)) {
                    syncProps.setEnabled(WindowSyncProperties.SYNC_FLAG.SCROLL_POS_LISTENING, fieldValue.equals("on") ? true : false);
                } else if (tokens[0].equals(CMConstants.WIN_SYNC_LISTEN_VERT_SCALE_FIELD)) {
                    syncProps.setEnabled(WindowSyncProperties.SYNC_FLAG.V_SCALE_LISTENING, fieldValue.equals("on") ? true : false);
                }
                break;

            case 'S':
                //qualified field: SynchronizeHorizontalScrolling | SynchronizeVerticalScrolling
                if (tokens[0].equals(CMConstants.WIN_SYNC_SYNC_HORIZ_SCROLLING_FIELD)) {
                    syncProps.setEnabled(WindowSyncProperties.SYNC_FLAG.H_SCROLL_POS_SYNC, fieldValue.equals("on") ? true : false);
                } else if (tokens[0].equals(CMConstants.WIN_SYNC_SYNC_VERT_SCROLLING_FIELD)) {
                    syncProps.setEnabled(WindowSyncProperties.SYNC_FLAG.V_SCROLL_POS_SYNC, fieldValue.equals("on") ? true : false);
                }
                break;

            default:
                System.out.println("Unable to set window sync property " + tokens[0]);
                break;
        }
    }

    /**
     * Get value of window scale property.
     * @param qualitifedField Qualified field reference
     * @param window scale properties
     * @return Value of the qualified field
     */
    private String getWinScaleProp(String qualifiedField, WindowScaleProperties scaleProps) {
        //rip apart the qualified field reference
        String[] tokens = qualifiedField.split(CMConstants.QUALIFIED_DELIMITER);

        String fieldValue = "";

        switch (tokens[0].charAt(0)) {
            case 'H':
                //qualified field: HorizontalScale
                if (tokens[0].equals(CMConstants.WIN_SCALE_HORIZ_SCALE_FIELD)) {
                    return Double.toString(scaleProps.getHorizontalScale());
                }
                break;

            case 'L':
                //qualified field: LockAspectRation
                if (tokens[0].equals(CMConstants.WIN_SCALE_LOCK_ASPECT_RATIO_FIELD)) {
                    return scaleProps.isAspectRatioLocked() ? "on" : "off";
                }
                break;

            case 'V':
                //qualified field: VerticalScale
                if (tokens[0].equals(CMConstants.WIN_SCALE_VERT_SCALE_FIELD)) {
                    return Double.toString(scaleProps.getVerticalScale());
                }
                break;

            default:
                System.out.println("Unable to get window scale property " + tokens[0]);
                break;
        }

        return fieldValue;
    }

    /**
     * Set value of window scale property.
     * @param qualitifedField Qualified field reference
     * @param Value of the qualified field
     * @param display properties
     */
    private void setWinScaleProp(String qualifiedField, String fieldValue, WindowScaleProperties scaleProps) {
        //rip apart the qualified field reference
        String[] tokens = qualifiedField.split(CMConstants.QUALIFIED_DELIMITER);

        switch (tokens[0].charAt(0)) {
            case 'H':
                //qualified field: HorizontalScale
                if (tokens[0].equals(CMConstants.WIN_SCALE_HORIZ_SCALE_FIELD)) {
                    double val = 1.0;
                    try {
                        val = Double.parseDouble(fieldValue);
                        scaleProps.setHorizontalScale(val);
                    } catch (NumberFormatException nfe) {
                        //use default
                    } catch (IllegalArgumentException iae) {
                        //use default
                    }
                }
                break;

            case 'L':
                //qualified field: LockAspectRatio
                if (tokens[0].equals(CMConstants.WIN_SCALE_LOCK_ASPECT_RATIO_FIELD)) {
                    scaleProps.setAspectRatioLocked(fieldValue.equals("on") ? true : false);
                }
                break;

            case 'V':
                //qualified field: VerticalScale
                if (tokens[0].equals(CMConstants.WIN_SCALE_VERT_SCALE_FIELD)) {
                    double val = 1.0;
                    try {
                        val = Double.parseDouble(fieldValue);
                        scaleProps.setVerticalScale(val);
                    } catch (NumberFormatException nfe) {
                        //use default
                    } catch (IllegalArgumentException iae) {
                        //use default
                    }
                }
                break;

            default:
                System.out.println("Unable to set window scale property " + tokens[0]);
                break;
        }
    }

    /**
     * Get value of window annotation property.
     * @param qualitifedField Qualified field reference
     * @param window anno properties
     * @return Value of the qualified field
     */
    private String getWinAnnoProp(String qualifiedField, WindowAnnotationProperties annoProps, GeoDataOrder orientation) {
        //rip apart the qualified field reference
        String[] tokens = qualifiedField.split(CMConstants.QUALIFIED_DELIMITER);

        // NOTE to preserve the existing functionality, the following WindowAnnotationProperties
        // sublcass instances are created.  Depending the orientation of the associated ViewerWindow,
        // exactly ONE of these will be set to a non-null value by down-casting the WindowAnnotationProperties.
        // These objects may be deleted when this method is split into 3 parts :
        // Setting the setting the annotated layer and misc. annotation properties (WindowAnnotationProperties)
        // Setting the Axes annotation properties (MapAnnotationProperties)
        // Setting the vertical and horizontal annotation properties (CrossSectionAnnotationProperties)
        CrossSectionAnnotationProperties xSecAnnoProps = null;

        switch (orientation) {
            case CROSS_SECTION_ORDER:
                xSecAnnoProps = (CrossSectionAnnotationProperties) annoProps;
                break;
            case MAP_VIEW_ORDER:
                break;
            default:
                throw new IllegalArgumentException("Unable to get all WinAnnoProp data because : " + orientation + " is not a valid Layer orientation.");
        }
        String fieldValue = "";

        switch (tokens[0].charAt(0)) {
            case 'A':
                //qualified field: AnnotatedLayer
                if (tokens[0].equals(CMConstants.WIN_ANNO_ANNO_LAYER_FIELD)) {
                    //get the layerID of the specified annotated layer. If there
                    //is none, the layerID will be Layer.NaL
                    if (orientation == GeoDataOrder.CROSS_SECTION_ORDER) {
                        return ((CrossSectionAnnotationProperties) annoProps).getAnnotatedLayerName();
                    } else {
                        return annoProps.getAnnotatedLayerName();
                    }
                } else //qualified field: Axes.[HorizontalAxisLocation | HorizontalMajorStep | HorizontalMinorStep | VerticalAxisLocation | VerticalMajorStep | VerticalMinorStep]
                if (tokens[0].equals(CMConstants.WIN_ANNO_AXES_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_HORIZ_AXIS_LOC_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_HORIZ_MAJOR_STEP_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_HORIZ_MINOR_STEP_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_VERTICAL_AXIS_LOC_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_VERTICAL_MAJOR_STEP_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_VERTICAL_MINOR_STEP_FIELD)) {
                    }
                }

                break;

            case 'H':
                //qualified field: Horizontal
                if (tokens[0].equals(CMConstants.WIN_ANNO_HORIZ_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_HORIZ_SELECTED_KEYS_FIELD)) {
                        String[] keys = ((CrossSectionAnnotationProperties) annoProps).getSelectedKeys();
                        //make into a comma separated list
                        int last = keys.length - 1;
                        for (int i = 0; i <= last; i++) {
                            fieldValue = keys[i];
                            if (i != last) {
                                fieldValue += ",";
                            }
                        }
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_HORIZ_LOC_FIELD)) {
                        return locToString(xSecAnnoProps.getHorizontalAnnotationLocation());
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_HORIZ_SYNC_KEY_FIELD)) {
                        return xSecAnnoProps.getSynchronizationKey();
                    }
                }
                break;

            case 'M':
                //qualified field: Misc
                if (tokens[0].equals(CMConstants.WIN_ANNO_MISC_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_MISC_TITLE_FIELD)) {
                        if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_TITLE_LOC_FIELD)) {
                            return locToString(annoProps.getTitleLocation());
                        } else if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_TITLE_TEXT_FIELD)) {
                            return annoProps.getTitle();
                        }
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_MISC_LABELS_FIELD)) {
                        return annoProps.getLabel(stringToLoc(fieldValue));
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_MISC_COLORBAR_FIELD)) {
                        return locToString(annoProps.getColorBarLocation());
                    }
                }
                break;

            case 'V':
                //qualified field: Vertical
                if (tokens[0].equals(CMConstants.WIN_ANNO_VERTICAL_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_FIELD)) {
                        if (tokens.length == 3) {
                            return stepTypeToString(xSecAnnoProps.getStepType());
                        }
                        if (tokens[2].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_FIELD)) {
                            if (tokens[3].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_MAJOR_FIELD)) {
                                return Double.toString(xSecAnnoProps.getMajorStep());
                            } else if (tokens[3].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_MINOR_FIELD)) {
                                return Double.toString(xSecAnnoProps.getMinorStep());
                            }
                        }
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_VERTICAL_APPEAR_FIELD)) {
                        if (tokens[2].equals(CMConstants.WIN_ANNO_VERTICAL_APPEAR_LOC_FIELD)) {
                            return locToString(xSecAnnoProps.getVerticalAnnotationLocation());
                        }
                    }
                }
                break;

            default:
                System.out.println("Unable to set window annotation property " + tokens[0]);
                break;
        }

        return fieldValue;
    }

    /**
     * Set value of window annotation property.
     * @param req The request command
     * @param qualitifedField Qualified field reference
     * @param fieldValue Value of the qualified field
     * @param annoProps Window annotation properties
     * @param orientation Type of window - XSection or Map
     * @param viewerWindow Display window
     */
    private void setWinAnnoProp(IQiWorkbenchMsg req, String qualifiedField,
            String fieldValue,
            WindowAnnotationProperties annoProps,
            GeoDataOrder orientation,
            ViewerWindow viewerWindow) {
        //rip apart the qualified field reference
        String[] tokens = qualifiedField.split(CMConstants.QUALIFIED_DELIMITER);
        // NOTE to preserve the existing functionality, the following WindowAnnotationProperties
        // sublcass instances are created.  Depending the orientation of the associated ViewerWindow,
        // exactly ONE of these will be set to a non-null value by down-casting the WindowAnnotationProperties.
        // These objects may be deleted when this method is split into 3 parts :
        // Setting the setting the annotated layer and misc. annotation properties (WindowAnnotationProperties)
        // Setting the Axes annotation properties (MapAnnotationProperties)
        // Setting the vertical and horizontal annotation properties (CrossSectionAnnotationProperties)

        switch (orientation) {
            case CROSS_SECTION_ORDER:
                annoProps = new CrossSectionAnnotationProperties((CrossSectionAnnotationProperties) annoProps);
                break;
            case MAP_VIEW_ORDER:
                annoProps = new MapAnnotationProperties((MapAnnotationProperties) annoProps);
                break;
            default:
                throw new IllegalArgumentException("Unable to get all WinAnnoProp data because : " + orientation + " is not a valid Layer orientation.");
        }

        switch (tokens[0].charAt(0)) {
            case 'A':
                //qualified field: AnnotatedLayer
                if (tokens[0].equals(CMConstants.WIN_ANNO_ANNO_LAYER_FIELD)) {
                    //Note: the fieldValue is the layerID which may be NaL
                    if (Layer.NaL.equals(fieldValue)) {
                        logger.warning("Cannot set annotated layer: layerID is Layer.NaL (not a layer).");
                    } else {
                        Layer layer = agent.getValidLayer(fieldValue);
                        if (layer == null) {
                            logger.warning("layerID '" + fieldValue + "' is associatd with a null layer");
                        } else {
                            if (orientation == GeoDataOrder.CROSS_SECTION_ORDER) {
                                ((CrossSectionAnnotationProperties) annoProps).setAnnotatedLayerName(layer.getLayerName());
                            } else {
                                annoProps.setAnnotatedLayerName(layer.getLayerName());
                            }
                        }
                    }
                } else //qualified field: Axes.[HorizontalAxisLocation | HorizontalMajorStep | HorizontalMinorStep | VerticalAxisLocation | VerticalMajorStep | VerticalMinorStep]
                if (tokens[0].equals(CMConstants.WIN_ANNO_AXES_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_HORIZ_AXIS_LOC_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_HORIZ_MAJOR_STEP_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_HORIZ_MINOR_STEP_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_VERTICAL_AXIS_LOC_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_VERTICAL_MAJOR_STEP_FIELD)) {
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_AXES_VERTICAL_MINOR_STEP_FIELD)) {
                    }
                }
                break;

            case 'H':
                //qualified field: Horizontal.[SelectedKeys | Location | SynchronizationKey]
                if (orientation != GeoDataOrder.CROSS_SECTION_ORDER) {
                    errmsg = "command ERROR: Cannot set annotation keys for a map window";
                    routeErrMsg(req);
                    break;
                }
                if (tokens[0].equals(CMConstants.WIN_ANNO_HORIZ_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_HORIZ_SELECTED_KEYS_FIELD)) {
                        //orientation is XSection
                        if (fieldValue.equals("")) {
                            ((CrossSectionAnnotationProperties) annoProps).deselectSelectedKeys();
                        } else {
                            //split the comma separated list into individual keys
                            String[] keys = fieldValue.split(",");
                            for (int i = 0; i < keys.length; i++) {
                                ((CrossSectionAnnotationProperties) annoProps).setKeySelected(keys[i]);
                            }
                        }
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_HORIZ_LOC_FIELD)) {
                        ((CrossSectionAnnotationProperties) annoProps).setHorizontalLocation(stringToLoc(fieldValue));
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_HORIZ_SYNC_KEY_FIELD)) {
                        ((CrossSectionAnnotationProperties) annoProps).setSynchronizationKey(fieldValue);
                    }
                }
                break;

            case 'M':
                //qualified field: Misc.[Title.[Location | Text] | Labels.[Left | Right | Top | Bottom] | ColorBar]
                if (tokens[0].equals(CMConstants.WIN_ANNO_MISC_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_MISC_TITLE_FIELD)) {
                        if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_TITLE_LOC_FIELD)) {
                            annoProps.setTitleLocation(stringToLoc(fieldValue));
                        } else if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_TITLE_TEXT_FIELD)) {
                            annoProps.setTitle(fieldValue);
                        }
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_MISC_LABELS_FIELD)) {
                        if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_LABELS_LEFT_FIELD)) {
                            annoProps.setLabel(LABEL_LOCATION.LEFT, fieldValue);
                        } else if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_LABELS_RIGHT_FIELD)) {
                            annoProps.setLabel(LABEL_LOCATION.RIGHT, fieldValue);
                        } else if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_LABELS_TOP_FIELD)) {
                            annoProps.setLabel(LABEL_LOCATION.TOP, fieldValue);
                        } else if (tokens[2].equals(CMConstants.WIN_ANNO_MISC_LABELS_BOTTOM_FIELD)) {
                            annoProps.setLabel(LABEL_LOCATION.BOTTOM, fieldValue);
                        }
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_MISC_COLORBAR_FIELD)) {
                        annoProps.setColorBarLocation(stringToLoc(fieldValue));
                    }
                }
                break;

            case 'V':
                //qualified field: Vertical,[StepType.[Automatic | UserDefined.[MajorStep | MinorStep]] | Appeaaarance.Location]
                if (tokens[0].equals(CMConstants.WIN_ANNO_VERTICAL_TAB)) {
                    if (tokens[1].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_FIELD)) {
                        if (tokens[2].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_AUTO_FIELD)) {
                            STEP_TYPE stepType = fieldValue.equals("on") ? STEP_TYPE.AUTOMATIC : STEP_TYPE.NONE;
                            ((CrossSectionAnnotationProperties) annoProps).setStepType(stepType);
                        } else if (tokens[2].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_FIELD)) {
                            if (tokens[3].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_MAJOR_FIELD)) {
                                double val = 1.0;
                                try {
                                    val = Double.parseDouble(fieldValue);
                                    ((CrossSectionAnnotationProperties) annoProps).setMajorStep(val);
                                } catch (NumberFormatException nfe) {
                                    //use default
                                }
                            } else if (tokens[3].equals(CMConstants.WIN_ANNO_VERTICAL_STEPTYPE_USERDEF_MINOR_FIELD)) {
                                double val = 1.0;
                                try {
                                    val = Double.parseDouble(fieldValue);
                                    ((CrossSectionAnnotationProperties) annoProps).setMinorStep(val);
                                } catch (NumberFormatException nfe) {
                                    //use default
                                }
                            }
                        }
                    } else if (tokens[1].equals(CMConstants.WIN_ANNO_VERTICAL_APPEAR_FIELD)) {
                        if (tokens[2].equals(CMConstants.WIN_ANNO_VERTICAL_APPEAR_LOC_FIELD)) {
                            ((CrossSectionAnnotationProperties) annoProps).setVerticalAnnotationLocation(stringToLoc(fieldValue));
                        }
                    }
                }
                break;

            default:
                System.out.println("Unable to set window annotation property " + tokens[0]);
                break;
        }
        viewerWindow.updateWindowPropertyGroup(annoProps, false);
    }

    /**
     * Map a location enumeration to a String.
     * @param loc Location as an enumeration
     * @returns String representation of the location enumeration
     */
    private String locToString(LABEL_LOCATION loc) {
        switch (loc) {
            case NONE:
                return "NONE";
            case TOP:
                return "TOP";
            case BOTTOM:
                return "BOTTOM";
            case LEFT:
                return "LEFT";
            case RIGHT:
                return "RIGHT";
            default:
                return "";
        }
    }

    /**
     * Map a String location to its enumeration.  Null/Non-enum values
     * of 'loc' result in a return value of 'LOCATION.NONE', but should
     * probably be allowed to throw NullPointerExceptions / IllegalArgumentExceptions
     * instead.
     * 
     * @param loc Location as a String
     * @param Enum represetation of the location String
     * 
     * @return the value of LOCATION.valueOf(loc), or LOCATION.NONE
     * if loc is a null/non-enum value.
     */
    private LABEL_LOCATION stringToLoc(String loc) {
        return getFromString(LABEL_LOCATION.class, loc, LABEL_LOCATION.NONE);
    }

    private static <E extends Enum<E>> E getFromString(Class<E> className, String str, E nullValue) {
        try {
            return Enum.valueOf(className, str);
        } catch (IllegalArgumentException iae) {
            logger.warning("The value '" + str + "' does not correspond to a validenum of type " + nullValue.getClass().getName() + ", returning LOCATION.NONE.");
        } catch (NullPointerException npe) {
            logger.warning("CSM.getFromString(null) was called, returning " + nullValue);
        }

        return nullValue;
    }

    /**
     * Map a step type enumeration to a String.
     * @param stepType Step type as an enumeration
     * @returns String representation of the step type enumeration
     */
    private String stepTypeToString(STEP_TYPE stepType) {
        return stepType.toString();
    }

    /**
     * Map a String step type to its enumeration
     * @param stepType Step type as a String
     * @param Enum represetation of the step type String
     */
    private STEP_TYPE stringToStepType(String stepType) {
        return getFromString(STEP_TYPE.class, stepType, STEP_TYPE.NONE);
    }
}
