/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.JobMonitor;
// JE FIELD change locations to org.apache...
// these packages are now under org.apache and so we need to change the import
// statements
//import com.sun.org.apache.xml.internal.serialize.OutputFormat;
//import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import javax.swing.JTable;
import org.apache.commons.validator.routines.FloatValidator;

/**
 * Title:        Util <br><br>
 * Description:  Common Util Classes<br><br>
 * 
 * @version 1.0
 */
public class Util {
	
    public static String getTemplate(Class me) {

        StringBuffer scriptTemplate = new StringBuffer();
        InputStream in = me.getResourceAsStream("/xml/script_template.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            String inputLine = "";
            while ((inputLine = reader.readLine()) != null) {
                scriptTemplate.append(inputLine + "\n");
            }
        } catch (IOException ioe) {
            System.out.println("ERROR: reading script_template.txt file" + ioe.getMessage());
        } finally {
            try {
                if (in != null)     in.close();
                if (reader != null) reader.close();
            } catch (IOException e) {}
        }
        return scriptTemplate.toString();
    }

    public static String nodeToXMLString(Node node){
        String xml = "";
        if(node == null)
            return xml;
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer tranformer = tFactory.newTransformer();
            DOMSource source = new DOMSource(node);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(baos);
            tranformer.transform(source,result);
            xml = baos.toString();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }
        return xml;
    }

    public static String toFormatedXMlString(Document doc){
        OutputFormat format = new OutputFormat(doc);
        format.setIndenting(true);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        XMLSerializer serializer = new XMLSerializer(output, format);
        try{
            serializer.serialize(doc);
            return output.toString();
        }catch(IOException e){
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Create a unique name for the generated script from a base name.
     */
    public static String createTimeStampedScriptPrefix(String baseName) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat formatter = new SimpleDateFormat(
            "yyyy_MM_dd_HH_mm_ss_SSS");
        String scriptPrefix = baseName + "_" + formatter.format(ts);

        return scriptPrefix;
    }

    public static void runScript(String scriptNamePrefix, final JobMonitor monitor, final JobManager manager, final JButton run, final JTextArea status, JTextArea summary) {
        String logFile = scriptNamePrefix + ".out";
        String scriptName = scriptNamePrefix + ".sh";
        ArrayList params = new ArrayList();
        params.add("cd " + monitor.getScriptDir() + "; chmod ug+x "
                + scriptName + "; sh " + monitor.getScriptDir() + "/" +scriptName + " > " + logFile + " 2>&1");
        //submit the script as a possibly long running job
        params.add("false");
        manager.submitJob(params);
        monitor.setJobStatusToRunning();
        status.setText("");

        String text = scriptName + "\n";
        text += logFile + "\n";
        text += "have been created in " + monitor.getScriptDir();
        summary.setText(text);
        run.setEnabled(false);


        //TODO will be used upon request based on menu input from user.
        Runnable heavyRunnable = new Runnable(){
            public void run(){
                int time = 5000;
                while(monitor.isJobRunning()){
                    if(!updateStatus(monitor,manager,run,status))
                        return;
                    try{
                        Thread.sleep(time);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
                return;
            }
        };
        new Thread(heavyRunnable).start();

    }

    private static boolean updateStatus(final JobMonitor monitor, JobManager manager, final javax.swing.JButton run, final javax.swing.JTextArea area){
        if(monitor == null || manager == null)
            return false;
        String status = monitor.updateStatus2(manager);
        if(status.equals(JobMonitor.JOB_UNKNOWN_STATUS)){
            JOptionPane.showMessageDialog(null, "Unexpected error in getting process ids.", "Unexpected Error.", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        Runnable updateGUI = new Runnable() {
            public void run() {
                if (monitor.isJobEnded()){
                    run.setEnabled(true);
                }else{
                    run.setEnabled(false);
                }

                area.setText(monitor.fetchStdoutText());
            }
        };
        javax.swing.SwingUtilities.invokeLater(updateGUI);
        return true;
    }

    public static String getTemplate(String name) {
        StringBuffer scriptTemplate = new StringBuffer();
        InputStream in = Util.class.getResourceAsStream(name);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            String inputLine = "";
            while ((inputLine = reader.readLine()) != null) {
                scriptTemplate.append(inputLine + "\n");
            }
        } catch (IOException ioe) {
            System.out.println("ERROR: reading invert_template.txt file" + ioe.getMessage());
        } finally {
            try {
                if (in != null)     in.close();
                if (reader != null) reader.close();
            } catch (IOException e) {}
        }
        return scriptTemplate.toString();
    }

    public static float getMax(float[] data){
        float max = data[0];
        for(float f : data){
            if(f >= max)
                max = f;
        }
        return max;
    }

    public static float getMin(float[] data){
        float min = data[0];
        for(float f : data){
            if(f <= min)
                min = f;
        }
        return min;
    }

    public static boolean floatEqualZero(float val){
        if(Math.abs(Math.abs(val)-Math.abs(Float.MIN_VALUE)) < 0.0000001)
            return true;
        else
            return false;
    }

    public static Object getValueAt(JTable table,int row, String columnName){
        if(table == null || row < 0 || columnName == null){
            throw new IllegalArgumentException("Invalid parameters when calling getValueAt(...)");
        }
        int count = table.getColumnCount();
        int column = -1;
        for(int i = 0; i < count; i++){
            if(table.getColumnName(i).equals(columnName)){
                column = i;
                break;
            }
        }
        if(column != -1){
            return table.getValueAt(row, column);
        }
        return null;
    }

    public static boolean noChar(String obj) {
        if (obj == null || obj.length() == 0) return true;
            return FloatValidator.getInstance().isValid(obj);
    }

    public static boolean valNotBtn0And1(JTable table, String columnName){
        int rowCount = table.getRowCount();
        boolean found = false;
        for(int i = 0; i < rowCount; i++){
            String s = (String)Util.getValueAt(table, i, columnName);
            if(s != null && s.trim().length() > 0 && FloatValidator.getInstance().isValid(s)){
                float f = Float.valueOf(s).floatValue();
                if(f < 0 || f > 1){
                    found = true;
                    break;
                }
            }
        }
        return found;
    }
}

