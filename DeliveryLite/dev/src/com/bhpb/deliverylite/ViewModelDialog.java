/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumnModel;

import com.bhpb.deliverylite.colorbar.Colorbar1;
import com.bhpb.geographics.colorbar.ColorbarUtil;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.qiworkbench.api.IDataObject;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.util.Util;



/**
 * 
 * ViewModelDialog.java
 *
 * Created on March 12, 2008, 11:25 AM
 */


public class ViewModelDialog extends javax.swing.JInternalFrame implements PropertyChangeListener, MetadataSettable {
	private static final Logger logger = Logger.getLogger(ViewModelDialog.class.toString());
    private static final int RESOURCE_SLEEP_TIME = 100; // if resource IO is blocked, wait 1/10 second	
    private DeliveryLiteUI parentGui;
	private int defaultLayerHeight = 100;
	private int modelX = 10;  //x0 where model graphic starts
	private int modelY = 20;  //y0 where model graphic starts
	private int modelWidth = 100;  // model cell default width
	public static final int DEFAULT_FRAME_WIDTH = 220;
	public static final int DEFAULT_FRAME_HEIGHT = 600;
	public static final int PIXELS_PER_SAMPLE = 6;
	private int x0 = 0;
	private int y0 = 0;
	private float currentMaxTraceValue = -1; //must be >= 0
	public static final int MODEL_BEFORE = 0; 
	public static final int MODEL_AFTER = 1;
	private static NumberFormat floatFormat = NumberFormat.getInstance();
	static{
	floatFormat.setMinimumFractionDigits(1);
	floatFormat.setMaximumFractionDigits(1);
	}
    /**
     * Creates new form ShowModelDialog
     */
    public ViewModelDialog(DeliveryLiteUI parentGui, boolean modal) {
        //super(JOptionPane.getFrameForComponent(parentGui), modal);
        this.parentGui = parentGui;
        if(parentGui != null){
        	modelBeforePropertyColorMap = parentGui.getModelBeforePropertyColorMap();
            //if(modelBeforePropertyColorMap == null)
            //    modelBeforePropertyColorMap = new HashMap<String,Property>();
            
        	currentUnlabeledColorbarMap = parentGui.getAvailableColorbars();
        	modelAfterDatasetPropertyColorMap = parentGui.getModelAfterDatasetPropertyColorMap();
            modelAfterDatasetPropertyColorMap.put("Input Model", modelBeforePropertyColorMap);
        }
        initComponents();
        setBackground(Color.white);
        this.setIconifiable(true);
        //this.setMaximizable(true);
        this.setResizable(false);
        //addPropertyChangeListener(this);
        //nearInternalFrame.getStackPanel().getWigglePane().addPropertyChangeListener(this);
    }
    
    //private float verticalScale = 1;
    //private float horizontalScale = 1;
    //private Rectangle scrollrectToVisible = new Rectangle();
    
    public Rectangle getScrollRectToVisible(){
    	return parentGui.getShowModelParams().getScrollToVisible();
    }
    
    public void setCurrentTraceMaxValue(float val){
    	currentMaxTraceValue = val;
    }
    
    public float getCurrentTraceMaxValue(){
    	return currentMaxTraceValue;
    }
    
    public void setMetaData(String dataset, GeoFileMetadata metadata, int stackMode, int runMode){
    	parentGui.setMetaData(dataset, metadata);
    	fillLayerProperty((ModelFileMetadata)metadata);
    }    
    
    public void fillLayerProperty(ModelFileMetadata data){
    	if(data == null)
    		return;
    	Vector v = new Vector();
    	v.add("Select One");
    	for(int i = 0; data.getProperties() != null && i < data.getProperties().size(); i++)
    		v.add(data.getProperties().get(i));
    	modelAfterPropertyComboBox.setModel(new DefaultComboBoxModel(v));
    	ModelDataset dataset = getSelectedModelAfterDataset();
    	Map<String,Property> propertyColorMap = new HashMap<String,Property>();
    	for(String propName : data.getProperties()){
    		Property property = dataset.getPropertyByName(propName);
    		String datasetName = dataset.getDatasetName();
    		String counterpart = DatasetNameHelper.counterPartName(datasetName);
    		if(modelAfterDatasetPropertyColorMap.containsKey(datasetName)){
    			Map<String,Property> map = modelAfterDatasetPropertyColorMap.get(datasetName);
    			if(map.containsKey(propName)){
    				Property p = map.get(propName);
    				property.setMax(p.getMax());
    				property.setMin(p.getMin());
    				property.setColorbar(p.getColorbar());
    			}else
    				setModelAfterPropertyColorbar(property);
    		}else if(modelAfterDatasetPropertyColorMap.containsKey(counterpart)){
    			Map<String,Property> map = modelAfterDatasetPropertyColorMap.get(counterpart);
    			if(map.containsKey(propName)){
    				Property p = map.get(propName);
    				property.setMax(p.getMax());
    				property.setMin(p.getMin());
    				property.setColorbar(p.getColorbar());
    			}else
    				setModelAfterPropertyColorbar(property);
    		}else
    			setModelAfterPropertyColorbar(property);
    		propertyColorMap.put(property.getName(), property);
    	}
    	modelAfterDatasetPropertyColorMap.put(data.getGeoFileName(), propertyColorMap);
    	String selectedModelAfterProperty = parentGui.getShowModelParams().getSelectedModelAfterProperty();
    	if(selectedModelAfterProperty != null && selectedModelAfterProperty.length() > 0){
    		int index = -1;
    		for(int i = 0; i < data.getProperties().size(); i++){
    			if(data.getProperties().get(i).equals(selectedModelAfterProperty)){
    				index = i+1;
    				break;
    			}
    		}
    		if(index != -1){
    			modelAfterPropertyComboBox.setSelectedIndex(index);
    		}
    	}
    }
    
    public float getVerticalScale(){
    	return parentGui.getShowModelParams().getVerticalScale();
    }
    
    private String checkTimeRangeFit(SeismicFileMetadata metadata){
    	String message = "";
    	DiscreteKeyRange range = metadata.getKeyRange("tracl");
    	if(range == null)
    		return "Could not get the time range information from the seismic trace.";
    	double tmin = range.getMin();
		double tmax = range.getMax();
		String sTime = parentGui.getMinTime();
    	float fStartTime = Integer.MAX_VALUE;
    	if(sTime != null && sTime.trim().length() > 0){
    		try{
    			fStartTime = Float.parseFloat(sTime);
    		}catch(NumberFormatException e){
    			e.printStackTrace();
    			message = "min time from the model is not numeric.";
    		}
    	}
    	
    	float fBaseTime = Integer.MAX_VALUE;
    	sTime = parentGui.getTimeBase();
    	if(sTime != null && sTime.trim().length() > 0){
    		try{
    			fBaseTime = Float.parseFloat(sTime);
    		}catch(NumberFormatException e){
    			e.printStackTrace();
    			message = "time base from the model is not numeric.";
    		}
    	}
    	
    	if(!(fBaseTime <= tmax && fBaseTime > tmin) || !(fStartTime < tmax && fStartTime >= tmin))
    		message = "The time information from the model does not fit the time range of the seismic trace.";
    	
    	return message;
    }
    
    public float getHorizontalScale(){
    	return parentGui.getShowModelParams().getHorizontalScale();
    }
    public void propertyChange(PropertyChangeEvent pce){
    	if(pce.getPropertyName().equals("verticalScale")){
    		Object o = pce.getNewValue();
    		float scale = Float.parseFloat(o.toString());
    		float verticalScale = scale;
    		parentGui.getShowModelParams().setVerticalScale(scale);
    		modelBeforeInternalFrame.refreshModelCanvas(scale);
    		if(modelAfterDatasetComboBox.getSelectedIndex() > 0 && modelAfterPropertyComboBox.getSelectedIndex() > 0){
                String datasetType = (String)modelAfterDatasetComboBox.getSelectedItem();
                DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), datasetType);
                String modelName = helper.getModelDatasetPrefix(parentGui.isNearAndFar());
                ModelDataset dataset = parentGui.getModelDatasetByName(modelName);
    			
    			Property property = dataset.getPropertyByName((String)modelAfterPropertyComboBox.getSelectedItem());
                String stddevName = helper.getStddevModelDatasetPrefix(parentGui.isNearAndFar());
    			ModelDataset datasetStdev = parentGui.getModelDatasetByName(stddevName);
    			setModelAfterPropertyColorbar(property);
//    			//property "t" will be sigma time value for each layer of the selected model
    			// such as median or mean
    			Property stddevProperty = datasetStdev.getPropertyByName("t");
    			
    			modelAfterInternalFrame.refreshModelCanvas(scale, dataset, property,stddevProperty);
    		}
    		float horizontalScale = parentGui.getShowModelParams().getHorizontalScale();
    		if(nearInternalFrame.getStackPanel() != null && nearInternalFrame.getStackPanel().isVisible())
    			nearInternalFrame.getStackPanel().redrawWigglePane(horizontalScale,verticalScale);
    		if(parentGui.isNearAndFar())
    			if(farInternalFrame.getStackPanel() != null && farInternalFrame.getStackPanel().isVisible())
    				farInternalFrame.getStackPanel().redrawWigglePane(horizontalScale,verticalScale);    		
    			
    	}else if(pce.getPropertyName().equals("scrollValue")){
    		Object newO = pce.getNewValue();
    		//Object oldO = pce.getOldValue();
    		int newValue = Integer.parseInt(newO.toString());
    		//int oldValue = Integer.parseInt(oldO.toString());
    		//System.out.println("newValue=" + newValue);
    		//System.out.println("oldValue=" + oldValue);
    		int oldPos = parentGui.getShowModelParams().getVerticalScrollPosition();
    		//System.out.println("oldPos=" + oldPos);
    		int deltaPos = newValue - oldPos;
    		//System.out.println("deltaPos=" + deltaPos);
    		Rectangle rect = parentGui.getShowModelParams().getScrollToVisible();
    		rect.y += deltaPos;
    		rect.width = modelBeforeInternalFrame.getModelCanvas().getVisibleRect().width;
    		rect.height = modelBeforeInternalFrame.getModelCanvas().getVisibleRect().height;
    		parentGui.getShowModelParams().setVerticalScrollPosition(newValue);
    		modelBeforeInternalFrame.getScrollPane().getVerticalScrollBar().setValue(newValue);
    		modelAfterInternalFrame.getScrollPane().getVerticalScrollBar().setValue(newValue);
    		if(parentGui.isNearAndFar())
    			farInternalFrame.getStackPanel().getScrollPane().getVerticalScrollBar().setValue(newValue);
    		nearInternalFrame.getStackPanel().getScrollPane().getVerticalScrollBar().setValue(newValue);
    		//nearInternalFrame.getStackPanel().getScrollPane().revalidate();
    	}else if(pce.getPropertyName().equals("scrollRectToVisible")){
    		Rectangle aRect = (Rectangle)pce.getNewValue();
    		
    		if(nearInternalFrame.getStackPanel() != null && nearInternalFrame.getStackPanel().getWigglePane().isVisible())
    			nearInternalFrame.getStackPanel().getWigglePane().scrollRectToVisible(aRect);
    		if(parentGui.isNearAndFar())
    			if(farInternalFrame.getStackPanel() != null && farInternalFrame.getStackPanel().getWigglePane().isVisible())
    				farInternalFrame.getStackPanel().getWigglePane().scrollRectToVisible(aRect);
    		//Rectangle rect = new Rectangle(aRect.x,aRect.y, aRect.width, aRect.height+ColumnView.SIZE+5);
    		Rectangle rect = new Rectangle(aRect.x,aRect.y, aRect.width, aRect.height);
    		//scrollrectToVisible = aRect;
    		parentGui.getShowModelParams().setScrollToVisible(rect);
    		modelBeforeInternalFrame.getModelCanvas().scrollRectToVisible(rect);
    		modelAfterInternalFrame.getModelCanvas().scrollRectToVisible(rect);
    		int pos = nearInternalFrame.getStackPanel().getScrollPane().getVerticalScrollBar().getValue();
    		parentGui.getShowModelParams().setVerticalScrollPosition(pos);
    	}else if(pce.getPropertyName().equals("internalFrameHeight")){
			Object oldValue = pce.getOldValue();
			Object newValue = pce.getNewValue();
			int h = Integer.parseInt(newValue.toString());
			modelBeforeInternalFrame.setSize(modelBeforeInternalFrame.getSize().width, h);
			nearInternalFrame.setSize(nearInternalFrame.getSize().width, h);
			farInternalFrame.setSize(farInternalFrame.getSize().width, h);
			modelAfterInternalFrame.setSize(modelAfterInternalFrame.getSize().width, h);
		 }
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">                          
    private void initComponents() {
    	controlPanel = new javax.swing.JPanel();
        modelControlPanel = new javax.swing.JPanel();
        
        
        modelAfterPanel = new javax.swing.JPanel();
        modelAfterDatasetComboBox = new javax.swing.JComboBox();
        modelAfterDatasetComboBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				modelAfterDatasetComboBoxActionPerformed();
			}
		});
        modelDatasetsLabel = new javax.swing.JLabel();
        showModelAfterPropertyRadioButton = new javax.swing.JRadioButton();
        showModelAfterPropertyRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelAfterPropertyRadioButtonActionPerformed();
			}
		});

        //boolean isSelected = parentGui.getShowModelParams().isShowModelAfterPropertyValueEnabled();
        //showModelAfterPropertyRadioButton.setSelected(isSelected);
        
        showModelAfterTimeRadioButton = new javax.swing.JRadioButton();
        showModelAfterTimeRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelAfterTimeRadioButtonActionPerformed();
			}
		});

        //isSelected = parentGui.getShowModelParams().isShowModelAfterTimeValueEnabled();
        //showModelAfterTimeRadioButton.setSelected(isSelected);
        
        showModelAfterSigmaTimeRadioButton = new javax.swing.JRadioButton();
        showModelAfterSigmaTimeRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelAfterSigmaTimeRadioButtonActionPerformed();
			}
		});
        //isSelected = parentGui.getShowModelParams().isShowModelAfterSigmaTimeEnabled();
        //showModelAfterSigmaTimeRadioButton.setSelected(isSelected);
        
        showModelAfterColorbarRadioButton = new javax.swing.JRadioButton();
        showModelAfterColorbarRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelAfterColorbarRadioButtonActionPerformed();
			}
        });
        
        //isSelected = parentGui.getShowModelParams().isShowModelAfterColorbarEnabled();
        //showModelAfterColorbarRadioButton.setSelected(isSelected);
        
        modelAfterPropertyComboBox = new javax.swing.JComboBox();
        modelAfterPropertyComboBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				modelAfterPropertyComboBoxActionPerformed();
			}
		});
        chooseModelAfterPropertyLabel = new javax.swing.JLabel();
        modelAfterColorBarButton = new javax.swing.JButton();
        modelAfterColorBarButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		modelAfterColorbarActionPerformed();
        		
        	}
        });
        modelBeforePanel = new javax.swing.JPanel();
        
        
        
        chooseModelBeforePropertyLabel = new javax.swing.JLabel();
        modelBeforePropertyComboBox = new javax.swing.JComboBox();
        showModelBeforePropertyRadioButton = new javax.swing.JRadioButton();
        
        showModelBeforePropertyRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelBeforePropertyRadioButtonActionPerformed();
			}
		});
        
        //isSelected = parentGui.getShowModelParams().isShowModelBeforePropertyValueEnabled();
        //showModelBeforePropertyRadioButton.setSelected(isSelected);
        
        showModelBeforeTimeRadioButton = new javax.swing.JRadioButton();
        
        showModelBeforeTimeRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelBeforeTimeRadioButtonActionPerformed();
			}
		});
        //isSelected = parentGui.getShowModelParams().isShowModelBeforeTimeValueEnabled();
        //showModelBeforeTimeRadioButton.setSelected(isSelected);
        
        showModelBeforeSigmaTimeRadioButton = new javax.swing.JRadioButton();
        showModelBeforeSigmaTimeRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelBeforeSigmaTimeRadioButtonActionPerformed();
			}
		});
        
        //isSelected = parentGui.getShowModelParams().isShowModelBeforeSigmaTimeEnabled();
        //showModelBeforeSigmaTimeRadioButton.setSelected(isSelected);
        //autoFitRadioButton = new javax.swing.JRadioButton();
        fitPageButton = new javax.swing.JButton();
        fitPageButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(modelBeforeInternalFrame.isAlreadyFit())
					return;
				modelBeforeInternalFrame.fitPage(); //pixel per unit time
			}
		});

        zoomInButton = new javax.swing.JButton();
        zoomInButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				float vscale = parentGui.getShowModelParams().getVerticalScale();
				vscale *= 1.2f;
				modelBeforeInternalFrame.setScale(vscale); //pixel per unit time
			}
		});

        zoomOutButton = new javax.swing.JButton();       
        zoomOutButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				float vscale = parentGui.getShowModelParams().getVerticalScale();
				vscale *= 0.8f;
				modelBeforeInternalFrame.setScale(vscale); //pixel per unit time
			}
		});
        
        showModelBeforeColorbarRadioButton = new javax.swing.JRadioButton();
        
        showModelBeforeColorbarRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showModelBeforeColorbarRadioButtonActionPerformed();
			}
		});
        
        //isSelected = parentGui.getShowModelParams().isShowModelBeforeColorbarEnabled();
        //showModelBeforeColorbarRadioButton.setSelected(isSelected);
        
        final Component comp = this;
        modelBeforeColorBarButton = new javax.swing.JButton();
        //modelBeforeColorBarButton.setEnabled(false);
        modelBeforeColorBarButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		modelBeforeColorbarActionPerformed();
        		
        	}
        });
        seismicControlPanel = new javax.swing.JPanel();
        showSyntheticBeforeRadioButton = new javax.swing.JRadioButton();
        //if(showSyntheticBefore)
        //	showSyntheticBeforeRadioButton.setSelected(true);
        ///else
        showSyntheticBeforeRadioButton.setSelected(false);
        showSyntheticBeforeRadioButton.setEnabled(false);
        checkSyntheticBeforeReady();
        
        if(parentGui.getShowModelParams().isShowSyntheticBeforeEnabled())
        	showSyntheticBeforeRadioButton.setSelected(true);
        else
        	showSyntheticBeforeRadioButton.setSelected(false);
        showSyntheticBeforeRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showSyntheticBeforeRadioButtonActionPerformed();
			}
		});

        showSyntheticAfterRadioButton = new javax.swing.JRadioButton();
        if(parentGui.getShowModelParams().isShowSyntheticAfterEnabled())
        	showSyntheticAfterRadioButton.setSelected(true);
        else
        	showSyntheticAfterRadioButton.setSelected(false);
        showSyntheticAfterRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showSyntheticAfterRadioButtonActionPerformed();
			}
		});
        showSyntheticAfterRadioButton.setEnabled(false);
        checkSyntheticAfterReady();
        showRealSeismicRadioButton = new javax.swing.JRadioButton();
        if(!parentGui.isNearAndFar()){
        	if(parentGui.getNearStackFileName().length() == 0)
        		showRealSeismicRadioButton.setEnabled(false);
        	else
        		showRealSeismicRadioButton.setEnabled(true);
        }else{
        	if(parentGui.getNearStackFileName().length() == 0 && parentGui.getFarStackFileName().length() == 0)
        		showRealSeismicRadioButton.setEnabled(false);
        	else
        		showRealSeismicRadioButton.setEnabled(true);
        }
        
        if(parentGui.getShowModelParams().isShowRealStackEnabled())
        	showRealSeismicRadioButton.setSelected(true);
        else
        	showRealSeismicRadioButton.setSelected(false);
        showRealSeismicRadioButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(showRealSeismicRadioButton.isSelected()){
					String message = parentGui.validateLiteParams();
					if(message != null && message.trim().length() > 0){
						JOptionPane.showMessageDialog(parentGui, message);
						showRealSeismicRadioButton.setSelected(false);
						return;
					}
				}
				showRealSeismicRadioButtonActionPerformed();
			}
		});
        
        traceMaxValueLabel = new javax.swing.JLabel();
        traceMaxValueTextField = new javax.swing.JTextField();
        newTraceMaxValueTextField = new javax.swing.JTextField();
        newTraceMaxValueTextField.setEditable(false);
        showFillRadioButton = new javax.swing.JRadioButton();
        showFillRadioButton.setSelected(true);//default
        showFillRadioButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		if(showFillRadioButton.isSelected()){
        			nearInternalFrame.getStackPanel().setFillColor(true);
        			if(parentGui.isNearAndFar())
        				farInternalFrame.getStackPanel().setFillColor(true);
        		}else{
        			nearInternalFrame.getStackPanel().setFillColor(false);
        			if(parentGui.isNearAndFar())
        				farInternalFrame.getStackPanel().setFillColor(false);
        		}
        	}
        });
        //newTraceMaxValueTextField.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.select")));
        newTraceMaxValueLabel = new javax.swing.JLabel();
        newTraceMaxValueImageButton = new javax.swing.JButton("Apply");
        Font font = newTraceMaxValueImageButton.getFont();
        newTraceMaxValueImageButton.setFont(new Font(font.getName(),Font.BOLD,10));
        newTraceMaxValueImageButton.setToolTipText("View new scale");
        newTraceMaxValueImageButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		newTraceMaxValueButtonPerformed();
        	}
        });
        scalePanel = new javax.swing.JPanel();
        verticalScaleLabel = new javax.swing.JLabel();
        horizontalScaleLabel = new javax.swing.JLabel();
        verticalScaleTextField = new javax.swing.JTextField();
        horizontalScaleTextField = new javax.swing.JTextField();
        horizontalScaleImageButton = new javax.swing.JButton();
        
        horizontalScaleImageButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		String val = horizontalScaleTextField.getText().trim();
        		if(val.length() == 0){
        			JOptionPane.showMessageDialog(comp, "Horizontal scale value field is empty.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
        			return;
        		}
        		float newFval = 0;
        		try{
        			newFval = Float.parseFloat(val);
        		}catch(NumberFormatException nfe){
        			JOptionPane.showMessageDialog(comp, "Horizontal scale value field must be a number.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
        			return;
        		}
        		if(newFval == 0)
        			return;
        		if(currentMaxTraceValue == -1 || currentMaxTraceValue == 0)
        			return;
        		
        		
        		String origMaxVal = traceMaxValueTextField.getText().trim();
        		if(origMaxVal.length() == 0)
        			return;
        		float forigMaxVal = Float.parseFloat(origMaxVal);
        		if(forigMaxVal == 0)
        			return;
        		float newMax = currentMaxTraceValue * newFval;

        		float hScale = newMax / forigMaxVal;
        		parentGui.getShowModelParams().setNewMaxAmplitude(newFval);
        		parentGui.getShowModelParams().setHorizontalScale(hScale);
        		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
        		if(nearInternalFrame.getStackPanel() != null && nearInternalFrame.getStackPanel().isVisible())
        			//nearInternalFrame.getStackPanel().showWiggleTrace(verticalScale,hScale, parentGui.getNearStackFileName(),DeliveryLitePlugin.RUN_NEAR_REAL);
        			nearInternalFrame.getStackPanel().redrawWigglePane(hScale,verticalScale);
        			if(farInternalFrame.getStackPanel() != null && farInternalFrame.getStackPanel().isVisible())
        				farInternalFrame.getStackPanel().redrawWigglePane(hScale,verticalScale);
        	}
        });
        verticalScaleImageButton = new javax.swing.JButton();

        verticalScaleImageButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		String val = verticalScaleTextField.getText().trim();
        		if(val.length() == 0){
        			JOptionPane.showMessageDialog(comp, "Vertical scale value field is empty.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
        			return;
        		}
        		float fval = 0;
        		try{
        			fval = Float.parseFloat(val);
        		}catch(NumberFormatException nfe){
        			JOptionPane.showMessageDialog(comp, "Vertical scale value must be a number.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
        			return;
        		}
        		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
        		verticalScale*=fval;
        		parentGui.getShowModelParams().setVerticalScale(verticalScale);
        		modelBeforeInternalFrame.refreshModelCanvas(verticalScale);
        		if(nearInternalFrame.getStackPanel() != null && nearInternalFrame.getStackPanel().isVisible())
        			//nearInternalFrame.getStackPanel().showWiggleTrace(verticalScale,1, parentGui.getNearStackFileName(),DeliveryLitePlugin.RUN_NEAR_REAL);
        			nearInternalFrame.getStackPanel().redrawWigglePane(1,verticalScale);
        		if(farInternalFrame.getStackPanel() != null && farInternalFrame.getStackPanel().isVisible())
        			farInternalFrame.getStackPanel().redrawWigglePane(1,verticalScale);

        	}
        });
        
        updateViewButton = new javax.swing.JButton();
        updateViewButton.setEnabled(false);
        frameHolderPane = new javax.swing.JDesktopPane();
        nearInternalFrame = new StackInternalFrame(parentGui,this);
        
        //farInternalFrame = new BaseInternalFrame(this);
        farInternalFrame = new StackInternalFrame(parentGui,this);
        buttonModelPanel = new javax.swing.JPanel();
        runPriorInversionButton = new javax.swing.JButton();
        runPriorInversionButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		parentGui.runPriorInversionButtonActionPerformed();
        		setRunButtonsEnabled(false);
        	}
        });
        runPostInversionButton = new javax.swing.JButton();
        runPostInversionButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		parentGui.runPostInversionButtonActionPerformed();
        		setRunButtonsEnabled(false);
        	}
        });
        makeHistogramButton = new javax.swing.JButton();
        makeHistogramButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeHistogramActionPerformed(e);
        	}
        });
        makeFullAsciiButton = new javax.swing.JButton();
        makeFullAsciiButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeFullAsciiPerformed(e);
        	}
        });
        
        makeScatterPlotButton = new javax.swing.JButton();
        makeScatterPlotButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeScatterPlotActionPerformed(e);
        	}
        });
        
        makeSpaghettiPlotButton = new javax.swing.JButton();
        makeSpaghettiPlotButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeSpaghettiPlotActionPerformed(e);
        	}
        });
        closeButton = new javax.swing.JButton();
        showModelMenuBar = new javax.swing.JMenuBar();
        actionMenu = new javax.swing.JMenu();
        runPriorInversionMenuItem = new javax.swing.JMenuItem();
        runPriorInversionMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		parentGui.runPriorInversionButtonActionPerformed();
        		setRunButtonsEnabled(false);
        	}
        });
        
        runPostInversionMenuItem = new javax.swing.JMenuItem();
        runPostInversionMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		parentGui.runPostInversionButtonActionPerformed();
        		setRunButtonsEnabled(false);
        	}
        });
        makeHistogramMenuItem = new javax.swing.JMenuItem();
        makeHistogramMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeHistogramActionPerformed(e);
        	}
        });
        makeScatterPlotMenuItem = new javax.swing.JMenuItem();
        makeScatterPlotMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeScatterPlotActionPerformed(e);
        	}
        });
        makeSpaghettiPlotMenuItem = new javax.swing.JMenuItem();
        makeSpaghettiPlotMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeSpaghettiPlotActionPerformed(e);
        	}
        });
        fullAsciiOutputMenuItem  = new javax.swing.JMenuItem();
        fullAsciiOutputMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeFullAsciiPerformed(e);
        	}
        });
        exitMenuItem = new javax.swing.JMenuItem();
        exitMenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		closeButtonActionPerformed(e);
        	}
        });
        ViewMenu = new javax.swing.JMenu();
        viewNearCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        viewNearCheckBoxMenuItem.setSelected(true);
        viewNearCheckBoxMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(viewNearCheckBoxMenuItem.isSelected())
					nearInternalFrame.setVisible(true);
				else
					nearInternalFrame.setVisible(false);
			}
		});
        viewFarCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        viewFarCheckBoxMenuItem.setSelected(true);
        viewFarCheckBoxMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(viewFarCheckBoxMenuItem.isSelected())
					farInternalFrame.setVisible(true);
				else
					farInternalFrame.setVisible(false);
			}
		});
        viewModelAfterCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        viewModelAfterCheckBoxMenuItem.setSelected(true);
        viewModelAfterCheckBoxMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(viewModelAfterCheckBoxMenuItem.isSelected())
					modelAfterInternalFrame.setVisible(true);
				else
					modelAfterInternalFrame.setVisible(false);
			}
		});

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Show Model");
        setBackground(new java.awt.Color(102, 204, 255));
        getAccessibleContext().setAccessibleParent(this);
        modelControlPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Model Control Panel"));
        chooseModelBeforePropertyLabel.setText("Choose Property:");

        controlPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        
        
        
        modelAfterPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Model After"));
        //modelAfterDatasetComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "P10", "P90", "mean", "median", "stddev" }));
        modelAfterDatasetComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] {}));

        modelDatasetsLabel.setText("Model Datasets:");

        showModelAfterPropertyRadioButton.setText("Show Property Value");
        showModelAfterPropertyRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelAfterPropertyRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showModelAfterTimeRadioButton.setText("Show Time Value");
        showModelAfterTimeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelAfterTimeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showModelAfterSigmaTimeRadioButton.setText("Show Sigma Time");
        showModelAfterSigmaTimeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelAfterSigmaTimeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showModelAfterColorbarRadioButton.setText("Show Colorbar");
        showModelAfterColorbarRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelAfterColorbarRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        //modelAfterPropertyComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[0]));
        //checkModelAfterReady();
        Vector<String> v = new Vector<String>();
        v.add("Select One");
   		for(String s : MODEL_AFTER_DATASETS_ABBREV)
   			v.add(s);
   		modelAfterDatasetComboBox.setModel(new DefaultComboBoxModel(v));
        chooseModelAfterPropertyLabel.setText("Choose Property:");

        //modelAfterColorBarButton.setText("Colorbar");

        modelAfterColorBarButton.setToolTipText("Open Colorbar Editor");
        java.net.URL imgURL = ViewModelDialog.class.getResource("/images/colorbar_icon.png");
        if(imgURL != null){
        	modelAfterColorBarButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }

        
        org.jdesktop.layout.GroupLayout modelAfterPanelLayout = new org.jdesktop.layout.GroupLayout(modelAfterPanel);
        modelAfterPanel.setLayout(modelAfterPanelLayout);
        modelAfterPanelLayout.setHorizontalGroup(
            modelAfterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelAfterPanelLayout.createSequentialGroup()
                .add(modelAfterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(showModelAfterColorbarRadioButton)
                    .add(showModelAfterSigmaTimeRadioButton)
                    .add(showModelAfterTimeRadioButton)
                    .add(showModelAfterPropertyRadioButton)
                    .add(modelAfterPropertyComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(modelAfterPanelLayout.createSequentialGroup()
                        .add(modelAfterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(modelAfterPanelLayout.createSequentialGroup()
                                .add(modelDatasetsLabel)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, modelAfterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, modelAfterDatasetComboBox, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, chooseModelAfterPropertyLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .add(modelAfterColorBarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(9, Short.MAX_VALUE))
        );
        modelAfterPanelLayout.setVerticalGroup(
            modelAfterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelAfterPanelLayout.createSequentialGroup()
                .add(modelDatasetsLabel)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(2,2,2)
                .add(modelAfterPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(modelAfterColorBarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(modelAfterDatasetComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)
                .add(chooseModelAfterPropertyLabel)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(2,2,2)
                .add(modelAfterPropertyComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                //.add(4,4,4)
                .add(showModelAfterPropertyRadioButton)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                 .add(4,4,4)
                .add(showModelAfterTimeRadioButton)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)
                .add(showModelAfterSigmaTimeRadioButton)
                .add(4,4,4)                
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(showModelAfterColorbarRadioButton)
                .add(2,2,2))
        );

        modelBeforePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Model Before"));
        modelControlPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Model Control Panel"));
        chooseModelBeforePropertyLabel.setText("Choose Property:");
        String [] columnNames = new String[0];
        if(parentGui != null){
        	javax.swing.JTable table = parentGui.getLayerTable();
        	TableColumnModel colModel = table.getColumnModel();
        	columnNames = new String[colModel.getColumnCount()];
        	for(int i = 0; i < colModel.getColumnCount(); i++){
        		columnNames[i] = table.getColumnName(i);
        	}
        }
        modelBeforePropertyComboBox.setModel(new javax.swing.DefaultComboBoxModel(columnNames));
        String selectedProp = parentGui.getShowModelParams().getSelectedModelBeforeProperty();
        int index = -1;
        for(int i = 0; i < columnNames.length; i++){
        	if(selectedProp.equals(columnNames[i])){
        		index = i;
        		break;
        	}
        }
        	
        //if(columnNames.length > 3)
        if(index != -1)
			modelBeforePropertyComboBox.setSelectedIndex(index); //set default property N/G
		modelBeforePropertyComboBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String selected = (String)modelBeforePropertyComboBox.getSelectedItem();
				modelBeforeInternalFrame.setSelectedProperty(selected);
				parentGui.getShowModelParams().setSelectedModelBeforeProperty(selected);
				float verticalScale = parentGui.getShowModelParams().getVerticalScale();
				modelBeforeInternalFrame.refreshModelCanvas(verticalScale);
			}
		});

        showModelBeforePropertyRadioButton.setText("Show Property Value");
        showModelBeforePropertyRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelBeforePropertyRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showModelBeforeTimeRadioButton.setText("Show Time Value");
        showModelBeforeTimeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelBeforeTimeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showModelBeforeSigmaTimeRadioButton.setText("Show Sigma Time");
        showModelBeforeSigmaTimeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelBeforeSigmaTimeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        //autoFitRadioButton.setText("Fit the Page");
        //fitPageButton.setText("Fit Page");

        fitPageButton.setToolTipText("View all in the panel");
        imgURL = ViewModelDialog.class.getResource("/images/ViewAll.gif");
        if(imgURL != null){
        	fitPageButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }
        
        zoomInButton.setToolTipText("Zoom in vertical display by 20%");
        imgURL = ViewModelDialog.class.getResource("/images/ZoomIn.gif");
        if(imgURL != null){
        	zoomInButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }
        
        
        zoomOutButton.setToolTipText("Zoom out vertical display by 20%");
        imgURL = ViewModelDialog.class.getResource("/images/ZoomOut.gif");
        if(imgURL != null){
        	zoomOutButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }

        //autoFitRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        //autoFitRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showModelBeforeColorbarRadioButton.setText("Show Colorbar");
        showModelBeforeColorbarRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showModelBeforeColorbarRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        //modelBeforeColorBarButton.setText("Colorbar");
        modelBeforeColorBarButton.setToolTipText("Open Colorbar Editor");
        imgURL = ViewModelDialog.class.getResource("/images/colorbar_icon.png");
        if(imgURL != null){
        	modelBeforeColorBarButton.setIcon(new javax.swing.ImageIcon(imgURL));
        }
        //Vector<Vector> model = parentGui.getLayerData();
		//float minTime = Float.parseFloat((String)(model.get(0).get(4)));
		//float maxTime = Float.parseFloat(parentGui.getTimeBase());
		//int modelH = model.size() * defaultLayerHeight;
		//float unit = (float)modelH / (maxTime - minTime);
        
        
        DeliveryLiteUI dl = (DeliveryLiteUI)parentGui;
        modelAfterInternalFrame = new ModelAfterInversionInternalFrame(dl, this);
        ModelDataset modelBefore = new ModelDataset(dl.getLayerTable().getModel(),dl.getLayerBoundries());
        modelBeforeInternalFrame = new ModelBeforeInversionInternalFrame(dl, this,modelBeforePropertyColorMap, (String)modelBeforePropertyComboBox.getSelectedItem(),modelBefore);


        org.jdesktop.layout.GroupLayout modelBeforePanelLayout = new org.jdesktop.layout.GroupLayout(modelBeforePanel);
        modelBeforePanel.setLayout(modelBeforePanelLayout);
        modelBeforePanelLayout.setHorizontalGroup(
            modelBeforePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelBeforePanelLayout.createSequentialGroup()
                .add(modelBeforePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(showModelBeforeTimeRadioButton)
                    .add(showModelBeforeSigmaTimeRadioButton))
                .add(29, 29, 29))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, modelBeforePanelLayout.createSequentialGroup()
                .add(modelBeforePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(modelBeforePanelLayout.createSequentialGroup()
                        .add(modelBeforePropertyComboBox, 0, 103, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                    .add(modelBeforePanelLayout.createSequentialGroup()
                        .add(chooseModelBeforePropertyLabel)
                        .add(3, 3, 3)))
                .add(modelBeforeColorBarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(43, 43, 43))
            .add(modelBeforePanelLayout.createSequentialGroup()
                .add(zoomInButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(zoomOutButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fitPageButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(88, 88, 88))
            .add(modelBeforePanelLayout.createSequentialGroup()
                .add(showModelBeforeColorbarRadioButton)
                .addContainerGap())
            .add(modelBeforePanelLayout.createSequentialGroup()
                .add(showModelBeforePropertyRadioButton)
                .addContainerGap())
        );
        modelBeforePanelLayout.setVerticalGroup(
            modelBeforePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelBeforePanelLayout.createSequentialGroup()
                .add(chooseModelBeforePropertyLabel)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(2,2,2)
                .add(modelBeforePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(modelBeforeColorBarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(modelBeforePropertyComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(showModelBeforePropertyRadioButton)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)                
                .add(showModelBeforeTimeRadioButton)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)                
                .add(showModelBeforeSigmaTimeRadioButton)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)                
                .add(showModelBeforeColorbarRadioButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(modelBeforePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(zoomInButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(zoomOutButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(fitPageButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );
        
        org.jdesktop.layout.GroupLayout modelControlPanelLayout = new org.jdesktop.layout.GroupLayout(modelControlPanel);
        modelControlPanel.setLayout(modelControlPanelLayout);
        modelControlPanelLayout.setHorizontalGroup(
            modelControlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelBeforePanel, 0, 164, Short.MAX_VALUE)
            .add(modelAfterPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
        );
        modelControlPanelLayout.setVerticalGroup(
            modelControlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelControlPanelLayout.createSequentialGroup()
                .add(modelBeforePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(4, 4, 4)
                .add(modelAfterPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        seismicControlPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Seismic Control Panel"));
        showSyntheticBeforeRadioButton.setText("Show Synthetic (Before)");
        showSyntheticBeforeRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showSyntheticBeforeRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showSyntheticAfterRadioButton.setText("Show Synthetic (After)");
        showSyntheticAfterRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showSyntheticAfterRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        showRealSeismicRadioButton.setText("Show Real");
        showRealSeismicRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showRealSeismicRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        
        showFillRadioButton.setText("fill color");
        showFillRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        showFillRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        
        updateViewButton.setText("Update Views");
        
        traceMaxValueLabel.setText("Trace Max Value:");
        
        traceMaxValueTextField.setText("      ");
        traceMaxValueTextField.setEditable(false);
        //traceMaxValue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        newTraceMaxValueLabel.setText("Max Value:");
        imgURL = ViewModelDialog.class.getResource("/images/ZoomRubber.gif");
        if(imgURL != null){
        	//newTraceMaxValueImageButton.setIcon(new javax.swing.ImageIcon(imgURL));
            horizontalScaleImageButton.setIcon(new javax.swing.ImageIcon(imgURL));
            verticalScaleImageButton.setIcon(new javax.swing.ImageIcon(imgURL));

        }
        scalePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Scale Multiplier", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11)));
        verticalScaleLabel.setText("Vertical:");

        horizontalScaleLabel.setText("Horizontal:");


        org.jdesktop.layout.GroupLayout scalePanelLayout = new org.jdesktop.layout.GroupLayout(scalePanel);
        scalePanel.setLayout(scalePanelLayout);
        scalePanelLayout.setHorizontalGroup(
            scalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scalePanelLayout.createSequentialGroup()
                .add(scalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(verticalScaleLabel)
                    .add(horizontalScaleLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(scalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(verticalScaleTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(horizontalScaleTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(scalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(verticalScaleImageButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(horizontalScaleImageButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        scalePanelLayout.setVerticalGroup(
            scalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scalePanelLayout.createSequentialGroup()
                .add(scalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(verticalScaleLabel)
                    .add(verticalScaleTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(verticalScaleImageButton))
                .add(11, 11, 11)
                .add(scalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(horizontalScaleLabel)
                    .add(horizontalScaleTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(horizontalScaleImageButton))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout seismicControlPanelLayout = new org.jdesktop.layout.GroupLayout(seismicControlPanel);
        seismicControlPanel.setLayout(seismicControlPanelLayout);
        seismicControlPanelLayout.setHorizontalGroup(
            seismicControlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(showSyntheticAfterRadioButton)
            .add(traceMaxValueLabel)
            .add(seismicControlPanelLayout.createSequentialGroup()
                .add(newTraceMaxValueLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(newTraceMaxValueImageButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
            .add(newTraceMaxValueTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 136, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(traceMaxValueTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 136, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(seismicControlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                .add(org.jdesktop.layout.GroupLayout.LEADING, seismicControlPanelLayout.createSequentialGroup()
                    .add(showRealSeismicRadioButton)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(showFillRadioButton))
                .add(org.jdesktop.layout.GroupLayout.LEADING, showSyntheticBeforeRadioButton))
        );
        seismicControlPanelLayout.setVerticalGroup(
            seismicControlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(seismicControlPanelLayout.createSequentialGroup()
                .add(seismicControlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(showRealSeismicRadioButton)
                    .add(showFillRadioButton))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)                    
                .add(showSyntheticBeforeRadioButton)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)                
                .add(showSyntheticAfterRadioButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(traceMaxValueLabel)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)                
                .add(traceMaxValueTextField,org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(seismicControlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(newTraceMaxValueLabel)
                    .add(newTraceMaxValueImageButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(4,4,4)                    
                .add(newTraceMaxValueTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(2,2,2))
        );
        

        
        org.jdesktop.layout.GroupLayout controlPanelLayout = new org.jdesktop.layout.GroupLayout(controlPanel);
        controlPanel.setLayout(controlPanelLayout);
        controlPanelLayout.setHorizontalGroup(
            controlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelControlPanel, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(seismicControlPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        controlPanelLayout.setVerticalGroup(
            controlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(controlPanelLayout.createSequentialGroup()
                .add(modelControlPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(2,2,2)
                .add(seismicControlPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        frameHolderPane.setLayout(null);
        frameHolderPane.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        farInternalFrame.setResizable(true);
        farInternalFrame.setTitle("Far");
        farInternalFrame.setVisible(true);
        
        frameHolderPane.add(farInternalFrame);
        farInternalFrame.setSize(new Dimension(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT));
        farInternalFrame.setLocation(x0,y0);
        //farInternalFrame.setBounds(x0, y0, DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);

        nearInternalFrame.setResizable(true);
        nearInternalFrame.setTitle("Near");
        nearInternalFrame.setVisible(true);
        frameHolderPane.add(nearInternalFrame);
        nearInternalFrame.setSize(new Dimension(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT));
        nearInternalFrame.setLocation(x0+DEFAULT_FRAME_WIDTH, y0);
        //nearInternalFrame.setBounds(x0+DEFAULT_FRAME_WIDTH, y0, DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
        
        modelBeforeInternalFrame.setResizable(true);
        modelBeforeInternalFrame.setTitle("Model (Before)");
        modelBeforeInternalFrame.setVisible(true);
        frameHolderPane.add(modelBeforeInternalFrame);
        modelBeforeInternalFrame.setSize(new Dimension(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT));
        modelBeforeInternalFrame.setLocation(x0+2*DEFAULT_FRAME_WIDTH, y0);
        //modelBeforeInternalFrame.setBounds(x0+2*DEFAULT_FRAME_WIDTH, y0, DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);

        modelAfterInternalFrame.setResizable(true);
        modelAfterInternalFrame.setTitle("Model (After)");
        modelAfterInternalFrame.setVisible(true);
        
        frameHolderPane.add(modelAfterInternalFrame);
        modelAfterInternalFrame.setSize(new Dimension(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT));
        modelAfterInternalFrame.setLocation(x0+3*DEFAULT_FRAME_WIDTH, y0);
        //modelAfterInternalFrame.setBounds(x0+3*DEFAULT_FRAME_WIDTH, y0, DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
        
        
        buttonModelPanel.setBackground(new java.awt.Color(255, 204, 102));
        buttonModelPanel.setPreferredSize(new java.awt.Dimension(600, 35));
        runPriorInversionButton.setText("Run Prior");
        buttonModelPanel.add(runPriorInversionButton);

        runPostInversionButton.setText("Run Post Inversion");
        buttonModelPanel.add(runPostInversionButton);

        makeHistogramButton.setText("Make Histogram");
        buttonModelPanel.add(makeHistogramButton);

        makeScatterPlotButton.setText("Make Scatter Plot");
        buttonModelPanel.add(makeScatterPlotButton);

        makeSpaghettiPlotButton.setText("Make Spaghetti Plot");
        buttonModelPanel.add(makeSpaghettiPlotButton);
        
        makeFullAsciiButton.setText("Full ASCII Output");
        buttonModelPanel.add(makeFullAsciiButton);
        
        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        buttonModelPanel.add(closeButton);

        showModelMenuBar.setBackground(new java.awt.Color(255, 255, 153));
        actionMenu.setText("Action");
        runPriorInversionMenuItem.setText("Run Prior");
        actionMenu.add(runPriorInversionMenuItem);

        runPostInversionMenuItem.setText("Run Post");
        runPostInversionMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //runPostInversionMenuItemActionPerformed(evt);
            }
        });

        actionMenu.add(runPostInversionMenuItem);

        makeHistogramMenuItem.setText("Make Histogram");
        actionMenu.add(makeHistogramMenuItem);

        makeScatterPlotMenuItem.setText("Make Scatter Plot");
        actionMenu.add(makeScatterPlotMenuItem);

        makeSpaghettiPlotMenuItem.setText("Make Spaghetti Plot");
        actionMenu.add(makeSpaghettiPlotMenuItem);
        fullAsciiOutputMenuItem.setText("Full ASCII Output");
        actionMenu.add(fullAsciiOutputMenuItem);
        exitMenuItem.setText("Exit");
        actionMenu.add(exitMenuItem);

        showModelMenuBar.add(actionMenu);

        ViewMenu.setText("View");
        viewNearCheckBoxMenuItem.setText("Show Near Panel");
        ViewMenu.add(viewNearCheckBoxMenuItem);

        viewFarCheckBoxMenuItem.setText("Show Far Panel");
        ViewMenu.add(viewFarCheckBoxMenuItem);

        viewModelAfterCheckBoxMenuItem.setText("Show Model After Inverstion");
        ViewMenu.add(viewModelAfterCheckBoxMenuItem);

        showModelMenuBar.add(ViewMenu);

        setJMenuBar(showModelMenuBar);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(controlPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(frameHolderPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 879, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonModelPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1073, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(controlPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
                    .add( frameHolderPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE))
                //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(buttonModelPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        pack();
    }// </editor-fold>

    private void newTraceMaxValueButtonPerformed(){
    	if(newTraceMaxValueTextField.isEditable() == false)
			return;
		String val = newTraceMaxValueTextField.getText().trim();
		if(val.length() == 0){
			JOptionPane.showMessageDialog(this, "Trace Max Value field is empty.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
			newTraceMaxValueTextField.requestFocus();
			return;
		}
		float newFval = 0;
		try{
			newFval = Float.parseFloat(val);
		}catch(NumberFormatException nfe){
			JOptionPane.showMessageDialog(this, "Trace Max Value field must be a number.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
			return;
		}
		if(newFval == 0)
			return;
		if(currentMaxTraceValue == -1 || currentMaxTraceValue == 0)
			return;
		
		float horizontalScale = currentMaxTraceValue / newFval;
		parentGui.getShowModelParams().setNewMaxAmplitude(newFval);
		parentGui.getShowModelParams().setHorizontalScale(horizontalScale);
		if(nearInternalFrame.getStackPanel() != null && nearInternalFrame.getStackPanel().isVisible())
			//nearInternalFrame.getStackPanel().showWiggleTrace(1,horizontalScale, parentGui.getNearStackFileName(),DeliveryLitePlugin.RUN_NEAR_REAL);
			nearInternalFrame.getStackPanel().redrawWigglePane(horizontalScale,1);
		if(parentGui.isNearAndFar())
			if(farInternalFrame.getStackPanel() != null && farInternalFrame.getStackPanel().isVisible())
    			farInternalFrame.getStackPanel().redrawWigglePane(horizontalScale,1);
    }
    
    private void makeSpaghettiPlotActionPerformed(ActionEvent e){
		new RunPlotDialog(parentGui, false, "Make Spaghetti Plot Dialog", getStackType(),
				DeliveryLiteConstants.MAKE_SPAGHETTI_PLOT).setVisible(true);
    }
    
    public List<Component> getListOfRunComponents(){
    	List<Component> list = new ArrayList<Component>();
    	list.add(runPriorInversionButton);
    	list.add(runPriorInversionMenuItem);
    	list.add(runPostInversionButton);
    	list.add(runPostInversionMenuItem);
    	return list;
    }
    
    public void setListOfRunComponentsEnabled(boolean b){
    	runPriorInversionButton.setEnabled(b);
    	runPriorInversionMenuItem.setEnabled(b);
    	runPostInversionButton.setEnabled(b);
    	runPostInversionMenuItem.setEnabled(b);
    }
    
    private void makeHistogramActionPerformed(ActionEvent e){
    	String [] properties = parentGui.getDeliveryAnalyserProperties();
		final int layer = parentGui.getNumberOfCurrentLayers();
		if(properties != null && properties.length > 0){
			List<String> list = Arrays.asList(properties);
			new RunPlotDialog(parentGui, false, "Make Histogram Plot Dialog", list, layer, 1, 
					getStackType(),DeliveryLiteConstants.MAKE_HISTORGRAM_PLOT).setVisible(true);
		}else{
			runDeliveryPropertiesWorker("Make Histogram Plot Dialog",layer,1,getStackType(),DeliveryLiteConstants.MAKE_HISTORGRAM_PLOT);
	    }
    }
    
    private void makeFullAsciiPerformed(ActionEvent e){
    	new RunPlotDialog(parentGui, false, "Make Full ASCII Output Dialog", 
   				getStackType(),DeliveryLiteConstants.MAKE_FULL_ASCII_OUTPUT).setVisible(true);
    }
    
    private void makeScatterPlotActionPerformed(ActionEvent e){
    	String [] properties = parentGui.getDeliveryAnalyserProperties();
		//int layer = parentGui.getNumberOfInitialLayers();
        int layer = parentGui.getNumberOfCurrentLayers();
		if(properties != null && properties.length > 0){
			List<String> list = Arrays.asList(properties);
			new RunPlotDialog(parentGui, false, "Make Scatter Plot Dialog", list, 
					layer, 2, getStackType(),DeliveryLiteConstants.MAKE_SCATTER_PLOT).setVisible(true);
		}else{
			runDeliveryPropertiesWorker("Make Scatter Plot Dialog",layer,2,getStackType(),DeliveryLiteConstants.MAKE_SCATTER_PLOT);
		}
    }
    
    private void showModelBeforeSigmaTimeRadioButtonActionPerformed(){
    	if(showModelBeforeSigmaTimeRadioButton.isSelected()){
			parentGui.getShowModelParams().setShowModelBeforeSigmaTimeEnabled(true);
			modelBeforeInternalFrame.setShowSigmaTimeValue(true);
		}else{
			modelBeforeInternalFrame.setShowSigmaTimeValue(false);
			parentGui.getShowModelParams().setShowModelBeforeSigmaTimeEnabled(false);
		}
		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
		modelBeforeInternalFrame.refreshModelCanvas(verticalScale);
    }
    
    private void showModelBeforeTimeRadioButtonActionPerformed(){
    	if(showModelBeforeTimeRadioButton.isSelected()){
			parentGui.getShowModelParams().setShowModelBeforeTimeValueEnabled(true);
			modelBeforeInternalFrame.setShowTimeValue(true);
		}else{
			modelBeforeInternalFrame.setShowTimeValue(false);
			parentGui.getShowModelParams().setShowModelBeforeTimeValueEnabled(false);
		}
		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
		modelBeforeInternalFrame.refreshModelCanvas(verticalScale);
    }
    
    public void setRunButtonsEnabled(boolean b){
    	runPostInversionButton.setEnabled(b);
    	runPostInversionMenuItem.setEnabled(b);
    	runPriorInversionButton.setEnabled(b);
    	runPriorInversionMenuItem.setEnabled(b);
    }
    
    private void showModelBeforePropertyRadioButtonActionPerformed(){
    	if(showModelBeforePropertyRadioButton.isSelected()){
			modelBeforeInternalFrame.setShowPropertyValue(true);
			parentGui.getShowModelParams().setShowModelBeforePropertyValueEnabled(true);
		}else{
			modelBeforeInternalFrame.setShowPropertyValue(false);
			parentGui.getShowModelParams().setShowModelBeforePropertyValueEnabled(false);
		}
		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
		modelBeforeInternalFrame.refreshModelCanvas(verticalScale);    	
    }
    
    private void showModelAfterSigmaTimeRadioButtonActionPerformed(){
		if(modelAfterDatasetComboBox.getSelectedIndex() < 1)
			return;
		if(modelAfterPropertyComboBox.getSelectedIndex() < 1)
			return;
        String datasetType = (String)modelAfterDatasetComboBox.getSelectedItem();
        DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), datasetType);
        String modelName = helper.getModelDatasetPrefix(parentGui.isNearAndFar());
        ModelDataset dataset = parentGui.getModelDatasetByName(modelName);
        String stddevName = helper.getStddevModelDatasetPrefix(parentGui.isNearAndFar());
		ModelDataset datasetStdev = parentGui.getModelDatasetByName(stddevName);
		if(dataset == null)
			return;
		if(showModelAfterSigmaTimeRadioButton.isSelected()){
			modelAfterInternalFrame.setShowSigmaTimeValue(true);
			parentGui.getShowModelParams().setShowModelAfterSigmaTimeEnabled(true);
		}else{
			modelAfterInternalFrame.setShowSigmaTimeValue(false);
			parentGui.getShowModelParams().setShowModelAfterSigmaTimeEnabled(false);
		}
		
		String key = (String)modelAfterPropertyComboBox.getSelectedItem();
		Property property = dataset.getPropertyByName(key);
		setModelAfterPropertyColorbar(property);
		Property stddevProperty = datasetStdev.getPropertyByName("t");
		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
		modelAfterInternalFrame.refreshModelCanvas(verticalScale, dataset, property,stddevProperty);
    }
    
    private void showModelAfterTimeRadioButtonActionPerformed(){
		if(modelAfterDatasetComboBox.getSelectedIndex() < 1)
			return;
		if(modelAfterPropertyComboBox.getSelectedIndex() < 1)
			return;
		String datasetType = (String)modelAfterDatasetComboBox.getSelectedItem();
        DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), datasetType);
        String modelName = helper.getModelDatasetPrefix(parentGui.isNearAndFar());
        ModelDataset dataset = parentGui.getModelDatasetByName(modelName);
        String stddevName = helper.getStddevModelDatasetPrefix(parentGui.isNearAndFar());
		ModelDataset datasetStdev = parentGui.getModelDatasetByName(stddevName);
		if(dataset == null)
			return;
		if(showModelAfterTimeRadioButton.isSelected()){
			modelAfterInternalFrame.setShowTimeValue(true);
			parentGui.getShowModelParams().setShowModelAfterTimeValueEnabled(true);
		}else{
			modelAfterInternalFrame.setShowTimeValue(false);
			parentGui.getShowModelParams().setShowModelAfterTimeValueEnabled(false);
		}
		
		String key = (String)modelAfterPropertyComboBox.getSelectedItem();
		Property property = dataset.getPropertyByName(key);
		setModelAfterPropertyColorbar(property);
		Property stddevProperty = datasetStdev.getPropertyByName("t");
		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
		modelAfterInternalFrame.refreshModelCanvas(verticalScale, dataset, property,stddevProperty);
    }
    
    private void showModelAfterPropertyRadioButtonActionPerformed(){
    	int modelAfterDatasetIndex = modelAfterDatasetComboBox.getSelectedIndex();
    	int modelAfterPropertyIndex = modelAfterPropertyComboBox.getSelectedIndex();
    	if(modelAfterDatasetIndex < 1)
			return;
		if(modelAfterPropertyIndex < 1)
			return;
		parentGui.setModelAfterDatasetIndex(modelAfterDatasetIndex);
		parentGui.setModelAfterPropertyIndex(modelAfterPropertyIndex);
		ModelDataset dataset = getSelectedModelAfterDataset();
		if(dataset == null)
			return;
		if(showModelAfterPropertyRadioButton.isSelected()){
			modelAfterInternalFrame.setShowPropertyValue(true);
			parentGui.getShowModelParams().setShowModelAfterPropertyValueEnabled(true);
		}else{
			modelAfterInternalFrame.setShowPropertyValue(false);
			parentGui.getShowModelParams().setShowModelAfterPropertyValueEnabled(false);
		}
		
		String key = (String)modelAfterPropertyComboBox.getSelectedItem();
		Property property = dataset.getPropertyByName(key);
		setModelAfterPropertyColorbar(property);
		//modelAfterInternalFrame.refreshModelCanvas(verticalScale, dataset, property,stddevProperty);
		modelAfterInternalFrame.setScrollValue(parentGui.getShowModelParams().getVerticalScrollPosition());
		//modelAfterInternalFrame.getModelCanvas().scrollRectToVisible(parentGui.getShowModelParams().getScrollToVisible());
    }
    
    private void showModelAfterColorbarRadioButtonActionPerformed(){
		if(modelAfterDatasetComboBox.getSelectedIndex() < 1)
			return;
		if(modelAfterPropertyComboBox.getSelectedIndex() < 1)
			return;


        String datasetType = (String)modelAfterDatasetComboBox.getSelectedItem();
        DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), datasetType);
        String modelName = helper.getModelDatasetPrefix(parentGui.isNearAndFar());
        ModelDataset dataset = parentGui.getModelDatasetByName(modelName);
        String stddevName = helper.getStddevModelDatasetPrefix(parentGui.isNearAndFar());
		ModelDataset datasetStdev = parentGui.getModelDatasetByName(stddevName);
		if(dataset == null)
			return;

		if(showModelAfterColorbarRadioButton.isSelected()){
			modelAfterInternalFrame.setShowColorbar(true);
			parentGui.getShowModelParams().setShowModelAfterColorbarEnabled(true);
		}else{
			modelAfterInternalFrame.setShowColorbar(false);
			parentGui.getShowModelParams().setShowModelAfterColorbarEnabled(false);
		}
		String key = (String)modelAfterPropertyComboBox.getSelectedItem();
		Property property = dataset.getPropertyByName(key);
		setModelAfterPropertyColorbar(property);
		Property stddevProperty = datasetStdev.getPropertyByName("t");
		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
		modelAfterInternalFrame.refreshModelCanvas(verticalScale, dataset, property,stddevProperty);
    }

    private void refreshCurrentUnlabeledColorbarMaps(){
    	List<Colorbar1> list = parentGui.getShowModelParams().getNonPredefinedColorbarList();
    	for(Colorbar1 cb : list){
    		if(!currentUnlabeledColorbarMap.containsKey(cb.getName()))
    			currentUnlabeledColorbarMap.put(cb.getName(), cb);
    	}
    	
    }
    
    private void modelBeforeColorbarActionPerformed(){
    	List<String> properies = new ArrayList<String>();
    	JTable layerTable = parentGui.getLayerTable();
    	for(int i = 0; i <layerTable.getColumnCount(); i++){
        	String key = layerTable.getColumnName(i);
        	properies.add(key);
    	}
    	javax.swing.JDialog dialog = null;
    	//Component parentComponentRoot = SwingUtilities.getRoot(this);
        Component parentComponentRoot = this;
    	//refreshCurrentUnlabeledColorbarMaps();
    	modelAfterDatasetPropertyColorMap.put("Input Model", modelBeforePropertyColorMap);
    	if(parentComponentRoot instanceof Frame) {
    		dialog = new PropertyColorbarEditDialog((Frame)parentComponentRoot, MODEL_BEFORE, properies, "Input Model", true, modelAfterDatasetPropertyColorMap,currentUnlabeledColorbarMap);
        } else if(parentComponentRoot instanceof Dialog) {
        	dialog = new PropertyColorbarEditDialog((Dialog)parentComponentRoot, MODEL_BEFORE, properies, "Input Model", true, modelAfterDatasetPropertyColorMap,currentUnlabeledColorbarMap);
        } else
            dialog = new PropertyColorbarEditDialog(this, MODEL_BEFORE, properies, "Input Model", true, modelAfterDatasetPropertyColorMap,currentUnlabeledColorbarMap);

    	//javax.swing.JDialog dialog = new PropertyColorbarEditDialog(this, MODEL_BEFORE, properies, "Input Model", true,modelBeforePropertyColorMap,currentUnlabeledColorbarMap);
    	if(dialog != null){
	    	dialog.setLocationRelativeTo(this);
	    	dialog.setVisible(true);
    	}
    }

    public void restoreShowModel(final ShowModelParams params){
    	
    	if(params.isShowRealStackEnabled()){
    		showRealSeismicRadioButtonActionPerformed();
    	}
    	if(params.isShowSyntheticBeforeEnabled()){
    		showSyntheticBeforeRadioButtonActionPerformed();
    	}
    	if(params.isShowSyntheticAfterEnabled()){
    		showSyntheticAfterRadioButtonActionPerformed();
    	}
    	float amp = params.getNewMaxAmplitude();
    	if(amp > 0){
    		newTraceMaxValueTextField.setText(String.valueOf(amp));
    	}
    	
    	restoreParams(params);
    	String dataset = params.getSelectedModelAfterDataset();
		if(dataset != null && dataset.length() > 0 ){
    		ComboBoxModel model = modelAfterDatasetComboBox.getModel();
    		for(int i = 0; i < model.getSize(); i++){
    			if(model.getElementAt(i).equals(dataset)){
    				modelAfterDatasetComboBox.setSelectedIndex(i);
    				break;
    			}
    		}
    	}
		if(amp != 0f){
			newTraceMaxValueButtonPerformed();
		}
    }
    
    private void modelAfterColorbarActionPerformed(){
    	if(modelAfterDatasetComboBox.getSelectedIndex() <= 0)
    		return;
    	String s = (String)modelAfterDatasetComboBox.getSelectedItem();
        DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), s);
    	String modeAfterdDatasetBase =  helper.getModelDatasetPrefix(parentGui.isNearAndFar());

    	ModelFileMetadata metadata = (ModelFileMetadata)parentGui.getMetadataByDataset(modeAfterdDatasetBase);
    	
    	List<String> properies = metadata.getProperties();
    	//Component parentComponentRoot = SwingUtilities.getRoot(this);
        Component parentComponentRoot = this;
    	javax.swing.JDialog dialog = null;
    	//refreshCurrentUnlabeledColorbarMaps();
    	modelAfterDatasetPropertyColorMap.put("Input Model", modelBeforePropertyColorMap);
    	if(parentComponentRoot instanceof Dialog) {
    		dialog = new PropertyColorbarEditDialog((Dialog)parentComponentRoot, MODEL_AFTER, properies, modeAfterdDatasetBase, true, modelAfterDatasetPropertyColorMap,currentUnlabeledColorbarMap);
        } else if(parentComponentRoot instanceof Frame) {
        	dialog = new PropertyColorbarEditDialog((Frame)parentComponentRoot, MODEL_AFTER, properies, modeAfterdDatasetBase, true, modelAfterDatasetPropertyColorMap,currentUnlabeledColorbarMap);
        } else
            dialog = new PropertyColorbarEditDialog(this, MODEL_AFTER, properies, modeAfterdDatasetBase, true, modelAfterDatasetPropertyColorMap,currentUnlabeledColorbarMap);

    	if(dialog != null){
	    	dialog.setLocationRelativeTo(this);
	    	dialog.setVisible(true);
    	}
    }    
    private String getStackType(){
    	if(parentGui.isNearAndFar())
    		return "near_far";
    	else
    		return "near";
    }
    

    public ModelDataset getSelectedModelAfterDataset(){
    	String s = (String)modelAfterDatasetComboBox.getSelectedItem();
        DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), s);
		String modelName = helper.getModelDatasetPrefix(parentGui.isNearAndFar());
		ModelDataset dataset = parentGui.getModelDatasetByName(modelName);
		return dataset;
    }

    private void showSyntheticBeforeRadioButtonActionPerformed(){
    	if(showSyntheticBeforeRadioButton.isSelected()){
			String nearDatasetName = "";
			String farDatasetName = "";
			SeismicFileMetadata nearMetadata;
			SeismicFileMetadata farMetadata;
			nearDatasetName = parentGui.getNearSyntheticBeforeDatasetName();
			nearMetadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(nearDatasetName);
			if(!getTraceData(nearDatasetName,nearMetadata, DeliveryLitePlugin.NEAR, DeliveryLitePlugin.SYNTHETIC_BEFORE, nearInternalFrame))
				return;
			if(parentGui.isNearAndFar()){
				farDatasetName = parentGui.getFarSyntheticBeforeDatasetName();
				farMetadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(farDatasetName);
				if(!getTraceData(farDatasetName,farMetadata, DeliveryLitePlugin.FAR, DeliveryLitePlugin.SYNTHETIC_BEFORE, farInternalFrame))
					return;
			}
			parentGui.getShowModelParams().setShowSyntheticBeforeEnabled(true);
		}else{
			float[] fs = new float[0];
			
			nearInternalFrame.getStackPanel().setTraceData(parentGui.getNearSyntheticBeforeDatasetName()
					,fs,DeliveryLitePlugin.NEAR,DeliveryLitePlugin.SYNTHETIC_BEFORE);

			if(parentGui.isNearAndFar()){
				farInternalFrame.getStackPanel().setTraceData(parentGui.getFarSyntheticBeforeDatasetName()
						,fs,DeliveryLitePlugin.FAR,DeliveryLitePlugin.SYNTHETIC_BEFORE);
			}
			parentGui.getShowModelParams().setShowSyntheticBeforeEnabled(false);
		}
    }
    
    private void showSyntheticAfterRadioButtonActionPerformed(){
		if(showSyntheticAfterRadioButton.isSelected()){
			String nearDatasetName = "";
			String farDatasetName = "";
			SeismicFileMetadata nearMetadata;
			SeismicFileMetadata farMetadata;
			nearDatasetName = parentGui.getNearSyntheticAfterDatasetName();
			nearMetadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(nearDatasetName);
			if(!getTraceData(nearDatasetName,nearMetadata, DeliveryLitePlugin.NEAR, DeliveryLitePlugin.SYNTHETIC_AFTER, nearInternalFrame))
				return;
			if(parentGui.isNearAndFar()){
				farDatasetName = parentGui.getFarSyntheticAfterDatasetName();
				farMetadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(farDatasetName);
				if(!getTraceData(farDatasetName,farMetadata, DeliveryLitePlugin.FAR, DeliveryLitePlugin.SYNTHETIC_AFTER, farInternalFrame))
					return;
			}
			parentGui.getShowModelParams().setShowSyntheticAfterEnabled(true);
		}else{
			float[] fs = new float[0];
			nearInternalFrame.getStackPanel().setTraceData(parentGui.getNearSyntheticAfterDatasetName()
					,fs,DeliveryLitePlugin.NEAR,DeliveryLitePlugin.SYNTHETIC_AFTER);
			if(parentGui.isNearAndFar()){
				farInternalFrame.getStackPanel().setTraceData(parentGui.getFarSyntheticAfterDatasetName()
						,fs,DeliveryLitePlugin.FAR,DeliveryLitePlugin.SYNTHETIC_AFTER);
			}
			parentGui.getShowModelParams().setShowSyntheticAfterEnabled(false);
		}
    }
    
    private void showRealSeismicRadioButtonActionPerformed(){
		
		if(showRealSeismicRadioButton.isSelected()){
			String nearDatasetName = "";
			String farDatasetName = "";
			SeismicFileMetadata nearMetadata;
			SeismicFileMetadata farMetadata;
			nearDatasetName = parentGui.getNearStackFileName();
			nearMetadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(nearDatasetName);
			if(!getTraceData(nearDatasetName,nearMetadata, DeliveryLitePlugin.NEAR, DeliveryLitePlugin.REAL, nearInternalFrame))
				return;
			if(parentGui.isNearAndFar()){
				farDatasetName = parentGui.getFarStackFileName();
				farMetadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(farDatasetName);
				if(!getTraceData(farDatasetName,farMetadata, DeliveryLitePlugin.FAR, DeliveryLitePlugin.REAL, farInternalFrame))
					return;
			}
			parentGui.getShowModelParams().setShowRealStackEnabled(true);
		}else{
			float[] fs = new float[0];
			nearInternalFrame.getStackPanel().setTraceData(parentGui.getNearStackFileName()
					,fs,DeliveryLitePlugin.NEAR,DeliveryLitePlugin.REAL);
			if(parentGui.isNearAndFar()){
				farInternalFrame.getStackPanel().setTraceData(parentGui.getFarStackFileName()
						,fs,DeliveryLitePlugin.FAR,DeliveryLitePlugin.REAL);
			}
			parentGui.getShowModelParams().setShowRealStackEnabled(false);
		}
		return;
    }
    
    
    public void restoreModelAfterPanel(int modelIndex, int propertyIndex){
    	modelAfterDatasetComboBox.setSelectedIndex(modelIndex);
    	modelAfterPropertyComboBox.setSelectedIndex(propertyIndex);
    }
    
    //private String selectedModelAfterProperty = "";
    public void modelAfterPropertyComboBoxActionPerformed(){
    	
    	if(modelAfterDatasetComboBox.getSelectedIndex() < 1)
    		return;
    	int index = modelAfterPropertyComboBox.getSelectedIndex();
    	if(index < 1){
    		parentGui.setModelAfterPropertyIndex(0);
    		parentGui.getShowModelParams().setSelectedModelAfterProperty("");
    		modelAfterInternalFrame.refreshModelCanvas(1, null, null,null);
    		return;
    	}
		String datasetType = (String)modelAfterDatasetComboBox.getSelectedItem();
        DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), datasetType);
        String modelName = helper.getModelDatasetPrefix(parentGui.isNearAndFar());
        ModelDataset dataset = parentGui.getModelDatasetByName(modelName);
        String stddevName = helper.getStddevModelDatasetPrefix(parentGui.isNearAndFar());
		ModelDataset datasetStdev = parentGui.getModelDatasetByName(stddevName);
		if(datasetStdev == null)
			return;
		String key = (String)modelAfterPropertyComboBox.getSelectedItem();
		Property property = modelAfterDatasetPropertyColorMap.get(dataset.getDatasetName()).get(key);
		Property stddevProperty = datasetStdev.getPropertyByName("t");
		if(stddevProperty == null)
			return;
		float verticalScale = parentGui.getShowModelParams().getVerticalScale();
		boolean selected = parentGui.getShowModelParams().isShowModelAfterPropertyValueEnabled();
		modelAfterInternalFrame.setShowPropertyValue(selected);
		selected = parentGui.getShowModelParams().isShowModelAfterTimeValueEnabled();
		modelAfterInternalFrame.setShowTimeValue(selected);		
		selected = parentGui.getShowModelParams().isShowModelAfterSigmaTimeEnabled();
		modelAfterInternalFrame.setShowSigmaTimeValue(selected);		
		selected = parentGui.getShowModelParams().isShowModelAfterColorbarEnabled();
		modelAfterInternalFrame.setShowColorbar(selected);		
		
		modelAfterInternalFrame.refreshModelCanvas(verticalScale, dataset, property,stddevProperty);

		parentGui.setModelAfterPropertyIndex(index);
		parentGui.getShowModelParams().setSelectedModelAfterProperty(key);
    }
    
    
    private synchronized boolean getTraceData(String dataset, SeismicFileMetadata metadata, int stackType, int runType, StackInternalFrame container){
    	if(metadata == null)
			parentGui.getAgent().getSeismicTraceByDataset(dataset,stackType,runType);
		else{
			String message = checkTimeRangeFit(metadata);
			if(message.length() > 0){
				JOptionPane.showMessageDialog(this, "The time information from the model does not fit the time range of the seismic trace.", "Data Mismatch.", JOptionPane.WARNING_MESSAGE);
				return false;
			}
			IDataObject[] dos = null;
			if((dos = parentGui.getDataObjectsByDataset(dataset)) != null){
				if(dos != null && dos.length >= 1 ){
					float[] data = dos[0].getTrace().getFloatVector();
					container.getStackPanel().setTraceData(dataset,data,stackType,runType);
				}else
					parentGui.getAgent().getSeismicTraceByMetaData1(metadata,stackType,runType);
			}else
				parentGui.getAgent().getSeismicTraceByMetaData1(metadata,stackType, runType);
		}
    	return true;
    }
    
    /**
     * keyed by model dataset name
     *	values are keyed by property name
     */
    private Map<String,Map<String,Property>> modelAfterDatasetPropertyColorMap;
    /**
     * Each property is initially assigned a default colorbar object.  Take loaded colorbar
     * objects and assign them to each property; find the minimum and maximun value for each
     * property, and form the colormap propertyColorMap
     */
    private void setModelAfterPropertyColorbar(Property property){
    	if(property == null)
    		return;
        float min = property.getMin();
        float max = property.getMax();
           	
        Colorbar1 colorbar = null;
        ModelDataset dataset = getSelectedModelAfterDataset();
        if(modelAfterDatasetPropertyColorMap.containsKey(dataset.getDatasetName())){
        	Map<String,Property> pMap = modelAfterDatasetPropertyColorMap.get(dataset.getDatasetName());
        	if(pMap.containsKey(property.getName())){
        		if(pMap.get(property.getName()).getColorbar() != null)
        			return;
        	}
        }
        HashMap<String,String> map = (HashMap)DeliveryLiteConstants.default_property_color_map.clone();
        
        if(map.containsKey(property.getName())){
           String colorbarName = map.get(property.getName());
           if(!currentUnlabeledColorbarMap.containsKey(colorbarName)){
               //colorbar = ColorbarUtil.getUnlabeledColorbarByName(colorbarName);
        	   ColorMap colorMap = null;
        	   try{
        		   colorMap = ColorbarUtil.getColorMapByResourceName(colorbarName);
        	   }catch(IOException e){
        		   e.printStackTrace();
        		   logger.warning("IO problem occurred while getting colormap object by using colorbar name=" + colorbarName);
        	   }
        	   colorbar = new Colorbar1(colorbarName,colorMap);
               if(colorbar != null){
                   currentUnlabeledColorbarMap.put(colorbarName, colorbar);
               }else{
                    colorbar = currentUnlabeledColorbarMap.get(colorbarName);
               }
            }else{
            	colorbar = currentUnlabeledColorbarMap.get(colorbarName);
            }
            Colorbar1 cb = new Colorbar1(colorbar);
            com.bhpb.deliverylite.colorbar.ColorbarUtil.labeledColorbar(cb, min, max);
            property.setColorbar(cb);
        }
    }

    
    private void modelAfterDatasetComboBoxActionPerformed(){
    	int index = modelAfterDatasetComboBox.getSelectedIndex();
    	if(index == 0){
    		modelAfterPropertyComboBox.setModel(new DefaultComboBoxModel(new Vector()));
    		modelAfterInternalFrame.refreshModelCanvas(1, null, null,null);
    	}
    	String s = "";
    	if(index > 0){
    		s = (String)modelAfterDatasetComboBox.getSelectedItem();

            DatasetNameHelper helper = new DatasetNameHelper(parentGui.getOutputBasename(), s);
    		String modeAfterdDatasetBase = helper.getModelDatasetPrefix(parentGui.isNearAndFar());
    		String modeAfterdDatasetBaseStddev = helper.getStddevModelDatasetPrefix(parentGui.isNearAndFar());
   			String tmpPath = QiProjectDescUtils.getDatasetsPath(parentGui.getAgent().getQiProjectDescriptor());
	   		String filesep = parentGui.getAgent().getMessagingMgr().getServerOSFileSeparator();
    		String modeAfterDatasePath = tmpPath +  filesep + modeAfterdDatasetBase + ".dat"; 
    		String modeAfterDataseStddevPath = tmpPath +  filesep + modeAfterdDatasetBaseStddev + ".dat";
   			String temp = parentGui.getAgent().checkFileExist(modeAfterDatasePath);
   			if(temp != null && !temp.equals("yes")){
   				JOptionPane.showMessageDialog(this, "Can not find dataset \"" + modeAfterdDatasetBase + "\"", 
   						"Dataset Not Found", JOptionPane.WARNING_MESSAGE);
   				modelAfterDatasetComboBox.setSelectedIndex(0);
   				return;
   			}
   			if(!modeAfterdDatasetBase.equals(modeAfterdDatasetBaseStddev)){
	   			temp = parentGui.getAgent().checkFileExist(modeAfterDataseStddevPath);
	   			if(temp != null && !temp.equals("yes")){
	   				JOptionPane.showMessageDialog(this, "Can not find dataset \"" + modeAfterdDatasetBaseStddev + "\"", 
	   						"Dataset Not Found", JOptionPane.WARNING_MESSAGE);
	   				modelAfterDatasetComboBox.setSelectedIndex(0);
	   				return;
	   			}
   			}
    		GeoFileMetadata metad = parentGui.getMetadataByDataset(modeAfterdDatasetBase);
    		GeoFileMetadata metadStddev = parentGui.getMetadataByDataset(modeAfterdDatasetBaseStddev);
    		if(metad == null){

    			int stackMode = DeliveryLitePlugin.NEAR;
    			if(parentGui.isNearAndFar())
    				stackMode = DeliveryLitePlugin.NEAR_FAR;
    			if(metadStddev == null && !modeAfterdDatasetBase.equals(modeAfterdDatasetBaseStddev)){
    				List<String> list = new ArrayList<String>();
    				list.add(modeAfterdDatasetBaseStddev);
    				list.add(modeAfterdDatasetBase);
    				int [] modes = {DeliveryLitePlugin.MODEL_AFTER_STDDEV,DeliveryLitePlugin.MODEL_AFTER};
    				parentGui.getAgent().getSeismicTraceByDataset(list, stackMode, modes);		
    			}else
    				parentGui.getAgent().getSeismicTraceByDataset(modeAfterdDatasetBase, stackMode, DeliveryLitePlugin.MODEL_AFTER);		
    		}else{
    			fillLayerProperty((ModelFileMetadata)metad);
    		}
    		//modelAfterInternalFrame.refreshModelCanvas(1, null, null,null);
    		parentGui.setModelAfterDatasetIndex(index);
    		/*ComboBoxModel model = modelAfterPropertyComboBox.getModel();
    		index = -1;
    		for(int i = 0; i < model.getSize(); i++){
    			String selectedModelAfterProperty = parentGui.getShowModelParams().getSelectedModelAfterProperty();
    			if(model.getElementAt(i).equals(selectedModelAfterProperty)){
    				index = i;
    				break;
    			}
    		}
    		if(index == -1)
    			modelAfterInternalFrame.refreshModelCanvas(1, null, null,null);
    		//else
    		//	modelAfterPropertyComboBox.setSelectedIndex(index);
    		 */
    		if(s.length() != 0)
    			parentGui.getShowModelParams().setSelectedModelAfterDataset(s);
    	}
    }
    
    public void setDatasetMaxAmp(String dataset, float[] data){
    	if(data.length == 0){
    		maxAmpMap.remove(dataset);
    		return;
    	}
    	float max = Math.abs(Util.getMax(data));
        float min = Math.abs(Util.getMin(data));
        if(max <= min)
        	max = min;
    	maxAmpMap.put(dataset,new Float(max));
    }

    public void setDatasetMaxAmp(String dataset, float max){
    	if(max == 0){
    		maxAmpMap.remove(dataset);
    		return;
    	}
    	maxAmpMap.put(dataset,new Float(max));
    	setTraceMaxValue(getMaxAmpFromMap());
    } 
    
    public void clearMaxAmpMap(){
    	maxAmpMap.clear();
    }
    
    public float getMaxAmpByDataset(String dataset){
    	if(maxAmpMap.containsKey(dataset)){
    		return maxAmpMap.get(dataset).floatValue();
    	}
    	return 0;
    }
    
    public float getMaxAmpFromMap(){
    	if(maxAmpMap.keySet().size() == 0)
    		return 0;
    	
    	float finalMax = 0;
    	float nearStackMax = 0;
    	String key = parentGui.getNearStackFileName();
    	if(maxAmpMap.containsKey(key))
    		nearStackMax = maxAmpMap.get(key);
    	if(nearStackMax >= finalMax)
    		finalMax = nearStackMax;
    	float nearSyntheticBeforeMax = 0;
    	key = parentGui.getNearSyntheticBeforeDatasetName();
    	if(maxAmpMap.containsKey(key))
    		nearSyntheticBeforeMax = maxAmpMap.get(key);
    	if(nearSyntheticBeforeMax >= finalMax)
    		finalMax = nearSyntheticBeforeMax;
    	float nearSyntheticAfterMax = 0;
    	key = parentGui.getNearSyntheticAfterDatasetName();
    	if(maxAmpMap.containsKey(key))
    		nearSyntheticAfterMax = maxAmpMap.get(key);

    	if(nearSyntheticAfterMax >= finalMax)
    		finalMax = nearSyntheticAfterMax;
    	float farStackMax = 0;
    	float farSyntheticBeforeMax = 0;
    	float farSyntheticAfterMax = 0;
    	if(parentGui.isNearAndFar()){
    		key = parentGui.getFarStackFileName();
    		if(maxAmpMap.containsKey(key))
        		farStackMax = maxAmpMap.get(key);
    		key = parentGui.getFarSyntheticBeforeDatasetName();
    		if(maxAmpMap.containsKey(key))
    			farSyntheticBeforeMax = maxAmpMap.get(key);
    		key = parentGui.getFarSyntheticAfterDatasetName();
    		if(maxAmpMap.containsKey(key))
    			farSyntheticAfterMax = maxAmpMap.get(key);
    		
    		if(farStackMax > finalMax)
        		finalMax = farStackMax;
    		if(farSyntheticBeforeMax > finalMax)
        		finalMax = farSyntheticBeforeMax;
    		if(farSyntheticAfterMax > finalMax)
        		finalMax = farSyntheticAfterMax;    		
    	}
    	
    	return finalMax;
    	
    }
    
    private Map<String,Float> maxAmpMap = new HashMap<String,Float>();
    
    public DeliveryLiteUI getParentGui(){
    	return parentGui;
    }	
    
    private void showModelBeforeColorbarRadioButtonActionPerformed(){
	  	if(showModelBeforeColorbarRadioButton.isSelected()){
			modelBeforeInternalFrame.setShowColorbar(true);
			parentGui.getShowModelParams().setShowModelBeforeColorbarEnabled(true);
		}else{
			modelBeforeInternalFrame.setShowColorbar(false);
			parentGui.getShowModelParams().setShowModelBeforeColorbarEnabled(false);
		}
    
	  	float verticalScale = parentGui.getShowModelParams().getVerticalScale();
	  	modelBeforeInternalFrame.refreshModelCanvas(verticalScale);
	}
    
    public void restoreParams(ShowModelParams params){
    	boolean isSelected = params.isShowModelBeforeColorbarEnabled();
        showModelBeforeColorbarRadioButton.setSelected(isSelected);
    	showModelBeforeColorbarRadioButtonActionPerformed();
        isSelected = parentGui.getShowModelParams().isShowModelAfterTimeValueEnabled();
        showModelAfterTimeRadioButton.setSelected(isSelected);
        //showModelAfterTimeRadioButtonActionPerformed();
        isSelected = parentGui.getShowModelParams().isShowModelAfterSigmaTimeEnabled();
        showModelAfterSigmaTimeRadioButton.setSelected(isSelected); 
        //showModelAfterSigmaTimeRadioButtonActionPerformed();
        isSelected = parentGui.getShowModelParams().isShowModelAfterColorbarEnabled();
        showModelAfterColorbarRadioButton.setSelected(isSelected);  
        //showModelAfterColorbarRadioButtonActionPerformed();
        isSelected = parentGui.getShowModelParams().isShowModelBeforePropertyValueEnabled();
        showModelBeforePropertyRadioButton.setSelected(isSelected);
        showModelBeforePropertyRadioButtonActionPerformed();
        isSelected = parentGui.getShowModelParams().isShowModelBeforeTimeValueEnabled();
        showModelBeforeTimeRadioButton.setSelected(isSelected);  
        showModelBeforeTimeRadioButtonActionPerformed();
        isSelected = parentGui.getShowModelParams().isShowModelBeforeSigmaTimeEnabled();
        showModelBeforeSigmaTimeRadioButton.setSelected(isSelected); 
        showModelBeforeSigmaTimeRadioButtonActionPerformed();
        isSelected = params.isShowModelAfterPropertyValueEnabled();
        showModelAfterPropertyRadioButton.setSelected(isSelected);
 
        //showModelAfterPropertyRadioButtonActionPerformed();
    }
    
    public void setTraceMaxValue(float val){
    	traceMaxValueTextField.setText(String.valueOf(val));
    	newTraceMaxValueTextField.setEditable(true);
    	currentMaxTraceValue = val;
    }
    
    
    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	parentGui.getShowModelParams().setVerticalScrollPosition(modelBeforeInternalFrame.getScrollPane().getVerticalScrollBar().getValue());
    	//parentGui.getShowModelParams().setScrollToVisible(rect);
    	//this.setVisible(false);
    	this.dispose();
    }

    
    private void checkSyntheticBeforeReady(){
    	Runnable runnable = new Runnable(){
			public void run(){
				ArrayList params = new ArrayList();
				MessagingManager messagingMgr = parentGui.getAgent().getMessagingMgr();
		   		params.add(messagingMgr.getLocationPref());
		   		String tmpPath = QiProjectDescUtils.getDatasetsPath(parentGui.getAgent().getQiProjectDescriptor());
		   		String filesep = messagingMgr.getServerOSFileSeparator();
		   		String nearSyntheticBeforeFilePath = "";
		   		String farSyntheticBeforeFilePath = "";
		   		if(!parentGui.isNearAndFar()){ //near only
		   			nearSyntheticBeforeFilePath = tmpPath +  filesep + parentGui.getNearSyntheticBeforeDatasetName() + ".dat";
		   			String temp = parentGui.getAgent().checkFileExist(nearSyntheticBeforeFilePath);
			   		if(temp != null && temp.equals("yes"))
			   			showSyntheticBeforeRadioButton.setEnabled(true);
		   		}else{// near & far 
		   			nearSyntheticBeforeFilePath = tmpPath +  filesep + parentGui.getNearSyntheticBeforeDatasetName() + ".dat";
		   			farSyntheticBeforeFilePath = tmpPath +  filesep + parentGui.getFarSyntheticBeforeDatasetName() + ".dat";
		   			boolean nearExist = false;
		   			boolean farExist = false;
		   			String temp = parentGui.getAgent().checkFileExist(nearSyntheticBeforeFilePath);
			   		if(temp != null && temp.equals("yes"))
			   			nearExist = true;
			   		temp = parentGui.getAgent().checkFileExist(farSyntheticBeforeFilePath);
			   		if(temp != null && temp.equals("yes"))
			   			farExist = true;
			   		if(nearExist || farExist)
			   			showSyntheticBeforeRadioButton.setEnabled(true);
		   		}
		   		
    		}
    	};
    	new Thread(runnable).start();
   	}	

    
    private void checkSyntheticAfterReady(){
    	Runnable runnable = new Runnable(){
			public void run(){
				MessagingManager messagingMgr = parentGui.getAgent().getMessagingMgr();
		   		String tmpPath = QiProjectDescUtils.getDatasetsPath(parentGui.getAgent().getQiProjectDescriptor());
		   		String filesep = messagingMgr.getServerOSFileSeparator();
		   		String nearSyntheticAfterFilePath = "";
		   		String farSyntheticAfterFilePath = "";
		   		if(!parentGui.isNearAndFar()){ //near only
		   			nearSyntheticAfterFilePath = tmpPath +  filesep + parentGui.getNearSyntheticAfterDatasetName() + ".dat";
		   			String temp = parentGui.getAgent().checkFileExist(nearSyntheticAfterFilePath);
			   		if(temp != null && temp.equals("yes"))
			   			showSyntheticAfterRadioButton.setEnabled(true); 
		   		}else{// near & far 
		   			nearSyntheticAfterFilePath = tmpPath +  filesep + parentGui.getNearSyntheticAfterDatasetName() + ".dat";
		   			farSyntheticAfterFilePath = tmpPath +  filesep + parentGui.getFarSyntheticAfterDatasetName() + ".dat";
		   			boolean nearExist = false;
		   			boolean farExist = false;
		   			String temp = parentGui.getAgent().checkFileExist(nearSyntheticAfterFilePath);
			   		if(temp != null && temp.equals("yes"))
			   			nearExist = true;
			   		temp = parentGui.getAgent().checkFileExist(farSyntheticAfterFilePath);
			   		if(temp != null && temp.equals("yes"))
			   			farExist = true;
			   		if(nearExist || farExist)
			   			showSyntheticAfterRadioButton.setEnabled(true);
		   		}
    		}
    	};
    	new Thread(runnable).start();
   	}
    
    public static final List<String> MODEL_AFTER_DATASETS_ABBREV = new ArrayList<String>();
    static{
    	MODEL_AFTER_DATASETS_ABBREV.add("mean");
    	MODEL_AFTER_DATASETS_ABBREV.add("stddev");
    	MODEL_AFTER_DATASETS_ABBREV.add("median");
    	MODEL_AFTER_DATASETS_ABBREV.add("P10");
    	MODEL_AFTER_DATASETS_ABBREV.add("P90");
        MODEL_AFTER_DATASETS_ABBREV.add("IS mean");
    	MODEL_AFTER_DATASETS_ABBREV.add("IS stddev");
    	MODEL_AFTER_DATASETS_ABBREV.add("IS median");
    	MODEL_AFTER_DATASETS_ABBREV.add("IS P10");
    	MODEL_AFTER_DATASETS_ABBREV.add("IS P90");

    }
    
    private void checkModelAfterReady(){
    	Runnable runnable = new Runnable(){
			public void run(){
				MessagingManager messagingMgr = parentGui.getAgent().getMessagingMgr();
		   		String tmpPath = QiProjectDescUtils.getDatasetsPath(parentGui.getAgent().getQiProjectDescriptor());
		   		String filesep = messagingMgr.getServerOSFileSeparator();
		   		Vector<String> v = new Vector();
		   		v.add("Select One");
		   		for(String s : MODEL_AFTER_DATASETS_ABBREV){
		   			String modeAfterDatasetBase = parentGui.getOutputBasename() + "_post_";
		   			if(!parentGui.isNearAndFar())
		   				modeAfterDatasetBase += "near_" + s + "_model";
		   			else{//TODO near and far functionality requirement is not yet set
		   				modeAfterDatasetBase += "near_far_" + s + "_model";
		   			}
		   			String modeAfterFilePath = tmpPath +  filesep + modeAfterDatasetBase + ".dat"; 
		   			String temp = parentGui.getAgent().checkFileExist(modeAfterFilePath);
		   			if(temp != null && temp.equals("yes"))
		   				v.add(s);
		   			try{
		   				Thread.sleep(500);
		   			}catch(InterruptedException e){}
				}
		   		modelAfterDatasetComboBox.setModel(new DefaultComboBoxModel(v));
    		}
    	};
    	new Thread(runnable).start();
   	}
    
    private void checkRealReady(){
    	Runnable runnable = new Runnable(){
			public void run(){
				MessagingManager messagingMgr = parentGui.getAgent().getMessagingMgr();
		   		String tmpPath = QiProjectDescUtils.getDatasetsPath(parentGui.getAgent().getQiProjectDescriptor());
		   		String filesep = messagingMgr.getServerOSFileSeparator();
		   		String realFilePath = "";
		   		if(!parentGui.isNearAndFar()){
		   			realFilePath = tmpPath +  filesep + parentGui.getNearStackFileName() + ".dat"; 
		   		}
		   		String temp = parentGui.getAgent().checkFileExist(realFilePath);
		   		if(temp != null && temp.equals("yes"))
		   			showSyntheticAfterRadioButton.setEnabled(true);
    		}
    	};
    	new Thread(runnable).start();
   	}	
    
	private List<Cell> formListOfCells(float unit){
		if(modelBeforePropertyComboBox.getSelectedIndex() == -1)
		   modelBeforePropertyComboBox.setSelectedIndex(6);
		Vector<Vector> model = parentGui.getLayerData();
		float minTime = Float.parseFloat((String)(model.get(0).get(4)));
		float time1 = minTime;
		//double time2 = 0.5;
		int accuHeight = 0;
		//double sigmatime = 0.1;
		//double tvdbml = 0;
		//double timeNext = 0;
		float sigmatime = 0;
		float propValue = 0;
		float timeNext = 0;
		String propName = (String)(modelBeforePropertyComboBox.getSelectedItem());
		Property prop = modelBeforePropertyColorMap.get(propName);
		float min = prop.getMin();
		float max = prop.getMax();
		int col = parentGui.getColIndexByPropertyName(propName);
		List<Cell> listOfCells = new ArrayList<Cell>(); 
		for(int i = 0; i < model.size()-1; i++){
			Vector v = model.get(i);
			Vector next = model.get(i+1);
			//for(int j = 0; j < v.size(); j++){
			String val1 = (String)v.get(4);
			String nextVal1 = (String)next.get(4);
			String val2 = (String)v.get(5);
			String val3 = (String)v.get(col+4);
			if(val2 != null && val2.trim().length() > 0){
				sigmatime = Float.parseFloat(val2);
			}
			if(val3 != null && val3.trim().length() > 0){
				propValue = Float.parseFloat(val3);
			}
			if(nextVal1 != null && nextVal1.trim().length() > 0){ 
				timeNext = Float.parseFloat(nextVal1);
			}
			if(val1 != null && val1.trim().length() > 0){
				//double dval = Double.parseDouble(val1);
				float dval = Float.parseFloat(val1);
				time1 = dval;
				int height = (int)Math.round(unit*(timeNext - time1));
				int index = 0;
				if(max != min)
					//index = Math.round(((propValue-min)/(max-min))* (prop.getColorbar().getListOfColor().size()-1));
					index = Math.round(((propValue-min)/(max-min))* (prop.getColorbar().getColorMap().getColors(ColorMap.RAMP).length-1));
				//Color c = prop.getColorbar().getListOfColor().get(index).getColor();
				Color c = prop.getColorbar().getColorMap().getColors(ColorMap.RAMP)[index];
				listOfCells.add(new Cell(new Point(modelX,modelY+accuHeight), new Dimension(modelWidth,height), c, time1, sigmatime,propName,propValue,unit));
				//time1 = time2;
				accuHeight += height;
			}
			//}
		}

		//deal with the bottom layer
		String val = (String)model.get(model.size()-1).get(4);
		time1 = Float.parseFloat(val);

		val = (String)model.get(model.size()-1).get(5);
		if(val != null && val.trim().length() > 0){
			sigmatime = Float.parseFloat(val);

		}
		String timeBase = parentGui.getTimeBase();
		int height = defaultLayerHeight;
		if(timeBase != null && timeBase.trim().length() > 0){
			float fTimeBase = Float.parseFloat(timeBase);
			height = (int)Math.round(unit*(fTimeBase - time1));
		}
		val = (String)model.get(model.size()-1).get(col+4);
		if(val != null && val.trim().length() > 0){
			propValue = Float.parseFloat(val);
		}
		int index = 0;
		if(max != min)
			//index = Math.round(((propValue-min)/(max-min))* (prop.getColorbar().getListOfColor().size()-1));
			index = Math.round(((propValue-min)/(max-min))* (prop.getColorbar().getColorMap().getColors(ColorMap.RAMP).length-1));
		//Color c = prop.getColorbar().getListOfColor().get(index).getColor();
		Color c = prop.getColorbar().getColorMap().getColors(ColorMap.RAMP)[index];
		listOfCells.add(new Cell(new Point(modelX,modelY+accuHeight), new Dimension(modelWidth,height), c, time1, sigmatime, (String)(modelBeforePropertyComboBox.getSelectedItem()),propValue, unit));
		return listOfCells;

	}
    //keyed by property name
    private Map<String,Property> modelBeforePropertyColorMap;
//  containing currently loaded UNLABELED colorbar
    //keyed by colorbar name
    private Map<String,Colorbar1> currentUnlabeledColorbarMap;  
    
    
    
    private void runDeliveryPropertiesWorker(final String title, final int layer, final int numPropSet, final String stackType, final int jobType){
    	Runnable heavyRunnable = new Runnable(){
   			public void run(){
   				List<String> props = new ArrayList<String>();
   				if(parentGui.getAgent().getDeliveryProperties(props)){
   					new RunPlotDialog(parentGui, false, title, props, layer, numPropSet, 
   							stackType,jobType).setVisible(true);
   					return;
   				}else{
   					JOptionPane.showMessageDialog(parentGui, 
   							"Error in getting deliveryAnalyser default properties. See Workbench support for further assistance.", 
   							"Data Access Error.",JOptionPane.WARNING_MESSAGE);
   				}
   			}
       	};
       	new Thread(heavyRunnable).start();
    }
    
    
    public void scaleChanged(float scale){
    	modelBeforeInternalFrame.refreshModelCanvas(scale);
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewModelDialog(null, true).setVisible(true);
            }
        });
    }
    
    public StackInternalFrame getNearInternalFrame(){
    	return nearInternalFrame;
    }
    
    public StackInternalFrame getFarInternalFrame(){
    	return farInternalFrame;
    }
    public ModelBeforeInversionInternalFrame getModelBeforeInternalFrame(){
    	return modelBeforeInternalFrame;
    }
    
    public ModelAfterInversionInternalFrame getModelAfterInternalFrame(){
    	return modelAfterInternalFrame;
    }    
    // Variables declaration - do not modify
    private javax.swing.JMenu ViewMenu;
    private javax.swing.JMenu actionMenu;
    //private javax.swing.JRadioButton autoFitRadioButton;
    private javax.swing.JButton fitPageButton;
    private javax.swing.JLabel chooseModelAfterPropertyLabel;
    private javax.swing.JLabel chooseModelBeforePropertyLabel;
    
    private javax.swing.JButton closeButton;
    private javax.swing.JButton modelBeforeColorBarButton;
    private javax.swing.JPanel modelBeforePanel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JMenuItem exitMenuItem;
    private StackInternalFrame farInternalFrame;
    private javax.swing.JDesktopPane frameHolderPane;
    private javax.swing.JButton horizontalScaleImageButton;
    private javax.swing.JLabel horizontalScaleLabel;
    private javax.swing.JTextField horizontalScaleTextField;
    private javax.swing.JComboBox modelBeforePropertyComboBox;
    private javax.swing.JButton makeHistogramButton;
    private javax.swing.JButton makeFullAsciiButton;
    private javax.swing.JMenuItem makeHistogramMenuItem;
    private javax.swing.JButton makeScatterPlotButton;
    private javax.swing.JMenuItem makeScatterPlotMenuItem;
    private javax.swing.JButton makeSpaghettiPlotButton;
    private javax.swing.JMenuItem makeSpaghettiPlotMenuItem;
    private javax.swing.JMenuItem fullAsciiOutputMenuItem;    
    private javax.swing.JButton modelAfterColorBarButton;
    private javax.swing.JComboBox modelAfterDatasetComboBox;
    private ModelAfterInversionInternalFrame modelAfterInternalFrame;
    private javax.swing.JPanel modelAfterPanel;
    private javax.swing.JComboBox modelAfterPropertyComboBox;
    
    private ModelBeforeInversionInternalFrame modelBeforeInternalFrame;
    private javax.swing.JPanel modelControlPanel;
    private javax.swing.JLabel modelDatasetsLabel;
    private StackInternalFrame nearInternalFrame;
    private javax.swing.JButton newTraceMaxValueImageButton;
    private javax.swing.JLabel newTraceMaxValueLabel;
    private javax.swing.JTextField newTraceMaxValueTextField;    
    protected javax.swing.JButton runPostInversionButton;
    protected javax.swing.JMenuItem runPostInversionMenuItem;
    protected javax.swing.JButton runPriorInversionButton;
    protected javax.swing.JMenuItem runPriorInversionMenuItem;
    private javax.swing.JPanel scalePanel;
    private javax.swing.JPanel seismicControlPanel;
    private javax.swing.JRadioButton showFillRadioButton;    
    private javax.swing.JRadioButton showModelBeforeColorbarRadioButton;
    private javax.swing.JMenuBar showModelMenuBar;
    private javax.swing.JPanel buttonModelPanel;
    private javax.swing.JRadioButton showModelBeforePropertyRadioButton;
    private javax.swing.JRadioButton showModelBeforeTimeRadioButton;
    private javax.swing.JRadioButton showRealSeismicRadioButton;
    private javax.swing.JRadioButton showModelBeforeSigmaTimeRadioButton;
    private javax.swing.JRadioButton showSyntheticAfterRadioButton;
    private javax.swing.JRadioButton showSyntheticBeforeRadioButton;
    
    private javax.swing.JRadioButton showModelAfterColorbarRadioButton;
    private javax.swing.JRadioButton showModelAfterPropertyRadioButton;
    private javax.swing.JRadioButton showModelAfterSigmaTimeRadioButton;
    private javax.swing.JRadioButton showModelAfterTimeRadioButton;
    
    private javax.swing.JTextField traceMaxValueTextField;
    private javax.swing.JLabel traceMaxValueLabel;
    
    private javax.swing.JButton verticalScaleImageButton;
    private javax.swing.JLabel verticalScaleLabel;
    private javax.swing.JTextField verticalScaleTextField;
    
    private javax.swing.JButton updateViewButton;
    private javax.swing.JCheckBoxMenuItem viewFarCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem viewModelAfterCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem viewNearCheckBoxMenuItem;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomOutButton;    
    // End of variables declaration                               
    
}
