/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bhpb.deliverylite.displayResults.DeliveryLiteDisplayTransducer;
import com.bhpb.geoio.filesystems.FileSystemConstants;
import com.bhpb.geoio.filesystems.FileSystemConstants.GeoIoOpStatus;
import com.bhpb.geoio.filesystems.geofileio.GeoReadData;
import com.bhpb.geoio.filesystems.metadata.GeoFileDataSummary;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.filesystems.properties.DatasetProperties;
import com.bhpb.geoio.filesystems.properties.ModelProperties;
import com.bhpb.geoio.filesystems.properties.SeismicProperties;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IDataObject;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentStateUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.qiworkbench.util.ExceptionMessageFormatter;
import com.bhpb.qiworkbench.workbench.QiComponentBase;
import com.bhpb.xsdparams.delivery.Inversion;
import com.bhpb.xsdparams.delivery.ObjectFactory;

/**
 * Delivery Lite plugin a BHP propritary qiComponent. It is
 * started as a thread by the Messenger Dispatcher that executes the
 * "Activate plugin" command.
 */
public class DeliveryLitePlugin extends QiComponentBase implements IqiWorkbenchComponent, Runnable {
    private static Logger logger = Logger.getLogger(DeliveryLitePlugin.class.getName());
    //NOTE: CANNOT use File.separator because when executed on a Windows
    //      machine, cl.getResource(DEFAULT) will return null.
    public static final String DEFAULT  = "xml" + "/" + "delivDefault.xml";
    public static final int RUN_NEAR_SYNTHETIC_BEFORE = 2;
    public static final int RUN_NEAR_SYNTHETIC_AFTER = 3;
    public static final int REAL = 0;
    public static final int SYNTHETIC_BEFORE = 1;
    public static final int SYNTHETIC_AFTER = 2;
    public static final int MODEL_AFTER = 3;
    public static final int MODEL_AFTER_STDDEV = 4;
    public static final int NEAR = 0;
    public static final int FAR = 1;
    public static final int NEAR_FAR = 2;
    private String           jobID;
    /** Path of XML model file */
    private String           modelXML = "";
    private Inversion        inversion = null;

    private DeliveryLiteUI   gui;
    private static Thread    pluginThread;
    private MessagingManager messagingMgr;
    private String myCID = "";
    private boolean stop     = false;
    private static int saveAsCount = 0;
    private IComponentDescriptor projMgrDesc = null;

    private QiProjectDescriptor qiProjectDesc = new QiProjectDescriptor();
    private static final int TIMEOUT = 30*60*1000;   //1/2 hour

    private static SimpleDateFormat formatter
       = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");

    protected String getCID() {
        return myCID;
    }

    public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjectDesc;
    }

    public Node getInversionNode(Node node){
    	NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
          child = children.item(i);
          if (child.getNodeType() == Node.ELEMENT_NODE) {
            nodeName = child.getNodeName();
            if (nodeName.equals("model_xml")) {
                children = child.getChildNodes();
                for (int j = 0; j < children.getLength(); j++) {
                    child = children.item(j);
                    if (child.getNodeType() == Node.ELEMENT_NODE) {
                    	nodeName = child.getNodeName();
                    	if (nodeName.equals("inversion"))
                    		return child;
                    }
                }
            }
          }
        }
        return null;
    }


    public Node getXmlNodeByNodeName(Node node,String name){
    	NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
          child = children.item(i);
          if (child.getNodeType() == Node.ELEMENT_NODE) {
            nodeName = child.getNodeName();
            if (nodeName.equals(name)) {
                return child;
            }
          }
        }
        return null;
    }

    /**
     * Get the component descriptor of the associated PM and therefore project.
     * @return Component descriptor of the associated PM
     */
    public IComponentDescriptor getQiProjMgrDescriptor() {
        return projMgrDesc;
    }

    public IComponentDescriptor getComponentDescriptor() {
        return messagingMgr.getMyComponentDesc();
    }

    /** Get component's Messaging Manager.
     *  @return  MessagingManager
     */
    public MessagingManager getMessagingMgr(){
        return messagingMgr;
    }

    /** Display step */
    private int stepState = ControlConstants.SESSION_STEP;
    /** Step action */
    private int actionState = ControlConstants.START_SESSION;
    /** Finite state machine for displaying the results of a generated script */
    private DeliveryLiteDisplayTransducer displayTransducer ;

    /**
     * Initialize the plugin component:
     * <ul>
     * <li>Create its messaging manager</li>
     * <li>Register with the Message Dispatcher</li>
     * </ul>
     */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            messagingMgr = new MessagingManager();
            myCID = Thread.currentThread().getName();
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.PLUGIN_AGENT_COMP,
                    DeliveryLiteConstants.DELIVERY_LITE_PLUGIN_NAME, myCID);
            //notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in DeliveryLitePlugin.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

	/** Invoke File Chooser Service
	 * @param list Passed through to fileChooser from caller
	 *
	 */
	public void callFileChooser(QiFileChooserDescriptor desc){
		if(desc == null)
			return;
		ComponentDescriptor fileChooser = null;
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			logger.warning("Respone to get file chooser service returning null due to timed out");
			return;
		}
		else if(MsgUtils.isResponseAbnormal(response)) {
			logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
			return;
		}
		else
			fileChooser = (ComponentDescriptor)MsgUtils.getMsgContent(response);

		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_QI_FILE_CHOOSER_CMD,fileChooser,QiFileChooserDescriptor.class.getName(),desc);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD,fileChooser);
		return;
	}

    /**
     * Generate state information into xml string format
     * @return  String
     */
    public String genState(){
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) +
                       "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) +
                       "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
        content.append(gui.genState());
        content.append("</component>\n");
        return content.toString();
    }

    /**
     * Generate state information into xml string format in repsonse to save as command
     * @return  String
     */
    public String genStateAsClone(){
        StringBuffer content = new StringBuffer();
        IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
        //TODO WorkbenchStateManager will need to be changed to conform to the package change of AmpExtPlugin in ServletDispatcher
        String displayName = "";
        saveAsCount++;
        if(saveAsCount == 1)
            displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
        else
            displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
        content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
        content.append(gui.genState());
        content.append("</component>\n");

        return content.toString();
    }

    /** restore plugin and it's gui to previous condition
     *
     * @param node xml containing saved state variables
     */
    void restoreState(Node node) {
        String preferredDisplayName = ((Element)node).getAttribute("preferredDisplayName");
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(DeliveryLiteUI.class.getName())) {
                    Element el = (Element)child;
                    String project = el.getAttribute("project_name");
                    String fileSystem = el.getAttribute("project_root");
                    QiProjectDescUtils.setQiProjectName(qiProjectDesc, project);
                    QiProjectDescUtils.setQiSpace(qiProjectDesc, fileSystem);
                    QiProjectDescUtils.setQiProjectReloc(qiProjectDesc, messagingMgr.getServerOSFileSeparator());

                    //Extract the XML model and save to a file which
                    //the new DeliveryLite will read in.
                    String xmlFilepath = getModelDir() + "last_lite_model.xml";

                    //extractXmlModel(node, xmlFilepath);
                    Node inversionNode = getInversionNode(child);
                    modelXML = xmlFilepath;
                    gui = new DeliveryLiteUI(this, inversionNode);


                    //Get the actual filename of the XML model the XML in the
                    //state is based on and reset the title to it
                    String modelFilename = el.getAttribute("model");
                    modelXML = getModelDir() + modelFilename;
                    gui.resetTitle(null);

                    //gui.restoreState(child);
                    String hStr = el.getAttribute("height");
                    int height = Integer.valueOf(hStr).intValue();
                    String wStr = el.getAttribute("width");
                    int width = Integer.valueOf(wStr).intValue();

                    String X = el.getAttribute("x");
                    int x = Integer.valueOf(X).intValue();
                    String Y = el.getAttribute("y");
                    int y = Integer.valueOf(Y).intValue();
                    LiteParams liteParam = getLiteParams(gui.getLiteParams(), el);

                    if (preferredDisplayName != null && preferredDisplayName.trim().length() > 0) {
                        CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                        gui.renamePlugin(preferredDisplayName);
                    }

                    gui.setLocation(x,y);
                    gui.setSize(width,height);
                    gui.updateLiteParams(liteParam);
                    gui.restoreRunParamState(el);
                    String modelName = el.getAttribute("modelName");
                    gui.setModelName(modelName);
                    Node showModelParamsNode = getXmlNodeByNodeName(child,ShowModelParams.getXmlAlias());
                    ShowModelParams showModelParams = gui.getShowModelParams();
                    showModelParams.setModelBeforeColorbarMap(gui.getModelBeforePropertyColorMap());
                    showModelParams.setModelAfterDatasetPropertyColorMap(gui.getModelAfterDatasetPropertyColorMap()); 
                    showModelParams.setNearAndFar(!liteParam.isNear());
                    showModelParams.setNearStackName(liteParam.getFileNearStack());
                    showModelParams.setFarStackName(liteParam.getFileFarStack());
                    showModelParams.setOutputBaseName(liteParam.getOutputBaseName());
                    Node nonFileBasedColorbarNode = getXmlNodeByNodeName(child,ShowModelParams.getXmlAliasColorbar());
                    showModelParams.restoreNonPredefinedColorbars(nonFileBasedColorbarNode);
                    
                    //showModelParams.restoreShowModelParams(showModelParamsNode,cbs);
                    showModelParams.restoreShowModelParams(showModelParamsNode);
                    
                    Node horizonsNode = getXmlNodeByNodeName(child,"horizons");
                    gui.restoreHorizonLineAttributes(horizonsNode);
                    Node jobNode = getXmlNodeByNodeName(child,"job_info");
                    if(jobNode != null){
                    	gui.restoreJobInfo(jobNode);
                    }
                    //gui.resetFrameString();
                }
            }
        }
    }


    public void runGeoFileMetadataTask(final MetadataSettable comp, final String filePath, final String command){
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering input dataset information";
				String text = "Please wait while system is gathering input dataset information....";

				Icon icon = null; //new ImageIcon(this.getClass().getResource("/icon/BHP_Logo.gif"));

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(gui,100,true,500,title,text,icon);
				//monitor.start("");
				ProgressUtil.start(monitor,"");

				GeoFileMetadata outMetadata = getGeoFileMetaData1(filePath, command);
				//int status = getSeismicFileMetaData(filePath, command);

				if(ProgressUtil.isCancel(monitor)){
					return;
				}
				//if(status == 0){
				if(outMetadata != null){
					int ind = filePath.lastIndexOf(".dat");
					String filePath1 = filePath;
					if(ind != -1)
						filePath1 = filePath1.substring(0,ind);
					if(command.equals(DeliveryLiteConstants.GET_NEAR_SEISMIC_DATASET_CMD)){
						//comp.setNearStackFile(filePath1);
						//nearMetadata = outMetadata;
						comp.setMetaData(outMetadata.getGeoFileName(),outMetadata,DeliveryLitePlugin.NEAR, DeliveryLitePlugin.REAL);
					}else if(command.equals(DeliveryLiteConstants.GET_FAR_SEISMIC_DATASET_CMD)){
						//comp.setFarStackFile(outMetadata.getGeoFileName());
						//farMetadata = outMetadata;
						comp.setMetaData(outMetadata.getGeoFileName(),outMetadata,DeliveryLitePlugin.FAR, DeliveryLitePlugin.REAL);
					}else if(command.equals(DeliveryLiteConstants.GET_NEAR_WAVELET_FILE_CMD)){
						gui.setNearStackWaveletFile(filePath1);
					}else if(command.equals(DeliveryLiteConstants.GET_FAR_WAVELET_FILE_CMD)){
						gui.setFarStackWaveletFile(filePath1);
					}
				}
				ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
			}
		};
		new Thread(heavyRunnable).start();
    }


    private boolean isSingleTraceDataset(GeoFileMetadata data){
    	for(int i = 0; data != null && i < data.getKeys().size(); i++){
    		String key = data.getKeys().get(i);
    		DiscreteKeyRange range = data.getKeyRange(key);
    		if(range.getMax() != range.getMin() && !key.equals("tracl"))
    			return false;
    	}
    	return true;
    }



    private GeoFileMetadata getGeoFileMetaData1(String filePath, String command){
    	 GeoFileMetadata outMetadata = getGeoFileMetaData(filePath);

    	 if(outMetadata != null){
    		 if(outMetadata instanceof SeismicFileMetadata){
    			 SeismicFileMetadata data = (SeismicFileMetadata)outMetadata;
    			 if(data.getHorizons().size() > 0){
    				 JOptionPane.showMessageDialog(gui, "This dataset does not appears to be SEISMIC set. Please try again with other dataset.");
    				 return null;
    			 }
    			 if(outMetadata.getKeys().size() == 0){
    				 JOptionPane.showMessageDialog(gui, "Problem occurred while calling bhpio on the given dataset " + filePath + ". Please check log messages or contact workbench support.");
    				 return null;
    			 }
    		 }
    	}
        if(command.equals(DeliveryLiteConstants.GET_NEAR_WAVELET_FILE_CMD) || command.equals(DeliveryLiteConstants.GET_FAR_WAVELET_FILE_CMD)){
        	if(!isSingleTraceDataset(outMetadata)){
        		JOptionPane.showMessageDialog(gui, "This dataset does not appears to be a single trace dataset. Please try again with other datasets.");
        		return null;
        	}
        }
        return outMetadata;
    }



    private GeoFileMetadata getGeoFileMetaData(String filePath){
    	//metadata = null;
    	ArrayList<String> params = new ArrayList<String>();
        params.add(messagingMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(filePath);
        params.add(FileSystemConstants.FILE_METADATA_DATA);

        String geoIORequestID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            JOptionPane.showMessageDialog(gui, errorMsg);
            logger.warning(errorMsg);
            return null;
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                JOptionPane.showMessageDialog(gui, errorMsg);
                return null;
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get metadata for geoFile "+filePath;
                    JOptionPane.showMessageDialog(gui, errorMsg);
                    logger.warning(errorMsg);
                    return null;
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.FILE_METADATA_DATA)) {
                        String errorMsg = "Not a response containing geoFile metadata"+response;
                        logger.warning(errorMsg);
                        JOptionPane.showMessageDialog(gui, errorMsg);
                        return null;
                    }
                    GeoFileMetadata temp = (GeoFileMetadata)contentList.get(2);
                    logger.fine("METADATA for "+filePath);
                    logger.fine(temp.toString());
                    return temp;
                } else {
                    String contentTypeDesc = "NULL";
                    if (contentType != null) {
                        contentTypeDesc = contentType.toString();
                    }
                    String errorMsg = "Error - expected response type of READ_GEOFILE_CMD is " + QIWConstants.ARRAYLIST_TYPE + ", but received " + contentTypeDesc;
                    logger.warning(errorMsg);
                    JOptionPane.showMessageDialog(gui, errorMsg);
                }
            }
        }
        return null;
    }

    public void getTracesByDatasets(final List<DatasetInfo> datasets){
    	if(datasets == null || datasets.size() == 0)
    		return;
    	final String datasetPath = QiProjectDescUtils.getDatasetsPath(getQiProjectDescriptor());
		Runnable heavyRunnable = new Runnable(){
			public void run(){
		    	String title = "Gathering Trace Data";
		    	String filesep = messagingMgr.getServerOSFileSeparator();
	    		String text = "Please wait while system is gathering trace data from ....";

	    		Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);

	    		com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(gui,100,true,500,title,text,icon);
	    		ProgressUtil.start(monitor,"");

	    		
		    	for(int i = 0; i < datasets.size(); i++){
		    		DatasetInfo dp = datasets.get(i);
		    		String filePathTemp = datasetPath + filesep + dp.name;
		    		String filePath = filePathTemp + ".dat";
		    		String temp = filePathTemp;
		    		int index = filePathTemp.lastIndexOf(filesep);

		    		if(index != -1)
		    			temp = temp.substring(index+1);
		    		GeoFileMetadata outMetadata = gui.getMetadataByDataset(dp.name);
		    		if(outMetadata == null){
		    			outMetadata = getGeoFileMetaData(filePath);
		    			//gui.setMetaData(dp.name, outMetadata, dp.stackType, dp.datasetType);
		    		}

		    		IDataObject[] objs = runGetIDataObjects(outMetadata);

		    		if(objs != null && objs.length > 0){
		    			logger.info("Summary data set for dataSetProperties.");
		    			logger.info("Read: " + objs.length + " traces");
		    			//gui.setTraceData(dp.name,objs);
		    		}
		    		if(ProgressUtil.isCancel(monitor)){
		    			return;
		    		}
		    		float f = (float)i/(float)datasets.size();
	                monitor.setCurrent("Restoring dataset " + dp.name,(int)(f*100));
		    	}
				//ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
			}
    	};
    	new Thread(heavyRunnable).start();
    }

    public void getSeismicTraceByDataset(final String dataset, final int stackMode, final int runMode){
    	if(dataset == null || dataset.trim().length() == 0)
    		return;
    	String datasetPath = QiProjectDescUtils.getDatasetsPath(getQiProjectDescriptor());
		String filesep = messagingMgr.getServerOSFileSeparator();
		final String filePathTemp = datasetPath + filesep + dataset;
    	final String filePath = filePathTemp + ".dat";
    	Runnable heavyRunnable = new Runnable(){
			public void run(){
		    	String title = "Gathering Trace Data";
		    	String filesep = messagingMgr.getServerOSFileSeparator();
		    	String temp = filePathTemp;
		    	int index = filePathTemp.lastIndexOf(filesep);

		    	if(index != -1)
		    		temp = temp.substring(index+1);
				String text = "Please wait while system is gathering trace data from " + temp + "....";

				Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(gui,100,true,500,title,text,icon);
				ProgressUtil.start(monitor,"");
				GeoFileMetadata outMetadata = getGeoFileMetaData(filePath);

				gui.setMetaData(dataset,outMetadata,stackMode, runMode);
				IDataObject[] objs = runGetIDataObjects(outMetadata);

				if(ProgressUtil.isCancel(monitor)){
					return;
				}
				if(objs != null && objs.length > 0){
					logger.info("Summary data set for dataSetProperties.");
			        logger.info("Read: " + objs.length + " traces");
			        gui.setTraceData(dataset,objs,stackMode,runMode);
				}
				ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
			}
    	};
    	new Thread(heavyRunnable).start();
    }

    
    public DatasetInfo getSeismicTraceByDataInfo(final DatasetInfo di){
    	if(di == null)
    		return null;
    	String datasetPath = QiProjectDescUtils.getDatasetsPath(getQiProjectDescriptor());

    	String filesep = messagingMgr.getServerOSFileSeparator();

    	String filePathTemp = datasetPath + filesep + di.name;
		String filePath = filePathTemp + ".dat";
		GeoFileMetadata outMetadata = gui.getMetadataByDataset(di.name);
		if(outMetadata == null)
			outMetadata = getGeoFileMetaData(filePath);
			//gui.setMetaData(dp.name, outMetadata, dp.stackType, dp.datasetType);
		di.setMetadata(outMetadata);

		IDataObject[] objs = runGetIDataObjects(outMetadata);

		if(objs != null && objs.length > 0){
			logger.info("Summary data set for dataSetProperties.");
			logger.info("Read: " + objs.length + " traces");
			di.setDataObjects(objs);
		}
		return di;
    }
    
    
    public void getSeismicTraceByDataset(final List<String> datasets, final int stackMode, final int[] runMode){
    	if(datasets == null || datasets.size() != runMode.length)
    		return;
    	final String datasetPath = QiProjectDescUtils.getDatasetsPath(getQiProjectDescriptor());

		Runnable heavyRunnable = new Runnable(){
			public void run(){
		    	String title = "Gathering Trace Data";
		    	String filesep = messagingMgr.getServerOSFileSeparator();

				String text = "Please wait while system is gathering trace data ....";

				Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(gui,100,true,500,title,text,icon);
				ProgressUtil.start(monitor,"");
				if(ProgressUtil.isCancel(monitor)){
					return;
				}
				for(int i = 0; i < datasets.size(); i++){
					String dataset = datasets.get(i);
					String filePath = datasetPath + filesep + dataset + ".dat";
					GeoFileMetadata outMetadata = getGeoFileMetaData(filePath);
					gui.setMetaData(dataset,outMetadata,stackMode, runMode[i]);
					IDataObject[] objs = runGetIDataObjects(outMetadata);

					if(objs != null && objs.length > 0){
						logger.info("Summary data set for dataSetProperties.");
				        logger.info("Read: " + objs.length + " traces");
				        gui.setTraceData(dataset,objs,stackMode,runMode[i]);
					}
				}
				ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				return;
			}
		};
		Thread worker = new Thread(heavyRunnable);
		worker.start();
    }

    public void getSeismicTraceByMetaData1(final SeismicFileMetadata metadata, final int stackMode, final int runMode){
    	if(metadata == null)
    		return;
    	Runnable heavyRunnable = new Runnable(){
			public void run(){
		    	String title = "Gathering Trace Data";
				String text = "Please wait while system is gathering trace data from " + metadata.getGeoFileName() + "....";

				Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);

				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(gui,100,true,500,title,text,icon);
				ProgressUtil.start(monitor,"");
				IDataObject[] objs = runGetIDataObjects(metadata);
				if(ProgressUtil.isCancel(monitor)){
					return;
				}
				if(objs != null && objs.length > 0){
					logger.info("Summary data set for dataSetProperties.");
			        logger.info("Read: " + objs.length + " traces");
			        //if(mode == RUN_NEAR_REAL){
			        gui.setTraceData(metadata.getGeoFileName(),objs,stackMode, runMode);
			        //}
				}
				ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
			}
    	};
    	new Thread(heavyRunnable).start();
    }

    private IDataObject[] runGetIDataObjects(GeoFileMetadata metadata){
    	TraceGetterWorker worker = new TraceGetterWorker(metadata);
    	Thread thread = new Thread(worker);
    	thread.start();
    	try{
    		thread.join();
    	}catch(InterruptedException ie){
    		ie.printStackTrace();
    	}
    	return worker.getIDataObjects();
    }
    
    public List<DatasetInfo> getTracesByMultipleDatasets(List<DatasetInfo> datasets){
    	if(datasets == null || datasets.size() == 0)
    		return null;
    	String title = "Gathering Trace Data";
    	String text = "Please wait while system is gathering trace data from ....";
		Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);

		com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(gui,100,false,500,title,text,icon);
		monitor.start("");
		

    	MultipleDatasetTracesGetterWorker worker = new MultipleDatasetTracesGetterWorker(datasets);
    	Thread th = new Thread(worker);
    	th.start();
    	try{
    		th.join();
    	}catch(InterruptedException e){}
    	
    	return worker.getDatasetInfo();
    }
    
    private class MultipleDatasetTracesGetterWorker implements Runnable{
    	List<DatasetInfo> datasetInfoList;
    	public MultipleDatasetTracesGetterWorker(List<DatasetInfo> datasetInfoList){
    		this.datasetInfoList = datasetInfoList;
    	}
    	
    	public List<DatasetInfo> getDatasetInfo(){
    		return datasetInfoList;
    	}
    	
    	public void run(){
    		String title = "Gathering Trace Data";
	    	
    		String text = "Please wait while system is gathering trace data from ....";
    		
    		Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);

    		com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(gui,100,false,500,title,text,icon);
    		monitor.start("");
    		
    		String datasetPath = QiProjectDescUtils.getDatasetsPath(getQiProjectDescriptor());
    		String filesep = messagingMgr.getServerOSFileSeparator();
    		monitor.setCurrent("Start gathering trace information ..",0);
            monitor.setIndeterminate(false);
	    	for(int i = 0; i < datasetInfoList.size(); i++){
	    		DatasetInfo dp = datasetInfoList.get(i);
	    		String filePathTemp = datasetPath + filesep + dp.name;
	    		String filePath = filePathTemp + ".dat";
	    		String temp = filePathTemp;
	    		int index = filePathTemp.lastIndexOf(filesep);

	    		if(index != -1)
	    			temp = temp.substring(index+1);
	    		GeoFileMetadata outMetadata = gui.getMetadataByDataset(dp.name);
	    		if(outMetadata == null)
	    			outMetadata = getGeoFileMetaData(filePath);
	    		dp.setMetadata(outMetadata);

	    		IDataObject[] objs = runGetIDataObjects(outMetadata);

	    		if(objs != null && objs.length > 0){
	    			logger.info("Summary data set for dataSetProperties.");
	    			logger.info("Read: " + objs.length + " traces");
	    			dp.setDataObjects(objs);
	    		}
	    		if(ProgressUtil.isCancel(monitor)){
	    			return;
	    		}
	    		float f = (float)(i+1)/(float)datasetInfoList.size();
               
               	monitor.setCurrent("Restoring dataset " + dp.name,(int)(f*100));
	    	}
    	}
    }
    /**
     * The given start value or end value may not be the exact value located on the sample index, this function tries to 
     * find the closest values that should represent the actual indexed start value and the end value
     * @param startTime  start value given
     * @param endTime    end value given
     * @param range  the reference DiscreteKeyRange usually with full range
     * @return
     */
    private DiscreteKeyRange resolveActualTimeKeyRange(float start, float end, DiscreteKeyRange range){
    	float min = (float)range.getMin();
		float max = (float)range.getMax();
		float inc = (float)range.getIncr();
		if(inc == 0f){
			throw new IllegalArgumentException("Illegal increment value (0) found.");
		}
		if(start == min && end == max)
			return range;
		if(start <= min)
            start = min;
		else{
			double rem = Math.IEEEremainder(start - min, inc);
            if(rem > Double.MIN_VALUE)
			//int val = (int)((start - min)/inc);
			//start = (val+1)*inc + min;
            	start = start - (float)rem + inc;
		}
        if(end <= min)
            end = min;
        else if(end == max)
        	end = max;
        else{
        	//int val = (int)((end - min)/inc);
        	double rem = Math.IEEEremainder(end - min, inc);
            if(rem > Double.MIN_VALUE)
            //end = (val)*inc + min;
            	end = end - (float)rem;
        }
        DiscreteKeyRange newRange = new DiscreteKeyRange(range);
        newRange.setRange(start,end,inc);
        return newRange;
    }
    
    private class TraceGetterWorker implements Runnable{
    	private IDataObject[] traces;
    	private GeoFileMetadata metadata;
    	public TraceGetterWorker(GeoFileMetadata metadata){
    		this.metadata = metadata;
    	}

    	public IDataObject[] getIDataObjects(){
    		return traces;
    	}

    	public void run(){
    		List<String> keys = metadata.getKeys();
    		HashMap<String,DiscreteKeyRange> map = new HashMap<String,DiscreteKeyRange>();
    		for(String s : keys){
    			if(s.equals("ep")){
    				double ep = Double.parseDouble(gui.getEp());
    				DiscreteKeyRange kr = new DiscreteKeyRange(ep,ep,
    						metadata.getKeyRange(s).getIncr(),metadata.getKeyRange(s).getKeyType());
    				map.put(s, kr);
    			}else if(s.equals("cdp")){
    				double cdp = Double.parseDouble(gui.getCdp());
    				DiscreteKeyRange kr = new DiscreteKeyRange(cdp,cdp,
    						metadata.getKeyRange(s).getIncr(),metadata.getKeyRange(s).getKeyType());
    				map.put(s, kr);
    			//}else if(s.equals("tracl")){
    			//	if(metadata instanceof SeismicFileMetadata){
    			//		DiscreteKeyRange kr = resolveActualTimeKeyRange(Float.valueOf(gui.getMinTime()).floatValue(), Float.valueOf(gui.getTimeBase()).floatValue(), metadata.getKeyRange(s));
    			//		map.put(s,kr);
    			//	}else{
    			//		map.put(s, metadata.getKeyRange(s));
    			//	}
    			}else
    				map.put(s, metadata.getKeyRange(s));
    		}
    		DatasetProperties datasetProperties = null;
    		if(metadata instanceof SeismicFileMetadata){
    			SeismicFileMetadata metadata1 = (SeismicFileMetadata)metadata;
    			datasetProperties = new SeismicProperties(metadata1);
    		}else if(metadata instanceof ModelFileMetadata){
    			ModelFileMetadata metadata1 = (ModelFileMetadata)metadata;
    			ModelProperties modelProperties = new ModelProperties(metadata1);
    			List<String> props = metadata1.getProperties();
    			modelProperties.setSelectedProperties(props);
    			datasetProperties = modelProperties;
    		}
            
            logger.info("datasetProperties=" + datasetProperties);
            if(datasetProperties != null){
            	datasetProperties.setKeyChosenRanges(map);
            	GeoFileDataSummary summaryData = getGeoFileDataSummary(datasetProperties);
            	datasetProperties.setSummary(summaryData);
            	traces = getTraces(datasetProperties);
            }
		}
	}

    private GeoFileDataSummary getGeoFileDataSummary(DatasetProperties dsProps){
        ArrayList params = new ArrayList<String>();
        params.add(messagingMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        params.add(FileSystemConstants.TRACE_SUMMARY_DATA);
        GeoFileDataSummary summaryData  = null;
        String geoIORequestID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
            QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);


        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            return summaryData;
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                return summaryData;
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_SUMMARY_DATA for datasetProperties " + dsProps.toString();
                    logger.warning(errorMsg);
                    return summaryData;
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !dataType.equals(FileSystemConstants.TRACE_SUMMARY_DATA)) {
                        String errorMsg = "Not a response containing geoFile TRACE_SUMMARY_DATA" + response;
                        logger.warning(errorMsg);
                        return summaryData;
                    }
                    summaryData = (GeoFileDataSummary )contentList.get(2);
                    logger.fine("TRACE_SUMMARY_DATA for datasetProperties" + dsProps);
                    logger.fine(summaryData.toString());
                    return summaryData;
                }
            }
        }
        return summaryData;
    }

    private IDataObject[] getTraces(DatasetProperties dsProps) {
        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(FileSystemConstants.BHP_SU_FORMAT);
        params.add(dsProps);
        if(dsProps instanceof ModelProperties)
        	params.add(FileSystemConstants.MODEL_DATA);
        else if(dsProps instanceof SeismicProperties)
        	params.add(FileSystemConstants.TRACE_DATA);
        params.add("0");
        GeoFileDataSummary summary = dsProps.getSummary();
        if(summary == null){
        	JOptionPane.showMessageDialog(gui, "Error in getting data summary. Please check the log or see workbench support.", "Error in Reading Data", JOptionPane.WARNING_MESSAGE);
        	return null;
        }

        params.add(Integer.valueOf(summary.getNumberOfTraces()).toString());
        if(dsProps instanceof ModelProperties){
        	ArrayList<String> modelProps = new ArrayList<String>();
        	ModelFileMetadata md = (ModelFileMetadata)dsProps.getMetadata();
        	if(md != null)
        		modelProps = md.getProperties();
        	params.add(modelProps);
        }
        String geoIORequestID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.READ_GEOFILE_CMD, QIWConstants.ARRAYLIST_TYPE, params, true);


        IDataObject[] result = new IDataObject[0];

        if (geoIORequestID == null) {
            String errorMsg = "Unable to send " + QIWConstants.READ_GEOFILE_CMD;
            logger.warning(errorMsg);
            result = new IDataObject[0];
        } else {
            // wait for the response, which will come.
            // The time to read the data is indefinite because it depends on the
            // amount of data to be read. Wait up to 1/2 hour. Either all of
            // the data will have been read by then or an abnormal response is
            // returned because of an IO error.
            // TODO: update this with some sort of progress indicator
            IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(geoIORequestID, TIMEOUT);
            if (response == null) {
                String errorMsg = "Unable to get response of " + QIWConstants.READ_GEOFILE_CMD + " after " + TIMEOUT + " seconds.";
                logger.warning(errorMsg);
                result = new IDataObject[0];
            } else {
                if (response.isAbnormalStatus()) {
                    String errorMsg = "Unable to get TRACE_DATA for datasetProperties " + dsProps.toString();
                    logger.info(errorMsg);
                    result = new IDataObject[0];
                }
                Object contentType = response.getContentType();
                Object content = response.getContent();

                if (QIWConstants.ARRAYLIST_TYPE.equals(contentType)) {
                    ArrayList contentList = (ArrayList) content;

                    //validate this is a response with the geoFile's metadata
                    String fileFormat = (String) contentList.get(0);
                    String dataType = (String) contentList.get(1);
                    boolean typeCheck = false;
                    if(dsProps instanceof ModelProperties && dataType.equals(FileSystemConstants.MODEL_DATA)){
                    	typeCheck = true;
                    	params.add(FileSystemConstants.MODEL_DATA);
                    }else if(dsProps instanceof SeismicProperties && dataType.equals(FileSystemConstants.TRACE_DATA)){
                    	typeCheck = true;
                    	params.add(FileSystemConstants.TRACE_DATA);
                    }
                    if (!fileFormat.equals(FileSystemConstants.BHP_SU_FORMAT) ||
                            !typeCheck) {
                        String errorMsg = "Not a response containing geoFile TRACE_SUMMARY_DATA" + response;
                        logger.warning(errorMsg);
                        result = new IDataObject[0];
                    } else {
                        //GeoFileDataSummary summaryData = (GeoFileDataSummary )contentList.get(2);
                        logger.fine("TRACE_DATA for datasetProperties" + dsProps);
                        //logger.fine(summaryData.toString());

                        logger.fine("Trace data for datasetProperties successfully read.");

                        List<IDataObject> traceList = new ArrayList<IDataObject>(dsProps.getSummary().getNumberOfTraces());

                        GeoReadData readData = (GeoReadData) contentList.get(2);

                        int MAX_DATA_READ_TIME = 60000;

                        boolean userSelectedContinue = true;
                        long startTime = System.currentTimeMillis();
                        long elapsedTime = 0;
                        do {
                        	logger.info("Waiting up to " + (MAX_DATA_READ_TIME/1000) + " seconds for read to complete...");

                            try {
                                elapsedTime = System.currentTimeMillis() - startTime;
                                Thread.sleep(500);
                            } catch (InterruptedException ie) {
                            	logger.warning("Interrupted while waiting for READ_GEOFILE_CMD to finish reading.");
                            }
                            if (readData.isOpFinished()) { // most prevalent condition - either the op finished or an error occured
                                GeoIoOpStatus statusCode = readData.getStatus();
                                if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_OK)) {
                                    logger.info("GeoIOop with status code: " + statusCode);
                                } else if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_ERROR)) {
                                    String errorMsg = readData.getErrorMsg();
                                    logger.info("GeoIoOp encountered error: " + errorMsg);
                                    JOptionPane.showMessageDialog(null, errorMsg, "Abnormal condition reading trace data",
                                            JOptionPane.ERROR_MESSAGE);
                                } else if (statusCode.equals(FileSystemConstants.GeoIoOpStatus.SC_UNKNOWN)) {
                                    logger.warning("GeoIoOp finished with status code: " + statusCode + ", the qiViewer may be unstable.");
                                } else {
                                    throw new UnsupportedOperationException("Unable to handle unknown GeoIoOpStatus: " + statusCode);
                                }
                            } else if (elapsedTime >= MAX_DATA_READ_TIME) {
                                int tracesRead = readData.getRemainingRecords();
                                int selection = JOptionPane.showConfirmDialog(null,
                                        tracesRead + " traces have been read but the operation is not finished." +
                                        "\n" +
                                        "Continue waiting for traces to be read?",
                                        "Timeout Exceeded",
                                        JOptionPane.YES_NO_OPTION);

                                //continue waiting if the user selects Yes, but not if
                                //the user selections No or closes the ConfirmDialog without making
                                //a selection
                                if (selection == JOptionPane.NO_OPTION)
                                    userSelectedContinue = false;

                            } else {
                                logger.info("User did not abort the progress monitor, nor was the MAX_DATA_READ_TIME exceeded.  Most likely all the data has been read.");
                            }
                            //monitor.setCancel(true);

                        } while (!readData.isOpFinished() && userSelectedContinue);

                        if (readData.isAbnormalStatus()) {
                            logger.info("READ_GEOFILE_CMD result was abnormal.");
                        } else {
                            logger.info("READ_GEOFILE_CMD result was normal.");
                        }

                        while (readData.hasMoreRecords()) {
                            IDataObject nextRecord = readData.nextRecord();
                            traceList.add(nextRecord);
                        }
                        result = traceList.toArray(new IDataObject[0]);
                    }
                }
            }
        }
        return result;
    }


    /**
     * Extract the XML model from the saved state and write to a file./.
     * @param node First child node of DeliveryLite element in saved state.
     * @param xmlFIlepath Path of the XML file
     *
     * @return List of XML model lines.
     */
    private void extractXmlModel(Node node, String xmlFilepath) {

        NodeList children = node.getChildNodes();
        String nodeName;
        Node child;
        for (int i = 0; i < children.getLength(); i++) {
          child = children.item(i);
          if (child.getNodeType() == Node.ELEMENT_NODE) {
            nodeName = child.getNodeName();
            if (nodeName.equals("model_xml")) {
                children = child.getChildNodes();
                for (int j = 0; j < children.getLength(); j++) {
                    child = children.item(j);
                    if (child.getNodeType() == Node.ELEMENT_NODE) {
                        nu.xom.Element modelElement = nu.xom.converters.DOMConverter.convert((Element)child);
                        String modelXml = modelElement.toXML();
                        try {
                            writeXmlFile(modelXml, xmlFilepath);
                        } catch (QiwIOException qioe) {
                            //TODO notify user cannot write model's XML when restoring state
                            logger.finest("Cannot write model's XML extracted from saved state.\n"+qioe.getMessage());
                        }
                        return;
                    }
                }
            }
          }
        }
    }

    /**
     * Write XML to a file. If the file exists, it will be overwritten, i.e., the
     * lines to be written will not be appended.
     *
     * @param modelXml Model's XML as a string.
     * @param filePath The path of the model XML file.
     * @return IO status.
     * @throws QiwIOException If the file path is null or empty, or there is
     * an IO exception while writing the file.
     */
    private void writeXmlFile(String modelXml, String filePath) throws QiwIOException {
        if (filePath == null || filePath.length() == 0) throw new QiwIOException("invalid file path="+filePath);

        File f = new File(filePath);
        BufferedWriter bw = null;

        try {
            FileOutputStream fos = new FileOutputStream(f);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
            bw.newLine();
            //write out the XML element by element
            String xmlString = modelXml;
            String line;
            int idx = xmlString.indexOf("> ");
            while (idx != -1) {
                line = xmlString.substring(0, idx+1);
                bw.write(line);
                bw.newLine();   //write the line separator
                xmlString = xmlString.substring(idx+1);
                idx = xmlString.indexOf("> ");
            }
            //write out the end of the XML string
            bw.write(xmlString);
            bw.newLine();
        } catch (IOException ioe) {
            if(ioe instanceof FileNotFoundException && ioe.getMessage().contains("Permission denied"))
                throw new QiwIOException("Insufficient privilege to write.");
            else
            throw new QiwIOException("IO exception writing XML file:"+ioe.getMessage());
        } finally {
            try {
                if (bw != null) bw.close();
            } catch (IOException e) {}
        }
    }

    private LiteParams getLiteParams(LiteParams lite, Element el) {
        lite.setPriorReal(el.getAttribute("priorReal"));
        lite.setPostReal(el.getAttribute("postReal"));
        lite.setTmin(el.getAttribute("tmin"));
        lite.setTmax(el.getAttribute("tmax"));
        lite.setFileNearStack(el.getAttribute("near_stack_dataset"));
        String tmp = el.getAttribute("far_stack_dataset");
        if(tmp == null)
        	tmp = "";
        lite.setFileFarStack(tmp);
        lite.setEp(el.getAttribute("ep"));
        lite.setCdp(el.getAttribute("cdp"));
        lite.setOutputBaseName(el.getAttribute("outputFileBaseName"));
        lite.setNumRuns(el.getAttribute("numRuns"));
        String near = el.getAttribute("near");
        if ("false".equals(near)) {
            lite.setNear(false);
        }
        return lite;
    }

    /**
     * Import state saved in a XML file. Use existing GUI. User not asked to save current
     * state first, because after import, saving state will save imported state. The preferred
     * display name is not changed. Therefore, the name in the GUI title and component tree node
     * don't have to change.
     *
     * @param node XML containing saved state variables
     */
    void importState(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(DeliveryLiteUI.class.getName())) {
                    //get project descriptor from state since GUI needs it
                    //keep the associated project
//                    qiProjectDesc = getProjectDesc(child);
                	restoreState(child);
                    //gui.restoreRunParamState(child);

                    //set title of window
                    //Not necessary to reset the title of the window, for the project didn't change
//                    String projectName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
//                    gui.resetTitle(projectName);
                }
            }
        }
    }

    /** Initialize the plugin, then start processing messages its receives.
     * The pleaseStop flag is set when user has terminated the GUI, and it is time to quit
     * If a message has the skip flag set, it is to be handled by the GUI, not this class
     */
    public void run() {
        init();
        while (stop == false) {
            //Check if associated with a PM. If not (because was restored), discover the PM
            //with a matching PID. There will be one once its GUI comes up.
            String myPid = QiProjectDescUtils.getPid(qiProjectDesc);
            while (projMgrDesc == null && !myPid.equals("")) {
                //NOTE: Request will stay forwever on outstanding request list if no PM with matching PID.
                //      This can occur when restoring the workbench and the PM hasn't been restored
                //      or it is being restored but its GUI is not yet up.
                //Message Dispatcher will broadcast command to all PMs. Only PM with matching PID
                //will send back a response.
                messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_ASSOC_PROJMGR_CMD, QIWConstants.STRING_TYPE, myPid);
                try {
                    //wait and then try again
                    Thread.currentThread().sleep(1000);
                    //if a message arrived, process it. It probably is the response from a PM.
                    if (!messagingMgr.isMsgQueueEmpty()) break;
                } catch (InterruptedException ie) {
                    continue;
                }
            }

            IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();
            if(msg != null){
            	 if(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip()){
            		 if (messagingMgr.getMatchingOutstandingRequest(msg) == null) {
                         msg = messagingMgr.getNextMsg();
                         logger.warning("Consuming response message which lacks a matching outstanding request. Response msg: " + msg);
                     }
            	 } else { // it is a request or asynchronous response, process it
            		 msg = messagingMgr.getNextMsgWait();
            		 try{
            		     processMsg(msg);
            		 }catch(Exception e){
	                   	e.printStackTrace();
	                   	logger.warning(ExceptionMessageFormatter.getFormattedMessage(e));
	                 }
	             }
            }
        }
    }



	public boolean getDeliveryProperties(List<String> properties) {
		if(properties == null)
			properties = new ArrayList<String>();
		else
			properties.clear();
		ArrayList<String> params = new ArrayList<String>();
		// make sure script is executable
		params.add(messagingMgr.getLocationPref());
		params.add("bash");
		params.add("-c");
		String command = "";

		command = " source ~/.bashrc; deliveryAnalyser -h";
		params.add(command);
		params.add("true");

		logger.info("command= " + params.get(3));
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";

		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg response = null;

      	response = messagingMgr.getMatchingResponseWait(msgID,5000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("SUBMIT_JOB_CMD message content = " + errorContent);
				return false;
			}else{
				errorContent = "Messaging timed out !!! ";
				logger.info("SUBMIT_JOB_CMD message content = " + errorContent);
				return false;
			}
		}
		String jobID = (String)MsgUtils.getMsgContent(response);
		status = MsgUtils.getMsgStatusCode(response);
		logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
		logger.info("status of SUBMIT_JOB_CMD normal response " + status);
		//get stdout
		params.clear();
		//params.add(QIWConstants.LOCAL_SERVICE_PREF);
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_JOB_OUTPUT_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);

		if(response == null || MsgUtils.isResponseAbnormal(response)) {
			if(response != null) {
				errorStatus = MsgUtils.getMsgStatusCode(response);
				errorContent = (String)MsgUtils.getMsgContent(response);
				logger.info("GET_JOB_OUTPUT_CMD errorContent= " + errorContent);
				//gui.setStdOutErrorMessage(errorContent,comp);
				//comp.setStdOutErrorMessage(errorContent);
			}else{
				errorContent = "GET_JOB_OUTPUT_CMD request messaging timed out.";
				logger.warning(errorContent);
				//gui.setStdOutErrorMessage(errorContent,comp);
				//comp.setStdOutErrorMessage(errorContent);
				errorStatus = -1;
			}
			return false;
		}
		else{
			ArrayList stdOut = (ArrayList)MsgUtils.getMsgContent(response);
			//status = MsgUtils.getMsgStatusCode(response);
			StringBuffer propertyLines = new StringBuffer();
			for(int i = 0; i < stdOut.size(); i++){
				System.out.println(stdOut.get(i));
				if(stdOut.get(i).equals("Conceivable properties for PROP are:")){
					i++;
					for(;i < stdOut.size(); i++){
						String temp = (String)stdOut.get(i);
						if(!temp.contains("sample-correlation is an indication of the correlation length of MCMC"))
							propertyLines.append(temp);
						else
							break;
					}
				}
			}
			String[] props = propertyLines.toString().split(" +");
			for(String s : props){
				properties.add(s);
			}
			//comp.setProperties(properties);
			return true;
		}
	}


    /**
     * Find the request matching the response and process the response based on the request.
     */
    public void processMsg(IQiWorkbenchMsg msg) {
        IQiWorkbenchMsg request = null;
        logger.fine("msg="+msg.toString());

        if (messagingMgr.isResponseMsg(msg)) {
            request = messagingMgr.checkForMatchingRequest(msg);
            String cmd = MsgUtils.getMsgCommand(request);
            // check if from the message dispatcher
            if (messagingMgr.isResponseFromMsgDispatcher(msg)) {
                if(cmd.equals(QIWConstants.NULL_CMD))
                    return;
            }
            else if (cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD)) {
                ArrayList<String> stdOut = (ArrayList<String>)msg.getContent();
                StringBuffer jobOutput = new StringBuffer();
                for(String s : stdOut){
                    jobOutput.append(s + "\n");
                }
            }
            else if (cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
                jobID = (String)msg.getContent();

            }
            else if(cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
                ArrayList list = (ArrayList)msg.getContent();
                final String selectionPath = (String)list.get(1);

                if (((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
                    final String filePath = (String)list.get(1);
                    String action = (String)list.get(3);

                    if (action.equals(QIWConstants.EXPORT_COMP_STATE)) {
                        System.out.println("DeliveryLite: export state to "+selectionPath);
                        //Write XML state to the specified file and location
                        String pmState = genState();
                        String xmlHeader = "<?xml version=\"1.0\" ?>\n";
    //TODO: handle remote case - use an IO service
                        try {
                            ComponentStateUtils.writeState(xmlHeader+pmState, selectionPath);
                        } catch (QiwIOException qioe) {
                            //TODO notify user cannot write state file
                            logger.finest(qioe.getMessage());
                        }
                    }
                    else if (action.equals(QIWConstants.IMPORT_COMP_STATE)) {
    //TODO: handle remote case - use an IO service
                        //check if file exists
                        if (ComponentStateUtils.stateFileExists(selectionPath)) {
                            try {
                                String pmState = ComponentStateUtils.readState(selectionPath);
                                Node compNode = ComponentStateUtils.getComponentNode(pmState);
                                if (compNode == null) {
                                    JOptionPane.showMessageDialog(gui, "Can't convert XML state to XML object", "Internal Processing Error", JOptionPane.ERROR_MESSAGE);
                                } else {
                                    //check for matching component type
                                    String compType = ((Element)compNode).getAttribute("componentType");
                                    String expectedCompType = messagingMgr.getRegisteredComponentDisplayNameByDescriptor(messagingMgr.getMyComponentDesc());
                                    if (!compType.equals(expectedCompType)) {
                                        JOptionPane.showMessageDialog(gui, "State file not for this qiComponent type. Select again", "Selection Error", JOptionPane.ERROR_MESSAGE);
                                    } else {
                                        //Note: don't ask if want to save state before importing because
                                        //      for future saves, the imported state will be saved.

                                        //import state
                                        importState(compNode);
                                    }
                                }
                            } catch (QiwIOException qioe) {
                                JOptionPane.showMessageDialog(gui, "Can't read state file. Select again.", "Processing Error", JOptionPane.ERROR_MESSAGE);
                                logger.finest(qioe.getMessage());
                            }
                        } else {
                            JOptionPane.showMessageDialog(gui, "State file doesn't exist or is not a file. Select again.", "Selection Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }else if(cmd.equals(QIWConstants.GET_RESULT_FROM_QI_FILE_CHOOSER_CMD)) {
				QiFileChooserDescriptor desc = (QiFileChooserDescriptor)msg.getContent();
				if(desc.getReturnCode()  == JFileChooser.APPROVE_OPTION){
					List<String> list = desc.getSelectedFileNameList();
					if(!desc.isMultiSelectionEnabled()){ //single file selection mode
						String filesep = messagingMgr.getServerOSFileSeparator();
						String filePath = desc.getSelectedFilePathBase() + filesep + list.get(0);
						String command = desc.getMessageCommand();
						if(command.equals(DeliveryLiteConstants.GET_NEAR_SEISMIC_DATASET_CMD) || command.equals(DeliveryLiteConstants.GET_NEAR_WAVELET_FILE_CMD)
								|| command.equals(DeliveryLiteConstants.GET_FAR_SEISMIC_DATASET_CMD) || command.equals(DeliveryLiteConstants.GET_FAR_WAVELET_FILE_CMD)){
							DeliveryLiteUI comp = (DeliveryLiteUI)desc.getParentGUI();
							runGeoFileMetadataTask(comp,filePath,command);
						}else if(command.equals(DeliveryLiteConstants.GET_MODEL_XML_FILE_CMD)){
							gui.importModel(filePath, null);
						}else if(command.equals(DeliveryLiteConstants.GET_MODEL_PARAMS_XML_FILE_CMD))
							gui.importModelAndParams(filePath);
						else if(command.equals(DeliveryLiteConstants.SAVE_MODEL_XML_FILE_CMD))
							gui.exportModelXML(filePath);
						else if(command.equals(DeliveryLiteConstants.SAVE_MODEL_PARAMS_XML_FILE_CMD))
							gui.exportModelParamsXML(filePath);
					}
				}
			}
            else if (cmd.equals(QIWConstants.GET_PROJ_INFO_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);
                if (gui != null) {
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.resetTitle(projName);
                }
            }
            else if (cmd.equals(QIWConstants.GET_ASSOC_PROJMGR_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                projMgrDesc = (IComponentDescriptor)projInfo.get(0);
                qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);
                String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                gui.resetTitle(projName);
            }
            else if (cmd.equals(QIWConstants.CONTROL_QIVIEWER_CMD)) {
                displayTransducer.processResp(msg);
            }
            else
                logger.warning("DeliveryLite Plugin: Response to " + cmd + " command not processed " + msg.toString());
            return;
        }
        //request
        else if (messagingMgr.isRequestMsg(msg)) {
            String cmd = msg.getCommand();
            // deactivate plugin came from user, tell gui to quit and stop run method
            if(cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) || cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
                if(DeliveryLiteConstants.DEBUG_PRINT > 0)
                    System.out.println(CompDescUtils.getDescPreferredDisplayName(messagingMgr.getMyComponentDesc()) + " quitting");
                try{
                    if(gui != null && gui.isVisible())
                        gui.closeGUI();
                    // unregister plugin
                    messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
                }
                catch (Exception e) {
                    IQiWorkbenchMsg res = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Unknown exception occurred with " + myCID);
                    messagingMgr.routeMsg(res);
                }
                stop = true;
                messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,myCID + " is successfully deactivated.");
            } else
            if(cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
                if(gui != null)
                    gui.setVisible(true);
            } else
            if(cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
                if(DeliveryLiteConstants.DEBUG_PRINT > 0)
                    System.out.println(myCID + " quitting");
                if(gui != null){
                	messagingMgr.sendResponse(msg,Component.class.getName(),(Component)gui);
                    gui.closeGUI();
                }
            } else
            if(cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
                String preferredDisplayName = (String)msg.getContent();
                if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
                    CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
                    gui.renamePlugin(preferredDisplayName);
                }
                messagingMgr.sendResponse(msg,QIWConstants.COMP_DESC_TYPE,messagingMgr.getMyComponentDesc());
            } else
//          save state
            if(cmd.equals(QIWConstants.SAVE_COMP_CMD))
                messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE, genState());
//          save component state as clone
            else if(cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
                messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genStateAsClone());
                // restore state
            } else
            if(cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
                Node node = (Node)msg.getContent();
                restoreState(node);

				// Create the finite state machine that will drive the qiViewer when
				// displaying the results of the generated DeliveryLite script.
				displayTransducer = new DeliveryLiteDisplayTransducer(gui, messagingMgr, stepState, actionState);

                messagingMgr.sendResponse(msg,DeliveryLiteUI.class.getName(),gui);
            } else
//          invoke is done after activate
            if(cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
                //Do nothing if don't have a GUI
                // Create the plugin's GUI, but don't make it visible. That is
                // up to the Workbench Manager who requested the plugin be
                // activated. Pass GUI the CID of its parent.
                //check to see if content incudes xml information which means invoke self will
                // do the restoration
                ArrayList msgList = (ArrayList)msg.getContent();
                String invokeType = (String)msgList.get(0);
                if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)){
                    Node node = ((Node)((ArrayList)msg.getContent()).get(2));
                    restoreState(node);
                } else if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)){
//                                qiProjectDesc = ((QiProjectDescriptor)((ArrayList)msg.getContent()).get(2));
                    gui = new DeliveryLiteUI(this,null);
                    gui.resetTitle(null);
                    java.awt.Point p = (java.awt.Point)msgList.get(1);
                    if(p != null)
                  	  gui.setLocation(p);
                }

				// Create the finite state machine that will drive the qiViewer when
				// displaying the results of the generated DeliveryLite script.
				displayTransducer = new DeliveryLiteDisplayTransducer(gui, messagingMgr, stepState, actionState);

                //Send a normal response back with the plugin's JInternalFrame and
                //let the Workbench Manager add it to the desktop and make
                //it visible.
                messagingMgr.sendResponse(msg,DeliveryLiteUI.class.getName(),gui);
            } else
//          user selected rename plugin menu item
            if(cmd.equals(QIWConstants.RENAME_COMPONENT)) {
                ArrayList<String> names = new ArrayList<String>(2);
                names = (ArrayList)msg.getContent();
                if(DeliveryLiteConstants.DEBUG_PRINT > 0)
                    System.out.println("Rename myself from " + names.get(0) + " to " + names.get(1));
                gui.renamePlugin(names.get(1));
            } else
            if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
                //Just a notification. No response required or expected.
                projMgrDesc = (IComponentDescriptor)msg.getContent();
                logger.info("Project Mgr CID = " + projMgrDesc.getCID());
                //Note: Cannot get info about PM's project because GUI may not be up yet.
                if (projMgrDesc != null)
                    messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
            } else
            if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
                ArrayList info = (ArrayList)msg.getContent();
                IComponentDescriptor pmDesc = (IComponentDescriptor)info.get(0);
                QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
                //ignore message if not from associated PM
                if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
                    qiProjectDesc = projDesc;
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    gui.validateProject();
                    gui.resetTitle(projName);

                }
            } else
            if (cmd.equals(QIWConstants.CONTROL_COMPONENT_CMD)) {
            	ArrayList info = (ArrayList)msg.getContent();
            	Document doc = (Document)info.get(0);
            	MessagingManager manager = (MessagingManager)info.get(1);
            	if(manager == null)
            		gui.setResponseComponentDesc(null);
            	if(doc == null)
            		return;
            	try{
	            	JAXBContext  context
	                = JAXBContext.newInstance("com.bhpb.xsdparams.delivery",
	                                  this.getClass().getClassLoader()) ;
	            	Unmarshaller unmarshaller = context.createUnmarshaller() ;
	            	inversion = (Inversion)unmarshaller.unmarshal(doc);
	            	//gui.populate();
	            	//gui.populateTable();
	            	//gui.importModel(modelXML);
	            	gui.resetModel();
            	}catch(Exception e){
            		e.printStackTrace();
            	}
            } else
			if (cmd.equals(QIWConstants.CONTROL_QIVIEWER_CMD)) {
				displayTransducer.processResp(msg);
			}
            else
                logger.warning("DeliveryLitePlugin: Request not processed, requeued: " + msg.toString());
            return;
        }
    }

    /** Send a message to the Workbench Manager to remove this instance from the component tree and send a message to it to deactivate itself.
     */
    public void deactivateSelf() {
        //ask Workbench Manager to remove DeliveryLite from workbench GUI and then send it back to self to deactivate self
        IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.QUIT_COMPONENT_CMD,wbMgr,QIWConstants.STRING_TYPE,
                messagingMgr.getMyComponentDesc());
    }

    /** Invoke File Chooser Service
     * @param list Passed through to fileChooser from caller
     *
     */
    public void callFileChooser(ArrayList list) {
        if(list == null)
            return;
        IComponentDescriptor fileChooser = null;
        String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
        if(response == null) {
            logger.warning("Respone to get file chooser service returning null due to timed out");
            return;
        }
        else if(MsgUtils.isResponseAbnormal(response)) {
            logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
            return;
        }
        else
            fileChooser = (IComponentDescriptor)MsgUtils.getMsgContent(response);

        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,fileChooser,QIWConstants.ARRAYLIST_TYPE,list);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,fileChooser);
        return;
    }

    /**
     * Write script content to a given directory
     * @param dir parameters in which the scripts is stored
     * @param scripts contents
     * @return
     */
    public void writeScript(String dir, String scripts) {
        boolean ok = false;
        String result = checkFileExist(dir);
        if (result.equals("no")) {
            int status = JOptionPane.showConfirmDialog(gui,
                    "The script will be written into " + dir
                  + " but the directory does not exist. Would you like the application to create it for you?",
                    "Confirm action", JOptionPane.YES_NO_OPTION);
            if (status == JOptionPane.YES_OPTION) {
                result = mkDir(dir);
                if (result.equals("success")) ok = true;
            } else
                return;
        } else if (result.equals("yes"))
            ok = true;

        if (ok) {
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            String fileNamePrefix = "wd_" + formatter.format(ts);
            String fileName = fileNamePrefix + ".sh";
            writeStringToFile(messagingMgr.getLocationPref(), dir + messagingMgr.getServerOSFileSeparator()
                             + fileName, scripts);
        }
    }

    /**
     * Write script content to a given path
     * @param path where the scripts is stored
     * @param content scripts contents
     * @return true or false
     */
    public boolean writeStringToFile(String locPref, String path, String content){
        ArrayList<String> params = new ArrayList<String>();
        params.add(locPref);
        params.add(path);
        params.add(content);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,
                                                QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
        if (resp == null) {
            logger.info("Response returning null from command FILE_WRITE_CMD in writeScripts path=" + path);
            return false;
        }else if(resp.isAbnormalStatus()) {
            logger.info("Abnormal response to write scripts FILE_WRITE_CMD:  in writeScripts " + (String)resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in writing script " + path + " cause: " +  (String)resp.getContent(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        } else {
            logger.info("normal response to write scripts FILE_WRITE_CMD: " + (String)resp.getContent());
        }
        return true;
    }

    /**
     * Write script content to a given path
     * @param path where the scripts is stored
     * @param content scripts contents
     * @return true or false
     */
    public boolean writeStringToFile(String path, String content){
        ArrayList<String> params = new ArrayList<String>();
        params.add(messagingMgr.getLocationPref());
        params.add(path);
        params.add(content);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,
                                                QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
        if (resp == null) {
            logger.info("Response returning null from command FILE_WRITE_CMD in writeScripts path=" + path);
            return false;
        }else if(resp.isAbnormalStatus()) {
            logger.info("Abnormal response to write scripts FILE_WRITE_CMD:  in writeScripts " + (String)resp.getContent());
            JOptionPane.showMessageDialog(gui, "Error in writing script " + path + " cause: " +  (String)resp.getContent(), "QI Workbench",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        } else {
            logger.info("normal response to write scripts FILE_WRITE_CMD: " + (String)resp.getContent());
        }
        return true;
    }

    public void submitJob(List<String> params) {
        jobID = null;
        params.add(0,messagingMgr.getLocationPref());
        params.add(1,"sh");
        params.add(2,"-c");
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                       QIWConstants.SUBMIT_JOB_CMD,
                       QIWConstants.ARRAYLIST_TYPE,params);
    }

	/**
	 * Check to see if a given directory exists in the file system
	 * @param filePath file path to be checked
	 * @return yes, no, or error
	 */
    public String checkFileExist(String filePath){
    	ArrayList params = new ArrayList();
    	params.add(messagingMgr.getLocationPref());
    	params.add(filePath);
   		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CHECK_FILE_EXIST_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
   		IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
   		String temp = "";
   		if(resp != null && !MsgUtils.isResponseAbnormal(resp)){
   			temp = (String)resp.getContent();
   		}else{
   			temp = "error";
   		}
   		return temp;
    }

    /**
     * Create a directory of a given path
     * @param filePath as directory name
     * @return yes, no or error
     */
    public String mkDir(String filePath) {
        ArrayList params = new ArrayList();
        params.add(messagingMgr.getLocationPref());
        params.add(filePath);
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CREATE_DIRECTORY_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
        IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId,1000);
        if(response != null && !MsgUtils.isResponseAbnormal(response))
            return (String)MsgUtils.getMsgContent(response);
        else if(response == null) {
            JOptionPane.showMessageDialog(gui,"Timed out problem in running CREATE_DIRECTORY_CMD.",
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            return "error";
        } else {
            JOptionPane.showMessageDialog(gui,"Error in running CREATE_DIRECTORY_CMD. " + (String)MsgUtils.getMsgContent(response),
                    "IO Error",JOptionPane.WARNING_MESSAGE);
            return "error";
        }
    }

    /**
     * call the state manager to store the state
     */
    public void saveState() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }


    /**
     * call the state manager to store the state
     */
    public void saveStateAsClone() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }

    /**
     * call the state manager to store the state then quit the component
     */
    public void saveStateThenQuit() {
        IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
        messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_THEN_QUIT_CMD,stMgrDesc,
                QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
    }


    /** Launch the Wavelet Decomposition plugin:
     *  <ul>
     *  <li>Start up the plugin thread which will initialize the plugin.</li>
     *  </ul>
     * <p>
     * NOTE: Each thread's init() must finish before the next thread is
     * started. This is accomplished by monitoring the SYNC_LOCK object.
     */
    public static void main(String[] args) {
        DeliveryLitePlugin pluginInstance = new DeliveryLitePlugin();
        String cid = args[0];
        pluginThread = new Thread(pluginInstance, cid);
        pluginThread.start();
        long threadId = pluginThread.getId();
        logger.info("Delivery Lite Thread-"+Long.toString(threadId)+" started");
        QIWConstants.SYNC_LOCK.lock();
        QIWConstants.SYNC_LOCK.unlock();
        logger.info("Delivery Lite main finished");
    }

    public void resize() {
    gui.setSize(DeliveryLiteConstants.PREF_WIDTH, DeliveryLiteConstants.PREF_HEIGHT);
    }

    /**
     * Get model object based model name
     */
    public Object getModel(boolean start) {
        Object model = null;
        model = getModelInversion(start);
        return model;
    }

    /**
     * Get model object based model name
     */
    public Object getModel(Node node) {
        try{
        ClassLoader cl = this.getClass().getClassLoader();
    	JAXBContext jc = JAXBContext.newInstance( "com.bhpb.xsdparams.delivery",cl );
        Unmarshaller u = jc.createUnmarshaller();
        inversion = (Inversion)u.unmarshal( node );
        return inversion;
        }catch(JAXBException e){
        	e.printStackTrace();
        }
        return null;
    }

    /**
     * Convert model to XML
     * @deprecated Never used.
     */
    public void toXmlFile(String  modelName){
       toXmlInversionFile(null);
    }

    /** @deprecated Called from toXmlFile() which is never used. */
    private void toXmlInversionFile(String fileName){
        if (inversion == null) return;
        try {
          JAXBContext context
             = JAXBContext.newInstance("com.bhpb.xsdparams.delivery",
                           this.getClass().getClassLoader());

          Marshaller marshaller = context.createMarshaller();
          marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.valueOf(true));
          if (fileName == null) {
              fileName = modelXML;
          }
          marshaller.marshal(inversion, new FileOutputStream(fileName));

        } catch (Exception ex) {
          ex.printStackTrace();
        }
    }


    /**
     * Read in the XML model.
     * @param start True forces reading in the XML model.
     * @return The model object.
     */
    private Object getModelInversion(boolean start) {
        URL modelURL   = null;
        InputSource is = null;
        try {
           if (start || inversion == null) {
              JAXBContext  context
             = JAXBContext.newInstance("com.bhpb.xsdparams.delivery",
                               this.getClass().getClassLoader()) ;

              Unmarshaller unmarshaller = context.createUnmarshaller() ;
              if (modelXML != null) {
            	  String status = checkFileExist(modelXML);
            	  if(status != null && status.equals("no")) // file does not exist or problem with checking
                  //File tempFile = new File(modelXML);
                  //if (!tempFile.exists()) {
                      modelXML = null;
                  //}
              }
              //If no XML model was set or the XML model file doesn't
              //exist, use the default XML model.
              if (modelXML == null) {
                  ClassLoader cl = getClass().getClassLoader();
                  modelURL = cl.getResource(DEFAULT);
                  if (modelURL == null) {
                      File f = new File(DEFAULT);
                      modelURL = f.toURI().toURL();
                  }
                  is = new InputSource(modelURL.toString());
                  inversion = (Inversion)unmarshaller.unmarshal(is) ;
                  modelXML = null;
              } else {
                  //Change the first <layer> to <top> which is required by the
                  //DeliveryLite Schema. NOTE: In the Delivery schema, the first
                  //layer is <layer> and its default name 'top'. JAXB wll not allow
                  //two elements to both be called <layer>. Besides, the XML elements
                  //for the first <layer> (top) are not the same as the other <layer>s
                  //whose default name is 'middle'.
                  //NOTE: Changing the first <layer> to <top> is an artifact of JAXB that
                  //can be gotten around if we wrote our own XML parser.
                  ///String modelFilepath = deliveryToLiteModel(modelXML);

                  //inversion = (Inversion)unmarshaller.unmarshal(new FileInputStream(modelFilepath)) ;
            	  Document doc = getXMLDocumentByXMLFilePath(modelXML);
            	  boolean isValid = checkValidDeliveryLiteDocument(doc,true);
            	  if(isValid)
            		  inversion = (Inversion)unmarshaller.unmarshal(doc) ;
            	  else{
            		  JOptionPane.showMessageDialog(gui, "The xml document does not appear to be a valid Model xml document.", "Invalid Document Found", JOptionPane.WARNING_MESSAGE);
            	  }

              }
           }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return inversion;
    }

    /**
     * Determine if the given document is a valid DeliveryLite document
     * @param doc  the given document
     * @param modelOnly  if valid document return only model node
     * @return true or false
     */
    public boolean checkValidDeliveryLiteDocument(Node node, boolean modelOnly){
    	if(node == null)
    		return false;
    	NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE){
                if (child.getNodeName().equals(DeliveryLiteUI.class.getName())){
                	if(!modelOnly)
                		return true;
                	else
                		return false;
                }else if(child.getNodeName().equals("inversion")){
                	if(modelOnly)
                		return true;
                	else
                		return false;
                }else
                	return checkValidDeliveryLiteDocument(child, modelOnly);
            }
        }
        return false;
    }

    /**
     * Get xml string by a given XML source file
     * @param path the file path
     * @return String
     */
    public String getXMLStringByXMLFilePath(String filePath){
    	if(filePath == null || filePath.trim().length() == 0)
    		return null;
         ArrayList<String> params = new ArrayList<String>();

         //Send a file read message to retrieve the ascii XML string
         //the first element of list is location preferenec
         params.add(messagingMgr.getLocationPref());
         //the second element is to add file path and name user just selected
         params.add(filePath);

         String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.FILE_READ_CMD, QIWConstants.ARRAYLIST_TYPE, params,true);
         IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,10000);
         if (resp == null){
        	 JOptionPane.showMessageDialog(gui, "Error in reading " + filePath + " due to timed out problem in retrieving message.", "QI Workbench",
        			 JOptionPane.WARNING_MESSAGE);
        	 return null;
         }else if(resp.isAbnormalStatus()) {
        	 logger.finest("IO error:"+(String)resp.getContent());
        	 JOptionPane.showMessageDialog(gui, "Error in reading " + filePath + "Cause: " + (String)resp.getContent(), "QI Workbench",
        			 JOptionPane.WARNING_MESSAGE);
        	 return null;
         }

         ArrayList<String> strList = (ArrayList<String>)resp.getContent();
         StringBuffer xml = new StringBuffer();
         for(String s : strList){
        	 xml.append(s);
         }
         return xml.toString();

    }

    /**
     * Get xml document by a given XML source file
     * @param path the file path
     * @return Document
     */
    private Document getXMLDocumentByXMLFilePath(String filePath){
    	if(filePath == null || filePath.trim().length() == 0)
    		return null;
    	Document doc = gui.createDOMforXML(filePath);
    	return doc;
    }


    public Inversion getInversion() {
        return inversion;
    }

    public void setInversion(Inversion inversion) {
        this.inversion = inversion;
    }

    public String getModelXML() {
        return modelXML;
    }

    /**
     * Save the path of the XML model.
     * @modelXML The path of the XML model file.
     */
    public void setModelXML(String modelXML) {
        this.modelXML = modelXML;
    }

    public DeliveryLiteUI getGui() {
        return gui;
    }

    public void setGui(DeliveryLiteUI gui) {
        this.gui = gui;
    }

    public String getModelDir() {
    	String filesep = messagingMgr.getServerOSFileSeparator();
        return QiProjectDescUtils.getTempPath(qiProjectDesc) + filesep;
    }

    /**
     * Get the name of the XML file.
     * @return The name of the XML file
     */
    public String getModelFilename() {
        if (modelXML == null || modelXML.equals("")) return "delivDefault.xml";
        int idx = modelXML.lastIndexOf(File.separator);
        if (idx == -1) return "delivDefault.xml";
        return modelXML.substring(idx+1);
    }

    public void saveRunParams(String baseName) {
        LiteParams lite = gui.getLiteParams();
        Properties props = new Properties();
        FileOutputStream ostream = null;
        try {
            props.setProperty("project_name", QiProjectDescUtils.getQiProjectName(qiProjectDesc));
            props.setProperty("project_root", QiProjectDescUtils.getQiSpace(qiProjectDesc));
            props.setProperty("priorReal", lite.getPriorReal());
            props.setProperty("priorPost", lite.getPostReal());
            props.setProperty("numRuns", lite.getNumRuns());
            props.setProperty("tmin", lite.getTmin());
            props.setProperty("tmax", lite.getTmax());
            props.setProperty("ep", lite.getEp());
            props.setProperty("cdp", lite.getCdp());
            String near = "Y";
            if (!lite.isNear()) {
              near = "N";
            }
            props.setProperty("near", near);
            String fileName = baseName + ".properties";
            ostream = new FileOutputStream(fileName);
            props.store(ostream, null);
        } catch (IOException ex){
               ex.printStackTrace();
        }finally{
        	try{
        		if(ostream != null)
        			ostream.close();
        	}catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }

    public LiteParams getRunParams(String baseName) {
        LiteParams lite = gui.getLiteParams();
        String propFile  = baseName + ".properties";
        Properties props = new Properties();
        String ret = null;
        FileInputStream instream = null;
        try {
        	instream = new FileInputStream(propFile);
            props.load(instream);

            ret = props.getProperty("project_name");
            QiProjectDescUtils.setQiProjectName(qiProjectDesc, ret);
            ret = props.getProperty("project_root");
            QiProjectDescUtils.setQiSpace(qiProjectDesc, ret);

            ret = props.getProperty("priorReal");
            lite.setPriorReal(ret);
            ret = props.getProperty("priorPost");
            lite.setPostReal(ret);
            ret = props.getProperty("numRuns");
            lite.setNumRuns(ret);
            ret = props.getProperty("tmin");
            lite.setTmin(ret);
            ret = props.getProperty("tmax");
            lite.setTmax(ret);
            ret = props.getProperty("ep");
            lite.setEp(ret);
            ret = props.getProperty("cdp");
            lite.setCdp(ret);
            ret = props.getProperty("near");
            if ("N".equals(ret)) {
                lite.setNear(false);
            } else {
                lite.setNear(true);
            }

        } catch (IOException ex){
               ex.printStackTrace();
        }finally{
        	try{
        		if(instream != null)
        			instream.close();
        	}catch (IOException ex){
                ex.printStackTrace();
            }
        }
        return lite;
    }

    public void showScriptPanels() {
        gui.showScriptPanels();
    }

    public String toXmlStrInversion(){
        String ret = "";
        if (inversion == null) {
            ObjectFactory objFactory = new ObjectFactory();
            inversion = objFactory.createInversion();
        }

        try {
           JAXBContext context
              = JAXBContext.newInstance("com.bhpb.xsdparams.delivery",
                                        this.getClass().getClassLoader());
           Marshaller marshaller = context.createMarshaller();
           marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.valueOf(true));
           ByteArrayOutputStream bos = new ByteArrayOutputStream();
           marshaller.marshal(inversion, bos);
           ret = bos.toString();
        }catch (Exception ex) {
            ex.printStackTrace();
        }

        return ret;
    }

    /**
     * Transform a Delivery XML model to a DeliveryLite XML model. That is,
     * change the first <layer> in the <model_description> to <top>. If it is
     * already <top>, then the XMl model is a DeliveryLite one.
     * @param modelFilepath Full path of the XML model file.
     * @return Full path of the XML model file.
     */
    private String deliveryToLiteModel(String modelFilepath) throws QiwIOException {
        String modelDir = getModelDir();
        String tempModelFile = "dl_temp_Model.xml";

        //Read XML model file until find <model_description>
        File modelIn = new File(modelFilepath);

        if (!modelIn.exists()) throw new QiwIOException("specified file does not exist; path="+modelFilepath);

        if (!modelIn.isFile()) throw new QiwIOException("specified file is not a file; path="+modelFilepath);

        File modelOut = new File(modelDir + tempModelFile);
        BufferedWriter bw = null;
        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(modelIn);
            br = new BufferedReader(new InputStreamReader(fis));
            FileOutputStream fos = new FileOutputStream(modelOut);
            bw = new BufferedWriter(new OutputStreamWriter(fos));

            String line = br.readLine();
            // read file line by line
            while (line != null) {
                if (line.indexOf("<model_description>") != -1) {
                    bw.write(line);
                    bw.newLine();   //write the line separator

                    //check if next line is <top> or <layer>
                    line = br.readLine();
                    //check if this is a DeliveryLite model
                    if (line!= null && line.indexOf("<top>") != -1) return modelFilepath;
                    if (line!= null && line.indexOf("<layer>") != -1) {
                        line = line.replace("<layer>", "<top>");
                        bw.write(line);
                        bw.newLine();   //write the line separator

                        //find matching </layer> and change to </top>
                        line = br.readLine();
                        boolean skipCheck = false;
                        while (line != null) {
                            if (!skipCheck && line.indexOf("</layer>") != -1) {
                                line = line.replace("</layer>", "</top>");
                                bw.write(line);
                                bw.newLine();   //write the line separator
                                skipCheck = true;
                            } else {
                                //write out line as is
                                bw.write(line);
                                bw.newLine();   //write the line separator
                            }
                            line = br.readLine();
                        }
                    }
                } else {
                    //write out line as is
                    bw.write(line);
                    bw.newLine();   //write the line separator
                }

                line = br.readLine();
            }
            if (br != null) br.close();
            if (bw != null) bw.close();
        } catch (IOException ioe) {
            throw new QiwIOException("IO exception reading/writing XML model file:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
                if (bw != null) bw.close();
            } catch (IOException e) {
            	e.printStackTrace();
            }
        }

        return modelOut.getAbsolutePath();
    }
}
