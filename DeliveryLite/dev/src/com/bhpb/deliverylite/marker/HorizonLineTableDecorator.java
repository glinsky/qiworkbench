/**
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.marker;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.EventObject;

import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import com.bhpb.deliverylite.DeliveryLiteUI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableCellRenderer;

public class HorizonLineTableDecorator
{
	private JTable table;  //layer table
	private TableModel fixedTableModel;
	private Component parentGui;
    private static Logger logger = Logger.getLogger(HorizonLineTableDecorator.class.getName());
	public HorizonLineTableDecorator(Component parentGui, JTable table, TableModel fixedTableModel){
		this.table = table;
		this.fixedTableModel = fixedTableModel;
		this.parentGui = parentGui;
		run();
	}
	public HorizonLineTableDecorator(){
		run();
	}
	public void run()
	{
		
		for(int i = 0; i < table.getColumnModel().getColumnCount(); i++){
			TableColumn column = table.getColumnModel().getColumn(i);
			column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
			column.setCellRenderer(new MyCellRenderer());
			//column.setCellEditor(new MyCellEditor(table.getModel()));
		}
        table.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                Point p = new Point(e.getX(), e.getY());

                int row = table.rowAtPoint(p);
                HorizonLineTableModel model = (HorizonLineTableModel)table.getModel();
                int thickness = model.getLineWidth(row);
                if(thickness > 0){
                    table.setToolTipText("line thickness:"  + thickness);
                }else{
                    table.setToolTipText(null);
                }
            }
        });
		table.addMouseListener(new MouseAdapter(){
			private void ShowPopup(MouseEvent e) {
				if (e.isPopupTrigger() && table.isEnabled()) {
					Point p = new Point(e.getX(), e.getY());
					int col = table.columnAtPoint(p);
					int row = table.rowAtPoint(p);

					// translate table index to model index
					int mcol = table.getColumn(table.getColumnName(col))
					.getModelIndex();

					if (row >= 0 && row < table.getRowCount()) {
						//cancelCellEditing();
						// create popup menu...
						JPopupMenu contextMenu = createContextMenu(row,
								mcol);
						// ... and show it
						if (contextMenu != null
								&& contextMenu.getComponentCount() > 0) {
							contextMenu.show(table, p.x, p.y);
						}
					}
				}
			}

			public void mousePressed(MouseEvent e) {
				ShowPopup(e);
			}
			
			public void mouseReleased(MouseEvent e) {
                
				ShowPopup(e);
			}
		});		
	}

	private class MyCellEditor extends DefaultCellEditor implements TableCellEditor{
		private Object value; //!! to hold pre-edited value  

		public MyCellEditor(TableModel model){
			super( new javax.swing.JTextField() );  
		}
		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {

			this.value = value;  //!! initialize value
			String valueAsText = "";
			if(value != null)
				valueAsText = value.toString();
			JTextField asTextField = (JTextField) super.getComponent();
	        asTextField.setText(valueAsText);

	        return asTextField;
			
		}


		public void cancelCellEditing() {
			// TODO Auto-generated method stub
			
		}
        @Override
		public Object getCellEditorValue() {
			return super.getCellEditorValue();
		}
        @Override
		public boolean isCellEditable(EventObject anEvent) {
			if(anEvent instanceof MouseEvent){
				MouseEvent me = (MouseEvent)anEvent;
				Point p = me.getPoint();
				javax.swing.JTable table = (javax.swing.JTable)anEvent.getSource();
				int c = table.columnAtPoint(p);
				int r = table.rowAtPoint(p);
				String type = (String)fixedTableModel.getValueAt(r, DeliveryLiteUI.LAYER_TYPE_COLUMN);
		        if(!type.equals(DeliveryLiteUI.SAND_LAYER_TYPE) && c >= 6){
		            return false;
		        }else{
		        	return true;
		        }
			}
			return false;
		}
	}
	
	
	private JPopupMenu createContextMenu(final int rowIndex,
			final int columnIndex) {
		JPopupMenu contextMenu = new JPopupMenu();

		JMenuItem addMenu = new JMenuItem();
		addMenu.setText("Add Layer Top");
		addMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addLayerTopActionPerformed(e,rowIndex);
			}
		});
		contextMenu.add(addMenu);
		HorizonLineTableModel mymodel = (HorizonLineTableModel)table.getModel();
		if(mymodel.getLineColor( rowIndex) != null)
			addMenu.setEnabled(false);
		JMenuItem removeMenu = new JMenuItem();
		removeMenu.setText("Remove Layer Top");
		removeMenu.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					HorizonLineTableModel model = (HorizonLineTableModel)table.getModel();
					model.removeLine(rowIndex);
					model.fireTableRowsUpdated(rowIndex, rowIndex);
				}
			});
		contextMenu.add(removeMenu);


        JMenuItem changeMenu = new JMenuItem();
		changeMenu.setText("Change Layer Top");
		if(mymodel.getLineColor(rowIndex) == null){
			changeMenu.setEnabled(false);
			removeMenu.setEnabled(false);
		}
		changeMenu.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					updateLayerTopActionPerformed(e,rowIndex);
				}
			});
		
		contextMenu.add(changeMenu);
		
		return contextMenu;
	}
	
	
	private JPanel buildCustomCellPanel(final TableModel tableModel, final int row, final int column, Object value){
		final HorizonLineTableModel model = (HorizonLineTableModel)tableModel;
		JPanel panel = new JPanel();
		Rectangle rect = table.getCellRect(row, column, false);
		panel.setPreferredSize(rect.getSize());
		JPanel graphicPanel = new JPanel(){
        	/**
             * Overrids the method of JPanel.
             */
            protected void paintComponent(Graphics g) {
            	
            	if(model.getLineWidth(row) == 0)
            		return;
                //Dimension d = getSize();
                Dimension d = getPreferredSize();
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                g2d.setColor(getBackground());
                g2d.fillRect(0, 0, d.width, d.height);
                //System.out.println("d.width=" + d.width);
                //System.out.println("d.height=" + d.height);
                g2d.setColor(Color.black);
                ///g2d.setFont(font);
                ///if(value != null){
                ////g2d.drawString(value.toString(), 5, d.height / 2 + 2);
                ////g2d.setStroke(new BasicStroke(value.intValue(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
                ////}
                //g2d.drawLine(d.width / 2 - 10, d.height / 2, d.width - 10, d.height / 2);
                g2d.setColor(model.getLineColor(row));
                //g2d.setStroke(new BasicStroke(model.getLineWidth(row), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
                //g2d.setStroke(new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));                
                //g2d.drawLine(0, d.height-2, d.width, d.height-2);
                //g2d.drawLine(0, 0, d.width,0 );
                g2d.fillRect(0, 0, d.width, model.getLineWidth(row));
            }
        };

        graphicPanel.setPreferredSize(new Dimension(panel.getPreferredSize().width,(int)(panel.getPreferredSize().height/3.0f)));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    	if(model.getLineWidth(row) != 0)
    		panel.add(graphicPanel);
		//Object val = tableModel.getValueAt(row, column);

		String cellVal = "";
		if(value != null){
			cellVal = value.toString();
		}
		JTextField tf = new JTextField(cellVal);
        //tf.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		tf.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		tf.selectAll();
		/*tf.addFocusListener(new FocusAdapter(){
			public void focusLost(FocusEvent e){
				if(e.getSource() instanceof JTextField) {
					JTextField src = (JTextField) e.getSource();
					String text =  src.getText();
					if(text != null)
						text = text.trim();
					else
						text = "";
					
					float fvalue = 0;
					if(text.length() != 0){
					try{
						fvalue = Float.parseFloat(text);
					}catch(NumberFormatException nfe){
						nfe.printStackTrace();
						JOptionPane.showMessageDialog(null, "Invalid numeric value", "Invalid Value", JOptionPane.WARNING_MESSAGE);
						src.setText(String.valueOf(tableModel.getValueAt(row, column)));
						return;
					}
					tableModel.setValueAt(String.valueOf(fvalue), row, column);
					}else
						tableModel.setValueAt("", row, column);
				}
			}
		});*/
		/*tf.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				//if(e.getClickCount() == 2){
				//	tf.setSelectionStart(0);
				//	tf.setSelectionEnd(tf.getText().trim().length());
				//}
				if(e.getSource() instanceof JTextField) {
					JTextField src = (JTextField) e.getSource();
					String text =  src.getText().trim();
					float fvalue = 0;
					try{
						fvalue = Float.parseFloat(text);
					}catch(NumberFormatException nfe){
						nfe.printStackTrace();
						JOptionPane.showMessageDialog(null, "Invalid numeric value", "Invalid Value", JOptionPane.WARNING_MESSAGE);
						src.setText(String.valueOf(tableModel.getValueAt(row, column)));
						return;
					}
					tableModel.setValueAt(fvalue, row, column);
				}
				
			}
		});
		*/
		panel.add(tf);
		tf.setPreferredSize(new Dimension(panel.getPreferredSize().width,(int)(2*panel.getPreferredSize().height/3.0f)));		
		String type = (String)fixedTableModel.getValueAt(row, DeliveryLiteUI.LAYER_TYPE_COLUMN);
        if(!type.equals(DeliveryLiteUI.SAND_LAYER_TYPE) && column >= 6){
            tf.setBackground(Color.LIGHT_GRAY);
            tf.setEditable(false);
        }else{
        	tf.setBackground(Color.WHITE);
        	tf.setEditable(true);
        }

        tf.setForeground(Color.BLACK);
		//panel.add(c);
        panel.revalidate();
		return panel;
	}
	
	private void addLayerTopActionPerformed(ActionEvent e, int rowIndex){
		HorizonLineSettingsPanel view = new HorizonLineSettingsPanel();
		HorizonLineTableModel tmodel = (HorizonLineTableModel)table.getModel();
		HorizonLineSettings model = new HorizonLineSettings();
		HorizonLineSettingsController controller = new HorizonLineSettingsController(model,view);
		//JDialog dlg = new EditorDialog((JFrame) SwingUtilities.getRoot(frame), view, controller);
		HorizonLineSettingsDialog dlg = new HorizonLineSettingsDialog((JFrame) SwingUtilities.getRoot(parentGui),true,view,controller);
        dlg.setVisible(true);
        if(dlg.getReturnCode() == javax.swing.JOptionPane.CANCEL_OPTION)
        	return;
        //System.out.println("Line width=" + controller.getLineWidth());
        //System.out.println("Line color=" + controller.getLineColor().toString());
		//System.out.println("rowIndex=" + rowIndex);
		tmodel.setLineColor(rowIndex, controller.getLineColor());
		tmodel.setLineWidth(rowIndex, controller.getLineWidth());
		//model.fireTableRowsUpdated(rowIndex, rowIndex);
		
		tmodel.fireTableRowsUpdated(rowIndex, rowIndex);
		//table.setModel(tmodel);
	}
	
	private void updateLayerTopActionPerformed(ActionEvent e, int rowIndex){
		HorizonLineSettingsPanel view = new HorizonLineSettingsPanel();
		HorizonLineTableModel tmodel = (HorizonLineTableModel)table.getModel();
		HorizonLineSettings model = new HorizonLineSettings(tmodel.getLineColor(rowIndex),tmodel.getLineWidth(rowIndex));
		HorizonLineSettingsController controller = new HorizonLineSettingsController(model,view);
		view.setLineColor(controller.getLineColor());
		view.setLineWidth(controller.getLineWidth());
		//JDialog dlg = new EditorDialog((JFrame) SwingUtilities.getRoot(frame), view, controller);
		HorizonLineSettingsDialog dlg = new HorizonLineSettingsDialog((JFrame) SwingUtilities.getRoot(parentGui),true,view,controller);
		dlg.setModal(true);
		dlg.setSize(400,200);
        dlg.setVisible(true);
        if(dlg.getReturnCode() == javax.swing.JOptionPane.CANCEL_OPTION)
        	return;
        //System.out.println("Line width=" + controller.getLineWidth());
        //System.out.println("Line color=" + controller.getLineColor().toString());
		//System.out.println("rowIndex=" + rowIndex);
		tmodel.setLineColor(rowIndex, controller.getLineColor());
		tmodel.setLineWidth(rowIndex, controller.getLineWidth());

		tmodel.fireTableRowsUpdated(rowIndex, rowIndex);
	}

    /**
     * A data structure to holds horizon marker panel to avoid unnecessarily allocating
     * JPanel type of container when user's action triggers resizing the component
     * which causes the repetitive rendering and new object creation;
     * similiar to 2*2 array but faster to retrieve
     * <row,<column,panel>>
     */
    private Map<String,Map<String,HorizonMarkerEditPanel>> rowHorizonMarkerMap =
            new HashMap<String,Map<String,HorizonMarkerEditPanel>>();

    public void initRowHorizonMarkerMap(){
        Set<String> sets = rowHorizonMarkerMap.keySet();
        Iterator it = sets.iterator();
        while(it.hasNext()){
            String key = (String)it.next();
            rowHorizonMarkerMap.get(key).clear();
        }
        rowHorizonMarkerMap.clear();
    }

    
    //keyed by row number
	private class MyCellRenderer extends DefaultTableCellRenderer
	{
        public MyCellRenderer(){
            setOpaque(true); //MUST do this for background to show up.
		}

        @Override
		public Component getTableCellRendererComponent(JTable table, Object
				value, boolean isSelected, boolean hasFocus, int row, int column)
		{
			//Component c =
			//	originalCellRenderer.getTableCellRendererComponent (table, value,
			//			isSelected, hasFocus, row, column);
            if(table != null){
                HorizonLineTableModel model = (HorizonLineTableModel)table.getModel();
                if(model.getLineWidth(row) == 0){
                    super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    String type = (String)fixedTableModel.getValueAt(row, DeliveryLiteUI.LAYER_TYPE_COLUMN);
                    if(!type.equals(DeliveryLiteUI.SAND_LAYER_TYPE) && column >= 6)
                        setBackground(Color.LIGHT_GRAY);
                    else
                        setBackground(Color.WHITE);
                    if(isSelected)
                        setBackground(Color.CYAN);
                    return this;
                }
            }
			//
            Map<String,HorizonMarkerEditPanel> hmep = null;
            HorizonMarkerEditPanel panel = null;
            if(rowHorizonMarkerMap.containsKey(String.valueOf(row))){
                hmep = rowHorizonMarkerMap.get(String.valueOf(row));
                if(hmep.containsKey(String.valueOf(column))){
                    panel = hmep.get(String.valueOf(column));
                    if(table.isCellSelected(row, column)){
                        panel.setBackground(Color.CYAN);
                        panel.setTextFieldBackgroundColor(Color.CYAN);
                    }else{
                        String type = (String)fixedTableModel.getValueAt(row, DeliveryLiteUI.LAYER_TYPE_COLUMN);
                        if(!type.equals(DeliveryLiteUI.SAND_LAYER_TYPE) && column >= 6){
                            panel.setBackground(Color.LIGHT_GRAY);
                            panel.setTextFieldBackgroundColor(Color.LIGHT_GRAY);
                        }else{
                            panel.setBackground(Color.WHITE);
                            panel.setTextFieldBackgroundColor(Color.WHITE);
                        }
                    }
                    if(value != null){
                        panel.setTextValue(value.toString());
                    }
                }
            }else
                hmep = new HashMap<String,HorizonMarkerEditPanel>();

            if(panel == null){
                panel = new HorizonMarkerEditPanel(table,fixedTableModel,row, column,value);
                if(table.isCellSelected(row, column)){
                    panel.setBackground(Color.CYAN);
                    panel.setTextFieldBackgroundColor(Color.CYAN);
                }else{
                    String type = (String)fixedTableModel.getValueAt(row, DeliveryLiteUI.LAYER_TYPE_COLUMN);
                    if(!type.equals(DeliveryLiteUI.SAND_LAYER_TYPE) && column >= 6){
                        panel.setBackground(Color.LIGHT_GRAY);
                        panel.setTextFieldBackgroundColor(Color.LIGHT_GRAY);
                    }else{
                        panel.setBackground(Color.WHITE);
                        panel.setTextFieldBackgroundColor(Color.WHITE);
                    }
                }
                hmep.put(String.valueOf(column),panel);
                rowHorizonMarkerMap.put(String.valueOf(row), hmep);
            }
            
            return panel;
		}
	}

}