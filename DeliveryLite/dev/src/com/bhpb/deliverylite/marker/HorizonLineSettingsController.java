/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.marker;
import java.awt.Color;
import java.util.logging.Logger;

import com.bhpb.geographics.controller.AbstractEditorPanelController;

public class HorizonLineSettingsController
		extends	AbstractEditorPanelController<HorizonLineSettings, HorizonLineSettingsPanel> {
	private static final Logger logger = Logger
			.getLogger(HorizonLineSettingsController.class.toString());

	public HorizonLineSettingsController(HorizonLineSettings model,
			HorizonLineSettingsPanel view) {
		super(model, view);
		view.setController(this);
	}

	public void run() {
		view.setLineColor(model.getLineColor());
		view.setLineWidth(model.getLineWidth());
	}

	public void setLineWidth(int width) {
		model.setLineWidth(width);
	}

	public void setLineColor(Color color) {
		model.setLineColor(color);
	}
	
	public int getLineWidth() {
		return model.getLineWidth();
	}

	public Color getLineColor() {
		return model.getLineColor();
	}

}