/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.marker;
import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;
import java.awt.Color;
import java.io.StringReader;
import org.w3c.dom.Node;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */
public class HorizonLineSettings {

    private int lineWidth = 6;
    private Color lineColor = Color.YELLOW;

    public HorizonLineSettings(){};
    public HorizonLineSettings(Color color, int width){
    	setLineColor(color);
    	this.lineWidth = width;
    }
    
    public int getLineWidth(){
        return lineWidth;
    }

    public void setLineWidth(int width){
        lineWidth = width;
    }


    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color color) {
    	if(color == null)
    		this.lineColor = Color.YELLOW;
    	else
    		this.lineColor = color;
    }


    public static String getXmlAlias() {
        return "horizonLineSettings";
    }

    /**
     * Deserializes a HorizonHighlightSettings.
     *
     * @param node w3c.dom.node created from the XML String serialization of PickingSettings
     *
     * @return the deserialized PickingSettings object
     */
    public static HorizonLineSettings restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        String xml = ElementAttributeReader.nodeToXMLString(node);

        HorizonLineSettings settings = (HorizonLineSettings) xStream.fromXML(new StringReader(xml));

        return settings;
    }

    /**
     * Serializes the state of this <code>HorizonHighlightSettings</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() {
        XStream xStream = XStreamUtil.getInstance();
        return xStream.toXML(this);
    }

}
