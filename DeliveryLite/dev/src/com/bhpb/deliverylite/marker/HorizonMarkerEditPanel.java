/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.marker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import com.bhpb.deliverylite.DeliveryLiteUI;

public class HorizonMarkerEditPanel extends javax.swing.JPanel{
	private int row;
	private int column;
	private HorizonLineTableModel model;
	private TableModel fixedTableModel;
	private javax.swing.JTable table;
	private Object value;
	private javax.swing.JTextField textField;
	public HorizonMarkerEditPanel(){}
	public HorizonMarkerEditPanel(javax.swing.JTable table, TableModel fixTableModel, int row, int column, Object value){
		this.table = table;
		this.model = (HorizonLineTableModel)table.getModel();
		this.fixedTableModel = fixTableModel;
		this.row = row;
		this.column = column;
		this.value = value;
		
		Rectangle rect = table.getCellRect(row, column, false);
		this.setPreferredSize(rect.getSize());
        javax.swing.JPanel graphicPanel;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		if(model.getLineWidth(row) != 0){
            graphicPanel = formGraphicPanel();
            //graphicPanel.setPreferredSize(new Dimension(this.getPreferredSize().width,(int)(this.getPreferredSize().height/3.0f)));
            graphicPanel.setPreferredSize(new Dimension(this.getPreferredSize().width,model.getLineWidth(row)));
    		this.add(graphicPanel);
        }

		String cellVal = "";
		if(value != null){
			cellVal = value.toString();
		}
		textField = new JTextField(cellVal);
        //tf.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		textField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		textField.selectAll();
		
		this.add(textField);

		textField.setPreferredSize(new Dimension(this.getPreferredSize().width,(int)(2*this.getPreferredSize().height/3.0f)));
		String type = (String)fixedTableModel.getValueAt(row, DeliveryLiteUI.LAYER_TYPE_COLUMN);
        if(!type.equals(DeliveryLiteUI.SAND_LAYER_TYPE) && column >= 6){
        	textField.setBackground(Color.LIGHT_GRAY);
        	textField.setEditable(false);
        }else{
        	textField.setBackground(Color.WHITE);
        	textField.setEditable(true);
        }

        textField.setForeground(Color.BLACK);
		//panel.add(c);
        //wf revalidate();
	}
	
	public String getTextValue(){
		return textField.getText();
	}

    public void setTextValue(String val){
        textField.setText(val);
    }

    public void setTextFieldBackgroundColor(Color c){
        textField.setBackground(c);
    }    
	private javax.swing.JPanel formGraphicPanel(){
		JPanel graphicPanel = new JPanel(){
        	/**
             * Overrids the method of JPanel.
             */
            @Override
            protected void paintComponent(Graphics g) {
            	
            	if(model.getLineWidth(row) == 0)
            		return;
                //Dimension d = getSize();
                Dimension d = getPreferredSize();
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                /////g2d.setColor(getBackground());
                /////g2d.fillRect(0, 0, d.width, d.height);
                //System.out.println("d.width=" + d.width);
                //System.out.println("d.height=" + d.height);
                //////g2d.setColor(Color.black);
                ///g2d.setFont(font);
                ///if(value != null){
                ////g2d.drawString(value.toString(), 5, d.height / 2 + 2);
                ////g2d.setStroke(new BasicStroke(value.intValue(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
                ////}
                //g2d.drawLine(d.width / 2 - 10, d.height / 2, d.width - 10, d.height / 2);
                g2d.setColor(model.getLineColor(row));
                //g2d.setStroke(new BasicStroke(model.getLineWidth(row), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
                //g2d.setStroke(new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));                
                //g2d.drawLine(0, d.height-2, d.width, d.height-2);
                //g2d.drawLine(0, 0, d.width,0 );
                g2d.fillRect(0, 0, d.width, model.getLineWidth(row));
            }
        };
        return graphicPanel;
	}
}
