package com.bhpb.deliverylite.marker;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.apache.commons.validator.routines.FloatValidator;

import com.bhpb.deliverylite.DeliveryLiteUI;

/*
###########################################################################
# DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
# Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
############################################################################
*/
public class HorizonLineTableModel extends DefaultTableModel {
	
	private int targetRowIndex = -1;
	private DeliveryLiteUI view;
	//key by row index string 
	private Map<String,LineAttributes> rowLineAttributes = new HashMap<String,LineAttributes>();
	public HorizonLineTableModel(DeliveryLiteUI view, Object[][] data, Object[] columnNames) {
        super(data, columnNames);
        this.view = view;
    }
	
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    	if(aValue == null)
    		return;
        String val = (String)aValue;
        
        val = val.trim();
        if(val.length() > 0){
            if(!FloatValidator.getInstance().isValid(val)){
                view.showValidationFailure("Invalid numeric value.");
                return;
            }else{
                float fval = Float.valueOf(val).floatValue();
                if(columnIndex >= 6 && columnIndex <= 10 ){
                    if(fval < 0 || fval > 1){
                        view.showValidationFailure("Value must be between 0 and 1.");
                        return;
                    }
                }
            }
        }else if(val.length() == 0 && (columnIndex == 0 || columnIndex == 1 || columnIndex == 5)){
        	view.showValidationFailure("Non-Blank value is required.");
        	return;
        }
        if(columnIndex < 0 || rowIndex < 0)
        	return;
        Vector rowVector = (Vector)dataVector.elementAt(rowIndex);
        rowVector.setElementAt(aValue, columnIndex);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
	public void resetRowLineAttributes(){
		rowLineAttributes = new HashMap<String,LineAttributes>();
	}
	
	public void setRowLineAttributes(LineAttributes attr){
		if(attr.getRowIndex() == -1)
			return;
		setLineColor(attr.getRowIndex(), attr.getLineColor());
		setLineWidth(attr.getRowIndex(), attr.getLineWidth());
	}

	public int getLineWidth(int row){
		String sRow = String.valueOf(row);
		if(rowLineAttributes.containsKey(sRow)){
			LineAttributes la = (LineAttributes)rowLineAttributes.get(sRow);
			return la.getLineWidth();
		}
		return 0;
	}
	
	
	public void removeLine(int row){
		String sRow = String.valueOf(row);
		if(rowLineAttributes.containsKey(sRow)){
			rowLineAttributes.remove(sRow);
		}
		return;
	}
	
	public void setLineColor(int row, Color color){
		String sRow = String.valueOf(row);
		if(rowLineAttributes.containsKey(sRow)){
			LineAttributes la = (LineAttributes)rowLineAttributes.get(sRow);
			la.setLineColor(color);
		}else{
			LineAttributes la = new LineAttributes();
			la.setLineColor(color);
			la.setRowIndex(row);
			rowLineAttributes.put(sRow, la);
		}
		return;
	}

	public void setLineWidth(int row, int width){
		String sRow = String.valueOf(row);
		if(rowLineAttributes.containsKey(sRow)){
			LineAttributes la = (LineAttributes)rowLineAttributes.get(sRow);
			la.setLineWidth(width);
		}
		return;
	}
	
	
	
	public Color getLineColor(int row){
		String sRow = String.valueOf(row);
		if(rowLineAttributes.containsKey(sRow)){
			LineAttributes la = (LineAttributes)rowLineAttributes.get(sRow);
			return la.getLineColor();
		}
		return null;
	}
	
	public void setTargetRowIndex(int row){
		targetRowIndex = row;
	}
	
	public int getTargetRowIndex(){
		return targetRowIndex;
	}
	
	public String genHorizonLineOnlyXMLStates(){
		if(rowLineAttributes.isEmpty())
			return "";
		Set<String> keys = rowLineAttributes.keySet();
		StringBuilder buf = new StringBuilder();
		for(String key: keys){
			LineAttributes la = rowLineAttributes.get(key);
			buf.append(la.saveState());
		}
		return buf.toString();
	}
/*	
	private class LineAttributes{
		private int lineWidth = 0;
		private Color lineColor = Color.RED;
		private int rowIndex = -1;
		
		public void setRowIndex(int row){
			rowIndex = row;
		}
		
		public int getRowIndex(){
			return rowIndex;
		}
		
		public void setLineWidth(int width){
			lineWidth = width;
		}
		
		public int getLineWidth(){
			return lineWidth;
		}
		
		public Color getLineColor(){
			return lineColor;
		}
		
		public void setLineColor(Color color){
			lineColor = color;
		}
	}
	*/
}
