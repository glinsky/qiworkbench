/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.marker;

import java.awt.Color;
import java.io.StringReader;

import org.w3c.dom.Node;

import com.bhpb.geographics.util.XStreamUtil;
import com.bhpb.geoio.util.ElementAttributeReader;
import com.thoughtworks.xstream.XStream;

public class LineAttributes {
	private int lineWidth = 0;
	private Color lineColor = Color.RED;
	private int rowIndex = -1;
	
	public void setRowIndex(int row){
		rowIndex = row;
	}
	
	public int getRowIndex(){
		return rowIndex;
	}
	
	public void setLineWidth(int width){
		lineWidth = width;
	}
	
	public int getLineWidth(){
		return lineWidth;
	}
	
	public Color getLineColor(){
		return lineColor;
	}
	
	public void setLineColor(Color color){
		lineColor = color;
	}
	public static String getXmlAlias() {
        return "lineAttributes";
    }

    /**
     * Deserializes a HorizonLineSettings.
     * 
     * @param node w3c.dom.node created from the XML String serialization of HorizonLineSettings
     * 
     * @return the deserialized HorizonLineSettings object
     */
    public static LineAttributes restoreState(Node node) {
        XStream xStream = XStreamUtil.getInstance();
        xStream.setClassLoader(LineAttributes.class.getClassLoader());
        xStream.alias(LineAttributes.getXmlAlias(), LineAttributes.class);
        String xml = ElementAttributeReader.nodeToXMLString(node);

        LineAttributes lineAttributes = (LineAttributes) xStream.fromXML(new StringReader(xml));

        return lineAttributes;
    }
    
    /**
     * Serializes the state of this <code>PickingSettings</code> as a {@link String}
     * representation of an XML element.
     */
    public String saveState() {
        XStream xStream = XStreamUtil.getInstance();
        //XStream xStream = new XStream(new DomDriver());
        xStream.alias(getXmlAlias(), LineAttributes.class);
        return xStream.toXML(this);
    }
}
