/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;


public class InfoPanel extends BasePane {
	private static final long serialVersionUID = 1L;
	
	JLabel qiSpace = new JLabel("");
	JCheckBox showScript; 
	JTextField projNameF, projDirF, datasetDirF, scriptDirF, tmpDirF; 
	JTextField seisDirF, invDirF, modelDirF, analyzerDirF;

	public InfoPanel(DeliveryLitePlugin delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.Y_AXIS));
		JPanel projPane  = createProjPane();
		
		infoPane.add(Box.createRigidArea(VGAP15));
		infoPane.add(projPane);
		getBasePanel().add(infoPane, BorderLayout.NORTH);
		
		populate();
	}
	
	public String validateParams() {
		String err = "";
		String tmp = invDirF.getText();
		if (tmp.length() == 0) {
			err += "Inversion Output directory on Project panel invalid\n";
		}
		tmp = modelDirF.getText();
		if (tmp.length() == 0) {
			err += "Model directory on Project panel invalid\n";
		}
		tmp = analyzerDirF.getText();
		if (tmp.length() == 0) {
			err += "Analyzer Output directory on Project panel invalid\n";
		}
		return err;
	}
	
	public String getInversionDir() {
		return invDirF.getText();
	}
	
	public String getModelDir() {
		return modelDirF.getText();
	}
	
	public String getAnalyzerDir() {
		return analyzerDirF.getText();
	}
	
	private JPanel createProjPane() {
		JPanel panelx = new JPanel();
		panelx.setLayout(new BorderLayout());
		
		JPanel projPanel = new JPanel();
        projPanel.setLayout(new BoxLayout(projPanel, BoxLayout.X_AXIS));
        TitledBorder tb = new TitledBorder("Environment Setup:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    projPanel.setBorder(tb);
        
        projPanel.add(Box.createRigidArea(HGAP30));
        
        JPanel left = new JPanel();
        left.setLayout(new BoxLayout(left, BoxLayout.X_AXIS));
        TitledBorder tbl = new TitledBorder("Project Info: Can be modified from qiProjectManager");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tbl.setTitleColor(Color.BLUE);
	    left.setBorder(tbl);
	    
        JPanel label1 = new JPanel(new GridLayout(7, 1));
        label1.add(new JLabel("QI Space:"));
        label1.add(new JLabel("Project  Name:"));
        label1.add(new JLabel("Project  Dir:"));
        label1.add(new JLabel("Seismic  Dir:"));
        label1.add(new JLabel("Datasets Dir:"));
        //label1.add(new JLabel("Scripts  Dir:"));
        label1.add(new JLabel("Temp     Dir:"));
        left.add(label1);
        left.add(Box.createRigidArea(HGAP15));
        
        JPanel field1 = new JPanel(new GridLayout(7, 1));
        projNameF   = new JTextField("", 25);
        projDirF    = new JTextField("", 25);
        seisDirF    = new JTextField("", 25);
        datasetDirF = new JTextField("", 25);
        scriptDirF  = new JTextField("", 25);
        tmpDirF     = new JTextField("", 25);
        
        projNameF.setEditable(false);
        projDirF.setEditable(false);
        seisDirF.setEditable(false);
        datasetDirF.setEditable(false);
        scriptDirF.setEditable(false);
        tmpDirF.setEditable(false);
        
        field1.add(qiSpace);
        field1.add(projNameF);
        field1.add(projDirF);
        field1.add(seisDirF);
        field1.add(datasetDirF);
        //field1.add(scriptDirF);
        field1.add(tmpDirF);
        left.add(field1);
        left.add(Box.createRigidArea(HGAP15));
        
        JPanel right = new JPanel();
        right.setLayout(new BoxLayout(right, BoxLayout.X_AXIS));
        TitledBorder tbr = new TitledBorder("Lite Specific:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tbr.setTitleColor(Color.BLUE);
	    right.setBorder(tbr);
	    
        JPanel label2 = new JPanel(new GridLayout(7, 1));
        label2.add(new JLabel(""));
        label2.add(new JLabel("Model  In / Out  Dir:"));
        label2.add(new JLabel("Inversion Output Dir:"));
        label2.add(new JLabel("Analyzer  Output Dir:"));
        showScript = new JCheckBox("Show Model & Scripts");
        showScript.addActionListener(new ShowScriptAction(delegate));
        label2.add(new JLabel(""));
        label2.add(showScript);
        label2.add(new JLabel(""));
        right.add(label2);
        right.add(Box.createRigidArea(HGAP15));
        
        JPanel field2 = new JPanel(new GridLayout(7, 1));
        
		QiProjectDescriptor projDesc = delegate.getQiProjectDescriptor();
        modelDirF  = new JTextField(QiProjectDescUtils.getTempReloc(projDesc), 15);
        invDirF    = new JTextField("prior_post", 15);
        analyzerDirF = new JTextField("analyzer", 15);
        //modelDirF.setEditable(false);
        //invDirF.setEditable(false);
        //analyzerDirF.setEditable(false);
        
        field2.add(new JLabel(""));
        field2.add(modelDirF);
        field2.add(invDirF);
        field2.add(analyzerDirF);
        field2.add(new JLabel(""));
        field2.add(new JLabel(""));
        field2.add(new JLabel(""));
        right.add(field2);
        
        projPanel.add(left);
        projPanel.add(Box.createRigidArea(HGAP15));
        //projPanel.add(right);
        
        panelx.add(projPanel, BorderLayout.WEST);
		return panelx;
	}
	
	public void resetParams() {
		QiProjectDescriptor projDesc = delegate.getQiProjectDescriptor();
		qiSpace.setText(QiProjectDescUtils.getQiSpace(projDesc));
		projNameF.setText(QiProjectDescUtils.getQiProjectName(projDesc));
		projDirF.setText(QiProjectDescUtils.getQiProjectReloc(projDesc));
		seisDirF.setText(QiProjectDescUtils.getSeismicDirs(projDesc).get(0));
		datasetDirF.setText(QiProjectDescUtils.getDatasetsReloc(projDesc));
		scriptDirF.setText(QiProjectDescUtils.getScriptsReloc(projDesc));
		tmpDirF.setText(QiProjectDescUtils.getTempReloc(projDesc));
	}
	
	public boolean showChecked() {
		return showScript.isSelected();
	}
}
