/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.util.*;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.EmptyBorder;
import javax.swing.*;

/**
  * Dialog for editing information in a leaf node (value, attributes, etc.).
  */
public class DEditDialog {

    // static variables used to identify what the DEditLeafDialog is editing:
    // values, attributes, or both (all).
    private static final int VALUE = 0;
    private JDialog dialog; // the JDialog framework for DEditLeafDialog
    private String value; // value for leaf node
    private JComboBox valueBox; // text field that holds value
    private JTextField tf;
    private boolean set; //true if dialog was closed by choosing 
    //"Set" not "Quit"
    private DEditDataType dt; // the data type for the leaf being edited
    private boolean valueIsConstraint = false; // true if value of leaf is
    // of type DEditDataType.CONSTRAINT
    // messages to be displayed concerning restrictions or info about values
    // and attribures
    //String[] constrMessages;
    //JLabel[] attrMessages;

    //private JFrame frame; // the JFrame (presumably a DEditor) the JDialog
    JInternalFrame frame; // the JFrame (presumably a DEditor) the JDialog
    // for this DEditLeafDialog will display in
    private String defValConstraint; // message about constraints on value 
    private JTextArea valConstraintText; // text area to disp. contraint info
    private Hashtable ht; // used to query important display information
    //private File suDir; // directory from which to start searches for files 
    //boolean supress;

    private List valueList;
    private static final int nHeaders = 80;

    /**
      * Constructor for DEditLeafDialog which sets up the current directory
      * and the special display properties.
      * @param curDir the File that will be the starting directory
      * @param configPairs key - value pairs to set up display properties.  
      * See README for detailed description.
      */
    public DEditDialog(String[][] configPairs)
 {

	ht = new Hashtable();
	reset();
	//suDir = curDir;
	if (configPairs == null) {
	    return;
	}
	for (int i = 0; i < configPairs.length; i++) {
	    if (configPairs[i] != null && configPairs[i].length == 2) {
		ht.put(configPairs[i][0],configPairs[i][1]);
	    }
	}
    }
    
    /**
      * Resets all fields for this class that need to be reset before
      * a new dialog can be created.
      */
    private void reset() {
	dialog = null;
	value = null; // value for leaf node
	valueBox = null; // text field that holds value
	tf = null;
	set = false; //true if dialog was closed by choosing 
	dt = null;
	valueIsConstraint = false;
	//constrMessages = null;
	//attrMessages = null;
	frame = null;
	defValConstraint = null;
	valConstraintText = null;
	//supress = false;
    }

    /**
      * Makes a JDialog with given parent JFrame, title, and initial value.
      * @param frame the parent JFrame for this DEditLeafDialog
      * @param title the String to go in the title of this dialog
      * @param dt the DEditDataType for the node this dialog will edit
      * @param initialValue the initial value (as a string) of the value 
      * in the leaf being edited
      * @return the new value if "Accept" is chosen, null otherwise
      */
    public String showValueDialog(JInternalFrame iframe, String title, 
				  DEditDataType dt,String initialValue) {
	reset();
	this.dt = dt;
	if (this.dt == null) {
	    //this.dt = new DEditDataType(DEditDataType.EPSILON,
	    //			DEditDataType.EMPTY,"",null);
	    return null;
	}
	this.value = initialValue;
	this.frame = iframe;
	JPanel choosePanel = setUpChoosePanel(VALUE);
	JPanel valPanel = setUpValPanel(VALUE);
	JPanel jp = setUpMainPanel();
	jp.add(choosePanel,BorderLayout.SOUTH);
	jp.add(valPanel,BorderLayout.CENTER);
	dialog = new JDialog(JOptionPane.getFrameForComponent(frame), title, true);
	dialog.getContentPane().add(jp);
	dialog.pack();
	dialog.setLocationRelativeTo(frame);
	boolean setVal = showDialog();
	if (setVal) {
	    return value;
	} else {
	    return null;
	}
    }
    

    

    /**
      * Creates and returns a JPanel with border to be used as the main 
      * panel in a DEditLeafDialog.
      * @return a JPanel to be used as the main panel in a DEditLeafDialog
      */
    private JPanel setUpMainPanel() {
	JPanel ret = new JPanel(new BorderLayout());
	ret.setBorder(new EmptyBorder(5,5,5,5));
	return ret;
    }


    /**
      * Creates, sets up, and returns a JPanel which will be used to
      * view and edit the value of the DEditNode being edited by this 
      * DEditLeafDialog.
      * @param whoToSet specifies which components to if the accept-type
      * action is launced (in this case via hitting enter in the JTextField)
      * @return a JPanel which will be used to edit a node's value
      */
    private JPanel setUpValPanel(final int whoToSet) {
        valueList = new ArrayList(nHeaders);
	tf = new JTextField(value);
	valueBox = new JComboBox();
	int type = dt.getType();
	JPanel textPanel;
	valueIsConstraint = (type == DEditDataType.CONSTRAINT);
	if (valueIsConstraint) {
	    textPanel = new JPanel(new BorderLayout());
	    defValConstraint = dt.getConstraintMessage();
	    valConstraintText = new JTextArea(defValConstraint,2,20);
	    valConstraintText.setEditable(false);
	    //valConstraintText.setLineWrap(true);
	    valConstraintText.setBackground(textPanel.getBackground());
	    textPanel.add(new JLabel("Value"),BorderLayout.NORTH);
	    textPanel.add(valConstraintText,BorderLayout.CENTER);
	    textPanel.add(tf,BorderLayout.SOUTH);
	    tf.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ae) {
			boolean dispose = false;
			switch(whoToSet) {
			case VALUE:dispose = setValue();break;
			default:dispose = false;
			}
			if (dispose) {
			    dialog.dispose();
			}
		    }
		});
	} else {
	    valueBox.setEditable(false);
	    for(int i = 0; i < dt.numValues(); i++)
              valueList.add(dt.getValueAt(i));
            Collections.sort(valueList);
	    for (int i = 0; i < dt.numValues(); i++) {
		// valueBox.addItem(dt.getValueAt(i));
                valueBox.addItem(valueList.get(i));
	    }
	    valueBox.setSelectedItem(value);
	    textPanel = new JPanel(new BorderLayout());
	    textPanel.add(new JLabel("Value"),BorderLayout.NORTH);
	    textPanel.add(valueBox,BorderLayout.SOUTH);
	}
	return textPanel;
    }


    /**
      * Creates and returns a JPanel containing buttons for the ultimate user 
      * choices: accept and cancel
      * @param whoToSet if accept is chosen, this determines what to set 
      * (attributes, value, or both)
      * @return a JPanel used to dismiss the DEditLeafDialog either by 
      * accepting or canceling the changes made
      */
    private JPanel setUpChoosePanel(final int whoToSet) {
	JPanel buttonPanel = new JPanel(new GridLayout(1,2));
	JButton setButton = new JButton("Accept");
	setButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    boolean dispose = false;
		    switch(whoToSet) {
		    case VALUE:dispose = setValue();break;
		    default:dispose = false;
		    }
		    if (dispose) {
			dialog.dispose();
		    }
		}
	    });
	buttonPanel.add(setButton);
	JButton cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    cancel();
		}
	    });
	buttonPanel.add(cancelButton);
	return buttonPanel;
    }

    /**
      * Helper which launches a generic choose file dialog and sets the 
      * field tf (a JTextField) to have the filename as its text
      */
    /*private void chooseFile() {
	JFileChooser chooser = new JFileChooser(suDir);
	chooser.setDialogTitle("Open File");
	chooser.setAcceptAllFileFilterUsed(true);
	int returnVal = chooser.showOpenDialog(dialog);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File f = chooser.getSelectedFile();
	    suDir = f.getParentFile();
	    tf.setText(f.getAbsolutePath());
	}
    }
    */

    /**
      * Sets the internal value to be used for the node's value with the 
      * value currently shown in the JTextField or JComboBox (as appropriate).
      * @return true if setting it to the new value is valid, false otherwise
      */
    private boolean setValue() {
	set = true;
	if (valueIsConstraint) {
	    if (tf.isEditable()) {
		value = tf.getText();
	    }
	} else {
	    value = (String)valueBox.getSelectedItem();
	}
	if (!dt.isValid(value)) {
	    JOptionPane.showMessageDialog(dialog,"Value is invalid.");
	    return false;
	}
	return true;
    }
    

    /**
      * Launched when the cancel button is hit.  Shouldn't really have to 
      * set set == false.
      */
    private void cancel() {
	set = false;
	dialog.dispose();
    }
    
    /**
      * Shows the dialog and returns the value of set which in turn will 
      * determine whether or not edited values should be set in the xml tree
      * or discarded.
      * @return true if the accept action was chosen
      */
    private boolean showDialog() {
	dialog.show();
	return set;
    }

}
