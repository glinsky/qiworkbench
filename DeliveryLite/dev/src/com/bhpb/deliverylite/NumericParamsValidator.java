/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;
/**
 * Validate non table cell numeric text field when the field is out of focus or when user hit enter key
 * after editing.
 * If the invalid value found, restore it back to the most recent value.
 * @author 
 */

import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import org.apache.commons.validator.routines.FloatValidator;

public class NumericParamsValidator {
	private NumericFieldParams modelParams;
	private boolean focusEnabled = true;
	private DeliveryLiteUI parentGui;
	
	public enum MODEL_PARAMS {TIME_BASE,SIGMA_TIMEBASE,MASTER_DEPTH_LAYER,
    	DEPTH_MASTER_LAYER,ERROR_MASTER_LAYER_DEPTH,
    	SINGLE_INLINE,SINGLE_XLINE,
    	NEAR_NOISE_RMS,FAR_NOISE_RMS,
    	AVO_A,AVO_B,
    	NUMBER_RUNS,
    	SYNTHETIC_SEISMIC_START_TIME,SYNTHETIC_SEISMIC_END_TIME,
    	SPAGHETTI_PLOT_START_TIME,SPAGHETTI_PLOT_END_TIME,
    };
    
	public NumericParamsValidator(DeliveryLiteUI parentGui, NumericFieldParams params){
		modelParams = params;
		this.parentGui = parentGui;
	}    	

	public void addListeners(final javax.swing.JTextField field, final MODEL_PARAMS type){
    	field.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	focusEnabled = false;
            	validate(field,type);
            	focusEnabled = true;
            }
        });
    	field.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
            	if(focusEnabled){
            		validate(field,type);
            	}
            }
        });
    }
	
	private void validate(javax.swing.JTextField field,MODEL_PARAMS type){
		String text = field.getText();
		if(!FloatValidator.getInstance().isValid(text)){
			parentGui.showValidationFailure("Invalid numeric value.");
			switch(type){
				case TIME_BASE:
					field.setText(String.valueOf(modelParams.getTimeBase()));
					break;
				case SIGMA_TIMEBASE:
					field.setText(String.valueOf(modelParams.getSigmaTimeBase()));
					break;
				case MASTER_DEPTH_LAYER:
					field.setText(String.valueOf(modelParams.getMasterDepthLayer()));
					break;
				case DEPTH_MASTER_LAYER:
					field.setText(String.valueOf(modelParams.getDepthMasterLayer()));
					break;
				case ERROR_MASTER_LAYER_DEPTH:
					field.setText(String.valueOf(modelParams.getErrorMasterLayerDepth()));
					break;
				case SINGLE_INLINE:
					field.setText(String.valueOf(modelParams.getSingleInline()));
				    break;
				case SINGLE_XLINE:
					field.setText(String.valueOf(modelParams.getSingleXline()));
				    break;
				case NEAR_NOISE_RMS:
					field.setText(String.valueOf(modelParams.getNearNoiseRMS()));
				    break;
				case FAR_NOISE_RMS:
					field.setText(String.valueOf(modelParams.getFarNoiseRMS()));
				    break;
				case AVO_A:
					field.setText(String.valueOf(modelParams.getAvoA()));
				    break;
				case AVO_B:
					field.setText(String.valueOf(modelParams.getAvoB()));
				    break;
				case NUMBER_RUNS:
					field.setText(String.valueOf(modelParams.getNumberRuns()));
				    break;
				case SYNTHETIC_SEISMIC_START_TIME:
					field.setText(String.valueOf(modelParams.getSyntheticSeismicStartTime()));
				    break;
				case SYNTHETIC_SEISMIC_END_TIME:
					field.setText(String.valueOf(modelParams.getSyntheticSeismicEndTime()));
				    break;
				case SPAGHETTI_PLOT_START_TIME:
					field.setText(String.valueOf(modelParams.getSpaghettiPlotStartTime()));
				    break;
				case SPAGHETTI_PLOT_END_TIME:
					field.setText(String.valueOf(modelParams.getSpaghettiPlotEndTime()));
				    break;
				default:
			}
			
			return;
		}else{
			float val = Float.valueOf(text).floatValue();
			switch(type){
				case TIME_BASE:
					modelParams.setTimeBase(val);
					break;
				case SIGMA_TIMEBASE:
					modelParams.setSigmaTimeBase(val);
					break;
				case MASTER_DEPTH_LAYER:
					modelParams.setMasterDepthLayer((int)val);
					break;
				case DEPTH_MASTER_LAYER:
					modelParams.setDepthMasterLayer(val);
					break;
				case ERROR_MASTER_LAYER_DEPTH:
					modelParams.setErrorMasterLayerDepth(val);
					break;
				case SINGLE_INLINE:
					modelParams.setSingleInline(val);
				    break;
				case SINGLE_XLINE:
					modelParams.setSingleXline(val);
				    break;
				case NEAR_NOISE_RMS:
					modelParams.setNearNoiseRMS(val);
				    break;
				case FAR_NOISE_RMS:
					modelParams.setFarNoiseRMS(val);
				    break;
				case AVO_A:
					modelParams.setAvoA(val);
				    break;
				case AVO_B:
					modelParams.setAvoB(val);
				    break;
				case NUMBER_RUNS:
					modelParams.setNumberRuns((int)val);
				    break;
				case SYNTHETIC_SEISMIC_START_TIME:
					modelParams.setSyntheticSeismicStartTime(val);
				    break;
				case SYNTHETIC_SEISMIC_END_TIME:
					modelParams.setSyntheticSeismicEndTime(val);
				    break;
				case SPAGHETTI_PLOT_START_TIME:
					modelParams.setSpaghettiPlotStartTime(val);
				    break;
				case SPAGHETTI_PLOT_END_TIME:
					modelParams.setSpaghettiPlotEndTime(val);
				    break;

				default:
			}
		}
	}
}
