/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.util.List;
/**
 * A generic Workbench base module
 *
 * @version 1.0
 */
public class BasePane extends JApplet {
	private static final long serialVersionUID = 1L;
	
	Border loweredBorder = new CompoundBorder(new SoftBevelBorder(
			SoftBevelBorder.LOWERED), new EmptyBorder(5, 5, 5, 5));

	// Premade convenience dimensions, for use wherever you need 'em.
	public static Dimension HGAP2 = new Dimension(2, 1);

	public static Dimension VGAP2 = new Dimension(1, 2);

	public static Dimension HGAP5 = new Dimension(5, 1);

	public static Dimension VGAP5 = new Dimension(1, 5);

	public static Dimension HGAP10 = new Dimension(10, 1);

	public static Dimension VGAP10 = new Dimension(1, 10);

	public static Dimension HGAP15 = new Dimension(15, 1);

	public static Dimension VGAP15 = new Dimension(1, 15);

	public static Dimension HGAP20 = new Dimension(20, 1);

	public static Dimension VGAP20 = new Dimension(1, 20);

	public static Dimension HGAP25 = new Dimension(25, 1);

	public static Dimension VGAP25 = new Dimension(1, 25);

	public static Dimension HGAP30 = new Dimension(30, 1);

	public static Dimension VGAP30 = new Dimension(1, 30);
	
	public static Dimension HGAP50 = new Dimension(50, 1);

	public static Dimension VGAP50 = new Dimension(1, 50);

	protected DeliveryLitePlugin delegate = null;

	private JPanel panel = null;

	private String resourceName = null;

	public BasePane(DeliveryLitePlugin delegate) {
		this(delegate, null, null);
	}

	public BasePane(DeliveryLitePlugin delegate, String resourceName, String iconPath) {
		UIManager.put("swing.boldMetal", Boolean.FALSE);
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		this.resourceName = resourceName;
		this.delegate = delegate;
	}

	public void initPanel() {}
	public int  err(String type) {return 0;}
	public void setModelName(String action) {}
    public String getNameEx(String action) {return "";}
    public void doStop() {}
    public void doSwitch() {}
    public void listSelected(int[] sels) {}
    public void tableRowSelected() {}
    public void tableRowsSelected() {}
    public void goModalDialog(String action) {}
    
    public void populate() {}
    public void manageObject(String action) {}
    public void manageObject(String action, int index) {}
    public void manageObject(List<String>paramList) {}
   
	public String getResourceName() {
		return resourceName;
	}

	public JPanel getBasePanel() {
		return panel;
	}

	public DeliveryLitePlugin getDelegate() {
		return delegate;
	}

	public void init() {
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(getBasePanel(), BorderLayout.CENTER);
	}

	void updateDragEnabled(boolean dragEnabled) {
	}
	
	protected JLabel createIconLabel(String tip, String imageFile) {
		ImageIcon icon = new ImageIcon(BasePane.class.getResource(imageFile));
		JLabel iLabel = new JLabel("", icon, SwingConstants.LEFT);
		iLabel.setToolTipText(tip);
		return iLabel;
	}

}
