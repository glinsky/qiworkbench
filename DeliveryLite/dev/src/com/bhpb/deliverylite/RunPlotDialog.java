/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.layout.GroupLayout.ParallelGroup;
import org.jdesktop.layout.GroupLayout.SequentialGroup;

import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.JobMonitor;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.util.Util;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

/*
 * RunPlotDialog.java
 *
 * RunPlotDialog will dynamically show number of property set panel (property combobox and layer number combobox) 
 * depending on the input parameters
 * Created on June 12, 2008, 11:36 AM
 */

/**
 *
 * @author  
 */
public class RunPlotDialog extends javax.swing.JDialog implements MetadataSettable {
	private static Logger logger = Logger.getLogger(RunPlotDialog.class.getName());
    private List<String> properties;
    private int numLayers = 0;
    private int jobType = -1;
    private int numPropertiesSet = 0;
    private String stackType = "near";
    private DeliveryLiteUI parentGui;
    private List<JLabel> listOfPropertyLabels = new ArrayList<JLabel>();
    private List<JLabel> listOfLayerLabels = new ArrayList<JLabel>();
    private List<JComboBox> listOfPropertyComboBoxs = new ArrayList<JComboBox>();
    private List<JComboBox> listOfLayerComboBoxs = new ArrayList<JComboBox>();
    private List<JPanel> listOfPropertyPanel = new ArrayList<JPanel>();
    private JobMonitor monitor;
    private JobManager manager;
    private DefaultTableModel model = new DefaultTableModel(){
		public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }
	};
    /** Creates new form RunPlotDialog */
    /**
     * @param parentGui the UI that initiates this dialog
     * @param modal      if this dialog should be modal
     * @param properties      a list of property strings
     * @param numLayers      number of layers
     * @param numPropertiesSet  number of property set
     * @param stackType     either "near" or "near_far"
     * @param jobType     either DeliveryLiteConstants.MAKE_SCATTER_PLOT,DeliveryLiteConstants.MAKE_SPAGHETTI_PLOT,
     * 					DeliveryLiteConstants.MAKE_FULL_ASCII_OUTPUT, or DeliveryLiteConstants.MAKE_HISTORGRAM_PLOT
     *   View Text Output should be enabled (this is only enabled when the dialog show Make Full ASCII output
     */
    public RunPlotDialog(DeliveryLiteUI parentGui, boolean modal, String title, List<String> properties,int numLayers, 
    		int numPropertiesSet, String stackType, int jobType) {
        super(JOptionPane.getFrameForComponent(parentGui), modal);
        this.setTitle(title);
        this.parentGui = parentGui;
        this.properties = properties;
        this.stackType = stackType;
        this.numLayers = numLayers;
        this.numPropertiesSet = numPropertiesSet;
        this.jobType = jobType;
        initComponents();
        /*final DeliveryLiteUI pGui = parentGui;
        final RunPlotDialog comp = this;
        if(properties == null || properties.size() == 0){
        	Runnable heavyRunnable = new Runnable(){
    			public void run(){
    				pGui.getAgent().getDeliveryProperties(comp);
    			}
        	};
        	new Thread(heavyRunnable).start();
    	}*/
        this.setLocationRelativeTo(parentGui);
    }
    
    
    public RunPlotDialog(DeliveryLiteUI parentGui, boolean modal, String title, String stackType, int jobType) {
        this(parentGui,modal,title, null,0,0,stackType,jobType);
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">
    public void setStackType(String type){
    	stackType = type;
    }
    
    private void initComponents() {
        runParamPanel = new javax.swing.JPanel();
        runPanel = new javax.swing.JPanel();
        runTypeLabel = new javax.swing.JLabel();
        runTypeComboBox = new javax.swing.JComboBox();
        runButton = new javax.swing.JButton();
        runButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		runButtonActionPerformed(e);
        	}
        });
        viewTextButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        closeButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		closeButtonActionPerformed(e);
        	}
        });
        property1Panel = new javax.swing.JPanel();
        statusPanel = new javax.swing.JPanel();
        fileSummaryScrollPane = new javax.swing.JScrollPane();
        fileSummaryTextArea = new javax.swing.JTextArea();
        statusScrollPane = new javax.swing.JScrollPane();
        statusTextArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        runParamPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Run Parameters"));
        runTypeLabel.setText("Run Type:");
        runTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "prior", "post" }));
        
        runButton.setText("Run");
        //runButton.setEnabled(false);
        viewTextButton.setText("View Text Ouput");
        viewTextButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		viewTextActionPerformed();
        	}
        });
        
        if(jobType != DeliveryLiteConstants.MAKE_FULL_ASCII_OUTPUT){
        	if(jobType == DeliveryLiteConstants.MAKE_SPAGHETTI_PLOT)
        		viewTextButton.setEnabled(false);
        	viewTextButton.setText("Get Properties");
        	final MetadataSettable comp = this;
        }
        closeButton.setText("Close");
        runPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        runPanel.add(runTypeLabel);
        runPanel.add(runTypeComboBox);
        if(jobType == DeliveryLiteConstants.MAKE_SPAGHETTI_PLOT){
        	//JLabel label = new JLabel("Stack:");
        	//stackTypeComboBox = new JComboBox(new String[]{"near", "near_far"});
        	//runPanel.add(label);
            //runPanel.add(stackTypeComboBox);
        }
        
        runPanel.add(runButton);
        if(jobType == DeliveryLiteConstants.MAKE_FULL_ASCII_OUTPUT){
        	runPanel.add(viewTextButton);
        }
        runPanel.add(closeButton);
        
        Vector<String> v1 = new Vector<String>();
        Vector<String> v2 = new Vector<String>();
        if(numPropertiesSet > 0){
        	for(String s : properties){
        		v1.add(s);
        	}
        }
        
        for(int i =0; i < numLayers; i++){
        	v2.add(String.valueOf(i+1));
        }
        
        for(int i = 0; i < numPropertiesSet; i++){
        	JLabel propertyLabel = new JLabel("Property " + (i+1));
        	listOfPropertyLabels.add(propertyLabel);
        	JLabel layerLabel = new JLabel("Layer " + (i+1));
        	listOfLayerLabels.add(layerLabel);
        	JComboBox propertyBox = new JComboBox(v1);
        	listOfPropertyComboBoxs.add(propertyBox);
        	JComboBox layerBox = new JComboBox(v2);
        	listOfLayerComboBoxs.add(layerBox);
        	listOfPropertyPanel.add(new JPanel());
        }
        
        for(int i = 0; i < listOfPropertyLabels.size(); i++){
        	org.jdesktop.layout.GroupLayout property1PanelLayout = new org.jdesktop.layout.GroupLayout(listOfPropertyPanel.get(i));
        	listOfPropertyPanel.get(i).setLayout(property1PanelLayout);
	        property1PanelLayout.setHorizontalGroup(
	            property1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
	            .add(property1PanelLayout.createSequentialGroup()
	                .addContainerGap()
	                .add(listOfPropertyLabels.get(i))
	                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
	                .add(listOfPropertyComboBoxs.get(i), org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
	                .add(21, 21, 21)
	                .add(listOfLayerLabels.get(i))
	                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
	                .add(listOfLayerComboBoxs.get(i), org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(236, Short.MAX_VALUE))
	        );
	        property1PanelLayout.setVerticalGroup(
	            property1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
	            .add(property1PanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
	            	.add(listOfPropertyLabels.get(i))
	            	.add(listOfPropertyComboBoxs.get(i), org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
	            	.add(listOfLayerLabels.get(i))
	            	.add(listOfLayerComboBoxs.get(i), org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
	        );
        }
        
        
        
        org.jdesktop.layout.GroupLayout runParamPanelLayout = new org.jdesktop.layout.GroupLayout(runParamPanel);
        ParallelGroup pGroup = runParamPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING);
        pGroup = pGroup.add(runPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        SequentialGroup sGroup = runParamPanelLayout.createSequentialGroup();
        sGroup = sGroup.add(runPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE);
        for(int i = 0; i < numPropertiesSet; i++){
        	pGroup = pGroup.add(listOfPropertyPanel.get(i), org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE);
        	sGroup = sGroup.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED);
            sGroup = sGroup.add(listOfPropertyPanel.get(i), org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE);
    	}
        runParamPanel.setLayout(runParamPanelLayout);
        runParamPanelLayout.setHorizontalGroup(
       		pGroup
        );
        runParamPanelLayout.setVerticalGroup(
            runParamPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(sGroup)
        );

        
        statusPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Job Status Details"));
        fileSummaryTextArea.setColumns(20);
        fileSummaryTextArea.setEditable(false);
        fileSummaryTextArea.setRows(5);
        fileSummaryScrollPane.setViewportView(fileSummaryTextArea);

        statusTextArea.setColumns(20);
        statusTextArea.setEditable(false);
        statusTextArea.setRows(5);
        statusScrollPane.setViewportView(statusTextArea);

        org.jdesktop.layout.GroupLayout statusPanelLayout = new org.jdesktop.layout.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fileSummaryScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
            .add(statusScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, statusPanelLayout.createSequentialGroup()
                .add(statusScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fileSummaryScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(runParamPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(statusPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(runParamPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(statusPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
    }// </editor-fold>                        
    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	this.setVisible(false);
    	this.dispose();
    }
    
    
    private void viewTextActionPerformed(){
    	final javax.swing.JDialog dialog = new javax.swing.JDialog(this,"View Full ASCII Output",false);
    	JPanel control = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    	JScrollPane scrollPane = new javax.swing.JScrollPane();
    	javax.swing.JButton close = new javax.swing.JButton("Close");
    	close.addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e){
    			dialog.setVisible(false);
    			dialog.dispose();
    		}
    	});
    	control.add(close);
    	JPanel main = new JPanel(new BorderLayout());
    	final javax.swing.JTable table = new javax.swing.JTable();
    	JTableHeader header = table.getTableHeader();
        header.setReorderingAllowed(false);
        final Font boldFont = header.getFont().deriveFont(Font.BOLD);
        final TableCellRenderer headerRenderer = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    	scrollPane.setViewportView(table);
    	main.add(scrollPane,BorderLayout.CENTER);
    	dialog.add(main,BorderLayout.CENTER);
    	dialog.add(control,BorderLayout.SOUTH);
    	dialog.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    	dialog.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
            	dialog.setVisible(false);
            	dialog.dispose();
            }
        });
    	dialog.setVisible(true);
    	dialog.setSize(600, 600);
    	
    	Runnable heavyRunnable = new Runnable(){
			public void run(){
				String title = "Gathering Full ASCII Output from deliveryAnalyser";
				String text = "Please wait while system is gathering the full ASCII output...";


				Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);
				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(dialog,100,true,500,title,text,icon);
				//monitor.start("");

				ProgressUtil.start(monitor,"");
				long offset = 0;
				long length = 1024000;
				StringBuffer buf = new StringBuffer();
				String str = getOutput(offset, length);
				if(str == null || str.length() == 0)
					return;
				buf.append(str);
				offset = str.length();
				while(str.length() != 0){
					str = getOutput(offset, length);
					buf.append(str);
					offset += str.length();
					try{
						Thread.sleep(1000);
					}catch(InterruptedException e){}
				}
				String content = buf.toString();
				int ind = content.indexOf('\n');
				
				if(ind != -1){
					String temp = content.substring(0, ind);
					String[] colNames = temp.split(" +");
					for(String s : colNames)
						model.addColumn(s);
					String temp1 = content.substring(ind+1);
					String[] temp2 = temp1.split("\n");
					for(String s : temp2){
						String[] cols = s.trim().split(" +");
						model.addRow(cols);
					}
				}
				table.setModel(model);
				
				if(ProgressUtil.isCancel(monitor)){
					return;
				}
								
				ProgressUtil.setCurrent(monitor,null,ProgressUtil.getTotal(monitor));
				
			}
    	};
    	new Thread(heavyRunnable).start();
    	
    }
    
    private String getOutput(long offset, long length){
    	byte[] bytes = null;
    	MessagingManager messagingMgr = parentGui.getAgent().getMessagingMgr();
        List params1 = new ArrayList();
        params1.add(messagingMgr.getLocationPref());
        String filePath = QiProjectDescUtils.getTempPath(parentGui.getAgent().getQiProjectDescriptor());
        filePath +=  messagingMgr.getServerOSFileSeparator();
        filePath += parentGui.getOutputBasename() + "_";
        filePath += (String)runTypeComboBox.getSelectedItem() + "_";
        filePath += stackType + "_full_ascii.txt";
        params1.add(filePath);  //the segy file path
        params1.add(Long.valueOf(offset));   //starting from the first byte
        params1.add(Long.valueOf(length)); //until position 3600
        String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
                QIWConstants.BINARY_FILE_READ_CMD,
                QIWConstants.ARRAYLIST_TYPE,params1,true);
        IQiWorkbenchMsg res = messagingMgr.getMatchingResponseWait(msgId,10000);
        if(res == null){
            logger.info("Null Response returned due to timed out.");
            return null;
        }else if(res.isAbnormalStatus()){
            String message = "Abnormal response returned due to " + res.getContent();
            logger.info(message);
            return null;
        }else{
            bytes = (byte[])res.getContent();
            if(bytes != null){
            	return new String(bytes);
            }
        }
        return null;
    }
    
    
    private void runButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	if(!validateInputs())
    		return;
    	String type = (String)runTypeComboBox.getSelectedItem();
    	String scriptNamePrefix = "";
    	if(jobType == DeliveryLiteConstants.MAKE_HISTORGRAM_PLOT){
    		scriptNamePrefix = writeScript("/xml/histogram_template.txt", "histogram", jobType, type, stackType);
    	}else if(jobType == DeliveryLiteConstants.MAKE_SCATTER_PLOT){
    		scriptNamePrefix = writeScript("/xml/scatter_template.txt", "scatter", jobType, type, stackType);
    	}else if(jobType == DeliveryLiteConstants.MAKE_SPAGHETTI_PLOT){
    		scriptNamePrefix = writeScript("/xml/spaghetti_template.txt", "spaghetti", jobType, type, stackType);
    	}else if(jobType == DeliveryLiteConstants.MAKE_FULL_ASCII_OUTPUT){
    		scriptNamePrefix = writeScript("/xml/full_asscii_template.txt", "full_ascii", jobType, type, stackType);
    	}
    	
//    	Get the most recent project descriptor
        QiProjectDescriptor qiProjectDesc = parentGui.getAgent().getQiProjectDescriptor();
        String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);
        String filesep = parentGui.getAgent().getMessagingMgr().getServerOSFileSeparator();
        String filePrefix = scriptDir + filesep + scriptNamePrefix;
        // Create a job monitor for the script.
        monitor = new JobMonitor(filePrefix, scriptDir, scriptNamePrefix);
        // Create a job manager for the script.
        manager = new JobManager(parentGui.getAgent().getMessagingMgr(), this);
    	if(scriptNamePrefix != null && scriptNamePrefix.length() > 0)
    		Util.runScript(scriptNamePrefix, monitor, manager, runButton, statusTextArea, fileSummaryTextArea);
    }
    
    public void setStdOutErrorMessage(String errorContent){
    	statusTextArea.setText(errorContent);
    }
    
    
    public void setProperties(String[] properties){
    	parentGui.setDeliveryAnalyserProperties(properties);
    	fillProperties(properties);
    	fillLayers();
    }
    
    private void fillLayers(){
    	Vector<String> v = new Vector<String>();
    	for(int i = 0; i < parentGui.getNumberOfInitialLayers(); i++){
    		v.add(String.valueOf(i+1));
    	}
    	for(JComboBox cb :listOfLayerComboBoxs){
    		cb.setModel(new DefaultComboBoxModel(v));
    	}
    }
    
    private void fillProperties(String[] properties){
    	Vector<String> v = new Vector<String>();
    	for(String s : properties){
    		v.add(s);
    	}
    	for(JComboBox cb :listOfPropertyComboBoxs){
    		cb.setModel(new DefaultComboBoxModel(v));
    	}
    }
    private boolean validateInputs(){
    	for(JComboBox cb : listOfPropertyComboBoxs){
    		if(cb.getSelectedIndex() == -1){
    			JOptionPane.showMessageDialog(this, "Missing Property.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
    			return false;
    		}
    	}
    	
    	for(JComboBox cb : listOfLayerComboBoxs){
    		if(cb.getSelectedIndex() == -1){
    			JOptionPane.showMessageDialog(this, "Missing Layer.", "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
    			return false;
    		}
    	}
    	return true;
    }
    
    /**
     * Generate and write out the Delivery script to the project's script directory (temp).
     * @return Pathname prefix of the script (i.e., path without .sh) if file successully
     * written; otherwise, null.
     */
    private String writeScript(String templateName, String baseName, int jobType, String type, String stackType) {
        String template = Util.getTemplate(templateName);
        String scripts  = formScripts(template,jobType, type,stackType);

        String scriptDir = QiProjectDescUtils.getTempPath(parentGui.getAgent().getQiProjectDescriptor());

        String fileNamePrefix = Util.createTimeStampedScriptPrefix(baseName + "_" + type + "_" + stackType);
        String fileName = fileNamePrefix + ".sh";
        String filesep = parentGui.getAgent().getMessagingMgr().getServerOSFileSeparator();
        boolean success = parentGui.getAgent().writeStringToFile(scriptDir + filesep + fileName, scripts);
        if (success)
            //return scriptDir + File.separator+ fileNamePrefix;
        	return fileNamePrefix;
        else
            return null;

    }
    /**
     * Generate DeliveryLite make plot script from a template.
     * @param templ MakePlot template script
     * @return Generated script
     */
    private String formScripts(String templ, int jobType, String runType, String stackType) {
        //NOTE: It is assumed BHP_SU and SU are on the end-users PATH
        QiProjectDescriptor projDesc = parentGui.getAgent().getQiProjectDescriptor();

        String template = templ;
        String qiSpace = QiProjectDescUtils.getQiSpace(projDesc);
        String qiProject = QiProjectDescUtils.getQiProjectReloc(projDesc);
        String qiDatasets = QiProjectDescUtils.getDatasetsReloc(projDesc);
        String qiSeismicDir = QiProjectDescUtils.getSeismicDirs(projDesc).get(0);
        String qiTmpDir = QiProjectDescUtils.getTempReloc(projDesc);
        
        template = template.replace("<QI_SPACE>",        qiSpace);
        template = template.replace("<QI_PROJECT_LOCATION>",    qiProject);
        template = template.replace("<QI_DATASETS>",   qiDatasets);
        template = template.replace("<QI_SEISMICS>",    qiSeismicDir);
        template = template.replace("<QI_TMP>",    qiTmpDir);

 
        template = template.replace("<EP>",         parentGui.getEp());
        template = template.replace("<CDP>",        parentGui.getCdp());
        template = template.replace("<RUN_TYPE>",       runType);
        template = template.replace("<STACK_TYPE>",       stackType);
        template = template.replace("<OUTPUT_BASENAME>",       parentGui.getOutputBasename());
        if(jobType == DeliveryLiteConstants.MAKE_HISTORGRAM_PLOT){
        	template = template.replace("<PROPERTY_1>",(String)listOfPropertyComboBoxs.get(0).getSelectedItem());
        	template = template.replace("<LAYER_1>",(String)listOfLayerComboBoxs.get(0).getSelectedItem());
        }else if(jobType == DeliveryLiteConstants.MAKE_SCATTER_PLOT){
        	template = template.replace("<PROPERTY_1>",(String)listOfPropertyComboBoxs.get(0).getSelectedItem());
        	template = template.replace("<LAYER_1>",(String)listOfLayerComboBoxs.get(0).getSelectedItem());
        	template = template.replace("<PROPERTY_2>",(String)listOfPropertyComboBoxs.get(1).getSelectedItem());
        	template = template.replace("<LAYER_2>",(String)listOfLayerComboBoxs.get(1).getSelectedItem());
        }else if(jobType == DeliveryLiteConstants.MAKE_SPAGHETTI_PLOT){
        	template = template.replace("<STACK_NAME>",     parentGui.getNearStackFileName());
        	template = template.replace("<MIN_TIME>",     parentGui.getSpaghettiMinTime());
            template = template.replace("<MAX_TIME>",     parentGui.getSpaghettiMaxTime());
        }else if(jobType == DeliveryLiteConstants.MAKE_FULL_ASCII_OUTPUT){
        	template = template.replace("<MIN_TIME>",     parentGui.getSpaghettiMinTime());
            template = template.replace("<MAX_TIME>",     parentGui.getSpaghettiMaxTime());
        }
        return template;
    }

    
    private void fillLayerProperty(ModelFileMetadata data){
    	if(data == null)
    		return;
    	Vector v1 = new Vector();
    	Vector v2 = new Vector();
    	for(int i = 0; data.getProperties() != null && i < data.getProperties().size(); i++)
    		v1.add(data.getProperties().get(i));
    	for(int i = 0; i < data.getNumLayers(); i++)
    		v2.add(String.valueOf(i+1));
    	for(int i = 0; i < listOfLayerComboBoxs.size(); i++){
    		//listOfPropertyComboBoxs.get(i).setModel(new DefaultComboBoxModel(v1));
    		listOfLayerComboBoxs.get(i).setModel(new DefaultComboBoxModel(v2));
    	}
    }
    
    //private Map<String,ModelFileMetadata> metadataMap = new HashMap<String,ModelFileMetadata>();
    public void setMetaData(String dataset, GeoFileMetadata metadata, int stackMode, int runMode){
    	ModelFileMetadata data =  (ModelFileMetadata)metadata;
    	//metadataMap.put(dataset, data);
    	parentGui.setMetaData(dataset, metadata);
    	fillLayerProperty(data);
    }
    
    
    private String getRealizationBaseName(){
    	
        int index = runTypeComboBox.getSelectedIndex();

        String dataset = parentGui.getOutputBasename();
        
        if(index == 0) //prior
        	dataset += "_prior";
        else if(index == 1) //post
        	dataset += "_post";
        
        if(!parentGui.isNearAndFar()) //near only
        	dataset += "_near";
        dataset += "_realizations";	
        return dataset;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	List<String> properties = new ArrayList();
            	properties.add("time");
            	properties.add("net-sand");
                //new RunPlotDialog(null, true, "", false).setVisible(true);
                new RunPlotDialog(null, true, "", properties, 4, 1,"near",0).setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify                     
    private javax.swing.JButton closeButton;
    private javax.swing.JScrollPane fileSummaryScrollPane;
    private javax.swing.JTextArea fileSummaryTextArea;
    private javax.swing.JPanel property1Panel;
    private javax.swing.JPanel property1Panel2;
    private javax.swing.JButton runButton;
    private javax.swing.JPanel runPanel;
    private javax.swing.JPanel runParamPanel;
    private javax.swing.JComboBox runTypeComboBox;
    private javax.swing.JComboBox stackTypeComboBox;
    private javax.swing.JLabel runTypeLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JScrollPane statusScrollPane;
    private javax.swing.JTextArea statusTextArea;
    private javax.swing.JButton viewTextButton;
    // End of variables declaration                   
    
}