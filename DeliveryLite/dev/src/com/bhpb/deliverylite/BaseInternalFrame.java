/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;


import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class BaseInternalFrame extends javax.swing.JInternalFrame{
	private List<PropertyChangeListener> pcListeners = new ArrayList<PropertyChangeListener>();
	private int height;
	public BaseInternalFrame(ViewModelDialog controller){
		addPropertyChangeListener1(controller);
		addComponentListener(new ComponentListener(){
			public void componentHidden(ComponentEvent e) {
			}
			public void componentMoved(ComponentEvent e) {
			}
			public void componentResized(ComponentEvent e) {
				int newHeight = e.getComponent().getSize().height;
				setHeight(newHeight);
			}
			public void componentShown(ComponentEvent e) {
			}
        });
	}
	public void addPropertyChangeListener1(PropertyChangeListener pcl) {
	  	pcListeners.add (pcl);
  	}
	
  	public void setHeight (int h) {
  		PropertyChangeEvent pce
  			= new PropertyChangeEvent (this,"internalFrameHeight",height,h);
  		for (PropertyChangeListener pcl : pcListeners)
  			pcl.propertyChange (pce);
  		this.height = h;
  	}
}