/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.deliverylite.colorbar.Colorbar1;
//import com.bhpb.deliverylite.colorbar.ColorbarUtil;
import com.bhpb.geographics.colorbar.ColorbarUtil;
import com.bhpb.geographics.util.ColorMap;

public class ShowModelParams {
	private float verticalScale = 1;
    private float horizontalScale = 1;
    private Rectangle scrollToVisible = new Rectangle();
    private String outputBaseName = "";
    private String nearStackName = "";
    private String farStackName = "";
    private boolean isNearAndFar = false;
    private boolean showRealStack = false;
    private boolean showSyntheticBefore = false;
    private boolean showSyntheticAfter = false;
    private String selectedModelAfterDataset = "";
    private String selectedModelAfterProperty = "";
    private String selectedModelBeforeProperty = DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH;
    private int verticalScrollPosition = 0;
    private float newMaxAmplitude = 0f;
    private boolean showModelBeforePropertyValue = true;
    private boolean showModelBeforeTimeValue = true;
    private boolean showModelBeforeSigmaTime = true;
    private boolean showModelBeforeColorbar = false;
    private boolean showModelAfterPropertyValue = true;
    private boolean showModelAfterTimeValue = true;
    private boolean showModelAfterSigmaTime = true;
    private boolean showModelAfterColorbar = false;
    private List<String> modelBeforePropertyList = new ArrayList<String>();
    private Map<String,Property> modelBeforeColorbarMap = null;
    private Map<String,Map<String,Property>>  modelAfterDatasetPropertyColorMap;
    private Map<String,Colorbar1>  currentUnlabeledColorbarMap;
    private static final Logger logger = Logger.getLogger(ShowModelParams.class.toString());
    
    public String getOutputBaseName(){
    	return outputBaseName;
    }

    public void setOutputBaseName(String name){
    	outputBaseName = name;
    }
    
    public void setNearAndFar(boolean b){
    	isNearAndFar = b;
    }
    
    public String getNearStackName(){
   		return nearStackName;
    }

    public void setNearStackName(String name){
    	nearStackName = name;
    }

    public String getFarStackName(){
   		return farStackName;
    }

    public void setFarStackName(String name){
    	farStackName = name;
    }
    
    public String getNearSyntheticBeforeDatasetName(){
   		return outputBaseName + DeliveryLiteUI._NEAR_SYNTHETIC_BEFORE;
    }

    public String getFarSyntheticBeforeDatasetName(){
   		return outputBaseName + DeliveryLiteUI._FAR_SYNTHETIC_BEFORE;
    }
    
    public String getNearSyntheticAfterDatasetName(){
   		return outputBaseName + DeliveryLiteUI._NEAR_SYNTHETIC_AFTER;
    }

    public String getFarSyntheticAfterDatasetName(){
   		return outputBaseName + DeliveryLiteUI._FAR_SYNTHETIC_AFTER;
    }

    public boolean isNearAndFar(){
    	return isNearAndFar;
    }

    public void setListOfProperties(List<String> properties){
    	modelBeforePropertyList = properties;
    }
    
    public Map<String,Colorbar1> getCurrentAvailableColorbars(){
    	return currentUnlabeledColorbarMap;
    }
    
    public void setCurrentAvailableColorbars(Map<String,Colorbar1> map){
    	currentUnlabeledColorbarMap = map;
    }
    
    public void setModelBeforeColorbarMap(Map<String,Property> map){
    	modelBeforeColorbarMap = map;
    }
    
    public void setModelAfterDatasetPropertyColorMap(Map<String,Map<String,Property>> map){
    	modelAfterDatasetPropertyColorMap = map;
    }
    
    public Map<String,Map<String,Property>> getModelAfterDatasetPropertyColorMap(){
    	return modelAfterDatasetPropertyColorMap;
    }
    
    public void setSelectedModelAfterDataset(String dataset){
    	selectedModelAfterDataset = dataset;
    }
    
    public String getSelectedModelAfterDataset(){
    	return selectedModelAfterDataset;
    }
    
    public void setSelectedModelAfterProperty(String prop){
    	selectedModelAfterProperty = prop;
    }
    
    public String getSelectedModelAfterProperty(){
    	return selectedModelAfterProperty;
    }

    public void setSelectedModelBeforeProperty(String prop){
    	selectedModelBeforeProperty = prop;
    }
    
    public String getSelectedModelBeforeProperty(){
    	return selectedModelBeforeProperty;
    }
    
    public void setShowRealStackEnabled(boolean bool){
    	showRealStack = bool;
    }
    
    public boolean isShowRealStackEnabled(){
    	return showRealStack;
    }

    public void setShowModelBeforePropertyValueEnabled(boolean bool){
    	showModelBeforePropertyValue = bool;
    }
    
    public boolean isShowModelBeforePropertyValueEnabled(){
    	return showModelBeforePropertyValue;
    }
    
    public void setShowModelBeforeTimeValueEnabled(boolean bool){
    	showModelBeforeTimeValue = bool;
    }
    
    public boolean isShowModelBeforeTimeValueEnabled(){
    	return showModelBeforeTimeValue;
    }
    
    public void setShowModelBeforeSigmaTimeEnabled(boolean bool){
    	showModelBeforeSigmaTime = bool;
    }
    
    public boolean isShowModelBeforeSigmaTimeEnabled(){
    	return showModelBeforeSigmaTime;
    }
    
    public void setShowModelBeforeColorbarEnabled(boolean bool){
    	showModelBeforeColorbar = bool;
    }
    
    public boolean isShowModelBeforeColorbarEnabled(){
    	return showModelBeforeColorbar;
    }
    
    public void setShowModelAfterPropertyValueEnabled(boolean bool){
    	showModelAfterPropertyValue = bool;
    }
    
    public boolean isShowModelAfterPropertyValueEnabled(){
    	return showModelAfterPropertyValue;
    }
    
    public void setShowModelAfterTimeValueEnabled(boolean bool){
    	showModelAfterTimeValue = bool;
    }
    
    public boolean isShowModelAfterTimeValueEnabled(){
    	return showModelAfterTimeValue;
    }
    
    public void setShowModelAfterSigmaTimeEnabled(boolean bool){
    	showModelAfterSigmaTime = bool;
    }
    
    public boolean isShowModelAfterSigmaTimeEnabled(){
    	return showModelAfterSigmaTime;
    }
    
    public void setShowModelAfterColorbarEnabled(boolean bool){
    	showModelAfterColorbar = bool;
    }
    
    public boolean isShowModelAfterColorbarEnabled(){
    	return showModelAfterColorbar;
    }
    
    public void setShowSyntheticBeforeEnabled(boolean bool){
    	showSyntheticBefore = bool;
    }
    
    public boolean isShowSyntheticBeforeEnabled(){
    	return showSyntheticBefore;
    }
    
    public void setShowSyntheticAfterEnabled(boolean bool){
    	showSyntheticAfter = bool;
    }
    
    public boolean isShowSyntheticAfterEnabled(){
    	return showSyntheticAfter;
    }
    
    public void setNewMaxAmplitude(float amp){
    	newMaxAmplitude = amp;
    }
    
    public float getNewMaxAmplitude(){
    	return newMaxAmplitude;
    }
    
    public void setVerticalScale(float vscale){
    	verticalScale = vscale;
    }
    
    public float getVerticalScale(){
    	return verticalScale;
    }
    
    public void setHorizontalScale(float hscale){
    	horizontalScale = hscale;
    }
    
    public float getHorizontalScale(){
    	return horizontalScale;
    }
    
    public void setScrollToVisible(Rectangle rect){
    	scrollToVisible = rect;
    }
    
    public Rectangle getScrollToVisible(){
    	return scrollToVisible;
    }
    
    public void setVerticalScrollPosition(int pos){
    	verticalScrollPosition = pos;
    }
    
    public int getVerticalScrollPosition(){
    	return verticalScrollPosition;
    }

    public void restoreShowModelParams(Node node) {    
    //public void restoreShowModelParams(Node node, Map<String,Colorbar1> nonFileBaseColorbars) {
    	if(node == null)
    		return;
    	NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
            	if(child.getNodeName().equals("horizontalScale")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			try{
            				horizontalScale = Float.parseFloat(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            	}else if(child.getNodeName().equals("verticalScale")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			try{
            				verticalScale = Float.parseFloat(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            	}else if(child.getNodeName().equals("verticalScrollPosition")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			try{
            				verticalScrollPosition = Integer.parseInt(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            	}else if(child.getNodeName().equals("scrollToVisible")){
            		Element el = (Element)child;
            		String val = el.getAttribute("x");
            		int x=0, y=0, width=0, height=0;
            		if(val != null && val.trim().length() > 0){
            			try{
            				scrollToVisible.x = Integer.parseInt(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            		val = el.getAttribute("y");
            		if(val != null && val.trim().length() > 0){
            			try{
            				scrollToVisible.y = Integer.parseInt(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            		val = el.getAttribute("width");
            		if(val != null && val.trim().length() > 0){
            			try{
            				scrollToVisible.width = Integer.parseInt(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            		val = el.getAttribute("height");
            		if(val != null && val.trim().length() > 0){
            			try{
            				scrollToVisible.height = Integer.parseInt(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            	}else if(child.getNodeName().equals("showRealStack")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
           				showRealStack = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showSyntheticBefore")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showSyntheticBefore = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showSyntheticAfter")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showSyntheticAfter = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelBeforePropertyValue")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelBeforePropertyValue = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelBeforeTimeValue")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelBeforeTimeValue = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelBeforeSigmaTime")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelBeforeSigmaTime = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelBeforeColorbar")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelBeforeColorbar = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelAfterPropertyValue")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelAfterPropertyValue = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelAfterTimeValue")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelAfterTimeValue = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelAfterSigmaTime")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelAfterSigmaTime = Boolean.valueOf(val);
            		}
           		}else if(child.getNodeName().equals("showModelAfterColorbar")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			showModelAfterColorbar = Boolean.valueOf(val);
            		}            		
           		}else if(child.getNodeName().equals("selectedModelAfterDataset")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			selectedModelAfterDataset = val;
            		}
           		}else if(child.getNodeName().equals("selectedModelAfterProperty")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			selectedModelAfterProperty = val;
            		}
           		}else if(child.getNodeName().equals("selectedModelBeforeProperty")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			selectedModelBeforeProperty = val;
            		}
           		}else if(child.getNodeName().equals("newMaxAmplitude")){
            		Element el = (Element)child;
            		String val = el.getAttribute("value");
            		if(val != null && val.trim().length() > 0){
            			try{
            				newMaxAmplitude = Float.parseFloat(val);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
           		}else if(child.getNodeName().equals("ModelBeforePropertyColormap")){
           			//restoreModelPropertyColormap(child,modelBeforeColorbarMap,nonFileBaseColorbars);
           			restoreModelPropertyColormap(child,modelBeforeColorbarMap);
           		}else if(child.getNodeName().equals("ModelAfterPropertyColormap")){
           			//restoreModelAfterDatasetProperties(child,modelAfterDatasetPropertyColorMap,nonFileBaseColorbars);
           			restoreModelAfterDatasetProperties(child,modelAfterDatasetPropertyColorMap);           			
           		}
            }
        }
    }
    
    
    //private void restoreModelPropertyColormap(Node node, Map<String,Property> map, Map<String,Colorbar1> nonFileBaseColorbars){
    private void restoreModelPropertyColormap(Node node, Map<String,Property> map){    	
    	if(node == null || map == null)
    		return;

    	NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
            	if(child.getNodeName().equals("property")){
            		Element el = (Element)child;
            		String name = el.getAttribute("name");
            		String minS = el.getAttribute("min");
            		String maxS = el.getAttribute("max");
            		String ignoreZeroS = el.getAttribute("ignoreZero");
            		float min = Float.MIN_VALUE;
            		float max = Float.MAX_VALUE;
            		boolean ignoreZero = false;
            		if(minS != null && minS.trim().length() > 0 && maxS != null && maxS.trim().length() > 0){
            			try{
            				min = Float.parseFloat(minS);
            				max = Float.parseFloat(maxS);
            			}catch(NumberFormatException e){
            				e.printStackTrace();
            			}
            		}
            		if(ignoreZeroS != null && ignoreZeroS.trim().length() > 0 ){
            			ignoreZero = Boolean.valueOf(ignoreZeroS);
            		}
            		//Colorbar1 colorbar = restoreColorbar(child,nonFileBaseColorbars);
            		Colorbar1 colorbar = restoreColorbar(child);
            		
            		com.bhpb.deliverylite.colorbar.ColorbarUtil.labeledColorbar(colorbar, min, max);
            		Property prop = new Property(name,min,max,colorbar,ignoreZero);
            		map.put(name,prop);
            	}
            }
        }
    }
    
    //private void restoreModelAfterDatasetProperties(Node node, Map<String,Map<String,Property>> map,Map<String,Colorbar1> nonFileBaseColorbars){
    private void restoreModelAfterDatasetProperties(Node node, Map<String,Map<String,Property>> map){    	
    	if(map == null)
    		return;
    	NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
            	if(child.getNodeName().equals("dataset")){
            		Element el = (Element)child;
            		String key = el.getAttribute("name");
            		Map<String,Property> mapP = null;
            		if(map.containsKey(key))
            			mapP = map.get(key);
            		else
            			mapP = new HashMap<String,Property>();
            		//restoreModelPropertyColormap(child,mapP,nonFileBaseColorbars);
            		restoreModelPropertyColormap(child,mapP);
            		map.put(key, mapP);
            	}
            }
        }
    }
    
    
    //private Colorbar1 restoreColorbar(Node node, Map<String,Colorbar1> nonFileBaseColorbars){
    private Colorbar1 restoreColorbar(Node node){    	
    	Colorbar1 colorbar = null;
    	NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
            	if(child.getNodeName().equals("colorbar")){
            		Element el = (Element)child;
            		String name = el.getAttribute("name");
            		String resourceBased = el.getAttribute("resourceBased");
            		boolean b_resourceBased = true;
            		if(resourceBased != null && resourceBased.trim().length() > 0 ){
            			b_resourceBased = Boolean.valueOf(resourceBased);
            		}
            		
            		if(b_resourceBased){
            			//if(DeliveryLiteUI.getAvailableColorbars().containsKey(name)){
            			if(currentUnlabeledColorbarMap.containsKey(name)){
            				//colorbar = DeliveryLiteUI.getAvailableColorbars().get(name);
            				colorbar = currentUnlabeledColorbarMap.get(name);
            				colorbar = new Colorbar1(colorbar);
            			}else{
            				//colorbar = ColorbarUtil.getUnlabeledColorbarByName(name);
            				ColorMap colorMap = null;
                      	    try{
                      		   colorMap = ColorbarUtil.getColorMapByResourceName(name);
                      	    }catch(IOException e){
                      		   e.printStackTrace();
                      		   logger.warning("IO problem occurred while getting colormap object by using colorbar name=" + name);
                      	    }
            				colorbar = new Colorbar1(name,colorMap);
            			}
            		}else{
            			//if(nonFileBaseColorbars.containsKey(name))
            			if(currentUnlabeledColorbarMap.containsKey(name))
                			colorbar = new Colorbar1(currentUnlabeledColorbarMap.get(name));
            			//colorbar = nonFileBaseColorbars.get(name);
            		}
            	}
            }
        }
        return colorbar;
    }
    

    public void restoreNonPredefinedColorbars(Node node){
    	//Map<String,Colorbar1> map = new HashMap<String,Colorbar1>();
    	//if(node == null)
    	//	return map;
    	if(node == null)
    		return;
       	NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
            	if(child.getNodeName().equals(Colorbar1.getXmlAlias())){
            		Colorbar1 cb = Colorbar1.restoreState(child);
            		currentUnlabeledColorbarMap.put(cb.getName(), cb);
            		//map.put(cb.getName(), cb);
            	}
            }
        }
        //return map;
        return;
    }
    
    public List<Colorbar1> getNonPredefinedColorbarList(){
    	List<Colorbar1> list = new ArrayList<Colorbar1>();
    	if(currentUnlabeledColorbarMap != null){
    		Set keys = currentUnlabeledColorbarMap.keySet();
    		Iterator it = keys.iterator();
    		while(it.hasNext()){
    			Object key = it.next();
    			Colorbar1 cb = currentUnlabeledColorbarMap.get(key);
    			if(cb != null && !cb.isResourceBased()){
    				if(!list.contains(cb))
    					list.add(cb);
    			}
    		}
    	}
    	return list;
    }
    
    public static String getXmlAlias(){
    	return "ShowModelParams";
    }
    
    public static String getXmlAliasColorbar(){
    	return "NonFileBasedColorbars";
    }

    public String toXML(){
    	StringBuffer content = new StringBuffer();
    	
    	List<Colorbar1> list = getNonPredefinedColorbarList();
    	if(list != null && list.size() > 0){
    		content.append("<" + getXmlAliasColorbar() + ">\n");
    		for(Colorbar1 cbar : list){
    			content.append(cbar.genState());
    		}
    		content.append("</" + getXmlAliasColorbar() + ">\n");
    	}
    	
       	content.append("<" + getXmlAlias() + ">\n");
        content.append("<horizontalScale value=\"" + horizontalScale + "\"" + "/>\n");
        content.append("<verticalScale value=\"" + verticalScale + "\"" + "/>\n");
        content.append("<verticalScrollPosition value=\"" + verticalScrollPosition + "\"" + "/>\n");
        content.append("<scrollToVisible x=\"" + scrollToVisible.x + "\" y=\"" + scrollToVisible.y + "\" width=\"" + scrollToVisible.width + "\" height=\"" + scrollToVisible.height + "\"/>\n");
        content.append("<showRealStack value=\"" + showRealStack + "\"" + "/>\n");
        content.append("<showSyntheticBefore value=\"" + showSyntheticBefore + "\"" + "/>\n");
        content.append("<showSyntheticAfter value=\"" + showSyntheticAfter + "\"" + "/>\n");
        content.append("<showModelBeforePropertyValue value=\"" + showModelBeforePropertyValue + "\"" + "/>\n");
        content.append("<showModelBeforeTimeValue value=\"" + showModelBeforeTimeValue + "\"" + "/>\n");
        content.append("<showModelBeforeSigmaTime value=\"" + showModelBeforeSigmaTime + "\"" + "/>\n");
        content.append("<showModelBeforeColorbar value=\"" + showModelBeforeColorbar + "\"" + "/>\n");
        content.append("<showModelAfterPropertyValue value=\"" + showModelAfterPropertyValue + "\"" + "/>\n");
        content.append("<showModelAfterTimeValue value=\"" + showModelAfterTimeValue + "\"" + "/>\n");
        content.append("<showModelAfterSigmaTime value=\"" + showModelAfterSigmaTime + "\"" + "/>\n");
        content.append("<showModelAfterColorbar value=\"" + showModelAfterColorbar + "\"" + "/>\n");        
        content.append("<selectedModelAfterDataset value=\"" + selectedModelAfterDataset + "\"" + "/>\n");
        content.append("<selectedModelAfterProperty value=\"" + selectedModelAfterProperty + "\"" + "/>\n");
        content.append("<selectedModelBeforeProperty value=\"" + selectedModelBeforeProperty + "\"" + "/>\n");
        content.append("<newMaxAmplitude value=\"" + newMaxAmplitude + "\"" + "/>\n");
        content.append("<ModelBeforePropertyColormap>\n");
        if(modelBeforePropertyList != null){
	        for(String name : modelBeforePropertyList){
				String colorbarName = "";
				if(modelBeforeColorbarMap.containsKey(name)){
					Property prop = modelBeforeColorbarMap.get(name);
					if(prop.getColorbar() != null)
						colorbarName = prop.getColorbar().getName();
					content.append("<property name=\"" + prop.getName() + "\" min=\"" + prop.getMin() + "\" max=\"" +  prop.getMax() + "\" ignoreZero=\"" +  prop.isIgnoreZero() + "\">\n");
					content.append("<colorbar name=\"" +  colorbarName  + "\" resourceBased=\"" +  prop.getColorbar().isResourceBased()  + "\">\n");
					content.append("</colorbar>\n");
					content.append("</property>\n");
				}
			}
        }
        content.append("</ModelBeforePropertyColormap>\n");
        content.append("<ModelAfterPropertyColormap>\n");
        if(modelAfterDatasetPropertyColorMap != null){
        	//Set set = modelAfterDatasetPropertyColorMap.keySet();
        	for (Map.Entry<String, Map<String,Property>> e1 : modelAfterDatasetPropertyColorMap.entrySet()){
        	//Iterator it = set.iterator();
        	//while(it.hasNext()){
        		//String key = (String)it.next();
        		String key = e1.getKey();
        		if(key.equals("Input Model") || !key.startsWith(outputBaseName))
        			continue;
        		content.append("<dataset name=\"" + key + "\">\n");
        		Map<String,Property> pMap = e1.getValue();
        		for (Map.Entry<String, Property> e : pMap.entrySet()){
        		//Set setP = pMap.entrySet();
        		//Iterator itP = setP.iterator();
		        //while(itP.hasNext()){
		        	//String name = (String)itP.next();
        			//String name = e.getKey();
					String colorbarName = "";
					Property prop = e.getValue();
					if(prop.getColorbar() != null)
						colorbarName = prop.getColorbar().getName();
					content.append("<property name=\"" + prop.getName() + "\" min=\"" + prop.getMin() + "\" max=\"" +  prop.getMax() + "\" ignoreZero=\"" +  prop.isIgnoreZero() + "\">\n");
					content.append("<colorbar name=\"" +  colorbarName  + "\" resourceBased=\"" +  prop.getColorbar().isResourceBased()  + "\">\n");
					content.append("</colorbar>\n");
					content.append("</property>\n");
				}
		        content.append("</dataset>\n");
        	}
        }
        content.append("</ModelAfterPropertyColormap>\n");
        content.append("</" + getXmlAlias() + ">\n");
        return content.toString();
    }
}
