/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;

public class TextView extends BasePane {
	private static final long serialVersionUID = 1L;
	JTextArea   textArea;
	
	public TextView(DeliveryLitePlugin delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		getBasePanel().add(createTextPane(), BorderLayout.CENTER);
	}
	
	public void setText(String text) {
		textArea.setText(text);
	}
	
	private JScrollPane createTextPane() {
		textArea = new JTextArea();
    	textArea.setEditable(false);
    	JScrollPane scrollPane = new JScrollPane(textArea);  
 	    return scrollPane;
    }

}
