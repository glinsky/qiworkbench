/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Constants used within Wavelet Decomposition
 *
 * @author Gil Hansen
 * @version 1.0
 */
public final class DeliveryLiteConstants {
    /**
     * Prevent object construction outside of this class.
     */
    private DeliveryLiteConstants() {}

    /** plugin's display name */
    public static final String DELIVERY_LITE_PLUGIN_NAME = "deliveryLite";

    /** plugin GUI's display name */
    public static final String DELIVERY_LITE_GUI_NAME = "Delivery Lite GUI";
    // debug print: 0=none, 1=moderate, 2=verbose
    public static final int DEBUG_PRINT = 2;

    // integer status
    public static final int ERROR_STATUS = 1;
    public static final int OK_STATUS = 0;
    public static final int JOB_RUNNING_STATUS = 1;
    public static final int JOB_ENDED_STATUS = 2;
    public static final int JOB_UNKNOWN_STATUS = 3;
    public static final int JOB_UNSUBMITTED_STATUS = 0;
    // messages used for file chooser service

    public static final int PREF_WIDTH  = 750;
    public static final int PREF_HEIGHT = 650;

    public static final String DEFAULT_FLUID_BRINE_NAME = "brine";
    public static final String DEFAULT_FLUID_OIL_NAME = "oil";
    public static final String DEFAULT_FLUID_GAS_NAME = "gas";
    public static final String DEFAULT_REFERENCE_FLUID_NAME = "reference fluid";
    public static final ArrayList<String> DEFAULT_FLUID_LIST = new ArrayList<String>();
    static{
    	DEFAULT_FLUID_LIST.add(DEFAULT_FLUID_BRINE_NAME);
    	DEFAULT_FLUID_LIST.add(DEFAULT_FLUID_OIL_NAME);
    	DEFAULT_FLUID_LIST.add(DEFAULT_FLUID_GAS_NAME);
    	DEFAULT_FLUID_LIST.add(DEFAULT_REFERENCE_FLUID_NAME);
    	//DEFAULT_FLUID_LIST.add("lsg");
    }

    public static final ArrayList<String> DEFAULT_STACK_LIST = new ArrayList<String>();
    static{
    	DEFAULT_STACK_LIST.add("near");
    	//DEFAULT_STACK_LIST.add("far");
    }
    public static final String DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME = "Sand";
    public static final String DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME = "Mixing Shale";
    public static final String DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME = "Bounding Shale";
    public static final ArrayList<String> DEFAULT_RESERVOIR_ENDMEMBER_LIST = new ArrayList<String>();
    static{
    	DEFAULT_RESERVOIR_ENDMEMBER_LIST.add(DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
    	//DEFAULT_STACK_LIST.add("far");
    }
    public static final ArrayList<String> DEFAULT_NONRESERVOIR_ENDMEMBER_LIST = new ArrayList<String>();
    static{
    	DEFAULT_NONRESERVOIR_ENDMEMBER_LIST.add(DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME);
    	DEFAULT_NONRESERVOIR_ENDMEMBER_LIST.add(DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME);
    }

    public static final String GET_NEAR_SEISMIC_DATASET_CMD = "getNearSeismicDataset";
    public static final String GET_FAR_SEISMIC_DATASET_CMD = "getFarSeismicDataset";
    public static final String GET_NEAR_WAVELET_FILE_CMD = "getNearWaveltFile";
    public static final String GET_FAR_WAVELET_FILE_CMD = "getFarWaveletFile";
    public static final String GET_IMPORT_XML_FILE_CMD = "getImportXMLFile";
    public static final String GET_MODEL_XML_FILE_CMD = "getModelXMLFile";
    public static final String GET_MODEL_PARAMS_XML_FILE_CMD = "getModelParamsXMLFile";
    public static final String SAVE_MODEL_XML_FILE_CMD = "saveModelXMLFile";
    public static final String SAVE_MODEL_PARAMS_XML_FILE_CMD = "saveModelParamsXMLFile";    
    public static final String EXPORT_XML_FILE_CMD = "exportXMLFile";
    public static final String DEFAULT_MASTER_DEPTH_LAYER = "1";
    public static final String DEFAULT_NUM_RUNS = "500";
    public static final String DEFAULT_MIN_TIME = "0.0";
    public static final String DEFAULT_MAX_TIME = "8.0";
    public static final String DEFAULT_SPAGETTI_PLOT_MIN_TIME = "1.0";
    public static final String DEFAULT_SPAGETTI_PLOT_MAX_TIME = "7.0";    
    public static final String DEFAULT_TIME = "0";
    public static final String DEFAULT_SATURATION_OF_OIL = "0.7";
    public static final String DEFAULT_SATURATION_OF_GAS = "0.8";
    public static final String DEFAULT_SATURATION_OF_LSG = "0.15";
    public static final String DEFAULT_FLUID_BRINE_VP = "5250.0";
    public static final float defaultFluidBrineVp = 5250.0f;
    public static final String DEFAULT_FLUID_BRINE_RHO = "1.01";
    public static final float defaultFluidBrineRho = 1.01f;
    public static final String DEFAULT_FLUID_OIL_VP = "4500.0";
    public static final float defaultFluidOilVp = 4500.0f;
    public static final String DEFAULT_FLUID_OIL_RHO = "0.9";
    public static final float defaultFluidOilRho = 0.9f;
    public static final String DEFAULT_FLUID_GAS_VP = "3500.0";
    public static final float defaultFluidGasVp = 3500.0f;
    public static final String DEFAULT_FLUID_GAS_RHO = "0.15";
    public static final float defaultFluidGasRho = 0.15f;
    public static final String DEFAULT_FLUID_LSG_VP = "3700.0";
    public static final String DEFAULT_FLUID_LSG_RHO = "0.35";
    public static final String DEFAULT_NEAR_STACK_NAME = "near";
    public static final String DEFAULT_FAR_STACK_NAME = "far";
    public static final String DEFAULT_NEAR_STACK_FILE_NAME = "tmp/near_input_seismic.su";
    public static final String DEFAULT_FAR_STACK_FILE_NAME = "tmp/far_input_seismic.su";
    public static final String DEFAULT_NEAR_STACK_WAVELET_FILE_NAME = "tmp/near_wavelet.su";
    public static final String DEFAULT_FAR_STACK_WAVELET_FILE_NAME = "tmp/far_wavelet.su";
    public static final String DEFAULT_STACK_WAVELET_FILE_NAME = "full_wavelet.su";
    public static final String DEFAULT_STACK_WAVELET_NOISE_RMS = "1.0";
    public static final String DEFAULT_STACK_MIN_OFFSET = "0";
    public static final String DEFAULT_STACK_MAX_OFFSET = "4000";
    public static final String DEFAULT_STACK_REFLECTOR_TIME = "1300";
    public static final String DEFAULT_STACK_VELOCITY = "7400";
    public static final String DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_INTERCEPT = "4320";
    public static final String DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_DEPTH_COEFFICIENT = "0.33";
    public static final String DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_LFIV_COEFFICIENT = "0";
    public static final String DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_SIGMA = "250";
    public static final String DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_INTERCEPT = "-2930";
    public static final String DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_SLOPE = "0.75";
    public static final String DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_SIGMA = "190";
    public static final String DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_INTERCEPT = "4640";
    public static final String DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_DEPTH_COEFFICIENT = "0.33";
    public static final String DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_LFIV_COEFFICIENT = "0";
    public static final String DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_SIGMA = "470";
    public static final String DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_INTERCEPT = "-1630";
    public static final String DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SLOPE = "0.57";
    public static final String DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SIGMA = "140";
    public static final String DEFAULT_ENDMEMBER_POROSITY_CURVE_INTERCEPT = "0.48";
    public static final String DEFAULT_ENDMEMBER_POROSITY_CURVE_SLOPE = "-1.9E-5";
    public static final String DEFAULT_ENDMEMBER_POROSITY_CURVE_SIGMA = "0.018";
    public static final String DEFAULT_ENDMEMBER_DENSITY_CURVE_FACTOR = "0.22";
    public static final String DEFAULT_ENDMEMBER_DENSITY_CURVE_EXPONENT = "0.26";
    public static final String DEFAULT_ENDMEMBER_DENSITY_CURVE_SIGMA = "0.02";
    public static final String DEFAULT_ENDMEMBER_VS_GRAIN = "13740";
    public static final String DEFAULT_ENDMEMBER_VP_GRAIN = "20151";
    public static final String DEFAULT_ENDMEMBER_RHO_GRAIN = "2.65";
    public static final String DEFAULT_SIGMA_TIME = "0.1";
    public static final String DEFAULT_LFIV = "8000.0";
    public static final String DEFAULT_OIL_SATURATION = "0.7";
    public static final float defautlOilSaturation = 0.7f;
    public static final String DEFAULT_HIGH_GAS_SATURATION = "0.8";
    public static final float defautlHigGasSaturation = 0.8f;
    public static final String DEFAULT_LOW_GAS_SATURATION = "0.2";
    public static final float defautlLowGasSaturation = 0.2f;
    /*
     * Column Names for Layer table
     */
    public static final String COLUMN_NAME_TIME = "time";
    public static final String COLUMN_NAME_SIGMA_TIME = "err_t";
    public static final String COLUMN_NAME_THICKNESS = "th";
    public static final String COLUMN_NAME_SIGMA_THICKNESS = "err_th";
    public static final String COLUMN_NAME_TVDBML = "TVDBML";
    public static final String COLUMN_NAME_LFIV = "LFIV";
    public static final String COLUMN_NAME_NET_TO_GROWTH = "N/G";
    public static final String COLUMN_NAME_SIGMA_NG = "err N/G";
    public static final String COLUMN_NAME_POSSIBILITY_OIL = "Po";
    public static final String COLUMN_NAME_POSSIBILITY_GAS = "Pg";
    public static final String COLUMN_NAME_POSSIBILITY_LSG = "Plsg";
    
    /*
     * Column Names for Fixed Layer table
     */
    public static final String COLUMN_NAME_POUND_SIGN = "#";
    public static final String COLUMN_NAME_ANALYZE = "Analyze";
    public static final String COLUMN_NAME_TYPE = "Type";
    public static final String COLUMN_NAME_LAYER_NAME = "Layer Name";
    
    
    
    
    public static final String [] PREDEFINED_COLORBARS={
    	"bossbar_rev","brine_gas","brine_oil",
    	"brine_oil_gas","contour.cmp","facies2",
    	"facies3","facies","good_bad",
    	"grey_scale","grey_scale_rev","inversion",
    	"posneg","posneg_reverse","rainbow",
    	"rainbow_rev","sand_shale","statistical_mean",
    	"statistical_mean_noblue","statistical_stdev"
    };
    /*
     * initial default colorbar names that needs to be loaded when the app is started
     */
    public static final String[] colorbars = {"grey_scale","good_bad","rainbow","sand_shale","brine_oil","brine_gas"};
    
    /*
     * initial default property name and its associated colorbar mapping that needs to be loaded 
     * when the app is started
     */
    public static final HashMap<String,String> default_property_color_map = new HashMap<String,String>();
    static{
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_TIME,"grey_scale");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME,"good_bad");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_THICKNESS,"rainbow");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS,"good_bad");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_TVDBML,"grey_scale");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_LFIV,"grey_scale");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH,"sand_shale");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG,"good_bad");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL,"brine_oil");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS,"brine_gas");
    	default_property_color_map.put(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG,"brine_gas");
    	default_property_color_map.put("sample-correlation","rainbow");
    	default_property_color_map.put("thickness","rainbow");
    	default_property_color_map.put("net-sand","rainbow");
    	default_property_color_map.put("net-hydrocarbon","rainbow");
    	default_property_color_map.put("net-oil","rainbow");
    	default_property_color_map.put("net-lsg","rainbow");
    	default_property_color_map.put("net-gas","rainbow");
    	default_property_color_map.put("fluid_type_0","brine_oil");
    	default_property_color_map.put("fluid_type_1","brine_oil");
    	default_property_color_map.put("fluid_type_2","brine_gas");
    	default_property_color_map.put("fluid_type_3","brine_gas");
    	default_property_color_map.put("d","grey_scale");
    	default_property_color_map.put("NG","sand_shale");
    	default_property_color_map.put("vp_m","rainbow");
    	default_property_color_map.put("vs_m","rainbow");
    	default_property_color_map.put("rho_m","rainbow");
    	default_property_color_map.put("vp_s","rainbow");
    	default_property_color_map.put("vs_s","rainbow");
    	default_property_color_map.put("phi_s","rainbow");
    	default_property_color_map.put("S_h","rainbow");
    	default_property_color_map.put("rho_h","rainbow");
    	default_property_color_map.put("fluid_type","rainbow");
    	default_property_color_map.put("vp_eff","rainbow");
    	default_property_color_map.put("rho_eff","rainbow");
    	default_property_color_map.put("Z_eff","rainbow");
    	default_property_color_map.put("R_near","rainbow");
    	default_property_color_map.put("R_far","rainbow");
    	default_property_color_map.put("seismic_chisquare","good_bad");
    	default_property_color_map.put("float_fraction","rainbow");
    	default_property_color_map.put("t_fc","rainbow");
    	default_property_color_map.put("d_fc","rainbow");
    	default_property_color_map.put("R_near_fc","rainbow");
    	default_property_color_map.put("R_far_fc","rainbow");
    	default_property_color_map.put("S_oil","rainbow");
    	default_property_color_map.put("S_lsg","rainbow");
    	default_property_color_map.put("S_gas","rainbow");
    	default_property_color_map.put("vs_eff","rainbow");
    	default_property_color_map.put("Zs_eff","rainbow");
    	default_property_color_map.put("t","grey_scale");
    };
    
    /**
     *	default mapping of each property and their possible ranges; note that Float.MIN_VALUE and Float.MAX_VALUE indicates 
     *	the values subject to change
     *	based on the coming xml model
     */ 
    public static final HashMap<String, Property> default_property_range = new HashMap<String,Property>();
    static{
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_TIME, new Property(DeliveryLiteConstants.COLUMN_NAME_TIME, Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME, new Property(DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME, 0, Float.MAX_VALUE, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_THICKNESS, new Property(DeliveryLiteConstants.COLUMN_NAME_THICKNESS, 0, Float.MAX_VALUE, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS,new Property(DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS, 0, Float.MAX_VALUE, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_TVDBML,new Property(DeliveryLiteConstants.COLUMN_NAME_TVDBML, 0, Float.MAX_VALUE, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_LFIV,new Property(DeliveryLiteConstants.COLUMN_NAME_LFIV, 0, Float.MAX_VALUE, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH,new Property(DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH, 0, 1, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG,new Property(DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG, 0, 0.5f, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL,new Property(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL, 0, 1, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS,new Property(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS, 0, 1, null));
    	default_property_range.put(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG,new Property(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG, 0, 1, null));
    	default_property_range.put("sample-correlation",new Property("sample-correlation",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("thickness",new Property("thickness",0, Float.MAX_VALUE, null));
    	default_property_range.put("net-sand",new Property("net-sand",0, Float.MAX_VALUE, null));
    	default_property_range.put("net-hydrocarbon",new Property("net-hydrocarbon",0, Float.MAX_VALUE, null));
    	default_property_range.put("net-oil",new Property("net-oil",0, Float.MAX_VALUE, null));
    	default_property_range.put("net-lsg",new Property("net-lsg",0, Float.MAX_VALUE, null));
    	default_property_range.put("net-gas",new Property("net-gas",0, Float.MAX_VALUE, null));
    	default_property_range.put("fluid_type_0",new Property("fluid_type_0",0, 1, null));
    	default_property_range.put("fluid_type_1",new Property("fluid_type_1",0, 1, null));
    	default_property_range.put("fluid_type_2",new Property("fluid_type_2",0, 1, null));
    	default_property_range.put("fluid_type_3",new Property("fluid_type_3",0, 1, null));
    	default_property_range.put("d",new Property("d",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("NG",new Property("NG",0, 1, null));
    	default_property_range.put("vp_m",new Property("vp_m",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("vs_m",new Property("vs_m",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("rho_m",new Property("rho_m",1.8f, 2.6f, null));
    	default_property_range.put("vp_s",new Property("vp_s",Float.MIN_VALUE, Float.MAX_VALUE, null,true));
    	default_property_range.put("vs_s",new Property("vs_s",Float.MIN_VALUE, Float.MAX_VALUE, null,true));
    	default_property_range.put("phi_s",new Property("phi_s",0.0f, 0.5f, null));
    	default_property_range.put("S_h",new Property("S_h",0, 1, null));
    	default_property_range.put("rho_h",new Property("rho_h",Float.MIN_VALUE, Float.MAX_VALUE, null,true));
    	default_property_range.put("fluid_type",new Property("fluid_type",0, 1, null));
    	default_property_range.put("vp_eff",new Property("vp_eff",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("rho_eff",new Property("rho_eff",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("Z_eff",new Property("Z_eff",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("R_near",new Property("R_near",0, 0.4f, null));
    	default_property_range.put("R_far",new Property("R_far",0, 0.4f, null));
    	default_property_range.put("seismic_chisquare",new Property("seismic_chisquare",0, 10f, null));
    	default_property_range.put("float_fraction",new Property("float_fraction",0, 0.2f, null));
    	default_property_range.put("t_fc",new Property("t_fc",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("d_fc",new Property("d_fc",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("R_near_fc",new Property("R_near_fc",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("R_far_fc",new Property("R_far_fc",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("S_oil",new Property("S_oil",0, 1, null));
    	default_property_range.put("S_lsg",new Property("S_lsg",0, 1, null));
    	default_property_range.put("S_gas",new Property("S_gas",0, 1, null));
    	default_property_range.put("vs_eff",new Property("vs_eff",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("Zs_eff",new Property("Zs_eff",Float.MIN_VALUE, Float.MAX_VALUE, null));
    	default_property_range.put("t",new Property("t",Float.MIN_VALUE, Float.MAX_VALUE, null));
    };
    
    public static final int MAKE_SPAGHETTI_PLOT = 0;
    public static final int MAKE_SCATTER_PLOT = 1;
    public static final int MAKE_HISTORGRAM_PLOT = 2;
    public static final int MAKE_FULL_ASCII_OUTPUT = 3;
    
}
