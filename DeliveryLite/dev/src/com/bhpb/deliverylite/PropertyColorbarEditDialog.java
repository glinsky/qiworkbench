/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.deliverylite;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

//import com.bhpb.deliverylite.colorbar.Colorbar;
//import com.bhpb.deliverylite.colorbar.ColorbarUtil;
import com.bhpb.deliverylite.colorbar.Colorbar1;
//import com.bhpb.geographics.colorbar.Colorbar;
import com.bhpb.geographics.colorbar.ColorbarUtil;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geographics.util.ColorMapEditor;
import javax.swing.SwingUtilities;

/**
 *
 * @author  
 */
public class PropertyColorbarEditDialog extends javax.swing.JDialog {
	private static final Logger logger = Logger.getLogger(PropertyColorbarEditDialog.class.toString());
	/**	
	 * keyed by data set name (model before called input_model"
	 * value keyed by property name
	 */
    private Map<String,Map<String,Property>> colormap; 
    private Map<String,Colorbar1> availableColorbar;//keyed by colorbar name
    private Map<String,String> modifiedPropertyColorbar = new HashMap<String,String>();
    private ViewModelDialog parentDialog;
    private List<String> properties;
    private String modelName = "";
    private int selectedRow = -1;
    private int mode = 0; //0-- model before; 1-- model after
    private static List<String> availablePredefinedColorbarNames;
    static{
    	availablePredefinedColorbarNames = ColorbarUtil.getListOfavailableColorbarNames();
    }
    
    /** Creates new form PropertyInfoDialog */
    public PropertyColorbarEditDialog(Frame parentUI, int mode, List<String> properies, String modelName, boolean modal, Map<String,Map<String,Property>> colormap, Map<String, Colorbar1> availableColorbar) {
    	super(parentUI, modal);
    	initGui(parentUI, mode, properies, modelName, modal, colormap, availableColorbar);
    }

    /** Creates new form PropertyInfoDialog */
    public PropertyColorbarEditDialog(ViewModelDialog parentUI, int mode, List<String> properies, String modelName, boolean modal, Map<String,Map<String,Property>> colormap, Map<String, Colorbar1> availableColorbar) {
    	super((Frame)SwingUtilities.getRoot(parentUI), modal);
    	initGui(parentUI, mode, properies, modelName, modal, colormap, availableColorbar);
    }


    public PropertyColorbarEditDialog(Dialog parentUI, int mode, List<String> properies, String modelName, boolean modal, Map<String,Map<String,Property>> colormap, Map<String, Colorbar1> availableColorbar) {
    	super(parentUI, modal);
    	initGui(parentUI, mode, properies, modelName, modal, colormap, availableColorbar);
    }    
    
    private void initGui(Component parentUI, int mode, List<String> properies, String modelName, boolean modal, Map<String,Map<String,Property>> colormap, Map<String, Colorbar1> availableColorbar) {
        this.parentDialog = (ViewModelDialog)parentUI;
        this.properties = properies;
        this.modelName = modelName;
        this.colormap = colormap;
        this.mode = mode;
        this.availableColorbar = availableColorbar;
        
        if(availableColorbar != null){
        	for(String s : availablePredefinedColorbarNames){
        		if(!availableColorbar.containsKey(s)){
        			ColorMap colorMap = null;
              	    try{
              		   colorMap = ColorbarUtil.getColorMapByResourceName(s);
              	    }catch(IOException e){
              		   e.printStackTrace();
              		   logger.warning("IO problem occurred while getting colormap object by using colorbar name=" + s);
              	    }
        			Colorbar1 cb = new Colorbar1(s,colorMap);
        			availableColorbar.put(s, cb);
        		}
        	}
        	//Set<String> keySets = availableColorbar.keySet();
        	//Iterator it = keySets.iterator();
        	//while(it.hasNext()){
        ///		Object key = it.next();
        	//	if(!availableColorbarNames.contains(key))
        	//		availableColorbarNames.add((String)key);
        	//}
        }
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {
        propertyInfoTableScrollPane = new javax.swing.JScrollPane();
        propertyInfoTable = new javax.swing.JTable();
        okButton = new javax.swing.JButton("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });
        applyButton = new javax.swing.JButton("Apply");
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyActionPerformed(evt);
            }
        });
        cancelButton = new javax.swing.JButton("Cancel");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Property Information");
        Object[][] modelObjs = new Object[0][5];
        if(properties != null){
        //if(parentUI != null){
        //JTable layerTable = parentUI.getLayerTable();
	        modelObjs = new Object[properties.size()][5];
	        //for(int i = 0; i <layerTable.getColumnCount(); i++){
	        //	String key = layerTable.getColumnName(i);
	        Map<String,Property> cm = colormap.get(modelName);
	        for(int i = 0; i < properties.size(); i++){
	        	String key = properties.get(i);
	        	if(cm.containsKey(key)){
	        		Property prop = cm.get(key);
	        		modelObjs[i][0] = prop.getName();
	        		modelObjs[i][1] = prop.getMin();
	        		modelObjs[i][2] = prop.getMax();
	        		modelObjs[i][3] = prop.getColorbar().getName();
	        		modelObjs[i][4] = "Edit";
	        	}
	        }
        }
        propertyInfoTable.setModel(new javax.swing.table.DefaultTableModel(/*
            new Object [][] {
                {"time", "0.1", "10.5", "grey_scale", "Edit"},
                {"err_t", "0", "0", "good_bad",  "Edit"},
                {"th", "0", "0", "rainbow",  "Edit"},
                {"err_th", "0", "0", "good_bad",  "Edit"},
                {"TVDBML", "0", "1050.0", "grey_scale",  "Edit"},
                {"LFIV", "0", "8000", "grey_scale",  "Edit"},
                {"N/G", "0", "1", "sand_shale",  "Edit"},
                {"err N/G", "0", "0.5", "good_bad",  "Edit"},
                {"Po", "0", "1", "brine_oil",  "Edit"},
                {"Pg", "0", "1", "brine_gas",  "Edit"},
                {"Plsg", "0", "1", "brine_gas",  "Edit"}
            }*/ modelObjs,
            new String [] {
                "Property Name", "min", "max", "Associated Predefined Colorbar", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        propertyInfoTableScrollPane.setViewportView(propertyInfoTable);
        
        ListSelectionModel rowSM = propertyInfoTable.getSelectionModel();

        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                //if (e.getValueIsAdjusting()) return;

                ListSelectionModel lsm = (ListSelectionModel)e.getSource();
                if (lsm.isSelectionEmpty()) {
                    selectedRow = -1;
                } else {
                    selectedRow = lsm.getMinSelectionIndex();
                }
            }
        });

        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        
        javax.swing.JLabel modelLabel = new javax.swing.JLabel("Model:  ");
        javax.swing.JLabel modelNameLabel = new javax.swing.JLabel(modelName);
        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(okButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(applyButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cancelButton)
                .addContainerGap())
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(propertyInfoTableScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 490, Short.MAX_VALUE)
                .addContainerGap())
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(modelLabel)
                .add(modelNameLabel)
                .addContainerGap(490, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                		.add(modelNameLabel)
                		.add(modelLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(propertyInfoTableScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 206, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cancelButton)
                    .add(applyButton)
                    .add(okButton))
                .addContainerGap())
        );
        

        //column.setCellRenderer(new ButtonRenderer(propertyInfoTable,4));
        TableColumn column = propertyInfoTable.getColumnModel().getColumn(3);
        column.setPreferredWidth(200); column.setMinWidth(200); column.setMaxWidth(200);
        colorbarComboBox = new JComboBox();
        Object[] availableColorbarNames = availableColorbar.keySet().toArray();
        Arrays.sort(availableColorbarNames);
        for(Object s : availableColorbarNames)
        	colorbarComboBox.addItem(s);
        colorbarComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorbarComboBoxActionPerformed(evt);
            }
        });
        
        column.setCellEditor(new DefaultCellEditor(colorbarComboBox));
        
//      Create button column
        column = propertyInfoTable.getColumnModel().getColumn(4);
        column.setCellRenderer(new ButtonRenderer());
        //  Create button column
        //ButtonColumn buttonColumn = new ButtonColumn(propertyInfoTable, 4);
        column.setCellEditor(
			new ButtonEditor(this,propertyInfoTable,modelName,colormap,availableColorbar,modifiedPropertyColorbar,colorbarComboBox));
        
        column = propertyInfoTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(200); column.setMinWidth(100); column.setMaxWidth(200);
        
        
        column = propertyInfoTable.getColumnModel().getColumn(3);
        
        
        JTableHeader header = propertyInfoTable.getTableHeader();
        final Font boldFont = header.getFont().deriveFont(Font.BOLD);
        final TableCellRenderer headerRenderer = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

        propertyInfoTable.getColumnModel().getColumn(0).setCellRenderer(new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont(boldFont);
                
                //comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
        //javax.swing.JPanel buttonPanel = new javax.swing.JPanel();
        //buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        //buttonPanel.add(okButton);
        //buttonPanel.add(applyButton);
        //buttonPanel.add(cancelButton);
        //this.getContentPane().add(propertyInfoTableScrollPane,BorderLayout.CENTER);
        //this.getContentPane().add(buttonPanel,BorderLayout.SOUTH);
        pack();
    }// </editor-fold>

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	this.dispose();
    }

    private void okActionPerformed(java.awt.event.ActionEvent evt) {
    	if(!validateEdit())
    		return;
    	commitChange();
    	this.dispose();
    }

    private boolean validateEdit(){
    	if(propertyInfoTable.getCellEditor() != null)
    		propertyInfoTable.getCellEditor().stopCellEditing();
    	TableModel model = propertyInfoTable.getModel();
    	StringBuffer buf = new StringBuffer();
    	for(int i = 0; i < model.getRowCount(); i++){
    		String min = model.getValueAt(i, 1).toString();
    		if(!isFloat(min))
    			buf.append((String)model.getValueAt(i,0) + " min value must be a number.\n");
    		 String max = model.getValueAt(i, 2).toString();
     		if(!isFloat(max))
     			buf.append((String)model.getValueAt(i,0) + " max value must be a number.\n");
     		if(Float.parseFloat(max) < Float.parseFloat(min))
     			buf.append((String)model.getValueAt(i,0) + " max value must not less than min value.\n");
    	}
    	String err = buf.toString();
    	if(err != null && err.length() > 0){
    		JOptionPane.showMessageDialog(this, err, "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
    		return false;
    	}
    	return true;
    }
    
    private boolean isFloat(String val){
    	if(val == null || val.trim().length() == 0)
    		return false;
    	try{
    		Float.parseFloat(val);
    	}catch(NumberFormatException e){
    		return false;
    	}
    	return true;
    }
    
    private void repaintModel(){
    	if(propertyInfoTable.getCellEditor() != null)
    		propertyInfoTable.getCellEditor().stopCellEditing();
    	if(parentDialog != null){
    		//if(mode == ViewModelDialog.MODEL_BEFORE)
    			parentDialog.getModelBeforeInternalFrame().refreshModelCanvas(parentDialog.getVerticalScale());
    		//else if(mode == ViewModelDialog.MODEL_AFTER)
    			parentDialog.modelAfterPropertyComboBoxActionPerformed();
    	}
    }
    
    private void applyActionPerformed(java.awt.event.ActionEvent evt) {
    	if(!validateEdit())
    		return;
    	commitChange();
    }

    private void commitChange(){
    	
    	TableModel model = propertyInfoTable.getModel();
    	for(int i = 0; i < model.getRowCount(); i++){
    		String propertyName = (String)model.getValueAt(i, 0);
    		Map<String,Property> cmap = colormap.get(modelName);
    		Property prop = cmap.get(propertyName);
    		String min = model.getValueAt(i, 1).toString();
    		String max = model.getValueAt(i, 2).toString();
   			prop.setMin(Float.parseFloat(min));
   			prop.setMax(Float.parseFloat(max));
    	
   			if(!modifiedPropertyColorbar.isEmpty() && modifiedPropertyColorbar.containsKey(propertyName)){
	    		String selected = modifiedPropertyColorbar.get(propertyName);
	    		
	    		if(availableColorbar.containsKey(selected)){
					Colorbar1 bar = availableColorbar.get(selected);
					com.bhpb.deliverylite.colorbar.ColorbarUtil.labeledColorbar(bar, prop.getMin(), prop.getMax());
					prop.setColorbar(bar);
	    		}else{
					ClassLoader classLoader = this.getClass().getClassLoader();
					ColorMap colorMap = null;
              	    try{
              		   colorMap = ColorbarUtil.getColorMapByResourceName(selected);
              	    }catch(IOException e){
              		   e.printStackTrace();
              		   logger.warning("IO problem occurred while getting colormap object by using colorbar name=" + selected);
              	    }
					Colorbar1 colorbar = new Colorbar1(selected,colorMap);
					availableColorbar.put(selected, colorbar);
					Colorbar1 bar = new Colorbar1(colorbar);
					com.bhpb.deliverylite.colorbar.ColorbarUtil.labeledColorbar(bar, prop.getMin(), prop.getMax());
					prop.setColorbar(bar);
				}
	    		modifiedPropertyColorbar.remove(selected);
    		}else{
    			Colorbar1 bar = prop.getColorbar();
    			com.bhpb.deliverylite.colorbar.ColorbarUtil.labeledColorbar(bar, prop.getMin(), prop.getMax());
    			prop.setColorbar(bar);
    		}
    		colormap.get(modelName).put(propertyName, prop);
            String name = "";
            String baseName = parentDialog.getParentGui().getOutputBasename();
            if(modelName.startsWith( baseName + "_post_"))
                name = modelName.replace("_post_", "_prior_");
            else if(modelName.startsWith( baseName + "_prior_"))
                name = modelName.replace("_prior_", "_post_");
            if(colormap.containsKey(name))
                colormap.get(name).put(propertyName, prop);
    		repaintModel();
    	}
    }
    
    private void colorbarComboBoxActionPerformed(java.awt.event.ActionEvent evt){
    	String selected = (String)colorbarComboBox.getSelectedItem();
    	if(selectedRow != -1){
    		String propertyName = (String)propertyInfoTable.getValueAt(selectedRow, 0);
    		modifiedPropertyColorbar.put(propertyName,selected);
    		applyActionPerformed(evt);
    	}
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                final PropertyColorbarEditDialog dialog = new PropertyColorbarEditDialog((Dialog)null, 0, null, null, true, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        dialog.dispose();
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify
    private javax.swing.JButton okButton;
    private javax.swing.JButton applyButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTable propertyInfoTable;
    private javax.swing.JScrollPane propertyInfoTableScrollPane;
    private JComboBox colorbarComboBox;
    // End of variables declaration
    private static class ButtonRenderer extends JButton implements TableCellRenderer {
    	public ButtonRenderer() {
    		setOpaque(true);
    	}

    	public Component getTableCellRendererComponent(JTable table, Object value,
    			boolean isSelected, boolean hasFocus, int row, int column) {
    		if (isSelected) {
    			setForeground(table.getSelectionForeground());
    			setBackground(table.getSelectionBackground());
    		} else {
    			setForeground(table.getForeground());
    			setBackground(UIManager.getColor("Button.background"));
    		}
    		//setText((value == null) ? "" : value.toString());
    		setText("Edit");
    		return this;
    	}
    }
    private static class ButtonEditor extends DefaultCellEditor {  
    	protected JButton button;  
    	private String    label;  
    	private boolean   isPushed;  
    	/**	
    	 * keyed by data set name (model before called "Input Model")
    	 * value keyed by property name
    	 */
    	private Map<String,Map<String,Property>> propertyMap;
    	private Map<String,Colorbar1> colorbarMap;
    	private JComboBox parentCombo;
    	private Map<String,String> modifiedMap;
    	private PropertyColorbarEditDialog parentGui;
    	private String modelName;
    	private JTable table;
    	private int row = 0;
    	private int col = 0;
    	public ButtonEditor(PropertyColorbarEditDialog parent, JTable tab, String mName, Map<String,Map<String,Property>> propMap,Map<String,Colorbar1> availableColorbars,Map<String,String> modified,JComboBox combo) {    
    		super(combo);   
    		this.table = tab;
    		this.modelName = mName;
    		button = new JButton("Edit");  
    		this.propertyMap = propMap;
    		this.colorbarMap = availableColorbars;
    		this.parentCombo = combo;
    		this.parentGui = parent;
    		this.modifiedMap = modified;
    		button.setOpaque(true);
    		button.setEnabled(true);
    		//final JTable fTable = table; 
    		//final Map<String,Property> fpropertyMap = propertyMap;
    		final JTable ftable = table;
    		button.addActionListener(new ActionListener() {      
    			public void actionPerformed(ActionEvent e) {    
    				String propertyName = (String)ftable.getValueAt(row, 0);
    				System.out.println("propertyName=" + propertyName);
    				Map<String,Property> cmap = propertyMap.get(modelName);
    				Property prop =  cmap.get(propertyName);
    				Colorbar1 colorbar = prop.getColorbar();
    				if(colorbar != null){
    					ColorMap colorMap = colorbar.getColorMap();
    					ColorMap colorMapOld = new ColorMap(colorMap);
    					ColorMapEditor cmEditor = new ColorMapEditor(0,0,colorMapOld);
    					cmEditor.setModal(true);
    			        cmEditor.setAlwaysOnTop(true);
    			        cmEditor.setVisible(true);
    			        ColorMap resultCMap = cmEditor.getColorMap();
    			        if(!resultCMap.equals(colorMap)){
    			        	//Set<String> set = colorbarMap.keySet();
    			        	Object[] array2 = colorbarMap.keySet().toArray();
    			        	Arrays.sort(array2);
    			        	Object [] array = new Object[array2.length+2];
    			        	//Object [] array2 = set.toArray();
    			        	for(int i = 0; i < array2.length; i++)
    			        		array[i+1] = array2[i];
    			        	array[0] = "The following are current list of available colorbar names.";
    			        	array[array.length-1] = "Please enter a new colorbar name.";
    			        	Object response = JOptionPane.showInputDialog(parentGui,
    			        			array, "Enter New Colorbar Name",
    				            JOptionPane.PLAIN_MESSAGE);
    				        System.out.println("Response: " + response);
    				        while(response != null && colorbarMap.containsKey(response) && colorbar.isResourceBased()){
    				        	JOptionPane.showMessageDialog(parentGui, "The colorbar name you entered already exists and is predefined. Please enter another name.", "Colorbar Name Already Exists", JOptionPane.WARNING_MESSAGE);
    				        	response = JOptionPane.showInputDialog(parentGui,
        			        			array, "Enter New Colorbar Name",
        				            JOptionPane.PLAIN_MESSAGE);
    				        }
    				        if(response != null){
    				        	//if(colorbar.isResourceBased()){
    				        	//	availableColorbarNames.add((String)response);
    				        	//	Collections.sort(availableColorbarNames);
    				        	//}else{
    				        		if(colorbarMap.containsKey(response) && !colorbar.isResourceBased()){
    				        			int n = JOptionPane.showConfirmDialog(
    				        					parentGui, "The colorbar name already exists and can be overwritten. Do you want to overwrite it?",
    				    	                    "Confirm To Proceed",
    				    	                    JOptionPane.YES_NO_OPTION);
    				    	            if(n == JOptionPane.NO_OPTION){
    				    	            	return;
    				    	            }
    				        		}
    				        		//int idx = availableColorbarNames.indexOf(response);
    				        		//availableColorbarNames.set(idx, (String)response);
    				        	//}
	    				        //colorbar.setColorMap(new ColorMap(resultCMap));
	    				        //colorbar.setResourceBased(false);
	    				        //colorbar.setName((String)response);
	    				        Colorbar1 cb = new Colorbar1((String)response,resultCMap);
	    				        cb.setResourceBased(false);
	    				        colorbarMap.put((String)response, cb);
	    				        
	    				        Object[] keys = colorbarMap.keySet().toArray();
	    				        Arrays.sort(keys);
	    				        ComboBoxModel model = new DefaultComboBoxModel(keys);
    				        	parentCombo.setModel(model);
    				        	model.setSelectedItem(response);
    				        	table.setValueAt(response, row, col-1);
    				        	parentCombo.setSelectedItem(response);
	    				        modifiedMap.put(prop.getName(),(String)response);
	    				        prop.setColorbar(cb);
	    				        applyToOtherProperties(response,cb);
	    				        
    				        }
    			        }
    				}

    				fireEditingStopped();  
    			}
    			
    			private void applyToOtherProperties(Object key, Colorbar1 cb){
    				for (Map.Entry<String, Map<String,Property>> e1 : propertyMap.entrySet()){
    				//Set set = propertyMap.keySet();
    				//Iterator it = set.iterator();
    				//while(it.hasNext()){
    					//Object o = it.next();
    					Map<String,Property> pmap = e1.getValue();
    					for (Map.Entry<String, Property> e : pmap.entrySet()){
    					//Set set1 = pmap.entrySet();
    					//Iterator it1 = set1.iterator();
    					//while(it1.hasNext()){
        					//Object o1 = it1.next();
        					Property p = e.getValue();
        					Colorbar1 c = p.getColorbar();
        					if(c != null && c.getName().equals(key)){
        						Colorbar1 cb1 = new Colorbar1(cb);
        						cb1.setLabels(c.getLabels());
        						p.setColorbar(cb1);
        					}
    					}
    				}
    				parentGui.repaintModel();
    			}
    		});  
    	}
    	public Component getTableCellEditorComponent(JTable table, Object value,  boolean isSelected, int row, int column) {    
    		if (isSelected) {      
    			button.setForeground(table.getSelectionForeground());      
    			button.setBackground(table.getSelectionBackground());    
    		} else{      
    			button.setForeground(table.getForeground());      
    			button.setBackground(table.getBackground());    
    		}    
    		//label = (value ==null) ? "" : value.toString();    
    		button.setText("Edit");    
    		isPushed = true;
    		this.row = row;
    		this.col = column;
    		return button;  
    	}   
    	public Object getTableCellValue(int row, int col) {    
    		if (isPushed)  {
    			return table.getCellEditor(row, col).getCellEditorValue();
    		}
    		return null;
    	}
    }
}
