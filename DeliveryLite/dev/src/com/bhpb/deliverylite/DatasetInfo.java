/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.qiworkbench.api.IDataObject;


public class DatasetInfo {
	protected String name;
	protected int stackType;//near or far
	protected int datasetType;  //real, synthetic before, or synthetic after
	private GeoFileMetadata metadata;
	private IDataObject[] data;
	 
	public DatasetInfo(String name, int stackType, int datasetType){
		this.name = name;
		this.stackType = stackType;
		this.datasetType = datasetType;
	}
	public void setMetadata(GeoFileMetadata metadata){
		this.metadata = metadata;
	}
	public GeoFileMetadata getMetadata(){
		return metadata;
	}
	public void setDataObjects(IDataObject[] data){
		this.data = data;
	}
	public IDataObject[] getDataObjects(){
		return data;
	}
}
