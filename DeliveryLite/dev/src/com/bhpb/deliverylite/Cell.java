/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.text.NumberFormat;

public	class Cell{
	private Point p;
	private Dimension d;
	private Color color;
	private float value;
	private float time;
	private float sigmaTime;
	private float pixelPerUnit = 0;
	private boolean showProperty = true;
	private boolean showTime = true;
	private boolean showSigmaTime = true;
	private String name;
	private boolean isTop = false;
	private static NumberFormat format = NumberFormat.getInstance();
	//public Cell(Point p, Dimension d, Color color, double time, double sigmatime, double value, double err, double unit){
	public Cell(Point p, Dimension d, Color color, float time, float sigmatime, String name, float value, float unit){		
		this.p = p;
		this.d = d;
		this.color = color;
		this.value = value;
		this.time = time;
		this.sigmaTime = sigmatime;
		pixelPerUnit = unit;
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public void setPixelPerUnit(float unit){
		pixelPerUnit = unit;
	}
	
	public void setTop(boolean bool){
		isTop = bool;
	}
	
	public void draw(Graphics g){
		Font oldfont = g.getFont();
		g.setColor(Color.black);
		g.draw3DRect(p.x, p.y, d.width, d.height, true);
		g.setColor(color);
		g.fill3DRect(p.x, p.y, d.width, d.height, true);
		g.setColor(Color.black);
		FontMetrics fm = g.getFontMetrics();
		
		//Format format = new Format("%.1f");
		format.setMaximumFractionDigits(1);
		format.setMinimumFractionDigits(1);
		int width = fm.stringWidth(String.valueOf(value));
		int px = p.x + (d.width - width)/2;
		int py = p.y + d.height/2 + fm.getAscent()/2;
		if(showProperty){
			Font font = new Font(oldfont.getFontName(),Font.BOLD, oldfont.getSize()+2);
			g.setFont(font);
			g.drawString(String.valueOf(value), px, py);
		}
		//g.drawString(String.valueOf(0), p.x + d.width+4, p.y+fm.getAscent()/2);
		g.setFont(oldfont);
		int h = (int)Math.round(sigmaTime * pixelPerUnit);
		if(showTime){
			if(!isTop)
				g.drawString(String.valueOf(format.format(time)), p.x + d.width+4, p.y + fm.getAscent()/2);
			else
				g.drawString(String.valueOf(format.format(time)), p.x + d.width+4, p.y + fm.getAscent()-2);
		}
		width = fm.stringWidth(format.format(sigmaTime));
		int sigx = p.x+d.width-width/2;
		g.drawLine(sigx, p.y + d.height,sigx,p.y + d.height - h);
		if(showSigmaTime)
			g.drawString(format.format(sigmaTime), sigx-width/2, p.y + d.height - h - fm.getDescent());
	}

	public void setShowProperty(boolean b){
		showProperty = b;
	}
	
	public void setShowSigmaTime(boolean b){
		showSigmaTime = b;
	}

	public void setShowTime(boolean b){
		showTime = b;
	}	
	
	public Color getColor(){
		return color;
	}
	
	public Point getP(){
		return p;
	}

	public Dimension getD(){
		return d;
	}

	public void setP(Point p){
		this.p = p;
	}
	
	public void setD(Dimension d){
		this.d = d;
	}
	
	public float getValue(){
		return value;
	}
	
	public float getSigmaTime(){
		return sigmaTime;
	}
	
	public void setValue(float value){
		this.value = value;
	}

	public float getTime(){
		return time;
	}
	
	public void setTime(float time){
		this.time = time;
	}
	
}