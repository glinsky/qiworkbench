/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.TableModel;

import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;

/**
 * A data structure containing information about the type of model dataset with a number of properties
 * per trace. Each property contains an array of values depending on the number of layers specified in
 * the dataset. 
 *  
 * @author 
 *
 */
public class ModelDataset {
	
	/**
	 * metadata containing information about the type of dataset "PROPERTY", number of layers, 
	 * number of properties; each property has values across a number of layers, ep, cdp and tracl
	 * tracl represents layer indexes.
	 */
	private ModelFileMetadata metadata;
	
	/**
	 * properties is keyed by the property name, the value are the property object
	 */
	private Map<String,Property> properties;
	
	/**
	 * Represent the time boundries for the dataset; its size is dependent on the number of layers
	 * specified in the dataset equals to the number of layers plus one.
	 */
	private float[] boundries;
	
	/**
	 * Mainly used for post model dataset where data are generated after inversion process
	 * @param metadata 
	 * @param data   trace data
	 */
	public ModelDataset(ModelFileMetadata metadata, float[] data){
		this.metadata = metadata;
		populateModelDataFromDataset(metadata, data);
		
	}
	
	/**
	 * Mainly used by the input model the user construct in the table
	 * @param model  the underlying input table model
	 * @param boundries  time field in the table
	 */
	public ModelDataset(TableModel model, float[] boundries){
		if(boundries != null)
			this.boundries = boundries.clone();
		
		populateModelDataFromTableModel(model);
	}
	
	public String getDatasetName(){
		return metadata.getGeoFileName();
	}

	public int getNumberOfLayer(){
		if(metadata != null)
			return metadata.getNumLayers();
		else if(boundries != null){
			return boundries.length;
		}
		return 0;
	}
	
	public ModelFileMetadata getMetadata(){
		return metadata;
	}
	
	/**
	 * Handle propery data from the model after inversion mainly from
	 * model dataset
	*/
	private synchronized void populateModelDataFromDataset(ModelFileMetadata metadata, float[] data){
		if(metadata == null || data == null)
			return;
		int numLayers = metadata.getNumLayers();
		List<String> propNames = metadata.getProperties();
		properties = new HashMap<String,Property>();
		float[] fs = new float[numLayers];
		boundries = new float[numLayers+1];  //boundry data always has a size of number of layers plus one
		int j = 0;
		int k = 0;
		int totalNumberPropertyValues = metadata.getNumProps()*numLayers;
		Map<String,Property> map = getPropertyMapClone();
		for(int i = 0; i < data.length; i++){
			if(i < totalNumberPropertyValues)
				fs[j++] = data[i];
			if(j == numLayers && i < totalNumberPropertyValues){
				Property prop = map.get(propNames.get(k));
                                if (prop == null) {     // this is an error condition!
                                  String nm = propNames.get(k);
        System.err.println("WARNING : prop is null because no property for "+nm+" exists.");
        System.err.println("WARNING : creating prop " + nm + " in DeliveryLiteConstants.default_property_range");
        System.err.println("WARNING : See DeliveryLiteConstants.java:304      JE FIELD");
                                  prop = new Property(nm);
                                  map.put(nm,prop);
                                }
				prop.setValues(fs);
				
				if(prop.getMin() == Float.MIN_VALUE && prop.getMax() == Float.MAX_VALUE){
					prop.setMin(getMin(fs,prop.isIgnoreZero()));
					prop.setMax(getMax(fs,prop.isIgnoreZero()));
				}else if(prop.getMin() != Float.MIN_VALUE && prop.getMax() == Float.MAX_VALUE){
					prop.setMax(getMax(fs,prop.isIgnoreZero()));
				}
				properties.put(prop.getName(),prop);
				k++;
				j = 0;
				fs = new float[numLayers];
			}else if(i >= totalNumberPropertyValues){
				boundries[j++] = data[i];
			}
		}
		
	}
	
	private Map<String,Property> getPropertyMapClone(){
		Map<String,Property> map = new HashMap<String,Property>();
		for(String key : DeliveryLiteConstants.default_property_range.keySet()){
			Property property = DeliveryLiteConstants.default_property_range.get(key);
			map.put(key, property.clone());
		}
		return map;
	}
	
	/**
	 * Fill propery data from the input layer table (before inversion) into a property Map keyed by the table column name
	 * Resolved min and max for each property based on the rules specified by the DeliveryLiteConstants.default_property_range
	*/
	private void populateModelDataFromTableModel(TableModel model){
		if(model == null || model.getColumnCount() == 0  || model.getRowCount() == 0)
			return;
		properties = new HashMap<String,Property>();
		int numberLayers = model.getRowCount();
		Map<String,Property> map = getPropertyMapClone();
		for(int i = 0; i < model.getColumnCount(); i++){
			String propName = model.getColumnName(i);
			Property prop = map.get(propName);
			float[] fs = new float[numberLayers];
			for(int j = 0; j < numberLayers; j++){
				String s = (String)model.getValueAt(j, i);
				float fval = 0;
				if(s != null && s.trim().length() > 0){
					try{
						fval = Float.parseFloat(s);
					}catch(NumberFormatException e){
						e.printStackTrace();
					}
				}
				fs[j] = fval; 
			}
			prop.setValues(fs);
			if(prop.getMin() == Float.MIN_VALUE && prop.getMax() == Float.MAX_VALUE){
				prop.setMin(getMin(fs,prop.isIgnoreZero()));
				prop.setMax(getMax(fs,prop.isIgnoreZero()));
			}else if(prop.getMin() != Float.MIN_VALUE && prop.getMax() == Float.MAX_VALUE){
				prop.setMax(getMax(fs,prop.isIgnoreZero()));
			}
			properties.put(prop.getName(),prop);
		}
	}
	
	public Property getPropertyByName(String key){
		if(properties.containsKey(key))
			return properties.get(key);
		return null;
	}
	
	/**
	 * Find the minimum value out of the given array of data.
	 * @param data  the given array of data
	 * @param ignoreZero  if the value of zero should be ignored during searching
	 * @return float
	 */
	public static float getMin(float[] data, boolean ignoreZero){
		float min = data[0];
		if(ignoreZero)
			min = Float.MAX_VALUE;
		for(float f : data){
			if(ignoreZero && f == 0f)
				continue;
				
			if(f <= min)
				min = f;
		}
		if(min == Float.MAX_VALUE)
			return data[0];
		return min;
	}
	
	public float[] getBoundries(){
		if(boundries != null)
			return boundries.clone();
		else
			return null;
	}

	/**
	 * Find the maximum value out of the given array of data.
	 * @param data  the given array of data
	 * @param ignoreZero  if the value of zero should be ignored during searching
	 * @return float
	 */
	public static float getMax(float[] data, boolean ignoreZero){
		float max = data[0];
		if(ignoreZero)
			max = Float.MIN_VALUE;
		for(float f : data){
			if(ignoreZero && f == 0f)
				continue;
				
			if(f >= max)
				max = f;
		}
		if(Math.abs(Math.abs(max)-Math.abs(Float.MIN_VALUE)) < 0.0000001)
			return data[0];
		return max;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
