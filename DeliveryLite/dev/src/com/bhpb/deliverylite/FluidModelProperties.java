/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.deliverylite;

import com.bhpb.util.Util;

/**
 * A data structure that store fluids information that appears on the table UI to assist
 * the validation process when user enter invalid value to the table cell.
 * @author 
 */
public class FluidModelProperties {
    private Fluid brine = new Fluid(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME,DeliveryLiteConstants.defaultFluidBrineVp,
            DeliveryLiteConstants.defaultFluidBrineRho,Float.MAX_VALUE,Float.MAX_VALUE);
    private Fluid oil = new Fluid(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME,DeliveryLiteConstants.defaultFluidOilVp,
            DeliveryLiteConstants.defaultFluidOilRho,Float.MAX_VALUE,Float.MAX_VALUE);
    private Fluid gas = new Fluid(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME,DeliveryLiteConstants.defaultFluidGasVp,
            DeliveryLiteConstants.defaultFluidGasRho,Float.MAX_VALUE,Float.MAX_VALUE);
    private Saturation oilSaturation = new Saturation(FLUID_TYPE.OIL,DeliveryLiteConstants.defautlOilSaturation,0);
    private Saturation highGasSaturation = new Saturation(FLUID_TYPE.HIGH_GAS,DeliveryLiteConstants.defautlHigGasSaturation,0);
    private Saturation lowGasSaturation = new Saturation(FLUID_TYPE.LOW_GAS,DeliveryLiteConstants.defautlLowGasSaturation,0);
    public enum FLUID_TYPE  { OIL, HIGH_GAS, LOW_GAS };
    public FluidModelProperties(){}

    /**
     * Copy constructor
     */
    public FluidModelProperties(FluidModelProperties model){
        brine = new Fluid(model.getBrineName(),model.getBrineVp(),model.getBrineRho(),model.getBrineSigmaVp(),model.getBrineSigmaRho());
        oil = new Fluid(model.getOilName(),model.getOilVp(),model.getOilRho(),model.getOilSigmaVp(),model.getOilSigmaRho());
        gas = new Fluid(model.getGasName(),model.getGasVp(),model.getGasRho(),model.getGasSigmaVp(),model.getGasSigmaRho());
        oilSaturation = new Saturation(FLUID_TYPE.OIL,model.getOilSaturation(), model.getOilSigmaSaturation());
        highGasSaturation = new Saturation(FLUID_TYPE.HIGH_GAS, model.getHighGasSaturation(), model.getHighGasSigmaSaturation());
        lowGasSaturation = new Saturation(FLUID_TYPE.LOW_GAS,model.getLowGasSaturation(), model.getLowGasSigmaSaturation());
    }

    public String getBrineName(){
        return brine.getName();
    }

    public String getOilName(){
        return oil.getName();
    }

    public String getGasName(){
        return gas.getName();
    }

    public float getBrineVp(){
        return brine.getVp();
    }

    public float getBrineRho(){
        return brine.getRho();
    }

    public float getBrineSigmaVp(){
        return brine.getSigmaVp();
    }

    public float getBrineSigmaRho(){
        return brine.getSigmaRho();
    }

    public float getOilVp(){
        return oil.getVp();
    }

    public float getOilRho(){
        return oil.getRho();
    }

    public float getOilSigmaVp(){
        return oil.getSigmaVp();
    }

    public float getOilSigmaRho(){
        return oil.getSigmaRho();
    }

    public float getGasVp(){
        return gas.getVp();
    }

    public float getGasRho(){
        return gas.getRho();
    }

    public float getGasSigmaVp(){
        return gas.getSigmaVp();
    }

    public float getGasSigmaRho(){
        return gas.getSigmaRho();
    }

    public float getOilSaturation(){
        return oilSaturation.getSaturation();
    }

    public float getSaturation(FLUID_TYPE type){
        switch(type){
            case OIL:
                return oilSaturation.getSaturation();
            case HIGH_GAS:
                return highGasSaturation.getSaturation();
            case LOW_GAS:
                return lowGasSaturation.getSaturation();
            default:
                return 0f;
        }
    }

    public void setSigmaSaturation(FLUID_TYPE type, float val){
        switch(type){
            case OIL:
                oilSaturation.setSigmaSaturation(val);
                break;
            case HIGH_GAS:
                highGasSaturation.setSigmaSaturation(val);
                break;
            case LOW_GAS:
                lowGasSaturation.setSigmaSaturation(val);
                break;
            default:
                return;
        }
    }

    public float getSigmaSaturation(FLUID_TYPE type){
        switch(type){
            case OIL:
                return oilSaturation.getSigmaSaturation();
            case HIGH_GAS:
                return highGasSaturation.getSigmaSaturation();
            case LOW_GAS:
                return lowGasSaturation.getSigmaSaturation();
            default:
                return 0f;
        }
    }

    public void setSaturation(FLUID_TYPE type, float val){
        switch(type){
            case OIL:
                oilSaturation.setSaturation(val);
                break;
            case HIGH_GAS:
                highGasSaturation.setSaturation(val);
                break;
            case LOW_GAS:
                lowGasSaturation.setSaturation(val);
                break;
            default:
                return;
        }
    }    
    public float getOilSigmaSaturation(){
        return oilSaturation.getSigmaSaturation();
    }

    public float getHighGasSaturation(){
        return highGasSaturation.getSaturation();
    }

    public float getHighGasSigmaSaturation(){
        return highGasSaturation.getSigmaSaturation();
    }

    public float getLowGasSaturation(){
        return lowGasSaturation.getSaturation();
    }

    public float getLowGasSigmaSaturation(){
        return lowGasSaturation.getSigmaSaturation();
    }

    public void setBrineVp(float vp){
         brine.setVp(vp);
    }

    public void setBrineRho(float rho){
        brine.setRho(rho);
    }

    public void setBrineSigmaVp(float val){
        brine.setSigmaVp(val);
    }

    public void setBrineSigmaRho(float val){
        brine.setSigmaRho(val);
    }

    public void setOilVp(float val){
        oil.setVp(val);
    }

    public void setOilRho(float val){
        oil.setRho(val);
    }

    public void setOilSigmaVp(float val){
        oil.setSigmaVp(val);
    }

    public void setOilSigmaRho(float val){
        oil.setSigmaRho(val);
    }

    public void setGasVp(float val){
        gas.setVp(val);
    }

    public void setGasRho(float val){
        gas.setRho(val);
    }

    public void setGasSigmaVp(float val){
        gas.setSigmaVp(val);
    }

    public void setGasSigmaRho(float val){
        gas.setSigmaRho(val);
    }

    public void setOilSaturation(float val){
        oilSaturation.setSaturation(val);
    }

    public void setOilSigmaSaturation(float val){
        oilSaturation.setSigmaSaturation(val);
    }

    public void setHighGasSaturation(float val){
        highGasSaturation.setSaturation(val);
    }

    public void setHighGasSigmaSaturation(float val){
        highGasSaturation.setSigmaSaturation(val);
    }

    public void setLowGasSaturation(float val){
        lowGasSaturation.setSaturation(val);
    }

    public void setLowGasSigmaSaturation(float val){
        lowGasSaturation.setSigmaSaturation(val);
    }

    public Object[][] toTableArray(){
        Object[][] params = new Object[2][9];
        if(brine.getVp() == Float.MAX_VALUE)
            params[0][0] = "";
        else
            params[0][0] = Float.toString(brine.getVp());

        if(brine.getSigmaVp() == Float.MAX_VALUE)
            params[1][0] = "";
        else
            params[1][0] = Float.toString(brine.getSigmaVp());

        if(brine.getRho() == Float.MAX_VALUE)
            params[0][1] = "";
        else
            params[0][1] = Float.toString(brine.getRho());

        if(brine.getSigmaRho() == Float.MAX_VALUE)
            params[1][1] = "";
        else
            params[1][1] = Float.toString(brine.getSigmaRho());

        if(oil.getVp() == Float.MAX_VALUE)
            params[0][2] = "";
        else
            params[0][2] = Float.toString(oil.getVp());

        if(oil.getSigmaVp() == Float.MAX_VALUE)
            params[1][2] = "";
        else
            params[1][2] = Float.toString(brine.getSigmaVp());

        if(oil.getRho() == Float.MAX_VALUE)
            params[0][3] = "";
        else
            params[0][3] = Float.toString(oil.getRho());

        if(oil.getSigmaRho() == Float.MAX_VALUE)
            params[1][3] = "";
        else
            params[1][3] = Float.toString(oil.getSigmaRho());

        if(gas.getVp() == Float.MAX_VALUE)
            params[0][4] = "";
        else
            params[0][4] = Float.toString(gas.getVp());

        if(gas.getSigmaVp() == Float.MAX_VALUE)
            params[1][4] = "";
        else
            params[1][4] = Float.toString(gas.getSigmaVp());

        if(gas.getRho() == Float.MAX_VALUE)
            params[0][5] = "";
        else
            params[0][5] = Float.toString(gas.getRho());

        if(gas.getSigmaRho() == Float.MAX_VALUE)
            params[1][5] = "";
        else
            params[1][5] = Float.toString(gas.getSigmaRho());

        float val = oilSaturation.getSaturation();
        if(Util.floatEqualZero(val))
            params[0][6] = "";
        else
            params[0][6] = Float.toString(val);

        val = oilSaturation.getSigmaSaturation();
        if(Util.floatEqualZero(val))
            params[1][6] = "";
        else
            params[1][6] = Float.toString(val);

        val = highGasSaturation.getSaturation();
        if(Util.floatEqualZero(val))
            params[0][7] = "";
        else
            params[0][7] = Float.toString(val);

        val = highGasSaturation.getSigmaSaturation();
        if(Util.floatEqualZero(val))
            params[1][7] = "";
        else
            params[1][7] = Float.toString(val);

        val = lowGasSaturation.getSaturation();
        if(Util.floatEqualZero(val))
            params[0][8] = "";
        else
            params[0][8] = Float.toString(val);

        val = lowGasSaturation.getSigmaSaturation();
        if(Util.floatEqualZero(val))
            params[1][8] = "";
        else
            params[1][8] = Float.toString(val);

        return params;

    }

    private static class Fluid{
        String name;
        float vp;
        float rho;
        float sigmaVp;  //if value = Float.MAX_VALUE represents empty or blank
        float sigmaRho;//if value = Float.MAX_VALUE represents empty or blank

        public Fluid(String name, float vp, float rho, float sigmaVp, float sigmaRho){
            this.vp = vp;
            this.rho = rho;
            this.sigmaVp = sigmaVp;
            this.sigmaRho = sigmaRho;
        }

        public String getName(){
            return name;
        }

        public float  getVp(){
            return vp;
        }
        public float getRho(){
            return  rho;
        }
        public float  getSigmaVp(){
            return sigmaVp;
        }
        public float getSigmaRho(){
            return  sigmaRho;
        }
        public void setName(String name){
            this.name = name;
        }

        public void setVp(float vp){
            this.vp = vp;
        }
        public void setRho(float rho){
            this.rho = rho;
        }
        public void setSigmaVp(float sigmaVp){
            this.sigmaVp = sigmaVp;
        }
        public void setSigmaRho(float sigmaRho){
            this.sigmaRho = sigmaRho;
        }
    }

    private static class Saturation{
        private float saturation;
        private float sigmaSaturation; //0 represents empty or blank
        private FLUID_TYPE fluidType;
        public Saturation(FLUID_TYPE fluidType, float saturation, float sigmaSaturation){
            this.saturation = saturation;
            this.sigmaSaturation = sigmaSaturation;
            this.fluidType = fluidType;
        }

        public FLUID_TYPE getFluidType(){
            return fluidType;
        }
        public float  getSaturation(){
            return saturation;
        }
        public float getSigmaSaturation(){
            return  sigmaSaturation;
        }

        public void setSaturation(float saturation){
            this.saturation = saturation;
        }

        public void setSigmaSaturation(float sigmaSaturation){
            this.sigmaSaturation = sigmaSaturation;
        }
    }
}