/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.colorbar;

import java.util.List;


import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bhpb.geographics.util.ColorMap;

public class Colorbar1 {
	private ColorMap colorMap;
	private List<String> labels;
	private String name = "";  //if the colorbar object from the resource file, then there is a name associated it
	private boolean resourceBased = true; //if this object is built from a default resource file (e.g. rainbow.cmp)
	
	public Colorbar1(){}
		
	public Colorbar1(ColorMap colorMap){
		this.colorMap = colorMap;
	}

	public Colorbar1(String name, ColorMap colorMap){
		this.name = name;
		this.colorMap = colorMap;
	}	
	
	public Colorbar1(Colorbar1 colorbar){
		this.name = colorbar.getName();
		this.resourceBased = colorbar.resourceBased;
		this.colorMap = new ColorMap(colorbar.getColorMap());
	}	
	
	public void setColorMap(ColorMap colorMap){
		this.colorMap = colorMap;
	}
	
	public String genState(){
    	StringBuffer content = new StringBuffer();
    	content.append("<" + getXmlAlias() + " name=\"" + name + "\" resourceBased=\"" + resourceBased + "\">\n");
		content.append(colorMap.genState());
		content.append("</" + getXmlAlias() + ">");
		return content.toString();
	}

	public static String getXmlAlias() {
	    return "colorbar";
	}	
	
	public static Colorbar1 restoreState(Node node){
		if(node == null)
			throw new IllegalArgumentException("Colorbar xml node can not be null");
		if(!node.getNodeName().equals("colorbar"))
			throw new IllegalArgumentException("Colorbar node name must be \"colorbar\"");
		Element el = (Element)node;
		Colorbar1 cb = new Colorbar1();
		String temp = el.getAttribute("name");
		cb.setName(temp);
		temp = el.getAttribute("resourceBased");
		if(temp != null && temp.trim().length() > 0){
			boolean b = Boolean.valueOf(temp).booleanValue();
			cb.setResourceBased(b);
		}
		NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
            	if(child.getNodeName().equals(ColorMap.getXmlAlias())){
            		ColorMap colorMap = ColorMap.restoreState(child);
            		cb.setColorMap(colorMap);
            	}
            }
        }
		return cb;
		
	}
	
	public ColorMap getColorMap(){
		return colorMap;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public boolean isResourceBased(){
		return resourceBased;
	}
	
	public void setResourceBased(boolean b){
		resourceBased = b;
	}
	
	public List<String> getLabels(){
		return labels;
	}

	public void setLabels(List<String> labels){
		this.labels = labels;
	}
	
}
