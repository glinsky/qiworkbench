/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.colorbar;

import java.awt.Color;

public class LabeledColor{
	private Color color;
	private boolean isLabeled = false;
	private String label; //string label showing the value represented by the color; could be empty
	public LabeledColor(Color color, String label){
		this.color = color;
		this.label = label;
		if(label != null && label.trim().length() > 0)
			isLabeled = true;
	}
	/*
	 * Copy constructor
	 */
	public LabeledColor(LabeledColor lc){
		this.label = lc.getLabel();
		if(label != null && label.trim().length() > 0)
			isLabeled = true;
		this.color = new Color(lc.getColor().getRed(),lc.getColor().getGreen(), lc.getColor().getBlue(), lc.getColor().getAlpha());
	}

	/*
	 * Getter and setter
	 */
	public LabeledColor(Color color){
		this(color,"");
	}
	public String getLabel(){
		return label;
	}
	
	public boolean isLabeled(){
		return isLabeled;
	}
	
	public Color getColor(){
		return color;
	}

	public void setColor(Color color){
		this.color = color;
	}	
	public void setLabel(String label){
		this.label = label;
		isLabeled = true;
	}
}

