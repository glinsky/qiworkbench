/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.colorbar;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.bhpb.geographics.util.ColorMap;


public class ColorbarUtil {
    private static NumberFormat format = NumberFormat.getInstance();
	/*
     * Label the given the colorbar based on the values between the min and max value represented by the color
     * @param colorbar  the colorbar to be labeled
     * @param min   the minimum value represted by the colorbar
     * @param max   the maximum value represted by the colorbar
     */
    public static void labeledColorbar(Colorbar1 colorbar, float min, float max){
    	format.setMaximumFractionDigits(1);
    	format.setMinimumFractionDigits(1);
    	Color[] list = colorbar.getColorMap().getColors(ColorMap.RAMP);
    	int size = list.length;
    	List<String> labels = new ArrayList<String>();
    	for(int i = 1; i <= size; i++){
    		float f = min;
    		String label = "";
    		if(max != min){
    			float rem = (max - min) / (size-1);
    			f = min + rem*(i-1);
    			label = format.format(f);
    		}else if(i == 1)
    			label = format.format(min);
    		labels.add(label);
    	}
    	colorbar.setLabels(labels);
    }
}
