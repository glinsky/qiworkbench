/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

public class NumericFieldParams {
	private float timeBase;
	private float sigmaTimeBase;
	private int masterDepthLayer;
	private float depthMasterLayer;
	private float errorMasterLayerDepth;
	private float syntheticSeismicStartTime;
	private float syntheticSeismicEndTime;
	private float spaghettiPlotStartTime;
	private float spaghettiPlotEndTime;
	private int numberRuns;
	private float nearNoiseRMS;
	private float farNoiseRMS;
	private float avoA;
	private float avoB;
	private float inline;
	private float xline;
	public NumericFieldParams(){}

	public void setNumberRuns(int number){
		this.numberRuns = number;
	}
	
	public int getNumberRuns(){
		return numberRuns;
	}
	
	public float getSyntheticSeismicStartTime(){
		return syntheticSeismicStartTime;
	}
	
	public void setSyntheticSeismicStartTime(float time){
		syntheticSeismicStartTime = time;
	}

	public float getSyntheticSeismicEndTime(){
		return syntheticSeismicEndTime;
	}
	
	public void setSyntheticSeismicEndTime(float time){
		syntheticSeismicEndTime = time;
	}
	
	public float getSpaghettiPlotStartTime(){
		return spaghettiPlotStartTime;
	}
	
	public void setSpaghettiPlotStartTime(float time){
		spaghettiPlotStartTime = time;
	}
	
	public float getSpaghettiPlotEndTime(){
		return spaghettiPlotEndTime;
	}
	
	public void setSpaghettiPlotEndTime(float time){
		spaghettiPlotEndTime = time;
	}
	
	public float getSingleInline(){
		return inline;
	}
	
	public void setSingleInline(float line){
		inline = line;
	}

	public float getSingleXline(){
		return xline;
	}
	
	public void setSingleXline(float line){
		xline = line;
	}
	
	public float getAvoA(){
		return avoA;
	}
	
	public void setAvoA(float avo){
		avoA = avo;
	}	

	public float getAvoB(){
		return avoB;
	}
	
	public void setAvoB(float avo){
		avoB = avo;
	}	
	
	public float getNearNoiseRMS(){
		return nearNoiseRMS;
	}
	
	public void setNearNoiseRMS(float rms){
		nearNoiseRMS = rms;
	}	

	public float getFarNoiseRMS(){
		return farNoiseRMS;
	}
	
	public void setFarNoiseRMS(float rms){
		farNoiseRMS = rms;
	}	

	
	public void setTimeBase(float timeBase){
		this.timeBase = timeBase;
	}
	
	public float getTimeBase(){
		return timeBase;
	}

	public void setSigmaTimeBase(float time){
		this.sigmaTimeBase = time;
	}
	
	public float getSigmaTimeBase(){
		return sigmaTimeBase;
	}

	public void setMasterDepthLayer(int layer){
		this.masterDepthLayer = layer;
	}
	
	public int getMasterDepthLayer(){
		return masterDepthLayer;
	}

	public void setDepthMasterLayer(float depth){
		this.depthMasterLayer = depth;
	}
	
	public float getDepthMasterLayer(){
		return depthMasterLayer;
	}

	public void setErrorMasterLayerDepth(float depth){
		this.errorMasterLayerDepth = depth;
	}
	
	public float getErrorMasterLayerDepth(){
		return errorMasterLayerDepth;
	}
	
	
}
