/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import com.bhpb.deliverylite.colorbar.Colorbar1;


public class Property implements Cloneable{
	private String name; //property name
	private float[] values;//list of values available for this property
	private float value;  //current value
	private float min;  //minimum value of this property
	private float max; //maximum value of this property
	private Colorbar1 colorbar;  //the colorbar object representing this property
	//a flag to indicate if value 0 should be ignored when detemining the min value or the max value of this property
	private boolean ignoreZero = false;
	
	public Property(String name, float min, float max, Colorbar1 colorbar){
		this(name,min,max,colorbar,false);
	}
	public Property(String name, float[] values, boolean ignore){
		this.name = name;
		if(values != null)
			this.values = values.clone();
		this.ignoreZero = ignore;
	}
	
	public Property(String name, float min, float max, Colorbar1 colorbar, boolean ignore){
		this.name = name;
		//this.value = value;
		this.min = min;
		this.max = max;
		this.colorbar = colorbar;
		this.ignoreZero = ignore;
	}	
        public Property(String name) {              // default property constructor   JE FIELD
          this(name,0.0f,1.0f,DeliveryLiteUI.getAvailableColorbars().get("rainbow"),true);
        }
	public void setColorbar(Colorbar1 colorbar){
		this.colorbar = colorbar;
	}
	
	public void setValues(float[] values){
		if(values != null)
			this.values = values.clone();
		
	}
	
	public float[] getValues(){
		if(values != null)
			return values.clone();
		else
			return null;
	}
	
	public Colorbar1 getColorbar(){
		return colorbar;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public float getValue(){
		return value;
	}
	
	public void setValue(float value){
		this.value = value;
	}

	public float getMin(){
		return min;
	}
	
	public void setMin(float min){
		this.min = min;
	}

	public float getMax(){
		return max;
	}
	
	public void setMax(float max){
		this.max = max;
	}
	
	public boolean isIgnoreZero(){
		return ignoreZero;
	}
	public Property clone(){
		try{
			super.clone();
		}catch(CloneNotSupportedException e){
			e.printStackTrace();
		}
		if(colorbar != null)
			return new Property(name, min, max, new Colorbar1(colorbar));
		else
			return new Property(name, min, max, null);
	}
	
}
