/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.Color;
import java.awt.Component;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/**
 * Validate table cell when the text field is out of focus or when user hit enter key.
 * If the invalid value found, restore it back to the most recent value.
 * @author 
 */

public class TableCellValidator
{
	private JTable table;
	private Component parentGui;
	private boolean blankAllowed = false;
	public TableCellValidator(DeliveryLiteUI parentGui, JTable table, boolean blankAllowed){
		this.table = table;
		this.parentGui = parentGui;
		this.blankAllowed = blankAllowed;
		run();
	}
	public void run()
	{
		for(int i = 0; i < table.getColumnModel().getColumnCount(); i++){
			TableColumn column = table.getColumnModel().getColumn(i);
			//column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
			//column.setCellRenderer(new MyCellRenderer(table.getDefaultRenderer(String.class),table.getModel()));
			column.setCellEditor(new MyCellEditor(table.getDefaultEditor(String.class),table.getModel()));
		}

	}

	private class MyCellEditor extends DefaultCellEditor{

		private Object value; //!! to hold pre-edited value  

		public MyCellEditor(TableCellEditor editor, TableModel model){
			super( new JTextField() );  
			setClickCountToStart(0);  
		}  
		
		public Object getCellEditorValue(){  
			try{  
				Object o = super.getCellEditorValue();
				String val = "";
				if(o != null)
					val = o.toString();
				if(val.trim().length() == 0 && blankAllowed)
					return val;
				float f = Float.parseFloat(super.getCellEditorValue().toString());  
				return String.valueOf(f);
			} catch (NumberFormatException e) {  
				JOptionPane.showMessageDialog(parentGui, "Invalid numeric value.");  
				return value;  //!! restore value to its original contents  
			}  
		}  

		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			this.value = value;  //!! initialize value      
			Component c = super.getTableCellEditorComponent(table, value, isSelected, row, column);  
			((JComponent) c).setBorder(new LineBorder(Color.BLACK));  
			return c;  
		}
	}
	
}