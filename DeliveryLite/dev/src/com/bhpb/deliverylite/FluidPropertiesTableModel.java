/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.validator.routines.FloatValidator;

import com.bhpb.deliverylite.FluidModelProperties.FLUID_TYPE;
import com.bhpb.util.Util;
import com.bhpb.xsdparams.delivery.Inversion;
import com.bhpb.xsdparams.delivery.VsFloat;

public class FluidPropertiesTableModel extends DefaultTableModel {

    /**
     * If TABLE_COLUMN_HEADERS are reordered, these flags must also be reorderd.
     * If any value are changes from true to false, edit functionality will be disabled for the column.
     * If any value is changed from false to true, setValueAt must be updated to provide
     * setter functionality.  Otherwise, the user will be permitted to edit the cell but
     * the new value will not persist.
     */
    private static final Logger logger = Logger.getLogger(FluidPropertiesTableModel.class.getName());
    private FluidModelProperties fluidModel;
    private DeliveryLiteUI view;
    private static Object[] columnNames = {"vp", "rho", "vp", "rho", "vp", "rho", "saturation","saturation","saturation"};

    public FluidPropertiesTableModel(FluidModelProperties fluidModel,
            DeliveryLiteUI view) {
        super(fluidModel.toTableArray(),columnNames);
        this.fluidModel = fluidModel;
        this.view = view;

    }




    /**
     * @param c the index of the column for which the data Class will be returned
     * @return the class to be displayed in column #C, used to determine which CellRenderer to use for the column
     */
    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }


    /**
     * True if and only if the cell's column is editable.  For a given column,
     * all cells are either editable or not editable regardless of row.
     *
     * @param rowIndex row on which the cell is located (0-based)
     * @param columnIndex column on which the cell is located (0-based)
     *
     * @return true if and only if the column is editable
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        String val = (String)aValue;
        val = val.trim();
        if(val.length() > 0){
            if(!FloatValidator.getInstance().isValid(val)){
                view.showValidationFailure("Invalid numeric value.");
                return;
            }else{
                float fval = Float.valueOf(val).floatValue();
                if(columnIndex >=6 && columnIndex <= 8 ){
                    if(fval < 0 || fval > 1){
                        view.showValidationFailure("Value must be between 0 and 1.");
                        return;
                    }
                }
                
                if(columnIndex == 6 && rowIndex == 0){
                	if(!Util.floatEqualZero(fval-fluidModel.getSaturation(FLUID_TYPE.OIL))){
                		updateStaturationForAllMiddleLayers(FLUID_TYPE.OIL,fval);
                		fluidModel.setSaturation(FLUID_TYPE.OIL, fval);
                	}
                }else if(columnIndex == 7 && rowIndex == 0){
                	if(!Util.floatEqualZero(fval-fluidModel.getSaturation(FLUID_TYPE.HIGH_GAS))){
                		updateStaturationForAllMiddleLayers(FLUID_TYPE.HIGH_GAS,fval);
                		fluidModel.setSaturation(FLUID_TYPE.HIGH_GAS, fval);
                	}
                }else if(columnIndex == 8 && rowIndex == 0){
                	if(!Util.floatEqualZero(fval-fluidModel.getSaturation(FLUID_TYPE.LOW_GAS))){
                		updateStaturationForAllMiddleLayers(FLUID_TYPE.LOW_GAS,fval);
                		fluidModel.setSaturation(FLUID_TYPE.LOW_GAS, fval);
                	}
                }else if(columnIndex == 6 && rowIndex == 1){
                	if(!Util.floatEqualZero(fval-fluidModel.getSigmaSaturation(FLUID_TYPE.OIL))){
                		updateStaturationForAllMiddleLayers(FLUID_TYPE.OIL,fval);
                		fluidModel.setSigmaSaturation(FLUID_TYPE.OIL, fval);
                	}
                }else if(columnIndex == 7 && rowIndex == 1){
                	if(!Util.floatEqualZero(fval-fluidModel.getSigmaSaturation(FLUID_TYPE.HIGH_GAS))){
                		updateStaturationForAllMiddleLayers(FLUID_TYPE.HIGH_GAS,fval);
                		fluidModel.setSigmaSaturation(FLUID_TYPE.HIGH_GAS, fval);
                	}
                }else if(columnIndex == 8 && rowIndex == 1){
                	if(!Util.floatEqualZero(fval-fluidModel.getSigmaSaturation(FLUID_TYPE.LOW_GAS))){
                		updateStaturationForAllMiddleLayers(FLUID_TYPE.LOW_GAS,fval);
                		fluidModel.setSigmaSaturation(FLUID_TYPE.LOW_GAS, fval);
                	}
                }
            }
        }
        if(columnIndex < 0 || rowIndex < 0)
        	return;
        Vector rowVector = (Vector)dataVector.elementAt(rowIndex);
        rowVector.setElementAt(aValue, columnIndex);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
    private void updateStaturationForAllMiddleLayers(FLUID_TYPE type, float val){
        List<Inversion.ModelDescription.MiddleLayer> layers = view.getAgent().getInversion().getModelDescription().getMiddleLayer();
        for(Inversion.ModelDescription.MiddleLayer layer : layers){
            if(type == FLUID_TYPE.OIL && layer.getReservoirEndmember().getOil() != null){
                VsFloat vsf = new VsFloat();
                vsf.setValue(val);
                layer.getReservoirEndmember().getOil().setSaturationOfOil(vsf);
            }
            if(type == FLUID_TYPE.HIGH_GAS && layer.getReservoirEndmember().getGas() != null){
                VsFloat vsf = new VsFloat();
                vsf.setValue(val);
                layer.getReservoirEndmember().getGas().setSaturationOfGas(vsf);
            }
            if(type == FLUID_TYPE.LOW_GAS && layer.getReservoirEndmember().getLsg() != null){
                VsFloat vsf = new VsFloat();
                vsf.setValue(val);
                layer.getReservoirEndmember().getLsg().setSaturationOfLsg(vsf);
            }
        }
    }
}