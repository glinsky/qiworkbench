/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

public class LiteParams {
	
	private String  project ;
	private String  seismicDir;
	private String  modelDir;
	private String  xmlModel;
	private String  fileNearStack;
	private String  fileFarStack;
	private String  nearFileWavelet;
	private String  farFileWavelet;
	private String  priorReal = "PRIOR";
	private String  postReal = "POST";
	private String  tmin = String.valueOf(DeliveryLiteConstants.DEFAULT_MIN_TIME);
	private String  tmax = String.valueOf(DeliveryLiteConstants.DEFAULT_MAX_TIME);
	private String  tminSpaghettiPlot = String.valueOf(DeliveryLiteConstants.DEFAULT_SPAGETTI_PLOT_MIN_TIME);
	private String  tmaxSpaghettiPlot = String.valueOf(DeliveryLiteConstants.DEFAULT_SPAGETTI_PLOT_MAX_TIME);
	
	private String  ep = "0";
	private String  cdp = "0";
	private String  numRuns = "0";
	private String  outBaseName = "";
	private boolean isNear = true;

	public boolean isNear() {
		return isNear;
	}
	public void setNear(boolean isNear) {
		this.isNear = isNear;
	}
	public String getFileFarStack() {
		return fileFarStack;
	}
	public void setFileFarStack(String fileFarStack) {
		this.fileFarStack = fileFarStack;
	}
	public String getFileNearStack() {
		return fileNearStack;
	}
	public void setFileNearStack(String fileNearStack) {
		this.fileNearStack = fileNearStack;
	}
	public String getOutputBaseName() {
		return outBaseName;
	}
	public void setOutputBaseName(String name) {
		this.outBaseName = name;
	}
	public String getNearFileWavelet() {
		return nearFileWavelet;
	}
	public void setNearFileWavelet(String fileWavelet) {
		this.nearFileWavelet = fileWavelet;
	}
	
	public String getFarFileWavelet() {
		return farFileWavelet;
	}
	
	public void setFarFileWavelet(String fileWavelet) {
		this.farFileWavelet = fileWavelet;
	}
	
	public String getModelDir() {
		return modelDir;
	}
	public void setModelDir(String modelDir) {
		this.modelDir = modelDir;
	}
	
	public String getPostReal() {
		return postReal;
	}
	public void setPostReal(String postReal) {
		this.postReal = postReal;
	}
	public String getPriorReal() {
		return priorReal;
	}
	public void setPriorReal(String priorReal) {
		this.priorReal = priorReal;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getSeismicDir() {
		return seismicDir;
	}
	public void setSeismicDir(String seismicDir) {
		this.seismicDir = seismicDir;
	}
	
	
	public String getCdp() {
		return cdp;
	}
	public void setCdp(String cdp) {
		this.cdp = cdp;
	}
	public String getEp() {
		return ep;
	}
	public void setEp(String ep) {
		this.ep = ep;
	}
	public String getNumRuns() {
		return numRuns;
	}
	public void setNumRuns(String numRuns) {
		this.numRuns = numRuns;
	}
	public String getTmax() {
		return tmax;
	}
	public void setTmax(String tmax) {
		this.tmax = tmax;
	}
	public String getTmaxSpaghettiPlot() {
		return tmaxSpaghettiPlot;
	}
	public void setTmaxSpaghettiPlot(String tmax) {
		this.tmaxSpaghettiPlot = tmax;
	}
	public String getTminSpaghettiPlot() {
		return tminSpaghettiPlot;
	}
	public void setTminSpaghettiPlot(String tmin) {
		this.tminSpaghettiPlot = tmin;
	}	

	public String getTmin() {
		return tmin;
	}
	public void setTmin(String tmin) {
		this.tmin = tmin;
	}
	public String getXmlModel() {
		return xmlModel;
	}
	public void setXmlModel(String xmlModel) {
		this.xmlModel = xmlModel;
	}
}
