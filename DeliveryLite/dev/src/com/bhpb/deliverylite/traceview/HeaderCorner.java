/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.traceview;

import java.awt.*;
import java.util.List;
import javax.swing.*;
/**
 * Top left-hand corner for the main scroll pane showing the names of header keys.
 *
 * @author
 */
public class HeaderCorner extends JPanel {
	public static final int SIZE = 35;
	private List<String> keys;
	
    public void setKeys(List<String> keys){
    	this.keys = keys;
    }

    protected void paintComponent(Graphics g) {
        // Fill me with dirty brown/orange.
    	Rectangle drawHere = g.getClipBounds();
        g.setColor(new Color(230, 163, 4));
        g.fillRect(0, 0, getWidth(), getHeight());
        if(keys == null || keys.size() == 0)
        	return;
        
        g.setFont(new Font("SansSerif", Font.PLAIN, 10));
        g.setColor(Color.black);
        FontMetrics fm = g.getFontMetrics();
        int height = fm.getAscent();
        int hw = drawHere.width/2;
        for(int i = 0; i < keys.size(); i++){
        	String s = keys.get(i);
        	int keyW = fm.stringWidth(s);
        	g.drawString(s, hw-keyW/2, SIZE-1-(2*(i+1)+1)*height/2);
        }
    }
}
