/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.traceview;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import com.bhpb.deliverylite.DeliveryLitePlugin;
import com.bhpb.deliverylite.DeliveryLiteUI;
import com.bhpb.deliverylite.ViewModelDialog;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.util.Util;
/* 
 * StackPanel.java the container of the wiggle trace drawing components including
 *   column view, row view, wiggle view and corner in a scroll pane
 */
public class StackPanel extends JPanel {
    private ColumnView columnView;
    private Ruler rowView;
    private JToggleButton isMetric;

    private WigglePane drawingPane;
    public static final int REAL = 0; 
    public static final int SYNTHETIC_BEFORE = 1;
    public static final int SYNTHETIC_AFTER = 2;

    private JScrollPane wiggleSCrollPane;
    float pixelsPerSample = ViewModelDialog.PIXELS_PER_SAMPLE;  //pixels per sample
    float default_drawing_pane_width = 156;
    float default_drawing_pane_height = 250;
    private Dimension2D area; //indicates area taken up by graphics
    //y coordinate starting value
    float y0 = 0;
    //x coordinate dividing positive amplitude from negative amplitude
    float x0;    
    int rulerUnitWidth=4;
    int spacer = 4;
    private HeaderCorner buttonCorner;
    private DeliveryLiteUI parentGui;
    private ViewModelDialog controller;
    private List<PropertyChangeListener> pcListeners = new ArrayList<PropertyChangeListener>();
    private float scale = 1;
    private int scrollValue = 0;
    private boolean forceAdjusting = false;
    private Map<String,DataHolder> dataMap = new HashMap<String,DataHolder>();
    private Map<String,DataHolder> dataMapOriginal = new HashMap<String,DataHolder>();
    public StackPanel(DeliveryLiteUI parentGui, ViewModelDialog controller) {
        //setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    	 this.parentGui = parentGui;
    	 this.controller = controller;
    	 addPropertyChangeListener(controller);
        setLayout(new BorderLayout());
       
        //Create the row and column headers.
        columnView = new ColumnView(default_drawing_pane_width);
        SeismicFileMetadata nearMetadata = null;
        if(parentGui != null)
        	nearMetadata = parentGui.getNearMetadata();
        if(nearMetadata != null){
        	DiscreteKeyRange range = nearMetadata.getKeyRange("tracl");
        	rowView = new Ruler(range.getMin(),range.getIncr());
        }else
        	rowView = new Ruler(2000,4);  //just arbitrary sample rate and min value
        columnView.setPreferredWidth(ViewModelDialog.DEFAULT_FRAME_WIDTH);
        rowView.setPreferredHeight(ViewModelDialog.DEFAULT_FRAME_HEIGHT);

        //Create the corners.
        buttonCorner = new HeaderCorner(); //use FlowLayout
        area = new Dimension(0,0);
        //drawingRect = new Rectangle2D.Float(0f,0f,default_drawing_pane_width,0);
        drawingPane = new WigglePane(this,controller,default_drawing_pane_width);
        
        //Set up the scroll pane.
        wiggleSCrollPane = new JScrollPane(drawingPane);
        wiggleSCrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
             public void adjustmentValueChanged(AdjustmentEvent e) {
            	//System.out.println("source=" + e.getSource().toString());
                if (!e.getValueIsAdjusting()) {
                    return;
                }
                setScrollValue(e.getValue());
            }
        });
 
        wiggleSCrollPane.setPreferredSize(new Dimension((int)default_drawing_pane_width, (int)default_drawing_pane_height));
        wiggleSCrollPane.setViewportBorder(
                BorderFactory.createLineBorder(Color.black));

        wiggleSCrollPane.setColumnHeaderView(columnView);
        wiggleSCrollPane.setRowHeaderView(rowView);

        wiggleSCrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER,
                                    buttonCorner);
        wiggleSCrollPane.setCorner(JScrollPane.LOWER_LEFT_CORNER,
                                    new Corner());
        wiggleSCrollPane.setCorner(JScrollPane.UPPER_RIGHT_CORNER,
                                    new Corner());

        //Put it in this panel.
        add(wiggleSCrollPane, BorderLayout.CENTER);
        /*JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton showButton = new JButton("Show");
        showButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		float[] data = getFloatArray();
        		points = fillPoints(data, default_drawing_pane_width, 1,pixelsPerSample);
        		float x1 = 0;
        		float y1 = 0;
        		//float x2 = drawingPane.getWidth();
        		float x2 = default_drawing_pane_width;
        		float y2 = 0;
        		if(points.size() > 0)
        			y2 = points.get(points.size()-1).y;
        		System.out.println("y2=" + y2);
        		area.setSize(x2,y2);
        		drawingRect = new Rectangle2D.Float(x1,y1,x2,y2);
        		//area.width = x2;
        		//area.height = y2;
        		drawingPane.setPreferredSize((Dimension)area);
        		drawingPane.revalidate();
        		drawingPane.setListOfPoints(points);
        		drawingPane.repaint();
        		
        		rowView.setPreferredHeight((int)area.getHeight());
        		rowView.revalidate();
        		rowView.repaint();
	        	List<String> values = new ArrayList<String>();
	        	values.add("12333");
	        	values.add("23455");
        		columnView.setValues(values);
        		columnView.repaint();
        		
        		List<String> keys = new ArrayList<String>();
        		keys.add("ep");
        		keys.add("cdp");
        		buttonCorner.setKeys(keys);
        		buttonCorner.repaint();
        	}
        });
        JButton autoFit = new JButton("Show All");
        autoFit.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		if(points.size() == 0)
        			return;
        		//float [] data = getFloatArray();
        		//float newH = (float)drawingPane.getVisibleRect().getHeight() / (float)(data.length-1);
        		//points = fillPoints(data, drawingPane.getVisibleRect());
        		List<String> values = new ArrayList<String>();
            	values.add("12333");
            	values.add("23455");
        		columnView.setValues(values);
        		List<String> keys = new ArrayList<String>();
        		keys.add("ep");
        		keys.add("cdp");
        		buttonCorner.setKeys(keys);

        		fitPageActionPerformed();
        		
        	}
        });
        controlPanel.add(showButton);
        controlPanel.add(autoFit);
        showPositive = new JRadioButton("P");
        showPositive.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		if(showPositive.isSelected())
        			drawingPane.setShowPositiveMF(true);
        		else
        			drawingPane.setShowPositiveMF(false);
        		drawingPane.repaint();
        	}
        });
        showNegative = new JRadioButton("N");
        showNegative.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		if(showNegative.isSelected())
        			drawingPane.setShowNegativeMF(true);
        		else
        			drawingPane.setShowNegativeMF(false);
        		drawingPane.repaint();
        	}
        });
        controlPanel.add(showPositive);
        controlPanel.add(showNegative);
        */
        //add(controlPanel, BorderLayout.SOUTH);
        //setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
    }

    public void enforceAdjustScrollValue(boolean bool){
    	forceAdjusting = bool;
    }
    
    private Rectangle rectToVisible;
    public void setScrollRectToVisible(Rectangle rect){
    	PropertyChangeEvent pce = new PropertyChangeEvent(this,"scrollRectToVisible",rectToVisible,rect);
        for(PropertyChangeListener pcl : pcListeners){
        	pcl.propertyChange(pce);
        }
        this.rectToVisible = rect;
    	//drawingPane.scrollRectToVisible(rect);
    }
    
    public void redrawRowView(List<Point2D.Float> list){
    	rowView.setPreferredHeight((int)list.get(list.size()-1).y);
    	rowView.setPoints(list);
    	rowView.repaint();
    }
    
    public JScrollPane getWiggleScrollPane(){
    	return wiggleSCrollPane;
    }
    
    public void setFillColor(boolean bool){
    	drawingPane.setShowPositiveMF(bool);
    	drawingPane.setShowNegativeMF(bool);
    	drawingPane.repaint();
    }
    
    public WigglePane getWigglePane(){
    	return drawingPane;
    }
    /*
     * Color map for three types of datasets
     */
	private static Map<String,Color> datasetModeColors = new HashMap<String,Color>();
	static{
		datasetModeColors.put(String.valueOf(DeliveryLitePlugin.REAL), Color.BLACK);
		datasetModeColors.put(String.valueOf(DeliveryLitePlugin.SYNTHETIC_BEFORE), Color.RED);
		datasetModeColors.put(String.valueOf(DeliveryLitePlugin.SYNTHETIC_AFTER), Color.GREEN);
	}
	
	
	private float[] getTraceSubset(float [] data, String dataset){
		String sTime = parentGui.getMinTime();
    	float istartTime = Integer.MAX_VALUE;
    	
    	if(sTime != null && sTime.trim().length() > 0){
    		try{
    			istartTime = Float.parseFloat(sTime);
    		}catch(NumberFormatException e){
    			e.printStackTrace();
    		}
    	}
    	float iEndTime = Integer.MAX_VALUE;
    	sTime = parentGui.getTimeBase();
    	if(sTime != null && sTime.trim().length() > 0){
    		try{
    			iEndTime = Float.parseFloat(sTime);
    		}catch(NumberFormatException e){
    			e.printStackTrace();
    		}
    	}
    	SeismicFileMetadata metadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(dataset);

    	if((iEndTime  == Integer.MAX_VALUE) || (istartTime  == Integer.MAX_VALUE) || metadata == null){
    		return data;
    	}else{
    		if(data.length == 0)
    			return data;
    		float[] fs = getSubsetByActualTimeRange(data, metadata, istartTime, iEndTime);
    		return fs;
    	}
    	
	}
	
    //private float[] data;
    public void setTraceData(String dataset,float[] data, int stackMode, int runMode){
    	//this.data = data;
    	boolean isFar = false;
    	if(stackMode == DeliveryLitePlugin.NEAR)
    		isFar = false;
    	else if(stackMode == DeliveryLitePlugin.FAR)
    		isFar = true;
    	float[] fs = getTraceSubset(data,dataset);
    	//float[] fs = data;
    	DataHolder holder = new DataHolder(dataset,fs,isFar,runMode);
    	DataHolder holderOrg = new DataHolder(dataset,fs,isFar,runMode);
    	float vScale = controller.getVerticalScale();
    	float hScale = controller.getHorizontalScale();

    	float oldCenterX = default_drawing_pane_width/2f;
    	float newCenterX = default_drawing_pane_width/2f;
    	if(hScale > 1)
    		newCenterX *= hScale;
    	if(dataMap.containsKey(dataset)){
    		if(data.length == 0){
    			dataMap.remove(dataset);
    		}else{
    			List<Point2D.Float> newpoints = dataMap.get(dataset).getPoints();
    			List<Point2D.Float> oldpoints = dataMapOriginal.get(dataset).getPoints();
    			float max1 = controller.getMaxAmpByDataset(dataset);
    			float max = controller.getMaxAmpFromMap();
    			if(max1 != max){
    				float ratio = max1 / max;
    				hScale *= ratio;
    			}
    			convertPoints(newCenterX,oldCenterX,newpoints, oldpoints,hScale, vScale);
    		}
    	}else{
    		List<Point2D.Float> origPoints = genInitialPoints(dataset,fs,runMode);
    		List<Point2D.Float> newPoints = new  ArrayList<Point2D.Float>();
    		for(Point2D.Float p : origPoints)
    			newPoints.add((Point2D.Float)p.clone());
    		holderOrg.setPoints(origPoints);
    		holder.setPoints(newPoints);
    		dataMap.put(dataset, holder);
    		dataMapOriginal.put(dataset, holderOrg);
    	}
    	redrawWigglePane(hScale, vScale);
   		//setScrollValue(parentGui.getShowModelParams().getVerticalScrollPosition());
   		drawingPane.scrollRectToVisible(parentGui.getShowModelParams().getScrollToVisible());
    }
    
    public void redrawWigglePane(float hScale, float vScale){
    	Iterator it = dataMap.keySet().iterator();
    	float oldCenterX = default_drawing_pane_width/2f;
    	float newCenterX = default_drawing_pane_width/2f;
    	if(hScale > 1)
    		newCenterX *= hScale;
    	while(it.hasNext()){
    		Object key = it.next();
    		List<Point2D.Float> newpoints = dataMap.get(key).getPoints();
    		List<Point2D.Float> oldpoints = dataMapOriginal.get(key).getPoints();
    		float datasetMax = controller.getMaxAmpByDataset((String)key);
    		float finalMax = controller.getMaxAmpFromMap();
    		float hscale = datasetMax / finalMax;
    		float h_scale = hScale * hscale;

    		convertPoints(newCenterX,oldCenterX,newpoints, oldpoints, h_scale, vScale);
    	}
    	showWiggleTrace();
    	if(newCenterX > oldCenterX){
    		int val = (int)(newCenterX - oldCenterX);
    		//wiggleSCrollPane.getHorizontalScrollBar().setValue(val);
    		Rectangle rect = parentGui.getShowModelParams().getScrollToVisible();
    		Rectangle aRect = new Rectangle(val,rect.y, (int)default_drawing_pane_width, (int)default_drawing_pane_height);
    		drawingPane.scrollRectToVisible(aRect);
    	}
    }
    
    public JScrollPane getScrollPane(){
		return wiggleSCrollPane;
	}
    
    public void addPropertyChangeListener(PropertyChangeListener pcl){
		pcListeners.add(pcl);
	}
	
	public void setScale(float newScale){
		PropertyChangeEvent pce = new PropertyChangeEvent(this,"verticalScale",scale,newScale);
        for(PropertyChangeListener pcl : pcListeners){
        	pcl.propertyChange(pce);
        }
        this.scale = newScale;
	}
    
	public void setScrollValue(int value){
		PropertyChangeEvent pce = new PropertyChangeEvent(this,"scrollValue",scrollValue,value);
		this.scrollValue = value;
        for(PropertyChangeListener pcl : pcListeners){
        	pcl.propertyChange(pce);
        }
        
    }	
    /**
     * @param allSamples  all sample data from the entire trace
     * @param metadata    the metadata for the underlying dataset
     * @param startTime     the starting time specified by the user
     * @param endTime     the end time specified by the user
     * @return   new array of float value
     */
    private static float[] getSubsetByActualTimeRange(float[] allSamples, SeismicFileMetadata metadata, float startTime, float endTime){
    	DiscreteKeyRange range = metadata.getKeyRange("tracl");
    	float minTime = (float)range.getMin();
    	float inc = (float)range.getIncr();
        if(inc == 0f){
            throw new IllegalArgumentException("Illegal increment value (0) found.");
        }
        if(startTime <= minTime)
            startTime = minTime;
        if(endTime <= minTime)
            endTime = minTime;

        double rem = Math.IEEEremainder(startTime - minTime, inc);
        if(rem > Double.MIN_VALUE)
            startTime = startTime - (float)rem + inc;
        int startIndex = 0;
        if(startTime > minTime)
            startIndex = (int)((startTime - minTime)/inc);

        rem = Math.IEEEremainder(endTime - minTime, inc);
        if(rem > Double.MIN_VALUE)
            endTime = endTime - (float)rem;
    	int endIndex = (int)((endTime - minTime)/inc);

    	float[] array = new float[endIndex-startIndex+1];
    	int j = 0;
    	for(int i = startIndex; i <= endIndex; i++){
    		array[j++] = allSamples[i];
    	}
    	return array;
    }
    
    private void convertPoints(float newCenterX, float oldCenterX, List<Point2D.Float> ps, List<Point2D.Float> psOrg, float hScale, float vScale){
		if(ps == null || ps.size() == 0)
			return;
		if(psOrg == null || psOrg.size() == 0)
			return;
		if(vScale != 1){
			float delta_y = psOrg.get(1).y - psOrg.get(0).y;
			delta_y *= vScale;
			for(int i = 0; i < ps.size(); i++){
				if(i > 0)
					ps.get(i).y = ps.get(i-1).y + delta_y;
			}
		}
		//if(hScale != 1){
			for(int i = 0; i < psOrg.size(); i++){
				if(psOrg.get(i).x > oldCenterX) {
					float delta_x = psOrg.get(i).x-oldCenterX;
					delta_x *= hScale;
					ps.get(i).x = newCenterX + delta_x;
				}else{
					float delta_x = oldCenterX - psOrg.get(i).x;
					delta_x *= hScale;
					ps.get(i).x = newCenterX - delta_x;
				}
			}
		//}
		return;
	}
    
    /*
     * Generate the  starting set of drawing points based the given dataset
     */
    private List<Point2D.Float> genInitialPoints(String dataset, float[] data, int mode){
    	List<Point2D.Float> newpoints = new ArrayList<Point2D.Float>();
    	if(data == null || data.length == 0)
    		return newpoints;
    	float newPixelPerSample = pixelsPerSample;
    	
   		newpoints = fillPoints(dataset, data, default_drawing_pane_width, 1,newPixelPerSample,mode);
    	return newpoints;
    }
    
    
    private DataHolder getTraceDataHolderByRunType(int type){
    	Iterator it = dataMap.keySet().iterator();
    	while(it.hasNext()){
    		String key = (String)it.next();
    		if(dataMap.get(key).mode == type)
    			return dataMap.get(key);
    	}
    	return null;
    }
    
    private void showWiggleTrace(){
    	DataHolder hder = getTraceDataHolderByRunType(DeliveryLitePlugin.REAL);
    	if(hder == null)
    		hder = getTraceDataHolderByRunType(DeliveryLitePlugin.SYNTHETIC_BEFORE);
    	
    	if(hder == null)
    		hder = getTraceDataHolderByRunType(DeliveryLitePlugin.SYNTHETIC_AFTER);
    	//Color color = datasetModeColors.get(String.valueOf(type));
    	if(hder == null){
    		drawingPane.setPopintsByMode(DeliveryLitePlugin.REAL,new ArrayList<Point2D.Float>());
    		drawingPane.repaint();
    		rowView.repaint();
    		columnView.setValues(new ArrayList<String>());
    		columnView.repaint();
    		return;
    	}

    	rowView.setPoints(hder.getPoints());
		SeismicFileMetadata metadata = (SeismicFileMetadata)parentGui.getMetadataByDataset(hder.dataset);
		if(metadata != null){
			String sTime = parentGui.getMinTime();
	    	float istartTime = Integer.MAX_VALUE;
	    	
	    	if(sTime != null && sTime.trim().length() > 0){
	    		try{
	    			istartTime = Float.parseFloat(sTime);
	    		}catch(NumberFormatException e){
	    			e.printStackTrace();
	    		}
	    	}
			if(istartTime  == Integer.MAX_VALUE){  //if problem in getting the starting time then use dataset min time
				rowView.setMinValue(metadata.getKeyRange("tracl").getMin());
			}else{
				rowView.setMinValue(istartTime);
			}
			rowView.setSampleRate(metadata.getKeyRange("tracl").getIncr());
		}
    	
    	List<Point2D.Float> newpoints = hder.getPoints();
    	float hscale = parentGui.getShowModelParams().getHorizontalScale();
    	float width = default_drawing_pane_width;
    	if(hscale > 1)
    		width = default_drawing_pane_width * hscale;
		float x2 = width;
		float y2 = 0;
		if(newpoints.size() > 0)
			y2 = newpoints.get(newpoints.size()-1).y;
		if(y2 > default_drawing_pane_height)
			x2 -= 3;
		if(x2 > default_drawing_pane_width)
			y2 -= 3;
		area.setSize(x2,y2);

		//area.width = x2;
		//area.height = y2;
		Iterator it = dataMap.keySet().iterator();
		drawingPane.clear();
		drawingPane.setTraceMaxWidth(width);
		while(it.hasNext()){
			DataHolder holder = dataMap.get(it.next());
			Color color = datasetModeColors.get(String.valueOf(holder.getMode()));
			drawingPane.setListOfPoints(holder.dataset,holder.points,holder.mode, color);
    	}
		drawingPane.repaint();
		drawingPane.setPreferredSize((Dimension)area);
		drawingPane.revalidate();
		
		rowView.repaint();
		rowView.setPreferredHeight((int)area.getHeight());
		rowView.revalidate();
		
    	List<String> values = new ArrayList<String>();
    	values.add(parentGui.getEp());
    	values.add(parentGui.getCdp());
    	
		
		List<String> keys = new ArrayList<String>();
		keys.add("ep");
		keys.add("cdp");
		buttonCorner.setKeys(keys);
		buttonCorner.repaint();
		columnView.setValues(values);
		columnView.setPreferredWidth((int)width);
		columnView.revalidate();
		columnView.setDefaultTraceMaxWidth(width);
//		float currMax = controller.getCurrentTraceMaxValue();
		int maxWidth = (int)(width/2);
		columnView.setMaxAmpWidth(maxWidth);
		columnView.repaint();
    }
    
    public void clearDataMap(){
    	dataMap.clear();
    	dataMapOriginal.clear();
    }
    public void setPixelsPerSample(float unit){
    	pixelsPerSample = unit;
    }
    
    private List<Point2D.Float> fillPoints(String dataset, float[] data, float paneWidth, float hScale, float pixelsPerSample, int mode){
    	List<Point2D.Float> points = new ArrayList<Point2D.Float>();
		if(data == null || data.length == 0)
			return points;
				
		float max = Math.abs(Util.getMax(data));
        float min = Math.abs(Util.getMin(data));
        if(max <= min)
        	max = min;
        controller.setDatasetMaxAmp(dataset, max);
        float x00 = paneWidth / 2;
        float unit = (float)x00 / (float)max;
        unit *= hScale;
        for(int i = 0; i < data.length; i++){
       		float x1 = x00 + unit * data[i]; 
       		float y1 = y0 + pixelsPerSample*i;
       		points.add(new Point2D.Float(x1,y1));
        }
        return points;
	}
    

	    
    
    //public float[] getFloatArray(){//String filePath){
    //	float[] myarray = new float[0];
    //	DataObject[] objs = parentGui.getAgent().getNearSeismicTraceByMetaData();
    //	if(objs != null && objs.length > 0){
    //		myarray = objs[0].getFloatVector();
    //	}
    //	return myarray;
    //}

    //for testing purpose only
    public float[] getFloatArrayLocal(){//String filePath){
    	String filePath = "/home/lilt9/qiProjects/neptune/datasets/POSTM_16flt_1970_9000_t2000_t4000.su";
        float[] myarray = new float[0];
        try {
           File file = new File(filePath);
           InputStream is = new FileInputStream(filePath);
           DataInputStream dis = new DataInputStream( is );
           long length = file.length();
           myarray = new float[((int)length-240)/4+1];
           System.out.println("file length=" + length);
           if (length > Integer.MAX_VALUE) {
              throw new IOException("File is too large");
           } else {
              byte[] bytes = new byte[(int)length];
              int offset = 240;
              int numRead = 0;
              while (offset < bytes.length && 
                 (numRead = is.read(bytes, offset, bytes.length-offset) ) >= 0) {
                 offset += numRead;
              }
              if (offset < bytes.length) {
                 throw new IOException("Could not completely read file "+file.getName());
              }
              dis.close();
              is.close();
              //System.out.println("offset="+offset);
              int cnt = 0;
              for (int start = 240; start < offset; start = start + 4) {
                 myarray[cnt] = (float)arr2float(bytes, start);
                 //System.out.printf("%d, %.4f %n", cnt, myarray[cnt]);
                 cnt++;
              }
           }
        } catch (Exception e) {
           e.printStackTrace();
        }
        /*float[] myarray1 = {489.7806, 283.8762,284.1276,556.6053,403.8991,-200.3890,-298.1874,198.7489,211.3696,-99.6241,361.2601,776.6893,-396.7808,-1780.6375,-956.3305,
        		811.4283, 637.0254, -813.0413, -1092.6503, -535.4237};
        int i = 0;
        for(float d : myarray1){
        	System.out.println(i++ + " " + d);
        }
        */
        return myarray;
    }
    
    public static float arr2float (byte[] arr, int start) {
		int i = 0;
		int len = 4;
		int cnt = 0;
		byte[] tmp = new byte[len];
		for (i = start; i < (start + len); i++) {
			tmp[cnt] = arr[i];
			cnt++;
		}
		int accum = 0;
		i = 0;
		for ( int shiftBy = 0; shiftBy < 32; shiftBy += 8 ) {
			accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
			i++;
		}
		return Float.intBitsToFloat(accum);
	}
        
    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = StackPanel.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("ScrollDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        JPanel newContentPane = new StackPanel(null,null); //only for UI
        
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setLayout(new BorderLayout());
        frame.add(newContentPane,BorderLayout.CENTER);
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    /**
     * Data structure to store the information that is associated with a given dataset 
     */
    private static class DataHolder {
    	//trace sample data
    	private float[] data;
    	//sample data converted to graphic points
    	private List<Point2D.Float> points;
    	//either near or far
    	private boolean isFar = false;  //is near by default
    	//associated dataset name;
    	private String dataset;
    	/**
    	 * whether this dataset is real trace, synthetic before, or synthetic after
    	 */
    	private int mode;
    	public DataHolder(String dataset, float[] data, boolean isFar, int mode){
    		this.dataset = dataset;
    		this.data = data;
    		this.mode = mode;
    		this.isFar = isFar;
    	}
    	public void setPoints(List<Point2D.Float> points){
    		this.points = points;
    	}
    	public int getMode(){
    		return mode;
    	}
    	public List<Point2D.Float> getPoints(){
    		return points;
    	}
    	public float[] getData(){
    		return data;
    	}
    	public boolean isNear(){
    		return !isFar;
    	}
    }
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
