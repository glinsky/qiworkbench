/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.deliverylite.traceview;

import java.awt.*;
import javax.swing.*;

/**
 * Plain corner for the main scroll pane.
 *
 * @author
 */
public class Corner extends JComponent {
    protected void paintComponent(Graphics g) {
        // Fill me with dirty brown/orange.
        g.setColor(new Color(230, 163, 4));
        g.fillRect(0, 0, getWidth(), getHeight());
    }
}
