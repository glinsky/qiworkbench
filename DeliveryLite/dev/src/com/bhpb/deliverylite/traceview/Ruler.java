/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.traceview;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import javax.swing.*;

import java.text.NumberFormat;
import java.util.List;

/**
 * Vertical component for the main scroll pane on the left-hand side. This serves as a ruler showing distribution of 
 * the sample values based on the given sample rate and the minimum value
 *
 * @author
 */

public class Ruler extends JComponent {
    public static final int INCH = Toolkit.getDefaultToolkit().
            getScreenResolution();
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    public static final int SIZE = 35;

    public int orientation;
    public boolean isMetric;
    private int increment;
    private int units;
    
    int timeInteval = 100;  //number of samples for each label
    private double minValue = 0;  //the starting value for the ruler
    double sampleRate = 0;    //the increment value for each sample
    private List<Point2D.Float> listOfPoints;  // the targer source location for drawing the ruler

    /**
     * @param minTime 	the minimum (starting) value for the ruler
     * @param sampleRate 	the increment
     * @param points 	a list of sample location that the ruler is used to measure
     */
    public Ruler(double minTime, double sampleRate) {
        minValue = minTime;
        this.sampleRate = sampleRate;
    }

    /**
     * @param o   orientation (HORIZONTAL or VERTICAL)
     * @param m   if the metric system is used
     * @param minTime the minimum (starting) value for the ruler
     * @param sampleRate 	the increment
     * @param points 	a list of sample location that the ruler is used to measure
     */
    public Ruler(int o, boolean m, double minTime, double sampleRate) {
    	this(minTime,sampleRate);
        orientation = o;
        isMetric = m;
        setIncrementAndUnits();
    }

    public void setPoints(List<Point2D.Float> list){
    	listOfPoints = list;
    }
    
    
    public void setSampleRate(double rate ){
    	sampleRate = rate;
    }
    
    public void setIsMetric(boolean isMetric) {
        this.isMetric = isMetric;
        setIncrementAndUnits();
        repaint();
    }

    private void setIncrementAndUnits() {
        if (isMetric) {
            units = (int)((double)INCH / (double)2.54); // dots per centimeter
            increment = units;
        } else {
            units = INCH;
            increment = units / 2;
        }
    }

    public boolean isMetric() {
        return this.isMetric;
    }

    public int getIncrement() {
        return increment;
    }

    public void setMinValue(double val){
    	minValue = val;
    }
    public void setIncrement(int increment){
    	this.increment = increment;
    }
    public void setPreferredHeight(int ph) {
        setPreferredSize(new Dimension(SIZE, ph));
    }

    public void setPreferredWidth(int pw) {
        setPreferredSize(new Dimension(pw, SIZE));
    }

    protected void paintComponent(Graphics g) {
        Rectangle drawHere = g.getClipBounds();

        // Fill clipping area with dirty brown/orange.
        g.setColor(new Color(230, 163, 4));
        g.fillRect(drawHere.x, drawHere.y, drawHere.width, drawHere.height);
        if(listOfPoints == null || listOfPoints.size() == 0)
        	return;
        // Do the ruler labels in a small font that's black.
        g.setFont(new Font("SansSerif", Font.PLAIN, 10));
        g.setColor(Color.black);
        
        // Some vars we need.
        int tickLength = 4;
        FontMetrics fm = g.getFontMetrics();
        //int sh = fm.getAsHeight();
        int sh = fm.getAscent();
        Graphics2D g2 = (Graphics2D)g;
        NumberFormat format = NumberFormat.getInstance();
        double maxTimeValue = minValue + (listOfPoints.size()-1)*sampleRate;
        format.setMaximumFractionDigits(0);
        String maxTime = format.format(maxTimeValue);
        int w = fm.stringWidth(maxTime);
        
        ///////
    	float totalPixels = 0f;
    	if(listOfPoints.size() > 0){
    		totalPixels = listOfPoints.get(listOfPoints.size()-1).y - listOfPoints.get(0).y; 
    	}
    	int numSamples = listOfPoints.size();
    	float pixelsPerSample = 0f;
    	int inter = 1;
    	if(numSamples > 1){
    		pixelsPerSample = totalPixels / (float)(numSamples-1);
    		if(pixelsPerSample > 0){
    			timeInteval = (int)(50/pixelsPerSample);
    			if(timeInteval <= 1)
    				timeInteval = 1;
    		}
    	}
    	/////
    	        
        
        
        
        //draw ticker
        int i = 0;
        for(i = 0;  i < listOfPoints.size(); i+=timeInteval){  
        	//for(i = 0;  i < listOfPoints.size(); i+=10){        	
        	Shape s3 = new Line2D.Float((float)drawHere.getMaxX()-tickLength, listOfPoints.get(i).y, (float)drawHere.getMaxX(), listOfPoints.get(i).y);
        	g2.draw(s3);
        }
        Shape s3 = new Line2D.Float((float)drawHere.getMaxX()-tickLength, listOfPoints.get(listOfPoints.size()-1).y, (float)drawHere.getMaxX(), listOfPoints.get(listOfPoints.size()-1).y);
    	g2.draw(s3);

        //if(listOfPoints.size() > timeInteval){
	        //for(i = 0; listOfPoints.size() > 1 && i < listOfPoints.size()-timeInteval; i+=timeInteval){
		    for(i = 0; listOfPoints.size() > 1 && i < listOfPoints.size(); i+=timeInteval){	        	
		       	if(i == 0) //the first one drawn lower
		       		g2.drawString(format.format(minValue+(i*sampleRate)),(float)(drawHere.getMaxX()-tickLength-w), (float)listOfPoints.get(i).y+sh);
		        else
		        	g2.drawString(format.format(minValue+(i*sampleRate)),(float)(drawHere.getMaxX()-tickLength-w), (float)listOfPoints.get(i).y+sh/3f);
	        }
	        //g2.drawString(format.format(minValue+(i*sampleRate)),(float)(drawHere.getMaxX()-tickLength-w), (float)listOfPoints.get(i).y);
	        g2.drawString(format.format(maxTimeValue),(float)(drawHere.getMaxX()-tickLength-w), (float)listOfPoints.get(listOfPoints.size()-1).y);
        //}else{
        //	for(i = 0; i < listOfPoints.size(); i+=10){
		//       	if(i == 0) //the first one drawn lower
		//       		g2.drawString(format.format(minValue+(i*sampleRate)),(float)(drawHere.getMaxX()-tickLength-w), (float)listOfPoints.get(i).y+sh);
		//        else
		//        	g2.drawString(format.format(minValue+(i*sampleRate)),(float)(drawHere.getMaxX()-tickLength-w), (float)listOfPoints.get(i).y+sh/3f);
	    //    }
	    //    g2.drawString(format.format(maxTimeValue),(float)(drawHere.getMaxX()-tickLength-w), (float)listOfPoints.get(listOfPoints.size()-1).y);
        //}
    }
}
