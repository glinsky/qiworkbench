/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite.traceview;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Label;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.bhpb.deliverylite.DeliveryLitePlugin;
import com.bhpb.deliverylite.ViewModelDialog;


/**
 * The pane shows the wiggle trace images
 *
 * @author
 */
//public class WigglePane  extends JPanel implements Scrollable, MouseListener, MouseMotionListener {
public class WigglePane  extends JPanel implements MouseListener, MouseMotionListener {	
	/** The component inside the scroll pane. */
	//float[] data = new float[0];
    /** true if we are in drag */
    boolean inDrag = false;
    /** starting location of a drag */
    float startX = -1, startY = -1;
    /** current location of a drag */    
    float curX = -1, curY = -1;
    private WigglePane drawingPane;
    float default_y_unit = 2;
    /** Kludge for showStatus */
    static Label status;
    //private float[] data;
    private final Color colors[] = {
        Color.red, Color.blue, Color.green, Color.orange,
        Color.cyan, Color.magenta, Color.darkGray, Color.yellow};
    //private final int color_n = colors.length;
    int scrollbarWidth = 15;
    //y coordinate starting value
    float y0 = 0;
    //x coordinate dividing positive amplitude from negative amplitude
    float x0;
    int rulerUnitWidth=4;
    int spacer = 4;
    int iMinTime = 500;
    int iMaxTime = 1000;
    int sampleRate = 4;
    int timeInteval = 100;
    private boolean showPositiveMF = true;
    private boolean showNegativeMF = true;
    private int maxUnitIncrement = 1;
    private StackPanel container;
    private ViewModelDialog controller;
    private float traceMaxWidth = 0;
    private Color positiveMFColor = Color.BLACK;
    private Color negativeMFColor = Color.WHITE;

	private Map<String,DrawingElements> pointsMap = new HashMap<String,DrawingElements> ();
	private boolean testForZoom = false;
	private int mode = 0;  //mode 0 near; mode 1 far
	public static final List<String> renderSequence = new ArrayList<String>();
	static{
		renderSequence.add(String.valueOf(DeliveryLitePlugin.REAL));
		renderSequence.add(String.valueOf(DeliveryLitePlugin.SYNTHETIC_BEFORE));
		renderSequence.add(String.valueOf(DeliveryLitePlugin.SYNTHETIC_AFTER));
	}
	
	public WigglePane(StackPanel container, ViewModelDialog controller, float width){
		this.controller = controller;

		this.container = container;
		traceMaxWidth = width;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		
		this.setBackground(Color.WHITE);
	}
	
	
	public void setTraceMaxWidth(float width){
		traceMaxWidth = width;
	}
	
	public void setShowPositiveMF(boolean bool){
		showPositiveMF = bool;
	}
	
	
	public void setShowNegativeMF(boolean bool){
		showNegativeMF = bool;
	}
	
	public void clear(){
		pointsMap.clear();
	}
	
	public void setListOfPoints(String dataset,List<Point2D.Float> points, int mode, Color color){
		pointsMap.put(dataset,new DrawingElements(dataset,points,mode,color));
	}
	
	public List<Point2D.Float> getListOfPoints(String dataset){
		if(pointsMap.containsKey(dataset))
			return pointsMap.get(dataset).getPoints();
		return null;
	}
	
	private String getKeyByRunMode(int mode){
		Iterator it = pointsMap.keySet().iterator();
		while(it.hasNext()){
			DrawingElements de = pointsMap.get(it.next());
			if(de.mode == mode)
				return de.dataset;
		}
		return null;
	}
	
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(pointsMap.isEmpty())
            return;
        Graphics2D g2 = (Graphics2D)g;
        if(inDrag){
        	float w = curX - startX, h = curY - startY;
        	if (startX < 0 || startY < 0)
        		return;
        	g2.setColor(Color.black);
        	Shape s = new Rectangle2D.Float(startX, startY,  w, h);
        	g2.draw(s);
        }
//        Color oldColor = g.getColor();
        //int j = 0;
        //float rectHeight = -1;
        /*
        if(testForZoom){
	        if(points.size() > 1)
	        	rectHeight = (points.get(1).y - points.get(0).y)*timeInteval/2;
	        for(int i = 0; points.size() > 1 && i < points.size()-1; i+=(timeInteval/2)){
	        	//float xx = sw+spacer+rulerUnitWidth+ spacer;
	        	float xx = 0;
	        	g.setColor(colors[j++]);
	        	if(j >= colors.length)
	        		j = 0;
	        	Shape s1 = new Rectangle2D.Float(xx, points.get(i).y, getVisibleRect().width-xx,rectHeight);
	        	g2.fill(s1);
	        }
        }
        */
        //drawing wiggle lines
        //int count = 0;
        //int size = pointsMap.keySet().size();
        //float alphaValue = 1;
        //if(size > 0)
        //	alphaValue = 1f / (float)size;
        
        //int compositeRule = AlphaComposite.SRC_OVER;
        //AlphaComposite ac = AlphaComposite.getInstance(compositeRule, alphaValue);
        
        for(String s : renderSequence){
        	String key = getKeyByRunMode(Integer.valueOf(s));
        	if(key != null){
        	//if(count > 0)
        	//	g2.setComposite(ac);
        		DrawingElements d = pointsMap.get(key);
        		drawWiggles(g2, d.color, d.getPoints(),d.mode);
        	//count++;
        	}
        }
        /*
        g.setColor(oldColor);

        
        List<Point2D.Float> list = new ArrayList<Point2D.Float>(points.size());
        list.addAll(points);
        Collections.copy(list, points);
        if(list.size() > 0){
			Point2D.Float pp1 = list.get(0);
			if(pp1.x != (traceMaxWidth/2)){				
				list.add(0,new Point2D.Float(traceMaxWidth/2,pp1.y));
			}
			if(list.size() > 1){
				Point2D.Float pp = list.get(list.size()-1);
				if(pp.x != (traceMaxWidth/2)){
					list.add(new Point2D.Float(traceMaxWidth/2,pp.y));
				}
			}
		}
        
        
        GeneralPath path = new GeneralPath();
        if(list.size() > 1){
        	int i = 0;
			for(i = 0; i < list.size()-1; i++){
				Line2D.Float line = new Line2D.Float(list.get(i).x,list.get(i).y,
						list.get(i+1).x,list.get(i+1).y);
				path.append(line, true);
			}
			if(showNegativeMF || showPositiveMF){
				Line2D.Float line = new Line2D.Float(list.get(0).x,list.get(0).y,
				list.get(list.size()-1).x,list.get(list.size()-1).y);
				path.append(line, true);
			}
		}
		g2.draw(path);
		
		Area fillableArea = new Area(path);
		float ymax = 0;
		if(points.size() > 0)
			ymax = points.get(points.size()-1).y;
		if(showPositiveMF){
			Area positiveArea = new Area(new Rectangle2D.Float(traceMaxWidth/2,0,traceMaxWidth,ymax));
			positiveArea.intersect(fillableArea);
			g2.setColor(positiveMFColor);
			g2.fill(positiveArea);
		}
		
		if(showNegativeMF){
			Area negativeArea = new Area(new Rectangle2D.Float(0,0,traceMaxWidth/2,ymax));
			negativeArea.intersect(fillableArea);
			g2.setColor(negativeMFColor);
			g2.fill(negativeArea);
		}
		g2.setColor(oldColor);  //black
		g2.draw(path); */
    }

    
    private void drawWiggles(Graphics2D g2, Color oldColor, List<Point2D.Float> points, int mode){
    	g2.setColor(oldColor);
        List<Point2D.Float> list = new ArrayList<Point2D.Float>(points.size());
        list.addAll(points);
        Collections.copy(list, points);
        if(list.size() > 0){
			Point2D.Float pp1 = list.get(0);
			if(Math.abs(pp1.x - (traceMaxWidth/2)) > 0.000001){	//not equal to 0			
				list.add(0,new Point2D.Float(traceMaxWidth/2,pp1.y));
			}
			if(list.size() > 1){
				Point2D.Float pp = list.get(list.size()-1);
				if(Math.abs(pp.x - (traceMaxWidth/2)) > 0.000001){
					list.add(new Point2D.Float(traceMaxWidth/2,pp.y));
				}
			}
		}
        
        
        GeneralPath path = new GeneralPath();
        if(list.size() > 1){
        	int i = 0;
			for(i = 0; i < list.size()-1; i++){
				Line2D.Float line = new Line2D.Float(list.get(i).x,list.get(i).y,
						list.get(i+1).x,list.get(i+1).y);
				path.append(line, true);
			}
			
			if(mode == DeliveryLitePlugin.REAL && (showNegativeMF || showPositiveMF)){
				Line2D.Float line = new Line2D.Float(list.get(0).x,list.get(0).y,
				list.get(list.size()-1).x,list.get(list.size()-1).y);
				path.append(line, true);
			}
		}
		g2.draw(path);
		
		if(mode == DeliveryLitePlugin.REAL){
			Area fillableArea = new Area(path);
			float ymax = 0;
			if(points.size() > 0)
				ymax = points.get(points.size()-1).y;
			
			if(showPositiveMF){
				Area positiveArea = new Area(new Rectangle2D.Float(traceMaxWidth/2,0,traceMaxWidth,ymax));
				positiveArea.intersect(fillableArea);
				g2.setColor(positiveMFColor);
				g2.fill(positiveArea);
			}
			
			if(showNegativeMF){
				Area negativeArea = new Area(new Rectangle2D.Float(0,0,traceMaxWidth/2,ymax));
				negativeArea.intersect(fillableArea);
				g2.setColor(negativeMFColor);
				g2.fill(negativeArea);
			}
		}
		g2.setColor(oldColor);  //black
		g2.draw(path);
    }
    
    public void setNegativeMFColor(Color color){
    	negativeMFColor = color;
    }
    
    public void setPositiveMFColor(Color color){
    	positiveMFColor = color;
    }
    
    //Handle mouse events.
    public void mouseReleased(MouseEvent e) {
    	boolean changed = false;
    	if(inDrag == false){
    		repaint();
    		return;
    	}
	    inDrag = false;
	    if(curY < 0)
	    	curY = 0;
	    if (SwingUtilities.isRightMouseButton(e)) {
	        //This will clear the graphic objects.
	        //area.setSize(0,0);
	        //area.width=0;
	        //area.height=0;
	        changed = true;
	    } else {
	    	//int panelHeight = this.getVisibleRect().height;
	    	int panelHeight = container.getVisibleRect().height-ColumnView.SIZE;
	    	
	    	float newHeight = -1;
	    	if(this.getHeight() > panelHeight)
	    		newHeight = panelHeight-scrollbarWidth;
	    	else
	    		newHeight = panelHeight;
	    	float len = curY - startY;
	    	if(Math.abs(len) < 0.000001)  //equivalent to zero
	    		return;
	    	float ratio = (newHeight) / len;
	    	
	    	float oldy0 = 0;
	    	if(pointsMap != null && !pointsMap.isEmpty()){
	    		Iterator it = pointsMap.keySet().iterator();
	    		if(it.hasNext())
	    			oldy0 = pointsMap.get(it.next()).points.get(0).y;
	    	}
	    	float newy = (startY-oldy0)*ratio;
	    	
	    	//System.out.println("ratio=" + ratio);
	    	float verticalScale = controller.getVerticalScale();
	    	verticalScale *= ratio;
	    	
	        Rectangle rect = new Rectangle(0, (int)newy, this.getVisibleRect().width, this.getVisibleRect().height);
	        
	        
	        container.setScale(verticalScale);
	        container.setScrollRectToVisible(rect); 
	        //this.scrollRectToVisible(rect); 
	        //controller.scaleChanged(y_scale);
	    }
	}
    public void mouseClicked(MouseEvent e){}
    public void mouseEntered(MouseEvent e){}
    public void mouseExited(MouseEvent e){}
    public void mousePressed(MouseEvent e){
    	Point p = e.getPoint();
        System.err.println("mousePressed at " + p);
        startX = p.x;
        startY = p.y;
        inDrag = false;
    }
    // And two methods from MouseMotionListener:
    public void mouseDragged(MouseEvent e) {
        if (pointsMap == null || pointsMap.keySet().size() == 0) {
            return;
        }
        Point p = e.getPoint();
        // System.err.println("mouse drag to " + p);
        //showStatus("mouse Dragged to " + p);
        curX = p.x;
        curY = p.y;
        inDrag = true;
        //if (inDrag) {
        repaint();
    //}
    }

    public void mouseMoved(MouseEvent e) {
    	if(pointsMap == null || pointsMap.keySet().size() == 0)
    		return;
    	Point p = e.getPoint();
    	Set set = pointsMap.keySet();
    	Iterator it = set.iterator();
    	while(it.hasNext()){
    		Object o = it.next();
    		DrawingElements de = pointsMap.get(o);
    		
    		for(int i = 0; i < de.points.size()-1; i++){
				Line2D.Float line = new Line2D.Float(de.points.get(i).x,de.points.get(i).y,
						de.points.get(i+1).x,de.points.get(i+1).y);
				if(line.ptLineDist(p.x,p.y) < 2){
					this.setToolTipText((String)o + " (in " + de.getColorName() + ")");
					return;
				}
			}
    	}
        //showStatus("mouse Moved to " + e.getPoint());
    }
    
    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    public int getScrollableUnitIncrement(Rectangle visibleRect,
                                          int orientation,
                                          int direction) {
        //Get the current position.
        int currentPosition = 0;
        if (orientation == SwingConstants.HORIZONTAL) {
            currentPosition = visibleRect.x;
        } else {
            currentPosition = visibleRect.y;
        }

        //Return the number of pixels between currentPosition
        //and the nearest tick mark in the indicated direction.
        if (direction < 0) {
            int newPosition = currentPosition -
                             (currentPosition / maxUnitIncrement)
                              * maxUnitIncrement;
            return (newPosition == 0) ? maxUnitIncrement : newPosition;
        } else {
            return ((currentPosition / maxUnitIncrement) + 1)
                   * maxUnitIncrement
                   - currentPosition;
        }
    }

    public int getScrollableBlockIncrement(Rectangle visibleRect,
                                           int orientation,
                                           int direction) {
        if (orientation == SwingConstants.HORIZONTAL) {
            return visibleRect.width - maxUnitIncrement;
        } else {
            return visibleRect.height - maxUnitIncrement;
        }
    }

    public boolean getScrollableTracksViewportWidth() {
        return true;
    }
    
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    public void setPopintsByMode(int mode, List<Point2D.Float> ps){
    	Iterator it = pointsMap.keySet().iterator();
    	while(it.hasNext()){
    		String key = (String)it.next();
    		if(pointsMap.get(key).mode == mode){
    			pointsMap.get(key).points = ps;
    		}
    	}
    	return;
    }
    
    
    
    private static class DrawingElements{
    	List<Point2D.Float> points;
    	//real, synthetic before, synthetic after
    	int mode;
    	//color for each mode wiggle line
    	Color color;
    	//dataset associated with mode
    	String dataset;
    	public DrawingElements(String dataset, List<Point2D.Float> points, int mode, Color color){
    		this.dataset = dataset;
    		this.points = points;
    		this.mode = mode;
    		this.color = color;
    	}
    	public List<Point2D.Float> getPoints(){
    		return points;
    	}
    	public int getMode(){
    		return mode;
    	}
    	public Color getColor(){
    		return color;
    	}
    	public String getColorName(){
    		if(color.equals(Color.black))
    			return "black";
    		else if(color.equals(Color.red))
    			return "red";
    		else if(color.equals(Color.blue))
    			return "blue";
    		else if(color.equals(Color.green))
    			return "green";
    		else if(color.equals(Color.cyan))
    			return "cyan";
    		else if(color.equals(Color.darkGray))
    			return "darkGray";
    		else if(color.equals(Color.gray))
    			return "gray";
    		else if(color.equals(Color.lightGray))
    			return "lightGray";
    		else if(color.equals(Color.magenta))
    			return "magenta";
    		else if(color.equals(Color.orange))
    			return "orange";
    		else if(color.equals(Color.pink))
    			return "pink";
    		else if(color.equals(Color.white))
    			return "white";
    		else if(color.equals(Color.yellow))
    			return "yellow";
    		return "";

    	}
    	public String getDataset(){
    		return dataset;
    	}
    }
    
}