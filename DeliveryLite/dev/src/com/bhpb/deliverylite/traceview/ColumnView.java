/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code.
 # Copyright (C) 2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.deliverylite.traceview;

import java.awt.*;
import javax.swing.*;

import org.apache.commons.lang.SystemUtils;

import java.math.BigInteger;
import java.math.MathContext;
import java.util.List;
/**
 * Horizontal top component for the main scroll pane. Show each key's name value pair
 *
 * @author
 */
public class ColumnView extends JComponent {
    public static final int SIZE = 35;

    private List<String> values;
    private float defaultTraceMaxWidth;  //the default width of the underlying pane 
    									//where the single trace drawn
    public ColumnView(float tracePaneWidth) {
        defaultTraceMaxWidth = tracePaneWidth;
    }

    public void setDefaultTraceMaxWidth(float width){
    	defaultTraceMaxWidth = width;
	}

    public void setPreferredHeight(int ph) {
        setPreferredSize(new Dimension(SIZE, ph));
    }

    public void setPreferredWidth(int pw) {
        setPreferredSize(new Dimension(pw, SIZE));
    }

    public void setValues(List<String> values){
    	this.values = values;
    }
    private int maxAmpWidth = 0;
    public void setMaxAmpWidth(int width){
    	maxAmpWidth = width;
    	//repaint();
    }
    
    protected void paintComponent(Graphics g) {
        Rectangle drawHere = g.getClipBounds();

        // Fill clipping area with dirty brown/orange.
        g.setColor(new Color(230, 163, 4));
        g.fillRect(drawHere.x, drawHere.y, drawHere.width, drawHere.height);
        //g.fillRect(0, 0, (int)defaultTraceMaxWidth+10, SIZE);

        if(values == null || values.size() == 0)
        	return;
        
        // Do the ruler labels in a small font that's black.
        g.setFont(new Font("SansSerif", Font.PLAIN, 10));
        g.setColor(Color.black);
        int tickerY0 = SIZE-2;
        int tickLength = 4;
        //g.drawLine(0, tickerY0, drawHere.width, tickerY0);
        g.drawLine(0, tickerY0, (int)defaultTraceMaxWidth, tickerY0);
        int tickerX0 = Math.round(defaultTraceMaxWidth/2);
        g.drawLine(tickerX0, tickerY0, tickerX0, tickerY0-tickLength);
        
        FontMetrics fm = g.getFontMetrics();
        int height = fm.getAscent();
        int hw = Math.round(defaultTraceMaxWidth/2);
        for(int i = 0; i < values.size(); i++){
        	String s = values.get(i);
        	int keyW = fm.stringWidth(s);
        	g.drawString(s, hw-keyW/2, SIZE-1-(2*(i+1)+1)*height/2);
        }
        /*
        if(maxAmpWidth != 0){
	        String s1 = "0";
	        int width = fm.stringWidth(s1);
        	g.drawString(s1, tickerX0-width/2, tickerY0-tickLength-1);
        	s1 = "1";
        	width = fm.stringWidth(s1);
        	g.drawLine(tickerX0+maxAmpWidth, tickerY0, tickerX0+maxAmpWidth, tickerY0-tickLength);
        	g.drawString(s1, tickerX0+maxAmpWidth-width/2, tickerY0-tickLength-1);
        	//s1 = "-1";
        	s1 = "   -1";
        	width = fm.stringWidth(s1);
        	g.drawLine(tickerX0-maxAmpWidth, tickerY0, tickerX0-maxAmpWidth, tickerY0-tickLength);
        	g.drawString(s1, tickerX0-maxAmpWidth-width/2, tickerY0-tickLength-1);
        	
        	//s1 = "1/2";
        	s1 = ".5";
        	width = fm.stringWidth(s1);
        	g.drawLine(tickerX0+maxAmpWidth/2, tickerY0, tickerX0+maxAmpWidth/2, tickerY0-tickLength);
        	g.drawString(s1, tickerX0+maxAmpWidth/2-width/2, tickerY0-tickLength-1);
        	//s1 = "-1/2";
        	s1 = "-.5";
        	width = fm.stringWidth(s1);
        	g.drawLine(tickerX0-maxAmpWidth/2, tickerY0, tickerX0-maxAmpWidth/2, tickerY0-tickLength);
        	g.drawString(s1, tickerX0-maxAmpWidth/2-width/2, tickerY0-tickLength-1);
        }*/
        if(maxAmpWidth != 0){
	        String s1 = "0";
	        int width = fm.stringWidth(s1);
        	g.drawString(s1, tickerX0-width/2, tickerY0-tickLength-1);
        	s1 = "1";
        	width = fm.stringWidth(s1);
        	g.drawLine(tickerX0+maxAmpWidth, tickerY0, tickerX0+maxAmpWidth, tickerY0-tickLength);
        	g.drawString(s1, tickerX0+maxAmpWidth-width/2, tickerY0-tickLength-1);
        	//s1 = "-1";
        	s1 = "   -1";
        	width = fm.stringWidth(s1);
        	g.drawLine(tickerX0-maxAmpWidth, tickerY0, tickerX0-maxAmpWidth, tickerY0-tickLength);
        	g.drawString(s1, tickerX0-maxAmpWidth-width/2, tickerY0-tickLength-1);
        	
        	
        	StringBuffer buf = new StringBuffer();
        	int unit = maxAmpWidth/2;
        	int numberTicker = maxAmpWidth/50 + 1; //make ticker interval
        	if(numberTicker > 0)
        		 unit = maxAmpWidth / numberTicker;
        	for(int i = 1; i < numberTicker; i++){
        		String frac = genSimplifiedForm(i, numberTicker);
	        	//buf.append(i);
	        	//buf.append("/");
	        	//buf.append(numberTicker);
        		buf.append(frac);
	        	s1 = buf.toString();
	        	width = fm.stringWidth(s1);
	        	g.drawLine(tickerX0+(unit*i), tickerY0, tickerX0+(unit*i), tickerY0-tickLength);
	        	g.drawString(s1, tickerX0+(unit*i)-width/2, tickerY0-tickLength-1);
	        	buf.delete(0, buf.length());
	        	//int minus = i * -1;
	        	//buf.append(minus);
	        	//buf.append("/");
	        	//buf.append(numberTicker);
	        	buf.append("-");
	        	buf.append(frac);
	        	s1 = buf.toString();
	        	s1 = buf.toString();
	        	width = fm.stringWidth(s1);
	        	g.drawLine(tickerX0-(unit*i), tickerY0, tickerX0-(unit*i), tickerY0-tickLength);
	        	g.drawString(s1, tickerX0-(unit*i)-width/2, tickerY0-tickLength-1);
	        	buf.delete(0, buf.length());
        	}
        }
    }
    
    private static String genSimplifiedForm(int nominator, int denominator){
    	BigInteger biNom = BigInteger.valueOf(nominator);
    	BigInteger biDenom = BigInteger.valueOf(denominator);
    	BigInteger result = biNom.gcd(biDenom);
    	if(result.intValue() != 0){
    		nominator = biNom.intValue() / result.intValue();
    		denominator = biDenom.intValue() / result.intValue();
    	}
    	return nominator + "/" + denominator;
    }
}
