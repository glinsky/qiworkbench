/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;


/**
 *A helper class that convert the portion of dataset name based its type of dataset;
 *either "IS" or "non-IS" type
 * @author 
 */
public class DatasetNameHelper {
    private String datasetType;  //either prior or post
    private String baseName = "";  //output basename
    public final static String IGNORE_SEISMIC = "IS ";
    public final static String DEFAULT_STDDEV_NAME = "stddev";

    /**
     * Form the dataset name based on the input datasetType name; if datasetType starts with "IS "
     * (Ignore Seismic), it is a prior model type of dataset and should be prepended by _prior_"
     * as opposed to no "IS " where dataset should be prepended by "_post_".
     * @param baseName  deliveryLite ouput base name
     * @param datasetType  model dataset type such as median, stddev, p10, p90..., etc.
     */
    public DatasetNameHelper(String baseName, String datasetType){
        if(datasetType == null || datasetType.trim().length() == 0)
            throw new IllegalArgumentException("Invalid dataset type parameter found.");
        this.baseName = baseName;
        this.datasetType = datasetType;
    }

    /**
     *  Determine if the type of model (prior or post) given the datasetType (median, stddev..).
     */
    public String getModelType(){
        String modelType = "post";
        if(datasetType.startsWith(IGNORE_SEISMIC))
            modelType = "prior";
        return modelType;
    }

    public String getActualDatasetType(){
        int index = datasetType.indexOf(IGNORE_SEISMIC);
        if(index == -1)
            return datasetType;
        return datasetType.substring(IGNORE_SEISMIC.length());
    }

    /**
     * if the datasetName is prior type of name, then convert it to post type of name
     * if the datasetName is post type of name, then convert it to prior type of name
     * @param datasetName
     * @return
     */
    public static String counterPartName(String datasetName){
    	String out = datasetName;
    	if(datasetName.contains("_post_"))
    		out = datasetName.replaceFirst("_post_", "_prior_");
    	else if(datasetName.contains("_prior_"))
    		out = datasetName.replaceFirst("_prior_","_post_");
    	return out;
    }
    
    public String getModelDatasetPrefix(boolean nearAndFar){
		String modelAfterDatasetBase = baseName + "_" + getModelType() + "_";
        String type = getActualDatasetType();
		if(!nearAndFar){
			modelAfterDatasetBase += "near_" + type + "_model";
		}else{
			modelAfterDatasetBase += "near_far_" + type + "_model";
		}
		return modelAfterDatasetBase;
	}

    public String getStddevModelDatasetPrefix(boolean nearAndFar){
		String modelAfterDatasetBase = baseName + "_" + getModelType() + "_";
		if(!nearAndFar){
			modelAfterDatasetBase += "near_" + DEFAULT_STDDEV_NAME + "_model";
		}else{
			modelAfterDatasetBase += "near_far_" + DEFAULT_STDDEV_NAME + "_model";
		}
		return modelAfterDatasetBase;
	}

}
