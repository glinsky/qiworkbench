/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code.
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.dnd.DnDConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.validator.routines.FloatValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bhpb.deliverylite.NumericParamsValidator.MODEL_PARAMS;
import com.bhpb.deliverylite.colorbar.Colorbar1;
import com.bhpb.deliverylite.marker.HorizonLineTableDecorator;
import com.bhpb.deliverylite.marker.HorizonLineTableModel;
import com.bhpb.deliverylite.marker.LineAttributes;
import com.bhpb.geographics.colorbar.ColorbarUtil;
import com.bhpb.geographics.util.ColorMap;
import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;
import com.bhpb.geoio.datasystems.DiscreteKeyRange;
import com.bhpb.geoio.filesystems.metadata.ModelFileMetadata;
import com.bhpb.geoio.filesystems.metadata.SeismicFileMetadata;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiwIOException;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IDataObject;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.client.util.GenericFileFilter;
import com.bhpb.qiworkbench.client.util.QiFileChooserDescriptor;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ComponentUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.IconResource;
import com.bhpb.qiworkbench.compAPI.JobManager;
import com.bhpb.qiworkbench.compAPI.JobMonitor;
import com.bhpb.qiworkbench.compAPI.ProgressUtil;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.bhpb.util.Util;
import com.bhpb.xsdparams.delivery.Inversion;
import com.bhpb.xsdparams.delivery.VarFloat;
import com.bhpb.xsdparams.delivery.VsFloat;
import java.beans.PropertyVetoException;

public class DeliveryLiteUI extends javax.swing.JInternalFrame implements MetadataSettable{
    private static Logger logger = Logger.getLogger(DeliveryLiteUI.class.getName());
    private static final long serialVersionUID = 1L;

    public static final int TABLE_ROW_HEIGHT = 21;
    public static final int TABLE_ROW_MARGIN = 1;
    public static final String SHALE_LAYER_TYPE = "Shale";
    public  static final String SAND_LAYER_TYPE  = "Sand";
    private static final String XML_MODEL_FILE = "delivery-lite-model.xml";
    private static final String XML_MODEL_FILE_PRIOR = "delivery-lite-model_prior.xml";
    private static final String XML_MODEL_FILE_POST = "delivery-lite-model_post.xml";
    private static final String XML_FILE = "delivery-lite.xml";
    private ShowModelParams showModelParams = new ShowModelParams();
    private Map<String,GeoFileMetadata> metadataMap = new HashMap<String,GeoFileMetadata>();
    private Map<String,IDataObject[]> dataMap = new HashMap<String,IDataObject[]>();
    public static final String _NEAR_SYNTHETIC_BEFORE = "_prior_near_seismic_ml";
    public static final String _NEAR_SYNTHETIC_AFTER = "_post_near_seismic";
    public static final String _FAR_SYNTHETIC_BEFORE = "_prior_far_seismic_ml";
    public static final String _FAR_SYNTHETIC_AFTER = "_post_far_seismic";
    private int modelAfterDatasetIndex = 0;
    private int modelAfterPropertyIndex = 0;
    /** Selected row */
    int selectedRow = -1;
    /** Layer type of each row */
    ArrayList<String> rowLayerTypes = new ArrayList<String>();
    /** Initial capacity of rowLayerTypes list */
    int initCapacity = 3;
    int tabIndex   = 0;
    int inputIndex = 0;

    InfoPanel infoPane;
    TextView  modelView;
    TextView  analyzerView;
    TextView  inversionView;

    /** Number of layers has been specified by user. This can only be done once. */
    boolean numLayersSpecified = false;
    private DeliveryLitePlugin agent;
    private Properties props;
    private String analyzerName = "gen_analyzer";
    private String invertName   = "gen_prior_post";
 

    private static SimpleDateFormat formatter
      = new SimpleDateFormat("yy_MM_dd");

    private LiteParams liteParams = new LiteParams();
    private IComponentDescriptor responseComponentDesc = null;
    Object [][] stackParams;
    Object [][] refSandParams;
    Object [][] mixShaleParams;
    Object [][] boundShaleParams;
    Object [][] grainParams;
    Object [][] refFluidParams;
    Object [][] fluidsParams;
    FluidModelProperties fluidsModel;
    String [][] topBot;
    private String currentOilSaturation = DeliveryLiteConstants.DEFAULT_OIL_SATURATION;
    private String currentOilSigmaSaturation;
    private String currentHighGasSaturation = DeliveryLiteConstants.DEFAULT_HIGH_GAS_SATURATION;
    private String currentHighGasSigmaSaturation;
    private String currentLowGasSaturation = DeliveryLiteConstants.DEFAULT_LOW_GAS_SATURATION;
    private String currentLowGasSigmaSaturation;
    private NumericFieldParams modelParams = new NumericFieldParams();
    private HorizonLineTableDecorator tableDecorator;
    //String [] titles = new String [] {"time", "err_t", "h", "err_h", "th", "err_th",
    String [] titles = new String [] {
            DeliveryLiteConstants.COLUMN_NAME_TIME,
            DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME, DeliveryLiteConstants.COLUMN_NAME_THICKNESS,
            DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS,DeliveryLiteConstants.COLUMN_NAME_TVDBML,
            DeliveryLiteConstants.COLUMN_NAME_LFIV, DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH,
            DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG, DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL,
            DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS, DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG};
    //"LFIV", "N/G", "err N/G", "Po", "So", "Pg", "Sg", "Plsg", "Slsg"};

    /**
     * keyed by model dataset name
     * values are keyed by property name
     */
    private Map<String,Map<String,Property>> modelAfterDatasetPropertyColorMap = new HashMap<String,Map<String,Property>>();
    
    /**
     * containing all property information including min, max, and its associated colorbar
     * keyed by property name valued by property object
     */
    private HashMap<String,Property> modelBeforePropertyColorMap = new HashMap<String,Property>();
    
    /**
     * containing currently loaded UNLABELED colorbar
     * keyed by colorbar name
    */
    private static Map<String,Colorbar1> currentUnlabeledColorbarMap = new HashMap<String,Colorbar1>();
    private JobMonitor monitor;
    private JobManager manager;
    private String[] deliveryAnalyserProperties;
    /** Creates new form DeliveryLiteUI */
    public DeliveryLiteUI(DeliveryLitePlugin agent, Node node) {
        this.agent = agent;

        infoPane = new InfoPanel(agent);
        infoPane.initPanel();

        modelView = new TextView(agent);
        modelView.initPanel();

        inversionView = new TextView(agent);
        inversionView.initPanel();

        analyzerView = new TextView(agent);
        analyzerView.initPanel();

        initTableParams();
        if(node == null)
            agent.getModel(true);
        else{

            agent.getModel(node);
        }
        populateTable();
        initComponents();
        executeMenu.remove(writeSeparator1);
        executeMenu.remove(writeAnalyzerScriptMenuItem);
        executeMenu.remove(writeDeliveryScriptsMenuItem);



        //Initialize the layer type of each row
        for (int i=0; i<initCapacity; i++) {
            rowLayerTypes.add(SHALE_LAYER_TYPE);
        }

        //Set up Reference Sand table, one of the matrix properties
        referenceSandTable.setModel(new javax.swing.table.DefaultTableModel(
            refSandParams,
            new String [] {
                "error", "A", "B", "C"
            }
        ){
            boolean[][] canEdit = new boolean [][] {
                    {true, true, true, true},
                    {true, true, true, false},
                    {true, true, true, false}
            };
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[rowIndex][columnIndex];
            }
        });
        //set column preferred width
        TableColumn column = referenceSandTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = referenceSandTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = referenceSandTable.getColumnModel().getColumn(2);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = referenceSandTable.getColumnModel().getColumn(3);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column.setCellRenderer(new TableCellColorRenderer2());
        
        referenceSandTable.setCellSelectionEnabled(true);
        //set size of table
        referenceSandTable.setMaximumSize(new java.awt.Dimension(440, 48));
        referenceSandTable.setMinimumSize(new java.awt.Dimension(440, 48));
        referenceSandTable.setPreferredSize(new java.awt.Dimension(440, 48));
        referenceSandScrollPane.setViewportView(referenceSandTable);
        //make column headers bold font
        JTableHeader header = referenceSandTable.getTableHeader();
        header.setReorderingAllowed(false);
        final Font boldFont = header.getFont().deriveFont(Font.BOLD);
        final TableCellRenderer headerRenderer = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

        new TableCellValidator(this,referenceSandTable,false);

        //Set up Mixing Shale table, one of the matrix properties
        mixingShaleTable.setModel(new javax.swing.table.DefaultTableModel(
            mixShaleParams,
            new String [] {
                "error", "A", "B", "C"
            }
        ){
            boolean[][] canEdit = new boolean [][] {
                    {true, true, true, true},
                    {true, true, true, false},
                    {true, true, true, false}
            };
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[rowIndex][columnIndex];
            }
        });

        //set column preferred width
        column = mixingShaleTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = mixingShaleTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = mixingShaleTable.getColumnModel().getColumn(2);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = mixingShaleTable.getColumnModel().getColumn(3);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column.setCellRenderer(new TableCellColorRenderer2());
        mixingShaleTable.setCellSelectionEnabled(true);
        //set size of table
        mixingShaleTable.setMaximumSize(new java.awt.Dimension(440, 48));
        mixingShaleTable.setMinimumSize(new java.awt.Dimension(440, 48));
        mixingShaleTable.setPreferredSize(new java.awt.Dimension(440, 48));
        mixingShaleScrollPane.setViewportView(mixingShaleTable);
        //make column headers bold font
        header = mixingShaleTable.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer2 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer2.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
        new TableCellValidator(this,mixingShaleTable,false);
        //Set up Bounding Shale table, one of the matrix properties
        boundingShaleTable.setModel(new javax.swing.table.DefaultTableModel(
            boundShaleParams,
            new String [] {
                "error", "A", "B", "C"
            }
        ){
            boolean[][] canEdit = new boolean [][] {
                    {true, true, true, true},
                    {true, true, true, false},
                    {true, true, true, false}
            };
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[rowIndex][columnIndex];
            }
        });

        //set column preferred width
        column = boundingShaleTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = boundingShaleTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = boundingShaleTable.getColumnModel().getColumn(2);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = boundingShaleTable.getColumnModel().getColumn(3);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column.setCellRenderer(new TableCellColorRenderer2());
        boundingShaleTable.setCellSelectionEnabled(true);
        //set size of table
        boundingShaleTable.setMaximumSize(new java.awt.Dimension(440, 48));
        boundingShaleTable.setMinimumSize(new java.awt.Dimension(440, 48));
        boundingShaleTable.setPreferredSize(new java.awt.Dimension(440, 48));
        boundingShaleScrollPane.setViewportView(boundingShaleTable);
        //make column headers bold font
        header = boundingShaleTable.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer3 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer3.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
        new TableCellValidator(this,boundingShaleTable,false);
        //Set up Grain table, one of the matrix properties
        grainTable.setModel(new javax.swing.table.DefaultTableModel(
                grainParams,
            new String [] {
                "vp", "vs", "rho"
            }
        ));

        //set size of table
        grainTable.setMaximumSize(new java.awt.Dimension(340, 48));
        grainTable.setMinimumSize(new java.awt.Dimension(340, 48));
        grainTable.setPreferredSize(new java.awt.Dimension(340, 48));
        grainScrollPane.setViewportView(grainTable);
        //make column headers bold font
        header = grainTable.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer4 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer4.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

        new TableCellValidator(this,grainTable,false);
         //Set up Reference Fluid table, one of the matrix properties
        referenceFluidTable.setModel(new javax.swing.table.DefaultTableModel(
            refFluidParams,
            new String [] {
                "vp", "rho"
            }
        ));
        referenceFluidTable.setEnabled(true);
        //set column preferred width
        column = referenceFluidTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = referenceFluidTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        referenceFluidTable.setCellSelectionEnabled(true);
        //set size of table
        referenceFluidTable.setMaximumSize(new java.awt.Dimension(240, 24));
        referenceFluidTable.setMinimumSize(new java.awt.Dimension(240, 24));
        referenceFluidTable.setPreferredSize(new java.awt.Dimension(240, 24));
        referenceFluidScrollPane.setViewportView(referenceFluidTable);
        //make column headers bold font
        header = referenceFluidTable.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer5 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer5.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
        new TableCellValidator(this,referenceFluidTable,false);
        //Set up Stack Parameters table, one of the matrix properties
        stackParametersTable.setModel(new javax.swing.table.DefaultTableModel(
        		stackParams,
        		new String [] {"Xmin (ft)", "Xmax", "ve (ft/s)", "time (ms)"}
        	){
        	public boolean isCellEditable(int row, int col) {
        		if(row ==1 && !isNearAndFar())
        			return false;
        		return true;
        	}
        });
        
        TableCellColorRenderer2 cellRenderer3 = new TableCellColorRenderer2();
        Enumeration<TableColumn> cols = stackParametersTable.getColumnModel().getColumns();
        while(cols.hasMoreElements()){
        	TableColumn col = cols.nextElement();
        	col.setCellRenderer(cellRenderer3);
    	}
        //set column preferred width
        column = stackParametersTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = stackParametersTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = stackParametersTable.getColumnModel().getColumn(2);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        column = stackParametersTable.getColumnModel().getColumn(3);
        column.setPreferredWidth(100); column.setMinWidth(100); column.setMaxWidth(100);
        column.setResizable(false);
        stackParametersTable.setCellSelectionEnabled(true);
        //set size of table
        stackParametersTable.setMaximumSize(new java.awt.Dimension(440, 48));
        stackParametersTable.setMinimumSize(new java.awt.Dimension(440, 48));
        stackParametersTable.setPreferredSize(new java.awt.Dimension(440, 48));
        stackParametersScrollPane.setViewportView(stackParametersTable);
        //make column headers bold font
        header = stackParametersTable.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer6 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer6.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });
        new TableCellValidator(this,stackParametersTable,false);
        //Set up Fluids column header
        fluidsTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                //"brine", "oil", "gas", "High", "Low"
                "brine", "oil", "gas", "oil", "high gas", "low gas"
            }
        ));
        //((DefaultTableModel)fluidsTable1.getModel()).fireTableDataChanged();
        //set column preferred width
        column = fluidsTable1.getColumnModel().getColumn(0);
        column.setPreferredWidth(160); column.setMinWidth(160); column.setMaxWidth(160);
        column.setResizable(false);
        column = fluidsTable1.getColumnModel().getColumn(1);
        column.setPreferredWidth(160); column.setMinWidth(160); column.setMaxWidth(160);
        column.setResizable(false);
        column = fluidsTable1.getColumnModel().getColumn(2);
        column.setPreferredWidth(160); column.setMinWidth(160); column.setMaxWidth(160);
        column.setResizable(false);
        column = fluidsTable1.getColumnModel().getColumn(3);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable1.getColumnModel().getColumn(4);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable1.getColumnModel().getColumn(5);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);

        fluidsTable1.setCellSelectionEnabled(true);
        //set size of table
        fluidsTable1.setMaximumSize(new java.awt.Dimension(720, 48));
        fluidsTable1.setMinimumSize(new java.awt.Dimension(720, 48));
        fluidsTable1.setPreferredSize(new java.awt.Dimension(720, 48));
        fluidsScrollPane1.setViewportView(fluidsTable1);
        //make column headers bold font
        header = fluidsTable1.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer7 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer7.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

        //Set up Fluids table
        fluidsTable2.setModel(new FluidPropertiesTableModel(fluidsModel,this));
        
        /*fluidsTable2.setModel(new javax.swing.table.DefaultTableModel(
            fluidsParams,
            new String [] {
                //"vp", "rho", "vp", "rho", "vp", "rho", "saturation", "saturation"
                "vp", "rho", "vp", "rho", "vp", "rho", "saturation", "saturation", "saturation"
            }
        ));
        ((DefaultTableModel)fluidsTable2.getModel()).fireTableDataChanged();
         */
        //set column preferred width
        column = fluidsTable2.getColumnModel().getColumn(0);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(1);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(2);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(3);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(4);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(5);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(6);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(7);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        column = fluidsTable2.getColumnModel().getColumn(8);
        column.setPreferredWidth(80); column.setMinWidth(80); column.setMaxWidth(80);
        column.setResizable(false);
        new TableCellValidator(this,fluidsTable2,true);
        fluidsTable2.setCellSelectionEnabled(true);
        //set size of table
        //fluidsTable2.setMaximumSize(new java.awt.Dimension(640, 48));
        //fluidsTable2.setMinimumSize(new java.awt.Dimension(640, 48));
        //fluidsTable2.setPreferredSize(new java.awt.Dimension(640, 48));
        fluidsTable2.setMaximumSize(new java.awt.Dimension(720, 48));
        fluidsTable2.setMinimumSize(new java.awt.Dimension(720, 48));
        fluidsTable2.setPreferredSize(new java.awt.Dimension(720, 48));
        fluidsScrollPane2.setViewportView(fluidsTable2);
        //make column headers bold font
        header = fluidsTable2.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer8 = header.getDefaultRenderer();
        header.setDefaultRenderer( new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer8.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

//        fixedLayerTable = new JTable(new FixedLayerTableModel());
        fixedLayerTable.setModel(new FixedLayerTableModel());
        //only one row at a time can be selected
        fixedLayerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        fixedLayerTable.setRowSelectionAllowed(true);
        //be notified when a row is selected
        ListSelectionModel rowSM = fixedLayerTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) return;

                ListSelectionModel lsm = (ListSelectionModel)e.getSource();
                if (lsm.isSelectionEmpty()) {
                    selectedRow = -1;
                } else {
                    selectedRow = lsm.getMinSelectionIndex();
                }
            }
        });

        //set column preferred width
        //  layer number
        column = fixedLayerTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(30); column.setMinWidth(30); column.setMaxWidth(30);
        column.setResizable(false);
        //  analyze check box
        column = fixedLayerTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(60); column.setMinWidth(60); column.setMaxWidth(60);
        column.setResizable(false);
        //  type of rock: Shale or Sand
        column = fixedLayerTable.getColumnModel().getColumn(2);
        JComboBox typeComboBox = new JComboBox();
        typeComboBox.addItem(SHALE_LAYER_TYPE);
        typeComboBox.addItem(SAND_LAYER_TYPE);
        typeComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeComboBoxActionPerformed(evt);
            }
        });
        column.setCellEditor(new DefaultCellEditor(typeComboBox));
        column.setPreferredWidth(60); column.setMinWidth(60); column.setMaxWidth(60);
        column.setResizable(false);
        //  layer name
        column = fixedLayerTable.getColumnModel().getColumn(3);
        column.setPreferredWidth(300); //column.setMinWidth(300); column.setMaxWidth(300);
        //column.setResizable(false);
        fixedLayerTable.setCellSelectionEnabled(true);
        //set size of table
        fixedLayerTable.setMaximumSize(new java.awt.Dimension(260, 600));
        fixedLayerTable.setMinimumSize(new java.awt.Dimension(260, 380));
        fixedLayerTable.setPreferredScrollableViewportSize(new Dimension(260, 600));
        fixedLayerScrollPane.setViewportView(fixedLayerTable);
        //make column headers bold font
        header = fixedLayerTable.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer9 = header.getDefaultRenderer();
        header.setDefaultRenderer(new DefaultTableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer9.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
//              setHorizontalAlignment(SwingConstants.CENTER);
                return comp;
            }
        });
        //set cell renderer. Background color depends on layer type
        TableCellColorRenderer cellRenderer = new TableCellColorRenderer();
        fixedLayerTable.setDefaultRenderer(String.class, cellRenderer);
        fixedLayerTable.setRowHeight(TABLE_ROW_HEIGHT);
        fixedLayerTable.setRowMargin(TABLE_ROW_MARGIN);
        layerTable.setRowHeight(TABLE_ROW_HEIGHT);
        layerTable.setRowMargin(TABLE_ROW_MARGIN);
//        fixedLayerTable.setDefaultRenderer(JCheckBox.class, cellRenderer);

        //Set up horizontal scrollable part of layer table used to create the Delivery model
        //layerTable.setModel(new javax.swing.table.DefaultTableModel(topBot, titles));
        layerTable.setModel(new LayerTableModel(topBot, titles));
        HorizonLineTableModel layerTableModel = new HorizonLineTableModel(this,topBot, titles);
        layerTable.setModel(layerTableModel);
        
        tableDecorator = new HorizonLineTableDecorator(this,layerTable,fixedLayerTable.getModel());
         ExcelAdapter myAd = new ExcelAdapter(layerTable, this);
        //layerTable.setModel(new LayerTableModel());


        //only one row at a time can be selected
        //layerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        layerTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        layerTable.setRowSelectionAllowed(true);
        //be notified when a row is selected
        rowSM = layerTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) return;

                ListSelectionModel lsm = (ListSelectionModel)e.getSource();
                if (lsm.isSelectionEmpty()) {
                    selectedRow = -1;
                } else {
                    selectedRow = lsm.getMinSelectionIndex();
                }
            }
        });

        // Disable auto resizing to make the table horizontal scrollable
        layerTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //set column preferred width

        column = layerTable.getColumnModel().getColumn(0);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(2);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(3);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(4);
        column.setPreferredWidth(70); column.setMinWidth(70); column.setMaxWidth(70);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(5);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(6);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(7);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(8);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(9);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);
        column = layerTable.getColumnModel().getColumn(10);
        column.setPreferredWidth(50); column.setMinWidth(50); column.setMaxWidth(50);
        column.setResizable(false);

        layerTable.setCellSelectionEnabled(true);
        //set size of table
        layerTable.setMaximumSize(new java.awt.Dimension(1660, 600));
        layerTable.setMinimumSize(new java.awt.Dimension(800, 380));
        layerTable.setPreferredScrollableViewportSize(new Dimension(500, 600));
        layerScrollPane.setViewportView(layerTable);
        layerTable.addNotify();
        layerScrollPane.setPreferredSize(new Dimension(573, 70));
        //make column headers bold font
        header = layerTable.getTableHeader();
        header.setReorderingAllowed(false);
        final TableCellRenderer headerRenderer10 = header.getDefaultRenderer();
        header.setDefaultRenderer( new DefaultTableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component comp = headerRenderer10.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont( boldFont );
                comp.setBackground(new Color(214, 214, 249));
                return comp;
            }
        });

        //set cell renderer. Background color depends on layer type
        //TableCellColorRenderer1 cellRenderer2 = new TableCellColorRenderer1();

        //layerTable.setDefaultRenderer(String.class, cellRenderer2);

        //Group the stack radio buttons.
        ButtonGroup stackGroup = new ButtonGroup();
        stackGroup.add(nearOnlyRadioButton);
        stackGroup.add(nearAndFarRadioButton);

        //make frame iconable and maximizable.
        this.setIconifiable(true);
        this.setMaximizable(true);
        this.setResizable(true);
        
        
        modelParams.setSyntheticSeismicStartTime(Float.valueOf(tmin4SyntheticSeismicTextField.getText().trim()).floatValue());
        modelParams.setSyntheticSeismicEndTime(Float.valueOf(tmax4SyntheticSeismicTextField.getText().trim()).floatValue());
        modelParams.setSpaghettiPlotStartTime(Float.valueOf(tmin4SpaghettiPlotTextField.getText().trim()).floatValue());
        modelParams.setSpaghettiPlotEndTime(Float.valueOf(tmax4SpaghettiPlotTextField.getText().trim()).floatValue());
        modelParams.setNumberRuns(Integer.valueOf(numberRunsTextField.getText().trim()).intValue());
        
        NumericParamsValidator validator = new NumericParamsValidator(this,modelParams);
        validator.addListeners(timeBaseTextField, MODEL_PARAMS.TIME_BASE);
        validator.addListeners(sigmaTimeBaseTextField,MODEL_PARAMS.SIGMA_TIMEBASE);
        validator.addListeners(masterDepthLayerTextField,MODEL_PARAMS.MASTER_DEPTH_LAYER);
        validator.addListeners(errorOfMasterLayerDepthTextField,MODEL_PARAMS.ERROR_MASTER_LAYER_DEPTH);
        validator.addListeners(depthOfMasterLayerTextField,MODEL_PARAMS.DEPTH_MASTER_LAYER);
        
        validator.addListeners(tmin4SyntheticSeismicTextField,MODEL_PARAMS.SYNTHETIC_SEISMIC_START_TIME);
        validator.addListeners(tmax4SyntheticSeismicTextField,MODEL_PARAMS.SYNTHETIC_SEISMIC_END_TIME);
        validator.addListeners(tmin4SpaghettiPlotTextField,MODEL_PARAMS.SPAGHETTI_PLOT_START_TIME);
        validator.addListeners(tmax4SpaghettiPlotTextField,MODEL_PARAMS.SPAGHETTI_PLOT_END_TIME);
        
        validator.addListeners(numberRunsTextField,MODEL_PARAMS.NUMBER_RUNS);
        validator.addListeners(cdpTextField,MODEL_PARAMS.SINGLE_XLINE);
        validator.addListeners(epTextField,MODEL_PARAMS.SINGLE_INLINE);
        validator.addListeners(nearNoiseRMSTextField,MODEL_PARAMS.NEAR_NOISE_RMS);
        validator.addListeners(farNoiseRMSTextField,MODEL_PARAMS.FAR_NOISE_RMS);
        validator.addListeners(avoATextField,MODEL_PARAMS.AVO_A);
        validator.addListeners(avoBTextField,MODEL_PARAMS.AVO_B);
        
        farStackFilenameTextField.setEditable(false);
        farStackWaveletFilenameTextField.setEditable(false);
        farNoiseRMSTextField.setEditable(false);
        //validateProject();
        populate();
        Inversion inversion = (Inversion)agent.getInversion();
        modelNameTextField.setText(inversion.getInversionInfo().getName());
        //Set the Prior and Post Realisations file names to default values.
        //autoSetIndex2();
        fillCurrentUnlabeledColorbarMap();
        populatePropertyColorMap();

        // Disable ability to display the results until script execution finishes.
		displayResultsButton.setEnabled(false);
        displayResultsMenuItem.setEnabled(false);
    }

    /*
     * suffix .dat will be added when needed and this will be a key to dataMap and metadataMap
     */
    public String getNearSyntheticBeforeDatasetName(){
    	return getOutputBasename() + _NEAR_SYNTHETIC_BEFORE;
    }

    public String getFarSyntheticBeforeDatasetName(){
    	return getOutputBasename() + _FAR_SYNTHETIC_BEFORE;
    }

    public void showValidationFailure(String message){
        com.bhpb.geographics.ui.ValidatingPanelAdapter.showValidationFailure(message, this);
    }

    /*
     * suffix .dat will be added when needed and this will be a key to dataMap and metadataMap
     */
    public String getNearSyntheticAfterDatasetName(){
    	return getOutputBasename() + _NEAR_SYNTHETIC_AFTER;
    }

    public String getFarSyntheticAfterDatasetName(){
    	return getOutputBasename() + _FAR_SYNTHETIC_AFTER;
    }

    /**
     * Take initial list of colorbar names, the default colorbar names initially assigned
     * to the model properties, construct the Colorbar object and contruct a colormap keyed by
     * the colorbar name with the colorbar object as value object.
     */
    private void fillCurrentUnlabeledColorbarMap(){
        for(String s : DeliveryLiteConstants.colorbars){
        	ColorMap colorMap = null;
        	try{
        		colorMap = ColorbarUtil.getColorMapByResourceName(s);
        	}catch(IOException e){
        		e.printStackTrace();
      		   	logger.warning("IO problem occurred while getting colormap object by using colorbar name=" + s);
        	}
        	if(colorMap != null)
        		currentUnlabeledColorbarMap.put(s, new Colorbar1(s,colorMap));
        }
        showModelParams.setCurrentAvailableColorbars(currentUnlabeledColorbarMap);
    }

    public float[] getLayerBoundries(){
    	TableModel model = layerTable.getModel();
    	float[] fs = new float[model.getRowCount()+1]; 
    	int columnIndex = getColIndexByPropertyName(DeliveryLiteConstants.COLUMN_NAME_TIME);
    	int i;
    	for(i = 0; i < model.getRowCount(); i++){
    		String val = (String)model.getValueAt(i, columnIndex);
    		float fval = 0;
    		try{
    			fval = Float.parseFloat(val);
    		}catch(NumberFormatException e){
    			e.printStackTrace();
    		}
    		fs[i] = fval;
    	}
    	float fval = 0;
		try{
			fval = Float.parseFloat(getTimeBase());
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
    	fs[i] = fval;
    	return fs;
    }
    public void setModelAfterDatasetIndex(int index){
    	modelAfterDatasetIndex = index;
    }
    
    public int getModelAfterDatasetIndex(){
    	return modelAfterDatasetIndex;
    }
    
    public void setModelAfterPropertyIndex(int index){
    	modelAfterPropertyIndex = index;
    }

    public int getModelAfterPropertyIndex(){
    	return modelAfterPropertyIndex;
    }
    /**
     * Each property is initially assigned a default colorbar object.  Take loaded colorbar
     * objects and assign them to each property; find the minimum and maximun value for each
     * property, and form the colormap propertyColorMap
     */
    public void populatePropertyColorMap(){
        //propertyColorMap.clear();
    	modelBeforePropertyColorMap = (HashMap)DeliveryLiteConstants.default_property_range.clone();
        TableModel model = getLayerTable().getModel();
        int count = model.getColumnCount();
        for(int i = 0; i < count; i++){
            String name = model.getColumnName(i);
            
            Property p = modelBeforePropertyColorMap.get(name);
            float min = 0;
            float max = 0;
            if(p.getMin() == Float.MIN_VALUE)
            	min = getMinByPropertyName(name);
            else
            	min = p.getMin();
            if(p.getMax() == Float.MAX_VALUE){
            	if(name.equals(DeliveryLiteConstants.COLUMN_NAME_TIME))
            		max = Float.parseFloat(getTimeBase());
            	else
            		max = getMaxByPropertyName(name);
        	}else
            	max = p.getMax();

            //Colorbar colorbar = null;
            Colorbar1 colorbar = null;
            if(DeliveryLiteConstants.default_property_color_map.containsKey(name)){
                String colorbarName = DeliveryLiteConstants.default_property_color_map.get(name);
                if(!currentUnlabeledColorbarMap.containsKey(colorbarName)){
                    //colorbar = ColorbarUtil.getUnlabeledColorbarByName(colorbarName);
                   ColorMap colorMap = null;
             	   try{
             		   colorMap = ColorbarUtil.getColorMapByResourceName(colorbarName);
             	   }catch(IOException e){
             		   e.printStackTrace();
             		   logger.warning("IO problem occurred while getting colormap object by using colorbar name=" + colorbarName);
             	   }
                   colorbar = new Colorbar1(colorbarName,colorMap);
                    //if(colorbar != null)
                    currentUnlabeledColorbarMap.put(colorbarName, colorbar);
                }else{
                    colorbar = currentUnlabeledColorbarMap.get(colorbarName);
                }
                Colorbar1 cb = new Colorbar1(colorbar);
                com.bhpb.deliverylite.colorbar.ColorbarUtil.labeledColorbar(cb, min, max);
                Property prop = new Property(name,min,max, cb);
                modelBeforePropertyColorMap.put(name, prop);
                showModelParams.setCurrentAvailableColorbars(currentUnlabeledColorbarMap);
            }
        }
    }

    private float getMinByPropertyName(String name){
        int numLayer = getLayerTable().getModel().getRowCount();
        int column = getColIndexByPropertyName(name);
        float min = 0;
        for(int i = 0; i < numLayer; i++){
            String val = (String)getLayerTable().getValueAt(i, column);
            float fval = 0;
            if(val != null && val.trim().length() > 0){
                fval = Float.parseFloat(val);
                if(i == 0)
                    min = fval;
                if(fval <= min)
                    min = fval;
            }
        }
        return min;
    }

    public int getColIndexByPropertyName(String name){
        JTable table = getLayerTable();
        for(int j = 0; j < table.getModel().getColumnCount(); j++){
            if(name.equals(table.getModel().getColumnName(j)))
                return j;
        }
        return -1;

    }
    
    public void setDeliveryAnalyserProperties(String[] props){
    	if(props != null)
    		deliveryAnalyserProperties = props.clone();
    	else
    		deliveryAnalyserProperties = null;
    }
    
    public String[] getDeliveryAnalyserProperties(){
    	if(deliveryAnalyserProperties != null)
    		return deliveryAnalyserProperties.clone();
    	else
    		return null;
    }
    
    private float getMaxByPropertyName(String name){
        int numLayer = getLayerTable().getModel().getRowCount();
        int column = getColIndexByPropertyName(name);
        float max = 0;
        for(int i = 0; i < numLayer; i++){
            String val = (String)getLayerTable().getValueAt(i, column);
            float fval = 0;
            if(val != null && val.trim().length() > 0){
                fval = Float.parseFloat(val);
                if(i == 0)
                    max = fval;
                if(fval >= max)
                    max = fval;
            }
        }
        return max;
    }


    private double[][] nearRangeInfo = {{-1,-1,1},{-1,-1,1},{0,0,1}}; //ep,cdp,time
    private double[][] farRangeInfo = {{-1,-1,1},{-1,-1,1},{0,0,1}}; //ep,cdp,time

    private void setNearStackRange(SeismicFileMetadata metadata){
        if(metadata == null)
            return;
        nearStackFilenameTextField.setText(metadata.getGeoFileName());
        ArrayList<String> keys = metadata.getKeys();
        for(int i = 0; keys != null && i < keys.size(); i++){
            if(keys.get(i).equals("ep")){
                DiscreteKeyRange range = metadata.getKeyRange(keys.get(i));
                nearInlineTextField.setText(String.valueOf(range.getMin()));
                nearInlineToTextField.setText(String.valueOf(range.getMax()));
                nearInlineByTextField.setText(String.valueOf(range.getIncr()));
                nearRangeInfo[0][0] = range.getMin();
                nearRangeInfo[0][1] = range.getMax();
                nearRangeInfo[0][2] = range.getIncr();
                if(range.getMin() == range.getMax()){
                	epTextField.setText(String.valueOf(range.getMin()));
                	modelParams.setSingleInline((float)range.getMin());
                }
                	
            }else if(keys.get(i).equals("cdp")){
                DiscreteKeyRange range = metadata.getKeyRange(keys.get(i));
                nearCrosslineTextField.setText(String.valueOf(range.getMin()));
                nearCrosslineToTextField.setText(String.valueOf(range.getMax()));
                nearCrosslineByTextField.setText(String.valueOf(range.getIncr()));
                nearRangeInfo[1][0] = range.getMin();
                nearRangeInfo[1][1] = range.getMax();
                nearRangeInfo[1][2] = range.getIncr();
                if(range.getMin() == range.getMax()){
                	cdpTextField.setText(String.valueOf(range.getMin()));
                	modelParams.setSingleXline((float)range.getMin());
                }
            }else if(keys.get(i).equals("tracl")){
                DiscreteKeyRange range = metadata.getKeyRange(keys.get(i));
                nearRangeInfo[2][0] = range.getMin();
                nearRangeInfo[2][1] = range.getMax();
                nearRangeInfo[2][2] = range.getIncr();
            }
        }
        nearInlineTextField.setEditable(false);
        nearInlineToTextField.setEditable(false);
        nearInlineByTextField.setEditable(false);
        nearCrosslineTextField.setEditable(false);
        nearCrosslineToTextField.setEditable(false);
        nearCrosslineByTextField.setEditable(false);
    }
	
	public double[] getNearEpRange() {
		double[] epRange = new double[3];
		epRange[0] = nearRangeInfo[0][0];  //min
		epRange[1] = nearRangeInfo[0][1];  //max
		epRange[2] = nearRangeInfo[0][2];  //incr
		return epRange;
	}
	
	public double[] getNearCdpRange() {
		double[] cdpRange = new double[3];
		cdpRange[0] = nearRangeInfo[1][0];  //min
		cdpRange[1] = nearRangeInfo[1][1];  //max
		cdpRange[2] = nearRangeInfo[1][2];  //incr
		return cdpRange;
	}

    public void setFarStackWaveletFile(String filePath){
        //String datasetPath = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
        int index = filePath.lastIndexOf(agent.getMessagingMgr().getServerOSFileSeparator());
        filePath = filePath.substring(index+1);
        farStackWaveletFilenameTextField.setText(filePath);
    }

    public void setNearStackWaveletFile(String filePath){
        //String datasetPath = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
        int index = filePath.lastIndexOf(agent.getMessagingMgr().getServerOSFileSeparator());
        filePath = filePath.substring(index+1);
        nearStackWaveletFilenameTextField.setText(filePath);
    }

    public String getOutputBasename(){
    	return outputBaseNameTextField.getText().trim();
    }

    public String getNearStackFileName(){
        return nearStackFilenameTextField.getText().trim();
    }

    public String getNearWaveletFileName(){
        return nearStackWaveletFilenameTextField.getText().trim();
    }

    public String getFarWaveletFileName(){
        return farStackWaveletFilenameTextField.getText().trim();
    }

    public String getFarStackFileName(){
        return farStackFilenameTextField.getText().trim();
    }

    public boolean isNearAndFar(){
        return nearAndFarRadioButton.isSelected();
    }

    public DeliveryLitePlugin getAgent(){
        return agent;
    }

    public Map<String,Property> getModelBeforePropertyColorMap(){
        return modelBeforePropertyColorMap;
    }

    public Map<String,Map<String,Property>> getModelAfterDatasetPropertyColorMap(){
        return modelAfterDatasetPropertyColorMap;
    }
    
    public static Map<String,Colorbar1> getAvailableColorbars(){
        return currentUnlabeledColorbarMap;
    }

    public String getTimeBase(){
        return timeBaseTextField.getText();
    }

    public String getSigmaTimeBase(){
        return timeBaseTextField.getText();
    }

    public String getMinTime(){
        return (String)layerTable.getValueAt(0, 0);
    }

    public void setFarStackRange(SeismicFileMetadata metadata){
        if(metadata == null)
            return;
        farStackFilenameTextField.setText(metadata.getGeoFileName());
        ArrayList<String> keys = metadata.getKeys();
        for(int i = 0; keys != null && i < keys.size(); i++){
            if(keys.get(i).equals("ep")){
                DiscreteKeyRange range = metadata.getKeyRange(keys.get(i));
                farInlineTextField.setText(String.valueOf(range.getMin()));
                farInlineToTextField.setText(String.valueOf(range.getMax()));
                farInlineByTextField.setText(String.valueOf(range.getIncr()));
                farRangeInfo[0][0] = range.getMin();
                farRangeInfo[0][1] = range.getMax();
                farRangeInfo[0][2] = range.getIncr();
                if(range.getMin() == range.getMax()){  //single trace automatically assign the ep field value
                	epTextField.setText(String.valueOf(range.getMin()));
                	modelParams.setSingleInline((float)range.getMin());
                }
            }else if(keys.get(i).equals("cdp")){
                DiscreteKeyRange range = metadata.getKeyRange(keys.get(i));
                farCrosslineTextField.setText(String.valueOf(range.getMin()));
                farCrosslineToTextField.setText(String.valueOf(range.getMax()));
                farCrosslineByTextField.setText(String.valueOf(range.getIncr()));
                farRangeInfo[1][0] = range.getMin();
                farRangeInfo[1][1] = range.getMax();
                farRangeInfo[1][2] = range.getIncr();
                if(range.getMin() == range.getMax()){//single trace automatically assign the cdp field value
                	cdpTextField.setText(String.valueOf(range.getMin()));
                	modelParams.setSingleXline((float)range.getMin());
                }
            }else if(keys.get(i).equals("tracl")){
                DiscreteKeyRange range = metadata.getKeyRange(keys.get(i));
                farRangeInfo[2][0] = range.getMin();
                farRangeInfo[2][1] = range.getMax();
                farRangeInfo[2][2] = range.getIncr();
            }
        }
        farInlineTextField.setEditable(false);
        farInlineToTextField.setEditable(false);
        farInlineByTextField.setEditable(false);
        farCrosslineTextField.setEditable(false);
        farCrosslineToTextField.setEditable(false);
        farCrosslineByTextField.setEditable(false);
    }
	
	public double[] getFarEpRange() {
		double[] epRange = new double[3];
		epRange[0] = farRangeInfo[0][0];  //min
		epRange[1] = farRangeInfo[0][1];  //max
		epRange[2] = farRangeInfo[0][2];  //incr
		return epRange;
	}
	
	public double[] getFarCdpRange() {
		double[] cdpRange = new double[3];
		cdpRange[0] = farRangeInfo[1][0];  //min
		cdpRange[1] = farRangeInfo[1][1];  //max
		cdpRange[2] = farRangeInfo[1][2];  //incr
		return cdpRange;
	}

    /**
     * Initialize the table model parameters.
     */
    private void initTableParams() {
        stackParams = new Object [][] {
                {null, null, null, null},
                {null, null, null, null}
        };

        refSandParams = new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
        };

        mixShaleParams = new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
        };

        boundShaleParams = new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
        };

        grainParams = new Object [][] {
                {null, null, null}
        };

        refFluidParams = new Object [][] {
                {null, null}
        };

        //fluidsParams = new Object [][] {
        //        {null, null, null, null, null, null, DeliveryLiteConstants.DEFAULT_OIL_SATURATION, DeliveryLiteConstants.DEFAULT_HIGH_GAS_SATURATION, DeliveryLiteConstants.DEFAULT_LOW_GAS_SATURATION},
        //        {null, null, null, null, null, null, null, null, null}
        //};
        fluidsModel = new FluidModelProperties();
        fluidsParams = fluidsModel.toTableArray();

        topBot = new String [][] {
                {"", "", "", "", "", "", "", "", "", "", "", "","", ""},
                {"", "", "", "", "", "", "", "", "", "", "", "", "", ""}
        };
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        landmarkInPanel = new javax.swing.JPanel();
        landmarkProjectLabel = new javax.swing.JLabel();
        projectTextField = new javax.swing.JTextField();
        listProjectsButton = new javax.swing.JButton();
        volumeLabel = new javax.swing.JLabel();
        voumeTextField = new javax.swing.JTextField();
        listVolumesButton = new javax.swing.JButton();
        landmarkRangeLabel = new javax.swing.JLabel();
        rangeLinesLabel = new javax.swing.JLabel();
        linesTextField = new javax.swing.JTextField();
        linesToLabel = new javax.swing.JLabel();
        linesToTextField = new javax.swing.JTextField();
        linesByLabel = new javax.swing.JLabel();
        linesByTextField = new javax.swing.JTextField();
        tracesLabel = new javax.swing.JLabel();
        tracesTextField = new javax.swing.JTextField();
        tracesToLabel = new javax.swing.JLabel();
        tracesToTextField = new javax.swing.JTextField();
        tracesByLabel = new javax.swing.JLabel();
        tracesByTextField = new javax.swing.JTextField();
        entireVolumeButton = new javax.swing.JButton();
        selectButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        deliveryLiteTabbedPane = new javax.swing.JTabbedPane();
        parametersPanel = new javax.swing.JPanel();
        inputsTabbedPane = new javax.swing.JTabbedPane();
        projectPanel = new javax.swing.JPanel();
        projectMetadataPanel = new javax.swing.JPanel();
        projectRelDirLabel1 = new javax.swing.JLabel();
        projectRelDirLabel2 = new javax.swing.JLabel();
        projectNameLabel1 = new javax.swing.JLabel();
        projectNameLabel2 = new javax.swing.JLabel();
        seismicRelDirLabel1 = new javax.swing.JLabel();
        seismicRelDirLabel2 = new javax.swing.JLabel();
        datasetsRelDirLabel1 = new javax.swing.JLabel();
        datasetsRelDirLabel2 = new javax.swing.JLabel();
        modelRelDirLabel1 = new javax.swing.JLabel();
        modelRelDirLabel2 = new javax.swing.JLabel();
        outputRelDirLabel1 = new javax.swing.JLabel();
        outputRelDirLabel2 = new javax.swing.JLabel();
        projectInfoLabel = new javax.swing.JLabel();
        inputPanel = new javax.swing.JPanel();
        modelParametersPanel = new javax.swing.JPanel();
        nearStackFilenameLabel = new javax.swing.JLabel();
        //nearStackFilenameTextField = new javax.swing.JTextField();
        nearStackFilenameTextField= new DropTextField(DnDConstants.ACTION_COPY, this, "nearStackFilename");
        farStackFilenameLabel = new javax.swing.JLabel();
        farStackFilenameTextField = new DropTextField(DnDConstants.ACTION_COPY, this, "farStackFilename");
        nearStackWaveletFilenameLabel = new javax.swing.JLabel();
        nearStackWaveletFilenameTextField = new DropTextField(DnDConstants.ACTION_COPY, this, "nearStackWavelet");
        farStackWaveletFilenameLabel = new javax.swing.JLabel();
        farStackWaveletFilenameTextField = new DropTextField(DnDConstants.ACTION_COPY, this, "farStackWavelet");
        nearWaveletSNLabel = new javax.swing.JLabel();
        nearNoiseRMSTextField = new javax.swing.JTextField();
        farWaveletSNLabel = new javax.swing.JLabel();
        farNoiseRMSTextField = new javax.swing.JTextField();
        avoALabel = new javax.swing.JLabel();
        avoATextField = new javax.swing.JTextField();
        avoBLabel = new javax.swing.JLabel();
        avoBTextField = new javax.swing.JTextField();



        final Component comp = this;
        nearStackFilenameBrowseButton = new javax.swing.JButton();
        nearStackFilenameBrowseButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String [] filterList = { ".dat",".DAT" };
                GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                String startDir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
                QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Select BHP-SU Dataset",startDir,filter,false,
                        false,false,
                        javax.swing.JFileChooser.OPEN_DIALOG,
                        DeliveryLiteConstants.GET_NEAR_SEISMIC_DATASET_CMD);
                agent.callFileChooser(desc);
            }
        });
        nearStackWaveletFIlenameBrowseButton = new javax.swing.JButton();
        nearStackWaveletFIlenameBrowseButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String [] filterList = { ".dat",".DAT" };
                GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                String startDir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
                QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Select BHP-SU Dataset",startDir,filter,false,
                        false,false,
                        javax.swing.JFileChooser.OPEN_DIALOG,
                        DeliveryLiteConstants.GET_NEAR_WAVELET_FILE_CMD);
                agent.callFileChooser(desc);
            }
        });
        nearStackRangeInfoPanel = new javax.swing.JPanel();
        nearInlineLabel = new javax.swing.JLabel();
        nearCrosslineLabel = new javax.swing.JLabel();
        nearInlineTextField = new javax.swing.JTextField();
        nearInlineTextField.setEditable(false);
        nearToLabel1 = new javax.swing.JLabel();
        nearInlineToTextField = new javax.swing.JTextField();
        nearInlineToTextField.setEditable(false);
        nearCrosslineTextField = new javax.swing.JTextField();
        nearCrosslineTextField.setEditable(false);
        nearToLabel2 = new javax.swing.JLabel();
        nearCrosslineToTextField = new javax.swing.JTextField();
        nearCrosslineToTextField.setEditable(false);
        nearByLabel1 = new javax.swing.JLabel();
        nearByLabel2 = new javax.swing.JLabel();
        nearInlineByTextField = new javax.swing.JTextField();
        nearInlineByTextField.setEditable(false);
        nearCrosslineByTextField = new javax.swing.JTextField();
        nearCrosslineByTextField.setEditable(false);
        farStackFilenameBrowseButton = new javax.swing.JButton();
        farStackFilenameBrowseButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String [] filterList = { ".dat",".DAT" };
                GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                String startDir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
                QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Select BHP-SU Dataset",startDir, filter,false,
                        false,false,
                        javax.swing.JFileChooser.OPEN_DIALOG,
                        DeliveryLiteConstants.GET_FAR_SEISMIC_DATASET_CMD);
                agent.callFileChooser(desc);
            }
        });
        farStackRangeInfoPanel = new javax.swing.JPanel();
        farInlineLabel = new javax.swing.JLabel();
        farCrosslineLabel = new javax.swing.JLabel();
        farInlineTextField = new javax.swing.JTextField();
        farInlineTextField.setEditable(false);
        farToLabel1 = new javax.swing.JLabel();
        farInlineToTextField = new javax.swing.JTextField();
        farInlineToTextField.setEditable(false);
        farCrosslineTextField = new javax.swing.JTextField();
        farCrosslineTextField.setEditable(false);
        farToLabel2 = new javax.swing.JLabel();
        farCrosslineToTextField = new javax.swing.JTextField();
        farCrosslineToTextField.setEditable(false);
        farByLabel1 = new javax.swing.JLabel();
        farByLabel2 = new javax.swing.JLabel();
        farInlineByTextField = new javax.swing.JTextField();
        farInlineByTextField.setEditable(false);
        farCrosslineByTextField = new javax.swing.JTextField();
        farCrosslineByTextField.setEditable(false);
        farStackWaveletFIlenameBrowseButton = new javax.swing.JButton();
        farStackWaveletFIlenameBrowseButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String [] filterList = { ".dat",".DAT" };
                GenericFileFilter filter =  new GenericFileFilter(filterList,"Data Files (*.dat)");
                String startDir = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
                QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Select BHP-SU Dataset",startDir,
                        filter,false,
                        false,false,
                        javax.swing.JFileChooser.OPEN_DIALOG,
                        DeliveryLiteConstants.GET_FAR_WAVELET_FILE_CMD);
                agent.callFileChooser(desc);
            }
        });
        singleTracePanel = new javax.swing.JPanel();


        outputPanel = new javax.swing.JPanel();
        outputParametersPanel = new javax.swing.JPanel();
        outputPriorRealisationsFilenameLabel = new javax.swing.JLabel();
        //outputPostRealisationsFilenameLabel = new javax.swing.JLabel();
        //outputPriorRealisationsFilenameTextField = new javax.swing.JTextField();
        
        outputBaseNameTextField = new javax.swing.JTextField();
        outputBaseNameTextField.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
            	String currVal = outputBaseNameTextField.getText();
            	if(currVal != null && currVal.trim().length() > 0){
            		if(!currVal.equals(showModelParams.getOutputBaseName()))
            			showModelParams = new ShowModelParams();
            	}
            }
        });
        //outputPostRealisationsFIlenameTextField = new javax.swing.JTextField();
        runPanel = new javax.swing.JPanel();
        runParametersPanel = new javax.swing.JPanel();
        numberRunsLabel = new javax.swing.JLabel();
        numberRunsTextField = new javax.swing.JTextField(DeliveryLiteConstants.DEFAULT_NUM_RUNS);
        tminLabel = new javax.swing.JLabel();
        tminLabel1 = new javax.swing.JLabel();
        tmin4SyntheticSeismicTextField = new javax.swing.JTextField(DeliveryLiteConstants.DEFAULT_MIN_TIME);
        tmaxLabel = new javax.swing.JLabel();
        tmaxLabel1 = new javax.swing.JLabel();
        syntheticSeismicLabel = new javax.swing.JLabel();
        spagettiPlotLabel = new javax.swing.JLabel();
        tmax4SyntheticSeismicTextField = new javax.swing.JTextField(DeliveryLiteConstants.DEFAULT_MAX_TIME);
        epLabel = new javax.swing.JLabel();
        epTextField = new javax.swing.JTextField("0");
        cdpLabel = new javax.swing.JLabel();
        cdpTextField = new javax.swing.JTextField("0");
        stacksLabel = new javax.swing.JLabel();
        nearOnlyRadioButton = new javax.swing.JRadioButton();
        nearOnlyRadioButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		nearOnlyRadioButtonActionPerformed(evt);
        	}
        });
        nearAndFarRadioButton = new javax.swing.JRadioButton();
        nearAndFarRadioButton.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		nearAndFarRadioButtonActionPerformed(evt);
        	}
        });
        matrixPropertiesPanel = new javax.swing.JPanel();
        matrixPropertiesFileLabel = new javax.swing.JLabel();
        importMatrixPropertiesButton = new javax.swing.JButton();
        referenceSandPanel = new javax.swing.JPanel();
        referenceSandScrollPane = new javax.swing.JScrollPane();
        referenceSandTable = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(referenceSandTable);
        refSandFormulaePane = new javax.swing.JPanel();
        refSandFormulaeTextArea = new javax.swing.JTextArea();
        mixingShalePanel = new javax.swing.JPanel();
        mixingShaleScrollPane = new javax.swing.JScrollPane();
        mixingShaleTable = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(mixingShaleTable);
        mixingShaleFormulaeTextArea = new javax.swing.JTextArea();
        grainPanel = new javax.swing.JPanel();
        grainScrollPane = new javax.swing.JScrollPane();
        grainTable = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(grainTable);
        boundingShalePanel = new javax.swing.JPanel();
        boundingShaleScrollPane = new javax.swing.JScrollPane();
        boundingShaleTable = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(boundingShaleTable);
        boundingShaleFormulaeTextArea = new javax.swing.JTextArea();
        referenceFluidPanel = new javax.swing.JPanel();
        referenceFluidScrollPane = new javax.swing.JScrollPane();
        referenceFluidTable = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(referenceFluidTable);
        refFluidValueLabel = new javax.swing.JLabel();
//        refFluidErrorLabel = new javax.swing.JLabel();
        exportMatrixPropertiesButton = new javax.swing.JButton();
        importedMatrixPropertiesFileLabel = new javax.swing.JLabel();
        fluidPropertiesPanel = new javax.swing.JPanel();
        fluidPropertiesFileLabel = new javax.swing.JLabel();
        importFluidPropertiesButton = new javax.swing.JButton();
        fluidsPanel = new javax.swing.JPanel();
        fluidsScrollPane1 = new javax.swing.JScrollPane();
        fluidsTable1 = new javax.swing.JTable();
        TableEditorStopper.ensureEditingStopWhenTableLosesFocus(fluidsTable1);
        fluidsScrollPane2 = new javax.swing.JScrollPane();
        fluidsTable2 = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(fluidsTable2);
        exportFluidPropertiesButton = new javax.swing.JButton();
        importedFluidPropertiesFileLabel = new javax.swing.JLabel();
        fluidsValueLabel = new javax.swing.JLabel();
        fluidsErrorLabel = new javax.swing.JLabel();
        stackPropertiesPanel = new javax.swing.JPanel();
        stackPropertiesFileLabel = new javax.swing.JLabel();
        importStackPropertiesButton = new javax.swing.JButton();
        stackParametersPanel = new javax.swing.JPanel();
        stackParametersScrollPane = new javax.swing.JScrollPane();
        stackParametersTable = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(stackParametersTable);
        nearLabel = new javax.swing.JLabel();
        farLabel = new javax.swing.JLabel();
        exportStackPropertiesButton = new javax.swing.JButton();
        importedStackPropertiesFileLabel = new javax.swing.JLabel();
        createModelPanel = new javax.swing.JPanel();
        modelNameLabel = new javax.swing.JLabel();
        modelNameTextField = new javax.swing.JTextField();
        modelPanel = new javax.swing.JPanel();
        fixedLayerScrollPane = new javax.swing.JScrollPane();
        fixedLayerTable = new javax.swing.JTable();
        TableEditorStopper.ensureEditingStopWhenTableLosesFocus(fixedLayerTable);
        layerScrollPane = new javax.swing.JScrollPane();
        layerScrollPane.getVerticalScrollBar().setModel(fixedLayerScrollPane.getVerticalScrollBar().getModel());
        fixedLayerScrollPane.getVerticalScrollBar().setModel(layerScrollPane.getVerticalScrollBar().getModel());
        layerTable = new javax.swing.JTable();
        //TableEditorStopper.ensureEditingStopWhenTableLosesFocus(layerTable);
        addLayerButton = new javax.swing.JButton();
        deleteLayer1Button = new javax.swing.JButton();
        addLayer2Button = new javax.swing.JButton();
        deleteLayer2Button = new javax.swing.JButton();
        validateModelButton = new javax.swing.JButton();
        writeModelButton = new javax.swing.JButton();
        numberLayersLabel = new javax.swing.JLabel();
        numLayersComboBox = new javax.swing.JComboBox();
        timeBaseLabel = new javax.swing.JLabel();
        timeBaseTextField = new javax.swing.JTextField();
        sigmaTimeBaseLabel = new javax.swing.JLabel();
        sigmaTimeBaseTextField = new javax.swing.JTextField();
        masterDepthLayerLabel = new javax.swing.JLabel();
        masterDepthLayerTextField = new javax.swing.JTextField();
        depthOfMasterLayerLabel = new javax.swing.JLabel();
        depthOfMasterLayerTextField = new javax.swing.JTextField();
        errorOfMasterLayerDepthLabel = new javax.swing.JLabel();
        errorOfMasterLayerDepthTextField = new javax.swing.JTextField();
        editModelButton = new javax.swing.JButton();
        tmin4SpaghettiPlotTextField = new javax.swing.JTextField(DeliveryLiteConstants.DEFAULT_SPAGETTI_PLOT_MIN_TIME);
        tmax4SpaghettiPlotTextField = new javax.swing.JTextField(DeliveryLiteConstants.DEFAULT_SPAGETTI_PLOT_MAX_TIME);

        final Component cmp = this;

        final ClassLoader cl = this.getClass().getClassLoader();
        editModelButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String err = validateParams();
                if (err.length() > 0) {
                    JOptionPane.showMessageDialog(cmp, err);
                    return;
                } else {
                   //createLayers();
                   //updateFluidsPropertiesInfo();
                   //updateRockPropertiesInfo();
                   updateInversionObject();
                }
                if(responseComponentDesc == null)
                    responseComponentDesc = ComponentUtils.launchNewComponent(QIWConstants.XMLEDITOR_NAME, agent.getMessagingMgr());

                //CANNOT proceed if bhpViewerDesc null
                if (responseComponentDesc == null) {
                    JOptionPane.showMessageDialog(cmp, "Cannot open a new xmlEditor for displaying results", "Launch Error", JOptionPane.WARNING_MESSAGE);
                    return;
                } else {
                    int count = 2;
                    boolean pass = false;
                    for(int i =0; i < count; i++){
                        String msgId = agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.IS_COMPONENT_UI_READY_CMD, responseComponentDesc,true);
                        IQiWorkbenchMsg response = agent.getMessagingMgr().getMatchingResponseWait(msgId,10000);
                        if(response != null && !response.isAbnormalStatus()){
                            Boolean bool = (Boolean)response.getContent();
                            if(bool.booleanValue() == false)
                                continue;
                            else{
                                pass = true;
                                break;
                            }
                        }
                    }
                    if(!pass){
                        JOptionPane.showMessageDialog(cmp, "xmlEditor is not yet ready. Please try again.", "Not Ready", JOptionPane.WARNING_MESSAGE);
                        return;
                    }

                    Document doc = inversionToDocument();
                    if(doc == null)
                        return;
                    //Start a control session
                    ArrayList params = new ArrayList();
                    //actions.add("action="+ControlConstants.START_CONTROL_SESSION_ACTION);
                    //ArrayList actionParams = new ArrayList();
                    //actionParams.add(xmlEditorDesc);
                    params.add(agent.getMessagingMgr());
                    String distribution = "MYCONFIG";
                    String pack = "delivery";
                    params.add(distribution);
                    params.add(pack);
                    params.add(doc);
                    agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_COMPONENT_CMD, responseComponentDesc, QIWConstants.ARRAYLIST_TYPE, params);
                }

                  //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
                  //marshaller.marshal(inversion, new FileOutputStream(tempModelFilepath));
                  //For DeliveryLite, the top layer element is <top> which needs to
                  //be changed to <layer> so Delivery can input the model.
                  //NOTE: Changing the first <layer> to <top> is an artifact of JAXB that
                  //can be gotten around if we wrote our own XML parser.

            }
        });
        showModelButton = new javax.swing.JButton();

        showModelButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String err = validateParams();
                if (err.length() > 0) {
                    JOptionPane.showMessageDialog(cmp, err);
                    return;
                }
                showModelParams.setNearAndFar(isNearAndFar());
                showModelParams.setOutputBaseName(getOutputBasename());
                showModelParams.setNearStackName(getNearStackFileName());
                showModelParams.setFarStackName(getFarStackFileName());
                restoreShowModelTraceData(showModelParams);
                //showModelActionPerformed();
                
            }
        });
        statusPanel = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        stateLabel = new javax.swing.JLabel();
        checkStatusButton = new javax.swing.JButton();
        detailsPanel = new javax.swing.JPanel();
        detailsSplitPane = new javax.swing.JSplitPane();
        stderrScrollPane = new javax.swing.JScrollPane();
        stderrTextArea = new javax.swing.JTextArea();
        stdoutScrollPane = new javax.swing.JScrollPane();
        stdoutTextArea = new javax.swing.JTextArea();
        cancelRunButton = new javax.swing.JButton();
        runScriptsButton = new javax.swing.JButton();
        runPostInversionButton = new javax.swing.JButton();
        writeModelScriptsButton = new javax.swing.JButton();
        runPriorInversionButton = new javax.swing.JButton();
        fileSummaryPanel = new javax.swing.JPanel();
        fileSummaryScrollPane = new javax.swing.JScrollPane();
        fileSummaryTextArea = new javax.swing.JTextArea();
        deliveryLiteMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        saveQuitMenuItem = new javax.swing.JMenuItem();
        quitMenuItem = new javax.swing.JMenuItem();
        prefsMenuItem = new javax.swing.JMenuItem();
        executeMenu = new javax.swing.JMenu();
        writeModelScriptsMenuItem = new javax.swing.JMenuItem();
        writeModelScriptsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                writeModelScriptActionPerformed(evt);
            }
        });
        writeSeparator1 = new javax.swing.JSeparator();
        writeModelMenuItem = new javax.swing.JMenuItem();
        writeModelMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //writeModelButtonActionPerformed(evt);
            	exportMenuItemActionPerformed(evt);
            }
        });
        writeDeliveryScriptsMenuItem = new javax.swing.JMenuItem();
        writeDeliveryScriptsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                writeDeliveryScriptActionPerformed(evt);
            }
        });
        writeAnalyzerScriptMenuItem = new javax.swing.JMenuItem();
        writeAnalyzerScriptMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                writeAnalyzerScriptActionPerformed(evt);
            }
        });
        writeSeparator2 = new javax.swing.JSeparator();
        runMenuItem = new javax.swing.JMenuItem();
        runPriorMenuItem = new javax.swing.JMenuItem();
        runPriorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	runPriorInversionButtonActionPerformed();
            }
        });
        runPostInverstionMenuItem = new javax.swing.JMenuItem();
        runPostInverstionMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	runPostInversionButtonActionPerformed();
            }
        });
        displayResultsMenuItem = new javax.swing.JMenuItem();
		displayResultsButton = new javax.swing.JButton();
        cancelRunMenuItem = new javax.swing.JMenuItem();
        statusMenuItem = new javax.swing.JMenuItem();
        statusMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkStatusButtonActionPerformed(evt);
            }
        });
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();
        fileSeparator = new javax.swing.JSeparator();
        importMenuItem = new javax.swing.JMenuItem();
        exportMenuItem = new javax.swing.JMenuItem();
        importMenuModel = new javax.swing.JMenuItem();
        exportMenuModel = new javax.swing.JMenuItem();

        landmarkInPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        landmarkProjectLabel.setText("Project:");

        listProjectsButton.setText("List...");

        volumeLabel.setText("Volume:");

        listVolumesButton.setText("List...");

        landmarkRangeLabel.setText("Range:");

        rangeLinesLabel.setText("Lines");

        linesTextField.setText("0");

        linesToLabel.setText("to");

        linesToTextField.setText("0");

        linesByLabel.setText("by");

        linesByTextField.setText("1");

        tracesLabel.setText("Traces");

        tracesTextField.setText("0");

        tracesToLabel.setText("to");

        tracesToTextField.setText("0");

        tracesByLabel.setText("by");

        tracesByTextField.setText("1");

        entireVolumeButton.setText("Entire Volume");

        selectButton.setText("Select from map");

        org.jdesktop.layout.GroupLayout landmarkInPanelLayout = new org.jdesktop.layout.GroupLayout(landmarkInPanel);
        landmarkInPanel.setLayout(landmarkInPanelLayout);
        landmarkInPanelLayout.setHorizontalGroup(
            landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(landmarkInPanelLayout.createSequentialGroup()
                        .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(volumeLabel)
                            .add(landmarkProjectLabel))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(voumeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                            .add(projectTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(listVolumesButton)
                            .add(listProjectsButton))
                        .addContainerGap())
                    .add(landmarkInPanelLayout.createSequentialGroup()
                        .add(landmarkRangeLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(landmarkInPanelLayout.createSequentialGroup()
                                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(rangeLinesLabel)
                                    .add(tracesLabel))
                                .add(8, 8, 8)
                                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(landmarkInPanelLayout.createSequentialGroup()
                                        .add(linesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 49, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(linesToLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(linesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 49, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(linesByLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(linesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(landmarkInPanelLayout.createSequentialGroup()
                                        .add(tracesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 49, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(tracesToLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(tracesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 49, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(tracesByLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(tracesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                            .add(landmarkInPanelLayout.createSequentialGroup()
                                .add(entireVolumeButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(selectButton))))))
        );
        landmarkInPanelLayout.setVerticalGroup(
            landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(landmarkInPanelLayout.createSequentialGroup()
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkProjectLabel)
                    .add(listProjectsButton)
                    .add(projectTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(volumeLabel)
                    .add(voumeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(listVolumesButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(landmarkRangeLabel)
                    .add(rangeLinesLabel)
                    .add(linesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesByLabel)
                    .add(linesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(linesToLabel)
                    .add(linesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tracesLabel)
                    .add(tracesTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesToLabel)
                    .add(tracesToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tracesByLabel)
                    .add(tracesByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(landmarkInPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(entireVolumeButton)
                    .add(selectButton))
                .addContainerGap(261, Short.MAX_VALUE))
        );
        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Delivery Lite Plugin");
        deliveryLiteTabbedPane.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(153, 0, 0), new java.awt.Color(153, 0, 153), new java.awt.Color(153, 153, 255), new java.awt.Color(0, 0, 0)));
        deliveryLiteTabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                deliveryLiteTabbedPaneStateChanged(evt);
            }
        });

        projectMetadataPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(null, new java.awt.Color(192, 192, 192)));
        projectRelDirLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        projectRelDirLabel1.setText("Project Relative Dir:");

        projectNameLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        projectNameLabel1.setText("Project Name:");

        seismicRelDirLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        seismicRelDirLabel1.setText("Seismic Relative Dir:");

        datasetsRelDirLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        datasetsRelDirLabel1.setText("Datasets Relative Dir:");

        modelRelDirLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        modelRelDirLabel1.setText("Model Relative Dir:");

        outputRelDirLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        outputRelDirLabel1.setText("Output Relative Dir:");

        org.jdesktop.layout.GroupLayout projectMetadataPanelLayout = new org.jdesktop.layout.GroupLayout(projectMetadataPanel);
        projectMetadataPanel.setLayout(projectMetadataPanelLayout);
        projectMetadataPanelLayout.setHorizontalGroup(
            projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(projectMetadataPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, projectMetadataPanelLayout.createSequentialGroup()
                        .add(seismicRelDirLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(seismicRelDirLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, projectMetadataPanelLayout.createSequentialGroup()
                        .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, projectNameLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, projectRelDirLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(projectNameLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(projectRelDirLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE))))
                .add(70, 70, 70)
                .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(outputRelDirLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(modelRelDirLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(datasetsRelDirLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(modelRelDirLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                    .add(datasetsRelDirLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                    .add(outputRelDirLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE))
                .addContainerGap())
        );
        projectMetadataPanelLayout.setVerticalGroup(
            projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(projectMetadataPanelLayout.createSequentialGroup()
                .add(25, 25, 25)
                .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(projectRelDirLabel1)
                    .add(projectRelDirLabel2)
                    .add(datasetsRelDirLabel1)
                    .add(datasetsRelDirLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(projectNameLabel1)
                    .add(projectNameLabel2)
                    .add(modelRelDirLabel1)
                    .add(modelRelDirLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(projectMetadataPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(seismicRelDirLabel1)
                    .add(seismicRelDirLabel2)
                    .add(outputRelDirLabel1)
                    .add(outputRelDirLabel2))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        projectInfoLabel.setFont(new java.awt.Font("Arial", 1, 13));
        projectInfoLabel.setText("The following properties are part of the project's metadata. Some can be edited via Edit | Project Properties:");

        org.jdesktop.layout.GroupLayout projectPanelLayout = new org.jdesktop.layout.GroupLayout(projectPanel);
        projectPanel.setLayout(projectPanelLayout);
        projectPanelLayout.setHorizontalGroup(
            projectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(projectPanelLayout.createSequentialGroup()
                .add(projectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(projectPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(projectMetadataPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(projectPanelLayout.createSequentialGroup()
                        .add(9, 9, 9)
                        .add(projectInfoLabel)))
                .addContainerGap())
        );
        projectPanelLayout.setVerticalGroup(
            projectPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(projectPanelLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(projectInfoLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(projectMetadataPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(507, Short.MAX_VALUE))
        );
        inputsTabbedPane.addTab("Project", infoPane.getBasePanel());
        inputsTabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                //inputTabbedPaneStateChanged(evt);
            }
        });


        nearStackFilenameLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        nearStackFilenameLabel.setText("Near Stack Filename:");

        farStackFilenameLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        farStackFilenameLabel.setText("Far Stack Filename:");

        nearStackWaveletFilenameLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        nearStackWaveletFilenameLabel.setText("Near Wavelet Filename:");

        farStackWaveletFilenameLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        farStackWaveletFilenameLabel.setText("Far Wavelet Filename:");

        nearWaveletSNLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        nearWaveletSNLabel.setText("Near Noise RMS:");

        farWaveletSNLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        farWaveletSNLabel.setText("Far Noise RMS:");

        avoALabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        avoALabel.setText("AVO-A:");

        avoBLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        avoBLabel.setText("AVO-B:");

        nearStackFilenameBrowseButton.setText("Browse");

        nearStackWaveletFIlenameBrowseButton.setText("Browse");

        nearStackRangeInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Range Info"));
        nearInlineLabel.setText("inline:");

        nearCrosslineLabel.setText("crossline:");

        nearToLabel1.setText("to");

        nearToLabel2.setText("to");

        nearByLabel1.setText("by");

        nearByLabel2.setText("by");

        org.jdesktop.layout.GroupLayout nearStackRangeInfoPanelLayout = new org.jdesktop.layout.GroupLayout(nearStackRangeInfoPanel);
        nearStackRangeInfoPanel.setLayout(nearStackRangeInfoPanelLayout);
        nearStackRangeInfoPanelLayout.setHorizontalGroup(
            nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, nearStackRangeInfoPanelLayout.createSequentialGroup()
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nearCrosslineLabel)
                    .add(nearInlineLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nearCrosslineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearInlineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nearToLabel1)
                    .add(nearToLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nearInlineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearCrosslineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nearByLabel1)
                    .add(nearByLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(nearInlineByTextField)
                    .add(nearCrosslineByTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)))
        );
        nearStackRangeInfoPanelLayout.setVerticalGroup(
            nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(nearStackRangeInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(nearInlineLabel)
                    .add(nearInlineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearToLabel1)
                    .add(nearInlineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearByLabel1)
                    .add(nearInlineByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(17, 17, 17)
                .add(nearStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(nearCrosslineLabel)
                    .add(nearCrosslineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearToLabel2)
                    .add(nearCrosslineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearByLabel2)
                    .add(nearCrosslineByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        farStackFilenameBrowseButton.setText("Browse");

        farStackRangeInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Range Info"));
        farInlineLabel.setText("inline:");

        farCrosslineLabel.setText("crossline:");

        farToLabel1.setText("to");

        farToLabel2.setText("to");

        farByLabel1.setText("by");

        farByLabel2.setText("by");

        org.jdesktop.layout.GroupLayout farStackRangeInfoPanelLayout = new org.jdesktop.layout.GroupLayout(farStackRangeInfoPanel);
        farStackRangeInfoPanel.setLayout(farStackRangeInfoPanelLayout);
        farStackRangeInfoPanelLayout.setHorizontalGroup(
            farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, farStackRangeInfoPanelLayout.createSequentialGroup()
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(farCrosslineLabel)
                    .add(farInlineLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(farCrosslineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(farInlineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(farToLabel1)
                    .add(farToLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(farInlineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(farCrosslineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(farByLabel1)
                    .add(farByLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(farInlineByTextField)
                    .add(farCrosslineByTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)))
        );
        farStackRangeInfoPanelLayout.setVerticalGroup(
            farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(farStackRangeInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(farInlineLabel)
                    .add(farInlineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(farToLabel1)
                    .add(farInlineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(farByLabel1)
                    .add(farInlineByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(17, 17, 17)
                .add(farStackRangeInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(farCrosslineLabel)
                    .add(farCrosslineTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(farToLabel2)
                    .add(farCrosslineToTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(farByLabel2)
                    .add(farCrosslineByTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        farStackWaveletFIlenameBrowseButton.setText("Browse");

        singleTracePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Single Trace"));




        org.jdesktop.layout.GroupLayout singleTracePanelLayout = new org.jdesktop.layout.GroupLayout(singleTracePanel);
        singleTracePanel.setLayout(singleTracePanelLayout);
        singleTracePanelLayout.setHorizontalGroup(
            singleTracePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(singleTracePanelLayout.createSequentialGroup()
                .add(singleTracePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cdpLabel)
                    .add(epLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(singleTracePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(epTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 63, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE)
                    .add(cdpTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 63, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE))
                .addContainerGap())
        );
        singleTracePanelLayout.setVerticalGroup(
            singleTracePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(singleTracePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(singleTracePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(epLabel)
                    .add(epTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(15, 15, 15)
                .add(singleTracePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cdpLabel)
                    .add(cdpTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        stacksLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        stacksLabel.setText("Stacks:");

        nearOnlyRadioButton.setText("Near Only");
        nearOnlyRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        nearOnlyRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        nearAndFarRadioButton.setText("Near and Far");
        nearAndFarRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        nearAndFarRadioButton.setMargin(new java.awt.Insets(0, 0, 0, 0));

        org.jdesktop.layout.GroupLayout modelParametersPanelLayout = new org.jdesktop.layout.GroupLayout(modelParametersPanel);
        modelParametersPanel.setLayout(modelParametersPanelLayout);
        modelParametersPanelLayout.setHorizontalGroup(
            modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelParametersPanelLayout.createSequentialGroup()
                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(modelParametersPanelLayout.createSequentialGroup()
                        //.addContainerGap()
                        .add(singleTracePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 110, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(modelParametersPanelLayout.createSequentialGroup()
                        .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(modelParametersPanelLayout.createSequentialGroup()
                                //.add(19, 19, 19)
                                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(nearStackRangeInfoPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                        .add(modelParametersPanelLayout.createSequentialGroup()
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(nearStackFilenameLabel)
                                            .add(11, 11, 11)
                                            .add(nearStackFilenameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 205, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(nearStackFilenameBrowseButton))
                                        .add(modelParametersPanelLayout.createSequentialGroup()
                                            .add(161, 161, 161)
                                            .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                                .add(org.jdesktop.layout.GroupLayout.LEADING, nearStackWaveletFilenameTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                                                .add(org.jdesktop.layout.GroupLayout.LEADING, nearAndFarRadioButton)
                                                .add(org.jdesktop.layout.GroupLayout.LEADING, modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                                    .add(org.jdesktop.layout.GroupLayout.LEADING, avoATextField)
                                                    .add(org.jdesktop.layout.GroupLayout.LEADING, nearNoiseRMSTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)))
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(nearStackWaveletFIlenameBrowseButton)))))
                            .add(modelParametersPanelLayout.createSequentialGroup()
                                //.addContainerGap()
                                .add(nearStackWaveletFilenameLabel))
                            .add(modelParametersPanelLayout.createSequentialGroup()
                                //.addContainerGap()
                                .add(avoALabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 60, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(modelParametersPanelLayout.createSequentialGroup()
                                //.addContainerGap()
                                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, modelParametersPanelLayout.createSequentialGroup()
                                        .add(stacksLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 60, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(nearOnlyRadioButton))
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, nearWaveletSNLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 141, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(avoBLabel)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, modelParametersPanelLayout.createSequentialGroup()
                                .add(farStackFilenameLabel)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(farStackFilenameTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 205, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(farStackFilenameBrowseButton))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, farStackRangeInfoPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(modelParametersPanelLayout.createSequentialGroup()
                                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(farStackWaveletFilenameLabel)
                                    .add(farWaveletSNLabel))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, modelParametersPanelLayout.createSequentialGroup()
                                        .add(farStackWaveletFilenameTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(farStackWaveletFIlenameBrowseButton))
                                    .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, avoBTextField)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, farNoiseRMSTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)))))))
                .addContainerGap())
        );
        modelParametersPanelLayout.setVerticalGroup(
            modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelParametersPanelLayout.createSequentialGroup()
                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(nearStackFilenameBrowseButton)
                    .add(farStackFilenameLabel)
                    .add(farStackFilenameBrowseButton)
                    .add(nearStackFilenameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearStackFilenameLabel)
                    .add(farStackFilenameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(17, 17, 17)
                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(nearStackRangeInfoPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(farStackRangeInfoPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(singleTracePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(31, 31, 31)
                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(nearStackWaveletFilenameLabel)
                    .add(nearStackWaveletFIlenameBrowseButton)
                    .add(nearStackWaveletFilenameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(farStackWaveletFilenameLabel)
                    .add(farStackWaveletFIlenameBrowseButton)
                    .add(farStackWaveletFilenameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(15, 15, 15)
                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(nearWaveletSNLabel)
                    .add(farWaveletSNLabel)
                    .add(farNoiseRMSTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(nearNoiseRMSTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(14, 14, 14)
                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(avoALabel)
                    .add(avoBLabel)
                    .add(avoBTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(avoATextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(24, 24, 24)
                .add(modelParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(stacksLabel)
                    .add(nearOnlyRadioButton)
                    .add(nearAndFarRadioButton))
                .addContainerGap(204, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout inputPanelLayout = new org.jdesktop.layout.GroupLayout(inputPanel);
        inputPanel.setLayout(inputPanelLayout);
        inputPanelLayout.setHorizontalGroup(
            inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(modelParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap()
                )
        );
        inputPanelLayout.setVerticalGroup(
            inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(modelParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(131, Short.MAX_VALUE))
        );
        inputsTabbedPane.addTab("Input", inputPanel);



        outputPriorRealisationsFilenameLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        outputPriorRealisationsFilenameLabel.setText("Output Filename:");

        org.jdesktop.layout.GroupLayout outputParametersPanelLayout = new org.jdesktop.layout.GroupLayout(outputParametersPanel);
        outputParametersPanel.setLayout(outputParametersPanelLayout);
        outputParametersPanelLayout.setHorizontalGroup(
            outputParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputParametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputPriorRealisationsFilenameLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputBaseNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 297, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(502, Short.MAX_VALUE))
        );
        outputParametersPanelLayout.setVerticalGroup(
            outputParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputParametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputPriorRealisationsFilenameLabel)
                    .add(outputBaseNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        numberRunsLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        numberRunsLabel.setText("Number of Runs:");


        tminLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        tminLabel.setText("tmin:");

        tmaxLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        tmaxLabel.setText("tmax:");

        tminLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        tminLabel1.setText("tmin:");

        tmaxLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        tmaxLabel1.setText("tmax:");

        syntheticSeismicLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        syntheticSeismicLabel.setText("Synthetic Seismic :");

        spagettiPlotLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        spagettiPlotLabel.setText("Spaghetti Plot :");


        org.jdesktop.layout.GroupLayout runParametersPanelLayout = new org.jdesktop.layout.GroupLayout(runParametersPanel);
        runParametersPanel.setLayout(runParametersPanelLayout);
        runParametersPanelLayout.setHorizontalGroup(
            runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(runParametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(runParametersPanelLayout.createSequentialGroup()
                        .add(numberRunsLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(numberRunsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(runParametersPanelLayout.createSequentialGroup()
                        .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(syntheticSeismicLabel)
                            .add(spagettiPlotLabel))
                        .add(31, 31, 31)
                        .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(tminLabel)
                            .add(tminLabel1))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, tmin4SpaghettiPlotTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, tmin4SyntheticSeismicTextField))
                        .add(16, 16, 16)
                        .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(tmaxLabel)
                            .add(tmaxLabel1))
                        .add(20, 20, 20)
                        .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(tmax4SpaghettiPlotTextField)
                            .add(tmax4SyntheticSeismicTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE))))
                        .add(16, 16, 16)
                        .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(secLabel)
                            .add(secLabel1))
                .addContainerGap(496, Short.MAX_VALUE))
        );
        runParametersPanelLayout.setVerticalGroup(
            runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(runParametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(numberRunsLabel)
                    .add(numberRunsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tminLabel)
                    .add(tmaxLabel)
                    .add(tmax4SyntheticSeismicTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tmin4SyntheticSeismicTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(secLabel)
                    .add(syntheticSeismicLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(runParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tmin4SpaghettiPlotTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tmax4SpaghettiPlotTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tminLabel1)
                    .add(tmaxLabel1)
                    .add(secLabel1)
                    .add(spagettiPlotLabel))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout outputPanelLayout = new org.jdesktop.layout.GroupLayout(outputPanel);
        outputPanel.setLayout(outputPanelLayout);
        outputPanelLayout.setHorizontalGroup(
            outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputPanelLayout.createSequentialGroup()
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(outputParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(runParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        outputPanelLayout.setVerticalGroup(
            outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(outputParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(runParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(489, Short.MAX_VALUE))
        );


        inputsTabbedPane.addTab("Output", outputPanel);

        matrixPropertiesFileLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        matrixPropertiesFileLabel.setText("Matrix Properties File:");

        importMatrixPropertiesButton.setText("Import");
        importMatrixPropertiesButton.setEnabled(false);
        importMatrixPropertiesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMatrixPropertiesButtonActionPerformed(evt);
            }
        });

        referenceSandPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        referenceSandScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        referenceSandScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        referenceSandScrollPane.setMaximumSize(new java.awt.Dimension(460, 460));

        referenceSandTable.setMaximumSize(new java.awt.Dimension(440, 48));
        referenceSandTable.setMinimumSize(new java.awt.Dimension(440, 48));
        referenceSandTable.setPreferredSize(new java.awt.Dimension(440, 48));
        referenceSandScrollPane.setViewportView(referenceSandTable);

        org.jdesktop.layout.GroupLayout refSandFormulaePaneLayout = new org.jdesktop.layout.GroupLayout(refSandFormulaePane);
        refSandFormulaePane.setLayout(refSandFormulaePaneLayout);
        refSandFormulaePaneLayout.setHorizontalGroup(
            refSandFormulaePaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 0, Short.MAX_VALUE)
        );
        refSandFormulaePaneLayout.setVerticalGroup(
            refSandFormulaePaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 0, Short.MAX_VALUE)
        );

        refSandFormulaeTextArea.setBackground(new java.awt.Color(214, 214, 249));
        refSandFormulaeTextArea.setColumns(20);
        refSandFormulaeTextArea.setFont(new java.awt.Font("Arial", 1, 12));
        refSandFormulaeTextArea.setRows(3);
        refSandFormulaeTextArea.setTabSize(4);
        refSandFormulaeTextArea.setText(" vp = A + B * depth + C * vi\n porosity = A + B * vp\n vs = A + B * vp");
        refSandFormulaeTextArea.setWrapStyleWord(true);
        refSandFormulaeTextArea.setMaximumSize(new java.awt.Dimension(280, 280));

        org.jdesktop.layout.GroupLayout referenceSandPanelLayout = new org.jdesktop.layout.GroupLayout(referenceSandPanel);
        referenceSandPanel.setLayout(referenceSandPanelLayout);
        referenceSandPanelLayout.setHorizontalGroup(
            referenceSandPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, referenceSandPanelLayout.createSequentialGroup()
                .add(referenceSandScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 403, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(refSandFormulaeTextArea, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(refSandFormulaePane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(290, 290, 290))
        );
        referenceSandPanelLayout.setVerticalGroup(
            referenceSandPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(referenceSandPanelLayout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .add(referenceSandPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, referenceSandPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, referenceSandPanelLayout.createSequentialGroup()
                            .add(refSandFormulaePane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(34, 34, 34))
                        .add(referenceSandPanelLayout.createSequentialGroup()
                            .add(referenceSandScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 71, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap()))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, referenceSandPanelLayout.createSequentialGroup()
                        .add(refSandFormulaeTextArea, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(21, 21, 21))))
        );

        mixingShalePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        mixingShaleScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        mixingShaleScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        mixingShaleScrollPane.setMaximumSize(new java.awt.Dimension(460, 460));

        mixingShaleTable.setMaximumSize(new java.awt.Dimension(440, 48));
        mixingShaleTable.setMinimumSize(new java.awt.Dimension(440, 48));
        mixingShaleTable.setPreferredSize(new java.awt.Dimension(440, 48));
        mixingShaleScrollPane.setViewportView(mixingShaleTable);

        mixingShaleFormulaeTextArea.setBackground(new java.awt.Color(214, 214, 249));
        mixingShaleFormulaeTextArea.setColumns(20);
        mixingShaleFormulaeTextArea.setFont(new java.awt.Font("Arial", 1, 12));
        mixingShaleFormulaeTextArea.setRows(3);
        mixingShaleFormulaeTextArea.setTabSize(4);
        mixingShaleFormulaeTextArea.setText(" vp = A + B * depth + C * vi\n density = A * vp ^ B\n vs = A + B * vp");
        mixingShaleFormulaeTextArea.setWrapStyleWord(true);
        mixingShaleFormulaeTextArea.setMaximumSize(new java.awt.Dimension(280, 280));

        org.jdesktop.layout.GroupLayout mixingShalePanelLayout = new org.jdesktop.layout.GroupLayout(mixingShalePanel);
        mixingShalePanel.setLayout(mixingShalePanelLayout);
        mixingShalePanelLayout.setHorizontalGroup(
            mixingShalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(mixingShalePanelLayout.createSequentialGroup()
                .add(mixingShaleScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 403, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(mixingShaleFormulaeTextArea, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(228, Short.MAX_VALUE))
        );
        mixingShalePanelLayout.setVerticalGroup(
            mixingShalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(mixingShalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(mixingShaleScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 71, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, mixingShalePanelLayout.createSequentialGroup()
                .addContainerGap(43, Short.MAX_VALUE)
                .add(mixingShaleFormulaeTextArea, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(20, 20, 20))
        );

        grainPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grain", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        grainScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        grainScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        grainScrollPane.setMaximumSize(new java.awt.Dimension(460, 460));

        grainTable.setMaximumSize(new java.awt.Dimension(440, 48));
        grainTable.setMinimumSize(new java.awt.Dimension(440, 48));
        grainTable.setPreferredSize(new java.awt.Dimension(440, 48));
        grainScrollPane.setViewportView(grainTable);

        org.jdesktop.layout.GroupLayout grainPanelLayout = new org.jdesktop.layout.GroupLayout(grainPanel);
        grainPanel.setLayout(grainPanelLayout);
        grainPanelLayout.setHorizontalGroup(
            grainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(grainPanelLayout.createSequentialGroup()
                .add(grainScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 195, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        grainPanelLayout.setVerticalGroup(
            grainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(grainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(grainScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(37, Short.MAX_VALUE))
        );

        boundingShalePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        boundingShaleScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        boundingShaleScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        boundingShaleScrollPane.setMaximumSize(new java.awt.Dimension(460, 460));

        boundingShaleTable.setMaximumSize(new java.awt.Dimension(440, 48));
        boundingShaleTable.setMinimumSize(new java.awt.Dimension(440, 48));
        boundingShaleTable.setPreferredSize(new java.awt.Dimension(440, 48));
        boundingShaleScrollPane.setViewportView(boundingShaleTable);

        boundingShaleFormulaeTextArea.setBackground(new java.awt.Color(214, 214, 249));
        boundingShaleFormulaeTextArea.setColumns(20);
        boundingShaleFormulaeTextArea.setFont(new java.awt.Font("Arial", 1, 12));
        boundingShaleFormulaeTextArea.setRows(3);
        boundingShaleFormulaeTextArea.setTabSize(4);
        boundingShaleFormulaeTextArea.setText(" vp = A + B * depth + C * vi\n density = A * vp ^ B\n vs = A + B * vp");
        boundingShaleFormulaeTextArea.setWrapStyleWord(true);
        boundingShaleFormulaeTextArea.setMaximumSize(new java.awt.Dimension(280, 280));

        org.jdesktop.layout.GroupLayout boundingShalePanelLayout = new org.jdesktop.layout.GroupLayout(boundingShalePanel);
        boundingShalePanel.setLayout(boundingShalePanelLayout);
        boundingShalePanelLayout.setHorizontalGroup(
            boundingShalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(boundingShalePanelLayout.createSequentialGroup()
                .add(boundingShaleScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 403, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(boundingShaleFormulaeTextArea, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(228, Short.MAX_VALUE))
        );
        boundingShalePanelLayout.setVerticalGroup(
            boundingShalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(boundingShalePanelLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(boundingShalePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, boundingShalePanelLayout.createSequentialGroup()
                        .add(boundingShaleScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 71, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, boundingShalePanelLayout.createSequentialGroup()
                        .add(boundingShaleFormulaeTextArea, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(22, 22, 22))))
        );

        referenceFluidPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Reference Fluid", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        referenceFluidScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        referenceFluidScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        referenceFluidScrollPane.setMaximumSize(new java.awt.Dimension(460, 460));

        referenceFluidTable.setMaximumSize(new java.awt.Dimension(440, 24));
        referenceFluidTable.setMinimumSize(new java.awt.Dimension(440, 24));
        referenceFluidTable.setPreferredSize(new java.awt.Dimension(440, 24));
        referenceFluidScrollPane.setViewportView(referenceFluidTable);

        refFluidValueLabel.setFont(new java.awt.Font("Arial", 1, 12));
        refFluidValueLabel.setText("Value");

        org.jdesktop.layout.GroupLayout referenceFluidPanelLayout = new org.jdesktop.layout.GroupLayout(referenceFluidPanel);
        referenceFluidPanel.setLayout(referenceFluidPanelLayout);
        referenceFluidPanelLayout.setHorizontalGroup(
            referenceFluidPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(referenceFluidPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(referenceFluidScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(refFluidValueLabel)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        referenceFluidPanelLayout.setVerticalGroup(
            referenceFluidPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(referenceFluidPanelLayout.createSequentialGroup()
                .add(referenceFluidPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(referenceFluidPanelLayout.createSequentialGroup()
                        .add(29, 29, 29)
                        .add(refFluidValueLabel))
                    .add(referenceFluidPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(referenceFluidScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 38, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        exportMatrixPropertiesButton.setText("Export");
        exportMatrixPropertiesButton.setEnabled(false);
        exportMatrixPropertiesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMatrixPropertiesButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout matrixPropertiesPanelLayout = new org.jdesktop.layout.GroupLayout(matrixPropertiesPanel);
        matrixPropertiesPanel.setLayout(matrixPropertiesPanelLayout);
        matrixPropertiesPanelLayout.setHorizontalGroup(
            matrixPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(matrixPropertiesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(matrixPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(matrixPropertiesPanelLayout.createSequentialGroup()
                        .add(importMatrixPropertiesButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(matrixPropertiesFileLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(importedMatrixPropertiesFileLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 348, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(exportMatrixPropertiesButton))
                    .add(matrixPropertiesPanelLayout.createSequentialGroup()
                        .add(grainPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(192, 192, 192)
                        .add(referenceFluidPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(mixingShalePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(referenceSandPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 873, Short.MAX_VALUE)
                    .add(boundingShalePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(22, 22, 22))
        );
        matrixPropertiesPanelLayout.setVerticalGroup(
            matrixPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(matrixPropertiesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(matrixPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(importMatrixPropertiesButton)
                    .add(matrixPropertiesFileLabel)
                    .add(importedMatrixPropertiesFileLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(exportMatrixPropertiesButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(referenceSandPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(mixingShalePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(13, 13, 13)
                .add(boundingShalePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(matrixPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(referenceFluidPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(grainPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(73, Short.MAX_VALUE))
        );
        inputsTabbedPane.addTab("Matrix", matrixPropertiesPanel);

        fluidPropertiesFileLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        fluidPropertiesFileLabel.setText("Fluid Properties File:");

        importFluidPropertiesButton.setText("Import");
        importFluidPropertiesButton.setEnabled(false);
        importFluidPropertiesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importFluidPropertiesButtonActionPerformed(evt);
            }
        });

        fluidsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Fluids", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        fluidsScrollPane1.setBorder(null);
        fluidsScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        fluidsScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        fluidsScrollPane1.setMaximumSize(new java.awt.Dimension(900, 460));
        fluidsTable1.setMaximumSize(new java.awt.Dimension(720, 48));
        fluidsTable1.setMinimumSize(new java.awt.Dimension(720, 48));
        fluidsTable1.setPreferredSize(new java.awt.Dimension(720, 48));

        fluidsScrollPane1.setViewportView(fluidsTable1);

        fluidsScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        //fluidsScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        fluidsScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        fluidsScrollPane2.setMaximumSize(new java.awt.Dimension(900, 460));
        //fluidsScrollPane2.setMaximumSize(new java.awt.Dimension(660, 460));
        fluidsScrollPane2.setOpaque(false);
        fluidsTable2.setOpaque(false);
        fluidsTable2.setMaximumSize(new java.awt.Dimension(720, 48));
        fluidsTable2.setMinimumSize(new java.awt.Dimension(720, 48));
        fluidsTable2.setPreferredSize(new java.awt.Dimension(720, 48));
        fluidsScrollPane2.setViewportView(fluidsTable2);

        org.jdesktop.layout.GroupLayout fluidsPanelLayout = new org.jdesktop.layout.GroupLayout(fluidsPanel);
        fluidsPanel.setLayout(fluidsPanelLayout);
        fluidsPanelLayout.setHorizontalGroup(
            fluidsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fluidsPanelLayout.createSequentialGroup()
                .add(fluidsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, fluidsScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 720, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, fluidsScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 720, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fluidsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fluidsValueLabel)
                    .add(fluidsErrorLabel))
                .add(123, 123, 123))
        );
        fluidsPanelLayout.setVerticalGroup(
            fluidsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, fluidsPanelLayout.createSequentialGroup()
                .add(fluidsScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fluidsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(fluidsPanelLayout.createSequentialGroup()
                        .add(19, 19, 19)
                        .add(fluidsValueLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(fluidsErrorLabel))
                    .add(fluidsPanelLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(fluidsScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 55, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(28, 28, 28))
        );

        exportFluidPropertiesButton.setText("Export");
        exportFluidPropertiesButton.setEnabled(false);
        exportFluidPropertiesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportFluidPropertiesButtonActionPerformed(evt);
            }
        });

        fluidsValueLabel.setFont(new java.awt.Font("Arial", 1, 12));
        fluidsValueLabel.setText("Value");

        fluidsErrorLabel.setFont(new java.awt.Font("Arial", 1, 12));
        fluidsErrorLabel.setText("Error");

        org.jdesktop.layout.GroupLayout fluidPropertiesPanelLayout = new org.jdesktop.layout.GroupLayout(fluidPropertiesPanel);
        fluidPropertiesPanel.setLayout(fluidPropertiesPanelLayout);
        fluidPropertiesPanelLayout.setHorizontalGroup(
            fluidPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fluidPropertiesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(fluidPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fluidsPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 795, Short.MAX_VALUE)
                    .add(fluidPropertiesPanelLayout.createSequentialGroup()
                        .add(importFluidPropertiesButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(fluidPropertiesFileLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(importedFluidPropertiesFileLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 348, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 244, Short.MAX_VALUE)
                        .add(exportFluidPropertiesButton)))
                .addContainerGap())
        );
        fluidPropertiesPanelLayout.setVerticalGroup(
            fluidPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fluidPropertiesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(fluidPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(importFluidPropertiesButton)
                    .add(fluidPropertiesFileLabel)
                    .add(importedFluidPropertiesFileLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(exportFluidPropertiesButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fluidsPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(442, Short.MAX_VALUE))
        );
        inputsTabbedPane.addTab("Fluid", fluidPropertiesPanel);

        stackPropertiesFileLabel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        stackPropertiesFileLabel.setText("Stack Properties File:");

        importStackPropertiesButton.setText("Import");
        importStackPropertiesButton.setEnabled(false);
        importStackPropertiesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importStackPropertiesButtonActionPerformed(evt);
            }
        });

        stackParametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stack Parameters", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        stackParametersScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        stackParametersScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        stackParametersScrollPane.setMaximumSize(new java.awt.Dimension(460, 460));

        stackParametersTable.setMaximumSize(new java.awt.Dimension(440, 48));
        stackParametersTable.setMinimumSize(new java.awt.Dimension(440, 48));
        stackParametersTable.setPreferredSize(new java.awt.Dimension(440, 48));
        stackParametersScrollPane.setViewportView(stackParametersTable);

        nearLabel.setFont(new java.awt.Font("Arial", 1, 12));
        nearLabel.setText("Near");

        farLabel.setFont(new java.awt.Font("Arial", 1, 12));
        farLabel.setText("Far");

        org.jdesktop.layout.GroupLayout stackParametersPanelLayout = new org.jdesktop.layout.GroupLayout(stackParametersPanel);
        stackParametersPanel.setLayout(stackParametersPanelLayout);
        stackParametersPanelLayout.setHorizontalGroup(
            stackParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(stackParametersPanelLayout.createSequentialGroup()
                .add(stackParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 403, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(stackParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(nearLabel)
                    .add(farLabel))
                .addContainerGap(429, Short.MAX_VALUE))
        );
        stackParametersPanelLayout.setVerticalGroup(
            stackParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(stackParametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(stackParametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(stackParametersScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(stackParametersPanelLayout.createSequentialGroup()
                        .add(nearLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(farLabel)))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        exportStackPropertiesButton.setText("Export");
        exportStackPropertiesButton.setEnabled(false);
        exportStackPropertiesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportStackPropertiesButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout stackPropertiesPanelLayout = new org.jdesktop.layout.GroupLayout(stackPropertiesPanel);
        stackPropertiesPanel.setLayout(stackPropertiesPanelLayout);
        stackPropertiesPanelLayout.setHorizontalGroup(
            stackPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, stackPropertiesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(stackPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, stackParametersPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(stackPropertiesPanelLayout.createSequentialGroup()
                        .add(importStackPropertiesButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(stackPropertiesFileLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(importedStackPropertiesFileLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 348, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 255, Short.MAX_VALUE)
                        .add(exportStackPropertiesButton)))
                .add(41, 41, 41))
        );
        stackPropertiesPanelLayout.setVerticalGroup(
            stackPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(stackPropertiesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(stackPropertiesPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(importStackPropertiesButton)
                    .add(stackPropertiesFileLabel)
                    .add(importedStackPropertiesFileLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(exportStackPropertiesButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(stackParametersPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(487, Short.MAX_VALUE))
        );
        inputsTabbedPane.addTab("Stack", stackPropertiesPanel);

        org.jdesktop.layout.GroupLayout parametersPanelLayout = new org.jdesktop.layout.GroupLayout(parametersPanel);
        parametersPanel.setLayout(parametersPanelLayout);
        parametersPanelLayout.setHorizontalGroup(
            parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(parametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputsTabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                .addContainerGap())
        );
        parametersPanelLayout.setVerticalGroup(
            parametersPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(parametersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputsTabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE))
        );
        deliveryLiteTabbedPane.addTab("Parameters", parametersPanel);

        modelNameLabel.setText("Model Name");

        modelPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Model", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 13)));
        modelPanel.setFont(new java.awt.Font("Lucida Grande", 1, 13));
        fixedLayerScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        fixedLayerTable.setMaximumSize(new java.awt.Dimension(275, 48));
        fixedLayerScrollPane.setViewportView(fixedLayerTable);

        layerScrollPane.setInheritsPopupMenu(true);
        layerScrollPane.setPreferredSize(new java.awt.Dimension(1660, 402));
        layerScrollPane.setViewportView(layerTable);

        addLayerButton.setText("Add Layer");
        addLayerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addLayerButtonActionPerformed(evt);
            }
        });

        deleteLayer1Button.setText("Delete Layer");
        deleteLayer1Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLayer1ButtonActionPerformed(evt);
            }
        });

        addLayer2Button.setText("Add Layer");
        addLayer2Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addLayer2ButtonActionPerformed(evt);
            }
        });

        deleteLayer2Button.setText("Delete Layer");
        deleteLayer2Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLayer2ButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout modelPanelLayout = new org.jdesktop.layout.GroupLayout(modelPanel);
        modelPanel.setLayout(modelPanelLayout);
        modelPanelLayout.setHorizontalGroup(
            modelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(modelPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(addLayerButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(deleteLayer1Button)
                .addContainerGap(729, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, modelPanelLayout.createSequentialGroup()
                .add(fixedLayerScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
                .add(0, 0, 0)
                .add(layerScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE))
            .add(modelPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(addLayer2Button)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(deleteLayer2Button)
                .addContainerGap(729, Short.MAX_VALUE))
        );
        modelPanelLayout.setVerticalGroup(
            modelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, modelPanelLayout.createSequentialGroup()
                .add(modelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(addLayerButton)
                    .add(deleteLayer1Button))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(modelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fixedLayerScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                    .add(layerScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(modelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(addLayer2Button)
                    .add(deleteLayer2Button)))
        );

        validateModelButton.setText("Validate Inputs");
        validateModelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validateModelButtonActionPerformed(evt);
            }
        });

        writeModelButton.setText("Write Model");
        writeModelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //writeModelButtonActionPerformed(evt);
            	exportMenuItemActionPerformed(evt);
            }
        });

        numberLayersLabel.setText("Number of Initial Layers");

        numLayersComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        numLayersComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numLayersComboBoxActionPerformed(evt);
            }
        });

        timeBaseLabel.setText("Time Base");

        sigmaTimeBaseLabel.setText("Sigma Time Base");

        masterDepthLayerLabel.setText("Master Depth Layer");

        depthOfMasterLayerLabel.setText("Depth of Master Layer");

        errorOfMasterLayerDepthLabel.setText("Error of Master Layer Depth");

        editModelButton.setText("Edit Model");

        showModelButton.setText("Show Model");

        org.jdesktop.layout.GroupLayout createModelPanelLayout = new org.jdesktop.layout.GroupLayout(createModelPanel);
        createModelPanel.setLayout(createModelPanelLayout);
        createModelPanelLayout.setHorizontalGroup(
            createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, createModelPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, modelPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(createModelPanelLayout.createSequentialGroup()
                        .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, createModelPanelLayout.createSequentialGroup()
                                .add(showModelButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(editModelButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(writeModelButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                            .add(createModelPanelLayout.createSequentialGroup()
                                .add(modelNameLabel)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(createModelPanelLayout.createSequentialGroup()
                                        .add(timeBaseTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 76, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(19, 19, 19)
                                        .add(sigmaTimeBaseLabel)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(sigmaTimeBaseTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 81, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(23, 23, 23)
                                        .add(masterDepthLayerLabel))
                                    .add(createModelPanelLayout.createSequentialGroup()
                                        .add(modelNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 215, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(15, 15, 15)
                                        //.add(numberLayersLabel)
                                        //.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        //.add(numLayersComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 42, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        ))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(createModelPanelLayout.createSequentialGroup()
                                        .add(masterDepthLayerTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 84, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(depthOfMasterLayerLabel))
                                    .add(errorOfMasterLayerDepthLabel))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(depthOfMasterLayerTextField)
                                    .add(errorOfMasterLayerDepthTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE))
                                .add(17, 17, 17))
                            .add(timeBaseLabel))
                        .add(validateModelButton)))
                .addContainerGap())
        );
        createModelPanelLayout.setVerticalGroup(
            createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(createModelPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(modelNameLabel)
                    .add(modelNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    //.add(numberLayersLabel)
                    //.add(numLayersComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(errorOfMasterLayerDepthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(errorOfMasterLayerDepthLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(timeBaseLabel)
                    .add(timeBaseTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(sigmaTimeBaseLabel)
                    .add(sigmaTimeBaseTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(masterDepthLayerLabel)
                    .add(masterDepthLayerTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(depthOfMasterLayerLabel)
                    .add(depthOfMasterLayerTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(14, 14, 14)
                .add(modelPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(createModelPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(validateModelButton)
                    .add(writeModelButton)
                    .add(editModelButton)
                    .add(showModelButton))
                .addContainerGap(89, Short.MAX_VALUE))
        );
        deliveryLiteTabbedPane.addTab("Model", createModelPanel);

        statusLabel.setText("Status:");

        stateLabel.setText("Not Started");
        List<Component> checkStatusButtons = new ArrayList<Component>();
        checkStatusButtons.add(checkStatusButton);
        checkStatusButtons.add(statusMenuItem);
        for(Component cm: checkStatusButtons)
        	cm.setEnabled(false);
        checkStatusButton.setText("Check Status");
        checkStatusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkStatusButtonActionPerformed(evt);
            }
        });

        detailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Status Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12)));
        detailsSplitPane.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        detailsSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        detailsSplitPane.setOneTouchExpandable(true);
        stderrTextArea.setColumns(20);
        stderrTextArea.setRows(5);
        stderrScrollPane.setViewportView(stderrTextArea);

        detailsSplitPane.setTopComponent(stderrScrollPane);

        stdoutTextArea.setColumns(20);
        stdoutTextArea.setRows(5);
        stdoutScrollPane.setViewportView(stdoutTextArea);

        detailsSplitPane.setRightComponent(stdoutScrollPane);

        org.jdesktop.layout.GroupLayout detailsPanelLayout = new org.jdesktop.layout.GroupLayout(detailsPanel);
        detailsPanel.setLayout(detailsPanelLayout);
        detailsPanelLayout.setHorizontalGroup(
            detailsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(detailsSplitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        detailsPanelLayout.setVerticalGroup(
            detailsPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, detailsSplitPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
        );

        cancelRunButton.setText("Cancel Run");
        cancelRunButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelRunButtonActionPerformed(evt);
            }
        });
		
        displayResultsButton.setText("Display Results");
        displayResultsButton.setToolTipText("Display script results in qiViewer");
        displayResultsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayResultsButtonActionPerformed(evt);
            }
        });

        runScriptsButton.setText("Run Analyzer");
        runScriptsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	runAnalyzerScript(evt);
            }
        });

        runPostInversionButton.setText("Run Post Inversion");
        runPostInversionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	runPostInversionButtonActionPerformed();
            }
        });


        writeModelScriptsButton.setText("Run Inversion");
        writeModelScriptsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runInversionScript(evt);
            }
        });

        runPriorInversionButton.setText("Run Prior");
        runPriorInversionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	runPriorInversionButtonActionPerformed();
            }
        });
        fileSummaryPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "File Summary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12)));
        fileSummaryTextArea.setColumns(20);
        fileSummaryTextArea.setRows(5);
        fileSummaryScrollPane.setViewportView(fileSummaryTextArea);

        org.jdesktop.layout.GroupLayout fileSummaryPanelLayout = new org.jdesktop.layout.GroupLayout(fileSummaryPanel);
        fileSummaryPanel.setLayout(fileSummaryPanelLayout);
        fileSummaryPanelLayout.setHorizontalGroup(
            fileSummaryPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fileSummaryScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        fileSummaryPanelLayout.setVerticalGroup(
            fileSummaryPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fileSummaryPanelLayout.createSequentialGroup()
                .add(fileSummaryScrollPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout statusPanelLayout = new org.jdesktop.layout.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, detailsPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(statusPanelLayout.createSequentialGroup()
                        .add(statusLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(stateLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 443, Short.MAX_VALUE)
                        .add(runPriorInversionButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(runPostInversionButton)
						.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(displayResultsButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(checkStatusButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cancelRunButton))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, fileSummaryPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(statusPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(statusLabel)
                    .add(stateLabel)
                    .add(cancelRunButton)
                    .add(checkStatusButton)
					.add(displayResultsButton)
                    .add(runPostInversionButton)
                    .add(runPriorInversionButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(detailsPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fileSummaryPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
                //.addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        deliveryLiteTabbedPane.addTab("Job Status", statusPanel);

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");


        importMenuModel.setMnemonic('m');
        importMenuModel.setText("Import Model Only");
        importMenuModel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMenuItemActionPerformed(evt);
            }
        });

        importMenuItem.setMnemonic('I');
        importMenuItem.setText("Import Model + Run Params");
        importMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importModelRunParams(evt);
            }
        });

        exportMenuModel.setMnemonic('x');
        exportMenuModel.setText("Export Model Only");
        exportMenuModel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMenuItemActionPerformed(evt);
            }
        });


        exportMenuItem.setMnemonic('E');
        exportMenuItem.setText("Export Model + Run Params");
        exportMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportModelRunParams(evt);
            }
        });

        javax.swing.JMenu importMenu = new javax.swing.JMenu();
        importMenu.setMnemonic('I');
        importMenu.setText("Import ...");

        importMenu.add(importMenuModel);
        importMenu.add(importMenuItem);

        fileMenu.add(importMenu);

        javax.swing.JMenu exportMenu = new javax.swing.JMenu();
        exportMenu.setMnemonic('E');
        exportMenu.setText("Export ...");
        exportMenu.add(exportMenuModel);
        exportMenu.add(exportMenuItem);

        fileMenu.add(exportMenu);

        fileMenu.add(fileSeparator);

        quitMenuItem.setMnemonic('Q');
        quitMenuItem.setText("Quit");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(quitMenuItem);

        saveMenuItem.setMnemonic('S');
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setMnemonic('A');
        saveAsMenuItem.setText("Save As");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        saveQuitMenuItem.setMnemonic('U');
        saveQuitMenuItem.setText("Save, Quit");
        saveQuitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveQuitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveQuitMenuItem);

        prefsMenuItem.setMnemonic('P');
        prefsMenuItem.setText("Preferences...");
        prefsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prefsMenuItemActionPerformed(evt);
            }
        });
        //fileMenu.add(prefsMenuItem);

        deliveryLiteMenuBar.add(fileMenu);

        executeMenu.setMnemonic('E');
        executeMenu.setText("Execute");
        writeModelScriptsMenuItem.setMnemonic('W');
        writeModelScriptsMenuItem.setText("Write Model & Scripts");
        writeModelScriptsMenuItem.setActionCommand("Write Model_Scripts");
        executeMenu.add(writeModelScriptsMenuItem);

        executeMenu.add(writeSeparator1);

        writeModelMenuItem.setMnemonic('M');
        writeModelMenuItem.setText("Write Model");
        executeMenu.add(writeModelMenuItem);

        writeDeliveryScriptsMenuItem.setMnemonic('D');
        writeDeliveryScriptsMenuItem.setText("Write Delivery Scripts");
        executeMenu.add(writeDeliveryScriptsMenuItem);

        writeAnalyzerScriptMenuItem.setMnemonic('A');
        writeAnalyzerScriptMenuItem.setText("Write Analyzer Script");
        executeMenu.add(writeAnalyzerScriptMenuItem);

        executeMenu.add(writeSeparator2);

        runMenuItem.setMnemonic('R');
        runMenuItem.setText("Run Inversion");

        runPriorMenuItem.setMnemonic('R');
        runPriorMenuItem.setText("Run Prior");
        executeMenu.add(runPriorMenuItem);

        runPostInverstionMenuItem.setMnemonic('P');
        runPostInverstionMenuItem.setText("Run Post Inversion");
        executeMenu.add(runPostInverstionMenuItem);

        displayResultsMenuItem.setMnemonic('D');
        displayResultsMenuItem.setText("Display Results");
        displayResultsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayResultsButtonActionPerformed(evt);
            }
        });
        executeMenu.add(displayResultsMenuItem);

        cancelRunMenuItem.setMnemonic('C');
        cancelRunMenuItem.setText("Cancel Run");
        cancelRunMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelRunButtonActionPerformed(evt);
            }
        });
        executeMenu.add(cancelRunMenuItem);

        statusMenuItem.setMnemonic('S');
        statusMenuItem.setText("Check Status");
        executeMenu.add(statusMenuItem);

        deliveryLiteMenuBar.add(executeMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");
        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setText("About...");

        aboutMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if(props == null){
                    String compName = agent.getMessagingMgr().getRegisteredComponentDisplayNameByDescriptor(agent.getComponentDescriptor());
                    props = ComponentUtils.getComponentVersionProperies(compName);
                }
                new About(comp,props);
            }
        });
        //aboutMenuItem.addActionListener(new About(this,props));
        helpMenu.add(aboutMenuItem);

        deliveryLiteMenuBar.add(helpMenu);

        setJMenuBar(deliveryLiteMenuBar);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(deliveryLiteTabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 964, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(deliveryLiteTabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 725, Short.MAX_VALUE))
        );

        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        resetTitle(QiProjectDescUtils.getQiProjectName(qpDesc));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void numLayersComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numLayersComboBoxActionPerformed
        //if the user has already specified the number of initial layers, ignore request
        
        List<Inversion.ModelDescription.MiddleLayer> layers =
            agent.getInversion().getModelDescription().getMiddleLayer();
        int size = layers.size() + 2;
        int selectedNum = Integer.parseInt((String)numLayersComboBox.getSelectedItem());
        if(selectedNum == size)
            return;

        if (numLayersSpecified) return;
        numLayersSpecified = true;

        if(selectedNum < size){
            int rowsToRemoved = size - selectedNum;
            FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
            HorizonLineTableModel tableModel = (HorizonLineTableModel)layerTable.getModel();
            for(int i = 1; i <= rowsToRemoved; i++){
                fixedTableModel.removeRow(i);
                tableModel.removeRow(i);
                layers.remove(i-1);
            }
            fixedTableModel.fireTableRowsDeleted(1, rowsToRemoved);
            tableModel.fireTableRowsDeleted(1, rowsToRemoved);
            numLayersComboBox.setEnabled(false);
            return;
        }
        //add number of additional layers after the 2nd layer. Initially there are 3 layers
        int atRow = 1;  //index of 2nd row
        int additionalRows = selectedNum - size;
        for (int i=1; i<=additionalRows; i++) {
            //add row to both layer tables
            FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
            fixedTableModel.addRow(atRow);
            fixedTableModel.fireTableRowsInserted(atRow, atRow);
            fixedLayerTable.addRowSelectionInterval(atRow, atRow);
            DefaultTableModel tableModel = (DefaultTableModel)layerTable.getModel();
            String newTime = getEstimatedNewTime(atRow);
            String newTVDBML = getEstimatedNewTVDBML(atRow);
            Inversion.ModelDescription.MiddleLayer layer = new Inversion.ModelDescription.MiddleLayer();
            VsFloat vsf = new VsFloat();
            vsf.setValue(Float.valueOf(newTime).floatValue());
            layer.setTime(vsf);
            VarFloat varf = new VarFloat();
            varf.setValue(Float.valueOf(DeliveryLiteConstants.DEFAULT_SIGMA_TIME).floatValue());
            layer.setSigmaTime(varf);
            vsf = new VsFloat();
            vsf.setValue(Float.valueOf(DeliveryLiteConstants.DEFAULT_LFIV).floatValue());
            layer.setLFIV(vsf);
            varf = new VarFloat();
            varf.setValue(Float.valueOf(newTVDBML).floatValue());
            layer.setDepthRockCurves(varf);
            vsf = new VsFloat();
            vsf.setValue(0);
            layer.setNetToGross(vsf);
            layers.add(0,layer);
            tableModel.insertRow(atRow, new Object [] {newTime, DeliveryLiteConstants.DEFAULT_SIGMA_TIME, "", "",newTVDBML, DeliveryLiteConstants.DEFAULT_LFIV, "0.0", "","", "", "", "","", "", "", ""});
            tableModel.fireTableRowsInserted(atRow, atRow);

            //remember layer type for row added
            rowLayerTypes.add(atRow, SHALE_LAYER_TYPE);
        }
        numLayersComboBox.setEnabled(false);
    }//GEN-LAST:event_numLayersComboBoxActionPerformed

    public int getNumberOfInitialLayers(){
    	int selectedNum = 0;
    	try{
    		selectedNum = Integer.parseInt((String)numLayersComboBox.getSelectedItem());
    	}catch(NumberFormatException e){
    		e.printStackTrace();
    	}
    	return selectedNum;
    }

    public int getNumberOfCurrentLayers(){
        return layerTable.getRowCount();
    }

    private void updateInversionObject(){
        updateInversionInfo();
        updateSeismicData();
        createLayers();
        updateFluidsPropertiesInfo();
        updateRockPropertiesInfo();
    }

    private void updateInversionInfo(){
        String modelName = modelNameTextField.getText().trim();
        agent.getInversion().getInversionInfo().setName(modelName);
    }

    public void runPostInversionButtonActionPerformed(){
    	String err = validateModel();
    	if(err != null && err.length() > 0){
    		JOptionPane.showMessageDialog(this, err, "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
    		return;
    	}
    	err = "";
    	err = validateLiteParams();
    	if(err != null && err.length() > 0){
    		JOptionPane.showMessageDialog(this, err, "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
    		return;
    	}

    	String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
    	String filename = agent.getModelDir() + filesep + XML_MODEL_FILE_POST;
   		if(!toXmlInversionFile(filename))
   			return;
   		String scriptPrefix = null;
   		if(nearOnlyRadioButton.isSelected())
   			scriptPrefix = writeInversionScript("/xml/post_near_template.txt", getOutputBasename(),"post","near");
   		else
   			scriptPrefix = writeInversionScript("/xml/post_near_far_template.txt", getOutputBasename(),"post","near_far");
   		if(scriptPrefix == null)
   			return;

   		QiProjectDescriptor qiProjectDesc = agent.getQiProjectDescriptor();
        String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);

        String filePrefix = scriptDir + filesep + scriptPrefix;
        // Create a job monitor for the script.
        monitor = new JobMonitor(filePrefix, scriptDir, scriptPrefix);
        // Create a job manager for the script.
        manager = new JobManager(agent.getMessagingMgr(), this);
        List<Component> components = getListOfRunActionComponents();
        List<Component> cancelRunButtons = new ArrayList<Component>();
        cancelRunButtons.add(cancelRunButton);
        cancelRunButtons.add(cancelRunMenuItem);
        runInversionScript(filePrefix,monitor,manager,components,cancelRunButtons,stdoutTextArea,stderrTextArea, fileSummaryTextArea);
    }
    public void runPriorInversionButtonActionPerformed(){
    	String err = validateModel();
    	if(err != null && err.length() > 0){
    		JOptionPane.showMessageDialog(this, err, "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
    		return;
    	}
    	err = "";
    	err = validateLiteParams();
    	if(err != null && err.length() > 0){
    		JOptionPane.showMessageDialog(this, err, "Invalid Data Entry", JOptionPane.WARNING_MESSAGE);
    		return;
    	}

    	String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
    	String filename = agent.getModelDir() + filesep + XML_MODEL_FILE_PRIOR;
   		if(!toXmlInversionFile(filename))
   			return;
   		
   		String scriptPrefix = null;
   		if(nearOnlyRadioButton.isSelected())
   			scriptPrefix = writeInversionScript("/xml/prior_near_template.txt", getOutputBasename(),"prior","near");
   		else
   			scriptPrefix = writeInversionScript("/xml/prior_near_far_template.txt", getOutputBasename(),"prior","near_far");
   		if(scriptPrefix == null)
   			return;
   		

        QiProjectDescriptor qiProjectDesc = agent.getQiProjectDescriptor();
        String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);

        String filePrefix = scriptDir + filesep + scriptPrefix;
        // Create a job monitor for the script.
        monitor = new JobMonitor(filePrefix, scriptDir, scriptPrefix);
        // Create a job manager for the script.
        manager = new JobManager(agent.getMessagingMgr(), this);
        List<Component> components = getListOfRunActionComponents();
        List<Component> cancelComps = new ArrayList<Component>();
        cancelComps.add(cancelRunButton);
        cancelComps.add(cancelRunMenuItem);
        runInversionScript(filePrefix,monitor,manager,components,cancelComps,stdoutTextArea,stderrTextArea, fileSummaryTextArea);
    }

    private List<Component> getListOfRunActionComponents(){
    	List<Component> components = new ArrayList<Component>();
    	components.add(runPriorInversionButton);
        components.add(runPostInversionButton);
        components.add(runPriorMenuItem);
        components.add(runPostInverstionMenuItem);
        if(showModelDialog != null && showModelDialog.isVisible()){
        	List<Component> list = showModelDialog.getListOfRunComponents();
        	for(Component comp : list)
        		components.add(comp);
        }
        return components;
    }
    
    private void updateSeismicData(){
        List<Inversion.SeismicData.Stack> stacks = agent.getInversion().getSeismicData().getStack();
        List<Inversion.SeismicData.Stack> stacks2 = new ArrayList<Inversion.SeismicData.Stack>();

        if(stacks.size() >= 1){
            Inversion.SeismicData.Stack stack = stacks.get(0);  //Near stack
            stack.setName(DeliveryLiteConstants.DEFAULT_NEAR_STACK_NAME);
            stack.setFilename(DeliveryLiteConstants.DEFAULT_NEAR_STACK_FILE_NAME);
            String val = (String)stackParametersTable.getValueAt(0, 0); //Near Xmin
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(val));
                stack.setMinOffset(vf);
            }
            val = (String)stackParametersTable.getValueAt(0, 1); //Near Xmax
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    stack.setMaxOffset(vf);
                }catch(NumberFormatException e){}

            }
            val = (String)stackParametersTable.getValueAt(0, 2); //Near ve
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    stack.setStackVelocity(vf);
                }catch(NumberFormatException e){}
            }
            val = (String)stackParametersTable.getValueAt(0, 3); //Near time
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    stack.setReflectorTime(vf);
                }catch(NumberFormatException e){}
            }
            stack.getWavelet().setFilename(DeliveryLiteConstants.DEFAULT_NEAR_STACK_WAVELET_FILE_NAME);
            val = nearNoiseRMSTextField.getText();
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    stack.getWavelet().setNoiseRms(vf);
                }catch(NumberFormatException e){}
            }
            stacks2.add(stack);
        }
        Inversion.SeismicData.Stack stack;
        if(nearAndFarRadioButton.isSelected()){
            if(stacks.size() >= 2){
                stack = stacks.get(1);  //Near stack
            }else{
                stack = new Inversion.SeismicData.Stack();
            }

            stack.setName(DeliveryLiteConstants.DEFAULT_FAR_STACK_NAME);
            stack.setFilename(DeliveryLiteConstants.DEFAULT_FAR_STACK_FILE_NAME);
            String val = (String)stackParametersTable.getValueAt(1, 0); //Near Xmin
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(val));
                stack.setMinOffset(vf);
            }
            val = (String)stackParametersTable.getValueAt(1, 1); //Near Xmax
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    stack.setMaxOffset(vf);
                }catch(NumberFormatException e){}

            }
            val = (String)stackParametersTable.getValueAt(1, 2); //Near ve
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    stack.setStackVelocity(vf);
                }catch(NumberFormatException e){}
            }
            val = (String)stackParametersTable.getValueAt(1, 3); //Near time
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    stack.setReflectorTime(vf);
                }catch(NumberFormatException e){}
            }

            Inversion.SeismicData.Stack.Wavelet wavelet = new Inversion.SeismicData.Stack.Wavelet();
            wavelet.setFilename(DeliveryLiteConstants.DEFAULT_FAR_STACK_WAVELET_FILE_NAME);
            val = farNoiseRMSTextField.getText();
            if(val != null && val.trim().length() > 0){
                VarFloat vf = new VarFloat();
                try{
                    float f = Float.parseFloat(val);
                    vf.setValue(f);
                    wavelet.setNoiseRms(vf);
                    stack.setWavelet(wavelet);
                }catch(NumberFormatException e){}
            }
            stacks2.add(stack);
        }
        stacks.clear();
        for(Inversion.SeismicData.Stack s : stacks2){
            stacks.add(s);
        }

        Inversion.SeismicData.AVOTerms avoTerms = agent.getInversion().getSeismicData().getAVOTerms();
        String val =  avoATextField.getText();
        if(val != null && val.trim().length() > 0){
            try{
                float f = Float.parseFloat(val);
                avoTerms.getA().setValue(f);
            }catch(NumberFormatException e){}
        }
        val =  avoBTextField.getText();
        if(val != null && val.trim().length() > 0){
            try{
                float f = Float.parseFloat(val);
                avoTerms.getB().setValue(f);
            }catch(NumberFormatException e){}
        }
    }


    public String getEp(){
        return epTextField.getText();
    }

    public String getCdp(){
        return cdpTextField.getText();
    }
    
    public String getSpaghettiMinTime(){
    	return tmin4SpaghettiPlotTextField.getText().trim();
    }
    
    public String getSpaghettiMaxTime(){
    	return tmax4SpaghettiPlotTextField.getText().trim();
    }
    
    public void initNearStackRangeFields(){
        nearInlineTextField.setText("");
        nearInlineToTextField.setText("");
        nearInlineByTextField.setText("");
        nearCrosslineTextField.setText("");
        nearCrosslineToTextField.setText("");
        nearCrosslineByTextField.setText("");
    }

    public void initFarStackRangeFields(){
        farInlineTextField.setText("");
        farInlineToTextField.setText("");
        farInlineByTextField.setText("");
        farCrosslineTextField.setText("");
        farCrosslineToTextField.setText("");
        farCrosslineByTextField.setText("");
    }

    public IComponentDescriptor getResponseComponentDesc(){
        return responseComponentDesc;
    }

    public void setResponseComponentDesc(IComponentDescriptor cd){
        responseComponentDesc = cd;
    }


    private Document inversionToDocument(){
        Inversion inversion = agent.getInversion();
        if (inversion == null) return null;
        Document doc = null;
        try{
            ClassLoader cl = this.getClass().getClassLoader();
            JAXBContext context
              = JAXBContext.newInstance("com.bhpb.xsdparams.delivery",
                                       cl);

            Marshaller marshaller = context.createMarshaller();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.newDocument();
            marshaller.marshal( inversion, doc );
            return doc;
        }catch(JAXBException je){
            je.printStackTrace();

        }catch(ParserConfigurationException pe){
            pe.printStackTrace();
        }
        return null;
    }


    private QiFileChooserDescriptor formQiFileChooserDescriptor(String title, String startDir, GenericFileFilter filter,
            boolean dirRem, boolean navUpEnabled, boolean multiFiles, int type, String command){
        QiFileChooserDescriptor desc = new QiFileChooserDescriptor();
        desc.setParentGUI(this);
        desc.setTitle(title);
        desc.setHomeDirectory(startDir);
        desc.setDirectoryRememberedEnabled(dirRem);
        desc.setFileFilter(filter);
        desc.setNavigationUpwardEnabled(navUpEnabled);
        desc.setMultiSelectionEnabled(multiFiles);
        desc.setetProducerComponentDescriptor(agent.getComponentDescriptor());
        desc.setDialogType(type);
        desc.setMessageCommand(command);
        desc.setServerUrl(agent.getMessagingMgr().getTomcatURL());
        return desc;
    }
    private void exportStackPropertiesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportStackPropertiesButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_exportStackPropertiesButtonActionPerformed

    private void importStackPropertiesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importStackPropertiesButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_importStackPropertiesButtonActionPerformed

    private void exportFluidPropertiesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportFluidPropertiesButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_exportFluidPropertiesButtonActionPerformed

    private void importFluidPropertiesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importFluidPropertiesButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_importFluidPropertiesButtonActionPerformed

    private void exportMatrixPropertiesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportMatrixPropertiesButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_exportMatrixPropertiesButtonActionPerformed

    private void importMatrixPropertiesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importMatrixPropertiesButtonActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_importMatrixPropertiesButtonActionPerformed

    private void deleteLayer2ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLayer2ButtonActionPerformed
        deleteLayer1ButtonActionPerformed(evt);
    }//GEN-LAST:event_deleteLayer2ButtonActionPerformed

    private void deleteLayer1ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLayer1ButtonActionPerformed
        int atRow = selectedRow;
        //do nothing if no row is selected
        if (atRow == -1) return;

        //once a row has been deleted, one cannot select the number of initial rows
        numLayersSpecified = true;

        //cannot delete the first or last row
        if (atRow == 0 || atRow == fixedLayerTable.getRowCount()-1) return;

        //delete row to both layer tables
        FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
        fixedTableModel.deleteRow(atRow);
        fixedTableModel.fireTableRowsDeleted(atRow, atRow);

        HorizonLineTableModel tableModel = (HorizonLineTableModel)layerTable.getModel();
        tableModel.removeRow(atRow);
        tableModel.fireTableRowsDeleted(atRow, atRow);

        //delete layer type for row deleted
        rowLayerTypes.remove(atRow);
        List<Inversion.ModelDescription.MiddleLayer> layers =
               agent.getInversion().getModelDescription().getMiddleLayer();
        layers.remove(atRow-1);
        //clear selection
        fixedLayerTable.clearSelection();
        layerTable.clearSelection();
    }//GEN-LAST:event_deleteLayer1ButtonActionPerformed

    private void displayResultsButtonActionPerformed(java.awt.event.ActionEvent evt) {
//GEN-FIRST:event_displayResultsButtonActionPerformed
        // Launch a new 2D viewer (qiViewer) for displaying the AmpExt results
        IComponentDescriptor qiViewerDesc = ComponentUtils.launchNewComponent(QIWConstants.QI_VIEWER_NAME, agent.getMessagingMgr());

        //CANNOT proceed if qiViewerDesc null
        if (qiViewerDesc == null) {
            JOptionPane.showMessageDialog(this, "Cannot open a new qiViewer for displaying results", "Launch Error", JOptionPane.ERROR_MESSAGE);
        } else {
            //Start a control session
            ArrayList actions = new ArrayList();
            actions.add("action="+ControlConstants.START_CONTROL_SESSION_ACTION);
            ArrayList actionParams = new ArrayList();
            actionParams.add(qiViewerDesc);
            actions.add(actionParams);
            agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
        }
    }//GEN-LAST:event_displayResultsButtonActionPerformed

    private void cancelRunButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelRunButtonActionPerformed
// TODO add your handling code here:
        if (manager.cancelJob1(monitor)) {
            // update GUI job monitoring components
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    stateLabel.setText(monitor.getJobStatus1());
                    // enable another script to be generated and executed
                    List<Component> components = getListOfRunActionComponents();
                    for(Component comp : components){
          				if(comp != null)
           					comp.setEnabled(true);
           			}
                    List<Component> cancelComps = new ArrayList<Component>();
        	        cancelComps.add(cancelRunButton);
        	        cancelComps.add(cancelRunMenuItem);
        	        for(Component comp : cancelComps){
           				if(comp != null)
           					comp.setEnabled(false);
           			}
        	        displayResultsMenuItem.setEnabled(false);
        			displayResultsButton.setEnabled(false);
                    
                    checkStatusButton.setEnabled(true);
                    statusMenuItem.setEnabled(true);

                    stdoutTextArea.setText(monitor.fetchStdoutText());
                    stderrTextArea.setText(monitor.fetchStderrText());
                }
            });
        }
    }//GEN-LAST:event_cancelRunButtonActionPerformed



    /**
     * Submit the generated script, which has already been saved, for execution.
     * Initialize GUI job monitoring components.
     *
     * @param scriptPrefix Prefix of script to be executed.
     */
    private void executeScript(String scriptPrefix) {
       //Get the most recent project descriptor
        QiProjectDescriptor qiProjectDesc = agent.getQiProjectDescriptor();
        String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);
        String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        String filePrefix = scriptDir + filesep + scriptPrefix;
        // Create a job monitor for the script.
        monitor = new JobMonitor(filePrefix, scriptDir, scriptPrefix);
        // Create a job manager for the script.
        manager = new JobManager(agent.getMessagingMgr(), this);

        String scriptFile = monitor.getScriptPath();
        String stdoutFile = monitor.getStdoutPath();
        String stderrFile = monitor.getStderrPath();

        ArrayList params = new ArrayList();
        // make sure script is executable
        params.add("cd " + scriptDir + "; chmod ug+x "
                + scriptFile + "; " + scriptFile + " 1>" + stdoutFile + " 2>" + stderrFile);
        params.add("false");
        // submit the script as a possibly long running job
        manager.submitJob(params);

        monitor.setJobStatusToRunning();

        // update GUI job monitoring components
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                stateLabel.setText(monitor.getJobStatus1());
                // disable the ability to run another generated script
                runPriorMenuItem.setEnabled(false);
                runPriorInversionButton.setEnabled(false);
                displayResultsMenuItem.setEnabled(false);
                displayResultsButton.setEnabled(false);
                // enable the ability to check status and cancel the job
                checkStatusButton.setEnabled(true);
                statusMenuItem.setEnabled(true);
                cancelRunButton.setEnabled(true);
                cancelRunMenuItem.setEnabled(true);

                stdoutTextArea.setText("");
                stderrTextArea.setText("");

                String text = monitor.getScriptPath() + "\n";
                text += monitor.getStderrPath() + "\n";
                text += monitor.getStdoutPath() + "\n";
                text += "have been created in " + monitor.getScriptDir();
                fileSummaryTextArea.setText(text);
            }
        });
    }


	private void runInversionScript(String filePathPrefix, final JobMonitor monitor, final JobManager manager, final List<Component> components, final List<Component> stops, final JTextArea status, final JTextArea errStatus, JTextArea summary) {
        int ind = filePathPrefix.lastIndexOf("/");
        String scriptNamePrefix = filePathPrefix;
        if(ind != -1){
            scriptNamePrefix = scriptNamePrefix.substring(ind+1,scriptNamePrefix.length());
        }
        String scriptName = scriptNamePrefix + ".sh";
		String logFile = scriptNamePrefix + ".out";
		String errFile = scriptNamePrefix + ".err";
		String scriptPath = filePathPrefix + ".sh";
		ArrayList params = new ArrayList();
		params.add("cd " + monitor.getScriptDir() + "; chmod ug+x "
				+ scriptPath + "; sh " + scriptName + " 1> " + logFile + " 2>" + errFile);
		params.add("false");//non blocking
		//submit the script as a possibly long running job
		manager.submitJob(params); //non blocking flag
		monitor.setJobStatusToRunning();
		status.setText("");
        errStatus.setText("");
        stateLabel.setText("");

		String text = scriptName + "\n";
		text += logFile + "\n";
		text += errFile + "\n";
		text += "have been created in " + monitor.getScriptDir();
		summary.setText(text);
		for(Component comp : components){
			if(comp != null)
				comp.setEnabled(false);
		}
		
		for(Component comp : stops){
			if(comp != null)
				comp.setEnabled(true);
		}
		startJobStatusWorker(components,stops,status,errStatus);
	}

	private void startJobStatusWorker(final List<Component> components,final List<Component> stops,final JTextArea status,final JTextArea errStatus){
		if(monitor == null || manager == null)
			return;
		Runnable heavyRunnable = new Runnable(){
			public void run(){
				int time = 5000;
				while(monitor.isJobRunning()){
					if(!updateStatus(monitor,manager,components,stops,status,errStatus)){
						for(Component comp : components){
							if(comp != null)
								comp.setEnabled(false);
						}
						return;
					}
					try{
						Thread.sleep(time);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
				}
				checkStatusClicked = false;
				List<Component> checkStatusButtons = new ArrayList<Component>();
		        checkStatusButtons.add(checkStatusButton);
		        checkStatusButtons.add(statusMenuItem);
                if(!monitor.isJobEnded()){
                    for(Component cm: checkStatusButtons)
                        cm.setEnabled(true);
                }
				
				return;
			}
		};
		new Thread(heavyRunnable).start();
	}
	
	private boolean updateStatus(final JobMonitor monitor, JobManager manager, final List<Component> components,final List<Component> stops, final javax.swing.JTextArea area, final javax.swing.JTextArea errArea){
		if(monitor == null || manager == null)
			return false;
		String status = monitor.updateStatus1(manager);
		if(status.equals(JobMonitor.JOB_UNKNOWN_STATUS)){
			JOptionPane.showMessageDialog(null, "Unexpected error in getting process ids. Please check the logs or seek workbench support.", "Unexpected Error.", JOptionPane.WARNING_MESSAGE);
			List<Component> checkStatusButtons = new ArrayList<Component>();
	        checkStatusButtons.add(checkStatusButton);
	        checkStatusButtons.add(statusMenuItem);
	        for(Component cm: checkStatusButtons)
	        	cm.setEnabled(true);
			checkStatusClicked = false;
			return false;
		}
		Runnable updateGUI = new Runnable() {
			public void run() {
				if (monitor.isJobEnded()){
					for(Component comp : components){
						if(comp != null)
							comp.setEnabled(true);
					}
					if(showModelDialog != null)
						showModelDialog.setListOfRunComponentsEnabled(true);
					for(Component comp : stops){
						if(comp != null)
						comp.setEnabled(false);
					}
					displayResultsMenuItem.setEnabled(true);
					displayResultsButton.setEnabled(true);
				}else{
					for(Component comp : components)
						if(comp != null)						
							comp.setEnabled(false);
					if(showModelDialog != null)
						showModelDialog.setListOfRunComponentsEnabled(false);
					
					for(Component comp : stops){
						if(comp != null)
						comp.setEnabled(true);
					}

					displayResultsMenuItem.setEnabled(false);
                    displayResultsButton.setEnabled(false);
				}
				stateLabel.setText(monitor.getJobStatus1());
				//area.setText(monitor.fetchStdoutText());
				//errArea.setText(monitor.fetchStderrText());
				area.setText(monitor.getStdoutText());
				errArea.setText(monitor.getStderrText());

			}
		};
		javax.swing.SwingUtilities.invokeLater(updateGUI);
		return true;
	}


    private void runInversionScript(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runScriptsButtonActionPerformed
        String err = validateParams();
        err += validateLiteParams();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
            return;
        }
        String path = writeInversionScript("/xml/invert_template.txt",invertName);
        if (path == null) return;

        String scriptFile = path + ".sh";
        String stdOutFile = path + ".out";
        String stdErrFile = path + ".err";
        String scriptDir = agent.getQiProjectDescriptor().getTempReloc();

        stateLabel.setText("Running \n");
        stdoutTextArea.setText("The script to be run ....\n");
        String temp = "cd " + scriptDir + "; ";
        String runCmd = temp;
        stdoutTextArea.append(temp + "\n");
        temp   = "chmod u+x " + scriptFile + "; ";
        stdoutTextArea.append(temp + "\n");
        runCmd += temp;
        temp = scriptFile + " 1>" + stdOutFile + " 2>" + stdErrFile;
        runCmd += temp;
        stdoutTextArea.append(temp + "\n");
        stderrTextArea.setText("started ...");

        ArrayList<String> params = new ArrayList<String>();
        params.add(runCmd);
        params.add("false");
        agent.submitJob(params);

        runMenuItem.setEnabled(false);

    }//GEN-LAST:event_runScriptsButtonActionPerformed

    private void runAnalyzerScript(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_writeModelScriptsButtonActionPerformed
        String err = validateParams();
        err += validateLiteParams();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
            return;
        }
        String path = writeAnalyzerScript();
        if (path == null) return;

        String scriptFile = path + ".sh";
        String stdOutFile = path + ".out";
        String stdErrFile = path + ".err";
        String scriptDir = agent.getQiProjectDescriptor().getTempReloc();

        stateLabel.setText("Running \n");
        stdoutTextArea.setText("The script to be run ....\n");
        String temp = "cd " + scriptDir + "; ";
        String runCmd = temp;
        stdoutTextArea.append(temp + "\n");
        temp   = "chmod u+x " + scriptFile + "; ";
        stdoutTextArea.append(temp + "\n");
        runCmd += temp;
        temp = scriptFile + " 1>" + stdOutFile + " 2>" + stdErrFile;
        runCmd += temp;
        stdoutTextArea.append(temp + "\n");
        stderrTextArea.setText("started ...");

        ArrayList<String> params = new ArrayList<String>();
        params.add(runCmd);
        params.add("false");
        agent.submitJob(params);

        runMenuItem.setEnabled(false);
    }//GEN-LAST:event_writeModelScriptsButtonActionPerformed

    private void validateModelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validateModelButtonActionPerformed
       String ret = validateModel();
       if (ret.length() == 0)
          JOptionPane.showMessageDialog(this,"Parameters Are Valid !!");
       else {
          JOptionPane.showMessageDialog(this, ret);
       }
    }//GEN-LAST:event_validateModelButtonActionPerformed

    private void writeModelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_writeModelButtonActionPerformed
        String ret = validateParams();
        if (ret.length() == 0) {
           //createLayers();
           //updateFluidsPropertiesInfo();
           updateInversionObject();
           toXmlInversionFile(null);
        } else {
          JOptionPane.showMessageDialog(this, ret);
        }
    }//GEN-LAST:event_writeModelButtonActionPerformed

    private void addLayer2ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addLayer2ButtonActionPerformed
         addLayerButtonActionPerformed(evt);
    }//GEN-LAST:event_addLayer2ButtonActionPerformed

    private void addLayerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addLayerButtonActionPerformed
        int atRow = selectedRow;
        //do nothing if no row is selected
        if (atRow == -1) return;

        //once a row has been deleted, one cannot select the number of initial rows
        numLayersSpecified = true;
        if (atRow == 0) atRow = 1;
        //add row to both layer tables
        FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
        fixedTableModel.addRow(atRow);
        fixedTableModel.fireTableRowsInserted(atRow, atRow);
        String newTime = getEstimatedNewTime(atRow);
        String newTVDBML = getEstimatedNewTVDBML(atRow);
//        fixedLayerTable.addRowSelectionInterval(atRow, atRow);
        HorizonLineTableModel tableModel = (HorizonLineTableModel)layerTable.getModel();
        tableModel.insertRow(atRow, new Object [] {newTime, DeliveryLiteConstants.DEFAULT_SIGMA_TIME, "", "",newTVDBML, DeliveryLiteConstants.DEFAULT_LFIV, "0.0", "","", "", "", ""});
        //tableModel.insertRow(atRow, new Object [] {"", DeliveryLiteConstants.DEFAULT_SIGMA_TIME, "", "","", DeliveryLiteConstants.DEFAULT_LFIV, "", "","", "", "", "","", "", "", ""});
        tableModel.fireTableRowsInserted(atRow, atRow);
        List<Inversion.ModelDescription.MiddleLayer> layers =
               agent.getInversion().getModelDescription().getMiddleLayer();
        Inversion.ModelDescription.MiddleLayer layer =
          genDefaultMiddleLayer(Float.valueOf(newTime).floatValue(), Float.valueOf(newTVDBML).floatValue());
        layers.add(atRow-1,layer);
        //remember layer type for row added
        rowLayerTypes.add(atRow, SHALE_LAYER_TYPE);

        //clear selection
        fixedLayerTable.clearSelection();
        layerTable.clearSelection();
    }//GEN-LAST:event_addLayerButtonActionPerformed

    private Inversion.ModelDescription.MiddleLayer genDefaultMiddleLayer(float time, float tvdbml){
        Inversion.ModelDescription.MiddleLayer layer = new Inversion.ModelDescription.MiddleLayer();
        Inversion.ModelDescription.MiddleLayer.NonreservoirEndmember nonreservoirEndmember =
                new Inversion.ModelDescription.MiddleLayer.NonreservoirEndmember();
        nonreservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME);
        Inversion.ModelDescription.MiddleLayer.ReservoirEndmember reservoirEndmember =
                new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember();
        reservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
        layer.setReservoirEndmember(reservoirEndmember);
        layer.setNonreservoirEndmember(nonreservoirEndmember);
        VsFloat vf = new VsFloat();
        vf.setValue(time);
        layer.setTime(vf);
        VarFloat vaf = new VarFloat();
        vaf.setValue(Float.valueOf(DeliveryLiteConstants.DEFAULT_SIGMA_TIME).floatValue());
        layer.setSigmaTime(vaf);
        vf = new VsFloat();
        vf.setValue(Float.valueOf(DeliveryLiteConstants.DEFAULT_LFIV).floatValue());
        layer.setLFIV(vf);
        vaf = new VarFloat();
        vaf.setValue(tvdbml);
        layer.setDepthRockCurves(vaf);
        return layer;
    }
    
    private void nearOnlyRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nearOnlyRadioButtonActionPerformed
        farStackFilenameTextField.setEditable(false);
        farStackWaveletFilenameTextField.setEditable(false);
        farNoiseRMSTextField.setEditable(false);
        if(showModelDialog != null && showModelDialog.isVisible()){
        	showModelDialog.dispose();
        }
        showModelDialog = null;
        //((DefaultTableModel)stackParametersTable.getModel()).fireTableRowsUpdated(1,1);
    }//GEN-LAST:event_nearOnlyRadioButtonActionPerformed

    private void nearAndFarRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nearAndFarRadioButtonActionPerformed
        farStackFilenameTextField.setEditable(true);
        farStackWaveletFilenameTextField.setEditable(true);
        farNoiseRMSTextField.setEditable(true);
        if(showModelDialog != null && showModelDialog.isVisible()){
        	showModelDialog.dispose();
        }
        showModelDialog = null;
    }//GEN-LAST:event_nearAndFarRadioButtonActionPerformed

    private void numberRunsTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numberRunsTextFieldActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_numberRunsTextFieldActionPerformed

    private boolean checkStatusClicked = false;
    private void checkStatusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkStatusButtonActionPerformed
        //displayResultsMenuItem.setEnabled(true);
		//displayResultsButton.setEnabled(true);
		if(monitor != null && manager != null){
			String scriptPrefix = monitor.getScriptPrefix();
			StringBuffer buf = new StringBuffer();
			buf.append(scriptPrefix + ".sh\n");;
			buf.append(scriptPrefix + ".out\n");;
			buf.append(scriptPrefix + ".err\n");;
			buf.append("have been created in " + monitor.getScriptDir());
			fileSummaryTextArea.setText(buf.toString());
			//if(monitor.isJobRunning())
			//	return;
			if(checkStatusClicked)// to prevent the user from constantly clicking the check status button
				return;
			checkStatusClicked = true;
			List<Component> checkStatusButtons = new ArrayList<Component>();
	        checkStatusButtons.add(checkStatusButton);
	        checkStatusButtons.add(statusMenuItem);
	        for(Component cm: checkStatusButtons)
	        	cm.setEnabled(false);

			List<Component> components = getListOfRunActionComponents();
			components.add(displayResultsMenuItem);
			components.add(displayResultsButton);
	        List<Component> cancelComps = new ArrayList<Component>();
	        cancelComps.add(cancelRunButton);
	        cancelComps.add(cancelRunMenuItem);
   	        for(Component comp : components){
  				if(comp != null)
   					comp.setEnabled(false);
   			}
	    			
   			for(Component comp : cancelComps){
   				if(comp != null)
   					comp.setEnabled(true);
   			}
   			monitor.setJobStatusToRunning();
   			startJobStatusWorker(components,cancelComps,stdoutTextArea,stderrTextArea);
		}
		
    }//GEN-LAST:event_checkStatusButtonActionPerformed

    private void deliveryLiteTabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_deliveryLiteTabbedPaneStateChanged
        int index = deliveryLiteTabbedPane.getSelectedIndex();
        if (index != tabIndex) {
            //validatePane(tabIndex);
            tabIndex = index;

        }
    }//GEN-LAST:event_deliveryLiteTabbedPaneStateChanged

    private void prefsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prefsMenuItemActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_prefsMenuItemActionPerformed

    private void saveQuitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveQuitMenuItemActionPerformed
        String err = hasError();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
            return;
        } else {
            agent.saveStateThenQuit();
            if(showModelDialog != null && showModelDialog.isVisible())
            	showModelDialog.dispose();
            showModelDialog = null;
//           saveAll();
//           agent.deactivateSelf();
        }
    }//GEN-LAST:event_saveQuitMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        //String err = hasError();
        //if (err.length() > 0) {
        //    JOptionPane.showMessageDialog(this, err);
        //} else {
            agent.saveState();
//           saveAll();
        //}
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        String err = hasError();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
        } else {
            agent.saveStateAsClone();
//           saveAll();
        }
    }


    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitMenuItemActionPerformed
        Component comp = this;
        if(responseComponentDesc != null){
            int n = JOptionPane.showConfirmDialog(
                    comp, "One component (" + responseComponentDesc.getPreferredDisplayName() + ") is found dependent on this component. Quit will also quit the associated component. Is this ok?",
                    "Confirm to proceed",
                    JOptionPane.YES_NO_OPTION);
            if(n == JOptionPane.YES_OPTION){
                IComponentDescriptor wbMgr =    agent.getMessagingMgr().getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
                agent.getMessagingMgr().sendRequest(QIWConstants.CMD_MSG,QIWConstants.QUIT_COMPONENT_CMD,wbMgr,
                                       QIWConstants.STRING_TYPE, responseComponentDesc);
            }else
                return;
        }
        agent.deactivateSelf();
        if(showModelDialog != null && showModelDialog.isVisible())
        	showModelDialog.dispose();
        showModelDialog = null;
    }//GEN-LAST:event_quitMenuItemActionPerformed

    private void importMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importMenuItemActionPerformed
        //Use a file chooser to select the state file

    	String [] filterList = { ".xml",".XML" };
        GenericFileFilter filter =  new GenericFileFilter(filterList,"Model Description File (*.xml)");
        String startDir = QiProjectDescUtils.getXmlPath(agent.getQiProjectDescriptor());
        QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Select Model xml file",startDir,filter,true,
                true,false,
                javax.swing.JFileChooser.OPEN_DIALOG,
                DeliveryLiteConstants.GET_MODEL_XML_FILE_CMD);
        agent.callFileChooser(desc);
/*
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        JFileChooser fc = new JFileChooser(QiProjectDescUtils.getTempPath(projDesc));
        fc.setDialogTitle("Import Model ...");
        EditorFileFilter filter = new EditorFileFilter(
                new String[] {"xml", "XML"}, "Model Description File");
        fc.addChoosableFileFilter(filter);
        fc.setFileFilter(filter);
        //fc.setControlButtonsAreShown(false);
        int result = fc.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            String xmlFile = fc.getSelectedFile().getPath();
            importModel(xmlFile,null);
        }
*/
        //fc.addPropertyChangeListener(createSelectionListener(this));
        //ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.IMPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingMgr());

        //NOTE: agent will invoke a file chooser service. When it processes the GET_RESULT_FROM_FILE_CHOOSER_CMD,
        //     it will read the state of the PM in XML and restore it using its restoreState() method.
    }//GEN-LAST:event_importMenuItemActionPerformed

    public void exportModelXML(String filePath){
    	updateInversionObject();
    	//Strip off [.xml, .XML] from XML file pathname.
        String baseFile = getFileBase(filePath);
        String filePath1 = baseFile + ".xml";
        //Write out XML model
        toXmlInversionFile(filePath1); //full path of XML file
        //Reset model in title to that of the exported file.
        agent.setModelXML(filePath1);
        resetTitle(null);
    }

    public void exportModelParamsXML(String filePath){
    	String xml = genState();
    	//Strip off [.xml, .XML] from XML file pathname.
        String baseFile = getFileBase(filePath);
        String filePath1 = baseFile + ".xml";
        if(agent.writeStringToFile(filePath1, xml) == true){
        	agent.setModelXML(filePath1);
        	resetTitle(null);
        }
    }

    private void exportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportMenuItemActionPerformed
        //Use a file chooser to specify the path of the file where the qiComponent's state will be saved.
        //QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        //ComponentStateUtils.callFileChooser(this, QiProjectDescUtils.getXmlPath(projDesc), QIWConstants.EXPORT_COMP_STATE, agent.getComponentDescriptor(), agent.getMessagingMgr());
        String err = validateParams();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
        } else {
        	String [] filterList = { ".xml",".XML" };
            GenericFileFilter filter =  new GenericFileFilter(filterList,"Model Description File (*.xml)");
            String startDir = QiProjectDescUtils.getXmlPath(agent.getQiProjectDescriptor());
            QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Save Model xml file",startDir,filter,false,
                    true,false,
                    javax.swing.JFileChooser.SAVE_DIALOG,
                    DeliveryLiteConstants.SAVE_MODEL_XML_FILE_CMD);
            agent.callFileChooser(desc);
           /*//createLayers();
           //updateFluidsPropertiesInfo();
           updateInversionObject();
           String openFilename = getSaveFile();
           if (openFilename == null) {
             return;
           }

           //Strip off [.xml, .XML] from XML file pathname.
           String baseFile = getFileBase(openFilename);
           String fileName = baseFile + ".xml";
           //Write out XML model
           toXmlInversionFile(fileName); //full path of XML file
           //Reset model in title to that of the exported file.
           agent.setModelXML(fileName);
           resetTitle(null);
           */
        }
    }//GEN-LAST:event_exportMenuItemActionPerformed

    private void typeComboBoxActionPerformed(java.awt.event.ActionEvent evt) {
        int atRow = selectedRow;
        if (selectedRow == -1) return;

        FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
        String layerType = (String)fixedTableModel.getValueAt(selectedRow, LAYER_TYPE_COLUMN);
        rowLayerTypes.set(selectedRow, layerType);

        fixedTableModel.fireTableRowsUpdated(atRow, atRow);
        DefaultTableModel md = (DefaultTableModel)layerTable.getModel();
    	int column = getColIndexByPropertyName(DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH);
        if(layerType.equals(SHALE_LAYER_TYPE)){
        	md.setValueAt("0.0", atRow, column);
        }else if(layerType.equals(SAND_LAYER_TYPE)){
        	md.setValueAt("1.0", atRow, column);
        }
        int col = getColIndexByPropertyName(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL);
        md.setValueAt("", atRow, col);
        col = getColIndexByPropertyName(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS);
        md.setValueAt("", atRow, col);
        col = getColIndexByPropertyName(DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL);
        md.setValueAt("", atRow, col);        
        md.fireTableRowsUpdated(atRow, atRow);
        //clear selection
        fixedLayerTable.clearSelection();
    }

    public static final  int LAYER_TYPE_COLUMN = 2;
    private static class FixedLayerTableModel extends DefaultTableModel {
        public FixedLayerTableModel() {
            super(new Object [][] {
                {"T",    Boolean.valueOf(false), SHALE_LAYER_TYPE, "TOP"},
                {"B",    Boolean.valueOf(false), SHALE_LAYER_TYPE, "BOTTOM"}},
                new String [] {
                    DeliveryLiteConstants.COLUMN_NAME_POUND_SIGN, DeliveryLiteConstants.COLUMN_NAME_ANALYZE, 
                    DeliveryLiteConstants.COLUMN_NAME_TYPE, DeliveryLiteConstants.COLUMN_NAME_LAYER_NAME}
            );
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        boolean[] canEdit = new boolean [] {
            false, true, true, true
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            //Type of first and last row are not editable
            if (columnIndex == 2 &&
                (rowIndex == 0 || rowIndex == getRowCount()-1)) return false;
            return canEdit[columnIndex];
        }

        /* Add a row before the selected row. If the selected row is the last
         * row, add the row before it. If no row is selected, add the row at
         * the end, i.e., before the last row.
         */
        public void addRow(int selectedRow) {
            insertRow(selectedRow, new Object [] {"", Boolean.valueOf(false), SHALE_LAYER_TYPE, ""});
            //renumber rows
            for (int i=0; i<getRowCount(); i++) {
                Vector rowData = (Vector)dataVector.elementAt(i);
                rowData.setElementAt(Integer.toString(i+1), 0);
            }
        }

        //Delete the selected row. The first and last row cannot be deleted.
        public void deleteRow(int selectedRow) {
            removeRow(selectedRow);
            //renumber rows
            for (int i=0; i<getRowCount(); i++) {
                Vector rowData = (Vector)dataVector.elementAt(i);
                rowData.setElementAt(Integer.toString(i+1), 0);
            }
        }

        //Renumber rows.
        public void renumberRows() {
            for (int i=0; i<getRowCount(); i++) {
                Vector rowData = (Vector)dataVector.elementAt(i);
                rowData.setElementAt(Integer.toString(i+1), 0);
            }
        }

        //Determine if Analyze is selected for the specified row
        public boolean isAnalyze(int row) {
            if (row >=getRowCount()) return false;
            Vector rowData = (Vector)dataVector.elementAt(row);
            Boolean analyze = (Boolean)rowData.get(1);
            return analyze.booleanValue();
        }
    }


    class LayerTableModel extends DefaultTableModel {

        public LayerTableModel() {
            super(new Object [][] {
                    {"", "", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", "", ""}
//                  {"", "", "", "", "", "", "", "", "", "", "", "","", ""},
//                    {"", "", "", "", "", "", "", "", "", "", "", "", "", ""}
                },
                new String [] {
                    DeliveryLiteConstants.COLUMN_NAME_TIME,
                    DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME, DeliveryLiteConstants.COLUMN_NAME_THICKNESS,
                    DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS,DeliveryLiteConstants.COLUMN_NAME_TVDBML,
                    DeliveryLiteConstants.COLUMN_NAME_LFIV, DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH,
                    DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG, DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL,
                    DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS, DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG}
            //"LFIV", "N/G", "err N/G", "Po", "So", "Pg", "Sg", "Plsg", "Slsg"}
            );
        }

        boolean[] canEdit = new boolean [] {
                //true, true, true, true, true, true, true, true, true, true, true, true, true,true
                true, true, true, true, true, true, true, true, true, true, true
        };

        public LayerTableModel(Object[][] data, Object[] columnNames) {
            super(data, columnNames);
        }

        public Class getColumnClass(int c) {
            return String.class;
        }
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            //FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
            //String type = (String)fixedTableModel.getValueAt(rowIndex, LAYER_TYPE_COLUMN);
            String rowLayerType = rowLayerTypes.get(rowIndex);

            if(!rowLayerType.equals(SAND_LAYER_TYPE) && columnIndex >= 6)
                return false;
            return canEdit[columnIndex];
            //return true;

        }

    }

    final private Color SHALE_COLOR = new Color(180, 180, 200);
    final private Color SAND_COLOR = new Color(255, 204, 85);
    class TableCellColorRenderer extends DefaultTableCellRenderer {
        public TableCellColorRenderer() {
            setOpaque(true); //MUST do this for background to show up.
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
            setEnabled(table == null || table.isEnabled());
            String rowLayerType = rowLayerTypes.get(row);
            if (rowLayerType.equals(SHALE_LAYER_TYPE)) setBackground(SHALE_COLOR);
            if (rowLayerType.equals(SAND_LAYER_TYPE)) setBackground(SAND_COLOR);
            setForeground(Color.BLACK);
            if(table != null)
            	super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            return this;
        }
    }

    class TableCellColorRenderer1 extends DefaultTableCellRenderer {
        public TableCellColorRenderer1() {
            setOpaque(true); //MUST do this for background to show up.
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
            FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
            String type = (String)fixedTableModel.getValueAt(row, LAYER_TYPE_COLUMN);
            if(!type.equals(SAND_LAYER_TYPE) && column >= 6)
                setBackground(Color.LIGHT_GRAY);
            else
                setBackground(Color.WHITE);

            setForeground(Color.BLACK);
            super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            return this;
        }
    }

    private static class TableCellColorRenderer2 extends DefaultTableCellRenderer {
        public TableCellColorRenderer2() {
            setOpaque(true); //MUST do this for background to show up.
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        	if(!table.getModel().isCellEditable(row, column))
                setBackground(Color.LIGHT_GRAY);
            else
                setBackground(Color.WHITE);

        	setForeground(Color.BLACK);
        	super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            return this;
        }
    }
    
    public ShowModelParams getShowModelParams(){
    	return showModelParams;
    }

    public void setShowModelParams(ShowModelParams params){
    	showModelParams = params;
    }
    
    public void restoreHorizonLineAttributes(Node node) {
    	if(node == null)
    		return;
    	NodeList children = node.getChildNodes();
    	HorizonLineTableModel hModel = (HorizonLineTableModel)layerTable.getModel();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
            	if(child.getNodeName().equals(LineAttributes.getXmlAlias())){
            		LineAttributes la = LineAttributes.restoreState(child);
            		hModel.setRowLineAttributes(la);
            	}
            }
        }
    }
    
    public void restoreRunParamState(Node node) {
        Element el = (Element)node;

        String hStr = el.getAttribute("height");
        int height = Integer.valueOf(hStr).intValue();
        String wStr = el.getAttribute("width");
        int width = Integer.valueOf(wStr).intValue();
        String X = el.getAttribute("x");
        if (X.trim().length() == 0)
            X = "0";    //default

        int x = Integer.valueOf(X).intValue();
        String Y = el.getAttribute("y");
        if (Y.trim().length() == 0)
            Y = "0";  //default

        int y = Integer.valueOf(Y).intValue();
        this.setSize(width,height);
        this.setLocation(x,y);
        String val = el.getAttribute("near_stack_dataset");
        if(val != null && val.trim().length() > 0){
            nearStackFilenameTextField.setText(val);
            String datasetPath = QiProjectDescUtils.getDatasetsPath(agent.getQiProjectDescriptor());
            String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
            String filePath = datasetPath + filesep + val + ".dat";
            agent.runGeoFileMetadataTask(this,filePath,DeliveryLiteConstants.GET_NEAR_SEISMIC_DATASET_CMD);
        }

        val = el.getAttribute("near_stack_iline");
        if(val != null && val.trim().length() > 0){
            nearInlineTextField.setText(val);
            try{
            nearRangeInfo[0][0] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                nearRangeInfo[0][0] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("near_stack_iline_to");
        if(val != null && val.trim().length() > 0){
            nearInlineToTextField.setText(val);
            try{
                nearRangeInfo[0][1] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                nearRangeInfo[0][1] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("near_stack_iline_by");
        if(val != null && val.trim().length() > 0){
            nearInlineByTextField.setText(val);
            try{
                nearRangeInfo[0][2] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                nearRangeInfo[0][2] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("near_stack_xline");
        if(val != null && val.trim().length() > 0){
            nearCrosslineTextField.setText(val);
            try{
                nearRangeInfo[1][0] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                nearRangeInfo[1][0] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("near_stack_xline_to");
        if(val != null && val.trim().length() > 0){
            nearCrosslineToTextField.setText(val);
            try{
                nearRangeInfo[1][1] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                nearRangeInfo[1][1] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("near_stack_xline_by");
        if(val != null && val.trim().length() > 0){
            nearCrosslineByTextField.setText(val);
            try{
                nearRangeInfo[1][2] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                nearRangeInfo[1][2] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("far_stack_iline");
        if(val != null && val.trim().length() > 0){
            farInlineTextField.setText(val);
            try{
                farRangeInfo[0][0] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                farRangeInfo[0][0] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("far_stack_iline_to");
        if(val != null && val.trim().length() > 0){
            farInlineToTextField.setText(val);
            try{
                farRangeInfo[0][1] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                farRangeInfo[0][1] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("far_stack_iline_by");
        if(val != null && val.trim().length() > 0){
            farInlineByTextField.setText(val);
            try{
                farRangeInfo[0][2] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                farRangeInfo[0][2] = Short.MIN_VALUE;
            }
        }
        val = el.getAttribute("far_stack_xline");
        if(val != null && val.trim().length() > 0){
            farCrosslineTextField.setText(val);
            try{
                farRangeInfo[1][0] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                farRangeInfo[1][0] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("far_stack_xline_to");
        if(val != null && val.trim().length() > 0){
            farCrosslineToTextField.setText(val);
            try{
                farRangeInfo[1][1] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                farRangeInfo[1][1] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("far_stack_xline_by");
        if(val != null && val.trim().length() > 0){
            farCrosslineByTextField.setText(val);
            try{
                farRangeInfo[1][2] = Double.parseDouble(val);
            }catch(NumberFormatException e){
                e.printStackTrace();
                farRangeInfo[1][2] = Short.MIN_VALUE;
            }
        }

        val = el.getAttribute("near_stack_wavelet_dataset");
        if(val != null && val.trim().length() > 0)
            nearStackWaveletFilenameTextField.setText(val);

        val = el.getAttribute("far_stack_dataset");
        if(val != null && val.trim().length() > 0)
            farStackFilenameTextField.setText(val);

        val = el.getAttribute("far_stack_wavelet_dataset");
        if(val != null && val.trim().length() > 0)
            farStackWaveletFilenameTextField.setText(val);

        val = el.getAttribute("oil_saturation");
        if(val != null && val.trim().length() > 0)
            fluidsTable2.getModel().setValueAt(val, 0, 6);
        val = el.getAttribute("high_gas_saturation");
        if(val != null && val.trim().length() > 0)
            fluidsTable2.getModel().setValueAt(val,0, 7);
        val = el.getAttribute("low_gas_saturation");
        if(val != null && val.trim().length() > 0)
            fluidsTable2.getModel().setValueAt(val,0, 8);

        val = el.getAttribute("sigma_oil_saturation");
        if(val != null && val.trim().length() > 0)
            fluidsTable2.getModel().setValueAt(val, 1, 6);
        val = el.getAttribute("sigma_high_gas_saturation");
        if(val != null && val.trim().length() > 0)
            fluidsTable2.getModel().setValueAt(val,1, 7);
        val = el.getAttribute("sigma_low_gas_saturation");
        if(val != null && val.trim().length() > 0)
            fluidsTable2.getModel().setValueAt(val,1, 8);

        val = el.getAttribute("numRuns");
        if(val != null && val.trim().length() > 0){
        	liteParams.setNumRuns(val);
            numberRunsTextField.setText(val);
            modelParams.setNumberRuns(Integer.valueOf(val).intValue());
        }
        val = el.getAttribute("tmin");
        if(val != null && val.trim().length() > 0){
            tmin4SyntheticSeismicTextField.setText(val);
        	liteParams.setTmin(val);
        	modelParams.setSyntheticSeismicStartTime(Float.valueOf(val).floatValue());
        }
        val = el.getAttribute("tmax");
        if(val != null && val.trim().length() > 0){
            tmax4SyntheticSeismicTextField.setText(val);
        	liteParams.setTmax(val);
        	modelParams.setSyntheticSeismicEndTime(Float.valueOf(val).floatValue());
        }

        val = el.getAttribute("tminSpaghettiPlot");
        if(val != null && val.trim().length() > 0){
            tmin4SpaghettiPlotTextField.setText(val);
            modelParams.setSpaghettiPlotStartTime(Float.valueOf(val).floatValue());
        }
        val = el.getAttribute("tmaxSpagettiPlot");
        if(val != null && val.trim().length() > 0){
            tmax4SpaghettiPlotTextField.setText(val);
            modelParams.setSpaghettiPlotEndTime(Float.valueOf(val).floatValue());
        }

        val = el.getAttribute("ep");
        if(val != null && val.trim().length() > 0){
            epTextField.setText(val);
        	liteParams.setEp(val);
        	modelParams.setSingleInline(Float.valueOf(val).floatValue());
        }

        val = el.getAttribute("cdp");
        if(val != null && val.trim().length() > 0){
            cdpTextField.setText(val);
        	liteParams.setCdp(val);
        	modelParams.setSingleXline(Float.valueOf(val).floatValue());
        }

        val = el.getAttribute("priorReal");
        if(val != null && val.trim().length() > 0){
        	outputBaseNameTextField.setText(val);
        	liteParams.setOutputBaseName(val);
        }

        val = el.getAttribute("postReal");
        if(val != null && val.trim().length() > 0){
        	outputBaseNameTextField.setText(val);
        	liteParams.setOutputBaseName(val);
        }
        
        val = el.getAttribute("outputFileBaseName");
        if(val != null && val.trim().length() > 0){
        	outputBaseNameTextField.setText(val);
        	liteParams.setOutputBaseName(val);
        }
        Node horizonsNode = agent.getXmlNodeByNodeName(node,"horizons");
        restoreHorizonLineAttributes(horizonsNode);
        ((DefaultTableModel)layerTable.getModel()).fireTableDataChanged();
    }

    public void restoreJobInfo(Node node){
    	Element el = (Element)node;
    	String jobStatus = el.getAttribute("jobStatus");
    	if(jobStatus == null || jobStatus.trim().length() == 0)
    		return;
    	jobStatus = jobStatus.trim();
    	String scriptPrefix = el.getAttribute("scriptNamePrefix");
    	if(scriptPrefix == null || scriptPrefix.trim().length() == 0)
    		return;
    	scriptPrefix = scriptPrefix.trim();
    	QiProjectDescriptor qiProjectDesc = agent.getQiProjectDescriptor();
        String scriptDir = QiProjectDescUtils.getTempPath(qiProjectDesc);
        String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        String filePrefix = scriptDir + filesep + scriptPrefix;
        // Create a job monitor for the script.
        monitor = new JobMonitor(filePrefix, scriptDir, scriptPrefix);
        // Create a job manager for the script.
        manager = new JobManager(agent.getMessagingMgr(), this);
        List<Component> components = getListOfRunActionComponents();
        components.add(displayResultsButton);
        components.add(displayResultsMenuItem);
        List<Component> cancelComps = new ArrayList<Component>();
        cancelComps.add(cancelRunButton);
        cancelComps.add(cancelRunMenuItem);
        List<Component> checkStatusButtons = new ArrayList<Component>();
        checkStatusButtons.add(checkStatusButton);
        checkStatusButtons.add(statusMenuItem);
        
    	if(jobStatus.equals(JobMonitor.JOB_RUNNING_STATUS)){
   	        for(Component comp : components){
   				if(comp != null)
   					comp.setEnabled(false);
   			}
    			
   			for(Component comp : cancelComps){
   				if(comp != null)
   					comp.setEnabled(true);
   			}
   			//monitor.setJobStatusToRunning();
   			//stateLabel.setText(jobStatus);
   			//startJobStatusWorker(components,cancelComps,stdoutTextArea,stderrTextArea);
    	}else if(jobStatus.equals(JobMonitor.JOB_ENDED_ABNORMALLY_STATUS) || jobStatus.equals(JobMonitor.JOB_ENDED_NORMALLY_STATUS)){
    		for(Component comp : components)
    			comp.setEnabled(true);
    	}
        monitor.setJobStatus(jobStatus);
    		//checkStatusButton.setEnabled(true);
    		//stateLabel.setText(jobStatus);
    		//updateStatus(monitor,manager,components,cancelComps,stdoutTextArea,stderrTextArea);
    	//}
    	stateLabel.setText(jobStatus);
    	for(Component comp : checkStatusButtons){
			if(comp != null)
				comp.setEnabled(true);
		}
    	
    }
    /**
     * Rename the plugin
     *
     * @param name from plugin message processor
     */
    public void renamePlugin(String name) {
        QiProjectDescriptor qpDesc = agent.getQiProjectDescriptor();
        resetTitle(QiProjectDescUtils.getQiProjectName(qpDesc));
    }

    /**
     * Close GUI and dispose, called by plugin when user has terminated GUI
     *
     */
    public void closeGUI() {
    	if(showModelDialog != null && showModelDialog.isVisible()){
    		showModelDialog.dispose();
    		showModelDialog.setVisible(false);
    	}
        setVisible(false);
        dispose();
    }

    /**
     * Save the state information for DeliveryLite GUI.
     *
     * @return State as an XML string.
     */
    public String genState() {
        //validateLiteParams();
        StringBuffer content = new StringBuffer();
        content.append("<" + this.getClass().getName() + " ");
        content.append("project_name=\"" + agent.getQiProjectDescriptor().getQiProjectName() + "\" ");
        content.append("project_root=\"" + agent.getQiProjectDescriptor().getQiSpace() + "\" ");
        content.append("height=\"" + this.getHeight() + "\" ");
        content.append("width=\"" + this.getWidth() + "\" ");
        content.append("x=\"" + this.getX() + "\" ");
        content.append("y=\"" + this.getY() + "\" ");
        content.append("outputFileBaseName=\"" + liteParams.getOutputBaseName() + "\" ");
        //content.append("priorReal=\"" + liteParams.getPriorReal() + "\" ");
        //content.append("postReal=\""  + liteParams.getPostReal()  + "\" ");
        //content.append("tmin=\"" + liteParams.getTmin() + "\" ");
        //content.append("tmax=\""  + liteParams.getTmax()  + "\" ");
        content.append("tmin=\"" + tmin4SyntheticSeismicTextField.getText().trim() + "\" ");
        content.append("tmax=\""  + tmax4SyntheticSeismicTextField.getText().trim()  + "\" ");
        content.append("tminSpaghettiPlot=\"" + tmin4SpaghettiPlotTextField.getText().trim() + "\" ");
        content.append("tmaxSpagettiPlot=\""  + tmax4SpaghettiPlotTextField.getText().trim()  + "\" ");

        //content.append("ep=\"" + liteParams.getEp() + "\" ");
        //content.append("cdp=\""  + liteParams.getCdp()  + "\" ");
        content.append("ep=\"" + epTextField.getText().trim() + "\" ");
        content.append("cdp=\""  + cdpTextField.getText().trim()  + "\" ");
        //content.append("numRuns=\""  + liteParams.getNumRuns()  + "\" ");
        content.append("numRuns=\""  + numberRunsTextField.getText().trim()  + "\" ");
        content.append("near=\""  + (nearOnlyRadioButton.isSelected() ? "true" : "false")  + "\" ");
        content.append("modelName=\""  + modelNameTextField.getText().trim() + "\" ");
        content.append("model=\""  + agent.getModelFilename()  + "\" ");
        
        String val = nearStackFilenameTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_dataset=\""  + val  + "\" ");
        val = nearInlineTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_iline=\""  + val  + "\" ");
        val = nearInlineToTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_iline_to=\""  + nearInlineToTextField.getText().trim()  + "\" ");
        val = nearInlineByTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_iline_by=\""  + nearInlineByTextField.getText().trim()  + "\" ");
        val = nearCrosslineTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_xline=\""  + nearCrosslineTextField.getText().trim()  + "\" ");
        val = nearCrosslineToTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_xline_to=\""  + nearCrosslineToTextField.getText().trim()  + "\" ");
        val = nearCrosslineByTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_xline_by=\""  + nearCrosslineByTextField.getText().trim()  + "\" ");
        val = farInlineTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_iline=\""  + farInlineTextField.getText().trim()  + " \" ");
        val = farInlineToTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_iline_to=\""  + farInlineToTextField.getText().trim()  + "\" ");
        val = farInlineByTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_iline_by=\""  + farInlineByTextField.getText().trim()  + "\" ");
        val = farCrosslineTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_xline=\""  + farCrosslineTextField.getText().trim()  + "\" ");
        val = farCrosslineToTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_xline_to=\""  + farCrosslineToTextField.getText().trim()  + "\" ");
        val = farCrosslineByTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_xline_by=\""  + farCrosslineByTextField.getText().trim()  + "\" ");
        val = farStackFilenameTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_dataset=\""  + farStackFilenameTextField.getText().trim()  + "\" ");
        val = nearStackWaveletFilenameTextField.getText().trim();
        if(val.length() > 0)
            content.append("near_stack_wavelet_dataset=\""  + nearStackWaveletFilenameTextField.getText().trim()  + "\" ");
        val = farStackWaveletFilenameTextField.getText().trim();
        if(val.length() > 0)
            content.append("far_stack_wavelet_dataset=\""  + farStackWaveletFilenameTextField.getText().trim()  + "\" ");

        val = getOilSaturationFromFluidsTable();
        if(val != null && val.trim().length() > 0)
            content.append("oil_saturation=\""  + val.trim() + "\" ");

        val = getHighGasSaturationFromFluidsTable();
        if(val != null && val.trim().length() > 0)
            content.append("high_gas_saturation=\""  + val.trim() + "\" ");

        val = getLowGasSaturationFromFluidsTable();
        if(val != null && val.trim().length() > 0)
            content.append("low_gas_saturation=\""  + val.trim() + "\" ");

        val = getSigmaOilSaturationFromFluidsTable();
        if(val != null && val.trim().length() > 0)
            content.append("sigma_oil_saturation=\""  + val.trim() + "\" ");
        val = getSigmaHighGasSaturationFromFluidsTable();
        if(val != null && val.trim().length() > 0)
            content.append("sigma_high_gas_saturation=\""  + val.trim() + "\" ");

        val = getSigmaLowGasSaturationFromFluidsTable();
        if(val != null && val.trim().length() > 0)
            content.append("sigma_low_gas_saturation=\""  + val.trim() + "\" ");

        //content.append("/> ");
        content.append("> \n");
        if(showModelParams != null){
        	TableModel model = getLayerTable().getModel();
        	int count = model.getColumnCount();
        	List<String> listOfPropertyNames = new ArrayList<String>();
        	for(int i = 0; i < count; i++){
                String name = model.getColumnName(i);
                listOfPropertyNames.add(name);
        	}
        	showModelParams.setListOfProperties(listOfPropertyNames);
        	showModelParams.setModelBeforeColorbarMap(modelBeforePropertyColorMap);
        	showModelParams.setModelAfterDatasetPropertyColorMap(modelAfterDatasetPropertyColorMap);
        	content.append(showModelParams.toXML());
        }
        String lastStateFile = agent.getModelDir() + "last_lite_model.xml";
        //createLayers();
        //updateFluidsPropertiesInfo();
        updateInversionObject();
        toXmlInversionFile(lastStateFile);

        //Include XML model in the state because 1) The XML file may not exist
        //when state is restored 2) THe XML model may have been modified after
        //being read in; 4) The XML file may have been modified after state is
        //saved.
        content.append("\n<model_xml>");
        //content.append("\n<model_xml>" + "\n");
       /*
        try {
            ArrayList<String> lines = readXmlFile(lastStateFile);
            //skip first line <?xml>; otherwise, the State Manager will
            //take an xcerces exception.
            //NOTE: The first line will be reinstated when the XML model
            //      is restored.
            int startIdx = 0;
            if (lines.get(0).startsWith("<?xml")) startIdx = 1;;
            for (int i=startIdx; i<lines.size(); i++) {
                content.append(lines.get(i) + "\n");
            }
        } catch (QiwIOException qioe) {
            //TODO notify user cannot read XML model when saving state
            logger.finest("Cannot read model's XML saved to a file and insert into state.\n"+qioe.getMessage());
            logger.info("Cannot read model's XML saved to a file and insert into state.\n"+qioe.getMessage());
        }
        */
        String xml = "";
        Document doc = inversionToDocument();
        if(doc != null){
            xml = Util.toFormatedXMlString(doc);
            xml = xml.substring(xml.indexOf("?>")+2);
        }
        content.append(xml);
        content.append("</model_xml>" + "\n");
        HorizonLineTableModel lineModel = (HorizonLineTableModel)layerTable.getModel();
        String lineState = lineModel.genHorizonLineOnlyXMLStates();
        if(lineState != null && lineState.length() > 0){
        	content.append("<horizons>" + "\n");
        	content.append(lineState);
        	content.append("</horizons>" + "\n");
        }
        //save the job status information if there is one
        if(monitor != null){
        	content.append("<job_info scriptNamePrefix=\"" + monitor.getScriptPrefix() + "\"\n");
        	content.append(" jobStatus=\"" + monitor.getJobStatus1() + "\"/>\n");
        }
        content.append("</" + this.getClass().getName() + ">\n");
        return content.toString();
    }

    
    /**
     * Read a XML file.
     *
     * @param filePath Path of the XML file.
     * @return List of each line in the file. A line does not contain any
     * line-termination characters. List is empty if file is empty.
     * @throws QiwIOException If the file path is null or empty, the path does
     * not exist, the path is not a file or there is an IO exception while
     * reading the file.
     */
    private ArrayList<String> readXmlFile(String filePath) throws QiwIOException {
        if (filePath == null || filePath.length() == 0) throw new QiwIOException("invalid file path="+filePath);

        File f = new File(filePath);

        if (!f.exists()) throw new QiwIOException("specified file does not exist; path="+filePath);

        if (!f.isFile()) throw new QiwIOException("specified file is not a file; path="+filePath);

        BufferedReader br = null;
        ArrayList<String> fileLines = new ArrayList<String>();
        try {
            FileInputStream fis = new FileInputStream(f);
            br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            // read file line by line
            while (line != null) {
                fileLines.add(line);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            throw new QiwIOException("IO exception reading file:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {}
        }

        return fileLines;
    }


    private ViewModelDialog showModelDialog;
    private void showModelActionPerformed(){
    	
    	
    	
        stopTableCellEditing();
        if(showModelDialog == null || !showModelDialog.isVisible()){
            showModelDialog = new ViewModelDialog(this,false);
            //showModelDialog.setLocationRelativeTo(JOptionPane.getFrameForComponent(this));
            showModelDialog.setVisible(true);

            javax.swing.JDesktopPane desktop = agent.getMessagingMgr().getWorkbenchDesktopPane();
            desktop.add(showModelDialog);
            //showModelDialog.setSize(1080, 700);
        }else{
            //showModelDialog.populatePropertyColorMap();
            showModelDialog.getModelBeforeInternalFrame().refreshModelCanvas(showModelParams.getVerticalScale());
            showModelDialog.getModelBeforeInternalFrame().getScrollPane().getVerticalScrollBar().setValue(showModelParams.getVerticalScrollPosition());
        }
        showModelDialog.setListOfRunComponentsEnabled(runPriorInversionButton.isEnabled());
        try{
        showModelDialog.setSelected(true);
        }catch(PropertyVetoException e){
            e.printStackTrace();
        }
        if(showModelDialog.isVisible()){
        showModelDialog.restoreShowModel(showModelParams);
        Rectangle aRect = showModelParams.getScrollToVisible();
        
        showModelDialog.getModelBeforeInternalFrame().getModelCanvas().scrollRectToVisible(aRect);
        
        }
        
    }


    public SeismicFileMetadata getNearMetadata(){
        String name = getNearStackFileName();
        if(name != null && name.length() > 0 && metadataMap.containsKey(name)){
            return (SeismicFileMetadata)metadataMap.get(name);
        }
        return null;
    }

    public synchronized void setMetaData(String dataset, GeoFileMetadata metadata, int stackMode, int runMode){
    	metadataMap.put(dataset, metadata);
    	if(metadata instanceof SeismicFileMetadata){
	    	SeismicFileMetadata metadata1 = (SeismicFileMetadata)metadata;
	    	if(stackMode == DeliveryLitePlugin.NEAR && runMode == DeliveryLitePlugin.REAL)
	    		setNearStackRange(metadata1);
	    	else if(stackMode == DeliveryLitePlugin.FAR && runMode == DeliveryLitePlugin.REAL)
	    		setFarStackRange(metadata1);
    	}
    }

    private Map<String,ModelDataset> modelMap = new HashMap<String,ModelDataset>();
    public synchronized void setMetaData(String dataset, GeoFileMetadata metadata){
    	metadataMap.put(dataset, metadata);
    }
    
    public synchronized void setTraceData(String dataset, IDataObject[] data){
    	dataMap.put(dataset, data);
    }
    
	/*public static String getModelDatasetPrefix(String base, boolean nearAndFar, String datasetType){
		String modelAfterDatasetBase = base + "_post_";
	
		if(!nearAndFar){
			modelAfterDatasetBase += "near_" + datasetType + "_model";
		}else{
			modelAfterDatasetBase += "near_far_" + datasetType + "_model";
		}
		return modelAfterDatasetBase;
	}
    */
    private void restoreShowModelTraceData(ShowModelParams params){
    	List<DatasetInfo> list = new ArrayList<DatasetInfo>();
    	int stackMode = DeliveryLitePlugin.NEAR;
    	if(params.isNearAndFar())
    		stackMode = DeliveryLitePlugin.NEAR_FAR;
    	
    	String s = params.getSelectedModelAfterDataset();
    	IDataObject[] dos = null;
		DatasetInfo di = null;
    	if(s != null && s.length() > 0){
            DatasetNameHelper helper = new DatasetNameHelper(getOutputBasename(), s);
    		String modelAfterDatasetBase = helper.getModelDatasetPrefix(params.isNearAndFar());
    		String modelAfterDatasetBaseStddev = helper.getStddevModelDatasetPrefix(params.isNearAndFar());
    		if((dos = getDataObjectsByDataset(modelAfterDatasetBase)) == null){
    			di = new DatasetInfo(modelAfterDatasetBase,stackMode,DeliveryLitePlugin.MODEL_AFTER);
    			list.add(di);
    		}
    		if(!modelAfterDatasetBase.equals(modelAfterDatasetBaseStddev)){
    			if((dos = getDataObjectsByDataset(modelAfterDatasetBaseStddev)) == null){
    				di = new DatasetInfo(modelAfterDatasetBaseStddev,stackMode,DeliveryLitePlugin.MODEL_AFTER);
    				list.add(di);
    			}
    		}
    	}
		
    	if(params.isShowRealStackEnabled()){
    		String dataset = getNearStackFileName(); 
    		if((dos = getDataObjectsByDataset(dataset)) == null){
    			stackMode = DeliveryLitePlugin.NEAR;
    			di = new DatasetInfo(dataset,stackMode,DeliveryLitePlugin.REAL);
    			list.add(di);
    		}
    		if(params.isNearAndFar()){
    			dataset = getFarStackFileName();	
    			stackMode = DeliveryLitePlugin.FAR;
    			if((dos = getDataObjectsByDataset(dataset)) == null){
    				di = new DatasetInfo(dataset,stackMode,DeliveryLitePlugin.REAL);
    				list.add(di);
    			}
    		}
		}

    	if(params.isShowSyntheticBeforeEnabled()){
    		String dataset = getNearSyntheticBeforeDatasetName();
    		stackMode = DeliveryLitePlugin.NEAR;
    		if((dos = getDataObjectsByDataset(dataset)) == null){
    			di = new DatasetInfo(dataset,stackMode,DeliveryLitePlugin.SYNTHETIC_BEFORE);
    			list.add(di);
    		}
    		if(params.isNearAndFar()){
    			dataset = getFarSyntheticBeforeDatasetName();
    			if((dos = getDataObjectsByDataset(dataset)) == null){
    				stackMode = DeliveryLitePlugin.FAR;
    				di = new DatasetInfo(dataset,stackMode,DeliveryLitePlugin.SYNTHETIC_BEFORE);
    				list.add(di);
    			}
    		}
		}

    	if(params.isShowSyntheticAfterEnabled()){
    		stackMode = DeliveryLitePlugin.NEAR;
    		String dataset = getNearSyntheticAfterDatasetName();
    		if((dos = getDataObjectsByDataset(dataset)) == null){
    			di = new DatasetInfo(dataset,stackMode,DeliveryLitePlugin.SYNTHETIC_AFTER);
    			list.add(di);
    		}
    		if(params.isNearAndFar()){
    			stackMode = DeliveryLitePlugin.FAR;
    			dataset = getFarSyntheticAfterDatasetName();
    			if((dos = getDataObjectsByDataset(dataset)) == null){
    				di = new DatasetInfo(dataset,stackMode,DeliveryLitePlugin.SYNTHETIC_AFTER);
    				list.add(di);
    			}
    		}
		}
    	
    	if(list.size() == 0){
    		showModelActionPerformed();
    		return;
    	}
    	final List<DatasetInfo> list1 = list;
    	final Component comp = this;
    	Runnable runnable = new Runnable(){
    		public void run(){
		    	String title = "Gathering Trace Data";
		    	String text = "Please wait while system is gathering trace data from ....";
				Icon icon = IconResource.getInstance().getImageIcon(IconResource.LOGO_ICON_QI);
				com.bhpb.qiworkbench.compAPI.ProgressMonitor monitor = ProgressUtil.createModalProgressMonitor(comp,100,false,500,title,text,icon);
				monitor.start("");
				monitor.setCurrent("Start gathering trace information ..",0);
		        monitor.setIndeterminate(false);
		    	for(int i = 0; i < list1.size(); i++){
		    		DatasetInfo data = agent.getSeismicTraceByDataInfo(list1.get(i));
					setMetaData(data.name,data.getMetadata());
					setTraceData(data.name,data.getDataObjects());
					if(data.datasetType == DeliveryLitePlugin.MODEL_AFTER){
						float[] fs = data.getDataObjects()[0].getTrace().getFloatVector();
		        		ModelDataset md = new ModelDataset((ModelFileMetadata)data.getMetadata(),fs);
		        		modelMap.put(data.name, md);
					}
					float f = (float)(i+1)/(float)list1.size();
					monitor.setCurrent("Restoring dataset " + data.name,(int)(f*100));
				}
		    	Runnable updateAComponent = new Runnable() {
                    public void run() {
                    	showModelActionPerformed();
                    }
                };
                SwingUtilities.invokeLater(updateAComponent);
	    	}
    	};
    	Thread worker = new Thread(runnable);
    	worker.start();
    	return;
    }
    
    public synchronized void setTraceData(String dataset, IDataObject[] data, int stackMode, int runMode){
        if(showModelDialog != null){
        	float[] fs = data[0].getTrace().getFloatVector();
        	if(runMode == DeliveryLitePlugin.SYNTHETIC_BEFORE || runMode == DeliveryLitePlugin.SYNTHETIC_AFTER || runMode == DeliveryLitePlugin.REAL){
        		if(stackMode == DeliveryLitePlugin.NEAR)
        			showModelDialog.getNearInternalFrame().getStackPanel().setTraceData(dataset,fs,stackMode,runMode);
        		else if(stackMode == DeliveryLitePlugin.FAR)
        			showModelDialog.getFarInternalFrame().getStackPanel().setTraceData(dataset,fs,stackMode,runMode);
        	}else if(runMode == DeliveryLitePlugin.MODEL_AFTER){
        		ModelDataset md = new ModelDataset((ModelFileMetadata)metadataMap.get(dataset),fs);
        		modelMap.put(dataset, md);
        		showModelDialog.fillLayerProperty(md.getMetadata());
        	}else if(runMode == DeliveryLitePlugin.MODEL_AFTER_STDDEV){
        		ModelDataset md = new ModelDataset((ModelFileMetadata)metadataMap.get(dataset),fs);
        		modelMap.put(dataset, md);
        	}
        }
        dataMap.put(dataset, data);
    }
    
    public synchronized ModelDataset getModelDatasetByName(String key){
    	if(modelMap.containsKey(key)){
    		return modelMap.get(key);
    	}
    	return null;
    }
    
    
    public ViewModelDialog getShowModelDialog(){
        return showModelDialog;
    }

    public JTable getLayerTable(){
        return layerTable;
    }
    /*
     * Get the data entered in the layer model
     *
     * @return List of data in each row
     */
    public Vector<Vector> getLayerData() {
        Vector<Vector> mergedData = new Vector<Vector>();

        //Get the Vector of Vectors  that contains the data values of the non-scrollable part of the layer table
        Vector fixedDataVector = ((DefaultTableModel)fixedLayerTable.getModel()).getDataVector();

        //Get the Vector of Vectors  that contains the data values of the scrollable part of the layer table
        Vector dataVector = ((DefaultTableModel)layerTable.getModel()).getDataVector();

        //Merge the two data vectors into one. They are the same size, i.e., have the same number of rows.
        for (int i=0; i<dataVector.size(); i++) {
            Vector leftPart = (Vector)fixedDataVector.get(i);
            Vector rightPart = (Vector)dataVector.get(i);
            Vector row = new Vector();
            //append the left part of the row to the end of the new row Vector
            row.addAll(leftPart);
            //append the right part of the row to the end of the new row Vector
            row.addAll(rightPart);
            mergedData.add(row);
        }

        return mergedData;
    }

    /**
     * Reset the GUI's title
     *
     * @param name Name of the associated project.
     */
    public void resetTitle(String projName) {
        if (projName == null) {
            projName = agent.getQiProjectDescriptor().getQiProjectName();
        }
        String compName = "";
        IComponentDescriptor agentDesc = agent.getComponentDescriptor();
        String pdn = CompDescUtils.getDescPreferredDisplayName(agentDesc);
        String screenName = CompDescUtils.getDescDisplayName(agentDesc);
        if (pdn.trim().length() > 0)
            compName = pdn.trim();
        else
            compName = screenName;

        infoPane.resetParams();
        String modelFile = agent.getModelXML();
        if (modelFile == null) {
            modelFile = DeliveryLitePlugin.DEFAULT;
        }

        //get the type of qiComponent
        int idx = screenName.indexOf("#");
        if (idx != -1) screenName = screenName.substring(0, idx);
        this.setTitle(screenName+": " + compName + "  Project: " + projName + " Model: " + modelFile);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DeliveryLiteUI(null,null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton addLayer2Button;
    private javax.swing.JButton addLayerButton;
    private javax.swing.JLabel avoALabel;
    private javax.swing.JTextField avoATextField;
    private javax.swing.JLabel avoBLabel;
    private javax.swing.JTextField avoBTextField;
    private javax.swing.JTextArea boundingShaleFormulaeTextArea;
    private javax.swing.JPanel boundingShalePanel;
    private javax.swing.JScrollPane boundingShaleScrollPane;
    private javax.swing.JTable boundingShaleTable;
    private javax.swing.JButton cancelRunButton;
    private javax.swing.JMenuItem cancelRunMenuItem;
    private javax.swing.JLabel cdpLabel;

    private javax.swing.JTextField cdpTextField;

    private javax.swing.JButton checkStatusButton;
    private javax.swing.JPanel createModelPanel;
    private javax.swing.JLabel datasetsRelDirLabel1;
    private javax.swing.JLabel datasetsRelDirLabel2;
    private javax.swing.JButton deleteLayer1Button;
    private javax.swing.JButton deleteLayer2Button;
    private javax.swing.JMenuBar deliveryLiteMenuBar;
    private javax.swing.JTabbedPane deliveryLiteTabbedPane;
    private javax.swing.JLabel depthOfMasterLayerLabel;
    private javax.swing.JTextField depthOfMasterLayerTextField;
    private javax.swing.JPanel detailsPanel;
    private javax.swing.JSplitPane detailsSplitPane;
//    private javax.swing.JMenu editMenu;
    private javax.swing.JButton editModelButton;
    private javax.swing.JButton entireVolumeButton;
    private javax.swing.JLabel epLabel;

    private javax.swing.JTextField epTextField;

    private javax.swing.JLabel errorOfMasterLayerDepthLabel;
    private javax.swing.JTextField errorOfMasterLayerDepthTextField;
    private javax.swing.JMenu executeMenu;
    private javax.swing.JButton exportFluidPropertiesButton;
    private javax.swing.JButton exportMatrixPropertiesButton;
    private javax.swing.JButton exportStackPropertiesButton;

    private javax.swing.JLabel farByLabel1;
    private javax.swing.JLabel farByLabel2;
    private javax.swing.JTextField farCrosslineByTextField;
    private javax.swing.JLabel farCrosslineLabel;
    private javax.swing.JTextField farCrosslineTextField;
    private javax.swing.JTextField farCrosslineToTextField;
    private javax.swing.JTextField farInlineByTextField;
    private javax.swing.JLabel farInlineLabel;
    private javax.swing.JTextField farInlineTextField;
    private javax.swing.JTextField farInlineToTextField;
    private javax.swing.JLabel farLabel;
    private javax.swing.JButton farStackFilenameBrowseButton;
    private javax.swing.JLabel farStackFilenameLabel;
    private javax.swing.JTextField farStackFilenameTextField;
    private javax.swing.JPanel farStackRangeInfoPanel;
    private javax.swing.JButton farStackWaveletFIlenameBrowseButton;
    private javax.swing.JLabel farStackWaveletFilenameLabel;
    private javax.swing.JTextField farStackWaveletFilenameTextField;
    private javax.swing.JLabel farToLabel1;
    private javax.swing.JLabel farToLabel2;
    private javax.swing.JLabel farWaveletSNLabel;
    private javax.swing.JTextField farNoiseRMSTextField;

    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel fileSummaryPanel;
    private javax.swing.JScrollPane fileSummaryScrollPane;
    private javax.swing.JTextArea fileSummaryTextArea;
    private javax.swing.JScrollPane fixedLayerScrollPane;
    private javax.swing.JTable fixedLayerTable;
    private javax.swing.JLabel fluidPropertiesFileLabel;
    private javax.swing.JPanel fluidPropertiesPanel;
    private javax.swing.JLabel fluidsErrorLabel;
    private javax.swing.JPanel fluidsPanel;
    private javax.swing.JScrollPane fluidsScrollPane1;
    private javax.swing.JScrollPane fluidsScrollPane2;
    private javax.swing.JTable fluidsTable1;
    private javax.swing.JTable fluidsTable2;
    private javax.swing.JLabel fluidsValueLabel;
    private javax.swing.JPanel grainPanel;
    private javax.swing.JScrollPane grainScrollPane;
    private javax.swing.JTable grainTable;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JButton importFluidPropertiesButton;
    private javax.swing.JButton importMatrixPropertiesButton;
    private javax.swing.JButton importStackPropertiesButton;
    private javax.swing.JLabel importedFluidPropertiesFileLabel;
    private javax.swing.JLabel importedMatrixPropertiesFileLabel;
    private javax.swing.JLabel importedStackPropertiesFileLabel;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JTabbedPane inputsTabbedPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel landmarkInPanel;
    private javax.swing.JLabel landmarkProjectLabel;
    private javax.swing.JLabel landmarkRangeLabel;
    private javax.swing.JScrollPane layerScrollPane;
    private javax.swing.JTable layerTable;
    private javax.swing.JLabel linesByLabel;
    private javax.swing.JTextField linesByTextField;
    private javax.swing.JTextField linesTextField;
    private javax.swing.JLabel linesToLabel;
    private javax.swing.JTextField linesToTextField;
    private javax.swing.JButton listProjectsButton;
    private javax.swing.JButton listVolumesButton;
    private javax.swing.JLabel masterDepthLayerLabel;
    private javax.swing.JTextField masterDepthLayerTextField;
    private javax.swing.JLabel matrixPropertiesFileLabel;
    private javax.swing.JPanel matrixPropertiesPanel;
    private javax.swing.JTextArea mixingShaleFormulaeTextArea;
    private javax.swing.JPanel mixingShalePanel;
    private javax.swing.JScrollPane mixingShaleScrollPane;
    private javax.swing.JTable mixingShaleTable;
    private javax.swing.JLabel modelNameLabel;
    private javax.swing.JTextField modelNameTextField;
    private javax.swing.JPanel modelPanel;
    private javax.swing.JPanel modelParametersPanel;
    private javax.swing.JLabel modelRelDirLabel1;
    private javax.swing.JLabel modelRelDirLabel2;
    private javax.swing.JRadioButton nearAndFarRadioButton;

    private javax.swing.JLabel nearByLabel1;
    private javax.swing.JLabel nearByLabel2;
    private javax.swing.JTextField nearCrosslineByTextField;
    private javax.swing.JLabel nearCrosslineLabel;
    private javax.swing.JTextField nearCrosslineTextField;
    private javax.swing.JTextField nearCrosslineToTextField;
    private javax.swing.JTextField nearInlineByTextField;
    private javax.swing.JLabel nearInlineLabel;
    private javax.swing.JTextField nearInlineTextField;
    private javax.swing.JTextField nearInlineToTextField;
    private javax.swing.JButton nearStackFilenameBrowseButton;
    private javax.swing.JPanel nearStackRangeInfoPanel;
    private javax.swing.JButton nearStackWaveletFIlenameBrowseButton;
    private javax.swing.JLabel nearToLabel1;
    private javax.swing.JLabel nearToLabel2;

    javax.swing.JLabel secLabel = new javax.swing.JLabel("sec");
    javax.swing.JLabel secLabel1 = new javax.swing.JLabel("sec");

    private javax.swing.JLabel nearLabel;
    private javax.swing.JRadioButton nearOnlyRadioButton;
    private javax.swing.JLabel nearStackFilenameLabel;
    private javax.swing.JTextField nearStackFilenameTextField;
    private javax.swing.JTextField nearStackWaveletFilenameTextField;
    private javax.swing.JLabel nearStackWaveletFilenameLabel;
    private javax.swing.JLabel nearWaveletSNLabel;
    private javax.swing.JTextField nearNoiseRMSTextField;
    private javax.swing.JComboBox numLayersComboBox;
    private javax.swing.JLabel numberLayersLabel;
    private javax.swing.JLabel numberRunsLabel;
    private javax.swing.JTextField numberRunsTextField;
    private javax.swing.JPanel outputPanel;
    private javax.swing.JPanel outputParametersPanel;
    //private javax.swing.JTextField outputPostRealisationsFIlenameTextField;
    //private javax.swing.JLabel outputPostRealisationsFilenameLabel;
    private javax.swing.JLabel outputPriorRealisationsFilenameLabel;
    //private javax.swing.JTextField outputPriorRealisationsFilenameTextField;
    private javax.swing.JTextField outputBaseNameTextField;
    private javax.swing.JLabel outputRelDirLabel1;
    private javax.swing.JLabel outputRelDirLabel2;
    private javax.swing.JPanel parametersPanel;
    private javax.swing.JMenuItem prefsMenuItem;
    private javax.swing.JLabel projectInfoLabel;
//    private javax.swing.JMenuItem projectMetadata;
    private javax.swing.JPanel projectMetadataPanel;
    private javax.swing.JLabel projectNameLabel1;
    private javax.swing.JLabel projectNameLabel2;
    private javax.swing.JPanel projectPanel;
    private javax.swing.JLabel projectRelDirLabel1;
    private javax.swing.JLabel projectRelDirLabel2;
    private javax.swing.JTextField projectTextField;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JLabel rangeLinesLabel;
//    private javax.swing.JLabel refFluidErrorLabel;
    private javax.swing.JLabel refFluidValueLabel;
    private javax.swing.JPanel refSandFormulaePane;
    private javax.swing.JTextArea refSandFormulaeTextArea;
    private javax.swing.JPanel referenceFluidPanel;
    private javax.swing.JScrollPane referenceFluidScrollPane;
    private javax.swing.JTable referenceFluidTable;
    private javax.swing.JPanel referenceSandPanel;
    private javax.swing.JScrollPane referenceSandScrollPane;
    private javax.swing.JTable referenceSandTable;
    private javax.swing.JMenuItem runMenuItem;
    private javax.swing.JMenuItem runPriorMenuItem;
    private javax.swing.JMenuItem runPostInverstionMenuItem;
    private javax.swing.JPanel runPanel;
    private javax.swing.JPanel runParametersPanel;
    private javax.swing.JButton runScriptsButton;
    private javax.swing.JButton runPostInversionButton;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveQuitMenuItem;
    private javax.swing.JLabel seismicRelDirLabel1;
    private javax.swing.JLabel seismicRelDirLabel2;
    private javax.swing.JButton selectButton;
    private javax.swing.JButton showModelButton;
    private javax.swing.JLabel sigmaTimeBaseLabel;
    private javax.swing.JTextField sigmaTimeBaseTextField;
    private javax.swing.JPanel singleTracePanel;
    private javax.swing.JPanel stackParametersPanel;
    private javax.swing.JScrollPane stackParametersScrollPane;
    private javax.swing.JTable stackParametersTable;
    private javax.swing.JLabel stackPropertiesFileLabel;
    private javax.swing.JPanel stackPropertiesPanel;
    private javax.swing.JLabel stacksLabel;
    private javax.swing.JLabel stateLabel;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JMenuItem statusMenuItem;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JScrollPane stderrScrollPane;
    private javax.swing.JTextArea stderrTextArea;
    private javax.swing.JScrollPane stdoutScrollPane;
    private javax.swing.JTextArea stdoutTextArea;
    private javax.swing.JLabel timeBaseLabel;
    private javax.swing.JTextField timeBaseTextField;
    private javax.swing.JLabel tmaxLabel;
    private javax.swing.JLabel tmaxLabel1;
    private javax.swing.JTextField tmax4SyntheticSeismicTextField;
    private javax.swing.JLabel tminLabel;
    private javax.swing.JLabel tminLabel1;
    private javax.swing.JLabel syntheticSeismicLabel;
    private javax.swing.JLabel spagettiPlotLabel;
    private javax.swing.JTextField tmin4SyntheticSeismicTextField;
    private javax.swing.JLabel tracesByLabel;
    private javax.swing.JTextField tracesByTextField;
    private javax.swing.JTextField tmin4SpaghettiPlotTextField;
    private javax.swing.JTextField tmax4SpaghettiPlotTextField;
    private javax.swing.JLabel tracesLabel;
    private javax.swing.JTextField tracesTextField;
    private javax.swing.JLabel tracesToLabel;
    private javax.swing.JTextField tracesToTextField;
    private javax.swing.JButton validateModelButton;
    private javax.swing.JLabel volumeLabel;
    private javax.swing.JTextField voumeTextField;
    private javax.swing.JMenuItem writeAnalyzerScriptMenuItem;
    private javax.swing.JMenuItem writeDeliveryScriptsMenuItem;
    private javax.swing.JButton writeModelButton;
    private javax.swing.JMenuItem writeModelMenuItem;
    private javax.swing.JButton writeModelScriptsButton;
    private javax.swing.JButton runPriorInversionButton;
    private javax.swing.JMenuItem writeModelScriptsMenuItem;
    private javax.swing.JSeparator writeSeparator1;
    private javax.swing.JSeparator writeSeparator2;
    private javax.swing.JSeparator fileSeparator;
    private javax.swing.JMenuItem importMenuItem;
    private javax.swing.JMenuItem exportMenuItem;
    private javax.swing.JMenuItem importMenuModel;
    private javax.swing.JMenuItem exportMenuModel;
    private javax.swing.JMenuItem displayResultsMenuItem;
	private javax.swing.JButton displayResultsButton;
    // End of variables declaration//GEN-END:variables


    public void populate() {
         populateStack();
         populateModel();
         populateTopBottom();
    }

    public void populateTable() {
        populateStackTable();
        populateMatrixTable();
        populateFluidTable();
    }

    public void populateStack() {
        List<Inversion.SeismicData.Stack> stacks = agent.getInversion().getSeismicData().getStack();
        int size = stacks.size();
        Inversion.SeismicData.Stack stack;
        //if (size < 1) return;
        int count = 0;
        ArrayList<String> defaultStackList = (ArrayList)DeliveryLiteConstants.DEFAULT_STACK_LIST.clone();
        for (int i = 0; i < size; i++) {
            stack = stacks.get(i);
            if(stack.getName().equals(DeliveryLiteConstants.DEFAULT_NEAR_STACK_NAME)){
                ///nearStackFilenameTextField.setText(stack.getFilename());
                if (stack.getWavelet() != null) {
                    ////nearStackWaveletFilenameTextField.setText(stack.getWavelet().getFilename());
                    nearNoiseRMSTextField.setText(Float.toString(stack.getWavelet().getNoiseRms().getValue()));
                    modelParams.setNearNoiseRMS(stack.getWavelet().getNoiseRms().getValue());
                }
                defaultStackList.remove(stack.getName());
                count++;
            }else if(stack.getName().equals(DeliveryLiteConstants.DEFAULT_FAR_STACK_NAME)){
                ///farStackFilenameTextField.setText(stack.getFilename());
                if (stack.getWavelet() != null) {
                    ///farStackWaveletFilenameTextField.setText(stack.getWavelet().getFilename());
                    farNoiseRMSTextField.setText(Float.toString(stack.getWavelet().getNoiseRms().getValue()));
                    modelParams.setFarNoiseRMS(stack.getWavelet().getNoiseRms().getValue());
                }
                count++;
                //defaultStackList.remove(stack.getName());
            }
        }

        //Reconstruct if missing default values
        for(int i = 0; i < defaultStackList.size(); i++){
            String name = defaultStackList.get(i);
            Inversion.SeismicData.Stack stk;
            if(name.equals(DeliveryLiteConstants.DEFAULT_NEAR_STACK_NAME)){
                JOptionPane.showMessageDialog(this, name + " is required for Stack Properties. Reset it to default.");
                stk = new Inversion.SeismicData.Stack();
                stk.setName(name);
                stk.setFilename(DeliveryLiteConstants.DEFAULT_NEAR_STACK_FILE_NAME);
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_MIN_OFFSET));
                stk.setMinOffset(vf);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_MAX_OFFSET));
                stk.setMaxOffset(vf);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_REFLECTOR_TIME));
                stk.setReflectorTime(vf);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_VELOCITY));
                stk.setStackVelocity(vf);
                Inversion.SeismicData.Stack.Wavelet wav = new Inversion.SeismicData.Stack.Wavelet();
                wav.setFilename(DeliveryLiteConstants.DEFAULT_STACK_WAVELET_FILE_NAME);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_WAVELET_NOISE_RMS));
                wav.setNoiseRms(vf);
                stk.setWavelet(wav);
                stacks.add(stk);
                count++;
                nearStackFilenameTextField.setText(stk.getFilename());
                if (stk.getWavelet() != null) {
                    nearStackWaveletFilenameTextField.setText(stk.getWavelet().getFilename());
                    nearNoiseRMSTextField.setText(Float.toString(stk.getWavelet().getNoiseRms().getValue()));
                    modelParams.setNearNoiseRMS(stk.getWavelet().getNoiseRms().getValue());
                }
            /*}else if(name.equals(DeliveryLiteConstants.DEFAULT_NEAR_STACK_NAME)){
                JOptionPane.showMessageDialog(this, name + " is required for Stack Properties. Reset it to default.");
                stk = new Inversion.SeismicData.Stack();
                stk.setName("far");
                stk.setFilename(DeliveryLiteConstants.DEFAULT_STACK_FAR_FILE_NAME);
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_MIN_OFFSET));
                stk.setMinOffset(vf);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_MAX_OFFSET));
                stk.setMaxOffset(vf);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_REFLECTOR_TIME));
                stk.setReflectorTime(vf);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_VELOCITY));
                stk.setStackVelocity(vf);
                Inversion.SeismicData.Stack.Wavelet wav = new Inversion.SeismicData.Stack.Wavelet();
                wav.setFilename(DeliveryLiteConstants.DEFAULT_STACK_WAVELET_FILE_NAME);
                vf = new VarFloat();
                vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_STACK_WAVELET_NOISE_RMS));
                wav.setNoiseRms(vf);
                stk.setWavelet(wav);
                stacks.add(stk);
                farStackFilenameTextField.setText(stk.getFilename());
                if (stk.getWavelet() != null) {
                    farStackWaveletFilenameTextField.setText(stk.getWavelet().getFilename());
                    farNoiseRMSTextField.setText(Float.toString(stk.getWavelet().getNoiseRms().getValue()));
                }
                */
            }
        }

        Inversion.SeismicData.AVOTerms avoTerm = agent.getInversion().getSeismicData().getAVOTerms();
        float a = avoTerm.getA().getValue();
        float b = avoTerm.getB().getValue();
        avoATextField.setText(Float.toString(a));
        modelParams.setAvoA(a);
        avoBTextField.setText(Float.toString(b));
        modelParams.setAvoA(b);

        //if (size == 1) {
        if(count == 1){
            nearOnlyRadioButton.setSelected(true);
            farStackFilenameTextField.setEditable(false);
            farStackWaveletFilenameTextField.setEditable(false);
            farNoiseRMSTextField.setEditable(false);
        } else {
            nearAndFarRadioButton.setSelected(true);
            farStackFilenameTextField.setEditable(true);
            farStackWaveletFilenameTextField.setEditable(true);
            farNoiseRMSTextField.setEditable(true);
        }
    }

    public void populateStackTable() {
        List<Inversion.SeismicData.Stack> stacks = agent.getInversion().getSeismicData().getStack();
        int size = stacks.size();
        if (size < 1) return;

        for(int i = 0; i < size; i++){
            //Inversion.SeismicData.Stack stack = stacks.get(0);
            Inversion.SeismicData.Stack stack = stacks.get(i);
            if(stack.getName().equals(DeliveryLiteConstants.DEFAULT_NEAR_STACK_NAME)){
                if (stack.getMinOffset() != null)
                    stackParams[0][0] = Float.toString(stack.getMinOffset().getValue());
                if (stack.getMaxOffset() != null)
                    stackParams[0][1] = Float.toString(stack.getMaxOffset().getValue());
                if (stack.getStackVelocity() != null)
                    stackParams[0][2] = Float.toString(stack.getStackVelocity().getValue());
                if (stack.getReflectorTime() != null)
                    stackParams[0][3] = Float.toString(stack.getReflectorTime().getValue());
            }else if(stack.getName().equals(DeliveryLiteConstants.DEFAULT_FAR_STACK_NAME)){
            //if (size > 1) {
                //stack = stacks.get(1);
                if (stack.getMinOffset() != null)
                   stackParams[1][0] = Float.toString(stack.getMinOffset().getValue());
                if (stack.getMaxOffset() != null)
                    stackParams[1][1] = Float.toString(stack.getMaxOffset().getValue());
                if (stack.getStackVelocity() != null)
                    stackParams[1][2] = Float.toString(stack.getStackVelocity().getValue());
                if (stack.getReflectorTime() != null)
                    stackParams[1][3] = Float.toString(stack.getReflectorTime().getValue());
            }
        }
    }

    public void populateMatrixTable() {
        String e, a, b, c;
        List<Inversion.RockFluidProperties.RockProperties.ReservoirEndmember> sands
             = agent.getInversion().getRockFluidProperties().getRockProperties().getReservoirEndmember();
        ArrayList<String> defaultReservoirEndmemberList = (ArrayList<String>)DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_LIST.clone();
        int size = sands.size();
        for(int i = 0; i < size; i++){
        //if (size  > 0) {
            Inversion.RockFluidProperties.RockProperties.ReservoirEndmember sand = sands.get(i);
            if(sand.getName().equals(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME)){
            //Inversion.RockFluidProperties.RockProperties.ReservoirEndmember sand = sands.get(0);
                e = Float.toString(sand.getVpCurve().getSigma().getValue());
                a = Float.toString(sand.getVpCurve().getIntercept().getValue());
                b = Float.toString(sand.getVpCurve().getDepthCoefficient().getValue());
                c = Float.toString(sand.getVpCurve().getLFIVCoefficient().getValue());
                refSandParams[0][0] = e;
                refSandParams[0][1] = a;
                refSandParams[0][2] = b;
                refSandParams[0][3] = c;

                e = Float.toString(sand.getPorosityCurve().getSigma().getValue());
                a = Float.toString(sand.getPorosityCurve().getIntercept().getValue());
                b = Float.toString(sand.getPorosityCurve().getSlope().getValue());
                refSandParams[1][0] = e;
                refSandParams[1][1] = a;
                refSandParams[1][2] = b;
                refSandParams[1][3] = null;

                e = Float.toString(sand.getVsCurve().getSigma().getValue());
                a = Float.toString(sand.getVsCurve().getIntercept().getValue());
                b = Float.toString(sand.getVsCurve().getSlope().getValue());
                refSandParams[2][0] = e;
                refSandParams[2][1] = a;
                refSandParams[2][2] = b;
                refSandParams[2][3] = null;

                String vp = Float.toString(sand.getVpGrain().getValue());
                String vs = Float.toString(sand.getVsGrain().getValue());
                String rho = Float.toString(sand.getRhoGrain().getValue());
                grainParams[0][0] = vp;
                grainParams[0][1] = vs;
                grainParams[0][2] = rho;
        //}
                defaultReservoirEndmemberList.remove(sand.getName());
            }
        }
        if(defaultReservoirEndmemberList.size() > 0){
            JOptionPane.showMessageDialog(this, defaultReservoirEndmemberList.get(0) + " is required for Matrix Properties. Reset it to default.");
            Inversion.RockFluidProperties.RockProperties.ReservoirEndmember sand1 = new
            Inversion.RockFluidProperties.RockProperties.ReservoirEndmember();
            sand1.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);

            Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VpCurve vcurve =
                new Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VpCurve();
            VarFloat vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_INTERCEPT));
            vcurve.setIntercept(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_DEPTH_COEFFICIENT));
            vcurve.setDepthCoefficient(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_LFIV_COEFFICIENT));
            vcurve.setLFIVCoefficient(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_SIGMA));
            vcurve.setSigma(vf);
            sand1.setVpCurve(vcurve);

            Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VsCurve vsCurve =
                new Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VsCurve();
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_INTERCEPT));
            vsCurve.setIntercept(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_SLOPE));
            vsCurve.setSlope(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_SIGMA));
            vsCurve.setSigma(vf);
            sand1.setVsCurve(vsCurve);

            Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.PorosityCurve pCurve =
                new Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.PorosityCurve();
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_POROSITY_CURVE_INTERCEPT));
            pCurve.setIntercept(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_POROSITY_CURVE_SLOPE));
            pCurve.setSlope(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_POROSITY_CURVE_SIGMA));
            pCurve.setSigma(vf);
            sand1.setPorosityCurve(pCurve);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_VS_GRAIN));
            sand1.setVsGrain(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_VP_GRAIN));
            sand1.setVpGrain(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_RHO_GRAIN));
            sand1.setRhoGrain(vf);
            sands.add(sand1);

            refSandParams[0][0] = DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_SIGMA;
            refSandParams[0][1] = DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_INTERCEPT;
            refSandParams[0][2] = DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_DEPTH_COEFFICIENT;
            refSandParams[0][3] = DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VPCURVE_LFIV_COEFFICIENT;

            refSandParams[1][0] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_POROSITY_CURVE_SIGMA;
            refSandParams[1][1] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_POROSITY_CURVE_INTERCEPT;
            refSandParams[1][2] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_POROSITY_CURVE_SLOPE;
            refSandParams[1][3] = null;

            refSandParams[2][0] = DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_SIGMA;
            refSandParams[2][1] = DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_INTERCEPT;
            refSandParams[2][2] = DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_VSCURVE_SLOPE;
            refSandParams[2][3] = null;

            grainParams[0][0] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_VP_GRAIN;
            grainParams[0][1] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_VS_GRAIN;
            grainParams[0][2] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_RHO_GRAIN;
        }
        //create required reservoir endmember if not exist

        List<Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember> shales
             = agent.getInversion().getRockFluidProperties().getRockProperties().getNonreservoirEndmember();
        size = shales.size();

        ArrayList<String> defaultNonReservoirEndmemberList = (ArrayList<String>)DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_LIST.clone();
        for (int i=0; i<size; i++) {
            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember shale = shales.get(i);
            if (shale.getName().equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME)) {

                e = Float.toString(shale.getVpCurve().getSigma().getValue());
                a = Float.toString(shale.getVpCurve().getIntercept().getValue());
                b = Float.toString(shale.getVpCurve().getDepthCoefficient().getValue());
                c = Float.toString(shale.getVpCurve().getLFIVCoefficient().getValue());
                boundShaleParams[0][0] = e;
                boundShaleParams[0][1] = a;
                boundShaleParams[0][2] = b;
                boundShaleParams[0][3] = c;

                e = Float.toString(shale.getDensityCurve().getSigma().getValue());
                a = Float.toString(shale.getDensityCurve().getFactor().getValue());
                b = Float.toString(shale.getDensityCurve().getExponent().getValue());
                boundShaleParams[1][0] = e;
                boundShaleParams[1][1] = a;
                boundShaleParams[1][2] = b;
                boundShaleParams[1][3] = null;

                e = Float.toString(shale.getVsCurve().getSigma().getValue());
                a = Float.toString(shale.getVsCurve().getIntercept().getValue());
                b = Float.toString(shale.getVsCurve().getSlope().getValue());
                boundShaleParams[2][0] = e;
                boundShaleParams[2][1] = a;
                boundShaleParams[2][2] = b;
                boundShaleParams[2][3] = null;
                defaultNonReservoirEndmemberList.remove(shale.getName());
            } else if (shale.getName().equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME)) {
                e = Float.toString(shale.getVpCurve().getSigma().getValue());
                a = Float.toString(shale.getVpCurve().getIntercept().getValue());
                b = Float.toString(shale.getVpCurve().getDepthCoefficient().getValue());
                c = Float.toString(shale.getVpCurve().getLFIVCoefficient().getValue());
                mixShaleParams[0][0] = e;
                mixShaleParams[0][1] = a;
                mixShaleParams[0][2] = b;
                mixShaleParams[0][3] = c;

                e = Float.toString(shale.getDensityCurve().getSigma().getValue());
                a = Float.toString(shale.getDensityCurve().getFactor().getValue());
                b = Float.toString(shale.getDensityCurve().getExponent().getValue());
                mixShaleParams[1][0] = e;
                mixShaleParams[1][1] = a;
                mixShaleParams[1][2] = b;
                mixShaleParams[1][3] = null;

                e = Float.toString(shale.getVsCurve().getSigma().getValue());
                a = Float.toString(shale.getVsCurve().getIntercept().getValue());
                b = Float.toString(shale.getVsCurve().getSlope().getValue());
                mixShaleParams[2][0] = e;
                mixShaleParams[2][1] = a;
                mixShaleParams[2][2] = b;
                mixShaleParams[2][3] = null;
                defaultNonReservoirEndmemberList.remove(shale.getName());
            }
        }
        ////create required nonreservoir endmember if not exist
        for(int i = 0; i < defaultNonReservoirEndmemberList.size(); i++){
            JOptionPane.showMessageDialog(this, defaultNonReservoirEndmemberList.get(i) + " is required for Matrix Properties. Reset it to default.");
            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember shale1 = new
            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember();
            shale1.setName(defaultNonReservoirEndmemberList.get(i));

            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VpCurve vcurve =
                new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VpCurve();
            VarFloat vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_INTERCEPT));
            vcurve.setIntercept(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_DEPTH_COEFFICIENT));
            vcurve.setDepthCoefficient(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_LFIV_COEFFICIENT));
            vcurve.setLFIVCoefficient(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_SIGMA));
            vcurve.setSigma(vf);
            shale1.setVpCurve(vcurve);

            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VsCurve vsCurve =
                new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VsCurve();
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_INTERCEPT));
            vsCurve.setIntercept(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SLOPE));
            vsCurve.setSlope(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SIGMA));
            vsCurve.setSigma(vf);
            shale1.setVsCurve(vsCurve);

            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.DensityCurve dCurve =
                new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.DensityCurve();
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_FACTOR));
            dCurve.setFactor(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_EXPONENT));
            dCurve.setExponent(vf);
            vf = new VarFloat();
            vf.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_SIGMA));
            dCurve.setSigma(vf);
            shale1.setDensityCurve(dCurve);
            shales.add(shale1);
            if(defaultNonReservoirEndmemberList.get(i).equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME)){
                mixShaleParams[0][0] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_SIGMA;
                mixShaleParams[0][1] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_INTERCEPT;
                mixShaleParams[0][2] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_DEPTH_COEFFICIENT;
                mixShaleParams[0][3] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_LFIV_COEFFICIENT;

                mixShaleParams[1][0] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_SIGMA;
                mixShaleParams[1][1] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_FACTOR;
                mixShaleParams[1][2] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_EXPONENT;
                mixShaleParams[1][3] = null;

                mixShaleParams[2][0] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SIGMA;
                mixShaleParams[2][1] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_INTERCEPT;
                mixShaleParams[2][2] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SLOPE;
                mixShaleParams[2][3] = null;
            }else if(defaultNonReservoirEndmemberList.get(i).equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME)){
                boundShaleParams[0][0] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_SIGMA;
                boundShaleParams[0][1] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_INTERCEPT;
                boundShaleParams[0][2] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_DEPTH_COEFFICIENT;
                boundShaleParams[0][3] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VPCURVE_LFIV_COEFFICIENT;

                boundShaleParams[1][0] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_SIGMA;
                boundShaleParams[1][1] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_FACTOR;
                boundShaleParams[1][2] = DeliveryLiteConstants.DEFAULT_ENDMEMBER_DENSITY_CURVE_EXPONENT;
                boundShaleParams[1][3] = null;

                boundShaleParams[2][0] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SIGMA;
                boundShaleParams[2][1] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_INTERCEPT;
                boundShaleParams[2][2] = DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_VSCURVE_SLOPE;
                boundShaleParams[2][3] = null;
            }
        }
    }


    private void populateFluidTable() {
        List<Inversion.RockFluidProperties.FluidProperties.Fluid> fluids
         = agent.getInversion().getRockFluidProperties().getFluidProperties().getFluid();
        String vp, rho, svp = null, srho = null;
        Inversion.RockFluidProperties.FluidProperties.Fluid fluid;
        int size = fluids.size();
        ArrayList<String> defaultFluidListClone = (ArrayList<String>)DeliveryLiteConstants.DEFAULT_FLUID_LIST.clone();
        //populate the Fluid Properties table
        for (int i=0; i<size; i++) {
            fluid = fluids.get(i);
            svp = null;
            srho = null;
            int j = -1;
            String fluidName = fluid.getName();
            if (fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME))   j = 0;
            else if (fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME)) j = 2;
            else if (fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME)) j = 4;
            else if(fluidName.equals(DeliveryLiteConstants.DEFAULT_REFERENCE_FLUID_NAME)){
                j = -1;
                vp = Float.toString(fluid.getVp().getValue());
                rho = Float.toString(fluid.getRho().getValue());
                refFluidParams[0][0] = vp;
                refFluidParams[0][1] = rho;
            }
            //else if (fluidName.equals("lsg")) j = 6;
            defaultFluidListClone.remove(fluidName);
            if (j == -1) continue;

            if(fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME)){
                fluidsModel.setBrineVp(fluid.getVp().getValue());
                fluidsModel.setBrineRho(fluid.getRho().getValue());
            }else if(fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME)){
                fluidsModel.setOilVp(fluid.getVp().getValue());
                fluidsModel.setOilRho(fluid.getRho().getValue());
            }else if(fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME)){
                fluidsModel.setGasVp(fluid.getVp().getValue());
                fluidsModel.setGasRho(fluid.getRho().getValue());
            }
            vp = Float.toString(fluid.getVp().getValue());
            rho = Float.toString(fluid.getRho().getValue());
            fluidsParams[0][j] = vp;
            fluidsParams[0][j+1] = rho;
            if (fluid.getSigmaVp() != null) {
                svp = Float.toString(fluid.getSigmaVp().getValue());
                fluidsParams[1][j] = svp;
            }
            if (fluid.getSigmaRho() != null) {
                srho = Float.toString(fluid.getSigmaRho().getValue());
                fluidsParams[1][j+1] = srho;
            }

            if (fluid.getSigmaRho() != null) {
                srho = Float.toString(fluid.getSigmaRho().getValue());
                fluidsParams[1][j+1] = srho;
            }
        }

        fluidsParams[0][6] = currentOilSaturation;
        fluidsParams[0][7] = currentHighGasSaturation;
        fluidsParams[0][8] = currentLowGasSaturation;
        fluidsParams[1][6] = currentOilSigmaSaturation;
        fluidsParams[1][7] = currentHighGasSigmaSaturation;
        fluidsParams[1][8] = currentLowGasSigmaSaturation;

        //Reconstruct the missing fluids
        for (int i=0; i<defaultFluidListClone.size(); i++) {
            svp = null;
            srho = null;
            int j = -1;
            String fluidName = defaultFluidListClone.get(i);
            JOptionPane.showMessageDialog(this, fluidName + " is required for Fluids table. Reset it to default.");
            if (fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME))   j = 0;
            else if (fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME)) j = 2;
            else if (fluidName.equals(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME)) j = 4;
            else if (fluidName.equals(DeliveryLiteConstants.DEFAULT_REFERENCE_FLUID_NAME)){
                j = -1;
                fluid = new Inversion.RockFluidProperties.FluidProperties.Fluid();
                VarFloat val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_VP));
                fluid.setVp(val);
                fluid.setName(fluidName);
                val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_RHO));
                fluid.setRho(val);
                fluids.add(fluid);
            }

            if (j == -1) continue;
            fluid = new Inversion.RockFluidProperties.FluidProperties.Fluid();
            if(j == 0){
                VarFloat val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_VP));
                fluid.setVp(val);
                fluid.setName(fluidName);
                val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_RHO));
                fluid.setRho(val);
            }else if(j == 2){
                VarFloat val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_OIL_VP));
                fluid.setVp(val);
                fluid.setName(fluidName);
                val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_OIL_RHO));
                fluid.setRho(val);
            }else if(j == 4){
                VarFloat val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_GAS_VP));
                fluid.setVp(val);
                fluid.setName(fluidName);
                val = new VarFloat();
                val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_GAS_RHO));
                fluid.setRho(val);
            }//else if(j == 6){
            //  VarFloat val = new VarFloat();
            //  val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_LSG_VP));
            //  fluid.setVp(val);
            //  fluid.setName(fluidName);
            //  val = new VarFloat();
            //  val.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_FLUID_LSG_RHO));
            //  fluid.setRho(val);
            //}
            fluids.add(fluid);

            vp = Float.toString(fluid.getVp().getValue());
            rho = "";
            if(fluid.getRho() != null)
                rho = Float.toString(fluid.getRho().getValue());
            fluidsParams[0][j] = vp;
            fluidsParams[0][j+1] = rho;
            if (fluid.getSigmaVp() != null) {
                svp = Float.toString(fluid.getSigmaVp().getValue());
                fluidsParams[1][j] = svp;
            }
            if (fluid.getSigmaRho() != null) {
                srho = Float.toString(fluid.getSigmaRho().getValue());
                fluidsParams[1][j+1] = srho;
            }
        }

        //populate the Reference Fluid table
        List<Inversion.RockFluidProperties.RockProperties.ReservoirEndmember> endmems
         = agent.getInversion().getRockFluidProperties().getRockProperties().getReservoirEndmember();
        //find the <reservoir_endmember> with a <reference_fluid>
        Inversion.RockFluidProperties.RockProperties.ReservoirEndmember refmem;
        String refFluid = "";
        for (int i=0; i<endmems.size(); i++) {
            refmem = endmems.get(i);
            refFluid = refmem.getReferenceFluid();
            if (refFluid != null && !refFluid.equals("")) break;    //found one
        }
        //find the Fluid name that matches the name of the reference fluid
        if (refFluid != null && !refFluid.equals("")) {
            svp = null;
            srho = null;
            for (int i=0; i<fluids.size(); i++) {
                fluid = fluids.get(i);
                if (fluid.getName().equals(refFluid)) {
                    vp = Float.toString(fluid.getVp().getValue());
                    rho = Float.toString(fluid.getRho().getValue());
                    refFluidParams[0][0] = vp;
                    refFluidParams[0][1] = rho;
/* Assumes errors are always zero for DeliveryLite
                    if (fluid.getSigmaVp() != null) {
                        svp = Float.toString(fluid.getSigmaVp().getValue());
                        refFluidParams[1][0] = svp;
                    }
                    if (fluid.getSigmaRho() != null) {
                        srho = Float.toString(fluid.getSigmaRho().getValue());
                        refFluidParams[1][1] = srho;
                    }
*/
                    break;
                }
            }
        } else {
            //Model has no reference fluid. Make a default one.
            size = endmems.size();
            //check if there is at least one <reservoir_endmember>; there should be
            if (size > 0) {
                //add the <reference_fluid> to the first <reservoir_endmember>
                refmem = endmems.get(0);
                refmem.setReferenceFluid("reference fluid");
                //create a Fluid with name "refernce fluid"
                fluid = new Inversion.RockFluidProperties.FluidProperties.Fluid();
                fluid.setName("reference fluid");
                VarFloat val = new VarFloat();
                val.setValue(5250.0f);
                fluid.setVp(val);
                val = new VarFloat();
                val.setValue(0.0f);
                fluid.setSigmaVp(val);
                val = new VarFloat();
                val.setValue(1.01f);
                fluid.setRho(val);
                val = new VarFloat();
                val.setValue(0.0f);
                fluid.setSigmaRho(val);
                fluids.add(fluid);
                //populate the Reference Fluid table
                refFluidParams[0][0] = "5250.0";
                refFluidParams[0][1] = "1.01";
            }
        }
    }

    private void populateModel() {
        FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
        HorizonLineTableModel tableModel = (HorizonLineTableModel)layerTable.getModel();
        //tableModel.resetRowLineAttributes();
        int oldSize = fixedTableModel.getRowCount();
        Inversion.ModelDescription.MiddleLayer layer = null;
        List<Inversion.ModelDescription.MiddleLayer> layers =
            agent.getInversion().getModelDescription().getMiddleLayer();
        int size = layers.size();
        int additionalRows = 0;
        int atRow = 1;
        if (size > 0)
           additionalRows = size;


        //insert new model rows after the old model's Top
        for (int i=0; i<additionalRows; i++) {
            layer = layers.get(i);
            String lName = layer.getName();
            float ng = layer.getNetToGross().getValue();
            String time = null;
            if(layer.getTime() != null)
                time   = Float.toString(layer.getTime().getValue());
            String stime = null;
            if (layer.getSigmaTime() != null)
               stime  = Float.toString(layer.getSigmaTime().getValue());

            String TVDBML = "";
            if(layer.getDepthRockCurves() != null){
                TVDBML = Float.toString(layer.getDepthRockCurves().getValue());
            }

            String depth  = "";
            if(layer.getDepth() != null)
                depth = Float.toString(layer.getDepth().getValue());
            //depthOfMasterLayerTextField.setText(depth);
            String sdepth = null;
            if (layer.getSigmaDepth() != null){
                sdepth  = Float.toString(layer.getSigmaDepth().getValue());
                errorOfMasterLayerDepthTextField.setText(sdepth);
                modelParams.setErrorMasterLayerDepth(layer.getSigmaDepth().getValue());
            }
            String th = null;
            if(layer.getThickness() != null)
              th = Float.toString(layer.getThickness().getValue());

            String sth = null;
            if (layer.getSigmaThickness() != null)
                sth  = Float.toString(layer.getSigmaThickness().getValue());

            String LFIV  = null;
            if(layer.getLFIV() != null)
                LFIV  = Float.toString(layer.getLFIV().getValue());
            String sLFIV = null;
            if (layer.getSigmaLFIV() != null)
                sLFIV  = Float.toString(layer.getSigmaLFIV().getValue());

            String ng1 = null;
            if(layer.getNetToGross() != null)
                ng1  = Float.toString(layer.getNetToGross().getValue());
            String sng = null;
            if (layer.getSigmaNetToGross() != null)
                sng  = Float.toString(layer.getSigmaNetToGross().getValue());
            String lType = getLayerType(ng, sng == null ? 0 : layer.getSigmaNetToGross().getValue());

            Inversion.ModelDescription.MiddleLayer.ReservoirEndmember reservoirEndmember = layer.getReservoirEndmember();
            String probOil = "", satOil = "";
            String probGas = "", satGas = "";
            String probLsg = "", satLsg = "";
            if (reservoirEndmember != null) {
                Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Oil oil = reservoirEndmember.getOil();
                if (oil != null) {
                    if (oil.getProbabilityOfOil() != null)
                        probOil = Float.toString(oil.getProbabilityOfOil().getValue());
                    if (oil.getSaturationOfOil() != null)
                        satOil = Float.toString(oil.getSaturationOfOil().getValue());
                }

                Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Gas gas = reservoirEndmember.getGas();
                if (gas != null) {
                    if (gas.getProbabilityOfGas() != null)
                        probGas = Float.toString(gas.getProbabilityOfGas().getValue());
                    if (gas.getSaturationOfGas() != null)
                        satGas = Float.toString(gas.getSaturationOfGas().getValue());
                }

                Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Lsg lsg = reservoirEndmember.getLsg();
                if (lsg != null) {
                    if (lsg.getProbabilityOfLsg() != null)
                        probLsg = Float.toString(lsg.getProbabilityOfLsg().getValue());
                    if (lsg.getSaturationOfLsg() != null)
                        satLsg = Float.toString(lsg.getSaturationOfLsg().getValue());
                }
            }
            fixedTableModel.insertRow(atRow, new Object [] { Integer.valueOf(i+2), lType.equals(SAND_LAYER_TYPE) ? true : false, lType, lName});
            fixedTableModel.fireTableRowsInserted(atRow, atRow);
            fixedLayerTable.addRowSelectionInterval(atRow, atRow);
            tableModel.insertRow(atRow, new Object [] {time, stime, th, sth, TVDBML, LFIV, ng1, sng, probOil, probGas, probLsg});
            tableModel.fireTableRowsInserted(atRow, atRow);
            //remember layer type for row added
            rowLayerTypes.add(atRow, lType);
            atRow++;
        }

        //remove old model entries. Leave top and bottom rows.
        //delete row in both layer tables
        atRow = size+1;
        for (int j=0; j<oldSize-2; j++) {
            fixedTableModel.deleteRow(atRow);
            fixedTableModel.fireTableRowsDeleted(atRow, atRow);

            tableModel.removeRow(atRow);
            tableModel.fireTableRowsDeleted(atRow, atRow);

            //delete layer type for row deleted
            rowLayerTypes.remove(atRow);

            //clear selection
            fixedLayerTable.clearSelection();
            layerTable.clearSelection();
        }

        String val = Integer.toString(size + 2);
        numLayersComboBox.setSelectedItem(val);
        //numLayersComboBox.setEnabled(false);

        fixedTableModel.renumberRows();
    }


    private String getEstimatedNewTime(int row){
        TableModel model = layerTable.getModel();
        int size = model.getRowCount();
        if(row == 0)
            row = 1;
        if(row >= size - 1)
            row = size - 1;

        String uptime = (String)model.getValueAt(row-1, 0);
        String downtime = (String)model.getValueAt(row, 0);
        float fUptime = Float.parseFloat(uptime);
        float fDowntime = Float.parseFloat(downtime);
        float diff = (fDowntime - fUptime) / 2;
        return String.valueOf(fUptime + diff);
    }

    private String getEstimatedNewTVDBML(int row){
        TableModel model = layerTable.getModel();
        int size = model.getRowCount();
        if(row == 0)
            row = 1;
        if(row >= size - 1)
            row = size - 1;

        String up = (String)model.getValueAt(row-1, 4);
        String down = (String)model.getValueAt(row, 4);
        float fUp = Float.parseFloat(up);
        float fDown = Float.parseFloat(down);
        float diff = (fDown - fUp) / 2;
        return String.valueOf(fUp + diff);
    }

    /** @deprecated Updating the model now handled in populateModel() as the middle layers
     *  of the new model are inserted and the middle layers of the old model removed.
     */
    private void updateModel() {
        Inversion.ModelDescription.MiddleLayer layer = null;
        List<Inversion.ModelDescription.MiddleLayer> layers =
            agent.getInversion().getModelDescription().getMiddleLayer();
        int size = layers.size();
        int additionalRows = 0;
        int atRow = 1;
        if (size > 0)
           additionalRows = size;
        for (int i=0; i<additionalRows; i++) {
            layer = layers.get(i);
            String lName = layer.getName();
            float ng = layer.getNetToGross().getValue();

            String time   = Float.toString(layer.getTime().getValue());
            String stime = null;
            if (layer.getSigmaTime() != null)
               stime  = Float.toString(layer.getSigmaTime().getValue());
            String depth  = Float.toString(layer.getDepth().getValue());
            String sdepth = null;
            if (layer.getSigmaDepth() != null)
                sdepth  = Float.toString(layer.getSigmaDepth().getValue());
            String th = null;
            if(layer.getThickness() != null)
                th  = Float.toString(layer.getThickness().getValue());
            String sth = null;
            if (layer.getSigmaThickness() != null)
                sth  = Float.toString(layer.getSigmaThickness().getValue());

            String LFIV  = Float.toString(layer.getLFIV().getValue());
            String sLFIV = null;
            if (layer.getSigmaLFIV() != null)
                sLFIV  = Float.toString(layer.getSigmaLFIV().getValue());

            String ng1  = Float.toString(layer.getNetToGross().getValue());
            String sng = null;
            if (layer.getSigmaNetToGross() != null)
                sng  = Float.toString(layer.getSigmaNetToGross().getValue());
            String lType = getLayerType(ng, sng == null ? 0 : layer.getSigmaNetToGross().getValue());

            FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
            fixedTableModel.insertRow(atRow, new Object [] {Integer.valueOf(i+2), lType.equals(SAND_LAYER_TYPE) ? true : false, lType, lName});
            fixedTableModel.fireTableRowsInserted(atRow, atRow);
            fixedLayerTable.addRowSelectionInterval(atRow, atRow);

            DefaultTableModel tableModel = (DefaultTableModel)layerTable.getModel();
            tableModel.insertRow(atRow, new Object [] {time, stime, depth, sdepth,th, sth, LFIV, sLFIV, ng1, sng, "","", ""});
            tableModel.fireTableRowsInserted(atRow, atRow);
            //remember layer type for row added
            rowLayerTypes.add(atRow, lType);
            atRow++;
        }
        String val = Integer.toString(size + 2);
        numLayersComboBox.setSelectedItem(val);
        numLayersComboBox.setEnabled(false);
    }

    private void createLayers() {
        String err = updateTopBottom();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
        } else {
           List<Inversion.ModelDescription.MiddleLayer> layers =
               agent.getInversion().getModelDescription().getMiddleLayer();
           //layers.clear();
           //addLayers(layers);
           updateMiddleLayers(layers);
        }
    }

    /** Update layers to values entered. */
    private void addLayers(List<Inversion.ModelDescription.MiddleLayer> layers) {
        Vector data  = ((DefaultTableModel)layerTable.getModel()).getDataVector();
        Vector fixed = ((DefaultTableModel)fixedLayerTable.getModel()).getDataVector();
        int numLayers = data.size();
        if (numLayers == 2) return;
        VarFloat varFloat = null;
        VsFloat vsFloat = null;
        Inversion.ModelDescription.MiddleLayer layer =  null;
        String depth = depthOfMasterLayerTextField.getText();
        float fdepth = 0;

        if(depth != null && depth.trim().length() > 0){
            try{
                fdepth = Float.valueOf(depth);
            }catch(Exception e){
                fdepth = 0;
            }
        }
        for (int i = 1; i < numLayers - 1; i++) {
            Vector datav = (Vector)data.get(i);
            Vector fixdv = (Vector)fixed.get(i);
            layer = new Inversion.ModelDescription.MiddleLayer();
            layer.setName((String)fixdv.get(3));
            String type = (String)fixdv.get(2);
//TODO: Why not take value of N/G?
            vsFloat = new VsFloat();
            if ("Sand".equals(type)) {
               vsFloat.setValue(1.0f);
            } else {
                vsFloat.setValue(0.0f);
            }
            layer.setNetToGross(vsFloat);

            //String time = (String)datav.get(0);
            String time = (String)layerTable.getValueAt(i,0);
            vsFloat = new VsFloat();
            vsFloat.setValue(Float.parseFloat(time));
            layer.setTime(vsFloat);
            //String stime = (String)datav.get(1);
            String stime = (String)layerTable.getValueAt(i,1);
            if (stime != null && stime.length() > 0) {
                varFloat = new VarFloat();
                varFloat.setValue(Float.parseFloat(stime));
                layer.setSigmaTime(varFloat);
            }

            String th = (String)layerTable.getValueAt(i,2);
            if (th != null && th.length() > 0) {
              vsFloat = new VsFloat();
              vsFloat.setValue(Float.parseFloat(th));
              layer.setThickness(vsFloat);

              String sth = (String)layerTable.getValueAt(i,3);
              if (sth != null && sth.length() > 0) {
                varFloat = new VarFloat();
                varFloat.setValue(Float.parseFloat(sth));
                layer.setSigmaThickness(varFloat);
              }
            }


            //String h = (String)datav.get(4);
            //String TVDBML = (String)datav.get(4);
            String TVDBML = (String)layerTable.getValueAt(i,4);
            if (TVDBML != null && TVDBML.length() > 0) {
               varFloat = new VarFloat();
               varFloat.setValue(Float.parseFloat(TVDBML));
               layer.setDepthRockCurves(varFloat);
            }


            vsFloat = new VsFloat();
            vsFloat.setValue(fdepth);
            layer.setDepth(vsFloat);

            String lfiv = (String)layerTable.getValueAt(i,5);

            vsFloat = new VsFloat();
            vsFloat.setValue(Float.parseFloat(lfiv));
            layer.setLFIV(vsFloat);

            String ng = (String)layerTable.getValueAt(i,6);
            vsFloat = new VsFloat();
            float fng = 0;
            float fsng = 0;
            if(ng != null && ng.trim().length() != 0){
                try{
                    fng = Float.parseFloat(ng);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                }
                vsFloat.setValue(fng);
                layer.setNetToGross(vsFloat);
            }else{
                vsFloat.setValue(fng);
                layer.setNetToGross(vsFloat);
            }

            //String sng = (String)datav.get(7);
            String sng = (String)layerTable.getValueAt(i,7);
            //String sng = (String)datav.get(9);
            if (sng != null && sng.length() > 0) {
                try{
                    fsng = Float.parseFloat(sng);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                }
                varFloat = new VarFloat();
                varFloat.setValue(fsng);
                layer.setSigmaNetToGross(varFloat);
            }

            String probOil = (String)layerTable.getValueAt(i,8);
            String probGas = (String)layerTable.getValueAt(i,9);
            String probLsg = (String)layerTable.getValueAt(i,10);

            String lType = getLayerType(fng, fsng);
            Inversion.ModelDescription.MiddleLayer.ReservoirEndmember reservoirEndmember = layer.getReservoirEndmember();

            if (reservoirEndmember == null){
                reservoirEndmember = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember();
                layer.setReservoirEndmember(reservoirEndmember);
            }
            reservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);

            Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Brine brine = reservoirEndmember.getBrine();
            if (brine == null) {
                brine = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Brine();
                reservoirEndmember.setBrine(brine);
            }
            brine.setName("brine");

            Inversion.ModelDescription.MiddleLayer.NonreservoirEndmember nonreservoirEndmember = layer.getNonreservoirEndmember();

            if (nonreservoirEndmember == null)
                nonreservoirEndmember = new Inversion.ModelDescription.MiddleLayer.NonreservoirEndmember();

            layer.setNonreservoirEndmember(nonreservoirEndmember);
            if(lType.equals(SAND_LAYER_TYPE))
                    nonreservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME);
            else if(lType.equals(SHALE_LAYER_TYPE))
                    nonreservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME);


            if(probOil != null && !probOil.equals("")){
                float fvalue = 0;
                try{
                    fvalue = Float.valueOf(probOil).floatValue();
                }catch(Exception e){
                    e.printStackTrace();
                    fvalue = 0;
                }
                if(fvalue != 0){
                    Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Oil oil = reservoirEndmember.getOil();
                    if (oil == null) {
                        oil = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Oil();
                        reservoirEndmember.setOil(oil);
                    }
                    oil.setName(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME);
                    VarFloat val;
                    VsFloat sval;
                    if (probOil != null && !probOil.equals("")) {
                        val = new VarFloat();
                        val.setValue(Float.parseFloat(probOil));
                        oil.setProbabilityOfOil(val);
                    }

                    String satOil = getOilSaturationFromFluidsTable();
                    if (satOil != null && satOil.trim().length() > 0) {
                        sval = new VsFloat();
                        sval.setValue(Float.parseFloat(satOil));
                        oil.setSaturationOfOil(sval);
                    }else{
                        sval = new VsFloat();
                        sval.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_SATURATION_OF_OIL));
                        oil.setSaturationOfOil(sval);
                    }

                    String errsatOil = getSigmaOilSaturationFromFluidsTable();
                    if (errsatOil != null && errsatOil.trim().length() > 0) {
                        val = new VarFloat();
                        val.setValue(Float.parseFloat(errsatOil));
                        oil.setSigmaSaturationOfOil(val);
                    }
                }
            }

            if(probGas != null && !probGas.equals("")){
                float fvalue = 0;
                try{
                    fvalue = Float.valueOf(probGas).floatValue();
                }catch(Exception e){
                    e.printStackTrace();
                    fvalue = 0;
                }
                if(fvalue != 0){
                    Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Gas gas = reservoirEndmember.getGas();
                    if (gas == null) {
                        gas = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Gas();
                        reservoirEndmember.setGas(gas);
                    }
                    gas.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                    VarFloat val = new VarFloat();
                    VsFloat sval = new VsFloat();
                    if (probGas != null && !probGas.equals("")) {
                        val.setValue(Float.parseFloat(probGas));
                        gas.setProbabilityOfGas(val);
                    }

                    String satGas = getHighGasSaturationFromFluidsTable();
                    if (satGas != null && satGas.trim().length() > 0) {
                        sval.setValue(Float.parseFloat(satGas));
                        gas.setSaturationOfGas(sval);
                    }else{
                        sval.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_SATURATION_OF_GAS));
                        gas.setSaturationOfGas(sval);
                    }

                    String errsatGas = getSigmaHighGasSaturationFromFluidsTable();
                    if (errsatGas != null && errsatGas.trim().length() > 0) {
                        val = new VarFloat();
                        val.setValue(Float.parseFloat(errsatGas));
                        gas.setSigmaSaturationOfGas(val);
                    }
                }
            }

            if(probLsg != null && !probLsg.equals("")){
                float fvalue = 0;
                try{
                    fvalue = Float.valueOf(probLsg).floatValue();
                }catch(Exception e){
                    e.printStackTrace();
                    fvalue = 0;
                }
                if(fvalue != 0){
                    Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Lsg lsg = reservoirEndmember.getLsg();
                    if (lsg == null) {
                        lsg = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Lsg();
                        reservoirEndmember.setLsg(lsg);
                    }
                    lsg.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                    VarFloat val = new VarFloat();
                    VsFloat sval = new VsFloat();
                    if (probLsg != null && !probLsg.equals("")) {
                        val.setValue(Float.parseFloat(probLsg));
                        lsg.setProbabilityOfLsg(val);
                    }

                    String satLsg = getLowGasSaturationFromFluidsTable();
                    if (satLsg != null && satLsg.trim().length() > 0) {
                        sval.setValue(Float.parseFloat(satLsg));
                        lsg.setSaturationOfLsg(sval);
                    }else{
                        sval.setValue(Float.parseFloat(DeliveryLiteConstants.DEFAULT_LOW_GAS_SATURATION));
                        lsg.setSaturationOfLsg(sval);
                    }

                    String errsatLsg = getSigmaLowGasSaturationFromFluidsTable();
                    if (errsatLsg != null && errsatLsg.trim().length() > 0) {
                        val = new VarFloat();
                        val.setValue(Float.parseFloat(errsatLsg));
                        lsg.setSigmaSaturationOfLsg(val);
                    }
                }
            }

            String temp = masterDepthLayerTextField.getText();
            if (temp != null && temp.length() > 0) {
                BigInteger bInt = new BigInteger(temp);
                agent.getInversion().getOutput().setMasterDepthLayerNumber(bInt);
                int laynum = bInt.intValue();
                String sigmaDepth = errorOfMasterLayerDepthTextField.getText();
                float fSigmaDepth = 0;
                if(sigmaDepth != null && sigmaDepth.trim().length() > 0)
                    try{
                        fSigmaDepth = Float.parseFloat(sigmaDepth);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                if(laynum > 1 && laynum < numLayers && laynum == (i+1)){
                    VsFloat sval = new VsFloat();
                    sval.setValue(fSigmaDepth);
                    layer.setSigmaDepth(sval);
                }
            }
            layers.add(i-1, layer);
        }
    }


    /** Update middle layers to values entered. */
    private void updateMiddleLayers(List<Inversion.ModelDescription.MiddleLayer> layers) {
        if(layers == null)
            return;
        Vector data  = ((HorizonLineTableModel)layerTable.getModel()).getDataVector();
        Vector fixed = ((DefaultTableModel)fixedLayerTable.getModel()).getDataVector();
        int numLayers = data.size();
        if (numLayers == 2) return;
        if(layers.size() != numLayers-2){
            throw new IllegalArgumentException("Wrong list of layers passed: list size should be equals to the number of layers.");
        }
        VarFloat varFloat = null;
        VsFloat vsFloat = null;
        Inversion.ModelDescription.MiddleLayer layer =  null;
        String depth = depthOfMasterLayerTextField.getText();
        float fdepth = 0;

        if(depth != null && depth.trim().length() > 0){
            try{
                fdepth = Float.valueOf(depth);
            }catch(Exception e){
                fdepth = 0;
            }
        }
        for (int i = 1; i < numLayers - 1; i++) {
            Vector datav = (Vector)data.get(i);
            Vector fixdv = (Vector)fixed.get(i);
            layer = layers.get(i-1);
            layer.setName((String)fixdv.get(3));
            String type = (String)fixdv.get(2);
//TODO: Why not take value of N/G?
            vsFloat = new VsFloat();
            if ("Sand".equals(type)) {
               vsFloat.setValue(1.0f);
            } else {
                vsFloat.setValue(0.0f);
            }
            layer.setNetToGross(vsFloat);

            //String time = (String)datav.get(0);
            String time = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_TIME);
            vsFloat = new VsFloat();
            if(time == null || time.trim().length() == 0)
                vsFloat.setValue(0);
            else
                vsFloat.setValue(Float.parseFloat(time));
            layer.setTime(vsFloat);
            String stime = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME);
            varFloat = new VarFloat();
            if(stime == null || stime.trim().length() == 0)
                vsFloat.setValue(0);
            else
                varFloat.setValue(Float.parseFloat(stime));
            layer.setSigmaTime(varFloat);

            String th = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_THICKNESS);
            
            if(th != null && th.trim().length() > 0){
                vsFloat = new VsFloat();
                vsFloat.setValue(Float.parseFloat(th));
                layer.setThickness(vsFloat);
            }else
            	layer.setThickness(null);

            String sth = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS);
            if(sth != null && sth.trim().length() > 0){
                varFloat = new VarFloat();
                varFloat.setValue(Float.parseFloat(sth.trim()));
                layer.setSigmaThickness(varFloat);
            }else
            	layer.setSigmaThickness(null);

            String TVDBML = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_TVDBML);
            if (TVDBML != null && TVDBML.trim().length() > 0){
            	varFloat = new VarFloat();
            	varFloat.setValue(Float.parseFloat(TVDBML));
                layer.setDepthRockCurves(varFloat);
            }

            vsFloat = new VsFloat();
            vsFloat.setValue(fdepth);
            layer.setDepth(vsFloat);

            String lfiv = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_LFIV);

            vsFloat = new VsFloat();
            if(lfiv != null && lfiv.trim().length() > 0){
                vsFloat.setValue(Float.parseFloat(lfiv));
                layer.setLFIV(vsFloat);
            }

            float fng = 0, fsng = 0;
            String ng = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH);
            vsFloat = new VsFloat();
            if(ng == null || ng.trim().length() == 0)
                vsFloat.setValue(fng);
            else{
                fng = Float.parseFloat(ng);
                vsFloat.setValue(fng);
            }

            layer.setNetToGross(vsFloat);

            String sng = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG);
            
            if (sng != null && sng.trim().length() > 0){
                varFloat = new VarFloat();
                fsng = Float.parseFloat(sng);
                varFloat.setValue(fsng);
                layer.setSigmaNetToGross(varFloat);
            }else
                layer.setSigmaNetToGross(null);

            String probOil = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL);
            String probGas = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS);
            String probLsg = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG);

            String lType = getLayerType(fng, fsng);
            Inversion.ModelDescription.MiddleLayer.ReservoirEndmember reservoirEndmember = layer.getReservoirEndmember();

            if (reservoirEndmember == null){
                reservoirEndmember = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember();
                reservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
            }else{
                String reservoirEndmemberName = reservoirEndmember.getName();
                if(reservoirEndmemberName == null || reservoirEndmemberName.trim().length() == 0)
                    reservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
            }
            layer.setReservoirEndmember(reservoirEndmember);

            Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Brine brine = reservoirEndmember.getBrine();
            if (brine == null) {
                brine = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Brine();
                brine.setName(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME);
                reservoirEndmember.setBrine(brine);
            }else{
                String name = brine.getName();
                if(name == null || name.trim().length() == 0)
                    brine.setName(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME);
            }

            Inversion.ModelDescription.MiddleLayer.NonreservoirEndmember nonreservoirEndmember = layer.getNonreservoirEndmember();

            if (nonreservoirEndmember == null)
                nonreservoirEndmember = new Inversion.ModelDescription.MiddleLayer.NonreservoirEndmember();

            String nrEndmemberName = nonreservoirEndmember.getName();
            if(lType.equals(SAND_LAYER_TYPE)){
                if(nrEndmemberName == null || nrEndmemberName.trim().length() == 0)
                    nonreservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME);
            }else if(lType.equals(SHALE_LAYER_TYPE)){
                if(nrEndmemberName == null || nrEndmemberName.trim().length() == 0)
                    nonreservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME);
            }

            layer.setNonreservoirEndmember(nonreservoirEndmember);

            if(probOil != null && probOil.trim().length() > 0){
                float fvalue = 0;
                try{
                    fvalue = Float.valueOf(probOil).floatValue();
                }catch(Exception e){
                    e.printStackTrace();
                    fvalue = 0;
                }
                if(fvalue > 0){
                    Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Oil oil = reservoirEndmember.getOil();
                    if (oil == null) {
                        oil = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Oil();
                        reservoirEndmember.setOil(oil);
                        oil.setName(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME);
                    }else{
                        String name = oil.getName();
                        if(name == null || name.trim().length() == 0)
                            oil.setName(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME);
                    }
                    
                    VarFloat val;
                    VsFloat sval;
                    val = new VarFloat();
                    val.setValue(Float.parseFloat(probOil));
                    oil.setProbabilityOfOil(val);

                    sval = oil.getSaturationOfOil();
                    if (sval != null) {
                        float fval =  sval.getValue();
                        if(!Util.floatEqualZero(fval))
                            oil.getSaturationOfOil().setValue(fval);
                        else
                            oil.setSaturationOfOil(null);
                    }else{
                        String satOil = getOilSaturationFromFluidsTable();
                        float fval;
                        if(satOil != null && satOil.trim().length() > 0){
                            fval =  Float.parseFloat(satOil);
                        }else{
                            fval =  Float.parseFloat(DeliveryLiteConstants.DEFAULT_SATURATION_OF_OIL);
                        }
                        sval = new  VsFloat();
                        sval.setValue(fval);
                        oil.setSaturationOfOil(sval);
                    }

                    val = oil.getSigmaSaturationOfOil();
                    
                    if (val != null) {
                        float fval =  val.getValue();
                        if(!Util.floatEqualZero(fval))
                            oil.getSigmaSaturationOfOil().setValue(fval);
                        else
                            oil.setSigmaSaturationOfOil(null);
                    }else{
                        String errsatOil = getSigmaOilSaturationFromFluidsTable();
                        if(errsatOil != null && errsatOil.trim().length() > 0){
                            float fval =  Float.parseFloat(errsatOil);
                            val = new VarFloat();
                            val.setValue(fval);
                            oil.setSigmaSaturationOfOil(val);
                        }else{
                            oil.setSigmaSaturationOfOil(null);
                        }
                    }
                }else{
                	reservoirEndmember.setOil(null);
                }
            }else{
                reservoirEndmember.setOil(null);
            }

            if(probGas != null && probGas.trim().length() > 0){
                float fvalue = 0;
                try{
                    fvalue = Float.valueOf(probGas).floatValue();
                }catch(Exception e){
                    e.printStackTrace();
                    fvalue = 0;
                }
                if(fvalue > 0){
                    Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Gas gas = reservoirEndmember.getGas();
                    if (gas == null) {
                        gas = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Gas();
                        reservoirEndmember.setGas(gas);
                        gas.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                    }else{
                        String name = gas.getName();
                        if(name == null || name.trim().length() == 0)
                            gas.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                    }
                    
                    VarFloat val = new VarFloat();
                    VsFloat sval = new VsFloat();
                    val.setValue(Float.parseFloat(probGas));
                    gas.setProbabilityOfGas(val);

                    sval = gas.getSaturationOfGas();
                    if (sval != null) {
                        float fval =  sval.getValue();
                        if(!Util.floatEqualZero(fval))
                            gas.getSaturationOfGas().setValue(fval);
                        else
                            gas.setSaturationOfGas(null);
                    }else{
                        String satGas = getHighGasSaturationFromFluidsTable();
                        float fval;
                        if(satGas != null && satGas.trim().length() > 0){
                            fval =  Float.parseFloat(satGas);
                        }else{
                            fval =  Float.parseFloat(DeliveryLiteConstants.DEFAULT_SATURATION_OF_GAS);
                        }
                        sval = new  VsFloat();
                        sval.setValue(fval);
                        gas.setSaturationOfGas(sval);
                    }

                    val = gas.getSigmaSaturationOfGas();

                    if (val != null) {
                        float fval =  val.getValue();
                        if(!Util.floatEqualZero(fval))
                            gas.getSigmaSaturationOfGas().setValue(fval);
                        else
                            gas.setSigmaSaturationOfGas(null);
                    }else{
                        String errsatGas = getSigmaHighGasSaturationFromFluidsTable();
                        if(errsatGas != null && errsatGas.trim().length() > 0){
                            float fval =  Float.parseFloat(errsatGas);
                            val = new VarFloat();
                            val.setValue(fval);
                            gas.setSigmaSaturationOfGas(val);
                        }else{
                            gas.setSigmaSaturationOfGas(null);
                        }
                    }
                }else{
                	reservoirEndmember.setGas(null);
                }
            }else{
                reservoirEndmember.setGas(null);
            }

            if(probLsg != null && probLsg.trim().length() > 0){
                float fvalue = 0;
                try{
                    fvalue = Float.valueOf(probLsg).floatValue();
                }catch(Exception e){
                    e.printStackTrace();
                    fvalue = 0;
                }
                if(!Util.floatEqualZero(fvalue)){
                    Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Lsg lsg = reservoirEndmember.getLsg();
                    if (lsg == null) {
                        lsg = new Inversion.ModelDescription.MiddleLayer.ReservoirEndmember.Lsg();
                        reservoirEndmember.setLsg(lsg);
                        lsg.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                    }else{
                        String name = lsg.getName();
                        if(name == null || name.trim().length() == 0)
                            lsg.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                    }
                    
                    VarFloat val = new VarFloat();
                    VsFloat sval = new VsFloat();
                    val.setValue(Float.parseFloat(probLsg));
                    lsg.setProbabilityOfLsg(val);


                    sval = lsg.getSaturationOfLsg();
                    if (sval != null) {
                        float fval =  sval.getValue();
                        if(!Util.floatEqualZero(fval))
                            lsg.getSaturationOfLsg().setValue(fval);
                        else
                            lsg.setSaturationOfLsg(null);
                    }else{
                        String satLsg = getLowGasSaturationFromFluidsTable();
                        float fval;
                        if(satLsg != null && satLsg.trim().length() > 0){
                            fval =  Float.parseFloat(satLsg);
                        }else{
                            fval =  Float.parseFloat(DeliveryLiteConstants.DEFAULT_SATURATION_OF_LSG);
                        }
                        sval = new  VsFloat();
                        sval.setValue(fval);
                        lsg.setSaturationOfLsg(sval);
                    }

                    val = lsg.getSigmaSaturationOfLsg();

                    if (val != null) {
                        float fval =  val.getValue();
                        if(!Util.floatEqualZero(fval))
                            lsg.getSigmaSaturationOfLsg().setValue(fval);
                        else
                            lsg.setSigmaSaturationOfLsg(null);
                    }else{
                        String errsatLsg = getSigmaLowGasSaturationFromFluidsTable();
                        if(errsatLsg != null && errsatLsg.trim().length() > 0){
                            float fval =  Float.parseFloat(errsatLsg);
                            val = new VarFloat();
                            val.setValue(fval);
                            lsg.setSigmaSaturationOfLsg(val);
                        }else{
                            lsg.setSigmaSaturationOfLsg(null);
                        }
                    }
                }else{
                	reservoirEndmember.setLsg(null);
                }
            }else{
                reservoirEndmember.setLsg(null);
            }

            String temp = masterDepthLayerTextField.getText();
            if (temp != null && temp.length() > 0) {
                BigInteger bInt = new BigInteger(temp);
                agent.getInversion().getOutput().setMasterDepthLayerNumber(bInt);
                int laynum = bInt.intValue();
                String sigmaDepth = errorOfMasterLayerDepthTextField.getText();
                float fSigmaDepth = 0;
                if(sigmaDepth != null && sigmaDepth.trim().length() > 0)
                    try{
                        fSigmaDepth = Float.parseFloat(sigmaDepth);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                if(laynum > 1 && laynum < numLayers && laynum == (i+1)){
                    VsFloat sval = new VsFloat();
                    sval.setValue(fSigmaDepth);
                    layer.setSigmaDepth(sval);
                }
            }
            //layers.add(i-1, layer);
        }
    }

    private void populateTopBottom() {
        Inversion.ModelDescription.TopLayer top =
            agent.getInversion().getModelDescription().getTopLayer();

//        float ng = top.getNetToGross().getValue();
        String time = null;
        if(top.getTime() != null)
            time = Float.toString(top.getTime().getValue());
        String stime = null;
        if (top.getSigmaTime() != null)
             stime  = Float.toString(top.getSigmaTime().getValue());
        String TVDBML = "";
        if(top.getDepthRockCurves() != null){
            TVDBML = Float.toString(top.getDepthRockCurves().getValue());
        }

        String depth = "";
        if(top.getDepth() != null)
            depth = Float.toString(top.getDepth().getValue());
        depthOfMasterLayerTextField.setText(depth);
        modelParams.setDepthMasterLayer(top.getDepth().getValue());
        String sdepth = null;
        if (top.getSigmaDepth() != null){
            sdepth  = Float.toString(top.getSigmaDepth().getValue());
            errorOfMasterLayerDepthTextField.setText(sdepth);
            modelParams.setErrorMasterLayerDepth(top.getSigmaDepth().getValue());
        }
        String th = null;
        if(top.getThickness() != null)
            th  = Float.toString(top.getThickness().getValue());
        String sth = null;
        if (top.getSigmaThickness() != null)
            sth  = Float.toString(top.getSigmaThickness().getValue());

        String LFIV  = "";
       if(top.getLFIV() != null)
           LFIV = Float.toString(top.getLFIV().getValue());
        String sLFIV = null;
        if (top.getSigmaLFIV() != null)
            sLFIV  = Float.toString(top.getSigmaLFIV().getValue());

        String ng1 = null;
        if(top.getNetToGross() != null)
            ng1 = Float.toString(top.getNetToGross().getValue());
        String sng = null;
        if (top.getSigmaNetToGross() != null)
            sng  = Float.toString(top.getSigmaNetToGross().getValue());
        //String lType = getLayerType(ng, sng == null ? 0.0 : top.getSigmaNetToGross().getValue());

        HorizonLineTableModel tableModel = (HorizonLineTableModel)layerTable.getModel();
        Vector record = (Vector)tableModel.getDataVector().get(0);
        int tableSize = tableModel.getRowCount();
        record.set(0, time);
        record.set(1, stime);

        record.set(2, th);
        record.set(3, sth);
        record.set(4, TVDBML);
        record.set(5, LFIV);
        record.set(6, ng1);
        record.set(7, sng);

        Inversion.ModelDescription.TopLayer.ReservoirEndmember reservoirEndmember = top.getReservoirEndmember();
        if (reservoirEndmember != null) {
            Inversion.ModelDescription.TopLayer.ReservoirEndmember.Oil oil = reservoirEndmember.getOil();
            String probOil = "", satOil = "";
            if (oil != null) {
                if (oil.getProbabilityOfOil() != null)
                    probOil = Float.toString(oil.getProbabilityOfOil().getValue());
                if (oil.getSaturationOfOil() != null)
                    satOil = Float.toString(oil.getSaturationOfOil().getValue());
            }

            Inversion.ModelDescription.TopLayer.ReservoirEndmember.Gas gas = reservoirEndmember.getGas();
            String probGas = "", satGas = "";
            if (gas != null) {
                if (gas.getProbabilityOfGas() != null)
                    probGas = Float.toString(gas.getProbabilityOfGas().getValue());
                if (gas.getSaturationOfGas() != null)
                    satGas = Float.toString(gas.getSaturationOfGas().getValue());
            }

            Inversion.ModelDescription.TopLayer.ReservoirEndmember.Lsg lsg = reservoirEndmember.getLsg();
            String probLsg = "", satLsg = "";
            if (lsg != null) {
                if (lsg.getProbabilityOfLsg() != null)
                    probLsg = Float.toString(lsg.getProbabilityOfLsg().getValue());
                if (lsg.getSaturationOfLsg() != null)
                    satLsg = Float.toString(lsg.getSaturationOfLsg().getValue());
            }
            record.set(8, probOil);
            record.set(9, probGas);
            record.set(10, probLsg);
        }

        String lName = top.getName();
        FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
        record = (Vector)fixedTableModel.getDataVector().get(0);
        record.set(3, lName);

        Inversion.ModelDescription.BottomLayer bot =
            agent.getInversion().getModelDescription().getBottomLayer();

//        ng = bot.getNetToGross().getValue();

        time = null;
        if(bot.getTime() != null)
            time   = Float.toString(bot.getTime().getValue());
        stime = null;
        if (bot.getSigmaTime() != null)
               stime  = Float.toString(bot.getSigmaTime().getValue());

        String bTVDBML = "";
        if(bot.getDepthRockCurves() != null){
            bTVDBML = Float.toString(bot.getDepthRockCurves().getValue());
        }
        depth = null;
        if(bot.getDepth() != null)
            depth  = Float.toString(bot.getDepth().getValue());
        sdepth = null;
        if (bot.getSigmaDepth() != null){
             sdepth  = Float.toString(bot.getSigmaDepth().getValue());
             errorOfMasterLayerDepthTextField.setText(sdepth);
             modelParams.setErrorMasterLayerDepth(bot.getSigmaDepth().getValue());
        }
        th = null;
        if(bot.getThickness() != null)
            th  = Float.toString(bot.getThickness().getValue());
        sth = null;
        if (bot.getSigmaThickness() != null)
                sth  = Float.toString(bot.getSigmaThickness().getValue());

        LFIV  = Float.toString(bot.getLFIV().getValue());
        sLFIV = null;
        if (bot.getSigmaLFIV() != null)
                sLFIV  = Float.toString(bot.getSigmaLFIV().getValue());

        ng1 = null;
        if(bot.getNetToGross() != null)
            ng1  = Float.toString(bot.getNetToGross().getValue());
        sng = null;
        if (bot.getSigmaNetToGross() != null)
            sng  = Float.toString(bot.getSigmaNetToGross().getValue());
        //lType = getLayerType(ng, sng == null ? 0.0 : bot.getSigmaNetToGross().getValue());

        record = (Vector)tableModel.getDataVector().get(tableSize-1);
        record.set(0, time);
        record.set(1, stime);

        record.set(2, th);
        record.set(3, sth);
        record.set(4, bTVDBML);
        record.set(5, LFIV);
        record.set(6, ng1);
        record.set(7, sng);

        Inversion.ModelDescription.BottomLayer.ReservoirEndmember reservoirEndmember2 = bot.getReservoirEndmember();
        if (reservoirEndmember2 != null) {
            Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Oil oil = reservoirEndmember2.getOil();
            String probOil = "", satOil = "";
            if (oil != null) {
                if (oil.getProbabilityOfOil() != null)
                    probOil = Float.toString(oil.getProbabilityOfOil().getValue());
                if (oil.getSaturationOfOil() != null)
                    satOil = Float.toString(oil.getSaturationOfOil().getValue());
            }

            Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Gas gas = reservoirEndmember2.getGas();
            String probGas = "", satGas = "";
            if (gas != null) {
                if (gas.getProbabilityOfGas() != null)
                    probGas = Float.toString(gas.getProbabilityOfGas().getValue());
                //if (gas.getSaturationOfGas() != null)
                //    satGas = Float.toString(gas.getSaturationOfGas().getValue());
            }

            Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Lsg lsg = reservoirEndmember2.getLsg();
            String probLsg = "", satLsg = "";
            if (lsg != null) {
                if (lsg.getProbabilityOfLsg() != null)
                    probLsg = Float.toString(lsg.getProbabilityOfLsg().getValue());
                //if (lsg.getSaturationOfLsg() != null)
                //    satLsg = Float.toString(lsg.getSaturationOfLsg().getValue());
            }
            record.set(8, probOil);
            record.set(9, probGas);
            record.set(10, probLsg);
        }

        lName = bot.getName();
        record = (Vector)fixedTableModel.getDataVector().get(tableSize-1);
        record.set(3, lName);

        String tbase  = Float.toString(bot.getTBase().getValue());
        String stbase = null;
        if (bot.getSigmaTBase() != null)
            stbase = Float.toString(bot.getSigmaTBase().getValue());
        timeBaseTextField.setText(tbase);
        modelParams.setTimeBase(bot.getTBase().getValue());
        sigmaTimeBaseTextField.setText(stbase);
        modelParams.setSigmaTimeBase(bot.getSigmaTBase().getValue());
        int layerNum = agent.getInversion().getOutput().getMasterDepthLayerNumber().intValue();
        String mLater = Integer.toString(layerNum);
        masterDepthLayerTextField.setText(mLater);
        modelParams.setMasterDepthLayer(layerNum);

    }

    private String updateTopBottom() {
        String err = "";
        VarFloat vFloat = null;
        
        HorizonLineTableModel tableModel = (HorizonLineTableModel)layerTable.getModel();

        Inversion.ModelDescription.TopLayer top =
            agent.getInversion().getModelDescription().getTopLayer();
        if(top == null)
            top = new Inversion.ModelDescription.TopLayer();
        String depth = depthOfMasterLayerTextField.getText();

        float fdepth = 0;

        if(depth != null && depth.trim().length() > 0){
            try{
                fdepth = Float.valueOf(depth);
            }catch(Exception e){
                fdepth = 0;
            }
        }
        VarFloat varFloat = new VarFloat();
        varFloat.setValue(fdepth);
        VsFloat vsFloat = new VsFloat();
        vsFloat.setValue(fdepth);
        top.setDepth(varFloat);
        //time on top layer (required)
        String temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_TIME);
        if(temp != null && temp.trim().length() > 0)
            top.getTime().setValue(Float.parseFloat(temp));
        
        //sigma time on top layer (required)
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME);
        if (temp != null && temp.length() > 0) 
            top.getSigmaTime().setValue(Float.parseFloat(temp));
        
        //thickness on top layer
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_THICKNESS);
        if(temp != null && temp.trim().length() > 0){
            vFloat = new VarFloat();
            vFloat.setValue(Float.parseFloat(temp));
            top.setThickness(vFloat);
        }else
        	top.setThickness(null);
        //sigma thickness on top layer
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS);
        if (temp != null && temp.trim().length() > 0) {
            vFloat = new VarFloat();
            vFloat.setValue(Float.parseFloat(temp.trim()));
            top.setSigmaThickness(vFloat);
        }else
        	top.setSigmaThickness(null);

        //TVDBML on top
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_TVDBML);
        if(temp != null && temp.trim().length() > 0){
            vFloat = new VarFloat();
            vFloat.setValue(Float.parseFloat(temp));
            top.setDepthRockCurves(vFloat);
        }

        //LFIV on top (required)
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_LFIV);
        //temp = (String)record.get(6);
        if(temp != null && temp.trim().length() > 0){
            top.getLFIV().setValue(Float.parseFloat(temp));
        }

        //Net to Growth on top layer (required)
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH);
        float fng = 0;
        float fsng = 0;
        if(temp != null && temp.trim().length() > 0)
            fng = Float.parseFloat(temp);
        top.getNetToGross().setValue(fng);

        //sigma N/G on top layer
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG);
        if (temp != null && temp.length() > 0) {
            fsng = Float.parseFloat(temp);
            vFloat = new VarFloat();
            vFloat.setValue(fsng);
            top.setSigmaNetToGross(vFloat);
        }else
            top.setSigmaNetToGross(null);

        String lType = getLayerType(fng, fsng);

        Inversion.ModelDescription.TopLayer.ReservoirEndmember reservoirEndmember = agent.getInversion().getModelDescription().getTopLayer().getReservoirEndmember();
        if (reservoirEndmember == null){
             reservoirEndmember = new Inversion.ModelDescription.TopLayer.ReservoirEndmember();
             reservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
        }else{
            String name = reservoirEndmember.getName();
            if(name == null || name.trim().length() == 0)
                reservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
        }
        top.setReservoirEndmember(reservoirEndmember);

        Inversion.ModelDescription.TopLayer.NonreservoirEndmember nonReservoirEndmember = agent.getInversion().getModelDescription().getTopLayer().getNonreservoirEndmember();
        if (nonReservoirEndmember == null){
            nonReservoirEndmember = new Inversion.ModelDescription.TopLayer.NonreservoirEndmember();
            top.setNonreservoirEndmember(nonReservoirEndmember);
        }

        String name = nonReservoirEndmember.getName();
        if(lType.equals(SAND_LAYER_TYPE)){
            if(name == null || name.trim().length() == 0)
                nonReservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME);
        }else if(lType.equals(SHALE_LAYER_TYPE)){
            if(name == null || name.trim().length() == 0)
                nonReservoirEndmember.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME);
        }

        //Po on top layer
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL);
        float po = 0;
        if(temp != null && temp.trim().length() > 0){
            po = Float.parseFloat(temp);
            if(po > 0){
                Inversion.ModelDescription.TopLayer.ReservoirEndmember.Oil oil = new Inversion.ModelDescription.TopLayer.ReservoirEndmember.Oil();
                oil.setName(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME);
                vFloat = new VarFloat();
                vFloat.setValue(po);
                oil.setProbabilityOfOil(vFloat);
                if(top.getReservoirEndmember() != null)
                    top.getReservoirEndmember().setOil(oil);
                else{
                    Inversion.ModelDescription.TopLayer.ReservoirEndmember re = new Inversion.ModelDescription.TopLayer.ReservoirEndmember();
                    re.setOil(oil);
                    top.setReservoirEndmember(re);
                }
            }else
            	top.getReservoirEndmember().setOil(null);
        }else
        	top.getReservoirEndmember().setOil(null);
        //Pg on top layer
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS);
        float pG = 0;
        if(temp != null && temp.trim().length() > 0){
            pG = Float.parseFloat(temp);
            if(pG > 0){
                Inversion.ModelDescription.TopLayer.ReservoirEndmember.Gas gas = new Inversion.ModelDescription.TopLayer.ReservoirEndmember.Gas();
                gas.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                vFloat = new VarFloat();
                vFloat.setValue(pG);
                gas.setProbabilityOfGas(vFloat);
                if(top.getReservoirEndmember() != null)
                    top.getReservoirEndmember().setGas(gas);
                else{
                    Inversion.ModelDescription.TopLayer.ReservoirEndmember re = new Inversion.ModelDescription.TopLayer.ReservoirEndmember();
                    re.setGas(gas);
                    top.setReservoirEndmember(re);
                }
            }else
            	top.getReservoirEndmember().setGas(null);
        }else
        	top.getReservoirEndmember().setGas(null);

        //Plsg on top layer
        temp = (String)Util.getValueAt(layerTable,0,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG);
        float plsg = 0;
        if(temp != null && temp.trim().length() > 0){
            plsg = Float.parseFloat(temp);
            if(plsg > 0){
                //Inversion.ModelDescription.TopLayer.ReservoirEndmember re = new Inversion.ModelDescription.TopLayer.ReservoirEndmember();
                Inversion.ModelDescription.TopLayer.ReservoirEndmember.Lsg lsg = new Inversion.ModelDescription.TopLayer.ReservoirEndmember.Lsg();
                lsg.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                vFloat = new VarFloat();
                vFloat.setValue(plsg);
                lsg.setProbabilityOfLsg(vFloat);
                if(top.getReservoirEndmember() != null)
                    top.getReservoirEndmember().setLsg(lsg);
                else{
                    Inversion.ModelDescription.TopLayer.ReservoirEndmember re = new Inversion.ModelDescription.TopLayer.ReservoirEndmember();
                    re.setLsg(lsg);
                    top.setReservoirEndmember(re);
                }
            }else
            	top.getReservoirEndmember().setLsg(null);
        }else
        	top.getReservoirEndmember().setLsg(null);

        int tableSize = tableModel.getRowCount();
        Inversion.ModelDescription.BottomLayer bot =
            agent.getInversion().getModelDescription().getBottomLayer();
        if(bot == null)
            bot = new Inversion.ModelDescription.BottomLayer();
        
        bot.setDepth(vsFloat);

        int botIndex = tableSize-1;
        float botTime = 0;
        //time on bottom layer (required)
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_TIME);

        if(temp != null && temp.trim().length() > 0){
            botTime = Float.parseFloat(temp);
            bot.getTime().setValue(botTime);
        }
        //sigma time on bottom layer (required)
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_SIGMA_TIME);
        if (temp != null && temp.length() > 0)
            bot.getSigmaTime().setValue(Float.parseFloat(temp));

        //thickness on bottom layer
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_THICKNESS);
        if(temp != null && temp.trim().length() > 0){
            VsFloat vsf = new VsFloat();
            vsf.setValue(Float.parseFloat(temp));
            bot.setThickness(vsf);
        }else
        	bot.setThickness(null);

        //sigma thickness on bottom layer
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_SIGMA_THICKNESS);
        if (temp != null && temp.trim().length() > 0) {
            vFloat = new VarFloat();
            vFloat.setValue(Float.parseFloat(temp.trim()));
            bot.setSigmaThickness(vFloat);
        }else
        	bot.setSigmaThickness(null);

        //TVDBML on bottom layer
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_TVDBML);
        if(temp != null && temp.trim().length() > 0){
            vFloat = new VarFloat();
            vFloat.setValue(Float.parseFloat(temp));
            bot.setDepthRockCurves(vFloat);
        }

        //LFIV on bottom layer(required)
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_LFIV);
        if(temp != null && temp.trim().length() > 0){
            bot.getLFIV().setValue(Float.parseFloat(temp));
        }

        //Net to Growth on bottom layer (required)
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH);
        fng = 0;
        fsng = 0;
        if(temp != null && temp.trim().length() > 0)
            fng = Float.parseFloat(temp);
        bot.getNetToGross().setValue(fng);


        //sigma N/G on bottom layer
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG);
        if (temp != null && temp.length() > 0) {
            fsng = Float.parseFloat(temp);
            vFloat = new VarFloat();
            vFloat.setValue(fsng);
            bot.setSigmaNetToGross(vFloat);
        }else
            bot.setSigmaNetToGross(null);

        lType = getLayerType(fng, fsng);

        Inversion.ModelDescription.BottomLayer.ReservoirEndmember re = agent.getInversion().getModelDescription().getBottomLayer().getReservoirEndmember();
        if (re == null){
            re = new Inversion.ModelDescription.BottomLayer.ReservoirEndmember();
            re.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
        }else{
            String nm = re.getName();
            if(nm == null || nm.trim().length() == 0)
                re.setName(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME);
        }
        bot.setReservoirEndmember(re);


        Inversion.ModelDescription.BottomLayer.NonreservoirEndmember nonRe = agent.getInversion().getModelDescription().getBottomLayer().getNonreservoirEndmember();
        if (nonRe == null){
            nonRe = new Inversion.ModelDescription.BottomLayer.NonreservoirEndmember();
            bot.setNonreservoirEndmember(nonRe);
        }
        name = nonRe.getName();
        if(lType.equals(SAND_LAYER_TYPE)){
            if(name == null || name.trim().length() == 0)
            nonRe.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME);
        }else if(lType.equals(SHALE_LAYER_TYPE)){
            if(name == null || name.trim().length() == 0)
                nonRe.setName(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME);
        }
            
        //pO on bottom layer
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL);
        if(temp != null && temp.trim().length() > 0){
            po = Float.parseFloat(temp);
            if(po > 0){
                Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Oil oil = new Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Oil();
                oil.setName(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME);
                vFloat = new VarFloat();
                vFloat.setValue(po);
                oil.setProbabilityOfOil(vFloat);
                if(top.getReservoirEndmember() != null)
                    bot.getReservoirEndmember().setOil(oil);
                else{
                    Inversion.ModelDescription.BottomLayer.ReservoirEndmember res = new Inversion.ModelDescription.BottomLayer.ReservoirEndmember();
                    re.setOil(oil);
                    bot.setReservoirEndmember(re);
                }
            }else
            	bot.getReservoirEndmember().setOil(null);
        }else
        	bot.getReservoirEndmember().setOil(null);

        //pG on bottom layer
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS);

        if(temp != null && temp.trim().length() > 0){
            pG = Float.parseFloat(temp);
            if(pG > 0){
                Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Gas gas = new Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Gas();
                gas.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                vFloat = new VarFloat();
                vFloat.setValue(pG);
                gas.setProbabilityOfGas(vFloat);
                if(top.getReservoirEndmember() != null)
                    bot.getReservoirEndmember().setGas(gas);
                else{
                    Inversion.ModelDescription.BottomLayer.ReservoirEndmember res = new Inversion.ModelDescription.BottomLayer.ReservoirEndmember();
                    re.setGas(gas);
                    bot.setReservoirEndmember(res);
                }
            }else
            	bot.getReservoirEndmember().setGas(null);
        }else
        	bot.getReservoirEndmember().setGas(null);

        //Plsg on bottom layer
        temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG);
        if(temp != null && temp.trim().length() > 0){
            plsg = Float.parseFloat(temp);
            if(plsg > 0){
                Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Lsg lsg = new Inversion.ModelDescription.BottomLayer.ReservoirEndmember.Lsg();
                lsg.setName(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
                vFloat = new VarFloat();
                vFloat.setValue(plsg);
                lsg.setProbabilityOfLsg(vFloat);
                if(bot.getReservoirEndmember() != null)
                    bot.getReservoirEndmember().setLsg(lsg);
                else{
                    Inversion.ModelDescription.BottomLayer.ReservoirEndmember res = new Inversion.ModelDescription.BottomLayer.ReservoirEndmember();
                    res.setLsg(lsg);
                    bot.setReservoirEndmember(re);
                }
            }else
            	bot.getReservoirEndmember().setLsg(null);
        }else
        	bot.getReservoirEndmember().setLsg(null);

        temp = timeBaseTextField.getText();
        float botBase = 0.0f;
        if (temp != null && temp.length() > 0) {
            botBase = Float.parseFloat(temp);
            bot.getTBase().setValue(botBase);
        }

        temp = sigmaTimeBaseTextField.getText();
        if (temp != null && temp.length() > 0) {
            vFloat = new VarFloat();
            vFloat.setValue(Float.parseFloat(temp));
            bot.getSigmaTBase().setValue(Float.parseFloat(temp));
        }

        //clear sigma depth fields
        top.setSigmaDepth(null);
        bot.setSigmaDepth(null);
        temp = masterDepthLayerTextField.getText();
        int layerNum = 1;
        if (temp != null && temp.length() > 0) {
            BigInteger bInt = new BigInteger(temp);
            layerNum = bInt.intValue();
            agent.getInversion().getOutput().setMasterDepthLayerNumber(bInt);
        }else{
            agent.getInversion().getOutput().setMasterDepthLayerNumber(new BigInteger(DeliveryLiteConstants.DEFAULT_MASTER_DEPTH_LAYER));
        }

        String sigmaDepth = errorOfMasterLayerDepthTextField.getText();
        float fSigmaDepth = 0;
        if(sigmaDepth != null && sigmaDepth.trim().length() > 0){
           fSigmaDepth = Float.parseFloat(sigmaDepth);
        }
        if(layerNum == 1){ //top layer
            VarFloat val = new VarFloat();
            val.setValue(fSigmaDepth);
            top.setSigmaDepth(val);
        }

        if(layerNum == tableSize){ //bottom layer
            VsFloat sval = new VsFloat();
            sval.setValue(fSigmaDepth);
            bot.setSigmaDepth(sval);
        }
        

        return err;
    }

    private String getVpGrain(){
        return (String)grainTable.getValueAt(0,0);
    }

    private String getVsGrain(){
        return (String)grainTable.getValueAt(0,1);
    }

    private String getRhoGrain(){
        return (String)grainTable.getValueAt(0,2);
    }

    private String getReferenceFluidVp(){
        return (String)referenceFluidTable.getValueAt(0,0);
    }

    private String getReferenceFluidRho(){
        return (String)referenceFluidTable.getValueAt(0,1);
    }

    private String getSandVPCurveSigmaFromSandTable(){
        return (String)referenceSandTable.getValueAt(0,0);
    }

    private String getSandVPCurveInterceptFromSandTable(){
        return (String)referenceSandTable.getValueAt(0,1);
    }

    private String getSandVPCurveDepthCoefficientFromSandTable(){
        return (String)referenceSandTable.getValueAt(0,2);
    }

    private String getSandVPCurveLFIVCoefficientFromSandTable(){
        return (String)referenceSandTable.getValueAt(0,3);
    }

    private String getSandVsCurveInterceptFromSandTable(){
        return (String)referenceSandTable.getValueAt(2,1);
    }

    private String getSandVsCurveSlopeFromSandTable(){
        return (String)referenceSandTable.getValueAt(2,2);
    }

    private String getSandVsCurveSigmaFromSandTable(){
        return (String)referenceSandTable.getValueAt(2,0);
    }

    private String getSandPorosityInterceptFromSandTable(){
        return (String)referenceSandTable.getValueAt(1,1);
    }

    private String getSandPorositySlopeFromSandTable(){
        return (String)referenceSandTable.getValueAt(1,2);
    }

    private String getSandPorositySigmaFromSandTable(){
        return (String)referenceSandTable.getValueAt(1,0);
    }



    private String getVPCurveSigmaFromTable(JTable table){
        return (String)table.getValueAt(0,0);
    }

    private String getVPCurveInterceptFromtTable(JTable table){
        return (String)table.getValueAt(0,1);
    }

    private String getVPCurveDepthCoefficientFromtTable(JTable table){
        return (String)table.getValueAt(0,2);
    }

    private String getVPCurveLFIVCoefficientFromTable(JTable table){
        return (String)table.getValueAt(0,3);
    }

    private String getVsCurveInterceptFromTable(JTable table){
        return (String)table.getValueAt(2,1);
    }

    private String getVsCurveSlopeFromTable(JTable table){
        return (String)table.getValueAt(2,2);
    }

    private String getVsCurveSigmaFromTable(JTable table){
        return (String)table.getValueAt(2,0);
    }

    private String getDensityCurveFactorFromTable(JTable table){
        return (String)table.getValueAt(1,1);
    }

    private String getDensityCurveExponentFromTable(JTable table){
        return (String)table.getValueAt(1,2);
    }

    private String getDensityCurveSigmaFromTable(JTable table){
        return (String)table.getValueAt(1,0);
    }

    private String getOilSaturationFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 6);
    }

    private String getSigmaOilSaturationFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 6);
    }

    private String getHighGasSaturationFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 7);
    }

    private String getSigmaHighGasSaturationFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 7);
    }

    private String getLowGasSaturationFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 8);
    }

    private String getSigmaLowGasSaturationFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 8);
    }
    //layer 0 means the first layer
    public String getTimeByLayer(int layer){
        return (String)layerTable.getValueAt(layer, 0);
    }

    public String getSigmaTimeByLayer(int layer){
        return (String)layerTable.getValueAt(layer, 1);
    }

    private String getBrineVpFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 0);
    }

    private String getBrineSigmaVpFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 0);
    }

    private String getBrineRhoFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 1);
    }

    private String getBrineSigmaRhoFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 1);
    }

    private String getOilVpFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 2);
    }

    private String getOilSigmaVpFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 2);
    }

    private String getOilRhoFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 3);
    }

    private String getOilSigmaRhoFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 3);
    }

    private String getGasVpFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 4);
    }

    private String getGasSigmaVpFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 4);
    }

    private String getGasRhoFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(0, 5);
    }

    private String getGasSigmaRhoFromFluidsTable(){
        TableModel model = fluidsTable2.getModel();
        return (String)model.getValueAt(1, 5);
    }


    private String validateModel(){
        boolean noChar = true;
        float botBase = 0;
        StringBuffer buf = new StringBuffer();
        String val = timeBaseTextField.getText().trim();
        if(val == null || val.length() == 0){
            buf.append("Time Base field is empty.\n");
        }else{
            noChar = noChar(val);
            if (!noChar) 
               buf.append("Invalid Number on Time Base field.\n");
            else
               botBase = Float.parseFloat(val);
        }

        val = sigmaTimeBaseTextField.getText().trim();
        if(val == null || val.length() == 0){
            buf.append("Sigma Time Base field is empty.\n");
        }else{
            noChar = noChar(val);
            if (!noChar) 
               buf.append("Invalid Number on Sigma Time Base field.\n");
        }

        val = errorOfMasterLayerDepthTextField.getText().trim();
        if(val == null || val.length() == 0){
            buf.append("Error of Master Layer Depth field is empty.\n");
        }else{
            noChar = noChar(val);
            if (!noChar) 
               buf.append("Invalid Number on Error of Master Layer Depth field.\n");
        }

        val = depthOfMasterLayerTextField.getText().trim();
        if(val == null || val.length() == 0){
            buf.append("Depth of Master Layer field is empty.\n");
        }else{
            noChar = noChar(val);
            if (!noChar) 
               buf.append("Invalid Number on Depth of Master Layer field.\n");
        }

        val = masterDepthLayerTextField.getText().trim();
        if(val == null || val.length() == 0){
            buf.append("Master Depth Layer field is empty.\n");
        }else{
            noChar = IntegerValidator.getInstance().isValid(val);
            if (!noChar) 
               buf.append("Invalid Number on Master Depth Layer field.\n");
        }

        String ret = buf.toString();
        if(ret.length() > 0)
            return ret;
        ret = validateLayers();
        if(ret.length() > 0)
            return ret;
        int botIndex = layerTable.getModel().getRowCount()-1;
        String temp = (String)Util.getValueAt(layerTable,botIndex,DeliveryLiteConstants.COLUMN_NAME_TIME);
        float botTime = Float.parseFloat(temp);
        if (botBase <= botTime) {
            ret = "Time Base must be > bottom layer Time\n";
        }
        return ret;
    }

    /*
     * Validate the layer model
     */
    public String validateLayers() {
        Vector fixedDataVector = ((DefaultTableModel)fixedLayerTable.getModel()).getDataVector();
        Map map = new HashMap<Object, Object>();
        int size = fixedDataVector.size();
        String ret = "";
        for (int i=0; i< size; i++) {
            Vector  vect = (Vector)fixedDataVector.get(i);
            //Object obj = vect.get(3);
            Object obj = fixedLayerTable.getValueAt(i, 3);
            if (obj != null && ((String)obj).length() > 0) {
              if (null != map.get(obj) ) {
                ret = "Duplicated name\n";
                break;
              } else {
                map.put(obj, "");
              }
            } else {
                ret = "Layer name is empty\n";
                break;
            }
        }
        //Get the Vector of Vectors  that contains the data values of the scrollable part of the layer table
        Vector dataVector = ((HorizonLineTableModel)layerTable.getModel()).getDataVector();

        //Merge the two data vectors into one. They are the same size, i.e., have the same number of rows.
        boolean noChar = true;
        StringBuffer buf = new StringBuffer();
        for (int i=0; i< size; i++) {
           Vector  vect = (Vector)dataVector.get(i);
           //for (int j = 0; j < 16; j++) {
           for (int j = 0; j < layerTable.getColumnCount(); j++) {
               //for (int j = 0; j < 14; j++) {
               //Object obj = vect.get(j);
               Object obj = layerTable.getValueAt(i, j);
               if (obj != null) {
                   String st = obj.toString();
                  noChar = noChar(st.trim());
               }
               if (!noChar)  break;
           }
           if (!noChar) {
               buf.append("Invalid Number\n");
               break;
           }
        }

        if(buf.length() > 0){
        	ret += buf.toString();
        	return ret;
        }
        
        if (valEmpty(0)) {
            ret += "Time Field Empty\n";
        } else {
            if (!valInc(0)) {
                ret += "Layer Time fields not incremental\n";
            }
        }

        if (valEmpty(1))
            ret += "Sigma Time Field Empty\n";

        if (valEmpty(4))
            ret += "TVDBML Field Empty\n";

        //if (valEmpty(6)) {

        if (valEmpty(5)) {
            ret += "LFIV Field Empty\n";
        }

        if (valEmpty(6)) {
            ret += "N/G Field Empty\n";
        }

        if(Util.valNotBtn0And1(layerTable, DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH))
            ret += "N/G Field value must be between 0 and 1\n";
        if(Util.valNotBtn0And1(layerTable, DeliveryLiteConstants.COLUMN_NAME_SIGMA_NG))
            ret += "err N/G Field value must be between 0 and 1\n";
        if(Util.valNotBtn0And1(layerTable, DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_OIL))
            ret += "Po Field value must be between 0 and 1\n";
        if(Util.valNotBtn0And1(layerTable, DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_GAS))
            ret += "Pg Field value must be between 0 and 1\n";
        if(Util.valNotBtn0And1(layerTable, DeliveryLiteConstants.COLUMN_NAME_POSSIBILITY_LSG))
            ret += "Plsg Field value must be between 0 and 1\n";
        
        int rowCount = fixedLayerTable.getModel().getRowCount();
        StringBuilder bb = new StringBuilder();
        for(int i =0; i < rowCount; i++){
        	String lType = (String)Util.getValueAt(fixedLayerTable,i,DeliveryLiteConstants.COLUMN_NAME_TYPE);
        	String ng = (String)Util.getValueAt(layerTable,i,DeliveryLiteConstants.COLUMN_NAME_NET_TO_GROWTH);
        	float fng = Float.valueOf(ng).floatValue();
        	if(lType.equals(SHALE_LAYER_TYPE) && !Util.floatEqualZero(fng)){
        		bb.append("N/G should be 0 when layer type is shale.");
        		break;
        	}
        	if(lType.equals(SAND_LAYER_TYPE) && Util.floatEqualZero(fng)){
        		bb.append("N/G should not be 0 when layer type is sand.");
        		break;
        	}
        }
        ret += bb.toString();
        return ret;
    }

    private String getLayerType(float ng, float sng) {
       if (Util.floatEqualZero(ng) && Util.floatEqualZero(sng))
           return SHALE_LAYER_TYPE;

       return SAND_LAYER_TYPE;
    }

    private boolean noChar(String obj) {
        if (obj == null || obj.length() == 0) return true;
        return FloatValidator.getInstance().isValid(obj);
    }

    
    private boolean valEmpty(int index) {
        boolean isEmpty = false;
        Vector dataVector = ((DefaultTableModel)layerTable.getModel()).getDataVector();

        for (int i=0; i< dataVector.size(); i++) {
          Vector  vect = (Vector)dataVector.get(i);
          //String  timeF = (String)vect.get(index);
          String  timeF = (String)layerTable.getValueAt(i, index);
          if (timeF == null || timeF.length() == 0) {
            isEmpty = true;
             break;
          }
        }
        return isEmpty;

    }

    private boolean valInc(int index) {
        boolean isInc = true;
        Vector dataVector = ((DefaultTableModel)layerTable.getModel()).getDataVector();
        float currVal = 0.0f;
        float val     = 1.0f;
        for (int i=0; i< dataVector.size(); i++) {

          Vector  vect = (Vector)dataVector.get(i);
          //String  timeF = (String)vect.get(index);
          String  timeF = (String)layerTable.getValueAt(i, index);
          val = Float.parseFloat(timeF);
          if (val < currVal) {
             isInc = false;
             break;
          } else {
              currVal = val;
          }
        }
        return isInc;

    }

    private String validateParams() {
        stopTableCellEditing();
        String err = "";
        err += validateModel();
        err += validateFluidsTable();
        err += validateMatrix();
        err += validateStack();
        return err;
    }

    private void stopTableCellEditing(){
        if(referenceSandTable.getCellEditor() != null)
            referenceSandTable.getCellEditor().stopCellEditing();
        if(mixingShaleTable.getCellEditor() != null)
            mixingShaleTable.getCellEditor().stopCellEditing();
        if(grainTable.getCellEditor() != null)
            grainTable.getCellEditor().stopCellEditing();
        if(boundingShaleTable.getCellEditor() != null)
            boundingShaleTable.getCellEditor().stopCellEditing();
        if(referenceFluidTable.getCellEditor() != null)
            referenceFluidTable.getCellEditor().stopCellEditing();
        if(fluidsTable1.getCellEditor() != null)
            fluidsTable1.getCellEditor().stopCellEditing();
        if(fluidsTable2.getCellEditor() != null)
            fluidsTable2.getCellEditor().stopCellEditing();
        if(stackParametersTable.getCellEditor() != null)
            stackParametersTable.getCellEditor().stopCellEditing();
        if(fixedLayerTable.getCellEditor() != null)
            fixedLayerTable.getCellEditor().stopCellEditing();
        if(layerTable.getCellEditor() != null)
            layerTable.getCellEditor().stopCellEditing();
        if(referenceSandTable.getCellEditor() != null)
            referenceSandTable.getCellEditor().stopCellEditing();
    }


    private String validateFluidsTable(){
        StringBuffer err = new StringBuffer();
        for(int row = 0; row < 2; row++){
            for(int column = 0; column < 9; column++){
                String value = (String)fluidsTable2.getValueAt(row, column);
                if(row == 0){
                    if(value == null || value.trim().length() == 0){
                        err.append("Missing value field in the Fluids table");
                        break;
                    }
                }
                boolean noChar = true;
                if (value != null) {
                    noChar = noChar(value);
                    if (!noChar){
                        err.append("Invalid number field found the Fluids table");
                        break;
                    }
                }
            }
        }
        return err.toString();
    }

    private void inputTabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {
        int index = inputsTabbedPane.getSelectedIndex();
        if (index != inputIndex) {
            String err = validatePane(inputIndex);
            if (index == 8 || index == 9) {
                err += validateLiteParams();
            }
            if (err.length() > 0) {
               JOptionPane.showMessageDialog(this, err);
               return;
            } else {
                inputIndex = index;
            }
//Note: Each time enter the Output tab, the filenames will be reset even
//      if the user edits them.
//            if (index == 2) {
//                autoSetIndex2();
//            } else
            if (index == 7) {
                modelView.setText(agent.toXmlStrInversion());
            } else if (index == 8) {
                String template = Util.getTemplate("/xml/invert_template.txt");
                String scripts  = formScripts(template);
                inversionView.setText(scripts);
            } else if (index == 9) {
                String template = Util.getTemplate("/xml/analyzer_template.txt");
                String scripts  = formScripts(template);
                analyzerView.setText(scripts);
            }
        }
    }
/*
    private void autoSetIndex2() {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        String time = formatter.format(ts);
        String tmp = outputPriorRealisationsFilenameTextField.getText().trim();
        if (tmp.length() == 0) {
            String prior = "PRIOR_" + time;
            outputPriorRealisationsFilenameTextField.setText(prior);
        }

        tmp = outputPostRealisationsFIlenameTextField.getText().trim();
        if (tmp.length() == 0) {
            String post = "POST_" + time;
            outputPostRealisationsFIlenameTextField.setText(post);
        }
    }
*/
    private String validatePane(int index) {
        String err = "";
        if (1 == index) {
            err = validateInput();
        } else if (4 == index){
            err = validateMatrix();
        } else if (5 == index) {
            err = validateFluid();
        } else if (6 == index) {
            err = validateStack();
        }
        return err;
    }

    private String validateInput() {
        String err = "";
        List<Inversion.SeismicData.Stack> stacks = agent.getInversion().getSeismicData().getStack();
        int size = stacks.size();
        if (size < 1) return "No Stack Data";
        Inversion.SeismicData.Stack stack = stacks.get(0);
        String temp = null;
        temp =nearStackFilenameTextField.getText();
        if (temp.length() == 0) {
            err += "Near stack file name is empty\n";
        } else {
            stack.setName(temp);
        }
        temp = nearStackWaveletFilenameTextField.getText();
        if (temp.length() == 0) {
            err += "Near wavelet file name is empty\n";
        } else {
            stack.getWavelet().setFilename(temp);
        }

        temp = nearNoiseRMSTextField.getText();
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Near Wavelet Signal-to-Noise\n";
        } else {
            stack.getWavelet().getNoiseRms().setValue(Float.parseFloat(temp));
        }



        Inversion.SeismicData.AVOTerms avoTerm = agent.getInversion().getSeismicData().getAVOTerms();
        temp = avoATextField.getText();
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid AVO-A\n";
        } else {
            avoTerm.getA().setValue(Float.parseFloat(temp));
        }

        temp = avoBTextField.getText();
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid AVO-B\n";
        } else {
            avoTerm.getB().setValue(Float.parseFloat(temp));
        }

        return err;
    }

    private String validateStack() {
        String err = "";
        List<Inversion.SeismicData.Stack> stacks = agent.getInversion().getSeismicData().getStack();
        int size = stacks.size();
        if (size < 1) return "No Stack Data";
        if(nearOnlyRadioButton.isSelected())
            size = 1;
        Vector dataVector = ((DefaultTableModel)stackParametersTable.getModel()).getDataVector();
        for(int i = 0; i < size; i++){
            Vector  vect = (Vector)dataVector.get(i);

            Inversion.SeismicData.Stack stack = stacks.get(i);
            String temp = null;
            temp = (String)vect.get(0);
            if (temp == null || temp.length() == 0 || !noChar(temp)) {
                err += "Invalid XMin\n";
            } else {
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(temp));
                stack.setMinOffset(vf);
            }

            temp = (String)vect.get(1);
            if (temp == null || temp.length() == 0 || !noChar(temp)) {
                err += "Invalid XMax\n";
            } else {
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(temp));
                stack.setMaxOffset(vf);
            }

            temp = (String)vect.get(2);
            if (temp == null || temp.length() == 0 || !noChar(temp)) {
                err += "Invalid Stack Velocity\n";
            } else {
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(temp));
                stack.setStackVelocity(vf);
            }

            temp = (String)vect.get(3);
            if (temp == null || temp.length() == 0 || !noChar(temp)) {
                err += "Invalid Replection Time\n";
            } else {
                VarFloat vf = new VarFloat();
                vf.setValue(Float.parseFloat(temp));
                stack.setReflectorTime(vf);
            }
        }
        if(err.length() > 0){
            deliveryLiteTabbedPane.setSelectedIndex(0);
            inputsTabbedPane.setSelectedIndex(6);
        }
        return err;

    }

    private String validateMatrix() {
        String temp, err="";
        List<Inversion.RockFluidProperties.RockProperties.ReservoirEndmember> sands
             = agent.getInversion().getRockFluidProperties().getRockProperties().getReservoirEndmember();
        int size = sands.size();
        Vector dataVector = ((DefaultTableModel)referenceSandTable.getModel()).getDataVector();
        Vector  vect = (Vector)dataVector.get(0);
        Inversion.RockFluidProperties.RockProperties.ReservoirEndmember sand = sands.get(0);
        temp = (String)vect.get(0);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp e\n";
        } else {
            sand.getVpCurve().getSigma().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(1);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp A\n";
        } else {
            sand.getVpCurve().getIntercept().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(2);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp B\n";
        } else {
            sand.getVpCurve().getDepthCoefficient().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(3);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp C\n";
        } else {
            sand.getVpCurve().getLFIVCoefficient().setValue(Float.parseFloat(temp));
        }

        vect = (Vector)dataVector.get(1);
        temp = (String)vect.get(0);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Porosity e\n";
        } else {
            sand.getPorosityCurve().getSigma().setValue(Float.parseFloat(temp));
        }

        temp = (String)vect.get(1);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Porosity A\n";
        } else {
            sand.getPorosityCurve().getIntercept().setValue(Float.parseFloat(temp));
        }

        temp = (String)vect.get(2);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Porosity B\n";
        } else {
            sand.getPorosityCurve().getSlope().setValue(Float.parseFloat(temp));
        }


        vect = (Vector)dataVector.get(2);
        temp = (String)vect.get(0);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vs e \n";
        } else {
            sand.getVsCurve().getSigma().setValue(Float.parseFloat(temp));
        }

        temp = (String)vect.get(1);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vs A \n";
        } else {
            sand.getVsCurve().getIntercept().setValue(Float.parseFloat(temp));
        }

        temp = (String)vect.get(2);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vs B \n";
        } else {
            sand.getVsCurve().getSlope().setValue(Float.parseFloat(temp));
        }

//      grain
        dataVector = ((DefaultTableModel)grainTable.getModel()).getDataVector();
        vect = (Vector)dataVector.get(0);
        temp = (String)vect.get(0);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Grain Vp\n";
        } else {
            sand.getVpGrain().setValue(Float.parseFloat(temp));
        }

        temp = (String)vect.get(1);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Grain Vs\n";
        } else {
            sand.getVsGrain().setValue(Float.parseFloat(temp));
        }

        temp = (String)vect.get(2);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Grain Rho\n";
        } else {
            sand.getRhoGrain().setValue(Float.parseFloat(temp));
        }

        List<Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember> shales
             = agent.getInversion().getRockFluidProperties().getRockProperties().getNonreservoirEndmember();
        size = shales.size();

        if (size > 0) {
            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember shale = shales.get(0);
            err += validateShale(shale, boundingShaleTable);
            if (size > 1) {
                shale = shales.get(1);
                err += validateShale(shale, mixingShaleTable);
            }
        }

        //reference fluid
        temp = (String)referenceFluidTable.getValueAt(0,0);
        if(temp == null || temp.trim().length() == 0)
            err += "missing value in the vp field of Reference Fluid table.";
        else if(!noChar(temp)){
            err += "Invalid number value in the vp field of Reference Fluid table.";
        }

        temp = (String)referenceFluidTable.getValueAt(0,1);
        if(temp == null || temp.trim().length() == 0)
            err += "missing value in the rho field of Reference Fluid table.";
        else if(!noChar(temp)){
            err += "Invalid number value in the rho field of Reference Fluid table.";
        }
        return err;
    }

    private String validateShale(Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember shale, JTable table) {
        String temp = null, err="";
        Vector dataVector = ((DefaultTableModel)table.getModel()).getDataVector();
        Vector  vect = (Vector)dataVector.get(0);
        temp = (String)vect.get(0);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp e\n";
        } else {
            shale.getVpCurve().getSigma().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(1);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp A\n";
        } else {
            shale.getVpCurve().getIntercept().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(2);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp B\n";
        } else {
            shale.getVpCurve().getDepthCoefficient().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(3);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vp C\n";
        } else {
            shale.getVpCurve().getLFIVCoefficient().setValue(Float.parseFloat(temp));
        }


        vect = (Vector)dataVector.get(1);
        temp = (String)vect.get(0);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Density e\n";
        } else {
            shale.getDensityCurve().getSigma().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(1);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Density A\n";
        } else {
            shale.getDensityCurve().getFactor().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(2);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Density B\n";
        } else {
            shale.getDensityCurve().getExponent().setValue(Float.parseFloat(temp));
        }


        vect = (Vector)dataVector.get(2);
        temp = (String)vect.get(0);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vs e\n";
        } else {
            shale.getVsCurve().getSigma().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(1);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vs A\n";
        } else {
            shale.getVsCurve().getIntercept().setValue(Float.parseFloat(temp));
        }
        temp = (String)vect.get(2);
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Vs B\n";
        } else {
            shale.getVsCurve().getSlope().setValue(Float.parseFloat(temp));
        }


        return err;
    }

    private String validateFluid() {
        String temp = null, err="";
        List<Inversion.RockFluidProperties.FluidProperties.Fluid> fluids
            = agent.getInversion().getRockFluidProperties().getFluidProperties().getFluid();
        Inversion.RockFluidProperties.FluidProperties.Fluid fluid;

        Vector dataVector = ((DefaultTableModel)fluidsTable2.getModel()).getDataVector();
        Vector  vect = (Vector)dataVector.get(0);
        Vector  vect1 = (Vector)dataVector.get(1);

        //error codes
        String invalidBrine = "Invalid Brine ", invalidBrineSigma = "Invalid Brine Sigma";
        String invalidOil = "Invalid Oil ", invalidOilSigma = "Invalid Oil Sigma";
        String invalidGas = "Invalid Gas ", invalidGasSigma = "Invalid Gas Sigma";
        String invalidLsg = "Invalid LSG ", invalidLsgSigma = "Invalid LSG Sigma";
        String invalidRefFluid = "Invalid Reference Fluid ";
        String invalidVp = "Vp\n", invalidRho = "Density\n";
        String invalidFluid = "", invalidFluidSigma = "";

        int size = fluids.size();
        for (int i=0; i<size; i++) {
            fluid = fluids.get(i);
            int j = -1;
            String fluidName = fluid.getName();
            if (fluidName.equals("brine")) {
                j = 0;
                invalidFluid = invalidBrine;
                invalidFluidSigma = invalidBrineSigma;
            }
            else if (fluidName.equals("oil")) {
                j = 2;
                invalidFluid = invalidOil;
                invalidFluidSigma = invalidOilSigma;
            }
            else if (fluidName.equals("gas")) {
                j = 4;
                invalidFluid = invalidGas;
                invalidFluidSigma = invalidGasSigma;
            }
            else if (fluidName.equals("lsg")) {
                j = 6;
                invalidFluid = invalidLsg;
                invalidFluidSigma = invalidLsgSigma;
            }
            else if (fluidName.equals("reference fluid")) {
                j = 8;
                invalidFluid = invalidRefFluid;
            }

            if (j == -1) continue;

            if (j == 8) {
                dataVector = ((DefaultTableModel)referenceFluidTable.getModel()).getDataVector();
                Vector v = (Vector)dataVector.get(0);
                temp = (String)v.get(0);
                if (temp.length() == 0 || !noChar(temp)) {
                    err += invalidFluid + invalidVp;
                } else {
                    fluid.getVp().setValue(Float.parseFloat(temp));
                }

                temp = (String)v.get(1);
                if ((temp != null && temp.length() == 0) || !noChar(temp)) {
                    err += invalidFluid + invalidRho;
                } else {
                    fluid.getRho().setValue(Float.parseFloat(temp));
                }
                continue;
            }

            fluid = fluids.get(i);
            temp = (String)vect.get(j);
            if (temp.length() == 0 || !noChar(temp)) {
                err += invalidFluid + invalidVp;
            } else {
                fluid.getVp().setValue(Float.parseFloat(temp));
            }

            temp = (String)vect.get(j+1);
            if ((temp != null && temp.length() == 0) || !noChar(temp)) {
                err += invalidFluid + invalidRho;
            } else {
                fluid.getRho().setValue(Float.parseFloat(temp));
            }

            if (fluid.getSigmaVp() != null) {
                temp = (String)vect1.get(j);
                if ((temp != null && temp.length() == 0) || !noChar(temp)) {
                    err += invalidFluidSigma + invalidVp;
                } else {
                    fluid.getSigmaVp().setValue(Float.parseFloat(temp));
                }
            }
            if (fluid.getSigmaRho() != null) {
                temp = (String)vect1.get(j+1);
                if ((temp != null && temp.length() == 0) || !noChar(temp)) {
                    err += invalidFluidSigma + invalidRho;
                } else {
                    fluid.getSigmaRho().setValue(Float.parseFloat(temp));
                }
            }
        }

        return err;
    }

    public void updateLiteParams(LiteParams lite) {
        //outputPriorRealisationsFilenameTextField.setText(lite.getPriorReal());
        //outputPostRealisationsFIlenameTextField.setText(lite.getPostReal());
        outputBaseNameTextField.setText(lite.getOutputBaseName());
        numberRunsTextField.setText(lite.getNumRuns());
        modelParams.setNumberRuns(Integer.valueOf(lite.getNumRuns()).intValue());
        String val = lite.getTmin();
        tmin4SyntheticSeismicTextField.setText(val);
        modelParams.setSyntheticSeismicStartTime(Float.valueOf(val).floatValue());
        val = lite.getTmax();
        tmax4SyntheticSeismicTextField.setText(val);
        modelParams.setSyntheticSeismicEndTime(Float.valueOf(val).floatValue());
        val = lite.getTmaxSpaghettiPlot();
        tmax4SpaghettiPlotTextField.setText(val);
        modelParams.setSpaghettiPlotStartTime(Float.valueOf(val).floatValue());
        val = lite.getTminSpaghettiPlot();
        tmin4SpaghettiPlotTextField.setText(val);
        modelParams.setSpaghettiPlotEndTime(Float.valueOf(val).floatValue());
        val = lite.getEp();
        epTextField.setText(val);
        modelParams.setSingleInline(Float.valueOf(val).floatValue());
        val = lite.getCdp();
        cdpTextField.setText(val);
        modelParams.setSingleXline(Float.valueOf(val).floatValue());
        //nearOnlyRadioButton.setEnabled(lite.isNear());
        if (lite.isNear()) {
            nearOnlyRadioButton.setSelected(true);
            farStackFilenameTextField.setEditable(false);
            farStackWaveletFilenameTextField.setEditable(false);
            farNoiseRMSTextField.setEditable(false);
        } else {
            nearAndFarRadioButton.setSelected(true);
            farStackFilenameTextField.setEditable(true);
            farStackWaveletFilenameTextField.setEditable(true);
            farNoiseRMSTextField.setEditable(true);
        }
    }

    public String validateLiteParams() {
        String err = "";

        String temp = nearStackFilenameTextField.getText();
        if (temp != null && temp.trim().length() == 0) {
            err += "Near stack file name is empty\n";
        } else {
            liteParams.setFileNearStack(temp);
        }

        temp = nearStackFilenameTextField.getText();
        if (temp != null && temp.trim().length() == 0) {
            err += "Near wavelet file name is empty\n";
        } else {
            liteParams.setNearFileWavelet(temp);
        }

        temp = nearNoiseRMSTextField.getText();
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid Near Wavelet Signal-to-Noise\n";
        } 

        temp = avoATextField.getText();
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid AVO-A\n";
        }

        temp = avoBTextField.getText();
        if (temp.length() == 0 || !noChar(temp)) {
            err += "Invalid AVO-B\n";
        } 
        
        if(nearAndFarRadioButton.isSelected()){
            temp = farStackFilenameTextField.getText();
            if (temp != null && temp.trim().length() == 0) {
                err += "Far stack file name is empty\n";
            } else {
                liteParams.setFileFarStack(temp);
            }
            temp = farStackWaveletFilenameTextField.getText();
            if (temp != null && temp.trim().length() == 0) {
                err += "Far wavelet file name is empty\n";
            } else {
                liteParams.setFarFileWavelet(temp);
            }
            
            temp = farNoiseRMSTextField.getText();
            if (temp.length() == 0 || !noChar(temp)) {
                err += "Invalid Far Wavelet Signal-to-Noise\n";
            } 

        }

        temp = outputBaseNameTextField.getText().trim();
        if (temp.length() == 0) {
            err += "Output file name is empty\n";
        } else {
            liteParams.setOutputBaseName(temp);
        }

        temp = numberRunsTextField.getText();
        if (temp.length() == 0) {
            err += "No Number of Runs Field\n";
        } else {
            if (!noChar(temp)) {
                err += "Number of Runs Field should be integer\n";
            } else {
                liteParams.setNumRuns(temp);
            }
        }
        temp = tmin4SyntheticSeismicTextField.getText();
        if (temp.length() == 0) {
            err += "tmin field for Synthetic Seismic is empty\n";
        } else {
            if (!noChar(temp)) {
                err += "tmin field for Synthetic Seismic should be digit number\n";
            } else {
                liteParams.setTmin(temp);
            }
        }

        temp = tmax4SyntheticSeismicTextField.getText();
        if (temp.length() == 0) {
            err += "tmax field for Synthetic Seismic is empty\n";
        } else {
            if (!noChar(temp)) {
                err += "tmax field for Synthetic Seismic should be digit number\n";
            } else {
                liteParams.setTmax(temp);
            }
        }

        temp = tmin4SpaghettiPlotTextField.getText();
        if (temp.length() == 0) {
            err += "tmin field for Spagetti Plot is empty\n";
        } else {
            if (!noChar(temp)) {
                err += "tmin field for Spagetti Plot should be digit number\n";
            } else {
                liteParams.setTmin(temp);
            }
        }

        temp = tmax4SpaghettiPlotTextField.getText();
        if (temp.length() == 0) {
            err += "tmax field for Spagetti Plot is empty\n";
        } else {
            if (!noChar(temp)) {
                err += "tmax field for Spagetti Plot should be digit number\n";
            } else {
                liteParams.setTmax(temp);
            }
        }


        temp = epTextField.getText();
        if (temp.length() == 0) {
            err += "ep field is empty\n";
        } else {
            if (!noChar(temp)) {
                err += "ep field should be digit number\n";
            } else {
                if(!(nearRangeInfo[0][0] <= Double.parseDouble(temp) && nearRangeInfo[0][1] >= Double.parseDouble(temp)))
                    err += "ep field should be within the ep range of the near stack.\n";
                else
                    liteParams.setEp(temp);
            }
        }

        temp = cdpTextField.getText();
        if (temp.length() == 0) {
            err += "cdp field is empty\n";
        } else {
            if (!noChar(temp)) {
                err += "cdp field should be digit number\n";
            } else {
                if(!(nearRangeInfo[1][0] <= Double.parseDouble(temp) && nearRangeInfo[1][1] >= Double.parseDouble(temp)))
                    err += "cdp field should be within the cdp range of the near stack.\n";
                else
                    liteParams.setCdp(temp);
            }
        }
        err += infoPane.validateParams();

        return err;
    }

    /**
     * Write out the XML model file. The model is encapsulated in the
     * Inversion object.
     * @param fileName Pathname of the XML model file to create.
     */
    private boolean toXmlInversionFile(String filePath) {
        Inversion inversion = agent.getInversion();
        if (inversion == null) return false;
        try {
          JAXBContext context
             = JAXBContext.newInstance("com.bhpb.xsdparams.delivery",
                                       this.getClass().getClassLoader());

          Marshaller marshaller = context.createMarshaller();
          marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.valueOf(true));
          if (filePath == null) {
        	  filePath = agent.getModelDir() + XML_MODEL_FILE;
          }

          //String tempModelFilepath = agent.getModelDir() + "dl_temp_Model.xml";
          //Write out the XML file to a temporary file, then transform to the Delivery model
          //marshaller.marshal(inversion, new FileOutputStream(tempModelFilepath));
          //marshaller.marshal(inversion, new FileOutputStream(fileName));
          DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
          Document document = builder.newDocument();

          marshaller.marshal(inversion,document);
          String content = Util.nodeToXMLString(document);
          if(agent.writeStringToFile(filePath, content))
        		return true;
          //For DeliveryLite, the top layer element is <top> which needs to
          //be changed to <layer> so Delivery can input the model.
          //NOTE: Changing the first <layer> to <top> is an artifact of JAXB that
          //can be gotten around if we wrote our own XML parser.
          //LiteToDeliveryModel(tempModelFilepath, fileName);
        //} catch (QiwIOException qioe) {
            //TODO notify user cannot write XML model
        //    logger.finest(qioe.getMessage());
        } catch (Exception ex) {
            //TODO notify user cannot write XML model
            ex.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * Get DOM from an XML, locating the file locally only if it is not an
     * applet.
     *
     * @param xmlFileName
     *            the name of the file to open
     * @param return
     *            a DOM Document for the XML in "xmlFileName"
     */
    public Document createDOMforXML(String xmlFileName) {
        //String contents = readLocalFile(xmlFileName);
    	String contents = agent.getXMLStringByXMLFilePath(xmlFileName);
        InputSource is = new InputSource(new StringReader(contents));
        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            return builder.parse(is);
        } catch (SAXException sxe) {
            sxe.printStackTrace();
            return null;
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
            // Parser with specified options can't be built
            return null;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Reads the contents of a local file and returns them as a String
     *
     * @param filename
     *            the name of the file whose contents will be retrieved
     * @return the contents of "filename" as a String
     */
    public String readLocalFile(String filename) {
        try {
            InputStream is = new FileInputStream(filename);
            if (is == null) {
                return null;
            }
            StringWriter out = new StringWriter();
            int c;

            while ((c = is.read()) != -1)
                out.write(c);

            is.close();
            return out.toString();
        } catch (Exception e) {
            System.err.println("reading exception for " + filename + ": "
                    + e.toString());
        }
        return null;
    }


//  This method writes a DOM document to a file
    private static void writeXmlFile(Document doc, String filename) {
        try {
            // Prepare the DOM document for writing
            Source source = new DOMSource(doc);

            // Prepare the output file
            File file = new File(filename);
            Result result = new StreamResult(file);

            // Write the DOM document to the file
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
        } catch (TransformerException e) {
        }
    }

    /**
     * Transform a Delivery Lite XML model to a Delivery XML model. That is,
     * change the <top> in the <model_description> to <layer>. If it is
     * already <layer>, then the XMl model is a Delivery one.
     * @param modelFilepath Full path of the XML model file to be written.
     * @return Full path of the XML model file.
     */
    private void LiteToDeliveryModel(String tempModelFilepath, String modelFilepath) throws QiwIOException {
        //Read temporary XML model file until find <model_description>
        File modelIn = new File(tempModelFilepath);

        if (!modelIn.exists()) throw new QiwIOException("specified file does not exist; path="+modelFilepath);

        if (!modelIn.isFile()) throw new QiwIOException("specified file is not a file; path="+modelFilepath);

        File modelOut = new File(modelFilepath);
        BufferedWriter bw = null;
        BufferedReader br = null;
        try {
            FileInputStream fis = new FileInputStream(modelIn);
            br = new BufferedReader(new InputStreamReader(fis));
            FileOutputStream fos = new FileOutputStream(modelOut);
            bw = new BufferedWriter(new OutputStreamWriter(fos));

            String line = br.readLine();
            // read file line by line
            while (line != null) {
                if (line.indexOf("<model_description>") != -1) {
                    bw.write(line);
                    bw.newLine();   //write the line separator

                    //check if next line is <top>
                    line = br.readLine();
                    if (line != null && line.indexOf("<top>") != -1) {
                        line = line.replace("<top>", "<layer>");
                        bw.write(line);
                        bw.newLine();   //write the line separator

                        //find matching </top> and change to </layer>
                        line = br.readLine();
                        boolean skipCheck = false;
                        while (line != null) {
                            if (!skipCheck && line.indexOf("</top>") != -1) {
                                line = line.replace("</top>", "</layer>");
                                bw.write(line);
                                bw.newLine();   //write the line separator
                                skipCheck = true;
                            } else {
                                //write out line as is
                                bw.write(line);
                                bw.newLine();   //write the line separator
                            }
                            line = br.readLine();
                        }
                    }
                } else {
                    //write out line as is
                    bw.write(line);
                    bw.newLine();   //write the line separator
                }

                line = br.readLine();
            }
        } catch (IOException ioe) {
            throw new QiwIOException("IO exception reading/writing XML model file:"+ioe.getMessage());
        } finally {
            try {
                if (br != null) br.close();
                if (bw != null) bw.close();
            } catch (IOException e) {}
        }

        return;
    }

    /**
     * Generate and write out the Delivery script to the project's script directory (temp).
     * @return Pathname prefix of the script (i.e., path without .sh) if file successully
     * written; otherwise, null.
     */
    private String writeInversionScript(String templateName, String baseName, String type, String stackType) {
        String template = Util.getTemplate(templateName);
        //String scripts  = formScripts(template);
        String scripts  = formScripts1(template,type,stackType);

        String scriptDir = QiProjectDescUtils.getTempPath(agent.getQiProjectDescriptor());
        //scriptDir = CommonUtil.getAppDir(CommonUtil.HOME_DIR) + File.separator + "scripts";

        String modelName = modelNameTextField.getText().trim();
        modelName= resolveBaseName(modelName);
        String fileNamePrefix = Util.createTimeStampedScriptPrefix(baseName + "_" + type + "_" + stackType + "_" + modelName);
        String fileName = fileNamePrefix + ".sh";
        String filesep = agent.getMessagingMgr().getServerOSFileSeparator();
        boolean success = agent.writeStringToFile(scriptDir + filesep + fileName, scripts);
        if (success)
            //return scriptDir + File.separator+ fileNamePrefix;
        	return fileNamePrefix;
        else
            return null;

    }


    private String resolveBaseName(String base){
    	String [] part = base.split(" +");
        if(part != null && part.length > 0){
        	StringBuffer buf = new StringBuffer();
        	for(int i = 0; i < part.length; i++){
        		buf.append(part[i]);
        		if(i < part.length-1)
        			buf.append("_");
        	}
        	return buf.toString();
        }else
        	return base;
    }

    /**
     * Generate and write out the Delivery script to the project's script directory (temp).
     * @return Pathname prefix of the script (i.e., path without .sh) if file successully
     * written; otherwise, null.
     */
    private String writeInversionScript(String templateName, String inversionName) {
        String template = Util.getTemplate(templateName);
        String scripts  = formScripts(template);
        String scriptDir = QiProjectDescUtils.getTempPath(agent.getQiProjectDescriptor());
        //scriptDir = CommonUtil.getAppDir(CommonUtil.HOME_DIR) + File.separator + "scripts";
        String result = agent.checkFileExist(scriptDir);
        boolean ok = false;
        if (result.equals("yes"))
            ok = true;
        else if (result.equals("no")) {
            int status = JOptionPane.showConfirmDialog(this,
                            "The script will be written into " + scriptDir
                          + " but the directory does not exist. Would you like the application to create it for you?",
                            "Confirm action", JOptionPane.YES_NO_OPTION);
            if (status == JOptionPane.YES_OPTION) {
                result = agent.mkDir(scriptDir);
                if (result.equals("success"))
                    ok = true;
            } else
                return null;
        }

        if (ok) {
            //Timestamp ts = new Timestamp(System.currentTimeMillis());
            String modelName = modelNameTextField.getText().trim();
            String [] part = modelName.split(" +");
            if(part != null && part.length > 0){
            	StringBuffer buf = new StringBuffer();
            	for(int i = 0; i < part.length; i++){
            		buf.append(part[i]);
            		if(i < part.length-1)
            			buf.append("_");
            	}
            	modelName = buf.toString();
            }
            String fileNamePrefix = Util.createTimeStampedScriptPrefix(inversionName + "_" + modelName);
            String fileName = fileNamePrefix + ".sh";
            boolean success = agent.writeStringToFile(scriptDir + File.separator + fileName, scripts);
            if (success)
                return scriptDir + File.separator+ fileNamePrefix;
            else
                return null;
        } else
            return null;
    }


    /**
     * Generate and write out the Analyzer script to the project's script directory (temp).
     * @return Pathname prefix of the script (i.e., path without .sh) if file successully
     * written; otherwise, null.
     */
    private String writeAnalyzerScript() {
        String template = Util.getTemplate("/xml/analyzer_template.txt");
        String scripts  = formScripts(template);
        String scriptDir = QiProjectDescUtils.getTempPath(agent.getQiProjectDescriptor());
        //scriptDir = CommonUtil.getAppDir(CommonUtil.HOME_DIR) + File.separator + "scripts";
        String result = agent.checkFileExist(scriptDir);
        boolean ok = false;
        if (result.equals("yes"))
            ok = true;
        else if (result.equals("no")) {
            int status = JOptionPane.showConfirmDialog(this,
                            "The script will be written into " + scriptDir
                          + " but the directory does not exist. Would you like the application to create it for you?",
                            "Confirm action", JOptionPane.YES_NO_OPTION);
            if (status == JOptionPane.YES_OPTION) {
                result = agent.mkDir(scriptDir);
                if (result.equals("success"))
                    ok = true;
            } else
                return null;
        }

        if (ok) {
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            String fileNamePrefix = analyzerName + modelNameTextField.getText().trim() + formatter.format(ts);
            String fileName = fileNamePrefix + ".sh";
            boolean success = agent.writeStringToFile(scriptDir + File.separator + fileName, scripts);
            if (success)
                return scriptDir + File.separator + fileNamePrefix;
            else
                return null;
        } else
            return null;
    }

    /**
     * Generate Delivery / Analyzer script from a template.
     * @param templ Delivery / Analyzer template
     * @return Generated script
     */
    private String formScripts(String templ) {
        //Determine the list of layers that have Analyze selected
        FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
        int numLayers = fixedTableModel.getRowCount();
        StringBuffer analyzeList = new StringBuffer("(");
        for (int i=0; i<numLayers; i++) {
            analyzeList.append(fixedTableModel.isAnalyze(i) ? "yes" : "no");
            if (i != numLayers-1) analyzeList.append(", ");
        }
        analyzeList.append(")");

        //NOTE: It is assumed BHP_SU and SU are on the end-users PATH
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();

        String template = templ;
        String qiSpace = QiProjectDescUtils.getQiSpace(projDesc);
        String qiProject = QiProjectDescUtils.getQiProjectReloc(projDesc);
        String qiDatasets = QiProjectDescUtils.getDatasetsReloc(projDesc);
        String qiSeismicDir = QiProjectDescUtils.getSeismicDirs(projDesc).get(0);

        String modelDir   = infoPane.getModelDir();
        //NOTE: For a Delivery model, the path of the wavelet, near stack and far
        //      stack files may be absolute and not relative to a qiProject. Either
        //      form the filepaths relative to the project's Seismic directory or
        //      leave as an absolute path.
        String waveletFilepath = nearStackWaveletFilenameTextField.getText();
        if (!waveletFilepath.startsWith("/"))
            waveletFilepath = qiSeismicDir + "/" + waveletFilepath;
        String nearStackFilepath = nearStackFilenameTextField.getText();
        if (!nearStackFilepath.startsWith("/"))
            nearStackFilepath = qiSeismicDir + "/" + nearStackFilepath;
        String farStackFilepath = farStackFilenameTextField.getText();
        if (!farStackFilepath.startsWith("/"))
            farStackFilepath = qiSeismicDir + "/" + farStackFilepath;

        template = template.replace("<QISPACE>",        qiSpace);
        template = template.replace("<PROJECT_DIR>",    qiProject);
        template = template.replace("<DATASETS_DIR>",   qiDatasets);
        template = template.replace("<SEISMIC_DIR>",    qiSeismicDir);

        template = template.replace("<MODEL_DIR>",      modelDir);
        template = template.replace("<XML_MODEL>",      XML_MODEL_FILE);
        template = template.replace("<NUM_RUNS>",       liteParams.getNumRuns());
        template = template.replace("<MIN_EP>",         liteParams.getEp());
        template = template.replace("<MIN_CDP>",        liteParams.getCdp());
        template = template.replace("<START_TIME>",     liteParams.getTmin());
        template = template.replace("<END_TIME>",       liteParams.getTmax());
        template = template.replace("<NAME_POST_REAL>", liteParams.getPostReal());
        template = template.replace("<NAME_PRIOR_REAL>",liteParams.getPriorReal());
        template = template.replace("<DATASETS_DIR>",   QiProjectDescUtils.getDatasetsReloc(projDesc));
        String invOutDir = infoPane.getInversionDir();
        template = template.replace("<INV_OUT_DIR>",    invOutDir);
        template = template.replace("<NEAR_STACK_FILEPATH>",    nearStackFilepath);
        template = template.replace("<FAR_STACK_FILEPATH>",     farStackFilepath);
        template = template.replace("<WAVELET_FILEPATH>",   waveletFilepath);
        String analyzerOutDir = infoPane.getAnalyzerDir();
        template = template.replace("<OUTPUT_DIR>",   analyzerOutDir);
        if (nearOnlyRadioButton.isSelected()) {
            template = template.replace("<NEAR_FAR_FLAG>",  "N");
        } else {
            template = template.replace("<NEAR_FAR_FLAG>", "Y");
        }
        template = template.replace("<LITE_LAYERS>", analyzeList);
        return template;
    }


    /**
     * Generate DeliveryLite / prior vs post script from a template.
     * @param templ DeliveryLite template script
     * @return Generated script
     */
    private String formScripts1(String templ, String runType, String stackType) {
        //NOTE: It is assumed BHP_SU and SU are on the end-users PATH
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();

        String template = templ;
        String qiSpace = QiProjectDescUtils.getQiSpace(projDesc);
        String qiProject = QiProjectDescUtils.getQiProjectReloc(projDesc);
        String qiDatasets = QiProjectDescUtils.getDatasetsReloc(projDesc);
        String qiSeismicDir = QiProjectDescUtils.getSeismicDirs(projDesc).get(0);
        String qiTmpDir = QiProjectDescUtils.getTempReloc(projDesc);
        //String modelDir   = infoPane.getModelDir();
        //NOTE: For a Delivery model, the path of the wavelet, near stack and far
        //      stack files may be absolute and not relative to a qiProject. Either
        //      form the filepaths relative to the project's Seismic directory or
        //      leave as an absolute path.
        String nearWaveletFilename = null;
        String farWaveletFilename = null;
        
        nearWaveletFilename = nearStackWaveletFilenameTextField.getText().trim();
        if(!stackType.equals("near"))
        	farWaveletFilename = farStackWaveletFilenameTextField.getText().trim();
        
        String nearStackFilename = nearStackFilenameTextField.getText().trim();
        String farStackFilename = null;
        //if (!nearStackFilepath.startsWith("/"))
        //    nearStackFilepath = qiSeismicDir + "/" + nearStackFilepath;
        if(!stackType.equals("near"))
        	farStackFilename = farStackFilenameTextField.getText().trim();

        template = template.replace("<QI_SPACE>",        qiSpace);
        template = template.replace("<QI_PROJECT_LOCATION>",    qiProject);
        template = template.replace("<QI_DATASETS>",   qiDatasets);
        template = template.replace("<QI_SEISMICS>",    qiSeismicDir);
        template = template.replace("<QI_TMP>",    qiTmpDir);

        //template = template.replace("<MODEL_DIR>",      modelDir);
        if(runType.equals("prior"))
        	template = template.replace("<XML_MODEL>",      XML_MODEL_FILE_PRIOR);
        else if(runType.equals("post"))
        	template = template.replace("<XML_MODEL>",      XML_MODEL_FILE_POST);
        else
        	template = template.replace("<XML_MODEL>",      XML_MODEL_FILE);
        template = template.replace("<XML_MODEL>",      XML_MODEL_FILE);
        template = template.replace("<NUMBER_REALIZATIONS>", liteParams.getNumRuns());
        template = template.replace("<EP>",         liteParams.getEp());
        template = template.replace("<CDP>",        liteParams.getCdp());
        template = template.replace("<MIN_TIME>",     liteParams.getTmin());
        template = template.replace("<MAX_TIME>",       liteParams.getTmax());
        template = template.replace("<RUN_TYPE>",       runType);
        template = template.replace("<STACK_TYPE>",       stackType);
        String invOutDir = infoPane.getInversionDir();
        template = template.replace("<INV_OUT_DIR>",    invOutDir);
        template = template.replace("<NEAR_STACK_NAME>",    nearStackFilename);
        template = template.replace("<NEAR_WAVELET_NAME>",   nearWaveletFilename);
        if(nearAndFarRadioButton.isSelected()){
        	template = template.replace("<FAR_STACK_NAME>",    farStackFilename);
        	template = template.replace("<FAR_WAVELET_NAME>",   farWaveletFilename);
        }
        //String analyzerOutDir = infoPane.getAnalyzerDir();
        String outBaseName = getOutputBasename();
        template = template.replace("<OUTPUT_BASENAME>",   outBaseName);
        if (nearOnlyRadioButton.isSelected()) {
            template = template.replace("<NEAR_FAR_FLAG>",  "N");
            template = template.replace("<FAR_STACK>",     "");
            template = template.replace("<FAR_WAVELET>",   "");
        } else {
            template = template.replace("<NEAR_FAR_FLAG>", "Y");
            String farStackFilepath = farStackFilenameTextField.getText().trim();
            template = template.replace("<FAR_STACK>",     farStackFilepath);
            String farWaveletFilepath = nearStackWaveletFilenameTextField.getText().trim();
            template = template.replace("<FAR_WAVELET>",   farWaveletFilepath);
        }
        //template = template.replace("<LITE_LAYERS>", analyzeList);
        return template;
    }

    private void writeModelScriptActionPerformed(java.awt.event.ActionEvent evt) {
        String ret = validateParams();
        ret += validateLiteParams();
        if (ret.length() == 0) {
           //createLayers();
           //updateFluidsPropertiesInfo();
            updateInversionObject();
           toXmlInversionFile(null);
        } else {
          JOptionPane.showMessageDialog(this, ret);
          return;
        }

        writeInversionScript("/xml/invert_template.txt",invertName);
    }

    private void updateFluidsPropertiesInfo(){
        List<Inversion.RockFluidProperties.FluidProperties.Fluid> fluids = agent.getInversion().getRockFluidProperties().getFluidProperties().getFluid();
        if(fluids == null)
            fluids = new ArrayList<Inversion.RockFluidProperties.FluidProperties.Fluid>();
        Inversion.RockFluidProperties.FluidProperties.Fluid fluid = new Inversion.RockFluidProperties.FluidProperties.Fluid();
        ArrayList<String> defaultFluids = (ArrayList<String>)DeliveryLiteConstants.DEFAULT_FLUID_LIST.clone();
        VarFloat vf;
        for(int i = 0; i < fluids.size(); i++){
            fluid = fluids.get(i);
            if(fluid.getName().equals(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME)){
                String vp = getBrineVpFromFluidsTable();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String svp = getBrineSigmaVpFromFluidsTable();
                if(svp != null && svp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(svp));
                    fluid.setSigmaVp(vf);
                }
                String rho = getBrineRhoFromFluidsTable();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                String srho = getBrineSigmaRhoFromFluidsTable();
                if(srho != null && srho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(srho));
                    fluid.setSigmaRho(vf);
                }
                defaultFluids.remove(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME);
            }else if(fluid.getName().equals(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME)){
                String vp = getOilVpFromFluidsTable();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String svp = getOilSigmaVpFromFluidsTable();
                if(svp != null && svp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(svp));
                    fluid.setSigmaVp(vf);
                }
                String rho = getOilRhoFromFluidsTable();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                String srho = getOilSigmaRhoFromFluidsTable();
                if(srho != null && srho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(srho));
                    fluid.setSigmaRho(vf);
                }
                defaultFluids.remove(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME);
            }else if(fluid.getName().equals(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME)){
                String vp = getGasVpFromFluidsTable();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String svp = getGasSigmaVpFromFluidsTable();
                if(svp != null && svp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(svp));
                    fluid.setSigmaVp(vf);
                }
                String rho = getGasRhoFromFluidsTable();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                String srho = getGasSigmaRhoFromFluidsTable();
                if(srho != null && srho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(srho));
                    fluid.setSigmaRho(vf);
                }
                defaultFluids.remove(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME);
            }else if(fluid.getName().equals(DeliveryLiteConstants.DEFAULT_REFERENCE_FLUID_NAME)){
                String vp = getReferenceFluidVp();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String rho = getReferenceFluidRho();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                defaultFluids.remove(DeliveryLiteConstants.DEFAULT_REFERENCE_FLUID_NAME);
            }
        }

        for(int i = 0; i < defaultFluids.size(); i++){
            String name = defaultFluids.get(i);
            if(name.equals(DeliveryLiteConstants.DEFAULT_FLUID_BRINE_NAME)){
                String vp = getBrineVpFromFluidsTable();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String svp = getBrineSigmaVpFromFluidsTable();
                if(svp != null && svp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(svp));
                    fluid.setSigmaVp(vf);
                }
                String rho = getBrineRhoFromFluidsTable();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                String srho = getBrineSigmaRhoFromFluidsTable();
                if(srho != null && srho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(srho));
                    fluid.setSigmaRho(vf);
                }
                fluids.add(fluid);
            }else if(name.equals(DeliveryLiteConstants.DEFAULT_FLUID_OIL_NAME)){
                String vp = getOilVpFromFluidsTable();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String svp = getOilSigmaVpFromFluidsTable();
                if(svp != null && svp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(svp));
                    fluid.setSigmaVp(vf);
                }
                String rho = getOilRhoFromFluidsTable();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                String srho = getOilSigmaRhoFromFluidsTable();
                if(srho != null && srho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(srho));
                    fluid.setSigmaRho(vf);
                }
                fluids.add(fluid);
            }else if(name.equals(DeliveryLiteConstants.DEFAULT_FLUID_GAS_NAME)){
                String vp = getGasVpFromFluidsTable();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String svp = getGasSigmaVpFromFluidsTable();
                if(svp != null && svp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(svp));
                    fluid.setSigmaVp(vf);
                }
                String rho = getGasRhoFromFluidsTable();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                String srho = getGasSigmaRhoFromFluidsTable();
                if(srho != null && srho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(srho));
                    fluid.setSigmaRho(vf);
                }
                fluids.add(fluid);
            }else if(fluid.getName().equals(DeliveryLiteConstants.DEFAULT_REFERENCE_FLUID_NAME)){
                String vp = getReferenceFluidVp();
                if(vp != null && vp.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(vp));
                    fluid.setVp(vf);
                }
                String rho = getReferenceFluidRho();
                if(rho != null && rho.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(rho));
                    fluid.setRho(vf);
                }
                fluids.add(fluid);
            }
        }
        currentOilSaturation = getOilSaturationFromFluidsTable();
        currentHighGasSaturation = getHighGasSaturationFromFluidsTable();
        currentLowGasSaturation = getLowGasSaturationFromFluidsTable();
        currentOilSigmaSaturation = getSigmaOilSaturationFromFluidsTable();
        currentHighGasSigmaSaturation = getSigmaHighGasSaturationFromFluidsTable();
        currentLowGasSigmaSaturation = getSigmaLowGasSaturationFromFluidsTable();
    }

    private void updateRockPropertiesInfo(){
        updateResvoirRockPropertiesInfo();
        updateNonResvoirRockPropertiesInfo();
    }

    private void updateResvoirRockPropertiesInfo(){
        List<Inversion.RockFluidProperties.RockProperties.ReservoirEndmember> res = agent.getInversion().getRockFluidProperties().getRockProperties().getReservoirEndmember();
        if(res == null)
            res = new ArrayList<Inversion.RockFluidProperties.RockProperties.ReservoirEndmember>();
        Inversion.RockFluidProperties.RockProperties.ReservoirEndmember re = new Inversion.RockFluidProperties.RockProperties.ReservoirEndmember();

        VarFloat vf;
        ArrayList<String> defaultResList = (ArrayList<String>)DeliveryLiteConstants.DEFAULT_RESERVOIR_ENDMEMBER_LIST.clone();
        for(int i = 0; i < res.size(); i++){
            re = res.get(i);
            if(re.getName().equals(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME)){

                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VpCurve vc = re.getVpCurve();
                if(vc == null)
                    vc = new Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VpCurve();

                String value = getSandVPCurveInterceptFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setIntercept(vf);
                }

                value = getSandVPCurveLFIVCoefficientFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setLFIVCoefficient(vf);
                }
                value = getSandVPCurveDepthCoefficientFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setDepthCoefficient(vf);
                }
                value = getSandVPCurveSigmaFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setSigma(vf);
                }
                re.setVpCurve(vc);

                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VsCurve vs = re.getVsCurve();
                if(vs == null)
                    vs = new Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VsCurve();
                value = getSandVsCurveSigmaFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSigma(vf);
                }

                value = getSandVsCurveSlopeFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSlope(vf);
                }

                value = getSandVsCurveInterceptFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setIntercept(vf);
                }
                re.setVsCurve(vs);

                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.PorosityCurve pc = re.getPorosityCurve();
                if(pc == null)
                    pc = new Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.PorosityCurve();
                value = getSandPorosityInterceptFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    pc.setIntercept(vf);
                }

                value = getSandPorositySlopeFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    pc.setSlope(vf);
                }

                value = getSandPorositySigmaFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    pc.setSigma(vf);
                }
                re.setPorosityCurve(pc);

                value = getVpGrain();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    re.setVpGrain(vf);
                }
                value = getVsGrain();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    re.setVsGrain(vf);
                }
                value = getRhoGrain();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    re.setRhoGrain(vf);
                }
                defaultResList.remove(re.getName());
            }
        }

        for(int i = 0; i < defaultResList.size(); i++){
            String name = defaultResList.get(i);
            if(name.equals(DeliveryLiteConstants.DEFAULT_ROCK_RESERVOIR_ENDMEMBER_SAND_NAME)){
                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VpCurve vc = new
                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VpCurve();

                String value = getSandVsCurveInterceptFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setIntercept(vf);
                }

                value = getSandVPCurveLFIVCoefficientFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setLFIVCoefficient(vf);
                }
                value = getSandVPCurveDepthCoefficientFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setDepthCoefficient(vf);
                }
                value = getSandVPCurveSigmaFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setSigma(vf);
                }
                re.setVpCurve(vc);

                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VsCurve vs = new
                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.VsCurve();

                value = getSandVsCurveSigmaFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSigma(vf);
                }

                value = getSandVsCurveSlopeFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSlope(vf);
                }

                value = getSandVsCurveInterceptFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setIntercept(vf);
                }
                re.setVsCurve(vs);

                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.PorosityCurve pc = new
                Inversion.RockFluidProperties.RockProperties.ReservoirEndmember.PorosityCurve();

                value = getSandPorosityInterceptFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    pc.setIntercept(vf);
                }

                value = getSandPorositySlopeFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    pc.setSlope(vf);
                }

                value = getSandPorositySigmaFromSandTable();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    pc.setSigma(vf);
                }
                re.setPorosityCurve(pc);

                value = getVpGrain();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    re.setVpGrain(vf);
                }
                value = getVsGrain();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    re.setVsGrain(vf);
                }
                value = getRhoGrain();
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    re.setRhoGrain(vf);
                }
                res.add(re);
            }
        }
    }

    private void updateNonResvoirRockPropertiesInfo(){
        List<Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember> nres = agent.getInversion().getRockFluidProperties().getRockProperties().getNonreservoirEndmember();
        if(nres == null)
            nres = new ArrayList<Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember>();


        VarFloat vf;
        ArrayList<String> defaultNonResList = (ArrayList<String>)DeliveryLiteConstants.DEFAULT_NONRESERVOIR_ENDMEMBER_LIST.clone();
        for(int i = 0; i < nres.size(); i++){
            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember nre = nres.get(i);
            if(nre.getName().equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME) ||
                    nre.getName().equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME)){
                JTable table = null;
                if(nre.getName().equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME))
                    table = mixingShaleTable;
                else
                    table = boundingShaleTable;

                Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VpCurve vc = nre.getVpCurve();
                if(vc == null)
                    vc = new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VpCurve();

                String value = getVPCurveInterceptFromtTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setIntercept(vf);
                }

                value = getVPCurveLFIVCoefficientFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setLFIVCoefficient(vf);
                }
                value = getVPCurveDepthCoefficientFromtTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setDepthCoefficient(vf);
                }
                value = getVPCurveSigmaFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setSigma(vf);
                }
                nre.setVpCurve(vc);

                Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VsCurve vs = nre.getVsCurve();
                if(vs == null)
                    vs = new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VsCurve();
                value = getVsCurveSigmaFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSigma(vf);
                }

                value = getVsCurveSlopeFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSlope(vf);
                }

                value = getVsCurveInterceptFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setIntercept(vf);
                }
                nre.setVsCurve(vs);

                Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.DensityCurve dc = nre.getDensityCurve();
                if(dc == null)
                    dc = new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.DensityCurve();
                value = getDensityCurveExponentFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    dc.setExponent(vf);
                }

                value = getDensityCurveFactorFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    dc.setFactor(vf);
                }

                value = getDensityCurveSigmaFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    dc.setSigma(vf);
                }
                nre.setDensityCurve(dc);
                defaultNonResList.remove(nre.getName());
            }
        }

        for(int i = 0; i < defaultNonResList.size(); i++){
            String name = defaultNonResList.get(i);
            Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember nre =
                new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember();
            nre.setName(name);
            if(name.equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME) ||
                    name.equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SHALE_NAME)){
                JTable table = null;
                if(name.equals(DeliveryLiteConstants.DEFAULT_ROCK_NONRESERVOIR_ENDMEMBER_SAND_NAME))
                    table = mixingShaleTable;
                else
                    table = boundingShaleTable;

                Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VpCurve vc = nre.getVpCurve();
                if(vc == null)
                    vc = new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VpCurve();

                String value = getVPCurveInterceptFromtTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setIntercept(vf);
                }

                value = getVPCurveLFIVCoefficientFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setLFIVCoefficient(vf);
                }
                value = getVPCurveDepthCoefficientFromtTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setDepthCoefficient(vf);
                }
                value = getVPCurveSigmaFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vc.setSigma(vf);
                }
                nre.setVpCurve(vc);

                Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VsCurve vs = nre.getVsCurve();
                if(vs == null)
                    vs = new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.VsCurve();
                value = getVsCurveSigmaFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSigma(vf);
                }

                value = getVsCurveSlopeFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setSlope(vf);
                }

                value = getVsCurveInterceptFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    vs.setIntercept(vf);
                }
                nre.setVsCurve(vs);

                Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.DensityCurve dc = nre.getDensityCurve();
                if(dc == null)
                    dc = new Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember.DensityCurve();
                value = getDensityCurveExponentFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    dc.setExponent(vf);
                }

                value = getDensityCurveFactorFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    dc.setFactor(vf);
                }

                value = getDensityCurveSigmaFromTable(table);
                if(value != null && value.trim().length() > 0){
                    vf = new VarFloat();
                    vf.setValue(Float.parseFloat(value));
                    dc.setSigma(vf);
                }
                nre.setDensityCurve(dc);
                nres.add(nre);
            }
        }
    }



    private void writeDeliveryScriptActionPerformed(java.awt.event.ActionEvent evt) {
        String err = validateParams();
        err += validateLiteParams();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
            return;
        }

        writeInversionScript("/xml/invert_template.txt",invertName);
    }

    private void writeAnalyzerScriptActionPerformed(java.awt.event.ActionEvent evt) {
        String err = validateParams();
        err += validateLiteParams();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
            return;
        }

        writeAnalyzerScript();
    }



    public void validateProject() {
        File tmpFile = null;
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        String qiSpace = QiProjectDescUtils.getQiSpace(projDesc);
        String tmp   = QiProjectDescUtils.getQiProjectReloc(projDesc);
        String fullProj = qiSpace;

        if (!File.separator.equals(tmp)) {
            fullProj += tmp;
            tmpFile = new File(fullProj);
            if (!tmpFile.exists()) {
                if (!tmpFile.mkdir()) {
                    JOptionPane.showMessageDialog(this, "Not able to create project directory");
                }
            }
        }

        tmp   = QiProjectDescUtils.getScriptsReloc(projDesc);
        validateDir(fullProj, tmp);

        tmp   = QiProjectDescUtils.getTempReloc(projDesc);
        validateDir(fullProj, tmp);

        tmp   = QiProjectDescUtils.getSeismicDirs(projDesc).get(0);
        validateDir(fullProj, tmp);

        tmp = infoPane.getInversionDir();
        validateDir(fullProj, tmp);

        tmp = infoPane.getModelDir();
        validateDir(fullProj, tmp);
    }

    private void validateDir(String projectDir, String relDir) {
        String fullPath = projectDir + File.separator + relDir;
        File tmpFile = new File(fullPath);
        if (!tmpFile.exists()) {
            if (!tmpFile.mkdir()) {
                JOptionPane.showMessageDialog(this, "Not able to create directory: " + fullPath);
            }
        }
    }

    /** @deprecated Old model's middle layers are now removed in populateModel() */
    private void delAndUpdateMiddleLayers() {
        DefaultTableModel tableModel = (DefaultTableModel)layerTable.getModel();
        FixedLayerTableModel fixedTableModel = (FixedLayerTableModel)fixedLayerTable.getModel();
        int size = fixedTableModel.getRowCount();
        if (size > 2) {
            int cnt = size - 2;
            for (int i = 0; i < cnt; i++) {
                tableModel.removeRow(1);
                fixedTableModel.deleteRow(1);
            }
            updateModel();
            updateTopBottom();
            fixedTableModel.renumberRows();
        }
    }

    /**
     * Gets the pathname of a file to use to save the currently open XML model.
     *
     * @return The pathname of a file to use to save the current XML model
     */
    private String getSaveFile() {
        QiProjectDescriptor projDesc = agent.getQiProjectDescriptor();
        JFileChooser chooser = new JFileChooser(QiProjectDescUtils.getTempPath(projDesc));
        chooser.setDialogTitle("Export Model ...");
        chooser.setAcceptAllFileFilterUsed(true);
        EditorFileFilter filter = new EditorFileFilter(
                new String[] {"xml", "XML"}, "Model Description File");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            return f.getAbsolutePath();
        } else {
            return null;
        }
    }

    public LiteParams getLiteParams() {
        return liteParams;
    }

    public void setLiteParams(LiteParams liteParams) {
        this.liteParams = liteParams;
    }

    private void importModelRunParams(java.awt.event.ActionEvent evt) {
    	String [] filterList = { ".xml",".XML" };
        GenericFileFilter filter =  new GenericFileFilter(filterList,"Model & Params File (*.xml)");
        String startDir = QiProjectDescUtils.getXmlPath(agent.getQiProjectDescriptor());
        QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Select Model & Params xml file",startDir,filter,true,
                true,false,
                javax.swing.JFileChooser.OPEN_DIALOG,
                DeliveryLiteConstants.GET_MODEL_PARAMS_XML_FILE_CMD);
        agent.callFileChooser(desc);
    }


    public GeoFileMetadata getMetadataByDataset(String dataset){
        if(metadataMap.containsKey(dataset)){
            return metadataMap.get(dataset);
        }
        return null;
    }

    public IDataObject[] getDataObjectsByDataset(String dataset){
        if(dataMap.containsKey(dataset)){
            return dataMap.get(dataset);
        }
        return null;
    }
    private void exportModelRunParams(java.awt.event.ActionEvent evt) {
        String err = validateParams();
        err += validateLiteParams();
        if (err.length() > 0) {
            JOptionPane.showMessageDialog(this, err);
            return;
        } else {
        	String [] filterList = { ".xml",".XML" };
            GenericFileFilter filter =  new GenericFileFilter(filterList,"Model & Params File (*.xml)");
            String startDir = QiProjectDescUtils.getXmlPath(agent.getQiProjectDescriptor());
            QiFileChooserDescriptor desc = formQiFileChooserDescriptor("Save Model & Params xml file",startDir,filter,false,
                    true,false,
                    javax.swing.JFileChooser.SAVE_DIALOG,
                    DeliveryLiteConstants.SAVE_MODEL_PARAMS_XML_FILE_CMD);
            agent.callFileChooser(desc);
        }
    }

    private void importRunParams(String fileName) {
        String baseFileName = getFileBase(fileName);
        agent.getRunParams(baseFileName);
        this.updateLiteParams(liteParams);
    }


    public void importModelAndParams(String filePath){
    	if(showModelDialog != null && showModelDialog.isVisible()){
    		showModelDialog.dispose();
    		showModelDialog.getNearInternalFrame().getStackPanel().clearDataMap();
    		showModelDialog.getFarInternalFrame().getStackPanel().clearDataMap();
    	}
    	showModelDialog = null;
    	showModelParams = new ShowModelParams();
		modelAfterDatasetIndex = 0;
	    modelAfterPropertyIndex = 0;
    	Document doc = createDOMforXML(filePath);
    	boolean isValid = agent.checkValidDeliveryLiteDocument(doc,false);
    	
        Node inversionNode = agent.getInversionNode(doc.getFirstChild());
        agent.getModel(inversionNode);
        resetHorizonMarkerDataStructure();
        resetModel();
        if(isValid)
    		restoreRunParamState(doc.getFirstChild());
    	else{
    		JOptionPane.showMessageDialog(this, "The xml document does not appear to be a valid Model & Run Parameters document.", "Invalid Document Found", JOptionPane.WARNING_MESSAGE);
    		return;
    	}
    }

    /**
     * Read in the XML model.
     * @fileName The path of the XML model file.
     */
    public void importModel(String fileName, Inversion inversion) {
        agent.setModelXML(fileName);
        resetTitle(null);
        if(inversion == null)
            agent.getModel(true);
        resetHorizonMarkerDataStructure();
        resetModel();
        ((DefaultTableModel)layerTable.getModel()).fireTableDataChanged();
   }

    private void resetHorizonMarkerDataStructure(){
        HorizonLineTableModel hTableModel = (HorizonLineTableModel)layerTable.getModel();
        if(hTableModel != null)
            hTableModel.resetRowLineAttributes();
        if(tableDecorator != null)
            tableDecorator.initRowHorizonMarkerMap();
    }

    public void resetModel() {
        initTableParams();
        //resetHorizonMarkerDataStructure();
        populate();
        populateTable();

        //clear model name, number of runs, tmin, tmax, ep and cdp
        Inversion inversion = (Inversion)agent.getInversion();

        modelNameTextField.setText(inversion.getInversionInfo().getName());

        //update stack
        DefaultTableModel tableModel = (DefaultTableModel)stackParametersTable.getModel();
        Vector record = (Vector)tableModel.getDataVector().get(0);
        record.set(0, stackParams[0][0]);
        record.set(1, stackParams[0][1]);
        record.set(2, stackParams[0][2]);
        record.set(3, stackParams[0][3]);
        tableModel.fireTableDataChanged();
        int size = agent.getInversion().getSeismicData().getStack().size();
        if (size > 1) {
            record = (Vector)tableModel.getDataVector().get(1);
            record.set(0, stackParams[1][0]);
            record.set(1, stackParams[1][1]);
            record.set(2, stackParams[1][2]);
            record.set(3, stackParams[1][3]);
            tableModel.fireTableDataChanged();
        }

        //update fluids
        tableModel = (DefaultTableModel)fluidsTable2.getModel();
        record = (Vector)tableModel.getDataVector().get(0);
        //set values
        record.set(0, fluidsParams[0][0]);
        record.set(1, fluidsParams[0][1]);
        record.set(2, fluidsParams[0][2]);
        record.set(3, fluidsParams[0][3]);
        record.set(4, fluidsParams[0][4]);
        record.set(5, fluidsParams[0][5]);
        record.set(6, fluidsParams[0][6]);
        record.set(7, fluidsParams[0][7]);
        record.set(8, fluidsParams[0][8]);
        //record.set(9, fluidsParams[0][9]);
        //record.set(10, fluidsParams[0][10]);
        //set sigmas
        record = (Vector)tableModel.getDataVector().get(1);
        record.set(0, fluidsParams[1][0]);
        record.set(1, fluidsParams[1][1]);
        record.set(2, fluidsParams[1][2]);
        record.set(3, fluidsParams[1][3]);
        record.set(4, fluidsParams[1][4]);
        record.set(5, fluidsParams[1][5]);
        record.set(6, fluidsParams[1][6]);
        record.set(7, fluidsParams[1][7]);
        record.set(8, fluidsParams[1][8]);
        //record.set(9, fluidsParams[1][9]);
        //record.set(10, fluidsParams[1][10]);

        tableModel.fireTableDataChanged();

        //update reference sands
        tableModel = (DefaultTableModel)referenceSandTable.getModel();
        record = (Vector)tableModel.getDataVector().get(0);
        record.set(0, refSandParams[0][0]);
        record.set(1, refSandParams[0][1]);
        record.set(2, refSandParams[0][2]);
        record.set(3, refSandParams[0][3]);
        record = (Vector)tableModel.getDataVector().get(1);
        record.set(0, refSandParams[1][0]);
        record.set(1, refSandParams[1][1]);
        record.set(2, refSandParams[1][2]);
        record = (Vector)tableModel.getDataVector().get(2);
        record.set(0, refSandParams[2][0]);
        record.set(1, refSandParams[2][1]);
        record.set(2, refSandParams[2][2]);
        tableModel.fireTableDataChanged();

        //update mixing shale
        tableModel = (DefaultTableModel)mixingShaleTable.getModel();
        record = (Vector)tableModel.getDataVector().get(0);
        record.set(0, mixShaleParams[0][0]);
        record.set(1, mixShaleParams[0][1]);
        record.set(2, mixShaleParams[0][2]);
        record.set(3, mixShaleParams[0][3]);
        record = (Vector)tableModel.getDataVector().get(1);
        record.set(0, mixShaleParams[1][0]);
        record.set(1, mixShaleParams[1][1]);
        record.set(2, mixShaleParams[1][2]);
        record = (Vector)tableModel.getDataVector().get(2);
        record.set(0, mixShaleParams[2][0]);
        record.set(1, mixShaleParams[2][1]);
        record.set(2, mixShaleParams[2][2]);
        tableModel.fireTableDataChanged();

        //update bounding shale
        tableModel = (DefaultTableModel)boundingShaleTable.getModel();
        record = (Vector)tableModel.getDataVector().get(0);
        record.set(0, boundShaleParams[0][0]);
        record.set(1, boundShaleParams[0][1]);
        record.set(2, boundShaleParams[0][2]);
        record.set(3, boundShaleParams[0][3]);
        record = (Vector)tableModel.getDataVector().get(1);
        record.set(0, boundShaleParams[1][0]);
        record.set(1, boundShaleParams[1][1]);
        record.set(2, boundShaleParams[1][2]);
        record = (Vector)tableModel.getDataVector().get(2);
        record.set(0, boundShaleParams[2][0]);
        record.set(1, boundShaleParams[2][1]);
        record.set(2, boundShaleParams[2][2]);
        tableModel.fireTableDataChanged();

        //update grain table
        tableModel = (DefaultTableModel)grainTable.getModel();
        record = (Vector)tableModel.getDataVector().get(0);
        record.set(0, grainParams[0][0]);
        record.set(1, grainParams[0][1]);
        record.set(2, grainParams[0][2]);
        tableModel.fireTableDataChanged();

        //update reference fluid
        tableModel = (DefaultTableModel)referenceFluidTable.getModel();
        record = (Vector)tableModel.getDataVector().get(0);
        record.set(0, refFluidParams[0][0]);
        record.set(1, refFluidParams[0][1]);
        tableModel.fireTableDataChanged();
        populatePropertyColorMap();
    }


    /**
     * Strip off [.xml, .XML] of an XML file or path of an XML file.
     * @param xmlFile Name or pathname of an XML file.
     * @return Name or pathname of an XML file with [.xml, .XML] stripped off
     */
    private String getFileBase(String xmlFile) {
        String ret = "";
        int index = xmlFile.indexOf(".xml");
        if (index < 0) {
            index = xmlFile.indexOf(".XML");
        }

        return index < 0 ? xmlFile : xmlFile.substring(0, xmlFile.length()-4);
    }

    public void showScriptPanels() {
        boolean show = infoPane.showChecked();
        if (show) {
            inputsTabbedPane.addTab("Model XML", modelView.getBasePanel());
            inputsTabbedPane.addTab("Inversion Script", inversionView.getBasePanel());
            inputsTabbedPane.addTab("Analyzer Script",  analyzerView.getBasePanel());
        } else {
            inputsTabbedPane.remove(modelView.getBasePanel());
            inputsTabbedPane.remove(inversionView.getBasePanel());
            inputsTabbedPane.remove(analyzerView.getBasePanel());
        }
    }

    private String hasError() {
        String err = validateParams();
        err += validateLiteParams();
        return err;
    }

    /** @deprecated Use saveState() instead. */
    private void saveAll() {
        //createLayers();
        //updateFluidsPropertiesInfo();
        //updateRockPropertiesInfo();
        updateInversionObject();
        String modelFile = agent.getModelXML();
        if (modelFile == null) {
           modelFile = agent.getModelDir() + "delivDefault.xml";
           agent.setModelXML(modelFile);
           resetTitle(null);
        }
        toXmlInversionFile(modelFile);
        agent.saveRunParams(modelFile);
    }

    /**
     * Set the name of the model. Used when restoring state.
     * @param name Name of the model.
     */
    public void setModelName(String name) {
        modelNameTextField.setText(name);
    }
}
