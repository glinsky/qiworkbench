/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007-2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.deliverylite.displayResults;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bhpb.deliverylite.DeliveryLiteUI;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;

/**
 * Create pathnames of all datasets used to display DeliveryLite results in the
 * qiViewer.
 * <p>
 * Generated file names have the following structure:<br>
 * <pre>    <output_prefix>_<post | prior>_<near | far>_<name></pre><br>
 * where <pre><name> ::= seismic | seismic_ml | ml | median_model</pre>
 *
 * @author Gil Hansen, LT Li
 * @version 1.0
 */

public class DatasetParams {
    public static final String NEAR_WAVELET = "near.wavelet";
    public static final String FAR_WAVELET = "far.wavelet";
    public static final String NEAR_STACK = "near.stack";
    public static final String FAR_STACK = "far.stack";

    public static final String POST_NEAR_SEISMIC = "post.near.seismic";
    public static final String PRIOR_NEAR_SEISMIC = "prior.near.seismic";
    public static final String PRIOR_NEAR_SEISMIC_ML = "prior.near.seismic.ml";
    public static final String POST_NEAR_SEISMIC_ML = "post.near.seismic.ml";
    public static final String POST_NEAR_SEISMIC_REAL = "post.near.seismic.realizations";
    public static final String PRIOR_NEAR_SEISMIC_REAL = "prior.near.seismic.realizations";
    public static final String POST_NEAR_MEDIAN_MODEL = "post.near.media.model";
    public static final String PRIOR_NEAR_MEDIAN_MODEL = "prior.near.median.model";
    public static final String POST_FAR_MEDIAN_MODEL = "post.far.media.model";
    public static final String PRIOR_FAR_MEDIAN_MODEL = "prior.far.median.model";
    public static final String POST_NEAR_REAL = "post.near.realizations";
    public static final String PRIOR_NEAR_REAL = "prior.near.realizations";
    public static final String POST_FAR_REAL = "post.far.realizations";
    public static final String PRIOR_FAR_REAL = "prior.far.realizations";
    public static final String PRIOR_NEAR_ML = "prior.near.ml";
    public static final String PRIOR_FAR_ML = "prior.far.ml";
    public static final String POST_FAR_SEISMIC = "post.far.seismic";
    public static final String PRIOR_FAR_SEISMIC = "prior.far.seismic";
    public static final String PRIOR_FAR_SEISMIC_ML = "prior.far.seismic.ml";
    public static final String POST_FAR_SEISMIC_ML = "post.far.seismic.ml";
    public static final String POST_FAR_SEISMIC_REAL = "post.far.seismic.realizations";
    public static final String PRIOR_FAR_SEISMIC_REAL = "prior.far.seismic.realizations";

    private DeliveryLiteUI parentUI;

    //Pathnames of the datasets are stored in a HashMap keyed by qualified filename
    private Map<String, String> pathnames = new HashMap<String, String>();

    public DatasetParams(DeliveryLiteUI ui){
        parentUI = ui;
        genPathnames();
    }

    /**
     * Get the pathname of a dataset
     * @param filename Qualified name of the dataset file
     * @return full path of the dataset file
     */
    public String getPathname(String filename) {
        return pathnames.get(filename);
    }

    private void genPathnames() {
        //get the output prefix
        String outPrefix = parentUI.getOutputBasename();
        String datasetDir = QiProjectDescUtils.getDatasetsPath(parentUI.getAgent().getQiProjectDescriptor());
        String filesep = parentUI.getAgent().getMessagingMgr().getServerOSFileSeparator();
        String baseName = datasetDir + filesep + outPrefix;

        String name = datasetDir + filesep + parentUI.getNearWaveletFileName() + ".dat";
        pathnames.put(NEAR_WAVELET, name);
        name = datasetDir + filesep + parentUI.getFarWaveletFileName() + ".dat";
        pathnames.put(FAR_WAVELET, name);
        name = datasetDir + filesep + parentUI.getNearStackFileName() + ".dat" ;
        pathnames.put(NEAR_STACK, name);
        name = datasetDir + filesep + parentUI.getFarStackFileName() + ".dat" ;
        pathnames.put(FAR_STACK, name);

        name = baseName + "_post_near_seismic.dat";
        pathnames.put(POST_NEAR_SEISMIC, name);
        name = baseName + "_prior_near_seismic.dat";
        pathnames.put(PRIOR_NEAR_SEISMIC, name);
        name = baseName + "_prior_near_seismic_ml.dat";
        pathnames.put(PRIOR_NEAR_SEISMIC_ML, name);
        name = baseName + "_post_near_seismic_ml.dat";
        pathnames.put(POST_NEAR_SEISMIC_ML, name);
        name = baseName + "_post_near_seismic_realizations.dat";
        pathnames.put(POST_NEAR_SEISMIC_REAL, name);
        name = baseName + "_prior_near_seismic_realizations.dat";
        pathnames.put(PRIOR_NEAR_SEISMIC_REAL, name);
        name = baseName + "_post_near_median_model.dat";
        pathnames.put(POST_NEAR_MEDIAN_MODEL, name);
        name = baseName + "_prior_near_median_model.dat";
        pathnames.put(PRIOR_NEAR_MEDIAN_MODEL, name);
        name = baseName + "_post_near_realizations.dat";
        pathnames.put(POST_NEAR_REAL, name);
        name = baseName + "_prior_near_realizations.dat";
        pathnames.put(PRIOR_NEAR_REAL, name);
        name = baseName + "_prior_near_ml.dat";
        pathnames.put(PRIOR_NEAR_ML, name);

        if (parentUI.isNearAndFar()){
            name = baseName + "_post_far_seismic.dat";
            pathnames.put(POST_FAR_SEISMIC, name);
            name = baseName + "_prior_far_seismic.dat";
            pathnames.put(PRIOR_FAR_SEISMIC, name);
            name = baseName + "_prior_far_seismic_ml.dat";
            pathnames.put(PRIOR_FAR_SEISMIC_ML, name);
            name = baseName + "_post_far_seismic_ml.dat";
            pathnames.put(POST_FAR_SEISMIC_ML, name);
            name = baseName + "_post_far_seismic_realizations.dat";
            pathnames.put(POST_FAR_SEISMIC_REAL, name);
            name = baseName + "_prior_far_seismic_realizations.dat";
            pathnames.put(PRIOR_FAR_SEISMIC_REAL, name);
            
            name = baseName + "_post_near_far_median_model.dat";
            pathnames.put(POST_FAR_MEDIAN_MODEL, name);
            name = baseName + "_prior_near_far_median_model.dat";
            pathnames.put(PRIOR_FAR_MEDIAN_MODEL, name);
            name = baseName + "_post_near_far_realizations.dat";
            pathnames.put(POST_FAR_REAL, name);
            name = baseName + "_prior_near_far_realizations.dat";
            pathnames.put(PRIOR_FAR_REAL, name);
            name = baseName + "_prior_near_far_ml.dat";
            pathnames.put(PRIOR_FAR_ML, name);
        }
    }
}
