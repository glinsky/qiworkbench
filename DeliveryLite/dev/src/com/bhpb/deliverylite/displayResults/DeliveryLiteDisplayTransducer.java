/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007-2008  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */

package com.bhpb.deliverylite.displayResults;

import java.lang.NumberFormatException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.ControlConstants;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.QIWConstants;

import com.bhpb.deliverylite.DeliveryLitePlugin;
import com.bhpb.deliverylite.DeliveryLiteUI;
import com.bhpb.deliverylite.displayResults.DatasetParams;
import com.bhpb.deliverylite.displayResults.DeliveryLiteControlConstants;

/**
 * Finite state machine (FSM) for displaying the results of DeliveryLite.
 */
public class DeliveryLiteDisplayTransducer {
    /** Display step */
    private int stepState = ControlConstants.SESSION_STEP;
    /** Step action */
    private int actionState = ControlConstants.START_SESSION;
    /** Agent's messaging manager. */
    MessagingManager messagingMgr;
    // GUI component
    private DeliveryLiteUI gui;
    /** Component descriptor of the qiViewer being controlled */
    ComponentDescriptor qiViewerDesc;
    DeliveryLitePlugin agent;
    
    /** Indicator that session was terminated either due to an error in the transduced or the user cancelled displaying the results */
    boolean sessionTerminated = false;
	boolean ignoreResponse = false;

    String sessionID = "", xsecWID = "", openDiaWID = "",
           layerDiaWID = "", syncDiaWID = "", annoDiaWID = "",
           scaleDiaWID = "";
    String geofileID = "";
    //command action
    ArrayList actions = new ArrayList();
	
    //parameters for multi-action commands
    ArrayList<String> actionParams1 = new ArrayList<String>();
    ArrayList<String> actionParams2 = new ArrayList<String>();
    ArrayList<String> actionParams3 = new ArrayList<String>();
    ArrayList<String> actionParams4 = new ArrayList<String>();
    ArrayList<String> actionParams5 = new ArrayList<String>();
    ArrayList<String> actionParams6 = new ArrayList<String>();
	ArrayList<String> actionParams7 = new ArrayList<String>();
	ArrayList<String> actionParams8 = new ArrayList<String>();
	ArrayList<String> actionParams9 = new ArrayList<String>();
	ArrayList<String> actionParams10 = new ArrayList<String>();
	ArrayList<String> actionParams11 = new ArrayList<String>();

    //Window characteristics: Window ID, size (width, height), position (x, y)
    String xsecWID1 = "", xsecWID2 = "", xsecWID3 = "";
    int xsecWidth1 = 0, xsecHeight1 = 0, xsecWidth2 = 0, xsecHeight2 = 0,
        xsecWidth3 = 0, xsecHeight3 = 0;
    int xsecX1 = 0, xsecY1 = 0, xsecX2 = 0, xsecY2 = 0, xsecX3 = 0,
        xsecY3 = 0;
	int viewerWidth = 50, viewerHeight = 50;    //leave room around the edges

    //Note: layer IDs are unique across all windows
    //Window #1 layers; leave extra room. If the layerID is -1, this means the 
    //file for that layer does not exist.
    String[] layerIDs1 = new String[20]; int layer1Size = 0;
    String[] layerPropsID1 = new String[20]; int layerProps1Size = 0;
    //Window #2 layers; leave extra room
    String[] layerIDs2 = new String[10]; int layer2Size = 0;
    String[] layerPropsID2 = new String[10]; int layerProps2Size = 0;
    //Window #3 layers
    String[] layerIDs3 = new String[2]; int layer3Size = 0;
    String[] layerPropsID3 = new String[2]; int layerProps3Size = 0;
    
    //Seismic layer properties ID for Window #1. Can have up to 8 seismic layers
    String[] seismicLayerPropsID = new String[10]; int seismicLayerPropsSize = 0;
    
    //intermediate holder for a layer property ID
    String layerPropsID = "";
    //intermediate holder for a layer ID
    String layerID = "";
	
	//Window and layer IDs returned when adding a layer
	ArrayList ids;
	
	//Min and Max trace values for a layer
	ArrayList<String> minMaxTraceVals;
    //Max trace value for a seismic trace in Window #1
    int maxTraceVal = 0;
    //Max trace values for all seismic traces in Window #1
    int[] maxTraceVals = new int[10]; int maxTraceValsSize = 0;
    //Loop index for adjusting min & max normalizations
    int seismicLayerIdx = 0;

    //Info about all the datasets used to display DeliveryLite results
    DatasetParams datasets;
    
    //Indicator if layer file exists ('yes' or 'no)
    String fileExists = "yes";
    //Indicator for processing a layer
    boolean skipLayer = false;

    boolean isNearAndFar = false;
	int layerIdx = 0;
	
	String hiddenLayer1="", //PRIOR_NEAR_SEISMIC_ML
		   hiddenLayer2="", //PRIOR_NEAR_SEISMIC
		   hiddenLayer3="", //PRIOR_FAR_SEISMIC_ML
		   hiddenLayer4="", //PRIOR_FAR_SEISMIC
           hiddenLayer5=""; //FAR_WAVELET
           
    String annotatedLayer1 = "", //near_stack
           annotatedLayer2 = "", //near_realization
           annotatedLayer3 = ""; //near_wavelet

    /**
     * Constructor. Initialize the finite state machine.
     * @param gui DeliveryLite GUI.
     * @param messagingMgr DeliveryLite Agent's messaging manager.
     * @param stepState The state (display step) in which to start.
     * @param actionState The action within a step in which to start.
     */
    public DeliveryLiteDisplayTransducer(DeliveryLiteUI gui, MessagingManager messagingMgr, int stepState, int actionState) {
        this.stepState = stepState;
        this.actionState = actionState;
        this.messagingMgr = messagingMgr;
        this.gui = gui;
        this.agent = gui.getAgent();
    }

    /**
     * Process the response message from a CONTROL_QIVIEWER command.
     * The actions in the request are specified by the actionState
     * which is relative to the step (stepState) in the display process.
     * <p>
     * Step state 0, action 0 is start_control_session.
     * <br> Step state 0, action 1 is end_control_session.
     * @param resp The response message.
     */
    public void processResp(IQiWorkbenchMsg resp) {
System.out.println("DeliveryLite::processResp: response="+resp);
        //Check if the user terminated the control session
        if (resp.isAbnormalStatus()) {
            String errmsg = (String)resp.getContent();
            if (errmsg.contains("CANCELLED")) {
                endControlSession(errmsg);
            }
        }
        
        switch (stepState) {
            case ControlConstants.SESSION_STEP:
                switch (actionState) {
                    case ControlConstants.START_SESSION:
                        //Check if session terminated and reinitialized for 
                        //reuse. NOTE: This consumes the response from the
                        //request to end the session.
                        if (ignoreResponse) {
                            //prepare to start a new session
                            //NOTE: The response from a CONTROL_QIVIEWER_CMD
                            //triggers the start of a new session.
                            ignoreResponse = false;
							sessionTerminated = false;
                            break;
                        }
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot start a display session");
                        } else {
                            //Create pathnames of all datasets used to display 
                            //DeliveryLite results. NOTE: This cannot be performed
                            //when the Transducer instance is created because
                            //the GUI is not yet populated.
                            datasets = new DatasetParams(gui);
                            isNearAndFar = gui.isNearAndFar();
                            
                            ArrayList outs = (ArrayList)resp.getContent();
                            sessionID = (String)outs.get(0);
                            qiViewerDesc = (ComponentDescriptor)outs.get(1);

                            stepState = DeliveryLiteControlConstants.GET_LAYER_PROPS_WIN1;
							
							actionState = DeliveryLiteControlConstants.INIT_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.INIT_PROGRESS_DIALOG_ACTION);
							actionParams1.add("title=Display DeliveryLite Results");
							actionParams1.add("windows=3");
							actionParams1.add("layers="+ (isNearAndFar ?  "11,6,2" : "7,4,1"));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case ControlConstants.END_SESSION:
                        //Prepare for next control session (in case use the same transducer)
                        initState();
                        break;

                    default:
                }
                break;

			//XSec Window #1 contains 11 layers, 4 of which are conditional
			//(namely, the far files). The layers are added in reverse order
			//because the viewer adds a new layer on top of the existing layers.
            case DeliveryLiteControlConstants.GET_LAYER_PROPS_WIN1:
                switch (actionState) {
					case DeliveryLiteControlConstants.INIT_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot initialize the progress dialog\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.GET_LAYER_PROPS;

                            //ASSUME the bottom layer exists so window is created
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_MEDIAN_MODEL));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
					case DeliveryLiteControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for 1st layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS11;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=SelectedProperty.Property");
                            actionParams1.add("value=NG");
                            actions.add(actionParams1);
                            
                            actionParams3.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams2);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS11:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set model layer's dataset properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=1");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Variable_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=ModelVerticalRange.StartTime");
                            actionParams11.add("value=0");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ModelVerticalRange.StructuralInterpolation");
                            actionParams7.add("value=off");
                            actions.add(actionParams7);
                            
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("colormap=sand_shale");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set model layer's display properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.CREATE_WINDOW;

                            actions.clear(); 
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_MEDIAN_MODEL));
							actionParams1.add("ltype="+ControlConstants.MODEL_LAYER);
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } 
						break;
                        
                    case DeliveryLiteControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #1\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
                            xsecWID1 = (String)ids.get(0);
							layer1Size = 0;
                            layerIDs1[layer1Size++] = (String)ids.get(1);
                            
                            //Check if file exists
                            fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.PRIOR_NEAR_ML));
                            
                            skipLayer = fileExists != null && fileExists.equals("no");
                            
                            actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER1 : DeliveryLiteControlConstants.GET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();
							if (!skipLayer) {
								actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
								actionParams1.add("format=BHP-SU");
								actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_ML));
							} else
								actions.add("action="+ControlConstants.NOP_ACTION);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 2nd layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();

                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=SelectedProperty.Property");
                            actionParams1.add("value=NG");
                            actions.add(actionParams1);
                            
                            actionParams3.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams2);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

					case DeliveryLiteControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 2nd model layer's dataset properties in XSec #1\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS12;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=1");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Variable_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=ModelVerticalRange.StartTime");
                            actionParams11.add("value=0");
                            actions.add(actionParams11);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ModelVerticalRange.StructuralInterpolation");
                            actionParams7.add("value=off");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("colormap=sand_shale");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS12:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 2nd model layer's display properties in XSec window #1\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_ML));
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 2nd layer to XSection window #1\n"+resp.getContent());
                            break;
                        } 

                        ids = (ArrayList)resp.getContent();
                        //check if the layer was added to the right XSec window
                        if (!xsecWID1.equals((String)ids.get(0))) {
                        }
                        //Check if the layer was added
                        if (((String)ids.get(1)).equals("-1")) {
                        } else
                            layerIDs1[layer1Size++] = layerID = (String)ids.get(1);
                    case DeliveryLiteControlConstants.SKIP_LAYER1:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.POST_NEAR_MEDIAN_MODEL));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                    
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER2 :  DeliveryLiteControlConstants.GET_LAYER_PROPS2;

                        actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_MEDIAN_MODEL));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);

                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 3rd layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS13;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=SelectedProperty.Property");
                            actionParams1.add("value=NG");
                            actions.add(actionParams1);
                            
                            actionParams3.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams2);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS13:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 3rd model layer's dataset properties\\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=1");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Variable_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=ModelVerticalRange.StartTime");
                            actionParams11.add("value=0");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ModelVerticalRange.StructuralInterpolation");
                            actionParams7.add("value=off");
                            actions.add(actionParams7);
                            
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("colormap=sand_shale");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 3rd model layer's display properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_MEDIAN_MODEL));
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
						ids = (ArrayList)resp.getContent();
						//check if the layer was added to the right XSec window
						if (!xsecWID1.equals((String)ids.get(0))) {
						}
						//Check if the layer was added
						if (((String)ids.get(1)).equals("-1")) {
						} else
							layerIDs1[layer1Size++] = (String)ids.get(1);
					case DeliveryLiteControlConstants.SKIP_LAYER2:
						//Note: near_stack always exists
						actionState = isNearAndFar ? DeliveryLiteControlConstants.GET_LAYER_PROPS3 : DeliveryLiteControlConstants.GET_LAYER_PROPS4;

						actions.clear(); actionParams1.clear();
						actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
						actionParams1.add("format=BHP-SU");
						actionParams1.add("file="+datasets.getPathname(isNearAndFar ? DatasetParams.FAR_STACK : DatasetParams.NEAR_STACK));
						actions.add(actionParams1);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 4th layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
							
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS15;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+gui.getEp());
                            actions.add(actionParams1);
							
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=ChosenRange.cdp");
                            actionParams4.add("value="+gui.getCdp());
                            actions.add(actionParams4);
                            
                            actionParams3.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams2);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

					case DeliveryLiteControlConstants.SET_LAYER_PROPS15:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 4th layer's dataset properties in XSec #1\n"+resp.getContent());
                        } else {
							actionState =  DeliveryLiteControlConstants.GET_TRACE_RANGE3;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//4th seismic layer (conditional) - FAR_STACK
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS3;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 4th seismic layer properties in XSec window #1\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.FAR_STACK));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 4th layer to XSection window #1\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
							//check if the layer was added to the right XSec window
                            if (!xsecWID1.equals((String)ids.get(0))) {
							}
							//Check if the layer was added
							if (((String)ids.get(1)).equals("-1")) {
							} else
                            	layerIDs1[layer1Size++] = (String)ids.get(1);

                            actionState = DeliveryLiteControlConstants.GET_LAYER_PROPS4;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.NEAR_STACK));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 5th layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
                            
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS16;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=ChosenRange.ep");
                            actionParams1.add("value="+gui.getEp());
                            actions.add(actionParams1);
							
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams4.add("field=ChosenRange.cdp");
                            actionParams4.add("value="+gui.getCdp());
                            actions.add(actionParams4);
                            
                            actionParams3.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams2);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

					case DeliveryLiteControlConstants.SET_LAYER_PROPS16:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 5th layer's dataset properties in XSec #1\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE4;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//5th seismic layer (conditional) - NEAR_STACK
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS4;
							
                           actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
					
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 5th seismic layer properties in XSec window #1\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER4;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.NEAR_STACK));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 5th layer to XSection window #1\n"+resp.getContent());
                        } else {
                            ids = (ArrayList)resp.getContent();
							//check if the layer was added to the right XSec window
                            if (!xsecWID1.equals((String)ids.get(0))) {
							}
							//Check if the layer was added
							if (((String)ids.get(1)).equals("-1")) {
							} else
                            	layerIDs1[layer1Size++] = annotatedLayer1 = (String)ids.get(1);
                                
                            //Check if file exists
                            fileExists = agent.checkFileExist(datasets.getPathname(isNearAndFar ? DatasetParams.PRIOR_FAR_SEISMIC : DatasetParams.PRIOR_NEAR_SEISMIC));
                            
                            skipLayer = fileExists != null && fileExists.equals("no");
                        
                            actionState = isNearAndFar ? (skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER5 : DeliveryLiteControlConstants.GET_LAYER_PROPS5) : (skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER8 : DeliveryLiteControlConstants.GET_LAYER_PROPS8);

                            actions.clear(); actionParams1.clear();
							if (!skipLayer) {
								actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
								actionParams1.add("format=BHP-SU");
								actionParams1.add("file="+datasets.getPathname(isNearAndFar ? DatasetParams.PRIOR_FAR_SEISMIC : DatasetParams.PRIOR_NEAR_SEISMIC));
							} else
								actions.add("action="+ControlConstants.NOP_ACTION);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 6th layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY5;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 6th layer in XSection window #1\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE5;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//6th seismic layer (conditional) - PRIOR_FAR_SEISMIC
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS5;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
 /*                           
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER5;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_FAR_SEISMIC));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 6th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
                        //check if the layer was added to the right XSec window
                        if (!xsecWID1.equals((String)ids.get(0))) {
                        }
                        //Check if the layer was added
                        if (((String)ids.get(1)).equals("-1")) {
                        } else {
                            layerIDs1[layer1Size++] = hiddenLayer4 = layerID =  (String)ids.get(1);
                            actionState = DeliveryLiteControlConstants.SET_LAYER_COLOR6;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_COLOR_ACTION);
                            actionParams1.add("layerID="+layerID);
                            actionParams1.add("property=Foreground");
                            actionParams1.add("color=Red");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
						break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_COLOR6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add Foreground color to 10th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                    case DeliveryLiteControlConstants.SKIP_LAYER5:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.PRIOR_FAR_SEISMIC_ML));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                            
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER6 :  DeliveryLiteControlConstants.GET_LAYER_PROPS6;

                        actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_FAR_SEISMIC_ML));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 7th layer to XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY6;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 7th layer in XSection window #1\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE6;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);							
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//7th seismic layer - PRIOR_FAR_SEISMIC_ML
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS6;
							
                           actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER6;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_FAR_SEISMIC_ML));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER6:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 7th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
                        //check if the layer was added to the right XSec window
                        if (!xsecWID1.equals((String)ids.get(0))) {
                        }
                        //Check if the layer was added
                        if (((String)ids.get(1)).equals("-1")) {
                        } else {
                            layerIDs1[layer1Size++] = hiddenLayer3 = layerID = (String)ids.get(1);
                            
                            actionState = DeliveryLiteControlConstants.SET_LAYER_COLOR1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_COLOR_ACTION);
                            actionParams1.add("layerID="+layerID);
                            actionParams1.add("property=Foreground");
                            actionParams1.add("color=Red");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
						break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_COLOR1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add Foreground color to 7th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                    case DeliveryLiteControlConstants.SKIP_LAYER6:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.POST_FAR_SEISMIC));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                        
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER7 :  DeliveryLiteControlConstants.GET_LAYER_PROPS7;

                        actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_FAR_SEISMIC));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS7:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 8th layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY7;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY7:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 8th layer in XSection window #1\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE7;
							
                            actions.add("action="+ ControlConstants.GET_TRACE_RANGE_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//8th seismic layer (conditional) - POST_FAR_SEISMIC
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE7:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState =   DeliveryLiteControlConstants.SET_LAYER_PROPS7;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS7:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER7;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_FAR_SEISMIC));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER7:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 8th ayer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
                        //check if the layer was added to the right XSec window
                        if (!xsecWID1.equals((String)ids.get(0))) {
                        }
                        //Check if the layer was added
                        if (((String)ids.get(1)).equals("-1")) {
                        } else {
                            layerIDs1[layer1Size++] = layerID = (String)ids.get(1);
                            actionState = DeliveryLiteControlConstants.SET_LAYER_COLOR3;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_COLOR_ACTION);
                            actionParams1.add("layerID="+layerID);
                            actionParams1.add("property=Foreground");
                            actionParams1.add("color=Green");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
						break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_COLOR3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add Foreground color to 8th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                    case DeliveryLiteControlConstants.SKIP_LAYER7:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                        
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER8 :  DeliveryLiteControlConstants.GET_LAYER_PROPS8;

                        actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS8:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 9th layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY8;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY8:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 9th layer in XSection window #1\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE8;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ ControlConstants.GET_TRACE_RANGE_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//9th seismic layer (conditional) - PRIOR_NEAR_SEISMIC
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE8:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
							actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS8;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS8:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set model layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER8;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER8:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
                        //check if the layer was added to the right XSec window
                        if (!xsecWID1.equals((String)ids.get(0))) {
                        }
                        //Check if the layer was added
                        if (((String)ids.get(1)).equals("-1")) {
                        } else {
                            layerIDs1[layer1Size++] = hiddenLayer2 = layerID =  (String)ids.get(1);
                            actionState = DeliveryLiteControlConstants.SET_LAYER_COLOR5;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_COLOR_ACTION);
                            actionParams1.add("layerID="+layerID);
                            actionParams1.add("property=Foreground");
                            actionParams1.add("color=Red");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
						break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_COLOR5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add Foreground color to 10th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                    case DeliveryLiteControlConstants.SKIP_LAYER8:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC_ML));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                        
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER9 :  DeliveryLiteControlConstants.GET_LAYER_PROPS9;

                        actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC_ML));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS9:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 10th layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY9;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY9:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 10th layer in XSection window #1\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE9;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ ControlConstants.GET_TRACE_RANGE_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//10th seismic layer (conditional) - PRIOR_NEAR_SEISMIC_ML
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE9:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
                            
							actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS9;
							
                           actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					//10th seismic layer - PRIOR_NEAR_SEISMIC_ML
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS9:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set model layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER9;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC_ML));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER9:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
                        //check if the layer was added to the right XSec window
                        if (!xsecWID1.equals((String)ids.get(0))) {
                        }
                        //Check if the layer was added
                        if (((String)ids.get(1)).equals("-1")) {
                        } else {
                            layerIDs1[layer1Size++] = hiddenLayer1 = layerID =  (String)ids.get(1);
							
                            actionState = DeliveryLiteControlConstants.SET_LAYER_COLOR2;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_COLOR_ACTION);
                            actionParams1.add("layerID="+layerID);
                            actionParams1.add("property=Foreground");
                            actionParams1.add("color=Red");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
						break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_COLOR2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add Foreground color to 10th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                    case DeliveryLiteControlConstants.SKIP_LAYER9:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.POST_NEAR_SEISMIC));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                        
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER10 :  DeliveryLiteControlConstants.GET_LAYER_PROPS10;

                        actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_SEISMIC));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS10:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 11th layer in XSection window #1\n"+resp.getContent());
                        } else {
                            layerPropsID1[layerProps1Size++] = layerPropsID =  (String)resp.getContent();
                            seismicLayerPropsID[seismicLayerPropsSize++] = layerPropsID;
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY10;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY10:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 11th layer in XSection window #1\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE10;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ ControlConstants.GET_TRACE_RANGE_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//11th seismic layer (conditional) - POST_NEAR_SEISMIC
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE10:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
                                
							actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS10;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
                            if (absmax>maxTraceVal) maxTraceVal = absmax;
                            maxTraceVals[maxTraceValsSize++] = absmax;
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
/*
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
*/
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=Interpolation.Type");
                            actionParams11.add("value=QUADRATIC");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					//11th seismic layer - POST_NEAR_SEISMIC
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS10:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 11th seismic layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER10;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_SEISMIC));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER10:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 11th seismic layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                        ids = (ArrayList)resp.getContent();
                        //check if the layer was added to the right XSec window
                        if (!xsecWID1.equals((String)ids.get(0))) {
                        }
                        //Check if the layer was added
                        if (((String)ids.get(1)).equals("-1")) {
                        } else {
                            layerIDs1[layer1Size++] = layerID = (String)ids.get(1);
							
                            actionState = DeliveryLiteControlConstants.SET_LAYER_COLOR4;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_COLOR_ACTION);
                            actionParams1.add("layerID="+layerID);
                            actionParams1.add("property=Foreground");
                            actionParams1.add("color=Green");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
						break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_COLOR4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add Foreground color to 11th layer to XSection window #1\n"+resp.getContent());
                            break;
                        }
                    case DeliveryLiteControlConstants.SKIP_LAYER10:
                        
                        //SET UP LOOP FOR ADJUSTING NORMALIZATION MIN & MAX VALUES
                        seismicLayerIdx = 0;
                        
                    //START of loop to adjust seismic layers' normalization min & max values
                    case DeliveryLiteControlConstants.DO_LAYER:
                        //Find next seismic layer to adjust
                        while (seismicLayerIdx < maxTraceValsSize) {
                            if (maxTraceVals[seismicLayerIdx] != maxTraceVal) break;
                            seismicLayerIdx++;
                        }
                        //checked all seismic layers?
                        if (seismicLayerIdx >= maxTraceValsSize) {
                            actionState = DeliveryLiteControlConstants.END_LAYER;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.NOP_ACTION);
                            actions.add(actionParams1);
                        } else {
                            actionState = DeliveryLiteControlConstants.ADJUST_NORM_MIN_MAX;
                            
                            layerPropsID = seismicLayerPropsID[seismicLayerIdx];
                            
                            actions.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(-maxTraceVal));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(maxTraceVal));
                            actions.add(actionParams4);
                        }
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    case DeliveryLiteControlConstants.ADJUST_NORM_MIN_MAX:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot adjust the normalization min & max for seismic layers in XSection window #1\n"+resp.getContent());
                            break;
                        }
                        
                        seismicLayerIdx++;
                        
                        actionState = DeliveryLiteControlConstants.DO_LAYER;
                        
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.NOP_ACTION);
                        actions.add(actionParams1);
                        
                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
                        
                    //END of adjusting seismic layers' normalization min & max values
                    case DeliveryLiteControlConstants.END_LAYER:
                        actionState = DeliveryLiteControlConstants.SCROLL_VERTICAL;

                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SCROLL_ACTION);
                        actionParams1.add("winID="+xsecWID1);
                        actionParams1.add("scrollbar="+ControlConstants.VERTICAL_SCROLLBAR);
                        //get the time of the model's top layer
                        actionParams1.add("position="+gui.getMinTime());
                        actions.add(actionParams1);

                        messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
						
                    case DeliveryLiteControlConstants.SCROLL_VERTICAL:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot position vertical scrollbar in XSection window #1\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.SET_ANNO_PROPS;

                            //Must set the annotation layer first
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID1);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer1);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID1);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=ep,cdp");
                            actions.add(actionParams1);
							
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.Location");
                            actionParams2.add("value=TOP");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.SynchronizationKey");
                            actionParams3.add("value=ep");
                            actions.add(actionParams3);
                            
                            actionParams7.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams7.add("winID="+xsecWID1);
                            actionParams7.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams7.add("field=Vertical.StepType.Automatic");
                            actionParams7.add("value=on");
                            actions.add(actionParams7);

                            actionParams8.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams8.add("winID="+xsecWID1);
                            actionParams8.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams8.add("field=Vertical.Appearance.Location");
                            actionParams8.add("value=LEFT");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID1);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
					
                    case DeliveryLiteControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties\n"+resp.getContent());
                        } else {
							actionState = isNearAndFar ?  DeliveryLiteControlConstants.HIDE_LAYER : DeliveryLiteControlConstants.HIDE_LAYER2;
							
                            actions.clear(); actionParams1.clear();
                            if (isNearAndFar) {
                                if (!hiddenLayer4.equals("")) {
                                    actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
									actionParams1.add("winID="+xsecWID1);
                                    actionParams1.add("layerID="+ hiddenLayer4);
                                    actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                                } else
                                    actions.add("action="+ControlConstants.NOP_ACTION);
                            } else {
                                if (!hiddenLayer2.equals("")) {
                                    actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
									actionParams1.add("winID="+xsecWID1);
                                    actionParams1.add("layerID="+ hiddenLayer2);
                                    actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                                } else
                                    actions.add("action="+ControlConstants.NOP_ACTION);
                            }
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					//Hide PRIOR_FAR_SEISMIC_ML layer
                    case DeliveryLiteControlConstants.HIDE_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide seismic layer\n"+resp.getContent());
                        } else {
							actionState =  DeliveryLiteControlConstants.HIDE_LAYER1;
							
                            actions.clear(); actionParams1.clear();
                            if (!hiddenLayer3.equals("")) {
                                actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
								actionParams1.add("winID="+xsecWID1);
                                actionParams1.add("layerID="+hiddenLayer3);
                                actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            } else
                                actions.add("action="+ControlConstants.NOP_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					//Hide PRIOR_FAR_SEISMIC layer
                    case DeliveryLiteControlConstants.HIDE_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide seismic layer\n"+resp.getContent());
                        } else {
							actionState =   DeliveryLiteControlConstants.HIDE_LAYER2;
							
                            actions.clear(); actionParams1.clear();
                            if (!hiddenLayer2.equals("")) {
                                actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
								actionParams1.add("winID="+xsecWID1);
                                actionParams1.add("layerID="+hiddenLayer2);
                                actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            } else
                                actions.add("action="+ControlConstants.NOP_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					//Hide PRIOR_NEAR_SEISMIC_ML layer
                    case DeliveryLiteControlConstants.HIDE_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide seismic layer\n"+resp.getContent());
                        } else {
							actionState =  DeliveryLiteControlConstants.HIDE_LAYER3;
							
                            actions.clear(); actionParams1.clear();
                            if (!hiddenLayer1.equals("")) {
                                actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
								actionParams1.add("winID="+xsecWID1);
                                actionParams1.add("layerID="+hiddenLayer1);
                                actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            } else
                                actions.add("action="+ControlConstants.NOP_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

					//hide PRIOR_NEAR_SEISMIC layer
                    case DeliveryLiteControlConstants.HIDE_LAYER3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide seismic layer\n"+resp.getContent());
                        } else {
                            //turn off broadcast so Window Scale properties can be set in insolation
                            actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case DeliveryLiteControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn off broadcast\n"+resp.getContent());
                        } else {
                            //set Window scale properties
                            actionState =  DeliveryLiteControlConstants.SET_SCALE_PROPS;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=HorizontalScale");
                            actionParams1.add("value=0.203");
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams2.add("field=VerticalScale");
                            actionParams2.add("value=0.08");
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams3);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Window Scale properties\n"+resp.getContent());
                        } else {
							actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS1;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID1);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams6.add("field=ListenToCursorPosition");
                            actionParams6.add("value=off");
                            actions.add(actionParams6);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
							
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID1);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=SynchronizeVerticalScrolling");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);
                            
                            actionParams7.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams7.add("winID="+xsecWID1);
                            actionParams7.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams7.add("field=SynchronizeHorizontalScrolling");
                            actionParams7.add("value=off");
                            actions.add(actionParams7);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID1);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams5);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.SET_SYNC_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set sync properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight1 = 600; xsecWidth1 = 600;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("width="+xsecWidth1);
                            actionParams1.add("height="+xsecHeight1);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("x="+xsecX1);
                            actionParams2.add("y="+xsecY1);
                            actions.add(actionParams2);
/*
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID1);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);
*/
							//redraw window so hidden layers displayed as hidden
                            actions.add("action="+ControlConstants.REDRAW_WINDOW_ACTION);
                            actionParams4.clear();
                            actionParams4.add("winID="+xsecWID1);
							actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSec window #1\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set divider in window #1\n"+resp.getContent());
                        } else {
                            //It is now safe to turn broadcast on
                            actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS2;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID1);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID1);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case DeliveryLiteControlConstants.SET_SYNC_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn on broadcast\n"+resp.getContent());
                        } else {
                            stepState = DeliveryLiteControlConstants.GET_LAYER_PROPS_WIN2;
                            actionState = DeliveryLiteControlConstants.GET_LAYER_PROPS;

							//ASSUME the bottom layer exists so window is created
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_REAL));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;									

			//XSec Window #2 contains6 layers, 4 of which are conditional
			//(namely, the far files). The layers are added in reverse order
			//because the viewer adds a new layer on top of the existing layers.
            case DeliveryLiteControlConstants.GET_LAYER_PROPS_WIN2:
                switch (actionState) {
                    case DeliveryLiteControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for 1st layer in XSection window #2\n"+resp.getContent());
                        } else {
                            layerPropsID2[layerProps2Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS11;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=SelectedProperty.Property");
                            actionParams1.add("value=NG");
                            actions.add(actionParams1);
                            
                            actionParams3.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams2);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS11:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set model layer's dataset properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=1");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Variable_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=ModelVerticalRange.StartTime");
                            actionParams11.add("value=0");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ModelVerticalRange.StructuralInterpolation");
                            actionParams7.add("value=off");
                            actions.add(actionParams7);
                            
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("colormap=sand_shale");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
					//1st model layer - PRIOR_NEAR_REAL
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set model layer display properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_REAL));
                            actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        } break;					
					
                    case DeliveryLiteControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #2\n"+resp.getContent());
							break;
                        }
						
						ids = (ArrayList)resp.getContent();
						xsecWID2 = (String)ids.get(0);
						layer2Size = 0;
						layerIDs2[layer2Size++] = annotatedLayer2 = (String)ids.get(1);
						
						//Check if file exists
						fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.POST_NEAR_REAL));
						
						skipLayer = fileExists != null && fileExists.equals("no");
						
						actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER1 : DeliveryLiteControlConstants.GET_LAYER_PROPS1;

						actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_REAL));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
						actions.add(actionParams1);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 2nd layer in XSection window #2\n"+resp.getContent());
                        } else {
                            layerPropsID2[layerProps2Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS12;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams1.clear();
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actionParams1.add("field=SelectedProperty.Property");
                            actionParams1.add("value=NG");
                            actions.add(actionParams1);
                            
                            actionParams3.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams3.add("layerPropsID="+layerPropsID);
							actions.add(actionParams3);
                            
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DATASET_PROPERTIES);
                            actions.add(actionParams2);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                    
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS12:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 2nd model layer's dataset properties in XSec #2\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS1;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=1");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value=0");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value=1");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Variable_Density");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams11.clear();
                            actionParams11.add("layerPropsID="+layerPropsID);
                            actionParams11.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams11.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams11.add("field=ModelVerticalRange.StartTime");
                            actionParams11.add("value=0");
                            actions.add(actionParams11);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ModelVerticalRange.StructuralInterpolation");
                            actionParams7.add("value=off");
                            actions.add(actionParams7);
                            
                            actions.add("action="+ControlConstants.SELECT_COLOR_MAP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("colormap=sand_shale");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.MODEL_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

					case DeliveryLiteControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 2nd model layer's properties in XSec window #2\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_REAL));
                            actionParams1.add("type="+ControlConstants.MODEL_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.ADD_LAYER1:
                         if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add model layer to XSection window #2\n"+resp.getContent());
							break;
                        }
						ids = (ArrayList)resp.getContent();
						//check if the layer was added to the right XSec window
						if (!xsecWID2.equals((String)ids.get(0))) {
						}
						//Check if the layer was added
						if (((String)ids.get(1)).equals("-1")) {
						} else
							layerIDs2[layer2Size++] = (String)ids.get(1);
					case DeliveryLiteControlConstants.SKIP_LAYER1:
						//Check if file exists
						fileExists = agent.checkFileExist(datasets.getPathname(isNearAndFar ? DatasetParams.PRIOR_FAR_SEISMIC : DatasetParams.PRIOR_NEAR_SEISMIC));
						
						skipLayer = fileExists != null && fileExists.equals("no");
					
						actionState = isNearAndFar ? (skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER2 : DeliveryLiteControlConstants.GET_LAYER_PROPS2) : (skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER4 : DeliveryLiteControlConstants.GET_LAYER_PROPS4);

						actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(isNearAndFar ? DatasetParams.PRIOR_FAR_SEISMIC_REAL : DatasetParams.PRIOR_NEAR_SEISMIC_REAL));
						} else 
							actions.add("action="+ControlConstants.NOP_ACTION);
						actions.add(actionParams1);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 3rd layer in XSection window #2\n"+resp.getContent());
                        } else {
                            layerPropsID2[layerProps2Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY2;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 3d layer in XSection window #2\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//3rd seismic layer (conditional) - PRIOR_FAR_SEISMIC_REAL
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState = DeliveryLiteControlConstants.SET_LAYER_PROPS2;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER2;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_FAR_SEISMIC_REAL));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
    
                    case DeliveryLiteControlConstants.ADD_LAYER2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 3rd layer to XSection window #1\n"+resp.getContent());
							break;
                        }
						ids = (ArrayList)resp.getContent();
						//check if the layer was added to the right XSec window
						if (!xsecWID2.equals((String)ids.get(0))) {
						}
						//Check if the layer was added
						if (((String)ids.get(1)).equals("-1")) {
						} else
							layerIDs2[layer2Size++] = hiddenLayer2 = (String)ids.get(1);
                    case DeliveryLiteControlConstants.SKIP_LAYER2:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.POST_FAR_SEISMIC_REAL));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                        
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER3 :  DeliveryLiteControlConstants.GET_LAYER_PROPS3;

						actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_FAR_SEISMIC_REAL));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
						actions.add(actionParams1);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 4th layer to XSection window #2\n"+resp.getContent());
                        } else {
							layerPropsID2[layerProps2Size++] = layerPropsID =  (String)resp.getContent();
							
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY3;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 4th layer in XSection window #2\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE3;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_TRACE_RANGE_ACTION);							
							actionParams1.add("layerPropsID="+layerPropsID);
							actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//4th seismic layer - POST_FAR_SEISMIC_REAL
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS3;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER3;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_FAR_SEISMIC_REAL));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER3:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 4th layer to XSection window #2\n"+resp.getContent());
							break;
                        }
						ids = (ArrayList)resp.getContent();
						//check if the layer was added to the right XSec window
						if (!xsecWID2.equals((String)ids.get(0))) {
						}
						//Check if the layer was added
						if (((String)ids.get(1)).equals("-1")) {
						} else
							layerIDs2[layer2Size++] = (String)ids.get(1);
                    case DeliveryLiteControlConstants.SKIP_LAYER3:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC_REAL));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                            
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER4 :  DeliveryLiteControlConstants.GET_LAYER_PROPS4;

						actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC_REAL));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
						actions.add(actionParams1);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 5th layer in XSection window #2\n"+resp.getContent());
                        } else {
                            layerPropsID2[layerProps2Size++] = layerPropsID =  (String)resp.getContent();
							
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY4;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 5th layer in XSection window #2\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE4;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ ControlConstants.GET_TRACE_RANGE_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//5th seismic layer (conditional) - PRIOR_NEAR_SEISMIC_REAL
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
                            actionState =   DeliveryLiteControlConstants.SET_LAYER_PROPS4;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set seismic layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER4;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.PRIOR_NEAR_SEISMIC_REAL));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER4:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 5th ayer to XSection window #2\n"+resp.getContent());
							break;
                        }
						ids = (ArrayList)resp.getContent();
						//check if the layer was added to the right XSec window
						if (!xsecWID2.equals((String)ids.get(0))) {
						}
						//Check if the layer was added
						if (((String)ids.get(1)).equals("-1")) {
						} else
							layerIDs2[layer2Size++] = hiddenLayer1 = (String)ids.get(1);
                    case DeliveryLiteControlConstants.SKIP_LAYER4:
                        //Check if file exists
                        fileExists = agent.checkFileExist(datasets.getPathname(DatasetParams.POST_NEAR_SEISMIC_REAL));
                        
                        skipLayer = fileExists != null && fileExists.equals("no");
                            
                        actionState = skipLayer ? DeliveryLiteControlConstants.SKIP_LAYER5 :  DeliveryLiteControlConstants.GET_LAYER_PROPS5;

						actions.clear(); actionParams1.clear();
						if (!skipLayer) {
							actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
							actionParams1.add("format=BHP-SU");
							actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_SEISMIC_REAL));
						} else
							actions.add("action="+ControlConstants.NOP_ACTION);
						actions.add(actionParams1);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;

                    case DeliveryLiteControlConstants.GET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get properties of 6th layer in XSection window #2\n"+resp.getContent());
                        } else {
                            layerPropsID2[layerProps2Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY5;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 6th layer in XSection window #2\n"+resp.getContent());
                        } else {
							actionState = DeliveryLiteControlConstants.GET_TRACE_RANGE5;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ ControlConstants.GET_TRACE_RANGE_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					//6th seismic layer (conditional) - POST_NEAR_SEISMIC_REAL
                    case DeliveryLiteControlConstants.GET_TRACE_RANGE5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get trace range values\n"+resp.getContent());
                        } else {
							minMaxTraceVals = (ArrayList<String>)resp.getContent();
							
							actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS5;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
                            actionParams1.add("value=0.5");
                            actions.add(actionParams1);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=LIMITS");
                            actions.add(actionParams2);
							
							int min = floor(minMaxTraceVals.get(0));
							int max = ceiling(minMaxTraceVals.get(1));
							int absmax = absMax(min, max);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=Normalization.Min_Value");
                            actionParams3.add("value="+String.valueOf(min < 0 ? -absmax : absmax));
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=Normalization.Max_Value");
                            actionParams4.add("value="+String.valueOf(max < 0 ? -absmax : absmax));
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=RasterizingType.*");
                            actionParams5.add("value=off");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=RasterizingType.Wiggle_Trace");
                            actionParams6.add("value=on");
                            actions.add(actionParams6);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams7.add("field=ClippingFactor");
                            actionParams7.add("value=4");
                            actions.add(actionParams7);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams8.clear();
                            actionParams8.add("layerPropsID="+layerPropsID);
                            actionParams8.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams8.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams8.add("field=DecimationSpacing");
                            actionParams8.add("value=5");
                            actions.add(actionParams8);
                            
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams9.clear();
                            actionParams9.add("layerPropsID="+layerPropsID);
                            actionParams9.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams9.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams9.add("field=RasterizingType.Positive_Fill");
                            actionParams9.add("value=on");
                            actions.add(actionParams9);
							
                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams10.clear();
                            actionParams10.add("layerPropsID="+layerPropsID);
                            actionParams10.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams10.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams10);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set model layer properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.ADD_LAYER5;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.POST_NEAR_SEISMIC_REAL));
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.ADD_LAYER5:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add 6th layer to XSection window #2\n"+resp.getContent());
							break;
                        }
						ids = (ArrayList)resp.getContent();
						//check if the layer was added to the right XSec window
						if (!xsecWID2.equals((String)ids.get(0))) {
						}
						//Check if the layer was added
						if (((String)ids.get(1)).equals("-1")) {
						} else
							layerIDs2[layer2Size++] = (String)ids.get(1);
					case DeliveryLiteControlConstants.SKIP_LAYER5:
						actionState = DeliveryLiteControlConstants.SCROLL_VERTICAL;
                        
                        //turn off broadcast so Window Scale properties can be set in insolation
                        actions.clear(); actionParams1.clear();
                        actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                        actionParams1.add("winID="+xsecWID2);
                        actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                        actionParams1.add("field=Broadcast");
                        actionParams1.add("value=off");
                        actions.add(actionParams1);

						actionParams2.clear();
						actions.add("action="+ControlConstants.SCROLL_ACTION);
						actionParams2.add("winID="+xsecWID2);
						actionParams2.add("scrollbar="+ControlConstants.VERTICAL_SCROLLBAR);
						//get the time of the model's top layer
						actionParams2.add("position="+gui.getMinTime());
						actions.add(actionParams2);

						messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        break;
						
                    case DeliveryLiteControlConstants.SCROLL_VERTICAL:
                         if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot position vertical scrollbar in XSection window #2\n"+resp.getContent());
                        } else {
                            actionState =  DeliveryLiteControlConstants.SET_ANNO_PROPS;

                            //Must set the annotation layer first
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID2);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer2);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID2);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=ep,cdp,mark");
                            actions.add(actionParams1);
							
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.Location");
                            actionParams2.add("value=TOP");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.SynchronizationKey");
                            actionParams3.add("value=ep");
                            actions.add(actionParams3);
                            
                            actionParams7.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams7.add("winID="+xsecWID2);
                            actionParams7.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams7.add("field=Vertical.StepType.Automatic");
                            actionParams7.add("value=on");
                            actions.add(actionParams7);

                            actionParams8.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams8.add("winID="+xsecWID2);
                            actionParams8.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams8.add("field=Vertical.Appearance.Location");
                            actionParams8.add("value=LEFT");
                            actions.add(actionParams8);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID2);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties\n"+resp.getContent());
                        } else {
							actions.clear(); actionParams1.clear();
							actionState = isNearAndFar ?  DeliveryLiteControlConstants.HIDE_LAYER : 
							DeliveryLiteControlConstants.HIDE_LAYER1;
							
                            if (isNearAndFar) {
                                if (!hiddenLayer2.equals("")) {
                                    actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
									actionParams1.add("winID="+xsecWID2);
                                    actionParams1.add("layerID="+ hiddenLayer2);
                                    actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                                } else
                                    actions.add("action="+ControlConstants.NOP_ACTION);
                            } else {
                                if (!hiddenLayer1.equals("")) {
                                    actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
									actionParams1.add("winID="+xsecWID2);
                                    actionParams1.add("layerID="+ hiddenLayer1);
                                    actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                                } else
                                    actions.add("action="+ControlConstants.NOP_ACTION);
                            }
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					//Hide PRIOR_FAR_SEISMIC_REAL layer
                    case DeliveryLiteControlConstants.HIDE_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide seismic layer\n"+resp.getContent());
                        } else {
							actionState =  DeliveryLiteControlConstants.HIDE_LAYER1;
							
                            actions.clear(); actionParams1.clear();
							if (!hiddenLayer1.equals("")) {
								actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
								actionParams1.add("winID="+xsecWID2);
								actionParams1.add("layerID="+hiddenLayer1);
								actionParams1.add("type="+ControlConstants.MODEL_LAYER);
							} else
								actions.add("action="+ControlConstants.NOP_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					//Hide PRIOR_NEAR_SEISMIC_REAL layer
                    case DeliveryLiteControlConstants.HIDE_LAYER1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide seismic layer\n"+resp.getContent());
                        } else {
                            //set Window scale properties
                            actionState =  DeliveryLiteControlConstants.SET_SCALE_PROPS;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=HorizontalScale");
                            actionParams1.add("value=36.1");
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams2.add("field=VerticalScale");
                            actionParams2.add("value=0.08");
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams3);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Window Scale properties\n"+resp.getContent());
                        } else {
							actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);
                            
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID2);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams6.add("field=ListenToCursorPosition");
                            actionParams6.add("value=off");
                            actions.add(actionParams6);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);
							
                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID2);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=SynchronizeVerticalScrolling");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);
                            
                            actionParams7.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams7.add("winID="+xsecWID2);
                            actionParams7.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams7.add("field=SynchronizeHorizontalScrolling");
                            actionParams7.add("value=off");
                            actions.add(actionParams7);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID2);
                            actionParams5.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams5);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.SET_SYNC_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set sync properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight2 = 600; xsecWidth2 = 600;
                            //deterimine positon of window next to XSection window #1
                            xsecX2 = xsecWidth1+1; xsecY2 = 0;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("width="+xsecWidth2);
                            actionParams1.add("height="+xsecHeight2);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("x="+xsecX2);
                            actionParams2.add("y="+xsecY2);
                            actions.add(actionParams2);
/*
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID2);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);
*/
							//redraw window so hidden layers displayed as hidden
                            actions.add("action="+ControlConstants.REDRAW_WINDOW_ACTION);
                            actionParams4.clear();
                            actionParams4.add("winID="+xsecWID2);
							actions.add(actionParams4);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSec window #2\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set divider in window #2\n"+resp.getContent());
                        } else {
                            //it is now safe to trun broadcast on
                            actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS2;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID2);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID2);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case DeliveryLiteControlConstants.SET_SYNC_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn on broadcast\n"+resp.getContent());
                        } else {
                            stepState = DeliveryLiteControlConstants.GET_LAYER_PROPS_WIN3;
							actionState = isNearAndFar ?  DeliveryLiteControlConstants.GET_LAYER_PROPS : DeliveryLiteControlConstants.GET_LAYER_PROPS1;
							
							//NOTE: The near_wavelet and far_wavelet always exist
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+(isNearAndFar ? datasets.getPathname(DatasetParams.FAR_WAVELET) : datasets.getPathname(DatasetParams.NEAR_WAVELET)));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            //Create XSec #3
            case DeliveryLiteControlConstants.GET_LAYER_PROPS_WIN3:
                switch (actionState) {
					case DeliveryLiteControlConstants.GET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for 1st layer in XSection window #3\n"+resp.getContent());
                        } else {
                            layerPropsID3[layerProps3Size++] = layerPropsID =  (String)resp.getContent();
                            
                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 1st layer in XSection window #3\n"+resp.getContent());
                        } else {
							actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
							actionParams1.add("value=0.2");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=RMS");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=RasterizingType.Wiggle_Trace");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=RasterizingType.Positive_Fill");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=ClippingFactor");
                            actionParams5.add("value=4");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=DecimationSpacing");
                            actionParams6.add("value=5");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams7);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 1st layer's properties\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.CREATE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type=XSection");
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+(isNearAndFar ? datasets.getPathname(DatasetParams.FAR_WAVELET) : datasets.getPathname(DatasetParams.NEAR_WAVELET)));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
						}
						break;
					
                    case DeliveryLiteControlConstants.CREATE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot open XSection window #3\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID3 = (String)ids.get(0);
                            layerIDs3[layer3Size++] = hiddenLayer1 = (String)ids.get(1);
							
							actionState = DeliveryLiteControlConstants.GET_LAYER_PROPS1;
							
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.GET_LAYER_PROPS_ACTION);
                            actionParams1.add("format=BHP-SU");
                            actionParams1.add("file="+datasets.getPathname(DatasetParams.NEAR_WAVELET));
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
							
					case DeliveryLiteControlConstants.GET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get layer properties for 2nd layer in XSection window #3\n"+resp.getContent());
                        } else {
                            layerPropsID3[layerProps3Size++] = layerPropsID =  (String)resp.getContent();

                            actionState = DeliveryLiteControlConstants.GET_DATASET_SUMMARY1;
                            
                            actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.GET_DATASET_SUMMARY_ACTION);
							actionParams1.add("layerPropsID="+layerPropsID);
							actions.add(actionParams1);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.GET_DATASET_SUMMARY1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot get dataset summary for 2nd layer in XSection window #3\n"+resp.getContent());
                        } else {
                            actionState =  DeliveryLiteControlConstants.SET_LAYER_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams1.add("layerPropsID="+layerPropsID);
                            actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams1.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams1.add("field=Normalization.Scale");
							actionParams1.add("value=0.2");
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams2.clear();
                            actionParams2.add("layerPropsID="+layerPropsID);
                            actionParams2.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams2.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams2.add("field=Normalization.Type");
                            actionParams2.add("value=RMS");
                            actions.add(actionParams2);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams3.clear();
                            actionParams3.add("layerPropsID="+layerPropsID);
                            actionParams3.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams3.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams3.add("field=RasterizingType.Wiggle_Trace");
                            actionParams3.add("value=on");
                            actions.add(actionParams3);

                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
                            actionParams4.clear();
                            actionParams4.add("layerPropsID="+layerPropsID);
                            actionParams4.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams4.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams4.add("field=RasterizingType.Positive_Fill");
                            actionParams4.add("value=on");
                            actions.add(actionParams4);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams5.clear();
                            actionParams5.add("layerPropsID="+layerPropsID);
                            actionParams5.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams5.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams5.add("field=ClippingFactor");
                            actionParams5.add("value=4");
                            actions.add(actionParams5);
							
                            actions.add("action="+ControlConstants.SET_LAYER_PROP_ACTION);
							actionParams6.clear();
                            actionParams6.add("layerPropsID="+layerPropsID);
                            actionParams6.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams6.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actionParams6.add("field=DecimationSpacing");
                            actionParams6.add("value=5");
                            actions.add(actionParams6);

                            actions.add("action="+ControlConstants.OK_LAYER_PROPS_ACTION);
                            actionParams7.clear();
                            actionParams7.add("layerPropsID="+layerPropsID);
                            actionParams7.add("type="+ControlConstants.SEISMIC_LAYER);
                            actionParams7.add("props="+ControlConstants.LAYER_DISPLAY_PROPERTIES);
                            actions.add(actionParams7);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.SET_LAYER_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set 2nd layer's display properties in XSection window #3\n"+resp.getContent());
                        } else {
                            actionState = isNearAndFar ? DeliveryLiteControlConstants.ADD_LAYER : DeliveryLiteControlConstants.CREATE_WINDOW1;

                            actions.clear(); actionParams1.clear();
                            if (isNearAndFar) {
                                actions.add("action="+ControlConstants.ADD_LAYER_ACTION);
                                actionParams1.add("layerPropsID="+layerPropsID);
                                actionParams1.add("winID="+xsecWID3);
                                actionParams1.add("format=BHP-SU");
                                actionParams1.add("file="+datasets.getPathname(DatasetParams.NEAR_WAVELET));
                                actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            } else {
								actions.add("action="+ControlConstants.OPEN_WINDOW_ACTION);
                                actionParams1.add("layerPropsID="+layerPropsID);
								actionParams1.add("type=XSection");
								actionParams1.add("format=BHP-SU");
								actionParams1.add("file="+datasets.getPathname(DatasetParams.NEAR_WAVELET));
								actions.add(actionParams1);
                            }
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
					case DeliveryLiteControlConstants.CREATE_WINDOW1:
                    case DeliveryLiteControlConstants.ADD_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot add near layer to XSection window #3\n"+resp.getContent());
                        } else {
                            ArrayList ids = (ArrayList)resp.getContent();
                            xsecWID3 = (String)ids.get(0);
                            layerIDs3[layer3Size++] = annotatedLayer3 = (String)ids.get(1);
                            
                            //turn off broadcast so Window Scale properties can be set in insolation
                            actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case DeliveryLiteControlConstants.SET_SYNC_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn off broadcast\n"+resp.getContent());
                        } else {
/*Unnecessary since doing 'zoom all' at the end
                            //set Window scale properties
                            actionState =  DeliveryLiteControlConstants.SET_SCALE_PROPS;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams1.add("field=HorizontalScale");
                            actionParams1.add("value=0.152");
                            actions.add(actionParams1);
                            
                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actionParams2.add("field=VerticalScale");
                            actionParams2.add("value=0.07");
                            actions.add(actionParams2);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_SCALE_PROPERTIES);
                            actions.add(actionParams3);
                            
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                        
                    case DeliveryLiteControlConstants.SET_SCALE_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Window Scale properties\n"+resp.getContent());
                        } else {
*/
							actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS1;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=ListenToHorizontalScale");
                            actionParams1.add("value=off");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams2.add("field=ListenToVerticalScale");
                            actionParams2.add("value=on");
                            actions.add(actionParams2);

                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams3.add("field=ListenToScrollPosition");
                            actionParams3.add("value=off");
                            actions.add(actionParams3);

                            actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams4.add("field=ListenToCursorPosition");
                            actionParams4.add("value=off");
                            actions.add(actionParams4);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams6.clear();
                            actionParams6.add("winID="+xsecWID3);
                            actionParams6.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams6);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                   case DeliveryLiteControlConstants.SET_SYNC_PROPS1:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Synchronization properties for XSection window #3\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.SET_ANNO_PROPS ;

                            //Must set the annotation layer first
                            actions.clear(); actionParams4.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams4.add("winID="+xsecWID3);
                            actionParams4.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams4.add("field=AnnotatedLayer");
                            actionParams4.add("value="+annotatedLayer3);
                            actions.add(actionParams4);
                            
                            //Clear the list of horizontal selected keys
                            actionParams6.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams6.add("winID="+xsecWID3);
                            actionParams6.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams6.add("field=Horizontal.SelectedKeys");
                            actionParams6.add("value=");
                            actions.add(actionParams6);
                            
                            actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams1.add("field=Horizontal.SelectedKeys");
                            actionParams1.add("value=ep");
                            actions.add(actionParams1);

                            actionParams2.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams2.add("field=Horizontal.SynchronizationKey");
                            actionParams2.add("value=ep");
                            actions.add(actionParams2);
							
                            actionParams3.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams3.add("field=Horizontal.Location");
                            actionParams3.add("value=TOP");
                            actions.add(actionParams3);
                            
                            actionParams7.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams7.add("winID="+xsecWID3);
                            actionParams7.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams7.add("field=Vertical.StepType.Automatic");
                            actionParams7.add("value=on");
                            actions.add(actionParams7);

                            actionParams8.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams8.add("winID="+xsecWID3);
                            actionParams8.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actionParams8.add("field=Vertical.Appearance.Location");
                            actionParams8.add("value=LEFT");
                            actions.add(actionParams8);

                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams5.clear();
                            actionParams5.add("winID="+xsecWID3);
                            actionParams5.add("props="+ControlConstants.WIN_ANNO_PROPERTIES);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.SET_ANNO_PROPS:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set Annotation properties\n"+resp.getContent());
                        } else {
                            actionState =  DeliveryLiteControlConstants.HIDE_LAYER;

                            actions.clear(); actionParams1.clear();
                            if (isNearAndFar) {
                                actions.add("action="+ControlConstants.HIDE_LAYER_ACTION);
								actionParams1.add("winID="+xsecWID3);
                                actionParams1.add("layerID="+hiddenLayer1);
                                actionParams1.add("type="+ControlConstants.SEISMIC_LAYER);
                            } else 
                                actions.add("action="+ControlConstants.NOP_ACTION);
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.HIDE_LAYER:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot hide seismic layer in XSection window #3\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.WIDEN_SPLIT_PANE;

                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_DIVIDER_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("splitPane="+ControlConstants.LAYER_EXPLORER);
                            actionParams1.add("dividerLoc=100");
                            actions.add(actionParams1);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.WIDEN_SPLIT_PANE:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot set the divider in XSection window #3\n"+resp.getContent());
                        } else {
                            actionState = DeliveryLiteControlConstants.RESIZE_WINDOW;

                            actions.clear(); actionParams1.clear();
                            //determine height and width
                            xsecHeight3 = 200; xsecWidth3 = 600;
                            //deterimine positon of window under XSection window #1
                            xsecX3 = xsecX1; xsecY3 = xsecY1 + xsecHeight1 + 1;

                            actions.add("action="+ControlConstants.RESIZE_WINDOW_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("width="+xsecWidth3);
                            actionParams1.add("height="+xsecHeight3);
                            actions.add(actionParams1);

                            actions.add("action="+ControlConstants.POSITION_WINDOW_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("x="+xsecX3);
                            actionParams2.add("y="+xsecY3);
                            actions.add(actionParams2);
							
							//redraw window so hidden layers displayed as hidden
                            actions.add("action="+ControlConstants.REDRAW_WINDOW_ACTION);
                            actionParams4.clear();
                            actionParams4.add("winID="+xsecWID3);
							actions.add(actionParams4);
                            
                            actions.add("action="+ControlConstants.ZOOM_ACTION);
                            actionParams3.clear();
                            actionParams3.add("winID="+xsecWID3);
                            actionParams3.add("direction=All");
                            actions.add(actionParams3);
							
                            actions.add("action="+ControlConstants.RESIZE_VIEWER_ACTION);
                            actionParams5.clear();

                            viewerWidth += xsecX2 + xsecWidth2;
							
                            viewerHeight += xsecY3 + xsecHeight3;

                            actionParams5.add("width="+viewerWidth);
                            actionParams5.add("height="+viewerHeight);
                            actions.add(actionParams5);

                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    case DeliveryLiteControlConstants.RESIZE_WINDOW:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot resize XSection Window #3\n"+resp.getContent());
                        } else {
/*disable for now
                            //it is now safe to turn broadcast on
                            actionState =  DeliveryLiteControlConstants.SET_SYNC_PROPS2;
                            
                            actions.clear(); actionParams1.clear();
                            actions.add("action="+ControlConstants.SET_WIN_PROP_ACTION);
                            actionParams1.add("winID="+xsecWID3);
                            actionParams1.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actionParams1.add("field=Broadcast");
                            actionParams1.add("value=on");
                            actions.add(actionParams1);
                            
                            actions.add("action="+ControlConstants.OK_WIN_PROPS_ACTION);
                            actionParams2.clear();
                            actionParams2.add("winID="+xsecWID3);
                            actionParams2.add("props="+ControlConstants.WIN_SYNC_PROPERTIES);
                            actions.add(actionParams2);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
                            
                    case DeliveryLiteControlConstants.SET_SYNC_PROPS2:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot turn on broadcast\n"+resp.getContent());
                        } else {
*/
							actionState = DeliveryLiteControlConstants.END_PROGRESS_DIALOG;
							
							actions.clear(); actionParams1.clear();
							actions.add("action="+ControlConstants.END_PROGRESS_DIALOG_ACTION);
							actions.add(actionParams1);
							
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;
						
                    case DeliveryLiteControlConstants.END_PROGRESS_DIALOG:
                        if (resp.isAbnormalStatus()) {
                            endControlSession("Cannot end process dialo\n"+resp.getContent());
                        } else {
                            stepState = ControlConstants.SESSION_STEP;
                            actionState = ControlConstants.END_SESSION;

                            actions.clear(); actionParams1.clear();
                            //End the control session
                            actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
                            actionParams1.add("sessionID="+sessionID);
                            actions.add(actionParams1);
                          
                            messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
                        }
                        break;

                    default:
                        //Internal error: No legal action transition state
                        //End a control session
                        endControlSession("Internal Error: No legal action transition for state "+actionState);
                }
                break;

            default:
                //Internal error: No legal step transition state
                //End the control session
                endControlSession("Internal Error: No legal step transition for state "+actionState);
        }
    }
	
	/**
	 * End the control session because of an error. Close the progress dialog.
	 * @param errorMsg The reason why the control session is being terminated.
	 */
	private void endControlSession(String errorMsg) {
		sessionTerminated = true;
		
		//Display reason ending the control session
		JOptionPane.showMessageDialog(gui, errorMsg+"\nCannot continue displaying the results.", "Display Error", JOptionPane.ERROR_MESSAGE);
		
		//End the control session
        stepState = ControlConstants.SESSION_STEP;
        actionState = ControlConstants.END_SESSION;
	
		actions.clear(); actionParams1.clear();
		actions.add("action="+ControlConstants.END_PROGRESS_DIALOG_ACTION);
		actions.add(actionParams1);
		actions.add("action="+ControlConstants.END_CONTROL_SESSION_ACTION);
		actionParams1.add("sessionID="+sessionID);
		actions.add(actionParams1);
		
		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.CONTROL_QIVIEWER_CMD, qiViewerDesc, QIWConstants.ARRAYLIST_TYPE, actions);
	}
    
    /**
     * Initialize the transducer's state variables. Invoked when displaying
     * results is ended so the transducer can be called again.
     */
    private void initState() {
        sessionID = ""; xsecWID = ""; openDiaWID = "";
        layerDiaWID = ""; syncDiaWID = ""; annoDiaWID = "";
        scaleDiaWID = "";
        geofileID = "";
        xsecWID1 = ""; xsecWID2 = ""; xsecWID3 = "";
        xsecWidth1 = 0; xsecHeight1 = 0; xsecWidth2 = 0; xsecHeight2 = 0;
        xsecWidth3 = 0; xsecHeight3 = 0;
        xsecX1 = 0; xsecY1 = 0; xsecX2 = 0; xsecY2 = 0; xsecX3 = 0;
        xsecY3 = 0;
        viewerWidth = 50; viewerHeight = 50;
        layer1Size = 0;
        layerProps1Size = 0;
        layer2Size = 0;
        layerProps2Size = 0;
        layer3Size = 0;
        layerProps3Size = 0;
        seismicLayerPropsSize = 0;
        layerPropsID = "";
        fileExists = "yes";
        skipLayer = false;
        isNearAndFar = false;
        layerIdx = 0;
        maxTraceVal = 0;
        maxTraceValsSize = 0;
        hiddenLayer1=""; //PRIOR_NEAR_SEISMIC_ML
        hiddenLayer2=""; //PRIOR_NEAR_SEISMIC
        hiddenLayer3=""; //PRIOR_FAR_SEISMIC_ML
        hiddenLayer4=""; //PRIOR_FAR_SEISMIC
        hiddenLayer5=""; //FAR_WAVELET
        annotatedLayer1 = ""; //near_stack
        annotatedLayer2 = ""; //near_realization
        annotatedLayer3 = ""; //near_wavelet

        stepState = ControlConstants.SESSION_STEP;
        actionState = ControlConstants.START_SESSION;
        
		//Check is forced control session to terminate (due to an error or the
		//user cancelled). If so, the response needs to be ignored.
        ignoreResponse = sessionTerminated ? true : false;
     }
	
	/**
	 * Round up the trace value to the nearest 100
	 * @param tval Trace value as a decimal number
	 * @return Integer trace value rounded up to the nearest 100
	 */
	private int ceiling(String tval) {
		int val = 0;
		try {
			val = Float.valueOf(Float.parseFloat(tval)).intValue();
		} catch (NumberFormatException nef) {
		}
		
		int upper = (val / 100) * 100;
		int rem = val % 100;
		upper += (rem != 0 && val > 0) ? 100 : 0;
		return upper;
	}
	 
	/**
	 * Round down the trace value to the nearest 100
	 * @param tval Trace value as a decimal number
	 * @return Integer trace value rounded down to the nearest 100
	 */
	private int floor(String tval) {
		int val = 0;
		try {
			val = Float.valueOf(Float.parseFloat(tval)).intValue();
		} catch (NumberFormatException nef) {
		}
		
		int lower = (val / 100) * 100;
		int rem = val % 100;
		lower += (rem != 0 && val < 0) ? -100 : 0;
		return lower;
	}
	
	/**
	 * Get the abs max of two integer value either of which can be negative.
	 */
	 private int absMax(int n, int m) {
		 int i1 = n < 0 ? -n : n;
		 int i2 = m < 0 ? -m : m;
		 return i1 < i2 ? i2 : i1;
	 }
	 
    /**
     * Get key's selected range
     * @param range[] Minimum value, maximum value, increment value
     * @return String representation of the range: min-max[incr]
     */
    private String getSelectedRange(double range[]) {
        return String.valueOf(range[0]) + "-" + String.valueOf(range[1]) + "[" + String.valueOf(range[2]) + "]";
    }
}
