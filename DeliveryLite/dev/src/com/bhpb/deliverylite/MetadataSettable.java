/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import com.bhpb.geoio.filesystems.metadata.GeoFileMetadata;


public interface MetadataSettable {
	public void setMetaData(String dataset, GeoFileMetadata metadata, int stackMode, int runMode);
}
