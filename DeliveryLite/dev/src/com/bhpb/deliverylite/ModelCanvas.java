/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.UIManager;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;

import com.bhpb.geographics.util.ColorMap;

public class ModelCanvas extends JPanel {
	Font	font = new java.awt.Font("SansSerif", Font.PLAIN, 12);
	private ModelCanvas canvas;
	private float pixelPerTimeUnit = 0;
	private DeliveryLiteUI parentUI;
	private Property prop;
	private List<Cell> listOfCells = new ArrayList<Cell>();	
	public final int  default_colorbar_width = 20;
	private static NumberFormat floatFormat = NumberFormat.getInstance();
	static{
	floatFormat.setMinimumFractionDigits(1);
	floatFormat.setMaximumFractionDigits(1);
	}
	private Map<String,Property> map;
	private boolean showColorbar = false;
	private boolean modelAfterInversionMode = false;  

	float[] boundries;
	private ViewModelDialog controller;
	public ModelCanvas(DeliveryLiteUI parentUI, ViewModelDialog controller,List<Cell> list, float unit, Property prop, Map<String,Property> map){		
		this.pixelPerTimeUnit = unit;
		listOfCells = list;
		this.prop = prop;
		this.map = map;
		this.parentUI = parentUI;
		this.controller = controller;
		canvas = this;
		addMouseListener(resizeListener);
		addMouseMotionListener(resizeListener);

		setBackground(Color.WHITE);
	}

	public void setShowColorbar(boolean b){
		showColorbar = b;
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		drawModel(g);
	}
	

	
	//public void fitPage(float newPixeoPerTimeUnit){
		//float ratio = newPixeoPerTimeUnit/pixelPerTimeUnit;
		//setScale(ratio);
		//pixelPerTimeUnit = newPixeoPerTimeUnit;
	//}
	
	
	public void setPixelPerTimeUnit(float unit){
		pixelPerTimeUnit = unit;
	}
	

	public List<Cell> getListOfCells(){
		return listOfCells;
	}
	
	public void setProperty(Property prop){
		this.prop = prop;
	}
	
	public Property getProperty(){
		return prop;
	}
	
	public void setListOfCells(List<Cell> list){
		listOfCells = list;
	}

	private void drawModel(Graphics g){
        if(listOfCells == null || listOfCells.size() == 0)
            return;
		for(Cell cell : listOfCells){
			cell.draw(g);
		}
		drawTimeBase(g);
		if(showColorbar)
			drawColorbar(g);
	}
	
	public void setBoundries(float[] boundries){
		if(boundries != null)
			this.boundries = boundries.clone();
	}
	private void drawColorbar(Graphics g){
		if(prop == null || listOfCells == null || listOfCells.size() == 0)
			return;
		//Property timeProp = map.get(DeliveryLiteConstants.COLUMN_NAME_TIME);
		//if(modelAfterInversionMode)
		//	timeProp = map.get("t");
		float max = prop.getMax();
		FontMetrics fm = g.getFontMetrics();
		int maxStringLength = fm.stringWidth(floatFormat.format(max));//for colorbar label
		int maxTimeStringLength = fm.stringWidth(floatFormat.format(boundries[boundries.length-1])); //for time label
		Cell cell = listOfCells.get(0);
		//allocate	10 pixels between the time label and colorbar label
		int x = cell.getP().x + cell.getD().width + maxStringLength + maxTimeStringLength + 10;
		int y = cell.getP().y;
		int width = default_colorbar_width;  
		int height = 10;
		int modelHeight = 0;
		Color[] list = prop.getColorbar().getColorMap().getColors(ColorMap.RAMP);
		List<String> labels = prop.getColorbar().getLabels();
		for(Cell c : listOfCells){
			modelHeight += c.getD().height;
		}
		//height = modelHeight / list.size();
		height = modelHeight / list.length;
		//int size = list.size();
		int size = list.length;
		Color oldColor = g.getColor(); 
		for(int i = 0; i < size; i++){
			g.draw3DRect(x, y+i*height, width, height, true);
			//g.setColor(list.get(i).getColor());
			g.setColor(list[i]);
			g.fillRect(x, y+i*height, width, height);
			//String lab = list.get(i).getLabel();
			String lab = labels.get(i);
			int w = fm.stringWidth(lab);
			g.setColor(oldColor);
			g.drawString(lab, x-w, y+height*i+fm.getHeight()/2);
		}
		//Cell c = listOfCells.get(listOfCells.size()-1);
		//setPreferredSize(new Dimension(x+width,c.getP().y + c.getD().height));
		//revalidate();
	}
	
	public void setModelAfterInversionMode(boolean bool){
		modelAfterInversionMode = bool;
		//if(bool == true){
		//	removeMouseListener(resizeListener);
			//removeMouseMotionListener(resizeListener);
		//}
	}
	
	private void drawTimeBase(Graphics g){
		if(listOfCells == null || listOfCells.size() < 1)
			return;
		Cell lastCell = listOfCells.get(listOfCells.size()-1);
		Point p = lastCell.getP();
		Dimension d = lastCell.getD();
//		FontMetrics fm = g.getFontMetrics();
		String timeBase = parentUI.getTimeBase();
		float fTimeBase = 0;
		if(timeBase != null && timeBase.trim().length() > 0)
			fTimeBase = Float.parseFloat(timeBase);

		timeBase = floatFormat.format(fTimeBase);
		//g.drawString(timeBase, p.x + d.width+4, p.y + d.height + fm.getAscent()/2);
		g.drawString(timeBase, p.x + d.width+4, p.y + d.height);
	}

	public void redraw() {
		repaint();
	}

	private transient MouseInputListener resizeListener = new MouseInputAdapter(){
		public void mouseMoved(MouseEvent me){
			if(popup != null){
				popup.hide();
			}
			if(isXInModelRange(me.getX())){
				int layerNumber = getLayerNumberYInRange(me.getY());
				if(layerNumber > 0){
					String message = "Layer " + layerNumber + " ";
					canvas.setToolTipText(message);
				}else{
					canvas.setToolTipText(null);
				}
				if(isYInMiddleLines(me.getY())){
					setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
					cursor = Cursor.S_RESIZE_CURSOR;
				}else{
					setCursor(Cursor.getDefaultCursor());
					cursor = Cursor.DEFAULT_CURSOR;
				}
			}else{
				if(modelAfterInversionMode){
					ModelDataset dataset = controller.getSelectedModelAfterDataset();
					canvas.setToolTipText(dataset.getDatasetName());;
				}else
					canvas.setToolTipText(null);
					
			}
		}

		public void mouseExited(MouseEvent mouseEvent){
			repaint();
			setCursor(Cursor.getDefaultCursor());
		}

		private int cursor;
		private Point startPos = null;

		private Popup popup;
		private boolean isXInModelRange(float x){
			Cell cell = null;
			if(listOfCells != null && listOfCells.size() > 0){
				cell = (Cell)listOfCells.get(0);
			}
			if(cell == null)
				return false;
			if(x >= cell.getP().x && x <= cell.getP().x + cell.getD().width)
				return true;
			return false;
		}
		
		private int getLayerNumberYInRange(float y){
			for(int i = 0; i < listOfCells.size(); i++){
				Cell cell = listOfCells.get(i);
				if(y >= cell.getP().y && y <= cell.getP().y + cell.getD().height)
					return i+1;
			}
			return -1;
		}
		
		private Cell getCellYInRange(float y){
			for(int i = 0; i < listOfCells.size(); i++){
				Cell cell = listOfCells.get(i);
				if(y >= cell.getP().y && y <= cell.getP().y + cell.getD().height)
					return cell;
			}
			return null;
		}

		private boolean isYInMiddleLines(int y){
			for(int i = 1; i < listOfCells.size(); i++){
				Cell cell = listOfCells.get(i);
				if(y >= cell.getP().y - 4 && y <= cell.getP().y + 4)
					return true;
			}
			return false;
		}

		public void mousePressed(MouseEvent me){
			if(modelAfterInversionMode)
				return;
			if(isXInModelRange(me.getX()) && isYInMiddleLines(me.getY())){
				startPos = me.getPoint();
				setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
				cursor = Cursor.S_RESIZE_CURSOR;
			}
		}

		public void mouseDragged(MouseEvent me){
			if(modelAfterInversionMode)
				return;
			if(startPos!=null){
				int dx = me.getX()-startPos.x;
				int dy = me.getY()-startPos.y;
				switch(cursor){
				case Cursor.N_RESIZE_CURSOR:
					setBounds(getX(), getY()+dy, getWidth(), getHeight()-dy);
					didResized();
					break;
				case Cursor.S_RESIZE_CURSOR:
					break;
				case Cursor.W_RESIZE_CURSOR:
					setBounds(getX()+dx, getY(), getWidth()-dx, getHeight());
					didResized();
					break;
				case Cursor.E_RESIZE_CURSOR:
					setBounds(getX(), getY(), getWidth()+dx, getHeight());
					startPos = me.getPoint();
					didResized();
					break;
				case Cursor.NW_RESIZE_CURSOR:
					setBounds(getX()+dx, getY()+dy, getWidth()-dx, getHeight()-dy);
					didResized();
					break;
				case Cursor.NE_RESIZE_CURSOR:
					setBounds(getX(), getY()+dy, getWidth()+dx, getHeight()-dy);
					startPos = new Point(me.getX(), startPos.y);
					didResized();
					break;
				case Cursor.SW_RESIZE_CURSOR:
					setBounds(getX()+dx, getY(), getWidth()-dx, getHeight()+dy);
					startPos = new Point(startPos.x, me.getY());
					didResized();
					break;
				case Cursor.SE_RESIZE_CURSOR:
					setBounds(getX(), getY(), getWidth()+dx, getHeight()+dy);
					startPos = me.getPoint();
					didResized();
					break;
				case Cursor.MOVE_CURSOR:
					Rectangle bounds = getBounds();
					bounds.translate(dx, dy);
					setBounds(bounds);
					didResized();
				}

				// cursor shouldn't change while dragging
				setCursor(Cursor.getPredefinedCursor(cursor));
			}
		}
		private int findLayerByMouseClicks(Point p){
			for(int i = 0; i < listOfCells.size(); i++){
				Cell cell = listOfCells.get(i);
				if((cell.getP().x < p.x && p.x < cell.getP().x + cell.getD().width) && 
						(cell.getP().y < p.y && p.y < cell.getP().y + cell.getD().height))
					return i;
			}
			return -1;
		}

		private int findColumnByName(String name, JTable table){
			for(int j = 0; j < table.getModel().getColumnCount(); j++){
				if(name.equals(table.getModel().getColumnName(j)))
					return j;
			}
			return -1;
		}
		
		public void mouseClicked(MouseEvent e){
			if(popup != null){
				popup.hide();
			}
			if(e.getButton() == MouseEvent.BUTTON3){
				if(isXInModelRange(e.getX())){
					int layerNumber = getLayerNumberYInRange(e.getY());
					if(layerNumber > 0){
						Cell cell = listOfCells.get(layerNumber-1);
						String message = "Layer " + layerNumber + "\n";
						message += "Property Name: " + cell.getName() + "\n";
						message += "Property Value: " + cell.getValue() + "\n";
						message += "Sigma time: " + cell.getSigmaTime();
						JTextPane textPane = new JTextPane();
						textPane.setText(message);
						Color bkcolor = UIManager.getColor("ToolTip.background");
						textPane.setBackground(bkcolor);
						textPane.setBorder(UIManager.getBorder("ToolTip.border"));
						popup = PopupFactory.getSharedInstance().getPopup(
								canvas, textPane,
								canvas.getLocationOnScreen().x+e.getX(),
								canvas.getLocationOnScreen().y+e.getY() + 30);
						popup.show();
					}else{
						if(popup != null){
							popup.hide();
							popup = null;
						}
					}
				}else{
					if(popup != null){
						popup.hide();
						popup = null;
					}
				}
			}
			if(!modelAfterInversionMode){
				if(e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2){
					Point p = e.getPoint();
					int layer = findLayerByMouseClicks(p);

					if(layer == -1)
						return;
					Cell cell = listOfCells.get(layer);
					int col = findColumnByName(cell.getName(),parentUI.getLayerTable());
					if(col == -1)
						return;
					if(!parentUI.getLayerTable().getModel().isCellEditable(layer, col))
						return;
	
					DEditDialog dialog = new DEditDialog(null);
					JInternalFrame iframe = parentUI.getShowModelDialog().getModelBeforeInternalFrame();
					String newVal = dialog.showValueDialog(iframe, "Edit Property Value", new DEditDataType(DEditDataType.CONSTRAINT,DEditDataType.FLOAT,"",null), String.valueOf(cell.getValue()));
					if (newVal == null)
						return;
	
					parentUI.getLayerTable().setValueAt(newVal, layer, col);
					cell.setValue(Float.parseFloat(newVal));
					repaint();
				}
			}
		}

		public void mouseReleased(MouseEvent mouseEvent){
			if(startPos!=null){
//				int dx = mouseEvent.getX()-startPos.x;
				int dy = mouseEvent.getY()-startPos.y;
				for(int i = 1; i < listOfCells.size(); i++){
					Cell cell = listOfCells.get(i);
					if(cell.getP().y - 4 <= startPos.y && cell.getP().y + 4 >= startPos.y){
						Cell cellUp = listOfCells.get(i-1);
						if(dy < 0){  //stop dragging over border that is beyond the border of the other layer 
							if(Math.abs(dy) > cellUp.getD().height)
								dy = cellUp.getD().height * -1;
						}else{
							if(Math.abs(dy) > cell.getD().height)
								dy = cell.getD().height;
						}
						cell.setP(new Point(cell.getP().x, cell.getP().y + dy));
						float timeDiff = dy / pixelPerTimeUnit;
						dy *= -1;
						cell.setD(new Dimension(cell.getD().width, cell.getD().height + dy));
						float newTime = cell.getTime() + timeDiff;
						cell.setTime(newTime);
						int col = findColumnByName(DeliveryLiteConstants.COLUMN_NAME_TIME, parentUI.getLayerTable());
						parentUI.getLayerTable().getModel().setValueAt(String.valueOf(newTime), i, col);
						//Cell cellUp = listOfCells.get(i-1);
						dy *= -1;
						cellUp.setD(new Dimension(cellUp.getD().width, cellUp.getD().height + dy));
						break;
					}
				}
				startPos = null;
				canvas.redraw();
			}
		};

		private void didResized(){
			if(getParent()!=null){
				getParent().repaint();
				invalidate();
				((JComponent)getParent()).revalidate();
			}
		}
	};
}