/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Title:        ShowScriptAction <br><br>
 * Description:  This action triggered from inside BasePanel's sub element
 *               <br><br>
 * @version 1.0
 */
public class ShowScriptAction implements ActionListener {
	private DeliveryLitePlugin  plugin;
	
    
    public  ShowScriptAction(DeliveryLitePlugin  plugin) {
    	this.plugin  = plugin;
    }
    

    public void actionPerformed(ActionEvent e){
    	plugin.showScriptPanels();
    }
            
}
