/*
 ###########################################################################
 # DeliveryLite - an open-source, trace-based Bayesian seismic inversion code..
 # Copyright (C) 2007-2009  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.deliverylite;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.util.Properties;

import javax.swing.*;

/**
 * Title:        DeliveryLite <br><br>
 * Description:  This action will pop up a dialog showing basic
 *               information of the program. <br><br>
 * @ Charlie Jiang
 * @ version 1.0
 */

public class About extends JDialog{
    //public class About implements ActionListener {
    private ImageIcon icon;
    private Component parent;
    private Properties props;
    public  About(Component parent, Properties props) {
        super(JOptionPane.getFrameForComponent(parent), "About DeliveryLite");
        this.parent = parent;
        this.props = props;
          icon = new ImageIcon(this.getClass().getResource("/images/BHP_Logo.gif"));
          javax.swing.SwingUtilities.invokeLater(new Runnable() {
              public void run() {
                  createAndShowGUI();
              }
          });

    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public void createAndShowGUI(){
        JEditorPane aboutTextEditorPane = new JEditorPane();
        aboutTextEditorPane.setEditable(false);
        aboutTextEditorPane.setContentType("text/html");

        aboutTextEditorPane.setText(getAboutHtmlText());
        aboutTextEditorPane.setBorder(BorderFactory.createLineBorder(Color.black));

        //final JDialog dialog = new JDialog(JOptionPane.getRootFrame(), "About DeliveryLite");
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);
        panel.add(new JLabel(icon), BorderLayout.CENTER);
        //dialog.getContentPane().add(new JLabel(_icon), BorderLayout.WEST);
        getContentPane().add(panel, BorderLayout.WEST);
        getContentPane().add(aboutTextEditorPane, BorderLayout.EAST);
        JButton okButton = new JButton("Close");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dispose();
            }
        });
        JPanel bpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bpanel.add(okButton);
        getContentPane().add(bpanel, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(parent);
        setVisible(true);
    }
    private String getAboutHtmlText(){
        StringBuffer sb = new StringBuffer();
        sb.append("<font face=\"Arial\" size=3><b>");
        sb.append("DeliveryLite</b> - a delivery for a single seismic trace inversion<br>");
        sb.append("This version is only for delivery experiment, It is a good starting point to learn full delivery: <br>");
        sb.append("See full delivery if you want to handle full-scale seismic data");
        sb.append("<br><br>");

        String versionProp = "";
        String buildProp = "";
        if(props != null){
            versionProp = props.getProperty("project.version");;
            buildProp = props.getProperty("project.build");
        }

        if(versionProp == null) {
             versionProp = "";
        }
        if(buildProp == null) {
             buildProp = "";
        }

        sb.append("Version: " + versionProp + "<br>");
        sb.append("Build id: " + buildProp + "<br><br>");
        sb.append("Copyright (C) 2007-2009 BHP Billiton Petroleum; BHP Billiton Confidential<br>");
        sb.append("<br><br>");
        sb.append("</font>");
        return sb.toString();

    }
}
