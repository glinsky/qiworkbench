#!/bin/sh
source ~/.bashrc
alias rm="rm -f"
#set -x
PATH=${PATH}:${SGE_O_PATH}

# Parameters specified in the qiProject
# qiSpace containing one or more projects
export QISPACE="<QI_SPACE>"
# location of project relative to $QISPACE
export QIPROJECT="<QI_PROJECT_LOCATION>"
# location of datasets relative to $QISPACE/$QIPROJECT
export QIDATASETS="<QI_DATASETS>"
# location of xml directory relative to $QISPACE/$QIPROJECT
#export QIXML="<QI_XML>"
# location of tmp directory relative to $QISPACE/$QIPROJECT
export QITMP="<QI_TMP>"
# list of seismic directories separated by a line separator (\n)
export QISEISMICS="<QI_SEISMICS>"
model_xml=<XML_MODEL>

project_directory=$QISPACE/$QIPROJECT
tmp=${QITMP}
delivery_xml=${tmp}/${model_xml}
number_realizations=<NUMBER_REALIZATIONS>
near_stack_name=<NEAR_STACK_NAME>
near_wavelet_name=<NEAR_WAVELET_NAME>
far_stack_name=<FAR_STACK_NAME>
far_wavelet_name=<FAR_WAVELET_NAME>
ep=<EP>
cdp=<CDP>
output_file_name=<OUTPUT_BASENAME>
min_time=<MIN_TIME>
max_time=<MAX_TIME>
type=<RUN_TYPE>
stack_list=<STACK_TYPE>
input_near_seismic=${tmp}/near_input_seismic.su
input_far_seismic=${tmp}/far_input_seismic.su
input_near_wavelet=${tmp}/near_wavelet.su
input_far_wavelet=${tmp}/far_wavelet.su


cd ${project_directory}

 # DO PRIOR INVERSION

bhpread endian=1 keys=ep,cdp keylist=${ep}:${cdp} pathlist=${QIDATASETS}/${near_stack_name}.dat filename=${near_stack_name} \
> ${input_near_seismic}

return_code=$?
echo "return_code=$return_code bhpread >"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

bhpread endian=1 keys=ep,cdp keylist=${ep}:${cdp} pathlist=${QIDATASETS}/${far_stack_name}.dat filename=${far_stack_name} \
> ${input_far_seismic}

return_code=$?
echo "return_code=$return_code bhpread >"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

bhpread endian=1 keys=ep,cdp keylist=*:* pathlist=${QIDATASETS}/${near_wavelet_name}.dat filename=${near_wavelet_name} \
> ${input_near_wavelet}

return_code=$?
echo "return_code=$return_code bhpread >"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

bhpread endian=1 keys=ep,cdp keylist=*:* pathlist=${QIDATASETS}/${far_wavelet_name}.dat filename=${far_wavelet_name} \
> ${input_far_wavelet}

return_code=$?
echo "return_code=$return_code bhpread >"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

rm ${tmp}/${output_file_name}_property_names.txt

delivery -IS -PD ${delivery_xml} -RWS -v 3 --Raftery-auto-decimation -o ${tmp}/${output_file_name}_${type}_${stack_list}_realizations.su \
-N ${number_realizations} --BHPcommand ${tmp}/${output_file_name}_property_names.txt 

return_code=$?
echo "return_code=$return_code delivery -IS"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat

bhpio delete=yes filename=${output_file_name}_${type}_${stack_list}_realizations pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat

cat tmp/${output_file_name}_${type}_${stack_list}_realizations.su \
| bhpwritecube init=yes stdin_endian=1 pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 key3=mark,1,1,${number_realizations} \
`cat ${tmp}/${output_file_name}_property_names.txt`

return_code=$?
echo "return_code=$return_code cat | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # GENERATE ML MODEL

delivery -p ${delivery_xml} -v 3 -o ${tmp}/${output_file_name}_${type}_${stack_list}_ml.su \
--BHPcommand ${tmp}/${output_file_name}_property_names.txt 

return_code=$?
echo "return_code=$return_code"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_${stack_list}_ml.dat

bhpio delete=yes filename=${output_file_name}_${type}_${stack_list}_ml pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_ml.dat

cat tmp/${output_file_name}_${type}_${stack_list}_ml.su \
| bhpwritecube init=yes stdin_endian=1 pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_ml.dat \
filename=${output_file_name}_${type}_${stack_list}_ml key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 \
 `cat ${tmp}/${output_file_name}_property_names.txt`

return_code=$?
echo "return_code=$return_code cat | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # FORM SEISMIC OF NEAR and FAR

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_near_seismic_realizations.dat

bhpio delete=yes filename=${output_file_name}_${type}_near_seismic_realizations \
pathlist=${QIDATASETS}/${output_file_name}_${type}_near_seismic_realizations.dat

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_far_seismic_realizations.dat

bhpio delete=yes filename=${output_file_name}_${type}_far_seismic_realizations \
pathlist=${QIDATASETS}/${output_file_name}_${type}_far_seismic_realizations.dat

  # Near
bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser -s ${min_time} ${max_time} R_near ${input_near_wavelet}  \
| bhpwritecube stdin_endian=1 init=yes key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 key3=mark,1,1,${number_realizations} \
pathlist=${QIDATASETS}/${output_file_name}_${type}_near_seismic_realizations.dat \
filename=${output_file_name}_${type}_near_seismic_realizations

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

  # far
bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser -s ${min_time} ${max_time} R_far ${input_far_wavelet}  \
| bhpwritecube stdin_endian=1 init=yes key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 key3=mark,1,1,${number_realizations} \
pathlist=${QIDATASETS}/${output_file_name}_${type}_far_seismic_realizations.dat \
filename=${output_file_name}_${type}_far_seismic_realizations

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # NEAR + FAR SEISMIC OF MEDIAN CHI SQUARE

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_near_seismic.dat

bhpio delete=yes filename=${output_file_name}_${type}_near_seismic pathlist=${QIDATASETS}/${output_file_name}_${type}_near_seismic.dat

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_far_seismic.dat

bhpio delete=yes filename=${output_file_name}_${type}_far_seismic pathlist=${QIDATASETS}/${output_file_name}_${type}_far_seismic.dat


bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser -s ${min_time} ${max_time} R_near ${input_near_wavelet}  --quantile seismic_chisquare 1 50  \
| bhpwritecube stdin_endian=1 init=yes key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 pathlist=${QIDATASETS}/${output_file_name}_${type}_near_seismic.dat \
filename=${output_file_name}_${type}_near_seismic

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser -s ${min_time} ${max_time} R_far ${input_far_wavelet}  --quantile seismic_chisquare 1 50  \
| bhpwritecube stdin_endian=1 init=yes key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 pathlist=${QIDATASETS}/${output_file_name}_${type}_far_seismic.dat \
filename=${output_file_name}_${type}_far_seismic

return_code=$?
echo "return_code=$return_code"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # NEAR + FAR SEISMIC OF ML

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_near_seismic_ml.dat

bhpio delete=yes filename=${output_file_name}_${type}_near_seismic_ml pathlist=${QIDATASETS}/${output_file_name}_${type}_near_seismic_ml.dat

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_far_seismic_ml.dat

bhpio delete=yes filename=${output_file_name}_${type}_far_seismic_ml pathlist=${QIDATASETS}/${output_file_name}_${type}_far_seismic_ml.dat

bhpread endian=1 keys=ep,cdp keylist=*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_ml.dat \
filename=${output_file_name}_${type}_${stack_list}_ml \
| deliveryAnalyser -s ${min_time} ${max_time} R_near ${input_near_wavelet}  \
| bhpwritecube stdin_endian=1 init=yes key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 pathlist=${QIDATASETS}/${output_file_name}_${type}_near_seismic_ml.dat \
filename=${output_file_name}_${type}_near_seismic_ml

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

bhpread endian=1 keys=ep,cdp keylist=*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_ml.dat \
filename=${output_file_name}_${type}_${stack_list}_ml \
| deliveryAnalyser -s ${min_time} ${max_time} R_far ${input_far_wavelet}  \
| bhpwritecube stdin_endian=1 init=yes key1=ep,${ep},1,1 key2=cdp,${cdp},1,1 pathlist=${QIDATASETS}/${output_file_name}_${type}_far_seismic_ml.dat \
filename=${output_file_name}_${type}_far_seismic_ml

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

# JG All the same hereon...

 # FORM MEAN MODEL

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_${stack_list}_mean_model.dat

bhpio delete=yes filename=${output_file_name}_${type}_${stack_list}_mean_model pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_mean_model.dat

bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser --BHPcommand ${tmp}/${output_file_name}_property_names.txt  --full-means \
| bhpwritecube init=yes stdin_endian=1 pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_mean_model.dat \
filename=${output_file_name}_${type}_${stack_list}_mean_model key1=ep,${ep},1,1 key2=cdp,${cdp},1,1  \
`cat ${tmp}/${output_file_name}_property_names.txt`

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # FORM STDDEV MODEL

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_${stack_list}_stddev_model.dat

bhpio delete=yes filename=${output_file_name}_${type}_${stack_list}_stddev_model pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_stddev_model.dat

bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser --BHPcommand ${tmp}/${output_file_name}_property_names.txt  --full-stddevs \
| bhpwritecube init=yes stdin_endian=1 pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_stddev_model.dat \
filename=${output_file_name}_${type}_${stack_list}_stddev_model key1=ep,${ep},1,1 key2=cdp,${cdp},1,1  `cat ${tmp}/${output_file_name}_property_names.txt`

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # FORM MEDIAN MODEL

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_${stack_list}_median_model.dat

bhpio delete=yes filename=${output_file_name}_${type}_${stack_list}_median_model pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_median_model.dat

bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser --BHPcommand ${tmp}/${output_file_name}_property_names.txt  --full-quantile 50 \
| bhpwritecube init=yes stdin_endian=1 pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_median_model.dat \
filename=${output_file_name}_${type}_${stack_list}_median_model key1=ep,${ep},1,1 key2=cdp,${cdp},1,1  `cat ${tmp}/${output_file_name}_property_names.txt`

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # FORM P10 MODEL

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_${stack_list}_P10_model.dat

bhpio delete=yes filename=${output_file_name}_${type}_${stack_list}_P10_model pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_P10_model.dat

bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser --BHPcommand ${tmp}/${output_file_name}_property_names.txt  --full-quantile 10 \
| bhpwritecube init=yes stdin_endian=1 pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_P10_model.dat \
filename=${output_file_name}_${type}_${stack_list}_P10_model key1=ep,${ep},1,1 key2=cdp,${cdp},1,1  \
`cat ${tmp}/${output_file_name}_property_names.txt`

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

 # FORM P90 MODEL

echo ${project_directory}/seismic > ${QIDATASETS}/${output_file_name}_${type}_${stack_list}_P90_model.dat

bhpio delete=yes filename=${output_file_name}_${type}_${stack_list}_P90_model pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_P90_model.dat

bhpread endian=1 keys=ep,cdp,mark keylist=*:*:* pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_realizations.dat \
filename=${output_file_name}_${type}_${stack_list}_realizations \
| deliveryAnalyser --BHPcommand ${tmp}/${output_file_name}_property_names.txt  --full-quantile 90 \
| bhpwritecube init=yes stdin_endian=1 pathlist=${QIDATASETS}/${output_file_name}_${type}_${stack_list}_P90_model.dat \
filename=${output_file_name}_${type}_${stack_list}_P90_model key1=ep,${ep},1,1 key2=cdp,${cdp},1,1  \
`cat ${tmp}/${output_file_name}_property_names.txt`

return_code=$?
echo "return_code=$return_code bhpread | deliveryAnalyser | bhpwritecube"
if [ ${return_code} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi

chmod g+w ${project_directory}/${QISEISMICS}/${output_file_name}_${type}_${stack_list}*
exit_status=$?
echo "exit_status=$exit_status"
if [ ${exit_status} != 0 ]
then
	echo "$0 job ends abnormally"
	exit 1
fi
echo "$0 job ends normally"


