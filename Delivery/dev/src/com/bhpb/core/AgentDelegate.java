package com.bhpb.core;

import javax.swing.ImageIcon;
import com.bhpb.qiworkbench.workbench.QiComponentBase;

public abstract class AgentDelegate extends QiComponentBase {
	public static String MODEL = "model";
    
	public Object getObjectByKey(String eventName) {return null;}
	
	abstract public void   resize();
	abstract public String getName();
	abstract public void   toXmlFile(String modelName);
	abstract public String toXmlString(String  modelName);
	abstract public Object getModel(String  modelName, boolean start);
	
	public  static ImageIcon createImageIcon(String filename) {
	 	 String path = "/icons/" + filename;
	 	 return new ImageIcon(path); 
	}	

}
