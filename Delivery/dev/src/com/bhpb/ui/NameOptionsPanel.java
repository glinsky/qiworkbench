/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.ui;


import java.util.ArrayList;
import java.util.List;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import com.bhpb.actions.InnerCompAction;

public class NameOptionsPanel  extends BhpPanel {
	private static final long serialVersionUID = 1L;
	JTextField nameF;
	JComboBox  options;
	JButton    addB, removeB;

	/* Generic Name + Options Panel
	 * params[0] is panel name
	 * the rest is actual params
	 * base pane may have multiple this type of pane
	 * It must implement manageObject
	 */
	public NameOptionsPanel(BasePane base, List<String>params, int width) {
		super(base, "", "");
		this.setLayout(new BorderLayout());
		
		List<String> strList = new ArrayList<String>(); 
		JPanel pane = new JPanel();
		pane.add(new JLabel("Name:"));
		
		nameF = new JTextField("", width);
		int size = params.size();
		if (size > 1) {
			options = new JComboBox() ;
			for (int i = 1; i < size; i++) {
				String option = params.get(i);
				options.addItem(option);
			}
		}
		addB    = new JButton("ADD");
		removeB = new JButton("DEL");
		
		String paneType = params.get(0);
	    strList.add(paneType + UiConst.ADD);
		addB.addActionListener(new InnerCompAction(getBase(), strList));
		strList = new ArrayList<String>(); 
		strList.add(paneType + UiConst.DEL);
		removeB.addActionListener(new InnerCompAction(getBase(), strList));
		pane.add(nameF);
		if (options != null) pane.add(options);
		pane.add(addB);
		pane.add(removeB);
		this.add(pane, BorderLayout.EAST);
	}
	
	public String getText() {
		return nameF.getText();
	}
	
	public int getType() {
		if (options == null) return 0;
		return options.getSelectedIndex();
	}
	
}
