/*
 * <Purpose>
 * <p>
 * Created on January 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.GridLayout;
import com.bhpb.xsdparams.delivery.Inversion;

public class StackPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField name, fileName, minOffset, maxOffset, reflectorTime, stackVelocity;
	private JTextField wavFileName, wavSN, timeAbove, timeBelow;
	
	public StackPanel(String title) {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		if (title != null) {
			TitledBorder tb = new TitledBorder(title);
	        //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	        tb.setTitleColor(Color.PINK);
			this.setBorder(tb);
		}
		this.add(createPart1());
		this.add(Box.createRigidArea(BasePane.VGAP5));
		this.add(createPart3());
		this.add(Box.createRigidArea(BasePane.VGAP5));
		this.add(createPart2());

	}
	
	private JPanel createPart1() {
		JPanel part1 = new JPanel(new GridLayout(6, 2));
		part1.add(new JLabel("Stack Name"));
		name = new JTextField("", 12);
		part1.add(name);
		
		part1.add(new JLabel("File Name"));
		fileName = new JTextField("", 12);
		part1.add(fileName);
		
		part1.add(new JLabel("Min Offset"));
		minOffset = new JTextField("", 12);
		part1.add(minOffset);
		
		part1.add(new JLabel("Max Offset"));
		maxOffset = new JTextField("", 12);
		part1.add(maxOffset);
		
		part1.add(new JLabel("Reflector Time"));
		reflectorTime = new JTextField("", 12);
		part1.add(reflectorTime);
		
		part1.add(new JLabel("Stack Velocity"));
		stackVelocity = new JTextField("", 12);
		part1.add(stackVelocity);
		return part1;
		
	}
	
	private JPanel createPart2() {
		JPanel part2 = new JPanel(new GridLayout(2, 2));
		part2.setBorder(new TitledBorder("Wavelet"));
		part2.add(new JLabel("File Name"));
		wavFileName = new JTextField("", 12);
		part2.add(wavFileName);
		
		part2.add(new JLabel("Signal/Noise"));
		wavSN = new JTextField("", 12);
		part2.add(wavSN);
		return part2;
		
	}
	
	private JPanel createPart3() {
		JPanel part3 = new JPanel(new GridLayout(2, 2));
		part3.add(new JLabel("Time Above"));
		timeAbove = new JTextField("", 12);
		part3.add(timeAbove);
		
		part3.add(new JLabel("Time Below"));
		timeBelow = new JTextField("", 12);
		part3.add(timeBelow);
		return part3;
		
	}
	
	public void populate(Inversion.SeismicData.Stack stack) {
		name.setText(stack.getName());
		fileName.setText(stack.getFilename());
		if (stack.getMinOffset() != null)
		   minOffset.setText(Float.toString(stack.getMinOffset().getValue()));
		if (stack.getMaxOffset() != null)
		   maxOffset.setText(Float.toString(stack.getMaxOffset().getValue()));
		if (stack.getReflectorTime() != null)
		   reflectorTime.setText(Float.toString(stack.getReflectorTime().getValue()));
		if (stack.getStackVelocity() != null)
		   stackVelocity.setText(Float.toString(stack.getStackVelocity().getValue()));
		if (stack.getTimeAbove() != null)
		   timeAbove.setText(Float.toString(stack.getTimeAbove().getValue()));
		if (stack.getTimeBelow() != null)
		   timeBelow.setText(Float.toString(stack.getTimeBelow().getValue()));
		   wavFileName.setText(stack.getWavelet().getFilename());
		if (stack.getWavelet() != null)
		   wavSN.setText(Float.toString(stack.getWavelet().getNoiseRms().getValue()));
	}

}
