/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.ui;
/**
 * Constants used within all UI component
 * 
 * @author Charlie Jiang
 * @version 1.0
 */
public final class UiConst {
    /**
     * Prevent object construction outside of this class.
     */
    private UiConst() {}

    public static final int    MAX_PANE          = 10;
    
    //
    public static final String TOP               = "TOP LAYER";
    public static final String BOTTOM            = "BOTTOM LAYER";
    public static final String BUTTON_UPD        = "Update";
    
    public static final String ADD               = "add";
    public static final String DEL               = "del";
    public static final String ADDDEL            = "adddel";
    
    public static final String GO_NORMAL         = "goNormal";
    public static final String GO_INDEX          = "goIndex";
    public static final String LOAD_MODEL        = "loadModel";
    public static final String LOAD_MODEL_UI     = "uiModel";
    public static final String LOAD_MODEL_DATA   = "dataModel";
    public static final String LOAD_MODEL_OTHER  = "otherData";
    
    public static final String ICON_MIX          = "/icons/mix.jpg";
    public static final String ICON_SAND         = "/icons/sand.jpg";
    public static final String ICON_SHALE        = "/icons/shale.jpg";
    public static final String ICON_HLEP         = "/icons/help.gif";
    public static final String ICON_ARROW        = "/icons/arrow_small.gif";
    public static final String ICON_ARROW_DOWN   = "/icons/arrow_down.gif";
    public static final String ICON_ARROW_OVER   = "/icons/grey_arrow.gif";
    
    public static final String ERR_DUPLICATE_NAME  = "Error: Duplicated Name";

}
