/*
 * Copyright (c) 2005 Sun Microsystems, Inc. All Rights Reserved.
 * Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
*/
package com.bhpb.ui;

import com.bhpb.core.*;
import com.bhpb.model.ui.Config;
import com.bhpb.model.ui.NameValues;
/**
 * A generic tabbed base module
 *
 * @version 1.0
 * @author  Charlie Jiang
 */
public class TabedBasePane extends BasePane {
	private static final long serialVersionUID = 1L;
	
	protected BhpTabbedPane thisTab;
	protected BasePane[] panes = new BasePane[UiConst.MAX_PANE];
	
	public TabedBasePane(AgentDelegate delegate) {
		this(delegate, null, null);
	}

	public TabedBasePane(AgentDelegate delegate, String resourceName, String iconPath) {
		super(delegate, resourceName, iconPath);
	}
	
	public void manageObject(String action, int index) {
		if (UiConst.GO_INDEX.equals(action)) {
			thisTab.setSelectedIndex(0);
			panes[0].manageObject(action);
		}
		panes[index].manageObject(action);	
	}
	
	protected NameValues getElement(String param) {
		NameValues vals = null;
		Config config = (Config)delegate.getModel(UiConst.LOAD_MODEL_UI, false);
		String compName = delegate.getName();
		vals = config.getComponent(compName).getElement(param);
		return vals;
	}
}
