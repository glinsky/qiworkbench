/*
 * Copyright (c) 2005 Sun Microsystems, Inc. All Rights Reserved.
 * Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
*/
package com.bhpb.ui;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
/**
 * A generic Workbench base module
 *
 * @version 1.0
 * @author  Charlie Jiang
 */
public class BhpTabbedPane extends JTabbedPane implements ChangeListener {
	private static final long serialVersionUID = 1L;
	private String  action;
	private IAction base;
	
	public BhpTabbedPane(IAction base, String action) {
		super();
		this.action = action;
		this.base  = base;
	}
	public void stateChanged(ChangeEvent e) {
		SingleSelectionModel model = (SingleSelectionModel) e.getSource();
		int index = model.getSelectedIndex();
		base.manageObject(action, index);
		
	}

}
