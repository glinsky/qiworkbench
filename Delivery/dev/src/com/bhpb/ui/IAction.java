/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.ui;

import java.util.List;
/**
 * A common interface that event action framework
 *
 * @author Charlie Jiang
 * @version 1.0
 */
public interface IAction {
	public void manageObject(String action);
    public void manageObject(String action, int index);
    public void manageObject(List<String>paramList);
	
}
