/*
###########################################################################
# XMLEditor - an editor for creating and editing XML files based on an XML Schema.
# This program module Copyright (C) 2006  BHP Billiton Petroleum
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License Version 2 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA or visit the
# link http://www.gnu.org/licenses/gpl.txt.
#
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.ui;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import com.bhpb.core.AgentDelegate;
import java.awt.BorderLayout;

public class JobStatus extends BasePane {
	private static final long serialVersionUID = 1L;
	JLabel     status;
	JTextArea  textArea1, textArea2;
	JButton    write, run, check, cancel;
	
	public JobStatus(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JSplitPane splitPane 
		  = new JSplitPane(JSplitPane.VERTICAL_SPLIT, 
				           createTextPane1(), 
				           createTextPane2());
	    splitPane.setContinuousLayout(true);
		splitPane.setOneTouchExpandable(true);
	    splitPane.setDividerLocation(100);
        getBasePanel().add(createButtons(), BorderLayout.NORTH);
		getBasePanel().add(splitPane, BorderLayout.CENTER);
	}
	
	private JScrollPane createTextPane1() {
		textArea1 = new JTextArea();
    	textArea1.setEditable(false);
    	JScrollPane scrollPane = new JScrollPane(textArea1);  
 	   return scrollPane;
    }
	private JScrollPane createTextPane2() {
		textArea2 = new JTextArea();
    	textArea2.setEditable(false);
    	JScrollPane scrollPane = new JScrollPane(textArea2);  
 	   return scrollPane;
    }
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		write = new JButton("Write Script");
		run   = new JButton("Run Script");
		check = new JButton("Check Status");
		cancel= new JButton("Cancel Run");
		pane.add(write);
		pane.add(run);
		pane.add(check);
		pane.add(cancel);
		pane.add(Box.createRigidArea(HGAP15));
		buttonPanel.add(pane, BorderLayout.EAST);
		
		JPanel pane1 = new JPanel();
		pane1.add(Box.createRigidArea(HGAP5));
		pane1.add(new JLabel("Status:"));
		status = new JLabel("Job Not Submitted");
		pane1.add(status);
		buttonPanel.add(pane1, BorderLayout.WEST);
		
		return buttonPanel;
	}

}
