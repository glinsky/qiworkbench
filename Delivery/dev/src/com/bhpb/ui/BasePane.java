/*
 * Copyright (c) 2005 Sun Microsystems, Inc. All Rights Reserved.
 * Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
*/
package com.bhpb.ui;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.io.*;
import java.net.*;

import com.bhpb.core.*;

/**
 * A generic Workbench base module
 *
 * @version 1.0
 * @author  Charlie Jiang
 */
public class BasePane extends JApplet implements IAction {
	private static final long serialVersionUID = 1L;
	public static final String ACTION_EXPAND  = "expand";
	public static final String ACTION_CHECKME = "checkMe";

	// The preferred size of the module
	//private int PREFERRED_WIDTH = 680;
	//private int PREFERRED_HEIGHT = 600;

	Border loweredBorder = new CompoundBorder(new SoftBevelBorder(
			SoftBevelBorder.LOWERED), new EmptyBorder(5, 5, 5, 5));

	// Premade convenience dimensions, for use wherever you need 'em.
	public static Dimension HGAP2 = new Dimension(2, 1);

	public static Dimension VGAP2 = new Dimension(1, 2);

	public static Dimension HGAP5 = new Dimension(5, 1);

	public static Dimension VGAP5 = new Dimension(1, 5);

	public static Dimension HGAP10 = new Dimension(10, 1);

	public static Dimension VGAP10 = new Dimension(1, 10);

	public static Dimension HGAP15 = new Dimension(15, 1);

	public static Dimension VGAP15 = new Dimension(1, 15);

	public static Dimension HGAP20 = new Dimension(20, 1);

	public static Dimension VGAP20 = new Dimension(1, 20);

	public static Dimension HGAP25 = new Dimension(25, 1);

	public static Dimension VGAP25 = new Dimension(1, 25);

	public static Dimension HGAP30 = new Dimension(30, 1);

	public static Dimension VGAP30 = new Dimension(1, 30);
	
	public static Dimension HGAP50 = new Dimension(50, 1);

	public static Dimension VGAP50 = new Dimension(1, 50);

	protected AgentDelegate delegate = null;

	private JPanel panel = null;

	private String resourceName = null;

	private String iconPath = null;

	private String sourceCode = null;

	// Resource bundle for internationalized and accessible text
	private ResourceBundle bundle = null;

	public BasePane(AgentDelegate delegate) {
		this(delegate, null, null);
	}

	public BasePane(AgentDelegate delegate, String resourceName, String iconPath) {
		UIManager.put("swing.boldMetal", Boolean.FALSE);
		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		this.resourceName = resourceName;
		this.iconPath = iconPath;
		this.delegate = delegate;

		//loadMetaData(null);
	}

	public void initPanel() {}
	public int  err(String type) {return 0;}
	public void setModelName(String action) {}
    public String getNameEx(String action) {return "";}
    public void doStop() {}
    public void doSwitch() {}
    public void listSelected(int[] sels) {}
    public void tableRowSelected() {}
    public void tableRowsSelected() {}
    public void goModalDialog(String action) {}
    
    public void populate() {}
    public void manageObject(String action) {}
    public void manageObject(String action, int index) {}
    public void manageObject(List<String>paramList) {}
   
	public String getResourceName() {
		return resourceName;
	}

	public JPanel getBasePanel() {
		return panel;
	}

	public AgentDelegate getDelegate() {
		return delegate;
	}

	public String getString(String key) {
		String value = "nada";
		if (bundle == null) {
			if (getDelegate() != null) {
				bundle = (ResourceBundle)getDelegate().getObjectByKey(key);
			} else {
				bundle = ResourceBundle.getBundle("resources.workbench");
			}
		}
		try {
			value = bundle.getString(key);
		} catch (MissingResourceException e) {
			System.out
					.println("java.util.MissingResourceException: Couldn't find value for: "
							+ key);
		}
		return value;
	}

	public char getMnemonic(String key) {
		return (getString(key)).charAt(0);
	}

	public ImageIcon createImageIcon(String filename, String description) {
		if (getDelegate() != null) {
			return getDelegate().createImageIcon(filename);
		} else {
			String path = "/images/" + filename;
			return new ImageIcon(getClass().getResource(path), description);
		}
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void loadMetaData(String metaFile) {
		if (metaFile != null) {
			String filename = metaFile;
			sourceCode = new String("<html><body bgcolor=\"#ffffff\"><pre>");
			InputStream is;
			InputStreamReader isr;
			CodeViewer cv = new CodeViewer();
			URL url;

			try {
				url = getClass().getResource(filename);
				is = url.openStream();
				isr = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(isr);

				// Read one line at a time, htmlize using super-spiffy
				// html java code formating utility from www.CoolServlets.com
				String line = reader.readLine();
				while (line != null) {
					sourceCode += cv.syntaxHighlight(line) + " \n ";
					line = reader.readLine();
				}
				sourceCode += new String("</pre></body></html>");
			} catch (Exception ex) {
				sourceCode = "Could not load file: " + filename;
			}
		}
	}

	public String getName() {
		return getString(getResourceName() + ".name");
	};

	public Icon getIcon() {
		return createImageIcon(iconPath, getResourceName() + ".name");
	};

	public String getToolTip() {
		return getString(getResourceName() + ".tooltip");
	};

	public JPanel createHorizontalPanel(boolean threeD) {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setAlignmentY(TOP_ALIGNMENT);
		p.setAlignmentX(LEFT_ALIGNMENT);
		if (threeD) {
			p.setBorder(loweredBorder);
		}
		return p;
	}

	public JPanel createVerticalPanel(boolean threeD) {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.setAlignmentY(TOP_ALIGNMENT);
		p.setAlignmentX(LEFT_ALIGNMENT);
		if (threeD) {
			p.setBorder(loweredBorder);
		}
		return p;
	}

	public void init() {
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(getBasePanel(), BorderLayout.CENTER);
	}

	void updateDragEnabled(boolean dragEnabled) {
	}
	
	protected JLabel createIconLabel(String tip, String imageFile) {
		ImageIcon icon = new ImageIcon(getClass().getResource(imageFile));
		JLabel iLabel = new JLabel("", icon, SwingConstants.LEFT);
		iLabel.setToolTipText(tip);
		return iLabel;
	}

}
