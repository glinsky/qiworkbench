package com.bhpb.ui;

import javax.swing.table.DefaultTableModel ;

public class BhpTableModel extends DefaultTableModel {
	private static final long serialVersionUID = 1L;

   
    /** Creates a new instance of EntryTableModel */
    public BhpTableModel(String[] strList) {
    	super();
    	
        for (int i = 0; i < strList.length; i++) {
        	addColumn(strList[i]);
		}		
       
    }
    
    public boolean isCellEditable(int row, int col) {
    	return false;
    }
    
}
