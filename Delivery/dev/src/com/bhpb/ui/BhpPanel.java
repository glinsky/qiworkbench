/*
 * Copyright (c) 2005 Sun Microsystems, Inc. All Rights Reserved.
 * Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
*/
package com.bhpb.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import com.bhpb.actions.InnerCompAction;
/**
 * A generic BHP Panel
 *
 * @version 1.0
 * @author  Charlie Jiang
 */
public class BhpPanel extends JPanel implements Comparable {
	private static final long serialVersionUID = 1L;
	protected float     order;
	protected BasePane  base;
	protected String    help, paneName  = "";
	protected JPanel    aLine, cpane;
	protected JCheckBox expand, checkMe;
	
	
	public BhpPanel(BasePane base, String name, String help) {
		super();
		this.base = base;
		this.paneName = name;
		this.help = help;
	}

	public BasePane getBase() {
		return base;
	}

	public void setBase(BasePane base, String name) {
		this.base = base;
		this.paneName = name;	
	}
	
	public void manageObject(List<String> strList) {}
	
	public boolean expanded() {return expand.isSelected();}
	public boolean selected() {return checkMe.isSelected();}
	public void setExpanded(boolean val) {
		expand.setSelected(val);
	}
	
	
	protected void createCheckBox(String param0) {
		expand = new JCheckBox(paneName, 
				     new ImageIcon(getClass().getResource(UiConst.ICON_ARROW)));
		expand.setRolloverIcon(
				     new ImageIcon(getClass().getResource(UiConst.ICON_ARROW_OVER)));
		expand.setSelectedIcon(
				     new ImageIcon(getClass().getResource(UiConst.ICON_ARROW_DOWN)));
		List<String> strList = new ArrayList<String>(); 
		strList.add(param0);
		strList.add(paneName);
		strList.add(BasePane.ACTION_EXPAND);
		expand.addActionListener(new InnerCompAction(getBase(), strList));
		
		checkMe = new JCheckBox("", false);
		checkMe.setOpaque(true);
		strList = new ArrayList<String>(); 
		strList.add(param0);
		strList.add(paneName);
		strList.add(BasePane.ACTION_CHECKME);
		checkMe.addActionListener(new InnerCompAction(getBase(), strList));
	}
	
	//this function needs to be overridden as necessary
	protected JPanel createLine() {
		aLine = new JPanel();
		aLine.setLayout(new BorderLayout());  
		if (cpane == null) cpane = new JPanel();
		if (help  != null) cpane.add(createIconLabel(help, UiConst.ICON_HLEP));
		cpane.add(checkMe);
		aLine.add(expand, BorderLayout.WEST);
		aLine.add(cpane, BorderLayout.EAST);
		return  aLine;
	}
	
	protected JLabel createIconLabel(String tip, String fileName) {
		ImageIcon icon = new ImageIcon(getClass().getResource(fileName));
		JLabel iLabel = new JLabel("", icon, SwingConstants.CENTER);
		iLabel.setToolTipText(tip);
		return iLabel;
	}

	public String getPaneName() {
		return paneName;
	}

	public void setPaneName(String paneName) {
		this.paneName = paneName;
	}
	
	public float getOrder() {
		return order;
	}

	public void setOrder(float order) {
		this.order = order;
	}
	
	public int compareTo(Object anotherObj) throws ClassCastException {
		if (!(anotherObj instanceof BhpPanel))
			throw new ClassCastException("BhpPanel object expected");
		
		float aValue = ((BhpPanel)anotherObj).getOrder();
	    int ret = Float.compare(this.order, aValue);
		return ret;
	}
	
	public void populate(Object obj) {}
}
