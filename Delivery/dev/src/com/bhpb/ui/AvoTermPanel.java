/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import com.bhpb.xsdparams.delivery.Inversion;

import java.awt.Color;
import java.awt.GridLayout;

public class AvoTermPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField a, sigmaA, b, sigmaB;
	
	public AvoTermPanel(String title) {
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		if (title != null) {
			TitledBorder tb = new TitledBorder(title);
	        //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	        tb.setTitleColor(Color.BLUE);
			this.setBorder(tb);
		}
		createPart1();

	}
	
	private void createPart1() {
		JPanel leftPane = new JPanel(new GridLayout(2, 2));
		leftPane.add(new JLabel("A:"));
		a = new JTextField("", 12);
		leftPane.add(a);
		
		leftPane.add(new JLabel("Sigma A:"));
		sigmaA = new JTextField("", 12);
		leftPane.add(sigmaA);
	
		JPanel rightPane = new JPanel(new GridLayout(2, 2));
		rightPane.add(new JLabel("B:"));
		b = new JTextField("", 12);
		rightPane.add(b);
		
		rightPane.add(new JLabel("Sigma B"));
		sigmaB = new JTextField("", 12);
		rightPane.add(sigmaB);
		
		this.add(Box.createRigidArea(BasePane.HGAP15));
		this.add(leftPane);
		this.add(Box.createRigidArea(BasePane.HGAP25));
		this.add(rightPane);
		
	}
	
	public void populate(Inversion.SeismicData.AVOTerms avoTerm) {
		a.setText(Float.toString(avoTerm.getA().getValue()));
		b.setText(Float.toString(avoTerm.getB().getValue()));
		if (avoTerm.getSigmaA() != null)
		   sigmaA.setText(Float.toString(avoTerm.getSigmaA().getValue()));
		if (avoTerm.getSigmaB() != null)
		   sigmaB.setText(Float.toString(avoTerm.getSigmaB().getValue()));
	}
}
