/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.models;

import java.io.Serializable;

/**
 * Title:        Application Info <br><br>
 * Description:  partial info included in XSD<br>
 * 
 * @version 1.0
 * @author  Charlie Jiang
 */

public class AppInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	String name;

	String title;

	String author;

	public AppInfo() {
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setParams(String name, String title, String author) {
		this.name = name;
		this.title = title;
		this.author = author;
	}

}