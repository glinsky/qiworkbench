/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.models.seismic;

import java.io.Serializable;
/**
 * Title: AvoTerms <br>
 * <br>
 * Description: partial info included in XSD<br>
 * 
 * @version 1.0
 * @author Charlie Jiang
 */
public class AvoTerms implements Serializable {
	private static final long serialVersionUID = 1L;
	
	float A      = 1.0f;
	
	//default = 0
	float sigmaA = -1.0f;
	
	float B = 1.0f;
	
    //default = 0
	float sigmaB = -1.0f;

	public AvoTerms() {
	}

	public float getA() {
		return A;
	}

	public void setA(float a) {
		A = a;
	}

	public float getB() {
		return B;
	}

	public void setB(float b) {
		B = b;
	}

	public float getSigmaA() {
		return sigmaA;
	}

	public void setSigmaA(float sigmaA) {
		this.sigmaA = sigmaA;
	}

	public float getSigmaB() {
		return sigmaB;
	}

	public void setSigmaB(float sigmaB) {
		this.sigmaB = sigmaB;
	}
}