/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
 */
package com.bhpb.models.seismic;

import java.io.Serializable;
/**
 * Title: Application Info <br>
 * <br>
 * Description: partial info included in XSD<br>
 * 
 * @version 1.0
 * @author Charlie Jiang
 */
public class Wavelet implements Serializable {
	private static final long serialVersionUID = 1L;
	public static float snDefault = 0.1f;
	
	String filename = "full_wavelet.su";

	// signal_to_noise
	float sn = -1f;;

	public Wavelet() {
	}

	public static float getSnDefault() {
		return snDefault;
	}

	public static void setSnDefault(float snDefault) {
		Wavelet.snDefault = snDefault;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public float getSn() {
		return sn;
	}

	public void setSn(float sn) {
		this.sn = sn;
	}

}