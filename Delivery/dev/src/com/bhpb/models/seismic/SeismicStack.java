/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.models.seismic;

import java.io.Serializable;

/**
 * Title:        SeismicStack <br><br>
 * Description:  partial info included in XSD<br>
 * 
 * @version 1.0
 * @author  Charlie Jiang
 */
public class SeismicStack implements Serializable {
	private static final long serialVersionUID = 1L;

	String name = "full";

	String filename = "full_stack.su";

	int min_offset = 0;
	
	int max_offset = 4000;
	
	float reflector_time = 1300f;
	
	float stack_velocity = 7400f;
	
	Wavelet wavelet = new Wavelet();
	
	//default = 50f
	float time_above = -1f;
	
//	default = 50f
	float time_below = -1f;
	
	
	public SeismicStack() {
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public int getMax_offset() {
		return max_offset;
	}


	public void setMax_offset(int max_offset) {
		this.max_offset = max_offset;
	}


	public int getMin_offset() {
		return min_offset;
	}


	public void setMin_offset(int min_offset) {
		this.min_offset = min_offset;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public float getReflector_time() {
		return reflector_time;
	}


	public void setReflector_time(float reflector_time) {
		this.reflector_time = reflector_time;
	}


	public float getStack_velocity() {
		return stack_velocity;
	}


	public void setStack_velocity(float stack_velocity) {
		this.stack_velocity = stack_velocity;
	}


	public float getTime_above() {
		return time_above;
	}


	public void setTime_above(float time_above) {
		this.time_above = time_above;
	}


	public float getTime_below() {
		return time_below;
	}


	public void setTime_below(float time_below) {
		this.time_below = time_below;
	}


	public Wavelet getWavelet() {
		return wavelet;
	}


	public void setWavelet(Wavelet wavelet) {
		this.wavelet = wavelet;
	}

}