/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.models.seismic;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
/**
 * Title:        SeismicModel <br><br>
 * Description:  partial info included in XSD<br>
 * 
 * @version 1.0
 * @author  Charlie Jiang
 */
public class SeismicModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	List<SeismicStack> stacks = new ArrayList<SeismicStack>();
	AvoTerms avoTerms = new AvoTerms();
	
	public AvoTerms getAvoTerms() {
		return avoTerms;
	}

	public void setAvoTerms(AvoTerms avoTerms) {
		this.avoTerms = avoTerms;
	}

	public List<SeismicStack> getStacks() {
		return stacks;
	}

	public void setStacks(List<SeismicStack> stacks) {
		this.stacks = stacks;
	}

	public SeismicModel() {
	}

}