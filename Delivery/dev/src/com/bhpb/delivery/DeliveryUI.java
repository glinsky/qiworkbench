/*
 * <Purpose>
 * <p>
 * Created on January 25, 2007, 1:13 PM
 *
 * @author Gil Hansen
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import org.w3c.dom.Node;
import com.bhpb.ui.UiConst;
import com.bhpb.ui.IAction;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpTabbedPane;
import com.bhpb.delivery.ui.JobPane;
import com.bhpb.delivery.ui.ParamPanel;
import com.bhpb.delivery.ui.ModelPanel;
import com.bhpb.delivery.ui.XmlsPanel;

/**
 * DeliveryLiteUI
 *
 * @author Gil Hansen
 * @author Charlie Jiang
 * @version 1.0
 */
public class DeliveryUI extends JInternalFrame implements IAction{
	private static final long serialVersionUID = 1L;

	private static Logger logger 
	  = Logger.getLogger(DeliveryUI.class.getName());

	private DeliveryPlugin agent;
	
	private JMenuBar menuBar; // menu bar

	private JToolBar toolbar; // the toolbar
	
	private BhpTabbedPane mainTab;
	
	BasePane[] panes = new BasePane[4];

	int prefWith = -1;
	int prefHeight = -1;
	Component gui;
	private  Action     saveAction;
	private  Action     saveAsAction;
	private final Dimension  BUTTON_DIM 
	   = new Dimension(DelivConst.BUTTON_ICON_SIZE, DelivConst.BUTTON_ICON_SIZE);

	/** Creates new form DeliveryLiteUI */
	public DeliveryUI(DeliveryPlugin agent) {
		this.agent = agent;
		agent.getModel(UiConst.LOAD_MODEL_DATA, true);
		if ("full-a".equals(agent.getName()))
		   this.setTitle("Delivery Full - Case A - " + agent.getModelXML());
		else
			this.setTitle("Delivery Full - Case B - " + agent.getModelXML());
		initialize(prefWith, prefHeight);
		buildGUI();
	}
	
	public DeliveryUI(String title, DeliveryPlugin agent) {
		this.agent = agent;
		agent.getModel(UiConst.LOAD_MODEL_DATA, true);
		if ("full-a".equals(agent.getName()))
		   this.setTitle(title + " Full - Case A - " + agent.getModelXML());
		else
			this.setTitle(title + " Full - Case B - " + agent.getModelXML());
		//this.setTitle(title + "TESTING");
		initialize(prefWith, prefHeight);
		buildGUI();
	}

	private void initialize(int width, int height) {
		//agent.getModel(UiConst.LOAD_MODEL_DATA, true);
		gui = (Component) this;
		if (width <= 0 || height <= 0) {
			width = DelivConst.PREF_WIDTH;
			height = DelivConst.PREF_HEIGHT;
		}
		
		this.setSize(width, height);
		this.setResizable(true);
		this.maximizable = true;
		this.iconable = true;
	    /*if ("full-a".equals(agent.getName()))
		   this.setTitle("Delivery Full - Case A - " + agent.getModelXML());
		else
			this.setTitle("Delivery Full - Case B - " + agent.getModelXML());*/
		
		toolbar = new JToolBar();
		menuBar = new JMenuBar();
		
		mainTab = new BhpTabbedPane(this, UiConst.GO_INDEX);
		
		BasePane bPane = new ParamPanel(agent);
		bPane.initPanel();
		mainTab.add(DelivConst.UI_ELE_PARAM_D,  bPane.getBasePanel());
		panes[0] = bPane;
		
		bPane = new ModelPanel(agent);
		bPane.initPanel();
		mainTab.add(DelivConst.UI_ELE_MODEL_D,  bPane.getBasePanel());
		panes[1] = bPane;
		
		
		bPane = new XmlsPanel(agent);
		bPane.initPanel();
		mainTab.add(DelivConst.UI_ELE_XML_D,  bPane.getBasePanel());
		panes[2] = bPane;
		
		
		bPane = new JobPane(agent);	
		bPane.initPanel();
		mainTab.add(DelivConst.UI_ELE_JOB_D,  bPane.getBasePanel());
		panes[3] = bPane;
		
		mainTab.getModel().addChangeListener(mainTab);
		
		this.setJMenuBar(menuBar);
		this.getContentPane().add(toolbar, BorderLayout.NORTH);
		this.getContentPane().add(mainTab, BorderLayout.CENTER);
	}

	/** Build GUI */
	private void buildGUI() {
		buildMenu();
		buildToolBar();
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	
	private void buildMenu() {
		buildFileMenu();
		buildEditMenu();
		buildExecuteMenu();
		buildHelpMenu();
	}
	
	private void buildToolBar() {
		ClassLoader cl = this.getClass().getClassLoader();
		addButton(cl, DelivConst.TB_SAVE, "Save");
		addButton(cl, DelivConst.TB_GEN, "Genearte Script");
		addButton(cl, DelivConst.TB_EXE, "Execute Script");
		addButton(cl, DelivConst.TB_HELP, "Help");
	}
	
	private void addButton(ClassLoader cl, String imgName, String tip) {
		ImageIcon icon = new ImageIcon(cl.getResource(imgName));
		JButton button = new JButton("");
		button.setIcon(icon);
		button.setToolTipText(tip);
		addIconButton(button);
	}
	
	private void buildFileMenu() {
        JMenu fileMenu = new JMenu("File");
        
//      open
        
        JMenuItem openItem = new JMenuItem("Import");
        openItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                //openXML();
            }
        });
        fileMenu.add(openItem);
        
         //save as
        JMenuItem exportItem = new JMenuItem("Export");
        exportItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                //openXML();
            }
        });
        fileMenu.add(exportItem);
        
        fileMenu.addSeparator();
        JMenuItem quit = new JMenuItem("Quit");
		quit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                agent.deactivateSelf();
                gui.setVisible(false);
                dispose();
            }
        });
		fileMenu.add(quit);
        
		//save
		saveAction = new AbstractAction("Save") {
			static final long serialVersionUID = 1l;
			public void actionPerformed(ActionEvent ae) {
			}
		};
		JMenuItem saveItem = new JMenuItem(saveAction);
		saveAction.setEnabled(false); // not enabled until file is opened
		fileMenu.add(saveItem);
		
		//save
		saveAsAction = new AbstractAction("Save As") {
			static final long serialVersionUID = 1l;
			public void actionPerformed(ActionEvent ae) {
			}
		};
		JMenuItem saveAsItem = new JMenuItem(saveAsAction);
		saveAsAction.setEnabled(false); // not enabled until file is opened
		fileMenu.add(saveAsItem);

		
        //Save and Exit
		JMenuItem exitItem = new JMenuItem("Save,Quit");
		exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
               //saveXML();
               //agent.saveMailConfigFile(true);
                agent.deactivateSelf();
                gui.setVisible(false);
                dispose();
            }
        });
		fileMenu.add(exitItem);
		
		menuBar.add(fileMenu);
	}
	
	private void buildEditMenu() {
        JMenu editMenu = new JMenu("Edit");
 
		JMenuItem pProp = new JMenuItem("Project Properties");
		pProp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		editMenu.add(pProp);
		menuBar.add(editMenu);
	}
	
	private void buildExecuteMenu() {
        JMenu exeMenu = new JMenu("Execute");
		JMenuItem writeMS = new JMenuItem("Write Model & Scripts");
		writeMS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		exeMenu.add(writeMS);
		exeMenu.addSeparator();
		
		JMenuItem writeM = new JMenuItem("Write Model");
		writeM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		exeMenu.add(writeM);
		
		JMenuItem writeD = new JMenuItem("Write Delivery Scripts");
		writeD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		exeMenu.add(writeD);
		
		JMenuItem writeA = new JMenuItem("Write Analyzer Scripts");
		writeM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		exeMenu.add(writeA);
		exeMenu.addSeparator();
		
		JMenuItem runS = new JMenuItem("Run Scripts");
		writeM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		exeMenu.add(runS);
		
		JMenuItem runC = new JMenuItem("Concel Run");
		writeM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		exeMenu.add(runC);
		
		JMenuItem check = new JMenuItem("Check Status");
		writeM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		exeMenu.add(check);
		menuBar.add(exeMenu);
	}
	
	private void buildHelpMenu() {
        JMenu helpMenu = new JMenu("Help");
 
		JMenuItem about = new JMenuItem("About");
		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			}
		});
		helpMenu.add(about);
		menuBar.add(helpMenu);
	}
	
	
	/**
     * Adds a JButton (which should have an associated icon) to the icon 
     * button panel at the top of the window.  Any text will be removed and
     * the icon will be resized to fit existing buttons.  As this is done 
     * only once, before JFrame.show(), it's acceptable to use smooth scaling.
     * @param button the JButton to add to the button panel
     */
    public void addIconButton(JButton button) {
	  Icon icon = button.getIcon();
	  if (icon != null && icon instanceof ImageIcon) {
	    ImageIcon imic = (ImageIcon)icon;
	    Image image = imic.getImage();
	    image = image.getScaledInstance(DelivConst.BUTTON_ICON_SIZE,
	    		DelivConst.BUTTON_ICON_SIZE, Image.SCALE_SMOOTH);
	    imic.setImage(image);    
	  }
	  button.setText("");
	  button.setMargin(DelivConst.NO_MARGIN_INSETS);
	  button.setSize(BUTTON_DIM);
	  toolbar.add(button);
    }

	public void restoreState(Node node) {

	}

	/**
	 * Rename the plugin
	 *
	 * @param name
	 *            from plugin message processor
	 */
	public void renamePlugin(String name) {
		this.setTitle(name + "  Project: " + "");
	}

	/**
	 * Close GUI and dispose, called by plugin when user has terminated GUI
	 *
	 */
	public void closeGUI() {
		setVisible(false);
		dispose();
	}

	/**
	 * Save the state information for the Wavelet Decomposition GUI.
	 *
	 * @return xml string.
	 */
	// TODO: rewrite WaveletDecomp saveState()
	public String genState() {
		StringBuffer content = new StringBuffer();

		content.append("<" + this.getClass().getName() + " ");
		return content.toString();
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new DeliveryUI(null).setVisible(true);
			}
		});
	}
	
	public void manageObject(String action, int index) {
		panes[index].manageObject(action, 0);
	}
	public void manageObject(String action) {}
	public void manageObject(List<String>paramList) {}
}
