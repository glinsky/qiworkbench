package com.bhpb.delivery.util;

import com.bhpb.model.ui.*;
import com.bhpb.delivery.DelivConst;
/**
 * Title:        DelivUtil <br><br>
 * Description:  this class is used for configurable GUI<br><br>
 * 
 * @version 1.0
 */
public class DelivUtil {
	
		public static Config defaultUIConfig() {
			Config config = new Config();
			NameComp lite = new NameComp("lite");
			NameValues vals = new NameValues(DelivConst.UI_ELE_PARAM);
			NameValue  v1   = new NameValue(DelivConst.PANE_PARAM_INFO,       "Info", "1");
			NameValue  v2   = new NameValue(DelivConst.PANE_PARAM_SEIS_DATA,  "Seismic Data", "1");
			NameValue  v3   = new NameValue(DelivConst.PANE_PARAM_ROCK_FLUID, "Rocks & Fluids", "1");
			NameValue  v4   = null;
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			lite.addElement(vals);
			
			vals = new NameValues(DelivConst.UI_ELE_MODEL);
			v1   = new NameValue(DelivConst.PANE_MODEL_TOP_BOT, "Top and Bottom", "1");
			v2   = new NameValue(DelivConst.PANE_MODEL_MIDDLE,  "Middle Layers", "1");
			v4   = new NameValue(DelivConst.PANE_MODEL_DEF,     "Model defs", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v4);
			lite.addElement(vals);
			
			vals = new NameValues(DelivConst.UI_ELE_XML);
			v1   = new NameValue(DelivConst.PANE_XML_ROCK_F,  "Rock & Fluid Defs", "1");
			v2   = new NameValue(DelivConst.PANE_XML_LAYERS,   "Layer Defs", "1");
			v3   = new NameValue(DelivConst.PANE_XML_MODEL,    "Full Model", "1");
			v4   = new NameValue(DelivConst.PANE_XML_UI,       "UI Config", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			vals.addNameValue(v4);
			lite.addElement(vals);
			
			vals = new NameValues(DelivConst.UI_ELE_JOB);
			v1   = new NameValue(DelivConst.PANE_JOB_WORKFLOW,  "Work Flow", "1");
			v2   = new NameValue(DelivConst.PANE_JOB_RUN,       "Run Settings", "1");
			v3   = new NameValue(DelivConst.PANE_JOB_STATUS,    "Job Status", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			lite.addElement(vals);
			
			NameComp full_a = new NameComp("full-a");
			vals = new NameValues("params");
			v1   = new NameValue(DelivConst.PANE_PARAM_INFO,       "Info", "1");
			v2   = new NameValue(DelivConst.PANE_PARAM_SEIS_DATA,  "Seismic Data", "1");
			v3   = new NameValue(DelivConst.PANE_PARAM_ROCK_FLUID, "Rocks and Fluids", "1");
			v4   = new NameValue(DelivConst.PANE_PARAM_PROP_IDX,   "Property Indices", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			vals.addNameValue(v4);
			full_a.addElement(vals);
			
			vals = new NameValues("model");
			v1   = new NameValue(DelivConst.PANE_MODEL_TOP_BOT, "Top and Bottom", "1");
			v2   = new NameValue(DelivConst.PANE_MODEL_MIDDLE,  "Middle Layers", "1");
			v4   = new NameValue(DelivConst.PANE_MODEL_DEF,     "Model defs", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v4);
			full_a.addElement(vals);
			
			vals = new NameValues("xmldef");
			v1   = new NameValue(DelivConst.PANE_XML_ROCK_F,  "Rock & Fluid Defs", "1");
			v2   = new NameValue(DelivConst.PANE_XML_LAYERS,   "Layer Defs", "1");
			v3   = new NameValue(DelivConst.PANE_XML_MODEL,    "Full Model", "1");
			v4   = new NameValue(DelivConst.PANE_XML_UI,       "UI Config", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			vals.addNameValue(v4);
			full_a.addElement(vals);
			
			vals = new NameValues("job");
			v1   = new NameValue(DelivConst.PANE_JOB_RUN,       "Run Settings", "1");
			v2   = new NameValue(DelivConst.PANE_JOB_WORKFLOW,  "Work Flow", "1");
			v3   = new NameValue(DelivConst.PANE_JOB_STATUS,    "Job Status", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			full_a.addElement(vals);
			
			NameComp full_b = new NameComp("full-b");
			vals = new NameValues("params");
			v1   = new NameValue(DelivConst.PANE_PARAM_INFO,       "Info", "1");
			v2   = new NameValue(DelivConst.PANE_PARAM_SEIS_DATA,  "Seismic Data", "1");
			v3   = new NameValue(DelivConst.PANE_PARAM_ROCK_FLUID, "Rocks and Fluids", "1");
			v4   = new NameValue(DelivConst.PANE_PARAM_PROP_IDX,   "Property Indices", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			vals.addNameValue(v4);
			full_b.addElement(vals);
			
			vals = new NameValues("model");
			v1   = new NameValue(DelivConst.PANE_MODEL_TOP_BOT, "Top and Bottom", "1");
			v2   = new NameValue(DelivConst.PANE_MODEL_MIDDLE,  "Middle Layers", "1");
			v3   = new NameValue(DelivConst.PANE_MODEL_S_INFO,  "Stacked Info", "1");
			v4   = new NameValue(DelivConst.PANE_MODEL_DEF,     "Model defs", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			vals.addNameValue(v4);
			full_b.addElement(vals);
			
			vals = new NameValues("xmldef");
			v1   = new NameValue(DelivConst.PANE_XML_ROCK_F,  "Rock & Fluid Defs", "1");
			v2   = new NameValue(DelivConst.PANE_XML_LAYERS,   "Layer Defs", "1");
			v3   = new NameValue(DelivConst.PANE_XML_MODEL,    "Full Model", "1");
			v4   = new NameValue(DelivConst.PANE_XML_UI,       "UI Config", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			vals.addNameValue(v4);
			full_b.addElement(vals);
			
			vals = new NameValues("job");
			v1   = new NameValue(DelivConst.PANE_JOB_RUN,       "Run Settings", "1");
			v2   = new NameValue(DelivConst.PANE_JOB_WORKFLOW,  "Work Flow", "1");
			v3   = new NameValue(DelivConst.PANE_JOB_STATUS,    "Job Status", "1");
			vals.addNameValue(v1);
			vals.addNameValue(v2);
			vals.addNameValue(v3);
			full_b.addElement(vals);
			
			config.addComponent(lite);
			config.addComponent(full_a);
			config.addComponent(full_b);
			return config;
		}
		
}