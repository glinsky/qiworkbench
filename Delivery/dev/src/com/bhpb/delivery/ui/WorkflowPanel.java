/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.awt.Font;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.bhpb.core.AgentDelegate;
import com.bhpb.ui.BasePane;
import java.awt.BorderLayout;

public class WorkflowPanel  extends BasePane {
	private static final long serialVersionUID = 1L;
	public WorkflowPanel(AgentDelegate delegate) {
		super(delegate);
	}	
	
    public void initPanel() {
    	JPanel pane = new JPanel();
    	pane.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
    	JPanel pane1 = new JPanel();
    	pane1.setLayout(new BoxLayout(pane1, BoxLayout.Y_AXIS));
    	JLabel diagram = new JLabel("Delivery Workflow Diagram");
    	diagram.setFont(new Font("Serif", Font.BOLD, 18));
    	pane1.add(diagram);
    	pane1.add(Box.createRigidArea(BasePane.VGAP10));
    	pane1.add(createIconLabel("workflow"));
    	
    	pane.add(Box.createRigidArea(BasePane.HGAP20));
    	pane.add(pane1);
		getBasePanel().add(pane, BorderLayout.CENTER);
	}
    
    private JLabel createIconLabel(String tip) {
		ImageIcon icon = new ImageIcon(getClass().getResource("/icons/workflow.gif"));
		JLabel iLabel = new JLabel("", icon, SwingConstants.CENTER);
		iLabel.setToolTipText(tip);
		return iLabel;
	}
}
