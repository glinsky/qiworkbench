/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import java.awt.GridLayout;
import com.bhpb.ui.BasePane;

public class GrainPropsPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private JButton updateB;
	private JTextField v1, v2;
	
	public GrainPropsPanel(String title, int width, String p1, String p2, int pad) {
		this.setLayout(new GridLayout(2, 2+pad));
		if (title != null) {
			TitledBorder tb = new TitledBorder(title);
	        //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	        tb.setTitleColor(Color.PINK);
			this.setBorder(tb);
		}
		
		this.add(new JLabel(p1));
		this.add(new JLabel(p2));
		if (pad > 0) {
		  for (int i = 0; i < pad; i++) {
			this.add(Box.createRigidArea(BasePane.HGAP2));
		  }
		}
		
		v1 = new JTextField("", width);
		v2 = new JTextField("", width);
		
		this.add(v1);
		this.add(v2);
		this.add(Box.createRigidArea(BasePane.HGAP2));
		ImageIcon icon = new ImageIcon(getClass().getResource("/icons/update.jpg"));
		updateB = new JButton("");
		updateB.setIcon(icon);
		updateB.setToolTipText("update");
		this.add(updateB);
		this.add(Box.createRigidArea(BasePane.HGAP2));
			  
	}

}
