/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import com.bhpb.ui.BasePane;
import com.bhpb.core.AgentDelegate;

public class ModelStackedInfo extends BasePane {
	private static final long serialVersionUID = 1L;
	JPanel  mPane;
	JButton updateB, addB;
	
	public ModelStackedInfo(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		mPane = new JPanel();
		mPane.setLayout(new BorderLayout());
		
		MappingAndRange   p1  = new MappingAndRange();
		StdDeviationPanel p2  = new StdDeviationPanel("Std Deviation");
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		pane.add(createButtons());
		pane.add(p1);
		pane.add(Box.createRigidArea(VGAP20));
		pane.add(p2);
		
		mPane.add(pane, BorderLayout.NORTH);
		getBasePanel().add(mPane);
	}
	
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		pane.add(updateB);
        //updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		pane.add(Box.createRigidArea(HGAP15));
		
		buttonPanel.add(pane, BorderLayout.EAST);

		return buttonPanel;
	}
	
}
