/*
# To contact BHP Billiton about this software you can e-mail
# info@qiworkbench.org or visit http://qiworkbench.org to learn more.
###########################################################################
*/
package com.bhpb.delivery.ui;
import com.bhpb.model.ui.NameValue;
import com.bhpb.model.ui.NameValues;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.JobStatus;
import com.bhpb.ui.BhpTabbedPane;
import com.bhpb.ui.TabedBasePane;
import com.bhpb.ui.UiConst;
import com.bhpb.core.AgentDelegate;
import com.bhpb.delivery.DelivConst;
import java.awt.BorderLayout;

public class JobPane extends TabedBasePane {
	private static final long serialVersionUID = 1L;
	
	public JobPane(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		thisTab = new BhpTabbedPane(this, UiConst.GO_NORMAL);
		
		BasePane out = new OutputPanel(delegate);
		out.initPanel();
		
		BasePane workflow = new WorkflowPanel(delegate);
		workflow.initPanel();
		
		BasePane status = new JobStatus(delegate);
		status.initPanel();
		
		int cnt = 0;
        NameValues vals = getElement(DelivConst.UI_ELE_JOB);
		
		NameValue val = vals.getNameValue(DelivConst.PANE_JOB_RUN);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  out.getBasePanel());
			panes[cnt] = out;
			cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_JOB_WORKFLOW);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  workflow.getBasePanel());
			panes[cnt] = workflow;
			cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_JOB_STATUS);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  status.getBasePanel());
			panes[cnt] = status;
			cnt++;
		}
		
		thisTab.getModel().addChangeListener(thisTab);
		getBasePanel().add(thisTab, BorderLayout.CENTER);
	}
}
