/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import com.bhpb.ui.UiConst;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpPanel;
import com.bhpb.ui.NameOptionsPanel;
import com.bhpb.core.AgentDelegate;
import com.bhpb.xsdparams.delivery.Inversion;

public class RockFluidPanel extends BasePane {
	private static final long serialVersionUID = 1L;
	
	public static final String ROCK       = "ROCK";
	public static final String FLUID      = "FLUID";
	public static final String SAND_TYPE  = "Sand";
	public static final String SHALE_TYPE = "Shale";
	public static final String SAND_PANE  = "SAND_CURVE";
	public static final String SHALE_PANE = "SHALE_CURVE";
	
	JButton    updateB;
	JPanel     rPane, fPane;
	NameOptionsPanel rockAdd, fluidAdd;
	Map<String, BhpPanel>  sands   = new HashMap<String, BhpPanel>();
	Map<String, BhpPanel>  shales  = new HashMap<String, BhpPanel>();
	Map<String, BhpPanel>  fluids  = new HashMap<String, BhpPanel>();
	
	public RockFluidPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel thisPane = new JPanel();
		thisPane.setLayout(new BoxLayout(thisPane, BoxLayout.Y_AXIS));
		
		rPane = new JPanel();
		
		JPanel rockPane   = createRockFluidPane();
		//add together
		thisPane.add(createButtons());
		thisPane.add(rockPane);
		getBasePanel().add(thisPane, BorderLayout.NORTH);
	}
	
	public void manageObject(List<String> paramList) {
		BhpPanel  bhpPanel = null;
		if (SAND_PANE.equals(paramList.get(0))) {
			bhpPanel = sands.get(paramList.get(1));
			bhpPanel.manageObject(paramList);
		} else if (SHALE_PANE.equals(paramList.get(0))) {
			bhpPanel = shales.get(paramList.get(1));
			bhpPanel.manageObject(paramList);
		} else if (FLUID.equals(paramList.get(0))) {
			bhpPanel = fluids.get(paramList.get(1));
			bhpPanel.manageObject(paramList);
		} else if ((ROCK+UiConst.ADD).equals(paramList.get(0) ) ) {
			addNew(0);
		} else if ((ROCK+UiConst.DEL).equals(paramList.get(0) )) {
			removeSelected(0);		
		} else if ((FLUID+UiConst.ADD).equals(paramList.get(0) ) ) {
			addNew(1);	
		} else if ((FLUID+UiConst.DEL).equals(paramList.get(0) )) {
			removeSelected(1);	
		}
		delegate.resize();
	}
	
	private JPanel createRockFluidPane() {
		JPanel rockFluidPane = new JPanel();
		rockFluidPane.setLayout(new BoxLayout(rockFluidPane, BoxLayout.X_AXIS));
		
		JPanel rockPane   = createRockPane();
		JPanel fluidPane  = createFluidPane();;
		
		//add together
		rockFluidPane.add(rockPane);
		rockFluidPane.add(Box.createRigidArea(HGAP15));
		rockFluidPane.add(fluidPane);
		return rockFluidPane;
	}
	
	private JPanel createRockPane() {
		Inversion model = (Inversion)delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
		List<Inversion.RockFluidProperties.RockProperties.ReservoirEndmember> 
		    sandList = model.getRockFluidProperties().getRockProperties().getReservoirEndmember();
		
		List<Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember> 
	        shaleList = model.getRockFluidProperties().getRockProperties().getNonreservoirEndmember();
		
		Inversion.RockFluidProperties.RockProperties.ReservoirEndmember sand;
		Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember shale;

		JPanel panex = new JPanel();
		panex.setLayout(new BorderLayout());
		
		JPanel rockPane = new JPanel();
		rockPane.setLayout(new BoxLayout(rockPane, BoxLayout.Y_AXIS));
		
		rPane.setLayout(new BoxLayout(rPane, BoxLayout.Y_AXIS));
		TitledBorder tb = new TitledBorder("Rock Properties:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    rockPane.setBorder(tb);
		
		int size = sandList.size();
		for (int i = 0; i < size; i++) {
			sand = sandList.get(i);
		    String key1 = sand.getName();
		    BhpPanel tempSand   = new SandCurvePanel(this, key1, 5, UIConst.TIP_SAND);
			sands.put(key1, tempSand);
			tempSand.populate(sand); 
			rPane.add(tempSand);
		}
		
		size = shaleList.size();
		for (int i = 0; i < size; i++) {
			shale = shaleList.get(i);
		    String key1 = shale.getName();
		    BhpPanel tempSand   = new ShaleCurvePanel(this, key1, 5, UIConst.TIP_SHALE);
			shales.put(key1, tempSand);
			tempSand.populate(shale);
			rPane.add(tempSand);
		}
		
		List<String>params = new ArrayList<String>();
		params.add(ROCK);
		params.add(SAND_TYPE);
		params.add(SHALE_TYPE);
		rockAdd = new NameOptionsPanel(this, params, 8);
		rockPane.add(rockAdd);
		rockPane.add(rPane);
        panex.add(rockPane, BorderLayout.NORTH);
		return panex;
	}
	
	private JPanel createFluidPane() {
		Inversion model = (Inversion)delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
		List<Inversion.RockFluidProperties.FluidProperties.Fluid>
		    fluidList = model.getRockFluidProperties().getFluidProperties().getFluid();
		Inversion.RockFluidProperties.FluidProperties.Fluid fluid;
		
		JPanel panex = new JPanel();
		panex.setLayout(new BorderLayout());
	
		JPanel fluidPane = new JPanel();
		fluidPane.setLayout(new BoxLayout(fluidPane, BoxLayout.Y_AXIS));
		
		fPane = new JPanel();
		fPane.setLayout(new BoxLayout(fPane, BoxLayout.Y_AXIS));
        TitledBorder tb = new TitledBorder("Fluid Properties:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    fluidPane.setBorder(tb);
	    
	    int size = fluidList.size();
		for (int i = 0; i < size; i++) {
			fluid = fluidList.get(i);
		    String key1 = fluid.getName();
		    BhpPanel tempSand   = new FluidPropsPanel(this, key1, null, 5);
			fluids.put(key1, tempSand);
			tempSand.populate(fluid); 
			fPane.add(tempSand);
		}
	    /*
	    FluidPropsPanel fluid1 = new FluidPropsPanel(this, "Brine", null, 5); 
	    fluids.put(fluid1.getPaneName(), fluid1);
	    FluidPropsPanel fluid2 = new FluidPropsPanel(this, "Oil", null, 5); 
	    fluids.put(fluid2.getPaneName(), fluid2);
	    FluidPropsPanel fluid3 = new FluidPropsPanel(this, "Gas", null, 5); 
	    fluids.put(fluid3.getPaneName(), fluid3);*/
	    
	    List<String>params = new ArrayList<String>();
		params.add(FLUID);
		fluidAdd = new NameOptionsPanel(this, params, 6);
		
	    fluidPane.add(fluidAdd);
	    fluidPane.add(fPane);
        panex.add(fluidPane, BorderLayout.NORTH);
        return panex;
	}
	
	public void populate() {
		Inversion model = (Inversion)delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
		List<Inversion.RockFluidProperties.RockProperties.ReservoirEndmember> 
		    sandList = model.getRockFluidProperties().getRockProperties().getReservoirEndmember();
		
		List<Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember> 
	        shaleList = model.getRockFluidProperties().getRockProperties().getNonreservoirEndmember();
		
		List<Inversion.RockFluidProperties.FluidProperties.Fluid>
		    fluidList = model.getRockFluidProperties().getFluidProperties().getFluid();
		
		Inversion.RockFluidProperties.RockProperties.ReservoirEndmember sand;
		Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember shale;
		Inversion.RockFluidProperties.FluidProperties.Fluid fluid;
		
		int size = sandList.size();
		for (int i = 0; i < size; i++) {
			sand = sandList.get(i);
		    String key1 = sand.getName();
		    BhpPanel bhpPanel = sands.get(key1);
		    bhpPanel.populate(sand); 
		}
	
	    size = shaleList.size();
	    for (int i = 0; i < size; i++)  {
		    shale = shaleList.get(i);
	        String key1 = shale.getName();
	        BhpPanel bhpPanel = shales.get(key1);
	        bhpPanel.populate(shale); 
	    }	
	    
	    size = fluidList.size();
	    for (int i = 0; i < size; i++)  {
		    fluid = fluidList.get(i);
	        String key1 = fluid.getName();
	        BhpPanel bhpPanel = fluids.get(key1);
	        bhpPanel.populate(fluid); 
	    }		
	}
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		pane.add(updateB);
		//updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		pane.add(Box.createRigidArea(HGAP15));
		buttonPanel.add(pane, BorderLayout.EAST);
		return buttonPanel;
	}
	
	private boolean validateName(int index, String name1) {
	    if (0 == index) {
	    	if (sands.keySet().contains(name1)) return false;
	    	if (shales.keySet().contains(name1)) return false;
	    } else {
	    	if (fluids.keySet().contains(name1));
	    }
		return true;
	}
	
	private void addNew(int index) {
		BhpPanel  basePane = null;
		String tempName = null;
		if (0 == index) {
			tempName = rockAdd.getText();
			if (tempName == null || tempName.length() == 0) return;
			if (! validateName(0, tempName)) {
				JOptionPane.showMessageDialog(this, UiConst.ERR_DUPLICATE_NAME);
				return;
			}
			if (0 == rockAdd.getType()) {
			   basePane   = new SandCurvePanel(this,   tempName, 5, UIConst.TIP_SAND);
			   sands.put(tempName, basePane);
			} else {
			   basePane  = new ShaleCurvePanel(this,tempName, 5, UIConst.TIP_SHALE);
			   shales.put(tempName, basePane);
			}
			 rPane.add(basePane);
		} else {
			tempName = fluidAdd.getText();
			if (tempName == null || tempName.length() == 0) return;
			if (! validateName(1, tempName)) {
				JOptionPane.showMessageDialog(this, UiConst.ERR_DUPLICATE_NAME);
				return;
			}
			basePane   = new FluidPropsPanel(this, tempName, null, 5);
			fluids.put(tempName, basePane);
			fPane.add(basePane);
		}
	}
	
	private void removeSelected(int index) {
		Iterator it = null;
		String key  = null;
		BhpPanel panex = null;
	    if (0 == index) { //rock
	    	it = sands.entrySet().iterator();
	    	while (it.hasNext()) {
	    		Map.Entry entry = (Map.Entry)it.next();
	    		key   = (String)entry.getKey();
	    		panex = (BhpPanel)entry.getValue();
	    	    if (panex.selected()) {
	    			sands.remove(key);
	    			rPane.remove(panex);
	    			panex = null;
	    		}
	    		
	    	}
	    	
	    	it = shales.entrySet().iterator();
	    	while (it.hasNext()) {
	    		Map.Entry entry = (Map.Entry)it.next();
	    		key   = (String)entry.getKey();
	    		panex = (BhpPanel)entry.getValue();
	    	    if (panex.selected()) {
	    			shales.remove(key);
	    			rPane.remove(panex);
	    			panex = null;
	    		}
	    	}
	
	    } else { //fluid
	    	it = fluids.entrySet().iterator();
	    	while (it.hasNext()) {
	    		Map.Entry entry = (Map.Entry)it.next();
	    		key   = (String)entry.getKey();
	    		panex = (BhpPanel)entry.getValue();
	    	    if (panex.selected()) {
	    			fluids.remove(key);
	    			fPane.remove(panex);
	    			panex = null;
	    		}
	    		
	    	}
	    }
	}
	
	
}
