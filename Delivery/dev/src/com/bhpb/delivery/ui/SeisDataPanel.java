/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.StackPanel;
import com.bhpb.ui.AvoTermPanel;
import com.bhpb.ui.UiConst;
import com.bhpb.xsdparams.delivery.Inversion;
import com.bhpb.core.AgentDelegate;

public class SeisDataPanel extends BasePane {
	private static final long serialVersionUID = 1L;
	StackPanel nearStack, farStack;
	AvoTermPanel avoTermPanel;
	JButton    addB, removeB, updateB;
	

	public SeisDataPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel settingPane = new JPanel();
		settingPane.setLayout(new BoxLayout(settingPane, BoxLayout.Y_AXIS));
		
		JPanel stack2  = createStacksPane();;
		avoTermPanel   = new AvoTermPanel("Avo Terms");
		//add together
		settingPane.add(createButtons());
	    settingPane.add(stack2);
	    settingPane.add(Box.createRigidArea(VGAP10));
	    settingPane.add(avoTermPanel);
		getBasePanel().add(settingPane, BorderLayout.NORTH);
		populate();
	}
	
	
	private JPanel createStacksPane() {
		JPanel stacksPanel = new JPanel();
		stacksPanel.setLayout(new BoxLayout(stacksPanel, BoxLayout.Y_AXIS));
		TitledBorder tb = new TitledBorder("Stacks:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    stacksPanel.setBorder(tb);
	    stacksPanel.add(createStacksPart1());
	    stacksPanel.add(createStacksPart2());
		return stacksPanel;
	}
	
	private JPanel createStacksPart1() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		addB    = new JButton("Add");
		removeB = new JButton("DEL");
		pane.add(addB);
		pane.add(removeB);
		addB.setEnabled(false);
		removeB.setEnabled(false);
		buttonPanel.add(pane, BorderLayout.EAST);
		return buttonPanel;
	}
	
	private JPanel createStacksPart2() {
		JPanel stacksPanel = new JPanel();
		stacksPanel.setLayout(new BoxLayout(stacksPanel, BoxLayout.X_AXIS));
		stacksPanel.add(Box.createRigidArea(HGAP15));
	    nearStack = new StackPanel("Near");
	    stacksPanel.add(nearStack);
	    stacksPanel.add(Box.createRigidArea(HGAP15));
	    farStack = new StackPanel("Far");
	    stacksPanel.add(farStack);
		return stacksPanel;
		
	}
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		//updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		pane.add(updateB);
		pane.add(Box.createRigidArea(HGAP15));
		buttonPanel.add(pane, BorderLayout.EAST);
		return buttonPanel;
	}

	public void populate() {
		Inversion model = (Inversion)delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
		Inversion.SeismicData.AVOTerms avoTerm = model.getSeismicData().getAVOTerms();
		List<Inversion.SeismicData.Stack> stacks = model.getSeismicData().getStack();
		
		avoTermPanel.populate(avoTerm);
		int size = stacks.size();
		if (size > 0) {
		  Inversion.SeismicData.Stack stack = stacks.get(0);
		  nearStack.populate( stack);
		  if (size > 1) {
			  stack = stacks.get(1);
			  farStack.populate( stack);
		  }
		}
	}
}
