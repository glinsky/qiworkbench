/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.awt.BorderLayout;
import com.bhpb.ui.*;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.UiConst;
import com.bhpb.core.AgentDelegate;
import com.bhpb.model.ui.NameValue;
import com.bhpb.model.ui.NameValues;
import com.bhpb.delivery.DelivConst;

public class ParamPanel extends TabedBasePane {
	private static final long serialVersionUID = 1L;
	
	public ParamPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		thisTab = new BhpTabbedPane(this, UiConst.GO_NORMAL);
		
		BasePane info = new InfoPanel(delegate);
		info.initPanel();
		
		BasePane seisData = new SeisDataPanel(delegate);
		seisData.initPanel();
		
		BasePane rockFluid = new RockFluidPanel(delegate);
		rockFluid.initPanel();
		
		BasePane indices = new IndicesPanel(delegate);
		indices.initPanel();
		
		int cnt = 0;
		NameValues vals = getElement(DelivConst.UI_ELE_PARAM);
		
		NameValue val = vals.getNameValue(DelivConst.PANE_PARAM_INFO);
		if (val != null) {
			   thisTab.add(val.getDisplayName(),  info.getBasePanel());
			   panes[cnt] = info;
			   cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_PARAM_SEIS_DATA);
		if (val != null) {
			   thisTab.add(val.getDisplayName(),  seisData.getBasePanel());
			   panes[cnt] = seisData;
			   cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_PARAM_ROCK_FLUID);
		if (val != null) {
			   thisTab.add(val.getDisplayName(),  rockFluid.getBasePanel());
			   panes[cnt] = rockFluid;
			   cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_PARAM_PROP_IDX);
		if (val != null) {
			   thisTab.add(val.getDisplayName(),  indices.getBasePanel());
			   panes[cnt] = indices;
		}
		
		thisTab.getModel().addChangeListener(thisTab);
		getBasePanel().add(thisTab, BorderLayout.CENTER);
	}
}
