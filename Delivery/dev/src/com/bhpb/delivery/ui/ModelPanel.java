/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.awt.BorderLayout;
import com.bhpb.model.ui.NameValue;
import com.bhpb.model.ui.NameValues;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpTabbedPane;
import com.bhpb.ui.TabedBasePane;
import com.bhpb.ui.UiConst;
import com.bhpb.core.AgentDelegate;
import com.bhpb.delivery.DelivConst;
import com.bhpb.ui.*;

public class ModelPanel extends TabedBasePane {
	private static final long serialVersionUID = 1L;
	public static final String LAYER     = "LAYER";
	public static final String LAYER_ADD = "addLayer";
	public static final String LAYER_DEL = "delLayer";

	public ModelPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		thisTab = new BhpTabbedPane(this, UiConst.GO_NORMAL);
		
		ReadmePanel readme = new ReadmePanel(delegate);
		readme.initPanel();
		
		ModelTopBottom topBottom = new ModelTopBottom(delegate);
		topBottom.initPanel();
		
		BasePane middle = new ModelMiddle(delegate);
		middle.initPanel();
		
		BasePane stackedInfo = new ModelStackedInfo(delegate);
		stackedInfo.initPanel();
		
		BasePane xml = new TextView(delegate);
		xml.initPanel();
		xml.setModelName(UiConst.LOAD_MODEL_DATA);
		
		int cnt = 0;
        NameValues vals = getElement(DelivConst.UI_ELE_MODEL);
        
        thisTab.add("Instruction",  readme.getBasePanel());
		panes[cnt] = readme;
		cnt++;
		
		NameValue val = vals.getNameValue(DelivConst.PANE_MODEL_TOP_BOT);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  topBottom.getBasePanel());
			panes[cnt] = topBottom;
			cnt++;
		}
		
		
		val = vals.getNameValue(DelivConst.PANE_MODEL_MIDDLE);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  middle.getBasePanel());
			panes[cnt] = middle;
			cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_MODEL_S_INFO);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  stackedInfo.getBasePanel());
			panes[cnt] = stackedInfo;
			cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_MODEL_DEF);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  xml.getBasePanel());
			panes[cnt] = xml;
		}
		thisTab.getModel().addChangeListener(thisTab);
		getBasePanel().add(thisTab, BorderLayout.CENTER);
	}

}
