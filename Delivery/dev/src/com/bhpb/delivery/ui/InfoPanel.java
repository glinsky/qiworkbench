/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import com.bhpb.ui.BasePane;
import com.bhpb.ui.UiConst;
import com.bhpb.core.AgentDelegate;
import com.bhpb.xsdparams.delivery.*;

public class InfoPanel extends BasePane {
	private static final long serialVersionUID = 1L;
	
	JTextField projNameF, projDirF, datasetDirF, seisDirF, outDirF, modelFileF;
	JTextField projTitleF, authorF;
	JComboBox  modeComboBox;
	JButton    updateB;
	JLabel     comment;
	

	public InfoPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.Y_AXIS));
		
		JPanel bPane   = createButtons();
		JPanel pane1   = createModePane();
		JPanel projPane  = createProjPane();
		
       //		add together
		infoPane.add(bPane);
		infoPane.add(pane1);
		infoPane.add(Box.createRigidArea(VGAP15));
		infoPane.add(projPane);
		getBasePanel().add(infoPane, BorderLayout.NORTH);
		
		populate();
	}
	
	private JPanel createProjPane() {
		JPanel projPanel = new JPanel();
        projPanel.setLayout(new BoxLayout(projPanel, BoxLayout.X_AXIS));
        TitledBorder tb = new TitledBorder("Project Information:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    projPanel.setBorder(tb);
        
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel label1 = new JPanel(new GridLayout(4, 1));
        label1.add(new JLabel("Project Title:"));
        label1.add(new JLabel("Project Name:"));
        label1.add(new JLabel("Project Relative Dir:"));
        label1.add(new JLabel("Datasets Relative Dir:"));
        projPanel.add(label1);
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel field1 = new JPanel(new GridLayout(4, 1));
        projTitleF   = new JTextField("", 12);
        projNameF   = new JTextField("", 12);
        projDirF    = new JTextField("", 12);
        datasetDirF = new JTextField("", 12);
        field1.add(projTitleF);
        field1.add(projNameF);
        field1.add(projDirF);
        field1.add(datasetDirF);
        projPanel.add(field1);
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel label2 = new JPanel(new GridLayout(4, 1));
        label2.add(new JLabel("Author:"));
        label2.add(new JLabel("Seismic Relative Dir:"));
        label2.add(new JLabel("Output Relative Dir:"));
        label2.add(new JLabel("Model File name:"));
        projPanel.add(label2);
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel field2 = new JPanel(new GridLayout(4, 1));
        authorF   = new JTextField("", 12);
        seisDirF   = new JTextField("", 12);
        outDirF    = new JTextField("", 12);
        modelFileF = new JTextField("", 12);
        field2.add(authorF);
        field2.add(seisDirF);
        field2.add(outDirF);
        field2.add(modelFileF);
        projPanel.add(field2);
        
		return projPanel;
	}
	
	private JPanel createModePane() {
		JPanel modePanel = new JPanel();
        modePanel.setLayout(new BorderLayout());
        TitledBorder tb = new TitledBorder("Inversion Option:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    modePanel.setBorder(tb);
       
        JPanel label1 = new JPanel();
        label1.add(Box.createRigidArea(HGAP15));
        label1.add(new JLabel("Mode:"));
        modeComboBox = new JComboBox() ;
        modeComboBox.addItem("Lite");
        modeComboBox.addItem("Full-A");
        modeComboBox.addItem("Full-B");
        modeComboBox.setEnabled(false);
        label1.add(modeComboBox);
        label1.add(Box.createRigidArea(HGAP30));
        comment = new JLabel("Single Trace Inversion");
        label1.add(comment);
        modePanel.add(label1, BorderLayout.WEST);
        modeComboBox.setSelectedIndex(getOption());
		return modePanel;
	}
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		//updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		pane.add(updateB);
		pane.add(Box.createRigidArea(HGAP15));
		buttonPanel.add(pane, BorderLayout.EAST);
		return buttonPanel;
	}	
	
	public void manageObject(String action) {
		delegate.getModel(UiConst.LOAD_MODEL_UI, false);
		delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
	}
	
	private int getOption() {
		int index = 0;
		String compName = delegate.getName();
		if ("full-a".equals(compName)) {
			index = 1;
			comment.setText("Full Inversion without layer mappings");
		} else if ("full-b".equals(compName)) {
			index = 2;
			comment.setText("Full Inversion with layer mappings");
		}
		return index;
	}
	
	public void populate() {
		Inversion model = (Inversion)delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
		Inversion.InversionInfo info = model.getInversionInfo();
		String temp = info.getAuthor();
		authorF.setText(temp);
		temp = info.getName();
		projNameF.setText(temp);
		temp = info.getProject();
		projTitleF.setText(temp);
	}
}
