/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import com.bhpb.ui.BasePane;
import com.bhpb.core.AgentDelegate;

public class OutputPanel extends BasePane {
	private static final long serialVersionUID = 1L;
	JTextField numRunF, tminF, tmaxF, epF, epBy, cdpF, cdpBy, epF2, cdpF2;
	JComboBox  orderCombo, depthModelCombo;
	JComboBox  vp_m,   vs_m,   rho_m,  rho_h;
	JComboBox  vp_s,   vs_s,   phi_s,  S_h;
	JComboBox  vp_eff, rho_eff, Z_eff, fluid_type;
	JComboBox  R_near, NG, d, chisquare;
	JComboBox  R_far,  LFIV,  float_fraction;
	JTextField headerField, masterLayer;
	JButton    updateB;
	JTextField priorOutF,  postOutF;
	

	public OutputPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.Y_AXIS));
		
		JPanel bPane   = createButtons();
		JPanel pane1   = createPane1();
		JPanel pane2   = createPropertyPane();
		
       //		add together
		infoPane.add(bPane);
		infoPane.add(createRunPane());
		infoPane.add(Box.createRigidArea(VGAP15));
		infoPane.add(createOutputPane());
		infoPane.add(Box.createRigidArea(VGAP25));
		infoPane.add(pane1);
		infoPane.add(Box.createRigidArea(VGAP15));
		infoPane.add(pane2);
		
		getBasePanel().add(infoPane, BorderLayout.NORTH);
	}
	
	private JPanel createPane1() {
		JPanel pane1 = new JPanel();
        pane1.setLayout(new BoxLayout(pane1, BoxLayout.X_AXIS));
        //TitledBorder tb = new TitledBorder("Project Information:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    //tb.setTitleColor(Color.BLUE);
	    //projPanel.setBorder(tb);
         
        pane1.add(Box.createRigidArea(HGAP10));
        pane1.add(new JLabel("DensityOrdering:"));
        orderCombo = new JComboBox() ;
        orderCombo.addItem("full");
        orderCombo.addItem("partial");
        orderCombo.addItem("none");
        orderCombo.setSelectedIndex(0);
        pane1.add(orderCombo);
        pane1.add(Box.createRigidArea(HGAP10));
        
        pane1.add(new JLabel("HeaderField:"));
        headerField = new JTextField("mark");
        pane1.add(headerField);
        pane1.add(Box.createRigidArea(HGAP10));
        
        pane1.add(new JLabel("MasterLayer#:"));
        masterLayer  = new JTextField("1");
        pane1.add(masterLayer);
        pane1.add(Box.createRigidArea(HGAP10));
        
        pane1.add(new JLabel("DepthModels"));
        depthModelCombo = new JComboBox() ;
        depthModelCombo.addItem("false");
        depthModelCombo.addItem("true");
        depthModelCombo.setSelectedIndex(0);
        pane1.add(depthModelCombo);
        pane1.add(Box.createRigidArea(HGAP10));
        
		return pane1;
	}
	
	private JPanel createPropertyPane() {
		JPanel propPanel = new JPanel();
		propPanel.setLayout(new GridLayout(5, 8));
        TitledBorder tb = new TitledBorder("Output Properties:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    propPanel.setBorder(tb);
       
	    JLabel label = new JLabel("vp_m");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        vp_m = createComboBox(1);
        propPanel.add(vp_m);
        
        propPanel.add(new JLabel("vs_m"));
        vs_m = createComboBox(1);
        propPanel.add(vs_m);
        
        label = new JLabel("rho_m");
        propPanel.add(label);
        rho_m = createComboBox(1);
        propPanel.add(rho_m);
        
        propPanel.add(new JLabel("rho_h"));
        rho_h = createComboBox(1);
        propPanel.add(rho_h);
        
        propPanel.add(new JLabel("vp_s"));
        vp_s = createComboBox(1);
        propPanel.add(vp_s);
        
        propPanel.add(new JLabel("vs_s"));
        vs_s = createComboBox(1);
        propPanel.add(vs_s);
        
        propPanel.add(new JLabel("phi_s"));
        phi_s = createComboBox(1);
        propPanel.add(phi_s);
        
        propPanel.add(new JLabel("S_h"));
        S_h = createComboBox(1);
        propPanel.add(S_h);
        
        propPanel.add(new JLabel("vp_eff"));
        vp_eff = createComboBox(1);
        propPanel.add(vp_eff);
        
        propPanel.add(new JLabel("rho_eff"));
        rho_eff=createComboBox(1);
        propPanel.add(rho_eff);
        
        propPanel.add(new JLabel("Z_eff"));
        Z_eff = createComboBox(1);
        propPanel.add(Z_eff);
        
        propPanel.add(new JLabel("fluid_type"));
        fluid_type = createComboBox(1);
        propPanel.add(fluid_type);
        
        propPanel.add(new JLabel("R_near"));
        R_near = createComboBox(1);
        propPanel.add(R_near);
        
        propPanel.add(new JLabel("NG"));
        NG = createComboBox(1);
        propPanel.add(NG);
        
        propPanel.add(new JLabel("d"));
        d = createComboBox(1);
        propPanel.add(d);
        
        propPanel.add(new JLabel("chisquare"));
        chisquare = createComboBox(1);
        propPanel.add(chisquare);
        
        propPanel.add(new JLabel("R_far"));
        R_far = createComboBox(1);
        propPanel.add(R_far);
        
        propPanel.add(new JLabel("LFIV"));
        LFIV = createComboBox(1);
        propPanel.add(LFIV);
        
        propPanel.add(new JLabel("float_fraction"));
        float_fraction = createComboBox(1);
        propPanel.add(float_fraction);
        
		return propPanel;
	}
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		//updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		pane.add(updateB);
		pane.add(Box.createRigidArea(HGAP15));
		buttonPanel.add(pane, BorderLayout.EAST);
		return buttonPanel;
	}
	
	private JPanel createOutputPane() {
		JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.X_AXIS));
        outputPanel.add(Box.createRigidArea(HGAP15));
        TitledBorder tb = new TitledBorder("Realizations:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
        outputPanel.setBorder(tb);
        outputPanel.add(Box.createRigidArea(HGAP10));
        outputPanel.add(new JLabel("Prior:"));
        priorOutF = new JTextField("", 10);
        outputPanel.add(priorOutF);
        outputPanel.add(Box.createRigidArea(HGAP15));
        outputPanel.add(new JLabel("Post:"));
        postOutF = new JTextField("", 10);
        outputPanel.add(postOutF);
		return outputPanel;
	}
	
	private JComboBox createComboBox(int index) {
		JComboBox combo = new JComboBox() ;
		combo.addItem("false");
		combo.addItem("true");
		combo.setSelectedIndex(index);
		return combo;
	}
	
	private JPanel createRunPane() {
		boolean isOne = true;
		String trace  = "Trace Number:";
		String epNum  = "ep  at:";
		String cdpNum = "cdp at:";
		if (!"lite".equalsIgnoreCase(delegate.getName())) {
			trace = "Trace Ranges:";
			epNum  = "ep  frm:";
			cdpNum = "cdp frm:";
			isOne = false;
		}
		JPanel runPane = new JPanel();
		runPane.setLayout(new BoxLayout(runPane, BoxLayout.Y_AXIS));
		TitledBorder tb = new TitledBorder("Run Info:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
		runPane.setBorder(tb);
		
		JPanel numRuns = new JPanel();
		numRuns.setLayout(new BorderLayout());
		
		JPanel numPanel = new JPanel();
		numRunF = new JTextField("", 3);
		numPanel.add(Box.createRigidArea(HGAP15));
		numPanel.add(new JLabel("# of Runs:"));
		numPanel.add(numRunF);
		numPanel.add(Box.createRigidArea(HGAP15));
		numPanel.add(new JLabel("tmin"));
		tminF = new JTextField("", 3);
		numPanel.add(tminF);
		numPanel.add(Box.createRigidArea(HGAP15));
		numPanel.add(new JLabel("tmax"));
		tmaxF = new JTextField("", 3);
		numPanel.add(tmaxF);
		numRuns.add(numPanel, BorderLayout.WEST);
		
		JPanel p0 = new JPanel();
		p0.setLayout(new BorderLayout()); 
		JPanel traceLabel = new JPanel();
	    traceLabel.add(Box.createRigidArea(HGAP15));
		traceLabel.add(new JLabel(trace));
		p0.add(traceLabel, BorderLayout.WEST);
		
		JPanel p1 = new JPanel();
		p1.setLayout(new BorderLayout()); 
		
		JPanel epPanel = new JPanel();
		epPanel.add(Box.createRigidArea(HGAP50));
		epPanel.add(new JLabel(epNum));
		epF = new JTextField("", 3);
		epPanel.add(epF);
		if (!isOne) {
		   epPanel.add(new JLabel("to"));
		   epF2 = new JTextField("", 3);
		   epPanel.add(epF2);
		   epPanel.add(new JLabel("by"));
		   epBy = new JTextField("", 3);
		   epPanel.add(epBy);
		}
		p1.add(epPanel, BorderLayout.WEST);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new BorderLayout()); 
		JPanel cdpPanel = new JPanel();
		cdpPanel.add(Box.createRigidArea(HGAP50));
		cdpPanel.add(new JLabel(cdpNum));
		cdpF = new JTextField("", 3);
		cdpPanel.add(cdpF);
		if (!isOne) {
		   cdpPanel.add(new JLabel("to"));
		   cdpF2 = new JTextField("", 3);
		   cdpPanel.add(cdpF2);
		   cdpPanel.add(new JLabel("by"));
		   cdpBy = new JTextField("", 3);
		   cdpPanel.add(cdpBy);
		}
		p2.add(cdpPanel, BorderLayout.WEST);
		//tmin.add(tminPanel, BorderLayout.WEST);
		
		JPanel nfPanel = new JPanel();
		nfPanel.setLayout(new BorderLayout());  
		
		
		JPanel nf = new JPanel();
		nf.add(Box.createRigidArea(HGAP15));
		nf.add(new JLabel("Stacks:"));
		nf.add(Box.createRigidArea(HGAP10));
		nfPanel.add(nf, BorderLayout.WEST);
		JRadioButton nearR = new JRadioButton("Near Only");
		nearR.setSelected(true);
		JRadioButton nfR = new JRadioButton("Near and Far");
		ButtonGroup bGroup = new ButtonGroup();
		bGroup.add(nearR);
		bGroup.add(nfR);
		nf.add(nearR);
		nf.add(nfR);
		
		runPane.add(numRuns);
		runPane.add(p0);
		runPane.add(p1);
		runPane.add(p2);
		runPane.add(nfPanel);
		
		return runPane;
	}
	
}
