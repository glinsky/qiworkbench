/*
 * <Purpose>
 * <p>
 * Created on March 20, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpPanel;
import com.bhpb.core.AgentDelegate;
import com.bhpb.actions.InnerCompAction;

public class ModelMiddle extends BasePane {
	private static final long serialVersionUID = 1L;
	JPanel     lPane;
	JTextField nameF, timeF, ngF;
	JButton    updateB, addB, removeB;
	Map<String, BhpPanel>  layers = new HashMap<String, BhpPanel>();
	
	public ModelMiddle(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel mPane = new JPanel();
		mPane.setLayout(new BorderLayout());
		
		BhpPanel pane1   = new LayerPanel(this, "Layer 1", "", "", null, 1);
		layers.put(pane1.getPaneName(), pane1);
		BhpPanel pane2   = new LayerPanel(this, "Layer 2", "", "", null, 1);
		layers.put(pane2.getPaneName(), pane2);
		
		JPanel Panex = new JPanel();
		Panex.setLayout(new BoxLayout(Panex, BoxLayout.Y_AXIS));
		Panex.add(Box.createRigidArea(VGAP5));
		Panex.add(createButtons());
		
		lPane = new JPanel();
		lPane.setLayout(new BoxLayout(lPane, BoxLayout.Y_AXIS));
		lPane.add(pane1);
		lPane.add(pane2);
		Panex.add(lPane);
		
		mPane.add(Panex, BorderLayout.NORTH);
		getBasePanel().add(mPane);
	}
	
	public void manageObject(List<String> paramList) {
		BhpPanel  bhpPanel = null;
		if (ModelPanel.LAYER.equals(paramList.get(0))) {
			bhpPanel = layers.get(paramList.get(1));
			bhpPanel.manageObject(paramList);
		} else if (ModelPanel.LAYER_ADD.equals(paramList.get(0) ) ) {
			addNew();
		} else if (ModelPanel.LAYER_DEL.equals(paramList.get(0) )) {
			removeSelected();		
		}
		delegate.resize();
	}
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		//updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		pane.add(updateB);
		pane.add(Box.createRigidArea(HGAP15));
		
		JPanel pane1 = new JPanel();
		addB = new JButton("ADD");
		List<String> strList = new ArrayList<String>();
		strList.add(ModelPanel.LAYER_ADD);
		addB.addActionListener(new InnerCompAction(this, strList));
		removeB = new JButton("DEL");
		strList = new ArrayList<String>();
		strList.add(ModelPanel.LAYER_DEL);
		removeB.addActionListener(new InnerCompAction(this, strList));
		nameF = new JTextField("", 10);
		timeF = new JTextField("", 6);
		ngF = new JTextField("", 3);
		pane1.add(Box.createRigidArea(HGAP15));
		pane1.add(new JLabel("Name:"));
		pane1.add(nameF);
		pane1.add(new JLabel("Time:"));
		pane1.add(timeF);
		pane1.add(new JLabel("N/G:"));
		pane1.add(ngF);
		pane1.add(addB);
		pane1.add(removeB);
		
		buttonPanel.add(pane, BorderLayout.EAST);
		buttonPanel.add(pane1, BorderLayout.WEST);
		return buttonPanel;
	}
	
	private void addNew() {
		String tempName = nameF.getText();
		String tempTime = timeF.getText();
		String tempNG   = ngF.getText();
		if (tempName == null || tempName.length() == 0) return;
		if (! validateName(tempName)) {
			JOptionPane.showMessageDialog(this, "Error: Duplicated Name");
			return;
		}
		BhpPanel bhpPane   = new LayerPanel(this, tempName, tempTime, tempNG, null, 1);
		layers.put(tempName, bhpPane);
		lPane.add(bhpPane);
	}
	
	private void removeSelected() {
		Iterator it = null;
		String key  = null;
		BhpPanel panex = null;
	    it = layers.entrySet().iterator();
	    while (it.hasNext()) {
	    	Map.Entry entry = (Map.Entry)it.next();
	    	key   = (String)entry.getKey();
	    	panex = (BhpPanel)entry.getValue();
	    	if (panex.selected()) {
	    		layers.remove(key);
	    		lPane.remove(panex);
	    		panex = null;
	    	}	
	    }
	}
	
	private boolean validateName(String name1) {
	    if (layers.keySet().contains(name1)) return false;
		return true;
	}
	
	
}
