/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2007  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.delivery.ui;
/**
 * Constants used within Wavelet Decomposition
 *
 * @author Charlie Jiang
 * @version 1.0
 */
public final class UIConst {
    /**
     * Prevent object construction outside of this class.
     */
    private UIConst() {}

    public static final String TIP_SAND   = "<html>vp = A + B*depth + C*vi<br>porosity = A + B*vp<br>vs = A + B*vp</html";
    public static final String TIP_SHALE  = "<html>vp = A + B*depth + C*vi<br>density = A*vp^B<br>vs = A + B*vp</html";
    
    public static final String[] MODEL_TABLE1 = {"#" , "Analyze", "Type", "LayerName", "Time", "SigmaTime", "depth", "SigmaDepth", "Thickness", "SigmaThickness", "LFIV", "SigmaLFIV"};
    public static final String[] MODEL_TABLE2 = {"#" , "Analyze", "Type", "LayerName", "N/G",  "Sigma N/G", "Spo", "So", "Spg", "Sg", "Splsg", "Slsg"};
    
}
