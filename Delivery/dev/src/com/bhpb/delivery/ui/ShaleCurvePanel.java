/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpPanel;
import com.bhpb.xsdparams.delivery.Inversion;

import java.awt.GridLayout;
import java.util.List;

public class ShaleCurvePanel extends BhpPanel {
	private JTextField vpE, vpA, vpB, vpC, vsE, vsA, vsB, r2E, r2A, r2B;
	private static final long serialVersionUID = 1L;
	JPanel shaleCurve, aLine;

	public ShaleCurvePanel(BasePane base, String name, int width, String tip) {
		super(base, name, tip);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		createCheckBox(RockFluidPanel.SHALE_PANE);
		aLine = createLine();
		shaleCurve = createShaleCurve(width);
		this.add(aLine);
		
		if (expanded()) {	
			this.add(shaleCurve);	
		} else {
			this.remove(shaleCurve);
		}		
		
	}
	
	public void manageObject(List<String> strList) {
		//String action = strList.get(2);
		if (expanded()) {
			this.add(shaleCurve);
		} else {
			this.remove(shaleCurve);
			
		}
		base.getDelegate().resize();
		
	}
	
	private JPanel createShaleCurve(int width) {
		JPanel sPane = new JPanel();
		sPane.setLayout(new GridLayout(4, 5));
		
		sPane.add(new JLabel(""));
		sPane.add(new JLabel("Err"));
		sPane.add(new JLabel("A"));
		sPane.add(new JLabel("B"));
		sPane.add(new JLabel("C"));
		
		
		vpE = new JTextField("", width);
		vpA = new JTextField("", width);
		vpB = new JTextField("", width);
		vpC = new JTextField("", width);
		
		
		r2E = new JTextField("", width);
		r2A = new JTextField("", width);
		r2B = new JTextField("", width);
		sPane.add(new JLabel("Vs"));
		sPane.add(r2E);
		sPane.add(r2A);
		sPane.add(r2B);
		sPane.add(new JLabel(""));
		
		vsE = new JTextField("", width);
		vsA = new JTextField("", width);
		vsB = new JTextField("", width);
		sPane.add(new JLabel("Density"));
		sPane.add(vsE);
		sPane.add(vsA);
		sPane.add(vsB);
		sPane.add(new JLabel(""));
		
		sPane.add(new JLabel("Vp"));
		sPane.add(vpE);
		sPane.add(vpA);
		sPane.add(vpB);
		sPane.add(vpC);
		
		return sPane;
	}
	
	public void populate(Object obj) {
		Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember shale = null;
		shale = (Inversion.RockFluidProperties.RockProperties.NonreservoirEndmember)obj;
		vpE.setText(Float.toString(shale.getVpCurve().getSigma().getValue()));
		vpA.setText(Float.toString(shale.getVpCurve().getIntercept().getValue()));
		vpB.setText(Float.toString(shale.getVpCurve().getDepthCoefficient().getValue()));
		vpC.setText(Float.toString(shale.getVpCurve().getLFIVCoefficient().getValue()));
		
		r2E.setText(Float.toString(shale.getDensityCurve().getSigma().getValue()));
		r2A.setText(Float.toString(shale.getDensityCurve().getExponent().getValue()));
		r2B.setText(Float.toString(shale.getDensityCurve().getFactor().getValue()));
		
		vsE.setText(Float.toString(shale.getVsCurve().getSigma().getValue()));
		vsA.setText(Float.toString(shale.getVsCurve().getIntercept().getValue()));
		vsB.setText(Float.toString(shale.getVsCurve().getSlope().getValue()));	
	}
	
}
