/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import java.awt.BorderLayout;
import java.awt.Color;
import com.bhpb.ui.BasePane;

public class MappingAndRange extends JPanel {
	private static final long serialVersionUID = 1L;
	
	SandCurvePanel[]   sands   = new SandCurvePanel[4];
	ShaleCurvePanel[]  shales  = new ShaleCurvePanel[4];
	FluidPropsPanel[]  fluids  = new FluidPropsPanel[5];

	public MappingAndRange() {
		createPane1();
	}
	
	private void createPane1() {
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel rockPane   = createMappingPane();
		JPanel fluidPane  = createRangePane();;
		
		//add together
		this.add(rockPane);
		this.add(Box.createRigidArea(BasePane.HGAP15));
	    
		this.add(fluidPane);
	}
	
	private JPanel createMappingPane() {
		JPanel mPane = new JPanel();
		mPane.setLayout(new BoxLayout(mPane, BoxLayout.Y_AXIS));
		TitledBorder tb = new TitledBorder("Mappings:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
		mPane.setBorder(tb);
		
		JPanel m1 = new JPanel();
		m1.setLayout(new BorderLayout());
		
		JPanel p1 = new JPanel();
		JTextField mapping1 = new JTextField("", 5);
        p1.add(new JLabel("Mapping:"));
        p1.add(mapping1);
        m1.add(p1, BorderLayout.WEST);
        
        JPanel m2 = new JPanel();
		m2.setLayout(new BorderLayout());
		
		JPanel p2 = new JPanel();
		JTextField mapping2 = new JTextField("", 5);
        p2.add(new JLabel("Mapping:"));
        p2.add(mapping2);
        m2.add(p2, BorderLayout.WEST);
        
        mPane.add(createButtons());
        mPane.add(m1);
        mPane.add(m2);
		return mPane;
	}
	
	private JPanel createRangePane() {
		JPanel rPane = new JPanel();
		rPane.setLayout(new BoxLayout(rPane, BoxLayout.Y_AXIS));
		TitledBorder tb = new TitledBorder("Exclusion Range:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
		rPane.setBorder(tb);
		
		rPane.add(createButtons());
		
		JPanel r1 = new JPanel();
		r1.setLayout(new BorderLayout());
		JPanel panel = new JPanel();
		JTextField start = new JTextField("", 2);
		JTextField end = new JTextField("", 2);
		panel.add(new JLabel("Layer Start:"));
		panel.add(start);
		panel.add(new JLabel("End:"));
		panel.add(end);
		r1.add(panel, BorderLayout.WEST);
		rPane.add(r1);
		
		JPanel r2 = new JPanel();
		r2.setLayout(new BorderLayout());
		panel = new JPanel();
		start = new JTextField("", 2);
		end = new JTextField("", 2);
		panel.add(new JLabel("Layer Start:"));
		panel.add(start);
		panel.add(new JLabel("End:"));
		panel.add(end);	
		r2.add(panel, BorderLayout.WEST);
        rPane.add(r2);
        
		return rPane;
	}
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		JPanel pane1 = new JPanel();
		JButton addB = new JButton("Add");
		JButton removeB = new JButton("Del");
		pane1.add(addB);
		pane1.add(removeB);
		pane1.add(Box.createRigidArea(BasePane.HGAP15));
		buttonPanel.add(pane1, BorderLayout.EAST);
		return buttonPanel;
	}
}
