/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import java.awt.GridLayout;

public class TailPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JButton updateB;
	
	public TailPanel(String title, String p1, String p2) {
		this.setLayout(new GridLayout(3, 1));
		if (title != null) {
			TitledBorder tb = new TitledBorder(title);
	        //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	        tb.setTitleColor(Color.PINK);
			this.setBorder(tb);
		}
		ImageIcon icon = new ImageIcon(getClass().getResource("/icons/update.jpg"));
		updateB = new JButton("");
		updateB.setIcon(icon);
		updateB.setToolTipText("update");
		this.add(updateB);
		this.add(new JLabel(p1));
		this.add(new JLabel(p2));	

	}

}
