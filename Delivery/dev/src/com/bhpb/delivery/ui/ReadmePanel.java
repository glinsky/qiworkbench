/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

import com.bhpb.core.AgentDelegate;
import com.bhpb.ui.BasePane;
import java.awt.BorderLayout;

public class ReadmePanel  extends BasePane {
	private static final long serialVersionUID = 1L;
	public ReadmePanel(AgentDelegate delegate) {
		super(delegate);
	}	
	
    public void initPanel() {
    	JPanel pane1 = new JPanel();
    	pane1.setLayout(new BoxLayout(pane1, BoxLayout.Y_AXIS));
    	pane1.add(createNotes());
    	pane1.add(Box.createRigidArea(BasePane.VGAP25));
    	pane1.add(createLegend());
		getBasePanel().add(pane1, BorderLayout.NORTH);
	}
    
    private JPanel createLegend() {
    	JPanel retPane = new JPanel();
    	retPane.setLayout(new BorderLayout());
    	
    	JPanel panex = new JPanel();
    	panex.setLayout(new GridLayout(2, 3));
    	TitledBorder tb = new TitledBorder("Legend:");
        tb.setTitleColor(Color.BLUE);
        panex.setBorder(tb);
        
        panex.add(new JLabel("Sand N/G = 1"));
        panex.add(new JLabel("Shale N/G = 0"));
        panex.add(new JLabel("Mix 0< N/G <1"));
        JLabel image = createIconLabel("sand", "/icons/sand.jpg");
        panex.add(image);
        image = createIconLabel("Shale", "/icons/shale.jpg");
        panex.add(image);
        image = createIconLabel("Mixing", "/icons/mix.jpg");
        panex.add(image);
        retPane.add(panex, BorderLayout.WEST);
        return  panex;
    }
    
    private JPanel createNotes() {
		JPanel panex = new JPanel();
		panex.setLayout(new BorderLayout());
		JPanel guide = new JPanel();
		guide.setLayout(new BoxLayout(guide, BoxLayout.Y_AXIS));
		JLabel label1 = new JLabel("Modeling Instructions:");
		label1.setFont(new Font("Serif", Font.BOLD, 14));
		label1.setForeground(Color.BLUE);
		JLabel label2 = new JLabel("1.   Delivery Component is based on James & Michael's paper - Delivery - Open Source");
		JLabel label3 = new JLabel("2.   Each layer is modelled as a mixture of two finely-laminated end-member rock types");
		JLabel label31= new JLabel("     a permeable and an impermeable member, see Delivery Page-6");
		JLabel label4 = new JLabel("3.   A typical scenario is that the top and bottom of a large packet of layers have been");
		JLabel label41= new JLabel("     picked reasonably accurately, but the time-loaction of the intermeadiate surfaces is");
		JLabel label42= new JLabel("     not well known. See Delivery Page-8");
		guide.add(Box.createRigidArea(VGAP15));
		guide.add(label1);
		guide.add(Box.createRigidArea(VGAP20));
		guide.add(label2);
		guide.add(Box.createRigidArea(VGAP10));
		guide.add(label3);
		guide.add(Box.createRigidArea(VGAP5));
		guide.add(label31);
		guide.add(Box.createRigidArea(VGAP10));
		guide.add(label4);
		guide.add(Box.createRigidArea(VGAP5));
		guide.add(label41);
		guide.add(Box.createRigidArea(VGAP5));
		guide.add(label42);
		panex.add(guide, BorderLayout.WEST);
		return panex;
	}
}
