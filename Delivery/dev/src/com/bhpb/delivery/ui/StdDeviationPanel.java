/*
 * <Purpose>
 * <p>
 * Created on March 15, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;


import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.GridLayout;

public class StdDeviationPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField vp_m, vs_m, rho_m, NG;
	private JTextField vp_s, vs_s, phi_s, thickness;
	
	public StdDeviationPanel(String title) {
		this.setLayout(new GridLayout(2, 8));
		if (title != null) {
			TitledBorder tb = new TitledBorder(title);
	        //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	        tb.setTitleColor(Color.BLUE);
			this.setBorder(tb);
		}
		createPane1();
	}
	
	private void createPane1() {
		this.add(new JLabel("vp_m"));
		vp_m = new JTextField("500");
		this.add(vp_m);
		
		this.add(new JLabel("vs_m"));
		vs_m = new JTextField("500");
		this.add(vs_m);
		
		this.add(new JLabel("rho_m"));
		rho_m = new JTextField("0.1");
		this.add(rho_m);
		
		this.add(new JLabel("NG"));
		NG = new JTextField("0.1");
		this.add(NG);
		
		this.add(new JLabel("vp_s"));
		vp_s = new JTextField("500");
		this.add(vp_s);
		
		this.add(new JLabel("vs_s"));
		vs_s = new JTextField("500");
		this.add(vs_s);
		
		this.add(new JLabel("phi_s"));
		phi_s = new JTextField("0.1");
		this.add(phi_s);
		
		this.add(new JLabel("thickness"));
		thickness = new JTextField("0.1");
		this.add(thickness);
	}	
}
