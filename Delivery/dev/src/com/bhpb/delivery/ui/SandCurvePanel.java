/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.GridLayout;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpPanel;
import com.bhpb.xsdparams.delivery.Inversion;

public class SandCurvePanel extends BhpPanel {
	private static final long serialVersionUID = 1L;
	private JTextField vpE, vpA, vpB, vpC, vpD, vsE, vsA, vsB, r2E, r2A, r2B, r2C;
	private JTextField rFluid, gVp, gVs, gRho;
    JPanel sandCurve, grainPane, sPane;
    
	public SandCurvePanel(BasePane base, String name, int width, String help) {
		super(base, name, help);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.help = help;
		createCheckBox(RockFluidPanel.SAND_PANE);
		aLine = createLine();
		sandCurve = createSandCurve(width);
		grainPane = createGrainPane();
		sPane = createSandPane();
		
		this.add(aLine);
		
		if (expanded()) {	
			this.add(sPane);	
		} else {
			this.remove(sPane);
		}		
	}
	
	public void manageObject(List<String> strList) {
		//String action = strList.get(2);
		if (expanded()) {
			this.add(sPane);
		} else {
			this.remove(sPane);
			
		}	
	}
	
	private JPanel createSandPane() {
		JPanel sPane = new JPanel();
		sPane.setLayout(new BoxLayout(sPane, BoxLayout.Y_AXIS));
        sPane.add(sandCurve);
        sPane.add(Box.createRigidArea(BasePane.VGAP5));
        sPane.add(grainPane);
		return  sPane;
	}
	
	private JPanel createSandCurve(int width) {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4, 5));
		panel.add(new JLabel(""));
		panel.add(new JLabel("Err"));
		panel.add(new JLabel("A"));
		panel.add(new JLabel("B"));
		panel.add(new JLabel("C"));
		panel.add(new JLabel("D"));
		
		vsE = new JTextField("", width);
		vsA = new JTextField("", width);
		vsB = new JTextField("", width);
		panel.add(new JLabel("Vs"));
		panel.add(vsE);
		panel.add(vsA);
		panel.add(vsB);
		panel.add(new JLabel(""));
		panel.add(new JLabel(""));
		
		r2E = new JTextField("", width);
		r2A = new JTextField("", width);
		r2B = new JTextField("", width);
		r2C = new JTextField("", width);
		panel.add(new JLabel("Porosity"));
		panel.add(r2E);
		panel.add(r2A);
		panel.add(r2B);
		panel.add(r2C);
		panel.add(new JLabel(""));
		
		vpE = new JTextField("", width);
		vpA = new JTextField("", width);
		vpB = new JTextField("", width);
		vpC = new JTextField("", width);
		vpD = new JTextField("", width);
		panel.add(new JLabel("Vp"));
		panel.add(vpE);
		panel.add(vpA);
		panel.add(vpB);
		panel.add(vpC);
		panel.add(vpD);
		
		return panel;
		
	}
	
	private JPanel createGrainPane() {
		JPanel panex= new JPanel();
		panex.setLayout(new BoxLayout(panex, BoxLayout.X_AXIS));
		
		JPanel fluid = new JPanel();
		fluid.setLayout(new BoxLayout(fluid, BoxLayout.X_AXIS));
		rFluid = new JTextField("",4);
		fluid.setBorder(new TitledBorder("Ref Fluid"));
		fluid.add(rFluid);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setBorder(new TitledBorder("Grain"));
		
		panel.add(new JLabel("vp"));
		gVp = new JTextField("",2);
		panel.add(gVp);
		
		panel.add(new JLabel("vs"));
		gVs = new JTextField("",2);
		panel.add(gVs);
		
		panel.add(new JLabel("rho"));
		gRho = new JTextField("",2);
		panel.add(gRho);
		
		panex.add(panel);
		panex.add(Box.createRigidArea(BasePane.HGAP5));
		panex.add(fluid);
		
		return panex;	
	}
	
	public void populate(Object obj) {
		Inversion.RockFluidProperties.RockProperties.ReservoirEndmember sand = null;
		sand = (Inversion.RockFluidProperties.RockProperties.ReservoirEndmember)obj;
		vpE.setText(Float.toString(sand.getVpCurve().getSigma().getValue()));
		vpA.setText(Float.toString(sand.getVpCurve().getIntercept().getValue()));
		vpB.setText(Float.toString(sand.getVpCurve().getDepthCoefficient().getValue()));
		vpC.setText(Float.toString(sand.getVpCurve().getLFIVCoefficient().getValue()));
		if (sand.getVpCurve().getPhiFloatCoefficient() != null)
		   vpD.setText(Float.toString(sand.getVpCurve().getPhiFloatCoefficient().getValue()));
		
		rFluid.setText(sand.getReferenceFluid());
		gVp.setText(Float.toString(sand.getVpGrain().getValue()));
		gVs.setText(Float.toString(sand.getVsGrain().getValue()));
		gRho.setText(Float.toString(sand.getRhoGrain().getValue()));
		
		r2E.setText(Float.toString(sand.getPorosityCurve().getSigma().getValue()));
		r2A.setText(Float.toString(sand.getPorosityCurve().getIntercept().getValue()));
		r2B.setText(Float.toString(sand.getPorosityCurve().getSlope().getValue()));
		if (sand.getPorosityCurve().getPhiFloatCoefficient() != null)
		r2C.setText(Float.toString(sand.getPorosityCurve().getPhiFloatCoefficient().getValue()));
		
		vsE.setText(Float.toString(sand.getVsCurve().getSigma().getValue()));
		vsA.setText(Float.toString(sand.getVsCurve().getIntercept().getValue()));
		vsB.setText(Float.toString(sand.getVsCurve().getSlope().getValue()));	
	}
}
