/*
 * <Purpose>
 * <p>
 * Created on March 15, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.bhpb.ui.UiConst;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpPanel;
import com.bhpb.xsdparams.delivery.Inversion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;

public class LayerPanel extends BhpPanel {
	private static final long serialVersionUID = 1L;
	private JTextField t_base,time,depth,th,NG, LFIV, So, Sg, Slsg, Po, Pg, Plsg;
	private JTextField s_t_base,s_time,s_depth,s_th,s_NG, s_LFIV, s_So, s_Sg, s_Slsg;
	private JComboBox sand, shale,brine,oil,gas,lsg;
	private JCheckBox sync;
	JPanel  layer;
	
	int layerAt = 1;
	//layerAt 
	//   = 0  top
	//   = 1  middle
	//   = 2  bottom
	public LayerPanel(BasePane base, String name, float ftime, float ng, String help, int layerAt) {
		super(base, name, help);
		String strTime = Float.toString(ftime);
		String strNG =Float.toString(ng);
		setValues(strTime, strNG, layerAt);
	}
	
	public LayerPanel(BasePane base, String name, String strTime, String strNG, String help, int layerAt) {
		super(base, name, help);
		setValues(strTime, strNG, layerAt);
		
	}
	
	private void setValues(String strTime, String strNG, int layerAt) {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.layerAt = layerAt;
		NG   = new JTextField(0);
		time = new JTextField(0);

		time.setText(strTime);
		NG.setText(strNG);	
		createCheckBox(ModelPanel.LAYER);
		if (layerAt != 1)  setExpanded(true);
		aLine = createLine();
		layer = createLayer();
		this.add(aLine);
		if (expanded()) {	
			this.add(layer);	
		} else {
			this.remove(layer);
		}			
	}
	
	public void manageObject(List<String> strList) {
		//String action = strList.get(2);
		if (expanded()) {
			this.add(layer);
		} else {
			this.remove(layer);
			
		}
		base.getDelegate().resize();
		
	}
	
	private JPanel createLayer() {
		JPanel panex = new JPanel();
		panex.setLayout(new BoxLayout(panex, BoxLayout.Y_AXIS));
		if (layerAt != 1) {
			TitledBorder tb = new TitledBorder(paneName);
	        //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	        tb.setTitleColor(Color.BLUE);
	        panex.setBorder(tb);
		}
		panex.add(createPane1());
		panex.add(createPane2());
		return panex;
	}
	private JPanel createPane1() {
		JPanel pane1 = new JPanel();
		pane1.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		  
		pane.add(Box.createRigidArea(BasePane.HGAP15));
	    sand = new JComboBox();
		sand.addItem("Ref Sand 1");
	    sand.addItem("Ref sand 2");
	    //pane.add(new JLabel("Sand"));
	    pane.add(sand);
	    
	    pane.add(Box.createRigidArea(BasePane.HGAP10));
	    shale = new JComboBox();
		shale.addItem("Ref Shale 1");
	    shale.addItem("Ref Shale 2");
	    //pane.add(new JLabel("Shale"));
	    pane.add(shale); 
	    
	    pane.add(Box.createRigidArea(BasePane.HGAP10));
	    brine = new JComboBox();
		brine.addItem("Brine 1");
	    brine.addItem("Brine 2");
	    pane.add(brine);
	  
	    Po = new JTextField("", 3);
	    Pg = new JTextField("", 3);
	    Plsg = new JTextField("",3);
	    pane.add(Box.createRigidArea(BasePane.HGAP15));
	    pane.add(new JLabel("Po"));
	    pane.add(Po);
	    pane.add(Box.createRigidArea(BasePane.HGAP2));
	    pane.add(new JLabel("Pg"));
	    pane.add(Pg);
	    pane.add(Box.createRigidArea(BasePane.HGAP2));
	    pane.add(new JLabel("Plsg"));
	    pane.add(Plsg);
	    
	    sync = new JCheckBox("Sync");
	   
	    pane1.add(pane, BorderLayout.WEST);
	    pane1.add(sync, BorderLayout.EAST);
	    
		return pane1;
	}	
	
	private JPanel createPane2() {
		//JPanel pane1 = new JPanel();
		//pane1.setLayout(new BorderLayout());
		
		JPanel pane2 = new JPanel();
		pane2.setLayout(new BoxLayout(pane2, BoxLayout.X_AXIS));
		pane2.add(createPane2_1());
		pane2.add(createPane2_2());
		pane2.add(createPane2_3());
		
		//pane1.add(pane2, BorderLayout.WEST);
		return pane2;
	}	
	
	private JPanel createPane2_1() {
		JPanel pane2 = new JPanel();
		pane2.setLayout(new GridLayout(3,1));
		
		pane2.add(new JLabel(""));
		pane2.add(new JLabel("val"));
		pane2.add(new JLabel("err"));
		return pane2;
	}	
	
	
	private JPanel createPane2_2() {
		JPanel pane2 = new JPanel();
		int num = 8;
		if (2 == layerAt) num = 9;
		pane2.setLayout(new GridLayout(3, num));
		
		pane2.add(new JLabel("time"));
		pane2.add(new JLabel("N/G"));
		pane2.add(new JLabel("depth"));
		pane2.add(new JLabel("th"));
		pane2.add(new JLabel("LFIV"));
		pane2.add(new JLabel("So"));
		pane2.add(new JLabel("Sg"));
		pane2.add(new JLabel("Slsg"));
		if (2 == layerAt) pane2.add(new JLabel("t_base"));
		
		
		depth = new JTextField(0);
		th = new JTextField(0);
		LFIV = new JTextField(8000);
		So = new JTextField(0);
		Sg = new JTextField(0);
		Slsg = new JTextField(0);
		t_base = new JTextField(0);
		
		pane2.add(time);
		pane2.add(NG);
		pane2.add(depth);
		pane2.add(th);
		pane2.add(LFIV);
		pane2.add(So);
		pane2.add(Sg);
		pane2.add(Slsg);
		if (2 == layerAt) pane2.add(t_base);
		
		s_time   = new JTextField(0);
		s_NG     = new JTextField(0);
		s_depth  = new JTextField(0);
		s_th     = new JTextField(0);
		s_LFIV   = new JTextField(8000);
		s_So     = new JTextField(0);
		s_Sg     = new JTextField(0);
		s_Slsg   = new JTextField(0);
		s_t_base = new JTextField(0);
		
		pane2.add(s_time);
		pane2.add(s_NG);
		pane2.add(s_depth);
		pane2.add(s_th);
		pane2.add(s_LFIV);
		pane2.add(s_So);
		pane2.add(s_Sg);
		pane2.add(s_Slsg);
		if (2 == layerAt) pane2.add(s_t_base);
		
		return pane2;
	}	
	
	private JPanel createPane2_3() {
		JPanel pane2 = new JPanel();
		pane2.setLayout(new GridLayout(3, 3));
		
		pane2.add(new JLabel("Oil"));
		pane2.add(new JLabel("Gas"));
		pane2.add(new JLabel("LSG"));
	    
		oil = new JComboBox();
		oil.addItem("none");
	    oil.addItem("Oil 1");
		oil.addItem("Oil 2");
		pane2.add(oil);
		
		
		gas = new JComboBox();
	    gas.addItem("none");
		gas.addItem("Gas 1");
	    gas.addItem("Gas 2");
	    pane2.add(gas);
	     
		
		lsg = new JComboBox();
		lsg.addItem("none");
	    lsg.addItem("LSG 1");
		lsg.addItem("LSG 2");
		pane2.add(lsg);
		
		pane2.add(new JLabel(""));
		pane2.add(new JLabel(""));
		pane2.add(new JLabel(""));
		
		return pane2;
	}	
	
	protected JPanel createLine() {
		cpane = new JPanel();
		JLabel image = createIconLabel("layer", getLayerImage());
		cpane.add(image);
		super.createLine();
		return aLine;
	}
	
	private String getLayerImage() {
	   String ret = null;
	   String strNG = NG.getText();
	   float type = 0;
	   if (!"".equals(strNG)) {
		   type = Float.parseFloat(strNG);
	   }
	   if ( type < 0.0001f) {
		   ret = UiConst.ICON_SHALE;
	   } else if ( type > 0.999f && type < 1.0001f) {
		   ret = UiConst.ICON_SAND;
	   } else {
		   ret = UiConst.ICON_MIX;
	   }
	   return ret;
	}
	
    public void populate(Inversion.ModelDescription.MiddleLayer layer) {
    	sync.setSelected(layer.isSynchroniseRockMatrix());
		if (layer.getReservoirEndmember().getOil() != null) {
		   Po.setText(Float.toString(layer.getReservoirEndmember().getOil().getProbabilityOfOil().getValue()));
		   So.setText(Float.toString(layer.getReservoirEndmember().getOil().getSaturationOfOil().getValue()));
		   if (layer.getReservoirEndmember().getOil().getSigmaSaturationOfOil() != null) 
		     s_So.setText(Float.toString(layer.getReservoirEndmember().getOil().getSigmaSaturationOfOil().getValue()));
		}
		if (layer.getReservoirEndmember().getGas() != null) {
		   Pg.setText(Float.toString(layer.getReservoirEndmember().getGas().getProbabilityOfGas().getValue()));
		   Sg.setText(Float.toString(layer.getReservoirEndmember().getGas().getSaturationOfGas().getValue()));
		   if (layer.getReservoirEndmember().getGas().getSigmaSaturationOfGas() != null) 
		      s_Sg.setText(Float.toString(layer.getReservoirEndmember().getGas().getSigmaSaturationOfGas().getValue()));  
		}
		if (layer.getReservoirEndmember().getLsg() != null) {
		   Plsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getProbabilityOfLsg().getValue()));
		   Slsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getSaturationOfLsg().getValue()));
		   if (layer.getReservoirEndmember().getLsg().getSigmaSaturationOfLsg() != null) 
		      s_Slsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getSigmaSaturationOfLsg().getValue()));
		}
		
		time.setText(Float.toString(layer.getTime().getValue()));
		NG.setText(Float.toString(layer.getNetToGross().getValue()));
		depth.setText(Float.toString(layer.getLFIV().getValue()));
		th.setText(Float.toString(layer.getThickness().getValue()));
		LFIV.setText(Float.toString(layer.getLFIV().getValue()));
		
		if (layer.getSigmaTime() != null) 
		   s_time.setText(Float.toString(layer.getSigmaTime().getValue()));
		if (layer.getSigmaNetToGross() != null) 
		   s_NG.setText(Float.toString(layer.getSigmaNetToGross().getValue()));
		if (layer.getSigmaDepth() != null) 
		   s_depth.setText(Float.toString(layer.getSigmaDepth().getValue()));
		if (layer.getSigmaThickness() != null)
		   s_th.setText(Float.toString(layer.getSigmaThickness().getValue()));
		if (layer.getSigmaLFIV() != null) 
		   s_LFIV.setText(Float.toString(layer.getSigmaLFIV().getValue()));	
	}
    
	public void populateTop(Inversion.ModelDescription.TopLayer layer) {
		sync.setSelected(layer.isSynchroniseRockMatrix());
		if (layer.getReservoirEndmember().getOil() != null) {
		   Po.setText(Float.toString(layer.getReservoirEndmember().getOil().getProbabilityOfOil().getValue()));
		   So.setText(Float.toString(layer.getReservoirEndmember().getOil().getSaturationOfOil().getValue()));
		   if (layer.getReservoirEndmember().getOil().getSigmaSaturationOfOil() != null) 
		     s_So.setText(Float.toString(layer.getReservoirEndmember().getOil().getSigmaSaturationOfOil().getValue()));
		}
		if (layer.getReservoirEndmember().getGas() != null) {
		   Pg.setText(Float.toString(layer.getReservoirEndmember().getGas().getProbabilityOfGas().getValue()));
		   Sg.setText(Float.toString(layer.getReservoirEndmember().getGas().getSaturationOfGas().getValue()));
		   if (layer.getReservoirEndmember().getGas().getSigmaSaturationOfGas() != null) 
		      s_Sg.setText(Float.toString(layer.getReservoirEndmember().getGas().getSigmaSaturationOfGas().getValue()));  
		}
		if (layer.getReservoirEndmember().getLsg() != null) {
		   Plsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getProbabilityOfLsg().getValue()));
		   Slsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getSaturationOfLsg().getValue()));
		   if (layer.getReservoirEndmember().getLsg().getSigmaSaturationOfLsg() != null) 
		      s_Slsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getSigmaSaturationOfLsg().getValue()));
		}
		
		time.setText(Float.toString(layer.getTime().getValue()));
		NG.setText(Float.toString(layer.getNetToGross().getValue()));
		depth.setText(Float.toString(layer.getLFIV().getValue()));
		th.setText(Float.toString(layer.getThickness().getValue()));
		LFIV.setText(Float.toString(layer.getLFIV().getValue()));
		
		if (layer.getSigmaTime() != null) 
		   s_time.setText(Float.toString(layer.getSigmaTime().getValue()));
		if (layer.getSigmaNetToGross() != null) 
		   s_NG.setText(Float.toString(layer.getSigmaNetToGross().getValue()));
		if (layer.getSigmaDepth() != null) 
		   s_depth.setText(Float.toString(layer.getSigmaDepth().getValue()));
		if (layer.getSigmaThickness() != null)
		   s_th.setText(Float.toString(layer.getSigmaThickness().getValue()));
		if (layer.getSigmaLFIV() != null) 
		   s_LFIV.setText(Float.toString(layer.getSigmaLFIV().getValue()));		
	}
	
    public void populateBottom(Inversion.ModelDescription.BottomLayer layer) {
    	sync.setSelected(layer.isSynchroniseRockMatrix());
		if (layer.getReservoirEndmember().getOil() != null) {
		   Po.setText(Float.toString(layer.getReservoirEndmember().getOil().getProbabilityOfOil().getValue()));
		   So.setText(Float.toString(layer.getReservoirEndmember().getOil().getSaturationOfOil().getValue()));
		   if (layer.getReservoirEndmember().getOil().getSigmaSaturationOfOil() != null) 
		     s_So.setText(Float.toString(layer.getReservoirEndmember().getOil().getSigmaSaturationOfOil().getValue()));
		}
		if (layer.getReservoirEndmember().getGas() != null) {
		   Pg.setText(Float.toString(layer.getReservoirEndmember().getGas().getProbabilityOfGas().getValue()));
		   Sg.setText(Float.toString(layer.getReservoirEndmember().getGas().getSaturationOfGas().getValue()));
		   if (layer.getReservoirEndmember().getGas().getSigmaSaturationOfGas() != null) 
		      s_Sg.setText(Float.toString(layer.getReservoirEndmember().getGas().getSigmaSaturationOfGas().getValue()));  
		}
		if (layer.getReservoirEndmember().getLsg() != null) {
		   Plsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getProbabilityOfLsg().getValue()));
		   Slsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getSaturationOfLsg().getValue()));
		   if (layer.getReservoirEndmember().getLsg().getSigmaSaturationOfLsg() != null) 
		      s_Slsg.setText(Float.toString(layer.getReservoirEndmember().getLsg().getSigmaSaturationOfLsg().getValue()));
		}
		
		time.setText(Float.toString(layer.getTime().getValue()));
		NG.setText(Float.toString(layer.getNetToGross().getValue()));
		depth.setText(Float.toString(layer.getLFIV().getValue()));
		th.setText(Float.toString(layer.getThickness().getValue()));
		LFIV.setText(Float.toString(layer.getLFIV().getValue()));
		t_base.setText(Float.toString(layer.getTBase().getValue()));
		
		if (layer.getSigmaTime() != null) 
		   s_time.setText(Float.toString(layer.getSigmaTime().getValue()));
		if (layer.getSigmaNetToGross() != null) 
		   s_NG.setText(Float.toString(layer.getSigmaNetToGross().getValue()));
		if (layer.getSigmaDepth() != null) 
		   s_depth.setText(Float.toString(layer.getSigmaDepth().getValue()));
		if (layer.getSigmaThickness() != null)
		   s_th.setText(Float.toString(layer.getSigmaThickness().getValue()));
		if (layer.getSigmaLFIV() != null) 
		   s_LFIV.setText(Float.toString(layer.getSigmaLFIV().getValue()));	
		if (layer.getSigmaTBase() != null) 
			s_t_base.setText(Float.toString(layer.getSigmaTBase().getValue()));		
	}
}
