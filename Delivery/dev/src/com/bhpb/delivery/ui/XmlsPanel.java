/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import java.awt.BorderLayout;
import com.bhpb.model.ui.NameValue;
import com.bhpb.model.ui.NameValues;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpTabbedPane;
import com.bhpb.ui.TabedBasePane;
import com.bhpb.ui.UiConst;
import com.bhpb.core.AgentDelegate;
import com.bhpb.delivery.DelivConst;
import com.bhpb.ui.*;

public class XmlsPanel extends TabedBasePane {
	private static final long serialVersionUID = 1L;

	public XmlsPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		thisTab = new BhpTabbedPane(this, UiConst.GO_NORMAL);
		
		BasePane rfXml = new TextView(delegate);
		rfXml.initPanel();
		rfXml.setModelName(UiConst.LOAD_MODEL_DATA);
		
		BasePane LayerXml = new TextView(delegate);
		LayerXml.initPanel();
		LayerXml.setModelName(UiConst.LOAD_MODEL_DATA);
		
		BasePane modelXml = new TextView(delegate);
		modelXml.initPanel();
		modelXml.setModelName(UiConst.LOAD_MODEL_DATA);
		
		BasePane uiXml = new TextView(delegate);
		uiXml.initPanel();
		uiXml.setModelName(UiConst.LOAD_MODEL_UI);
		
		int cnt = 0;
        NameValues vals = getElement(DelivConst.UI_ELE_XML);
		
		NameValue val = vals.getNameValue(DelivConst.PANE_XML_ROCK_F);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  rfXml.getBasePanel());
			panes[cnt] = rfXml;
			cnt++;
		}
		
		
		val = vals.getNameValue(DelivConst.PANE_XML_LAYERS);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  LayerXml.getBasePanel());
			panes[cnt] = LayerXml;
			cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_XML_MODEL);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  modelXml.getBasePanel());
			panes[cnt] = modelXml;
			cnt++;
		}
		
		val = vals.getNameValue(DelivConst.PANE_XML_UI);
		if (val != null) {
			thisTab.add(val.getDisplayName(),  uiXml.getBasePanel());
			panes[cnt] = uiXml;
		}
		
		thisTab.getModel().addChangeListener(thisTab);
		getBasePanel().add(thisTab, BorderLayout.CENTER);
	}

}
