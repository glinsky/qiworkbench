/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.border.TitledBorder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import com.bhpb.ui.BasePane;
import com.bhpb.core.AgentDelegate;

public class SettingsPanel extends BasePane {
	private static final long serialVersionUID = 1L;
	
	JTextField projNameF, projDirF, datasetDirF, seisDirF, outDirF, modelFileF;
	JTextField stackNameF, waveletNameF, snF, aF, stackNameN, waveletNameN, snN, bF;
	JTextField priorOutF,  postOutF, numRunF, tminF, tmaxF, epF, cdpF;
	
	

	public SettingsPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel settingPane = new JPanel();
		settingPane.setLayout(new BoxLayout(settingPane, BoxLayout.Y_AXIS));
		
		JPanel projPane   = createProjPane();
		JPanel inputPane  = createInputPane();;
		JPanel outputPane = createOutputPane();
		JPanel runPane    = createRunPane();
		
		//add together
		settingPane.add(Box.createRigidArea(VGAP15));
		settingPane.add(projPane);
	    settingPane.add(Box.createRigidArea(VGAP10));
	    
	    settingPane.add(inputPane);
	    settingPane.add(Box.createRigidArea(VGAP10));
	    
	    settingPane.add(outputPane);
	    settingPane.add(Box.createRigidArea(VGAP10));
	    
	    settingPane.add(runPane);
	    settingPane.add(Box.createRigidArea(VGAP10));
	    
		getBasePanel().add(settingPane, BorderLayout.NORTH);
	}
	
	private JPanel createProjPane() {
		JPanel projPanel = new JPanel();
        projPanel.setLayout(new BoxLayout(projPanel, BoxLayout.X_AXIS));
        TitledBorder tb = new TitledBorder("Project Information:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    projPanel.setBorder(tb);
        
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel label1 = new JPanel(new GridLayout(3, 1));
        label1.add(new JLabel("Project Name:"));
        label1.add(new JLabel("Project Relative Dir:"));
        label1.add(new JLabel("Datasets Relative Dir:"));
        projPanel.add(label1);
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel field1 = new JPanel(new GridLayout(3, 1));
        projNameF   = new JTextField("", 12);
        projDirF    = new JTextField("", 12);
        datasetDirF = new JTextField("", 12);
        field1.add(projNameF);
        field1.add(projDirF);
        field1.add(datasetDirF);
        projPanel.add(field1);
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel label2 = new JPanel(new GridLayout(3, 1));
        label2.add(new JLabel("Seismic Relative Dir:"));
        label2.add(new JLabel("Output Relative Dir:"));
        label2.add(new JLabel("Model File name:"));
        projPanel.add(label2);
        projPanel.add(Box.createRigidArea(HGAP15));
        
        JPanel field2 = new JPanel(new GridLayout(3, 1));
        seisDirF   = new JTextField("", 12);
        outDirF    = new JTextField("", 12);
        modelFileF = new JTextField("", 12);
        field2.add(seisDirF);
        field2.add(outDirF);
        field2.add(modelFileF);
        projPanel.add(field2);
        
		return projPanel;
	}
	
	private JPanel createInputPane() {
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));
		TitledBorder tb = new TitledBorder("Input:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
		inputPanel.setBorder(tb);
		inputPanel.add(Box.createRigidArea(HGAP15));
		
        JPanel label1 = new JPanel(new GridLayout(4, 1));
        label1.add(new JLabel("Stack File Name:"));
        label1.add(new JLabel("Wavelet File Name:"));
        label1.add(new JLabel("Wavelet S/N:"));
        label1.add(new JLabel("AVO:"));
        
        JPanel label2 = new JPanel(new GridLayout(4, 1));
        label2.add(new JLabel("Near:"));
        label2.add(new JLabel("Near:"));
        label2.add(new JLabel("Near:"));
        label2.add(new JLabel("A:"));
        
        JPanel field1 = new JPanel(new GridLayout(4, 1));
        stackNameN    = new JTextField("", 12);
        waveletNameN  = new JTextField("", 12);
        snN           = new JTextField("", 12);
        aF           = new JTextField("", 3);
        field1.add(stackNameN);
        field1.add(waveletNameN);
        field1.add(snN);
        field1.add(aF);
        
        JPanel label3 = new JPanel(new GridLayout(4, 1));
        label3.add(new JLabel("Far:"));
        label3.add(new JLabel("Far:"));
        label3.add(new JLabel("Far:"));
        label3.add(new JLabel("B:"));
        
        
        JPanel field2 = new JPanel(new GridLayout(4, 1));
        stackNameN    = new JTextField("", 12);
        waveletNameN  = new JTextField("", 12);
        snN           = new JTextField("", 12);
        bF           = new JTextField("", 3);
        field2.add(stackNameN);
        field2.add(waveletNameN);
        field2.add(snN);
        field2.add(bF);
        
        
        inputPanel.add(label1);
        inputPanel.add(Box.createRigidArea(HGAP10));
        inputPanel.add(label2);
        inputPanel.add(Box.createRigidArea(HGAP10));
        inputPanel.add(field1);
        inputPanel.add(Box.createRigidArea(HGAP25));
        inputPanel.add(label3);
        inputPanel.add(Box.createRigidArea(HGAP10));
        inputPanel.add(field2);
        
		return inputPanel;
	}
	
	private JPanel createOutputPane() {
		JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.X_AXIS));
        outputPanel.add(Box.createRigidArea(HGAP15));
        TitledBorder tb = new TitledBorder("Output:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
        outputPanel.setBorder(tb);
        outputPanel.add(new JLabel("Realizations Output"));
        outputPanel.add(Box.createRigidArea(HGAP10));
        outputPanel.add(new JLabel("Prior:"));
        priorOutF = new JTextField("", 10);
        outputPanel.add(priorOutF);
        outputPanel.add(Box.createRigidArea(HGAP15));
        outputPanel.add(new JLabel("Post:"));
        postOutF = new JTextField("", 10);
        outputPanel.add(postOutF);
		return outputPanel;
	}
	
	private JPanel createRunPane() {
		JPanel runPane = new JPanel();
		runPane.setLayout(new BoxLayout(runPane, BoxLayout.Y_AXIS));
		TitledBorder tb = new TitledBorder("Run Settings:");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
		runPane.setBorder(tb);
		
		JPanel numRuns = new JPanel();
		numRuns.setLayout(new BorderLayout());
		
		JPanel numPanel = new JPanel();
		numRunF = new JTextField("", 3);
		numPanel.add(Box.createRigidArea(HGAP15));
		numPanel.add(new JLabel("Number of Runs:"));
		numPanel.add(numRunF);
		numRuns.add(numPanel, BorderLayout.WEST);
		
		JPanel tmin = new JPanel();
		tmin.setLayout(new BorderLayout()); 
		
		JPanel tminPanel = new JPanel();
		tminPanel.add(Box.createRigidArea(HGAP15));
		tminPanel.add(new JLabel("tmin:"));
		tminF = new JTextField("", 3);
		tminPanel.add(tminF);
		tminPanel.add(new JLabel("tmax:"));
		tmaxF = new JTextField("", 3);
		tminPanel.add(tmaxF);
		tminPanel.add(new JLabel("ep:"));
		epF = new JTextField("", 3);
		tminPanel.add(epF);
		tminPanel.add(new JLabel("cdp:"));
		cdpF = new JTextField("", 3);
		tminPanel.add(cdpF);
		tmin.add(tminPanel, BorderLayout.WEST);
		
		JPanel nfPanel = new JPanel();
		nfPanel.setLayout(new BorderLayout());  
		
		
		JPanel nf = new JPanel();
		nf.add(Box.createRigidArea(HGAP15));
		nf.add(new JLabel("Stacks:"));
		nfPanel.add(nf, BorderLayout.WEST);
		JRadioButton nearR = new JRadioButton("Near Only");
		JRadioButton nfR = new JRadioButton("Near and Far");
		ButtonGroup bGroup = new ButtonGroup();
		bGroup.add(nearR);
		bGroup.add(nfR);
		nf.add(nearR);
		nf.add(nfR);
		
		runPane.add(numRuns);
		runPane.add(tmin);
		runPane.add(nfPanel);
		
		return runPane;
	}
}
