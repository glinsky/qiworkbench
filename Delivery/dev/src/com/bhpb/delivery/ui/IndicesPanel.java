/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import com.bhpb.ui.BasePane;
import com.bhpb.core.AgentDelegate;

public class IndicesPanel extends BasePane {
	private static final long serialVersionUID = 1L;

	JTextField min_offset, max_offset, reflector_time, stack_velocity;
	JTextField nearSN,     farSN,      probOil,    satOil,        sigmaSatOil;
	JTextField time_above, time_below, probGas,    satGas,        sigmaSatGas;
	JTextField A,          s_A,        probLsg,    satLsg,        sigmaSatLsg;
	JTextField B,          s_B,        rhoBrine,   s_rhoBrine,    vp_brine; 
	JTextField net2gross,  s_net2gross,float_fra,  s_float_fra,   s_vp_brine; 
	JTextField time,       s_time,     LFIV,       s_LFIV,        thickness; 
	JTextField depth,      s_depth,    SSdepth,    s_SSdepth,     s_thickness;
	JButton    updateB;
	
	public IndicesPanel(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		JPanel infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.Y_AXIS));
		
		JPanel bPane   = createButtons();
		JPanel pane1   = createIndicesPane();
		
        //add together
		infoPane.add(bPane);
		infoPane.add(pane1);
		
		getBasePanel().add(infoPane, BorderLayout.NORTH);
	}
	
	private JPanel createIndicesPane() {
		JPanel propPanel = new JPanel();
		propPanel.setLayout(new GridLayout(8, 10));
        TitledBorder tb = new TitledBorder("Link -> Param Location in SU Data File");
	    //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	    tb.setTitleColor(Color.BLUE);
	    propPanel.setBorder(tb);
        //(1)
	    JLabel label = new JLabel("min_offset");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        min_offset = new JTextField("1");
        propPanel.add(min_offset);
        
        label = new JLabel("max_offset");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        max_offset = new JTextField("1");
        propPanel.add(max_offset);
        
        label = new JLabel("refl_time");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        reflector_time = new JTextField("1");
        propPanel.add(reflector_time);
        
        label = new JLabel("stack_v");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        stack_velocity = new JTextField("1");
        propPanel.add(stack_velocity);
        propPanel.add(new JLabel(""));
        propPanel.add(new JLabel(""));
        
        //(2)
        label = new JLabel("near S/N");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        nearSN = new JTextField("1");
        propPanel.add(nearSN);
        
        label = new JLabel("far S/N");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        farSN = new JTextField("1");
        propPanel.add(farSN);
        
        
        label = new JLabel("prob_oil");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        probOil = new JTextField("1");
        propPanel.add(probOil);
        
        label = new JLabel("sat_oil");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        satOil = new JTextField("1");
        propPanel.add(satOil);
        
        label = new JLabel("s_sat_oil");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        sigmaSatOil = new JTextField("1");
        propPanel.add(sigmaSatOil);
        
        //(3)
        label = new JLabel("timeAbove");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        time_above = new JTextField("1");
        propPanel.add(time_above);
        
        label = new JLabel("timeBelow");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        time_below = new JTextField("1");
        propPanel.add(time_below);
        
        
        label = new JLabel("prob gas");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        probGas = new JTextField("1");
        propPanel.add(probGas);
        
        label = new JLabel("sat gas");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        satGas = new JTextField("1");
        propPanel.add(satGas);
        
        label = new JLabel("s_sat_gas");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        sigmaSatGas = new JTextField("1");
        propPanel.add(sigmaSatGas);
        
        //(4)
        label = new JLabel("A");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        A = new JTextField("1");
        propPanel.add(A);
        
        label = new JLabel("s_A");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_A = new JTextField("1");
        propPanel.add(s_A);
        
        
        label = new JLabel("prob_LSG");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        probLsg = new JTextField("1");
        propPanel.add(probLsg);
        
        label = new JLabel("sat_LSG");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        satLsg = new JTextField("1");
        propPanel.add(satLsg);
        
        label = new JLabel("s_sat_LSG");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        sigmaSatLsg = new JTextField("1");
        propPanel.add(sigmaSatLsg);
        
        //(5)
        label = new JLabel("B");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        B = new JTextField("1");
        propPanel.add(B);
        
        label = new JLabel("s_B");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_B = new JTextField("1");
        propPanel.add(s_B);
        
        
        label = new JLabel("rho_brine");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        rhoBrine = new JTextField("1");
        propPanel.add(rhoBrine);
        
        label = new JLabel("s_rho b");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_rhoBrine = new JTextField("1");
        propPanel.add(s_rhoBrine);
        
        label = new JLabel("vp_brine");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        vp_brine = new JTextField("1");
        propPanel.add(vp_brine);
        
        //(6)
        label = new JLabel("N/G");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        net2gross = new JTextField("1");
        propPanel.add(net2gross);
        
        label = new JLabel("sigma N/G");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_net2gross = new JTextField("1");
        propPanel.add(s_net2gross);
        
        
        label = new JLabel("fraction");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        float_fra = new JTextField("1");
        propPanel.add(float_fra);
        
        label = new JLabel("s_frac");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_float_fra = new JTextField("1");
        propPanel.add(s_float_fra);
        
        label = new JLabel("s_vp_brine");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_vp_brine = new JTextField("1");
        propPanel.add(s_vp_brine);
        
        //(7)
        label = new JLabel("time");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        time = new JTextField("1");
        propPanel.add(time);
        
        label = new JLabel("s_time");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_time = new JTextField("1");
        propPanel.add(s_time);
        
        
        label = new JLabel("LFIV");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        LFIV = new JTextField("1");
        propPanel.add(LFIV);
        
        label = new JLabel("s_LFIV");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_LFIV = new JTextField("1");
        propPanel.add(s_LFIV);
        
        label = new JLabel("thickness");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        thickness = new JTextField("1");
        propPanel.add(thickness);
        
        //(8)
        label = new JLabel("depth");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        depth = new JTextField("1");
        propPanel.add(depth);
        
        label = new JLabel("s_depth");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_depth = new JTextField("1");
        propPanel.add(s_depth);
        
        
        label = new JLabel("SSdepth");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        SSdepth = new JTextField("1");
        propPanel.add(SSdepth);
        
        label = new JLabel("s_SSdepth");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_SSdepth = new JTextField("1");
        propPanel.add(s_SSdepth);
        
        label = new JLabel("sThickness");
	    label.setHorizontalTextPosition(JLabel.RIGHT);
        propPanel.add(label);
        s_thickness = new JTextField("1");
        propPanel.add(s_thickness);
     
        
		return propPanel;
	}
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		pane.add(updateB);
		//updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		buttonPanel.add(pane, BorderLayout.EAST);
		return buttonPanel;
	}
	
}
