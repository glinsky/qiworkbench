/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import java.awt.GridLayout;
import com.bhpb.ui.BasePane;

public class StackPropsPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JButton updateB;
	private JTextField nXmin,nXmax, nVe, nTime, fXmin, fXmax;
	
	public StackPropsPanel(String title, int width) {
		this.setLayout(new GridLayout(3, 3));
		if (title != null) {
			TitledBorder tb = new TitledBorder(title);
	        //tb.setBorder(new BevelBorder(BevelBorder.LOWERED));
	        tb.setTitleColor(Color.PINK);
			this.setBorder(tb);
		}
		this.add(new JLabel("Xmin (ft)"));
		this.add(new JLabel("Xmax"));
		this.add(new JLabel("ve (ft/s)"));
		this.add(new JLabel("time (s)"));
		this.add(Box.createRigidArea(BasePane.HGAP2));
		
		nXmin = new JTextField("", width);
		nXmax = new JTextField("", width);
		nVe   = new JTextField("", width);
		nTime = new JTextField("", width);
		this.add(nXmin);
		this.add(nXmax);
		this.add(nVe);
		this.add(nTime);
		this.add(new JLabel("near"));
		
		fXmin = new JTextField("", width);
		fXmax = new JTextField("", width);
		this.add(fXmin);
		this.add(fXmax);
		this.add(Box.createRigidArea(BasePane.HGAP2));
		ImageIcon icon = new ImageIcon(getClass().getResource("/icons/update.jpg"));
		updateB = new JButton("");
		updateB.setIcon(icon);
		updateB.setToolTipText("update");
		this.add(updateB);
		this.add(new JLabel("far"));
	}

}
