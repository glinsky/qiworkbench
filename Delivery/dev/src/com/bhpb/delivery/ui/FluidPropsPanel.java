/*
 * <Purpose>
 * <p>
 * Created on March 15, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;


import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.util.List;

import com.bhpb.ui.BasePane;
import com.bhpb.ui.BhpPanel;
import com.bhpb.xsdparams.delivery.Inversion;

public class FluidPropsPanel extends BhpPanel {
	private static final long serialVersionUID = 1L;
	private JTextField v1, v2, e1, e2;
	JPanel fluidPane;
	
	public FluidPropsPanel(BasePane base, String name, String help, int width) {
		super(base, name, help);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		createCheckBox(RockFluidPanel.FLUID);
		aLine = createLine();
		fluidPane = createFluidPane(width);
		this.add(aLine);
		
		if (expanded()) {	
			this.add(fluidPane);	
		} else {
			this.remove(fluidPane);
		}			
		
	}
	
	public void manageObject(List<String> strList) {
		//String action = strList.get(2);
		if (expanded()) {
			this.add(fluidPane);
		} else {
			this.remove(fluidPane);
			
		}
		
	}
	private JPanel createFluidPane(int width) {
		JPanel fPane = new JPanel();
		fPane.setLayout(new GridLayout(3, 3));
		fPane.add(new JLabel(""));
		fPane.add(new JLabel("vp"));
		fPane.add(new JLabel("rho"));
		
		v1 = new JTextField("", width);
		v2 = new JTextField("", width);
		fPane.add(new JLabel("value"));
		fPane.add(v1);
		fPane.add(v2);
			
		e1 = new JTextField("", width);
		e2 = new JTextField("", width);
		fPane.add(new JLabel("sigma"));
		fPane.add(e1);
		fPane.add(e2);	
		return fPane;
	}

	public void populate(Object obj) {
		Inversion.RockFluidProperties.FluidProperties.Fluid fluid = null;
		fluid = (Inversion.RockFluidProperties.FluidProperties.Fluid)obj;
		v1.setText(Float.toString(fluid.getVp().getValue()));
		v2.setText(Float.toString(fluid.getRho().getValue()));
		if (fluid.getSigmaVp() != null)
		   e1.setText(Float.toString(fluid.getSigmaVp().getValue()));
		if (fluid.getSigmaRho() != null)
		   e2.setText(Float.toString(fluid.getSigmaRho().getValue()));
		
	
	}
	
}
