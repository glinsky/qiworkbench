/*
 * <Purpose>
 * <p>
 * Created on March 25, 2007, 1:13 PM
 *
 * @author Charlie Jiang
 * @version 1.0
 */
package com.bhpb.delivery.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import com.bhpb.ui.BasePane;
import com.bhpb.ui.UiConst;
import com.bhpb.xsdparams.delivery.Inversion;
import com.bhpb.core.AgentDelegate;

public class ModelTopBottom extends BasePane {
	private static final long serialVersionUID = 1L;
	JPanel mPane;
	JButton updateB;
	LayerPanel pane1, pane2;
	public ModelTopBottom(AgentDelegate delegate) {
		super(delegate);
	}
	
	public void initPanel() {
		
		Inversion model = (Inversion)delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
		Inversion.ModelDescription.TopLayer  top  = model.getModelDescription().getTopLayer();
		Inversion.ModelDescription.BottomLayer bot  = model.getModelDescription().getBottomLayer();
		
		mPane = new JPanel();
		mPane.setLayout(new BorderLayout());
		
		pane1   = new LayerPanel(this,  UiConst.TOP,    top.getTime().getValue(),  
				                                        top.getNetToGross().getValue(), null, 0);
		pane2   = new LayerPanel(this,  UiConst.BOTTOM, bot.getTime().getValue(),
				                                        bot.getNetToGross().getValue(), null, 2);
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		pane.add(createButtons());
		pane.add(pane1);
		pane.add(Box.createRigidArea(VGAP5));
		pane.add(pane2);
		mPane.add(pane, BorderLayout.NORTH);
		getBasePanel().add(mPane);
		
		pane1.populateTop(top);
		pane2.populateBottom(bot);
	}
	
	public void populate() {
		Inversion model = (Inversion)delegate.getModel(UiConst.LOAD_MODEL_DATA, false);
		Inversion.ModelDescription.TopLayer    top  = model.getModelDescription().getTopLayer();
		Inversion.ModelDescription.BottomLayer bot  = model.getModelDescription().getBottomLayer();
		pane1.populateTop(top);
		pane2.populateBottom(bot);
	}
	
	private JPanel createButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		
		JPanel pane = new JPanel();
		updateB = new JButton("Update");
		pane.add(updateB);
		//updateB.addActionListener(new UpdateAndExport(this, "UPDATE"));
		pane.add(Box.createRigidArea(HGAP15));
		buttonPanel.add(pane, BorderLayout.EAST);
		return buttonPanel;
	}
}
