package com.bhpb.delivery.distribution;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.zip.Adler32;
import java.util.zip.CheckedInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
//import java.util.zip.ZipOutputStream;
import com.bhpb.distribution.DistConst;
import com.bhpb.distribution.IDistribution;
import com.bhpb.qiworkbench.compAPI.CommonUtil;

public class DelivDistribution  implements IDistribution {
	static final int     BUFFER        = 2048;
	public static String UI_CONFIG     = "uiConfig";
	public static String EMAIL_CONFIG  = "editEmail";
	public static String LAST_DIST     = "lastdist";
	
	String name;
	String compDir;
	String outputDir;
	String mailDir;
	
	String config;
	String distDir;
	String binDir;
	
	public DelivDistribution() {
		makeCompDir();
	}
	
	public String getName() {
		return name;
	}
	
	public String getDefaultDir() {
		return distDir;
	}
	
	public String getDirByName(String name) {
		String dir = distDir;
		if (DistConst.OUTPUT.equals(name)) {
			dir = outputDir;
		} else if (DelivDistConst.COMPONENT.equals(name)) {
			dir = compDir;
		} else if (DistConst.MAIL.equals(name)) {
			dir = mailDir;
		} else if (DistConst.CONFIG.equals(name)) {
			dir = config;
		} else if (DistConst.BIN.equals(name)) {
			dir = binDir;
		} 
		return dir;
	}

	public void copyBinary() throws IOException{
		String destDir   = getDirByName(DistConst.BIN);
		String sourceDir = CommonUtil.getAppDir(CommonUtil.COMP_DIR);
		String fName = DelivDistConst.COMPONENT + ".jar";
		copyFile(sourceDir + fName, destDir + fName);
    }
	
	public void copyFile(File sourceF, String destStr) throws IOException{
		File dest = new File(destStr);
		CommonUtil.copyFile(sourceF, dest, true);
    }
	
	public void copyFile(String sourceStr, String destStr) throws IOException{
		    
		    if (sourceStr.equalsIgnoreCase(destStr)) return;
			File source = new File(sourceStr);
			File dest = new File(destStr);
			System.out.println("sourceFile: "+sourceStr + " destFile: " + destStr );
			CommonUtil.copyFile(source, dest, true);
	}
	
	public void copyFiles(String sourceDir, String destDir) throws IOException{
		if (sourceDir.equals(destDir)) return;
		
		File aFile = new File(sourceDir);
		String files[] = aFile.list();
		for (int i = 0; i < files.length; i++) {
			String temp = sourceDir + File.separator + files[i];
			aFile = new File(temp);
			if (!aFile.isDirectory()) {
			    copyFile(temp, destDir   + File.separator + files[i]);
			}
		} 
		
    }
	
	public void copyFiles(List<String> files, String type) throws IOException{
		String destDir = getDirByName(type);
		int cnt = files.size();
		for (int i = 0; i < cnt; i++) {
			String filePath = files.get(i);
			System.out.println("file Name = " + filePath);
			File source = new File(filePath);
			String fileName = source.getName();
			File dest = new File(destDir + File.separator + fileName);
			CommonUtil.copyFile(source, dest, true);
		}
	}
	
	private void makeCompDir() {
		compDir = CommonUtil.getExternalDir(DelivDistConst.COMPONENT);
		File aFile = new File(compDir);
		if (!aFile.exists()) {
			aFile.mkdir();
		}
		
		mailDir = compDir + File.separator + DistConst.MAIL;
		aFile = new File(mailDir);
		if (!aFile.exists()) {
			aFile.mkdir();
		}
		
		config = compDir + File.separator + DistConst.CONFIG;
		aFile =  new File(config);
		if (!aFile.exists()) {
			aFile.mkdir();
		}
		
		outputDir = compDir + File.separator + DistConst.OUTPUT;
		aFile =  new File(outputDir);
		if (!aFile.exists()) {
			aFile.mkdir();
		}
	}
	
    public void makeNamedDir(String distDir1) {
		
		if (distDir1 != null) {
		    distDir = config + File.separator + distDir1;
		    name = distDir1;
		} else {
			distDir = config + File.separator + DistConst.MYDIST;
			name = DistConst.MYDIST;
		}
		
		
		File aFile =  new File(distDir);
		if (!aFile.exists()) {
			aFile.mkdir();
		}
		
		String subDir = distDir + File.separator;
		binDir = subDir + DistConst.BIN;
		aFile =  new File(binDir);
		if (!aFile.exists()) {
			aFile.mkdir();
		}
	}
    
    public  void bhpZip(String name, boolean zipBin) {
    	String toFile = outputDir + File.separator + name;
    }
    
    public void bhpUnZip(String name, String fullName) {
    	String temp = name.substring(0, name.length()-4);
    	makeNamedDir(temp);
    	String zipFile =  fullName;
    	try {
            FileInputStream fis = new FileInputStream(zipFile);
            CheckedInputStream checksum = new CheckedInputStream(fis, new Adler32());
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(checksum));
            ZipEntry entry;
            while((entry = zis.getNextEntry()) != null) {
               extractFile(zis, entry.getName());
            }
            zis.close();
            System.out.println("Checksum: " + checksum.getChecksum().getValue());
         } catch(Exception e) {
            e.printStackTrace();
         }
    }
    
    private void extractFile(ZipInputStream zis, String fileName) {
    	if (fileName == null) return;
    	System.out.println("Extracting: " +fileName);
    	BufferedOutputStream dest = null;
    	int count;
        byte data[] = new byte[BUFFER];
        try {
        	String destFile = getDestFile(fileName);
            FileOutputStream fos = new FileOutputStream(destFile);
            dest = new BufferedOutputStream(fos, BUFFER);
            while ((count = zis.read(data, 0, BUFFER)) != -1) {
                dest.write(data, 0, count);
            }
            dest.flush();
            dest.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private String getDestFile(String fileName) {
    	String retDir = distDir;
    	if (fileName.toUpperCase().lastIndexOf(".JAR") > 0) {
    		retDir = CommonUtil.getAppDir(CommonUtil.COMP_DIR);
    	} else if (DistConst.CONFIG_FILE.equals(fileName) ){
    		retDir = distDir;
    	} 
    	String destFile = retDir + File.separator + fileName;
    	return destFile;
    }
    
    public void   sendByEmail() {
    	
    }
	public void   pushToServer() {
		
	}
	
	public String getProperty(String type) {
		String ret = null;
		if (LAST_DIST.equals(type) ) {
			ret = getDefaultName(type);
		} else if (EMAIL_CONFIG.equals(type)) {
			ret = getEmailConfig();
		} else if (UI_CONFIG.equals(type)) {
			ret = getUIConfig();
		}
		return ret;
	}
	
	public void setProperty(String type, String name) {
		if (type != null) {
			setDefault(type, name);
		}
	}
	
	private String getDefaultName(String propName) {
		String ret = null;
		String propFile  = getPropFile();
		File aFile = new File(propFile);
		if (aFile.exists()) {
		   Properties props = new Properties();
		   try {
			  props.load(new FileInputStream(propFile));
			  ret = props.getProperty(propName);
		   } catch (IOException ex){
			   ex.printStackTrace();
		   }
		}
		if (ret == null) ret = DistConst.MYDIST;
		return ret;
	}
	
	private void setDefault(String propName, String name) {
		String propFile  = getPropFile();
		Properties props = new Properties();
		try {
			  props.load(new FileInputStream(propFile));
			  props.setProperty(propName, name);
			  props.store(new FileOutputStream(propFile), null);
		} catch (IOException ex){
			   ex.printStackTrace();
		}
	}
	
	private String getPropFile() {
		String propFile  = compDir + File.separator + DistConst.PROP_FILE;
		return propFile;
	}
	
	private String getEmailConfig() {
		String propFile  = compDir + File.separator + DistConst.MAIL 
		                   + File.separator + DistConst.CONFIG_FILE;
		return propFile;
	}
	
	private String getUIConfig() {
        String fileName = compDir + File.separator + DistConst.CONFIG_FILE;
        return fileName;
	}
}
