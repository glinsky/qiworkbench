/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.delivery;

import java.awt.Insets;
/**
 * Constants used within Wavelet Decomposition
 *
 * @author Gil Hansen
 * @author Charlie Jiang
 * @version 1.0
 */
public final class DelivConst {
    /**
     * Prevent object construction outside of this class.
     */
    private DelivConst() {}

    public static final int PREF_WIDTH  = 750;
    public static final int PREF_HEIGHT = 650;
    
    public static final int INIT_LAYER_NUM = 3;
    public static final String SAND  = "Sand";
    public static final String SHALE = "Shale";
    
    public static final String LITE  = "lite";
    public static final String DELIV = "deliv";
    
    //actions
    public static final String LOAD_MODEL  = "loadModel";
    
    public final static int BUTTON_ICON_SIZE = 15; // size of icon buttons
    public final static Insets NO_MARGIN_INSETS = new Insets(0,0,0,0);
    public static final String TB_SAVE   = "icons/save.jpg";
    public static final String TB_GEN    = "icons/generate.jpg";
    public static final String TB_EXE    = "icons/execute.jpg";
    public static final String TB_HELP   = "icons/help.jpg";
    
    /** plugin's display name */
    public static final String DELIVERY_LITE_PLUGIN_NAME = "delivery";

    /** plugin GUI's display name */
    public static final String DELIVERY_LITE_GUI_NAME = "Delivery GUI";
    // debug print: 0=none, 1=moderate, 2=verbose
    public static final int DEBUG_PRINT = 2;

    // integer status
    public static final int ERROR_STATUS = 1;
    public static final int OK_STATUS = 0;
    public static final int JOB_RUNNING_STATUS = 1;
    public static final int JOB_ENDED_STATUS = 2;
    public static final int JOB_UNKNOWN_STATUS = 3;
    public static final int JOB_UNSUBMITTED_STATUS = 0;
    
    public static final String MODEL_UI              = "modelUI";
    
    public static final String UI_ELE_PARAM          = "params";
    public static final String UI_ELE_MODEL          = "model";
    public static final String UI_ELE_XML            = "xmldef";
    public static final String UI_ELE_JOB            = "job";
    
    public static final String UI_ELE_PARAM_D        = "Params";
    public static final String UI_ELE_MODEL_D        = "Model";
    public static final String UI_ELE_XML_D          = "XML";
    public static final String UI_ELE_JOB_D          = "Job";
    
    public static final String PANE_PARAM_INFO       = "p_info";
    public static final String PANE_PARAM_SEIS_DATA  = "p_seis_data";
    public static final String PANE_PARAM_ROCK_FLUID = "p_rock_fluid";
    public static final String PANE_PARAM_PROP_IDX   = "p_prop_idx";
    
    public static final String PANE_MODEL_INSTRUCT   = "m_instrct";
    public static final String PANE_MODEL_TOP_BOT    = "m_top_bot";
    public static final String PANE_MODEL_MIDDLE     = "m_middle";
    public static final String PANE_MODEL_S_INFO     = "m_stack_info";
    public static final String PANE_MODEL_DEF        = "m_def";
    
    public static final String PANE_JOB_RUN          = "j_run";
    public static final String PANE_JOB_WORKFLOW     = "j_workflow";
    public static final String PANE_JOB_STATUS       = "j_status";
    
    public static final String PANE_XML_ROCK_F       = "x_rock_f";
    public static final String PANE_XML_LAYERS       = "x_layers";
    public static final String PANE_XML_MODEL        = "x_model";
    public static final String PANE_XML_UI           = "x_ui";
}
