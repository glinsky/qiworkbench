/*
 ###########################################################################
 # DeliveryLite - 
 # Copyright (C) 2006  BHP Billiton Petroleum; BHP Billiton Confidential
 ############################################################################
*/
package com.bhpb.delivery;

import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bhpb.ui.*;
import com.bhpb.core.AgentDelegate;
import com.bhpb.distribution.DistConst;
import com.bhpb.distribution.IDistribution;
import com.bhpb.model.ui.Config;
import com.bhpb.model.ui.UIConfigConverter;
import com.bhpb.qiworkbench.ComponentDescriptor;
import com.bhpb.qiworkbench.IqiWorkbenchComponent;
import com.bhpb.qiworkbench.QiProjectDescriptor;
import com.bhpb.qiworkbench.QiWorkbenchMsg;
import com.bhpb.qiworkbench.compAPI.CompDescUtils;
import com.bhpb.qiworkbench.compAPI.MessagingManager;
import com.bhpb.qiworkbench.compAPI.MsgStatus;
import com.bhpb.qiworkbench.compAPI.MsgUtils;
import com.bhpb.qiworkbench.compAPI.QIWConstants;
import com.bhpb.qiworkbench.compAPI.QiProjectDescUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.bhpb.delivery.util.DelivUtil;
import com.bhpb.delivery.distribution.DelivDistribution;
import com.bhpb.qiworkbench.api.IComponentDescriptor;
import com.bhpb.qiworkbench.api.IQiWorkbenchMsg;
import com.bhpb.xsdparams.delivery.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * DeliveryLitePlugin
 *
 * @author Charlie Jiang
 * @author Gilbert Hansen
 * @version 1.0
 */
public class DeliveryPlugin extends AgentDelegate implements IqiWorkbenchComponent, Runnable {
	private static Logger logger = Logger.getLogger(DeliveryPlugin.class.getName());
	private static String DEFAULT  = "xml/delivDefault.xml";

	// messaging mgr for this class only
	private MessagingManager messagingMgr;

//	gui component
	private DeliveryUI gui;

//	my thread
	private static Thread pluginThread;
	

	/** CID for component instance. Generated before the thread is started and carried as the thread's name. */
	private static String myCID = "";

//	boolean used to stop plugin thread
	private boolean stop = false;

	private static int saveAsCount = 0;
	private static SimpleDateFormat formatter 
	        = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");

	private String jobID;
    private String qiProjectID = "";
	private QiProjectDescriptor qiProjectDesc = new QiProjectDescriptor();
	private ComponentDescriptor projMgrDesc = null;
	IDistribution prepare;
	
	String     agentName = "full-a";
	
	String     modelXML;
	Config     uiConfig;
	Inversion  inversion;
	
	protected static String getCID() {
		return myCID;
	}

	public IComponentDescriptor getComponentDescriptor(){
		return messagingMgr.getMyComponentDesc();
	}
	
	public QiProjectDescriptor getQiProjectDescriptor() {
        return qiProjectDesc;
    }

	 /**
     * Get the component descriptor of the associated PM and therefore project.
     * @return Component descriptor of the associated PM
     */
    public ComponentDescriptor getQiProjMgrDescriptor() {
        return projMgrDesc;
    }


	/**
	 * Initialize the plugin component:
	 * <ul>
	 * <li>Create its messaging manager</li>
	 * <li>Register with the Message Dispatcher</li>
	 * </ul>
	 */
    public void init() {
        try {
            QIWConstants.SYNC_LOCK.lock();
            messagingMgr = new MessagingManager();
            
            myCID = Thread.currentThread().getName();
            
            // register self with the Message Dispatcher
            messagingMgr.registerComponent(QIWConstants.PLUGIN_AGENT_COMP, DelivConst.DELIVERY_LITE_PLUGIN_NAME, myCID);
            prepare = new DelivDistribution();
            
            if (messagingMgr.getState(DelivConst.DELIV, DelivConst.LITE) == null){
                messagingMgr.saveState(DelivConst.DELIV, DelivConst.LITE, "");
            } else {
                agentName = "full-b";
            }
            //notify the startup method initialization finished
            QIWConstants.SYNC_LOCK.unlock();
            super.setInitSuccessful(true);
        } catch (Exception e) {
            super.setInitSuccessful(false);
            super.addInitException(e);
            logger.severe("Exception caught in DeliveryPlugin.init() : " + e.getMessage());
        } finally {
            super.setInitFinished(true);
        }
    }

	/**
	 * Generate state information into xml string format
	 * @return  String
	 */
	public String genState(){
		StringBuffer content = new StringBuffer();
		IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
		//TODO WorkbenchStateManager will need to be changed to conform to the package change of WaveletDecompPlugin in ServletDispatcher
		content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + CompDescUtils.getDescPreferredDisplayName(desc) + "\">\n");
		content.append(gui.genState());
		content.append("</component>\n");
		return content.toString();
	}

	/**
	 * Generate state information into xml string format in repsonse to save as command
	 * @return  String
	 */
	public String genStateAsClone(){
		StringBuffer content = new StringBuffer();
		IComponentDescriptor desc = messagingMgr.getMyComponentDesc();
		//TODO WorkbenchStateManager will need to be changed to conform to the package change of AmpExtPlugin in ServletDispatcher
		String displayName = "";
		saveAsCount++;
		if(saveAsCount == 1)
			displayName = "CopyOf" + CompDescUtils.getDescPreferredDisplayName(desc);
		else
			displayName = "Copy(" + saveAsCount + ")Of" + CompDescUtils.getDescPreferredDisplayName(desc);
		content.append("<component componentKind=\"" + CompDescUtils.getDescComponentKind(desc) + "\" componentType=\"" + messagingMgr.getRegisteredComponentDisplayNameByDescriptor(desc) + "\" preferredDisplayName=\"" + displayName + "\">\n");
		content.append(gui.genState());
		content.append("</component>\n");

		return content.toString();
	}

	public QiProjectDescriptor getProjectDescriptor(){
		return qiProjectDesc;
	}
	
	/** restore plugin and it's gui to previous condition
	 *
	 * @param node xml containing saved state variables
	 */
	void restoreState(Node node){
		String preferredDisplayName = ((Element)node).getAttribute("preferredDisplayName");
		NodeList children = node.getChildNodes();
		for(int i = 0; i < children.getLength(); i++){
			Node child = children.item(i);
			if(child.getNodeType() == Node.ELEMENT_NODE){
				if(child.getNodeName().equals(DeliveryUI.class.getName())){
					String project = ((Element)child).getAttribute("project_name");
					String fileSystem = ((Element)child).getAttribute("project_root");
					QiProjectDescUtils.setQiSpace(qiProjectDesc, fileSystem);
					QiProjectDescUtils.setQiProjectName(qiProjectDesc, project);
					QiProjectDescUtils.setQiProjectReloc(qiProjectDesc, messagingMgr.getServerOSFileSeparator());
					gui = new DeliveryUI(this);
					gui.restoreState(child);

					if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
						CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
						gui.renamePlugin(preferredDisplayName);
					}
				}
			}
		}
	}

	/** Initialize the plugin, then start processing messages its receives.
	 * The pleaseStop flag is set when user has terminated the GUI, and it is time to quit
	 * If a message has the skip flag set, it is to be handled by the GUI, not this class
	 */
	public void run() {
		// initialize the Wavelet Decomposition plugin
		init();
//		process any messages received from other components
		while(stop == false) {
			IQiWorkbenchMsg msg = messagingMgr.peekNextMsg();

			if(msg != null && !(msg.getMsgKind().equals(QIWConstants.DATA_MSG) && msg.skip())){
				msg = messagingMgr.getNextMsgWait();
				processMsg(msg);
			}
		}
	}

	/**
	 * Find the request matching the response and process the response based on the request.
	 */
	public void processMsg(IQiWorkbenchMsg msg) {
		/** Request that matches the response */
		IQiWorkbenchMsg request = null;

//		log message traffic
		logger.fine("msg="+msg.toString());

//		Check if a response. If so, process and consume response
		if(messagingMgr.isResponseMsg(msg)) {
			request = messagingMgr.checkForMatchingRequest(msg);
			String cmd = MsgUtils.getMsgCommand(request);
			// check if from the message dispatcher
			if(messagingMgr.isResponseFromMsgDispatcher(msg)) {
				if(cmd.equals(QIWConstants.NULL_CMD))
					return;
			}
			else if(cmd.equals(QIWConstants.GET_JOB_OUTPUT_CMD)) {
				ArrayList<String> stdOut = (ArrayList<String>)msg.getContent();
				String jobOutput = "";
				for(String s : stdOut){
					jobOutput += s + "\n";
				}
				//gui.setStdOutTextArea(jobOutput);

			}
			else if(cmd.equals(QIWConstants.SUBMIT_JOB_CMD)) {
				jobID = (String)msg.getContent();
			}
//			route the result from file chooser service back to its caller
			else if(cmd.equals(QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD)) {
				ArrayList list = (ArrayList)msg.getContent();
				if(((Integer)list.get(0)).intValue() == JFileChooser.APPROVE_OPTION) {
					final String filePath = (String)list.get(1);
					String action = (String)list.get(3);
				}
			} else  if (cmd.equals(QIWConstants.GET_PROJ_INFO_CMD)) {
                ArrayList projInfo = (ArrayList)msg.getContent();
                qiProjectID = (String)projInfo.get(0);
                qiProjectDesc = (QiProjectDescriptor)projInfo.get(1);

                //reset window title (if GUI up)
                if (gui != null) {
                    String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
                    //gui.resetTitle(projName);
                }
			}
//			TODO other possible responses...
			else
				logger.warning("WaveletDecompPlugin: Response to " + cmd + " command not processed " + msg.toString());
			return;
		}
//		Check if a request. If so, process and send back a response
		else if(messagingMgr.isRequestMsg(msg)) {
			String cmd = msg.getCommand();
			// deactivate plugin came from user, tell gui to quit and stop run method
			if(cmd.equals(QIWConstants.DEACTIVATE_PLUGIN_CMD) || cmd.equals(QIWConstants.REMOVE_COMPONENT_CMD) || 
					      cmd.equals(QIWConstants.DEACTIVATE_COMPONENT_CMD) || cmd.equals(QIWConstants.REMOVE_PLUGIN_CMD)) {
				if(DelivConst.DEBUG_PRINT > 0)
					System.out.println(CompDescUtils.getDescPreferredDisplayName(messagingMgr.getMyComponentDesc()) + " quitting");
				try{
					if(gui != null && gui.isVisible())
						gui.closeGUI();
					// unregister plugin
					messagingMgr.unregisterComponent(messagingMgr.getComponentDesc(myCID));
				}
				catch (Exception e) {
					IQiWorkbenchMsg res = MsgUtils.genAbnormalMsg(msg, MsgStatus.SC_INTERNAL_ERROR, "Unknown exception occurred with " + myCID);
					messagingMgr.routeMsg(res);
				}
				stop = true;
				messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,myCID + " is successfully deactivated.");
			} else
				if(cmd.equals(QIWConstants.OPEN_COMPONENT_GUI_CMD)) {
					if(gui != null)
						gui.setVisible(true);
				} else
					if(cmd.equals(QIWConstants.CLOSE_COMPONENT_GUI_CMD)) {
						if(DelivConst.DEBUG_PRINT > 0)
							System.out.println(myCID + " quitting");
						if(gui != null){
							messagingMgr.sendResponse(msg,Component.class.getName(),(Component)gui);
							gui.closeGUI();
						}
					} else
						if(cmd.equals(QIWConstants.SET_PREFERRED_DISPLAY_NAME_CMD)) {
							String preferredDisplayName = (String)msg.getContent();
							if(preferredDisplayName != null && preferredDisplayName.trim().length() > 0){
								CompDescUtils.setDescPreferredDisplayName(messagingMgr.getMyComponentDesc(), preferredDisplayName);
								gui.renamePlugin(preferredDisplayName);
							}
							messagingMgr.sendResponse(msg,QIWConstants.COMP_DESC_TYPE,messagingMgr.getMyComponentDesc());
						}
//			save state
						else if(cmd.equals(QIWConstants.SAVE_COMP_CMD))
							messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genState());
//			save component state as clone
						else if(cmd.equals(QIWConstants.SAVE_COMP_AS_CLONE_CMD)) {
							messagingMgr.sendResponse(msg,QIWConstants.STRING_TYPE,genStateAsClone());
							// restore state
						} else if(cmd.equals(QIWConstants.RESTORE_COMP_CMD)) {
							Node node = (Node)msg.getContent();
							restoreState(node);
							messagingMgr.sendResponse(msg,DeliveryUI.class.getName(),gui);
						}
//			invoke is done after activate
						else if(cmd.equals(QIWConstants.INVOKE_SELF_CMD)) {
							//Do nothing if don't have a GUI
							// Create the plugin's GUI, but don't make it visible. That is
							// up to the Workbench Manager who requested the plugin be
							// activated. Pass GUI the CID of its parent.
							//check to see if content incudes xml information which means invoke self will
							// do the restoration
							ArrayList msgList = (ArrayList)msg.getContent();
							String invokeType = (String)msgList.get(0);
							if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_RESTORED)){
								Node node = ((Node)((ArrayList)msg.getContent()).get(2));
								restoreState(node);
							} else if(invokeType.equals(QIWConstants.COMPONENT_INVOKE_NEW)){
								//qiProjectDesc = ((QiProjectDescriptor)((ArrayList)msg.getContent()).get(2));
								gui = new DeliveryUI(this.getComponentDescriptor().getPreferredDisplayName(),this);
								java.awt.Point p = (java.awt.Point)msgList.get(1);
				                  if(p != null)
				                	  gui.setLocation(p);
							}

							//Send a normal response back with the plugin's JInternalFrame and
							//let the Workbench Manager add it to the desktop and make
							//it visible.
							messagingMgr.sendResponse(msg,DeliveryUI.class.getName(),gui);
						}
//			user selected rename plugin menu item
						else if(cmd.equals(QIWConstants.RENAME_COMPONENT)) {
							ArrayList<String> names = new ArrayList<String>(2);
							names = (ArrayList)msg.getContent();
							if(DelivConst.DEBUG_PRINT > 0)
								System.out.println("Rename myself from " + names.get(0) + " to " + names.get(1));
							gui.renamePlugin(names.get(1));
						} else if (cmd.equals(QIWConstants.NOTIFY_ASSOC_PROJMGR_CMD)) {
				              //Just a notification. No response required or expected.
				              projMgrDesc = (ComponentDescriptor)msg.getContent();
				              //Note: Cannot get info about PM's project because GUI may not be up yet.
				              if (projMgrDesc != null)
				                  messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.GET_PROJ_INFO_CMD, projMgrDesc);
				          } else if (cmd.equals(QIWConstants.NOTIFY_PROJ_INFO_CHANGED_CMD)) {
				              ArrayList info = (ArrayList)msg.getContent();
				              ComponentDescriptor pmDesc = (ComponentDescriptor)info.get(0);
				              QiProjectDescriptor projDesc = (QiProjectDescriptor)info.get(1);
				              //ignore message if not from associated PM
				              if (projMgrDesc != null && pmDesc.getCID().equals(projMgrDesc.getCID())) {
				            	  qiProjectDesc = projDesc;
//				TODO: update PID [get from projDesc?]
				                  //update window title
				                  String projName = QiProjectDescUtils.getQiProjectName(qiProjectDesc);
				                  //gui.resetTitle(projName);
				              }
				          } else
							logger.warning("WaveletDecompPlugin: Request not processed, requeued: " + msg.toString());
			return;
		}
	}



	/** Send a message to the Workbench Manager to remove this instance of WaveletDecomp from the component tree and send a message to it to deactivate itself.
	 */
	public void deactivateSelf(){
		//ask Workbench Manager to remove WaveletDecomp from workbench GUI and then send it back to self to deactivate self
		IComponentDescriptor wbMgr = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_MGR_NAME);
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.QUIT_COMPONENT_CMD,wbMgr,QIWConstants.STRING_TYPE,
				messagingMgr.getMyComponentDesc());
	}
	
	/** Get Messaging Manager of WaveletDecomp component.
	 *  @return  MessagingManager
	 */
	public MessagingManager getMessagingMgr(){
		return messagingMgr;
	}

	/** Invoke File Chooser Service
	 * @param list Passed through to fileChooser from caller
	 *
	 */
	public void callFileChooser(ArrayList list){
		if(list == null)
			return;
		ComponentDescriptor fileChooser = null;
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_FILE_CHOOSER_SERVICE_CMD,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null){
			logger.warning("Respone to get file chooser service returning null due to timed out");
			return;
		}
		else if(MsgUtils.isResponseAbnormal(response)) {
			logger.warning("Internal error occurring in getting file chooser service. Cause: " + MsgUtils.getMsgContent(response));
			return;
		}
		else
			fileChooser = (ComponentDescriptor)MsgUtils.getMsgContent(response);

		messagingMgr.sendRequest(QIWConstants.CMD_MSG, QIWConstants.INVOKE_FILE_CHOOSER_CMD,fileChooser,QIWConstants.ARRAYLIST_TYPE,list);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.GET_RESULT_FROM_FILE_CHOOSER_CMD,fileChooser);
		return;
	}

	/**
	 * Get the job output by using JobAdaptor
	 * @return
	 */
	public void getJobOutput(){
		if(jobID != null){
			ArrayList params = new ArrayList();
			params.add(messagingMgr.getLocationPref());
			params.add(jobID);
			String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
					QIWConstants.GET_JOB_OUTPUT_CMD,
					QIWConstants.ARRAYLIST_TYPE,params);
		}else
			logger.info("jobID is not yet available for browsing the output.");
	}

	/**
	 * Write script content to a given directory
	 * @param dir parameters in which the scripts is stored
	 * @param scripts contents
	 * @return
	 */
	public void writeScript(String dir, String scripts){
		String result = checkDirExist(dir);
		logger.info("check if the " + dir + " exists: " + result);
		boolean ok = false;

		if (result.equals("no")) {
			int status = JOptionPane
			.showConfirmDialog(
					gui,
					"The script will be written into "
					+ dir
					+ " but the directory does not exist. Would you like the application to create it for you?",
					"Confirm action", JOptionPane.YES_NO_OPTION);
			if (status == JOptionPane.YES_OPTION) {
				result = mkDir(dir);
				if (result.equals("success"))
					ok = true;
				logger.info("Create directory " + dir + " returning " + result);
			} else
				return;
		} else if (result.equals("yes"))
			ok = true;
		
		if (ok){
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			String fileNamePrefix = "wd_" + formatter.format(ts);
			String fileName = fileNamePrefix + ".sh";
			boolean success = writeScripts(dir + messagingMgr.getServerOSFileSeparator()
					+ fileName, scripts);
		}
	}

	public void submitJob(List<String> params) {
		int errorStatus = 0;
		jobID = null;
		String errorContent = "";
		params.add(0,messagingMgr.getLocationPref());
		params.add(1,"sh");
		params.add(2,"-c");
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params);
	}

	/**
	 * Cancel a job by using JobAdaptor
	 * @return
	 */
	public void cancelJob(){
		if(jobID == null || jobID.trim().length() == 0){
			logger.info("no job active to cancel or job id not yet available for cancelation");
			return;
		}
		ArrayList params = new ArrayList();
		params.add(messagingMgr.getLocationPref());
		params.add(jobID);
		messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.KILL_JOB_CMD,QIWConstants.ARRAYLIST_TYPE,params);
	}


//	public int submitJob(String script,String paramString) {
	public void submitJob(String script,String paramString) {
		// submit job for execution
		int status = 0;
		int errorStatus = 0;
		String errorContent = "";
		ArrayList params = new ArrayList();
		params.add(0,messagingMgr.getLocationPref());
		params.add(1,"sh");
		params.add(2,"-c");
		params.add(3,"cd " + messagingMgr.getProject() + "/scripts; " + script + " " + paramString);
		logger.info("params = " + params);
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params);
		
	}

	/**
	 * Create a directory of a given path
	 * @param filePath as directory name
	 * @return yes, no or error
	 */
	public String mkDir(String filePath){
		ArrayList params = new ArrayList();
		params.add(messagingMgr.getLocationPref());
		params.add(filePath);
		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CREATE_DIRECTORY_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgId,1000);
		if(response != null && !MsgUtils.isResponseAbnormal(response))
			return (String)MsgUtils.getMsgContent(response);
		else if(response == null){
			JOptionPane.showMessageDialog(gui,"Timed out problem in running CREATE_DIRECTORY_CMD.",
					"IO Error",JOptionPane.WARNING_MESSAGE);
			return "error";
		}else{
			JOptionPane.showMessageDialog(gui,"Error in running CREATE_DIRECTORY_CMD. " + (String)MsgUtils.getMsgContent(response),
					"IO Error",JOptionPane.WARNING_MESSAGE);
			return "error";
		}
	}

	/**
	 * Write script content to a given path
	 * @param path where the scripts is stored
	 * @param content scripts contents
	 * @return true or false
	 */
	public boolean writeScripts(String path, String content){
		ArrayList<String> params = new ArrayList<String>();
		//the first element is the loation preference (local or remote)
		params.add(messagingMgr.getLocationPref());
//		the second element is to add file path and name user just selected
		params.add(path);
//		the 3rd element is to add xml string returned from the components
		params.add(content);

//		send a message to message dispatcher for persist the state information into selected file path within preferred environment (local or remote)
		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.FILE_WRITE_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);

		if (resp == null){
			logger.info("Response returning null from command FILE_WRITE_CMD in writeScripts path=" + path);
			return false;
		}else if(resp.isAbnormalStatus()){
			logger.info("Abnormal response to write scripts FILE_WRITE_CMD:  in writeScripts " + (String)resp.getContent());
			JOptionPane.showMessageDialog(gui, "Error in writing script " + path + " cause: " +  (String)resp.getContent(), "QI Workbench",
					JOptionPane.WARNING_MESSAGE);
			return false;
		} else{
			logger.info("normal response to write scripts FILE_WRITE_CMD: " + (String)resp.getContent());
		}
		return true;
	}

	/**
	 * Check to see if a given directory exists in the file system
	 * @param filePath file path to be checked
	 * @return yes, no, or error
	 */
	public String checkDirExist(String filePath){
		ArrayList params = new ArrayList();
		params.add(messagingMgr.getLocationPref());
		params.add(filePath);
		String msgId = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.CHECK_FILE_EXIST_CMD,QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg resp = messagingMgr.getMatchingResponseWait(msgId,5000);
		String temp = "";
		if(resp != null && !MsgUtils.isResponseAbnormal(resp)){
			temp = (String)resp.getContent();
		}else if(resp == null){
			JOptionPane.showMessageDialog(gui,"Timed out problem in running CHECK_FILE_EXIST_CMD.",
					"IO Error",JOptionPane.WARNING_MESSAGE);
			temp = "error";
		}else{
			JOptionPane.showMessageDialog(gui,"Error in running CHECK_FILE_EXIST_CMD. " + (String)MsgUtils.getMsgContent(resp),
					"IO Error",JOptionPane.WARNING_MESSAGE);
			temp = "error";
		}
		return temp;
	}

	/**
	 * showErrorDialog is called when ErrorDialogService is needed
	 * @param type is QIWConstants.ERROR_DIALOG or QIWConstants.WARNING_DIALOG
	 * @param message is one-line message
	 * @param stack is StackTrace
	 * @param causes is list of possible causes
	 * @param suggestions is list of suggested remedies
	 */
	public void showErrorDialog(String type, StackTraceElement[] stack, String message,
			String[] causes, String[] suggestions) {
		ArrayList list = new ArrayList(7);
		// component
		list.add(gui);
//		message type
		list.add(type);
//		caller's display name
		list.add(getComponentDescriptor().getDisplayName());
//		stack trace
		list.add(stack);
//		message
		list.add(message);
//		possible causes
		list.add(causes);
//		suggested remedies
		list.add(suggestions);
//		send message to start error service
		getErrorService(list);
	}
	/**
	 * showInternalErrorDialog starts error service for showing internal errors, for example, unexpected NULL etc
	 * @param stack stackTrace
	 * @param message one-line error message
	 */
	public void showInternalErrorDialog(StackTraceElement[] stack, String message) {
		showErrorDialog(QIWConstants.ERROR_DIALOG,stack,message,
				new String[] {"Internal plugin error"},
				new String[] {"Contact workbench support"});
	}

	/**
	 * getErrorService starts ErrorDialogService
	 * @param list ArrayList of arguments
	 */
	private void getErrorService(ArrayList list) {
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.GET_ERROR_DIALOG_SERVICE_CMD,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
		if(response == null || response.isAbnormalStatus()) {
			JOptionPane.showMessageDialog(null,"Error getting Error Service");
			return;
		}
		ComponentDescriptor errorServiceDesc = (ComponentDescriptor)response.getContent();
		msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.INVOKE_ERROR_DIALOG_SERVICE_CMD,
				errorServiceDesc,QIWConstants.ARRAYLIST_TYPE,list,true);
		response = messagingMgr.getMatchingResponseWait(msgID,10000);
	}
	/**
	 * Cancel a given shell script name by calling the linux command kill and ps
	 * @param scriptName
	 * @return
	 */
	public boolean cancelJob(String scriptName){
		ArrayList params = new ArrayList();
		String cmd = "kill -9 `ps ux | awk '/" + scriptName +"/ && !/awk/ {print $2}'`";
		logger.info("cmd " + cmd);
		params.add(0,messagingMgr.getLocationPref());
		params.add(1,"sh");
		params.add(2,"-c");
		params.add(cmd);
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,5000);
		if(response == null){
			logger.info("response to SUBMIT_JOB_CMD for Cancel Job is null due to timed out problem.");
			return false;
		}else if(response.isAbnormalStatus()){
			logger.info("response to SUBMIT_JOB_CMD for Cancel Job with abnormal status: " + (String)response.getContent());
			return false;
		}else
			return true;

	}

	/**
	 * Get process id of a given shell script name by calling the linux command
	 * @param scriptName
	 * @return process id
	 */
	public String getProcessId(String scriptName) {
		ArrayList params = new ArrayList();
		String cmd = "ps ux | awk '/" + scriptName +"/ && !/awk/ {print $2}'";
		params.add(0,messagingMgr.getLocationPref());
		params.add(1,"sh");
		params.add(2,"-c");
		params.add(cmd);
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
				QIWConstants.SUBMIT_JOB_CMD,
				QIWConstants.ARRAYLIST_TYPE,params,true);
		IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,5000);

		if(response == null){
			logger.info("Response to SUBMIT_JOB_CMD getProcessId for " + scriptName + " is null due to timed out problem.");
			return null;
		}else if(response.isAbnormalStatus()){
			logger.info("Response to SUBMIT_JOB_CMD getProcessId for " + scriptName + " with abnormal status: " + (String)response.getContent());
			return null;
		}else{
			String jobID = (String)MsgUtils.getMsgContent(response);
			logger.info("SUBMIT_JOB_CMD jobID = " + jobID);
			params.clear();
			params.add(messagingMgr.getLocationPref());
			params.add(jobID);
			msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
					QIWConstants.GET_JOB_OUTPUT_CMD,
					QIWConstants.ARRAYLIST_TYPE,params,true);
			response = messagingMgr.getMatchingResponseWait(msgID,5000);

			if(response == null){
				logger.info("Response to GET_JOB_OUTPUT_CMD getProcessId for " + scriptName + " is null due to timed out problem.");
				return null;
			}else if(response.isAbnormalStatus()) {
				logger.info("Response to GET_JOB_OUTPUT_CMD getProcessId for " + scriptName + " with abnormal status: " + (String)response.getContent());
				return null;
			}else{
				ArrayList stdOut = (ArrayList)response.getContent();
				logger.info("stdOut getProcessId for " + scriptName + " = " + stdOut);
				if(stdOut != null && stdOut.size() > 0)
					return (String)stdOut.get(0);
				else
					return "";
			}
		}
	}

	public String getJobStatus(){
		if(jobID != null){
			ArrayList params = new ArrayList();
			params.add(messagingMgr.getLocationPref());
			params.add(jobID);
			String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,
					QIWConstants.GET_JOB_STATUS_CMD,
					QIWConstants.ARRAYLIST_TYPE,params,true);
			IQiWorkbenchMsg response = messagingMgr.getMatchingResponseWait(msgID,10000);
			if(response == null){
				logger.info("Response to GET_JOB_STATUS_CMD returning null due to timed out problem.");
				return "Messaging Timed Out";
			}else if(MsgUtils.isResponseAbnormal(response)){
				logger.info("Abnormal response to GET_JOB_STATUS_CMD due to " + MsgUtils.getMsgContent(response));
				return "Abnormal Messaging Response";
			}else{
				int status = ((Integer)response.getContent()).intValue();
				if(status == -1)
					return "Job Running";
				else
					return "Job ends with status of " + status;
			}
		}else{
			logger.info("The job id is not yet available for browsing job status information.");
			return "No Running Job Found";
		}

	}

	/**
	 * call the state manager to store the state
	 */
	public void saveState(){
		IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_CMD,stMgrDesc,
				QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
	}

	/**
	 * call the state manager to store the state
	 */
	public void saveStateAsClone(){
		IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_AS_CLONE_CMD,stMgrDesc,
				QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
	}
	
	/**
	 * call the state manager to store the state then quit the component
	 */
	public void saveStateThenQuit(){
		IComponentDescriptor stMgrDesc = messagingMgr.getComponentDescFromDisplayName(QIWConstants.WORKBENCH_STATE_MANAGER_NAME);
		String msgID = messagingMgr.sendRequest(QIWConstants.CMD_MSG,QIWConstants.SAVE_COMP_THEN_QUIT_CMD,stMgrDesc,
				QIWConstants.STRING_TYPE,messagingMgr.getMyComponentDesc());
	}


	/** Launch the Wavelet Decomposition plugin:
	 *  <ul>
	 *  <li>Start up the plugin thread which will initialize the plugin.</li>
	 *  </ul>
	 * <p>
	 * NOTE: Each thread's init() must finish before the next thread is
	 * started. This is accomplished by monitoring the SYNC_LOCK object.
	 */
	public static void main(String[] args) {
		DeliveryPlugin pluginInstance = new DeliveryPlugin();
		String cid = args[0];
		pluginThread = new Thread(pluginInstance, cid);
		pluginThread.start();
		long threadId = pluginThread.getId();
		logger.info("Delivery Lite Thread-"+Long.toString(threadId)+" started");
		QIWConstants.SYNC_LOCK.lock();
		QIWConstants.SYNC_LOCK.unlock();
		logger.info("Delivery Lite main finished");
	}
	
	public String getName() {
		return agentName;
	}
	
	public void resize() {
		gui.setSize(DelivConst.PREF_WIDTH, DelivConst.PREF_HEIGHT);
	}
	/**
	 * get model object based model name
	 */
	public Object getModel(String  modelName, boolean start){
	    Object model = null; 
		if (UiConst.LOAD_MODEL_UI.equals(modelName)) {
			model = getModelUI(start);
		} else if (UiConst.LOAD_MODEL_DATA.equals(modelName) ){
			model = getModelInversion(start);
		}
        return model;
    }
	
	/**
	 * Convert model to XML
	 */
	public void toXmlFile(String  modelName){
		if (UiConst.LOAD_MODEL_UI.equals(modelName)) {
			toXmlUIFile();
		} else if (UiConst.LOAD_MODEL_DATA.equals(modelName) ){
			toXmlInversionFile();
		}
    }
	
	/**
	 * Convert model to XML in a string text format
	 */
	public String toXmlString(String  modelName){
		String ret = "";
		if (UiConst.LOAD_MODEL_UI.equals(modelName)) {
			ret = toXmlStrUI();
		} else if (UiConst.LOAD_MODEL_DATA.equals(modelName) ){
			ret = toXmlStrInversion();
		}
		return ret;
    }
	
	private void toXmlUIFile() {
		if (uiConfig == null)  return;
		XStream xStream = new XStream(new DomDriver());
        xStream.alias(DistConst.CONFIG_NAME, Config.class);
        xStream.registerConverter(new UIConfigConverter());
        String xmlFile = prepare.getProperty(DelivDistribution.UI_CONFIG);
        messagingMgr.saveToFile(xmlFile, xStream, uiConfig);
    }
	
	private void toXmlInversionFile(){
	    if (inversion == null) return; 
		try {
		  JAXBContext context 
		     = JAXBContext.newInstance("com.bhpb.xsdparams.delivery", 
		    		                   this.getClass().getClassLoader());
		  
		  Marshaller marshaller = context.createMarshaller();
		  marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
		  marshaller.marshal(inversion, new FileOutputStream(modelXML));
		  
		}catch (Exception ex) {
		  ex.printStackTrace();
		}
    }
	
	private Object getModelInversion(boolean start){
		URL modelURL   = null;
		InputSource is = null;
		logger.info("--------------model XML = " + modelXML);
		try {
		   if (start || inversion == null) {
		      JAXBContext  context 
		         = JAXBContext.newInstance("com.bhpb.xsdparams.delivery", 
		        		                   this.getClass().getClassLoader()) ;
		      
		      Unmarshaller unmarshaller = context.createUnmarshaller() ;
		      if (modelXML == null) {
					ClassLoader cl = getClass().getClassLoader();
					modelURL = cl.getResource(DEFAULT);
				    if (modelURL == null) {
				          File f = new File(DEFAULT);
				          modelURL = f.toURL();
				    }
				    is = new InputSource(modelURL.toString());
				    inversion = (Inversion)unmarshaller.unmarshal(is) ;
				    logger.info("--------------HERE = " + modelURL.toString());
				    modelXML = DEFAULT;
			  } else {
		            inversion = (Inversion)unmarshaller.unmarshal(new FileInputStream(modelXML)) ;
			  }
		   } 
		   
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return inversion;
	}
	
	private Object getModelUI(boolean start){
       
		if (uiConfig == null || start) {
            XStream xStream = new XStream(new DomDriver());
            xStream.alias(DistConst.CONFIG_NAME, Config.class);
            xStream.registerConverter(new UIConfigConverter());
            String xmlFile = prepare.getProperty(DelivDistribution.UI_CONFIG);
            File aFile= new File(xmlFile);
            if (!aFile.exists()) {
                uiConfig = DelivUtil.defaultUIConfig();   
                messagingMgr.saveToFile(xmlFile, xStream, uiConfig);
            } else {
            	uiConfig = (Config)messagingMgr.loadFromFile(
                        DelivConst.MODEL_UI, DistConst.CONFIG_NAME , xmlFile, xStream);
            }
       }
       return uiConfig;
    }
	
	private String toXmlStrUI() {
		XStream xStream = new XStream(new DomDriver());
        xStream.alias(DistConst.CONFIG_NAME, Config.class);
        xStream.registerConverter(new UIConfigConverter());
        Object config = getModel(UiConst.LOAD_MODEL_UI, false);
        return xStream.toXML(config);
	}
	
	private String toXmlStrInversion(){
		String ret = "";
		if (inversion  == null) {
		    ObjectFactory objFactory = new ObjectFactory();
		    inversion = objFactory.createInversion();
	    }
		
		try {
		   JAXBContext context 
		      = JAXBContext.newInstance("com.bhpb.xsdparams.delivery", 
		    		                    this.getClass().getClassLoader());
		   Marshaller marshaller = context.createMarshaller();
		   marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
		   ByteArrayOutputStream bos = new ByteArrayOutputStream();
		   marshaller.marshal(inversion, bos);
		   ret = bos.toString();
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ret;
	}

	public String getModelXML() {
		return modelXML;
	}

	public void setModelXML(String modelXML) {
		this.modelXML = modelXML;
	}
}
